m = 9;
hold on
T = 50;
num = 100000;

for l=0:4
    Perr = zeros(1,T);
    for k = T:T
        for i = 1:num
            %the first l ys are flipped to N(1,1)
            %the last l ys are N(-1,1)
            %the midle m-2l ys are changed to N(0,1) by a change of measure
            y = randn(m,k);
            y(1:l,:) = y(1:l,:)+1;
            y(m-l+1:m,:) = y(m-l+1:m,:)-1;
            ysum = sum(y,2);
            p = exp(-sum(ysum(l+1:m-l))-(m-2*l)*k/2);
            ysum = sort(ysum);
            if sum(ysum(l+1:m-l))>0
                Perr(k) = Perr(k) + p;
            end
        end
    end
    
    Perr = Perr/num;
%    plot(1:T,log(Perr));
    rate = -log(Perr(T))/T
end