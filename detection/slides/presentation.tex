\documentclass{beamer}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,plotmarks,circuits.ee.IEC,positioning,fit}

\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength\figureheight
\newlength\figurewidth
\setlength\figureheight{7cm} 
\setlength\figurewidth{10cm} 

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{../tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Smin}{Smin}
\DeclareMathOperator{\Smid}{Smid}
\DeclareMathOperator{\Smax}{Smax}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Sequential Detection]{Sequential Detection in Adversarial Environment}

\author[Yilin Mo]{Yilin Mo,\, Richard M. Murray}
\institute[Caltech]{California Institute of Technology\\ Control \& Dynamical Systems } 
\date[Sept 2, 2014]{Sept 2, 2014 at Nanyang Technological University}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Cyber Physical Systems (CPS) refer to the embedding of widespread sensing, computation, communication and control into physical spaces.
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. \alert{Safety Critical}
    \item The next generation CPS, such as smart grids, will make extensive use of information technology. 
    \item Stuxnet raised significant concerns on CPS security. 
    \item How to analyze classical estimation/control algorithms on CPS?
    \item How to design secure estimation/control algorithms on CPS?
  \end{itemize}
\end{frame}

\begin{frame}{A Classical Detection Problem}
  \begin{itemize}
    \item We want to decide whether the state $\theta$ is $-1$ or $1$.
      \begin{align*}
	\theta=\begin{cases}
	  -1 &\text{w.p.~}0.5\\
	  +1 &\text{w.p.~}0.5\\
	\end{cases}
      \end{align*}
    \item $m$ sensors are measuring the state: $y_i(k)\sim\mathcal N(\theta,1)$.
   \item Let us define 
     \begin{displaymath}
       y(k) = \begin{bmatrix}y_1(k)&\dots&y_m(k)\end{bmatrix},\,Y(k) = \begin{bmatrix}y(1)&\dots&y(k)\end{bmatrix}.
     \end{displaymath}
   \item A detector at time $k$  is a function $f_k:\mathbb R^{mk}\rightarrow \{-1,1\}$.
   \item A detection strategy is an infinite sequence of detectors $f = (f_1,\,f_2,\,\dots)$.
    \end{itemize}
\end{frame}

\begin{frame}{A Classical Detection Problem}
  
  Without the attacker, it is well-known that the optimal detector with minimum detection error  is a Naive Bayes Detector:
      \begin{align*}
	\hat \theta(k)=f_k(Y(k))=\begin{cases}
	  -1 &\text{if }\sum_{i=1}^m\sum_{t=1}^k y_i(t)/mk < 0\\
	  +1 &\text{if }\sum_{i=1}^m\sum_{t=1}^k y_i(t)/mk \geq 0\\
	\end{cases}.
      \end{align*}

  However, the Naive Bayes Detector is not secure. Suppose the attacker compromise one sensor. Thus, it can manipulate the summation arbitrarily. Hence, it has full control over $\hat \theta$. 
\end{frame}

\begin{frame}{Countermeasures}
  \begin{block}{Information Security}
    \begin{itemize}
      \item Tamper-resistant microprocessor, Software attestation, Secure Communication Protocol,\dots
      \item It is hard to guarantee security for every single sensor. (A single compromised sensor can totally ruin the Naive Bayes detector.)
      \item Physical attacks?
    \end{itemize}
  \end{block}
  \begin{block}{System Theory}
    \begin{itemize}
      \item Bad data detection, Robust detection ($\varepsilon-$contamination, bounded total variation difference, \dots)  
      \item The uncertainty, failure and noise models from system theory are usually quite different from the attack models.
    \end{itemize}
  \end{block}
  Our goal: To design the optimal detection strategy that can withstand Byzantine attacks from at most $n$ sensors.
\end{frame}

\begin{frame}{Attack Model}
  The attacker compromised $n$ sensors, the set of which is denoted as $\mathcal I$. The detector knows $n$, but does not know $\mathcal I$.
  \begin{displaymath}
   y'(k) = y(k) + u(k),  
  \end{displaymath}
  where $u_i(k) = 0$ if $i\notin \mathcal I$.

  We consider different information set $\mathcal G(k)$ of the attacker at time $k$:
  \begin{itemize}
    \item Weak Attack Model: The attacker knows only the measurements of the compromised sensors, i.e., $\mathcal G(k) = Y_{\mathcal I}(k)$.
    \item Strong Attack Model: The attacker knows the measurements of all sensors, i.e., $\mathcal G(k) = Y(k)$. 
  \end{itemize}
 The disturbance $u(k)$ depends on the information set $\mathcal G(k)$ and $\mathcal I$: 
  \begin{displaymath}
   u(k) = g_k(\mathcal G(k),\mathcal I). 
  \end{displaymath}
  The attack strategy $g = (g_1,\,g_2,\,\dots)$.
\end{frame}

\begin{frame}{Attack Model}
  Attacks are different from uncertainties, failures, noises:
  \begin{itemize}
      \item Not ``energy'' constrained or even bounded: 0-norm constrained, the compromised measurements can be arbitrarily large
      \item Not independent or random: the compromised measurements are jointly selected by the attacker 
  \end{itemize}
\end{frame}

\begin{frame}{Detection Performance: Probability of Error}
  \begin{itemize}
    \item The probability of error at time $k$ is denoted as
  \begin{displaymath}
    P_e(k) = \max_{\mathcal I}\; P(f_k(Y'(k)) \neq \theta). 
  \end{displaymath}
\item Clearly, $P_e(k)$ is a function of the detection strategy $f$ and the attack strategy $g$.
\item  In general, for a fixed $k$, optimizing $P_e(k)$ directly (either from the system's perspective or the attacker's perspective) is difficult.
\item As a result, we will focus on the asymptotic performance when $k\rightarrow\infty$.
  \end{itemize}
\end{frame}

\begin{frame}{Chernoff Information}
  \begin{itemize}
    \item Suppose $m = 1$ and $n = 0$, the Naive Bayes Detector takes the following form:
      \begin{align*}
	\hat \theta(k)=f_k(Y(k))=\begin{cases}
	  -1 &\text{if }\sum_{t=1}^k y_1(t)/k < 0\\
	  +1 &\text{if }\sum_{t=1}^k y_1(t)/k \geq 0\\
	\end{cases}.
      \end{align*}
    \item By LLN, the probability of error goes to $0$.
    \item Moreover, it goes to $0$ exponentially fast, i.e.,
      \begin{displaymath}
	\lim_{k\rightarrow\infty}-\frac{\log P_e(k)}{k} = \max_{0<\alpha<1} -\log \int_{-\infty}^\infty \left(\frac{p^-(x)}{p^+(x)}\right)^\alpha p^+(x) dx = \frac{1}{2}.
      \end{displaymath}
    \item For general $m$ with $n = 0$, the probability of error for the Naive Bayes Detector satisfies:
      \begin{displaymath}
	P_e(k)\sim e^{-\frac{1}{2}mk}.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Asymptotic Performance}
  \begin{itemize}
    \item In adversarial environment, let us define the rate function $I$ as
      \begin{displaymath}
	I = \liminf_{k\rightarrow\infty} -\frac{\log P_e(k)}{k}.
      \end{displaymath}
      Roughly speaking, 
      \begin{displaymath}
	P_e(k)\sim e^{-Ik}. 
      \end{displaymath}
      Larger rate implies better detection performance.
    \item  $I$ is a function of both detection strategy $f$ and attack strategy $g$.
    \item The detector wants to maximize $I$ while the attacker wants to minimize $I$.
    \item Our goal is to find a Nash-equilibrium $(f^*,g^*)$, such that
      \begin{displaymath}
	I(f^*,g)\geq I(f^*,g^*) \geq I(f,g^*).	
      \end{displaymath}
  \end{itemize}
\end{frame}
\section{Main Results}
\begin{frame}{Nash-equilibrium when $m \leq 2n$}
  \it Theorem: For both weak and strong attack model, the following strategy pair $(f^*,g^*)$ is a Nash-equilibrium with $I(f^*,g^*) = 0$:
  \begin{block}{Attack Strategy $g^*$}
    \begin{enumerate}
      \item Flip $m-n$ compromised sensors' measurements.
      \item Set the rest $2n-m$ compromised sensors' measurements to $0$. 
    \end{enumerate}
  \end{block}
  \begin{block}{Detection Strategy $f^*$}
    $f_k = 1$ for all $k$.
  \end{block}
\end{frame}

\begin{frame}{Sketch of the Proof: $I(f^*,g)\geq I(f^*,g^*)$}
  \begin{enumerate}
    \item If the detector employs the strategy $f^*$, then regardless of the attack strategy $g$:
      \begin{displaymath}
	P_e(k) = 0.5,\,\forall k,
      \end{displaymath}
      Therefore,
      \begin{displaymath}
	I(f^*,g) = I(f^*,g^*) = \liminf_{k\rightarrow\infty}-\frac{\log P_e(k)}{k}= 0.
      \end{displaymath}
  \end{enumerate}
\end{frame}

\begin{frame}{Sketch of the Proof: $I(f^*,g^*)\geq I(f,g^*)$}
  \begin{enumerate}
  \setcounter{enumi}{1}
    \item Suppose that $m =3$, $n = 2$: 
      \begin{center}
	\begin{tikzpicture}
	  \matrix(yy)[matrix of math nodes,ampersand replacement=\&,column sep=1cm] {
	  \theta = -1 :\& Observed: \& \theta = 1:\\
	  y_1(k)\sim\mathcal N(-1,1) \& y_1'(k)\sim\mathcal N(-1,1) \& y_1(k)\sim\mathcal N(1,1)\\
	  y_2(k)\sim\mathcal N(-1,1) \& y_2'(k)\sim\mathcal N(1,1) \& y_2(k)\sim\mathcal N(1,1)\\
	  y_3(k)\sim\mathcal N(-1,1) \& y_3'(k)=0 \& y_3(k)\sim\mathcal N(1,1)\\
	  };
	  \draw [->] (yy-3-1) to node[above]{flip} (yy-3-2);
	  \draw [->] (yy-4-1) to node[below]{set to 0} (yy-4-2);
	  \draw [->] (yy-2-3) to node[above]{flip} (yy-2-2);
	  \draw [->] (yy-4-3) to node[below]{set to 0} (yy-4-2);
	\end{tikzpicture}
      \end{center}
      The compromised measurements do not yield any information on whether $\theta = 1$ or $\theta = -1$. The optimal detector in this case is $f_k = 1$ or $f_k = -1$.
  \end{enumerate}
\end{frame}

\begin{frame}{Nash-Equilibrium when $m \geq 2n+1$}
  We need to define the following ``summation'' functions:
 \begin{definition}
   Define the symmetric functions $\Smin_{2n},\,\Smid_{2n},\,\Smax_{2n}:\mathbb R^{m}\rightarrow \mathbb R$ as  
      \begin{align*}
	\Smin_{2n}(x_1,\ldots,x_m) &\triangleq \sum_{i=1}^{m-2n}x_i,\\
	\Smid_{2n}(x_1,\ldots,x_m) &\triangleq \sum_{i=n+1}^{m-n}x_i,\\
	\Smax_{2n}(x_1,\ldots,x_m) &\triangleq \sum_{i=2n+1}^{m}x_i,
      \end{align*}
      when $x_1\leq \dots\leq x_m$.
    \end{definition}
\end{frame}

\begin{frame}{Nash-Equilibrium when $m \geq 2n+1$}
  \begin{itemize}
    \item If $\|x' - x\|_0\leq l$, then
      \begin{displaymath}
	\Smin_{2n}(x) \leq \Smid_{2n}(x')\leq \Smax_{2n}(x).
      \end{displaymath}
    \item Assuming $m=5$, $n = 1$:
      \begin{center}
	\begin{tikzpicture}[semithick,>=latex]
	  \draw [->] (0,0)--(10,0);
	  \draw plot[mark=x] coordinates{(1,0)} node [above] {$x_1$};
	  \draw plot[mark=x] coordinates{(2,0)} node [above] {$x_2$};
	  \draw plot[mark=x] coordinates{(4,0)} node [above] {$x_3$};
	  \draw plot[mark=x] coordinates{(5,0)} node [above] {$x_4$};
	  \draw plot[mark=x] coordinates{(7,0)} node [above] {$x_5$};

	  \draw [->] (0,-2)--(10,-2);
	  \draw plot[mark=x] coordinates{(9,-2)} node [above] {$x_1'$};
	  \draw plot[mark=x] coordinates{(2,-2)} node [above] {$x_2'$};
	  \draw plot[mark=x] coordinates{(4,-2)} node [above] {$x_3'$};
	  \draw plot[mark=x] coordinates{(5,-2)} node [above] {$x_4'$};
	  \draw plot[mark=x] coordinates{(7,-2)} node [above] {$x_5'$};

	  \draw [->,dashed] (1,0)--(1,-1)--(9,-1)--(9,-2);
      \begin{pgfonlayer}{background}
	\fill [fill=red!30] (3,-2.5) rectangle (8,-1.25);
	\node at (5.5,-2.3) {$\Smid_2(x')$};

	\fill [fill=blue!30] (3,-0.5) rectangle (8,0.75);
	\node at (5.5,-0.3) {$\Smax_2(x)$};
      \end{pgfonlayer}
	\end{tikzpicture}
      \end{center}
    \item $\Smid_{2n}$ is a more ``secure'' version of sum. 
  \end{itemize}
\end{frame}


\begin{frame}{Nash-Equilibrium when $m \geq 2n+1$}
\it Theorem: For both weak and strong attack model, the following strategy pair $(f^*,g^*)$ is a Nash-equilibrium with $I(f^*,g^*) = (m-2n)/2$:
  \begin{block}{Attack Strategy $g^*$}
       Flip $n$ compromised sensors' measurements.
  \end{block}
  \begin{block}{Detection Strategy $f^*$}
    \begin{enumerate}
      \item For each sensor $i$, compute a local sum $ \Lambda_i'(k) = \sum_{t=1}^k y_i'(t)$.
      \item The detector $f_k$ is given by
	\begin{displaymath}
	  f_k(Y'(k)) = \begin{cases}
	    -1 &\text{if }\Smid_{2n}(\Lambda'(k))< 0\\
	    1 &\text{if }\Smid_{2n}(\Lambda'(k))\geq 0\\
	  \end{cases}
	\end{displaymath}
    \end{enumerate}
  \end{block}
\end{frame}

\begin{frame}{Sketch of the Proof: $I(f^*,g)\geq (m-2n)/2$}
  \begin{itemize}
    \item Assuming that the detector employs $f^*$.
    \item Notice that there are at most $n$ local sums such that the following inequality holds:
      \begin{displaymath}
	\sum_{t=1}^k y_i'(t)= \Lambda_i'(k)\neq \Lambda_i(k) = \sum_{t=1}^k y_i(t)
      \end{displaymath}
    \item Hence,
      \begin{displaymath}
	\Smin_{2n}(\Lambda(k))\leq \Smid_{2n}(\Lambda'(k))\leq \Smax_{2n}(\Lambda(k)) 
      \end{displaymath}
    \item The detector will make the corrected decision if
      \begin{displaymath}
	\left\{ \theta = 1,\,\Smin_{2n}(\Lambda(k))\geq 0 \right\}\bigcup \left\{ \theta = -1,\,\Smax_{2n}(\Lambda(k))< 0 \right\}.
      \end{displaymath}
    \item The probability of such an event is roughly $\sim e^{-(m-2n)k/2}$
    \item Hence $I(f^*,g)\geq (m-2n)/2$.
  \end{itemize}
\end{frame}

\begin{frame}{Sketch of the Proof: $I(f,g^*)\leq (m-2n)/2$}
  \begin{itemize}
    \item Assume that the attacker employs the flipping strategy $g^*$.
    \item Suppose that the attacker flips the measurements of sensors in $\mathcal I$, then the probability for the detector to observe $Y'(k)$ is
      \begin{align*}
	P(Y'(k)|\theta = 1,\mathcal I) &= \prod_{i\notin \mathcal I} \prod_{t=1}^k p^+(y_i'(t))\prod_{i\in \mathcal I} \prod_{t=1}^k p^-(y_i'(t)),\\
	P(Y'(k)|\theta =- 1,\mathcal I) &= \prod_{i\notin \mathcal I} \prod_{t=1}^k p^-(y_i'(t))\prod_{i\in \mathcal I} \prod_{t=1}^k p^+(y_i'(t)).
      \end{align*}
    \item One can prove that the optimal $f_k$ (with minimum probability of error) is
      \begin{displaymath}
	\frac{\sum_{\mathcal I} P(Y'(k)|\theta = 1,\mathcal I)}{\sum_{\mathcal I}P(Y'(k)|\theta=-1,\mathcal I)}\overset{\theta = -1}{\underset{\theta = 1}{\lessgtr}} 1
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Sketch of the Proof: $I(f,g^*)\leq (m-2n)/2$}
  \begin{itemize}
    \item The rate function for that optimal detector is $(m-2n)/2$. 
    \item Hence, for any detection strategy
      \begin{displaymath}
	I(f,g^*)\leq (m-2n)/2
      \end{displaymath}
    \item Therefore
      \begin{displaymath}
	I(f,g^*)\leq (m-2n)/2 = I(f^*,g^*)\leq I(f^*,g).
      \end{displaymath}
  \end{itemize}
\end{frame}

\section{Numerical Examples}
\begin{frame}{Numerical Examples}
  We assume $m = 9$ and plot the probability of error for the equilibrium strategy for different $n$
    \begin{center}
      \inputtikz{Perror}
    \end{center}
\end{frame}

\begin{frame}{Numerical Examples}

  We fix $k = 50$ and compute the corresponding $ - \log(P_e(k))/k$.
    \begin{table}[h]
      \centering
      \begin{tabular}{c|cc}
	\toprule
	& $-\frac{\log(P_e(k))}{k}$ & $I(f^*,g^*)$\\
	\midrule
	$l=0$ & 4.58&4.5\\
	$l=1$ & 3.58 & 3.5\\
	$l=2$ & 2.57 & 2.5\\
	$l=3$ & 1.57 & 1.5\\
	$l=4$ & 0.56 & 0.5\\
	\bottomrule
      \end{tabular}  
    \end{table}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
  \begin{itemize}
    \item We consider the problem of sequential detection in the presence of compromised sensory data.
    \item We characterize the performance of the detector by the rate function.
    \item We assume that the detector wants to maximize the rate function, while the attacker wants to minimize the rate function.
    \item For the case where $m \leq 2n$, the optimal attack strategy is a flip \& set strategy, where the optimal detector is a constant.
    \item For the case where $m \geq 2n+1$, the optimal attack strategy is a flip strategy, where the optimal detector is given by a truncated sum. 
    \item Compromising $1$ sensor is effectively equivalent to removing $2$ sensors:
      \begin{displaymath}
	I(f^*,g^*) = \max ( 0, \frac{m-2n}{2}).
      \end{displaymath}
  \end{itemize}
\end{frame}
\end{document}
