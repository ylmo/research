%\documentclass[10pt,final,journal,letterpaper]{IEEEtran}
%double column, 9pt, for final publication of technical note 
%\documentclass[9pt,technote,letterpaper]{IEEEtran}
%single column double space, for journal draft
\documentclass[12pt,draftcls,onecolumn]{IEEEtran}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}
\newcommand{\tv}{\tilde v}
\newcommand{\tx}{\tilde x}
\newcommand{\ts}{\tilde s}
\newcommand{\tC}{\tilde C}
\newcommand{\tW}{\tilde W}
\newcommand{\tw}{\tilde w}
\newcommand{\tu}{\tilde u}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\diag}{diag}

\title{Privacy Preserving Average Consensus}

\author{Yilin Mo$^*$
\thanks{
$*$: Yilin Mo is with the CDS department of California Institute of Technology, CA 91125. Email: {yilinmo@caltech.edu}
}
}

\begin{document} \maketitle

\begin{abstract}

\end{abstract}

The consensus algorithm is divided into two steps:
\begin{enumerate}
  \item Each node adds a random noise $v_i(k)$ to its state $x_i(k)$, where $k$ is the index of time and $i$ is the index of node. We define the new state to be $x_{i}^+(k)$, i.e.,
    \begin{equation}
      x_{i}^+(k) = x_i(k) + v_i(k).  
      \label{eq:noisestep}
    \end{equation}
  \item Each node communicate with its neighbor and update its state to the average value, i.e.,
    \begin{equation}
      x_i(k+1) = \sum_{j\in \mathcal N(i)}a_{i,j}x_{j}^+(k),
      \label{eq:consensusstep}
    \end{equation}
    where $\mathcal N(i)$ denotes the set of all neighbors of node $i$.
\end{enumerate}
\eqref{eq:noisestep} and \eqref{eq:consensusstep} can be jointly written in matrix form as
\begin{equation}
  x(k+1) = A(x(k) + v(k)),
  \label{eq:matrixform}
\end{equation}
where $x(k) \triangleq \left[ x_1(k),\dots,x_n(k) \right]'\in \mathbb R^n$, $v(k)\triangleq \left[ v_1(k),\dots,v_n(k) \right]'\in \mathbb R^n$ and $A\in \mathbb R^{n\times n}$ where $A_{i,j} = a_{i,j}$. The eigenvalues of $A$ are denoted as
\begin{equation}
 1=|\lambda_1|\leq |\lambda_2|\leq\dots\leq|\lambda_n|, 
\end{equation}
where we define $\rho \triangleq |\lambda_2|$

We make the following assumptions:
\begin{enumerate}
  \item $A$ is symmetric. 
  \item The initial condition $x(0)$ is a zero mean\footnote{If $x(0)$ is not zero-mean, then we can always substract the mean since it is an a-prior knowledge.} Gaussian random variable with covariance $I$.
  \item The noise process $\{v_i(k)\}$ for each node $i$ is identical with and independent from each other. The noise process is also independent from the initial conditions $x(0)$. The noise process is zero-mean, i.e.,
    \begin{equation}
     \mathbb E v_i(k) = 0,\forall i,k.
    \end{equation}
    Furthermore, we assume that the covariance of the noise process satisfies
    \begin{equation}
      \alpha \phi^{2k} \leq \Cov\left(\sum_{l=0}^k v_i(l)\right)\leq \beta \phi^{2k},\forall i,
    \end{equation}
    where $0<\alpha<\beta$, $|\phi|<1$.
\end{enumerate}
\subsection{Convergence of Consensus Algorithm}
Since $A$ is symmetric, there exists an orthogonal matrix $U$, such that
\begin{equation}
A = U'\Lambda U,  
\end{equation}
where $\Lambda = \diag(\lambda_1,\dots,\lambda_n)$. Let us denote 
\begin{align}
  \tx(k) &\triangleq [\tx_1(k),\dots,\tx_n(k)]' = U x(k),\\
  \tv(k) &\triangleq [\tv_1(k),\dots,\tv_n(k)]' = U v(k).
\end{align}
To simplify notation, we further define
\begin{displaymath}
  \ts_i(\lambda,k) = \sum_{l=0}^k \lambda^{k-l}\tv_i(l).
\end{displaymath}
It is trivial to see that $\tx(0)$ is zero mean Gaussian with covariance $I$. The following Lemmas characterize the statistical properties of ${\tv_i(k)}$:
\begin{lemma}
  \label{lemma:noiseproperty1}
  $\{\tv_i(k)\}$ is a zero-mean process. If $i\neq j$, then $\{\tv_i(k)\}$ and $\{\tv_j(k)\}$ are linear independent\footnote{It is worth noticing that unless $\{v_i(k)\}$ are Gaussian process, $\{\tv_i(k)\}$ and $\{\tv_j(k)\}$ are not independent in general.}with respect to each other, i.e.,
  \begin{displaymath}
   \Cov(\tv_i(k_1),\tv_j(k_2))=0,\forall i\neq j. 
  \end{displaymath}
  Moreover, $\{\ts_i(k)\}$ satisfies
  \begin{equation}
      \alpha \phi^{2k} \leq \Cov\left( \ts_i(1,k)\right)\leq \beta \phi^{2k},\forall i,
    \label{eq:covexponentialdecay}
  \end{equation}
\end{lemma}
\begin{proof}
The proof can be easily done by expanding $\tv_i(k)$ as a linear combination of $v_i(k)$s. 
\end{proof}
\begin{lemma}
  \label{lemma:noiseproperty2}
  The covariance of $\tv_i(k)$ satisfies
  \begin{equation}
    \Cov(\tv_i(k))\leq M_1\phi^{2k},
  \end{equation}
  where $M_1>0$ is a constant independent of $k$.
\end{lemma}
\begin{proof}
Since
\begin{displaymath}
  \tv_i(k)  = \ts_i(1,k) -  \ts_i(1,k-1),
\end{displaymath}
we have that
\begin{align*}
  \Cov(\tv_i(k))&\leq \left[\sqrt{\Cov(\ts_i(1,k)}+\sqrt{\Cov (\ts_i(1,k-1))}\right]^2\\
  &\leq \beta\left( 1/\phi+1 \right)^2\phi^{2k}.
\end{align*}
\end{proof}
\begin{lemma}
  For any $-1<\lambda < 1$, the following inequality holds
  \begin{equation}
    \Cov(\ts_i(\lambda,k))\leq \begin{cases}
      M_2\left[\max(|\lambda|,\phi)\right]^{2k}&\textrm{if }|\lambda|\neq \phi\\
      M_2 k^2\phi^{2k}&\textrm{if }|\lambda|=\phi
    \end{cases}.
    \label{eq:convergence1}
  \end{equation}
  where $M_2 > 0$ is a constant independent of $k$. 
  \label{lemma:convergence1}
\end{lemma}
\begin{proof}
  By induction, we have that
  \begin{displaymath}
    \Cov( \ts_i(\lambda,k))\leq \left\{\sum_{l=0}^k \sqrt{\Cov\left[\lambda^{k-l}\tv_i(l)\right]}\right\}^2
    \leq  M_1\left\{\sum_{l=0}^k |\lambda|^{k-l}\phi^l\right\}^2.
  \end{displaymath}
  If $|\lambda|>\phi$, then
  \begin{displaymath}
    \Cov( \ts_i(\lambda,k))\leq \frac{M_1|\lambda|^2}{(|\lambda|-\phi)^2}|\lambda|^{2k}
  \end{displaymath}
  Similarly, if $\phi > |\lambda|$, then
  \begin{displaymath}
    \Cov( \ts_i(\lambda,k))\leq \frac{M_1|\phi|^2}{(|\lambda|-\phi)^2}\phi^{2k}
  \end{displaymath}
  Finally, if $\phi = |\lambda|$, then
  \begin{displaymath}
    \Cov( \ts_i(\lambda,k))\leq M_1k^2\phi^{2k}
  \end{displaymath}
\end{proof}
Now we are ready to prove the main theorem on the convergence speed of consensus.
\begin{theorem}
  \label{theorem:consensusconvergence}
  Define 
  \begin{equation}
    \bar x \triangleq \frac{1}{n}\sum_{i=1}^n x_i(0), \, e(k) \triangleq x(k) - \bar x \mathbf 1,
  \end{equation}
  where $\mathbf 1\in \mathbb R^n$ is an all one vector. If $\phi\neq |\lambda_2|$, then there exists constants $M_3,M_4>0$, such that the following inequalities hold:
  \begin{equation}
    M_3 \left[\max(\phi,|\lambda_2|)\right]^{2k} \leq \mathbb E\|e(k)\|_2^2\leq M_4 \left[\max(\phi,|\lambda_2|) \right]^{2k}.
    \label{eq:consensusconvergence1}
  \end{equation}
  Otherwise if $\phi = |\lambda_2|$, then there exists constants $M_5,M_6>0$, such that the following inequality hold:
  \begin{equation}
    M_5 |\lambda_2|^{2k} \leq \mathbb E\|e(k)\|_2^2\leq M_6 k^2 |\lambda_2| ^{2k}.
    \label{eq:consensusconvergence2}
  \end{equation}
\end{theorem}
\begin{proof}
  By the linear independence between the initial states and noise processes, one can verify that
  \begin{align*}
    \mathbb E\|e(k) \|_2^2 &=(\tx_1(k) - \bar x)^2 + \sum_{i=2}^n \tx_i(k)^2\\
    &= \Cov(\ts_i(1,k))+\sum_{i=2}^n |\lambda_i|^{2n}\Cov(\tx_i(0)) + \sum_{i=2}^n \Cov(\ts_i(\lambda_i,k))\\
    & = \Cov(\ts_i(1,k))+\sum_{i=2}^n |\lambda_i|^{2n} + \sum_{i=2}^n \Cov(\ts_i(\lambda_i,k)).
  \end{align*}
  By Lemma~\ref{lemma:noiseproperty1} and \ref{lemma:convergence1}, we can finish the proof. 
\end{proof}
\subsection{Perfect Observability and Fundamental Limitations}
We now consider the case that an observer, either an outsider or a node, is trying to estimate the initial values of the whole network based on the information it receives. We assume the observer receives the message from a subset of $m$ nodes $i_1,\dots,i_m$. Let us define
\begin{equation}
  C \triangleq \left[e_{i_1},\dots,e_{i_m}\right] ',
  \label{eq:defcmatrix}
\end{equation}
where $e_i$ is an all zero vector except the $i$th entry equals $1$. Define all the messages received by the observer at time $k$ to be $y(k)$, then
\begin{displaymath}
 y(k) = C(x(k) + v(k)) = \tC (\tx(k) + \tv(k)), 
\end{displaymath}
where $\tC \triangleq CU'$. We further define $\tW$ as the row space of $\tC$.

Denote $\mathcal F(k)$ as the sigma-algebra generated by $y(0),\dots,y(k)$. 
\begin{definition}
Let $w\in \mathbb R^n$, then $w'x(0)$ is called perfectly observable if there exists $h(k)\in \mathbb R$, which is measurable with respect to $\mathcal F(k)$, such that
\begin{displaymath}
  \lim_{k\rightarrow\infty} \mathbb E(h(k)-w'x(0))^2 = 0 
\end{displaymath}
\end{definition}
\begin{remark}
  all perfectly observable $w'x(0)$s form a subspace.
\end{remark}
\begin{theorem}
  $\mathbf 1'x(0)$ is perfectly observable. Furthermore, if there exists a vector $\tw \in \tW$, such that
  \begin{displaymath}
   \tw = [0,\tw_2,\dots,\tw_n]',
  \end{displaymath}
  then $w'x(0)$ is also perfectly observable, where
  \begin{displaymath}
    w = U'\left[0,\frac{\tw_2}{1-\lambda_2},\dots,\frac{\tw_n}{1-\lambda_n}\right]'
  \end{displaymath}
  \label{theorem:perfectobservability}
\end{theorem}
The following lemma is needed to prove \ref{theorem:perfectobservability}.
\begin{lemma}
  \label{lemma:convergence2}
  For any $-1<\lambda < 1$, the following holds
  \begin{equation}
    \lim_{k\rightarrow\infty} \Cov\left[\sum_{l=0}^k\ts_i(\lambda,l)\right]=0
    \label{eq:convergence2}
  \end{equation}
\end{lemma}
\begin{proof}
  Since $|\lambda|<1$, one can prove the following equality:
  \begin{align*}
    \sum_{l=0}^k \ts_i(\lambda,l) = \frac{1}{1-\lambda}\left(\ts_i(1,k) -\lambda\ts_i(\lambda,k) \right),
  \end{align*}
  which implies that
  \begin{displaymath}
    \Cov\left[\sum_{l=0}^k\ts_i(\lambda,l)\right]\leq \frac{1}{(1-\lambda)^2}\left\{ \sqrt{\Cov(\ts_i(1,k)) }+ \lambda \sqrt{\Cov(\ts_i(\lambda,k)) }\right\}^2.
  \end{displaymath}
  By \eqref{eq:covexponentialdecay} and \eqref{eq:convergence1}, we can conclude the proof.
\end{proof}
We are now ready to proof Theorem~\ref{theorem:perfectobservability}
\begin{proof}
$\mathbf 1'x(0)$ is perfectly observable by Theorem~\ref{theorem:consensusconvergence}. 

Now assume that $\tw =[0,\tw_2,\dots,\tw_n]'\in \tW$, there exists $\tu\in \mathbb R^m$, such that $\tw' = \tu'\tC$. Now consider 
\begin{align*}
  h(k) &=\sum_{l=0}^k \tu' y(l) = \sum_{l=0}^k \tw'( \tx(l) +\tw'\tv(l))\\
  &=\sum_{i=2}^n\sum_{l=0}^k \tw_i(\lambda_i^l\tx_i(0) + \ts_i(\lambda_i,l))\\
  &=\sum_{i=2}^n(1-\lambda_i^{k+1})\frac{\tw_i}{1-\lambda_i} \tx_i(0) + \sum_{i=2}^n\sum_{l=0}^k\ts_i(\lambda_i,l)
\end{align*}
Clearly, the first summation converges to $w'x(0)$ and the covariance of the second summation converges to $0$ by Lemma~\ref{lemma:convergence2}. As a result, $w'x(0)$ is perfectly observable.
\end{proof}
\begin{remark}
  Since $C$ is of rank $m$, the perfect observable subspace is at least of rank $m$, due to Theorem~\ref{theorem:perfectobservability}.
\end{remark}
\subsection{Design of the Noise Process}
In this section, we propose a form of the noise process $\{v_i(k)\}$, such that the observer cannot learn the exact value of $w'x(0)$ if it is not 

\appendices

\bibliographystyle{IEEEtran}
\bibliography{}
\end{document}

