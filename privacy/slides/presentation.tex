\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,mindmap,plotmarks}

\usepackage{pgfplots}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\newcommand{\tI}{\tilde {\mathcal I}}
\newcommand{\tA}{\tilde A}
\newcommand{\ty}{\tilde y}
\newcommand{\tx}{\tilde x}
\newcommand{\tw}{\tilde w}
\newcommand{\tv}{\tilde v}
\newcommand{\tC}{\tilde C}
\newcommand{\tP}{\tilde P}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Privacy]{Privacy Preserving Average Consensus}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{Control and Dynamical Systems\\ California Institute of Technology}
\date[Dec 15, 2014]{CDC 2014\\ \vspace{1cm}
\small Joint Work with Richard M. Murray}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette secondary}{fg=caltechcolor!70!black,bg=gray!15!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}
%\logo{\includegraphics[height=0.75cm]{caltechlogo.pdf}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Average consensus is a widely used algorithm for distributed computing and control.
    \item Applications: coordination of groups of mobile autonomous agents, cooperative control of vehicle formations, dynamic load balancing, social networks, \dots
    \item In many applications, the participating agent may not want to reveal its own initial state.
    \item Our goal: Design an average consensus algorithm, such that the privacy of the initial states of all agents is preserved.
  \end{itemize}
\end{frame}

\begin{frame}{Preliminary: Average Consensus}
  \begin{itemize}
    \item We model the network composed of $n$ agents as an \emph{undirected} and \emph{connected} graph $G = \{V,\,E\}$.
    \item Define the neighborhood of sensor $i$ as $\mathcal N(i)$.
    \item Each agent has an initial scalar state $x_{i}(0)$.
    \item Update equation:
      \begin{displaymath}
	x_i(k+1) = a_{ii} x_{i}(k) + \sum_{j\in \mathcal N(i)} a_{ij} x_{j}(k).  
      \end{displaymath}
    \item Update equation in matrix form:
      \begin{displaymath}
	x(k+1) = A x(k),
      \end{displaymath}
      where we assume that $A$ is \emph{symmetric}.
  \end{itemize}
\end{frame}

\begin{frame}{Preliminary: Average Consensus}
     Define the average vector as
      \begin{displaymath}
	\bar x\triangleq \frac{\mathbf 1' x(0)}{n} \mathbf 1.
      \end{displaymath}
  The following conditions are necessary and sufficient for all the agents to reach average consensus:
  \begin{enumerate}
    \item[(A1)] $ \lambda_1 = 1$ and  $|\lambda_i| < 1$ for all $i = 2,\ldots, n$.
    \item[(A2)] $A\mathbf 1 = \mathbf 1$, i.e., $\mathbf 1$ is an eigenvector of $A$.
  \end{enumerate}
\end{frame}

\begin{frame}{Privacy Concerns}
  \begin{itemize}
    \item Without loss of generality, we consider agent $n$ wants to estimate the initial states of other agents.
    \item Denote the neighborhood of agent $n$ as
      \begin{displaymath}
	\mathcal N(n) = \{j_1,\dots,j_m\}.
      \end{displaymath}
    \item Define
      \begin{displaymath}
	\begin{split}
	  C &\triangleq \begin{bmatrix}
	    e_{j_1}&\dots&e_{j_m}&e_n
	  \end{bmatrix}' \in \mathbb R^{(m+1)\times n},\\
	  y(k)&\triangleq \begin{bmatrix}x_{j_1}(k)&\dots&x_{j_m}(k)&x_n(k)
	  \end{bmatrix}=Cx(k).
	\end{split}
      \end{displaymath}
    \item The information set for agent $n$ at time $k$ is 
      \begin{displaymath}
	\mathcal I(k) = \{y(0),\dots,y(k)\}. 
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Privacy Concerns}
  For agent $n$, the problem becomes a standard estimation problem of the following linear system: 
  \begin{displaymath}
    x(k+1) = A x(k),\,y(k) = C x(k). 
  \end{displaymath}
  Since the system is noiseless, if $x_i$ is in the observable space of $(A,C)$, then agent $n$ can perfectly recover the initial state $x_i(0)$ of agent $i$. 

  \alert{The privacy of agent $i$ is breached.}

  \begin{center}
    \begin{tikzpicture}
      \node (n4) at (0,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {4};
      \node (n1) at (0,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {1};
      \node (n3) at (1.5cm,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {3};
      \node (n2) at (1.5cm,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {2};
      \draw [semithick] (n1)--(n2)--(n3)--(n4)--(n1);
    \end{tikzpicture}
  \end{center}

  For agent $4$, it can infer all the initial state $x(0)$ in two steps.
\end{frame}

\section{Problem Formulation}
\begin{frame}{Privacy Preserving Average Consensus}
  \begin{enumerate}
    \item At time $k$, each agent generates a standard normal distributed random variable $v_i(k)$ with mean $0$ and variance 1. We assume that all the random variables $\{v_i(k)\}_{i=1,\dots,n,\,k=0,1,\dots}$ are jointly independent.
    \item Each agent then adds a random noise $w_i(k)$ to its state $x_i(k)$, where
      \begin{displaymath}
	w_i(k) = \begin{cases}
	  v_i(0)&\text{, if }k = 0\\
	  \varphi^kv_i(k)-\varphi^{k-1}v_i(k-1)&\text{, otherwise}
	\end{cases},
	\label{eq:addednoise}
      \end{displaymath}
      where $0<|\varphi|<1$ is a constant for all agents. Define the new state to be $x_{i}^+(k)$, i.e.,
      \begin{displaymath}
	x_{i}^+(k) = x_i(k) + w_i(k).  
	\label{eq:noisestep}
      \end{displaymath}
    \item Each agent then communicates with its neighbors and update its state to the average value, i.e.,
      \begin{displaymath}
	x_i(k+1) = a_{ii}x_i^+(k)+\sum_{j\in \mathcal N(i)}a_{ij}x_{j}^+(k).
	\label{eq:consensusstep}
      \end{displaymath}
  \end{enumerate}
\end{frame}

\begin{frame}{Privacy Preserving Average Consensus}
  \begin{figure}[ht]
    \begin{center}
      \begin{tikzpicture}
	\node [label=above:$k:$] at (-1.5,0) {};
	\node [label=above:$0$] at (0,0) {};
	\node [label=above:$1$] at (3,0) {};
	\node [label=above:$2$] at (6,0) {};
	\node [label=above:$3$] at (9,0) {};
	\node [] at (-1.5,-2) {$w_i(k):$};
	  \node [] (n0) at (0,-2){$v_i(0)$}; 
	\node [] (n1) at (3,-2){$\begin{array}{c}\textcolor{red}{\varphi v_i(1)}\\+\\- v_i(0)\end{array}$}; 
	  \node [] (n2) at (6,-2){$\begin{array}{c}\textcolor{blue}{\varphi^2 v_i(2)}\\+\\\textcolor{red}{- \varphi v_i(1)}\end{array}$}; 
	    \node [] (n3) at (9,-2){$\begin{array}{c}\textcolor{caltechcolor}{\varphi^3 v_i(3)}\\+\\\textcolor{blue}{ -\varphi^2 v_i(2)}\end{array}$}; 
	      \draw [->,thick] (n0)--(n1);
	      \draw [->,thick] (n1)--(n2);
	      \draw [->,thick] (n2)--(n3);
      \end{tikzpicture}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Privacy Preserving Average Consensus}
  \begin{itemize}
    \item Update equation in matrix form:
      \begin{displaymath}
	x(k+1) =Ax^+(k)= A(x(k) + w(k)).
      \end{displaymath}
    \item Agent $n$ only receives a noisy version of the state:
      \begin{displaymath}
	y(k) = C x^+(k) = C(x(k)+w(k)).	
      \end{displaymath}
 \end{itemize}
\end{frame}

\begin{frame}{Performance Metric}
  Define the error vector as
  \begin{displaymath}
    z(k) \triangleq x(k) - \bar x.
  \end{displaymath}
  Define the mean square convergence rate as
  \begin{displaymath}
    \rho \triangleq \limsup_{k\rightarrow\infty} \left( \sup_{z(0)\neq 0}\frac{\mathbb E_v z(k)'z(k)}{z(0)'z(0)}\right)^{1/k},
  \end{displaymath}
\end{frame}

\begin{frame}{Performance Metric}
  \begin{itemize}
    \item The new information set of agent $n$ at time $k$ is
      \begin{displaymath}
	\mathcal I(k) \triangleq \{x_n(0),y(0),\dots,y(k)\}.
      \end{displaymath}
    \item Denote the maximum likelihood estimate of $x(0)$ given $\mathcal I(k)$ as $\hat x(0|k)$, the covariance of which is defined as $P(k)$.

    \item  Clearly, $\mathcal I(k)\subset \mathcal I(k+1)$, which implies $P(k)\geq P(k+1)$. Hence, we can define

      \begin{displaymath}
	P = \lim_{k\rightarrow\infty} P(k). 
      \end{displaymath}

      $P$ is best estimation performance that can be achieved by agent $n$ (with infinite observations).

    \item If $P_{ii} = 0$, then agent $n$ can infer the initial state $x_i(0)$ of agent $i$ without any error (asymptotically).

  \end{itemize}
  Generally speaking, if $v$ is in the null space of $P$, then agent $n$ can perfectly infer $v'x(0)$.

  Our goal is to make $\rho$ \emph{small}, while making $P$ \emph{large}.
\end{frame}

\section{Main result}

\subsection{Convergence Rate}

\begin{frame}{Convergence Result}
  \begin{theorem}
    For any initial condition $x(0)$, $x(k)$ converges to $\bar x$ in the mean square sense. Furthermore, the mean square convergence rate $\rho$ equals
    \begin{displaymath}
      \rho = \max(|\varphi|^2,|\lambda_2|^2,|\lambda_n|^2).
    \end{displaymath}
  \end{theorem}
\end{frame}

\subsection{Estimation Performance}

\begin{frame}{Reduced System}
  \begin{itemize}
    \item Define $\tilde x(k) = \begin{bmatrix} x_1(k)&\dots&x_{n-1}(k) 
      \end{bmatrix}$. 
    \item $\tx(k)$ follows:
      \begin{displaymath}
	\tx(k+1) = \tA \tx(k) + \zeta x_n(k),
      \end{displaymath}
      where $\tA\in \mathbb R^{(n-1)\times(n-1)}$ is the principal minor of $A$ by removing the last row and column
    \item Similarly, we can define
      \begin{displaymath}
	\ty = \begin{bmatrix}\tx^+_{j_1}(k)&\dots&\tx^+_{j_m}(k)
	\end{bmatrix} = \tC \tx^+(k) .
      \end{displaymath}
    \item We assume that $(\tA,\tC)$ is observable, otherwise we can do a Kalman decomposition and only consider the observable space.
  \end{itemize}
\end{frame}

\begin{frame}{Reduced System}
  \begin{theorem}
    $\tilde A$ is strictly stable, i.e., $\|\tA\|<1$. 
  \end{theorem}
  \begin{itemize}
    \item Consider the following $n-1$ by $n-1$ symmetric matrix
      \begin{displaymath}
	\mathcal S = (I-\tA)^{-1}\tC'\tC(I-\tA)^{-1}
      \end{displaymath}
    \item $I-\tA$ is invertible, $\tC$ is of rank $m$. Hence $\rank(\mathcal S) = m$. 
    \item Let $\psi_1,\dots,\psi_{n-1}\in \mathbb R^{n-1}$ be the eigenvectors of $\mathcal S$. 
    \item Assume that the eigenvalues corresponding to $\{\psi_1,\dots,\psi_m\}$ are non-zero and the eigenvalues corresponding to $\{\psi_{m+1},\dots,\psi_{n-1}\}$ are zero. 
    \item Define
      \begin{align}
	\mathcal Q_1&\triangleq \begin{bmatrix}
	  \psi_1&\dots&\psi_m
	\end{bmatrix}\in \mathbb R^{(n-1)\times m},\\
	\mathcal Q_2& \triangleq \begin{bmatrix}
	  \psi_{m+1}&\dots&\psi_{n-1}
	\end{bmatrix}\in \mathbb R^{(n-1)\times (n-m-1)}.
      \end{align}
  \end{itemize}
\end{frame}

\begin{frame}{Estimation Performance}
  \begin{theorem}
    Suppose that $1>\varphi>\|\tA\|$. $P$ is given by the following equality:
    \begin{displaymath}
      P =\begin{bmatrix}
	\mathcal Q_2\left[\mathcal Q_2'(I-\tA)^{-1}Y(I-\tA)^{-1}\mathcal Q_2\right]^{-1} \mathcal Q_2'&\mathbf 0\\
	\mathbf 0'&0
      \end{bmatrix}
    \end{displaymath}
    where $ Y = \lim_{k\rightarrow\infty} Y(k)$ is the limit of the following recursive Riccati equations:
    \begin{align*}
      Y(0)& = \tA \mathcal U \tA,\\
      Y(k+1) &=\tA \mathcal U\tA  +\varphi^{-2}\tA&\left[ Y^+(k) -  Y^+(k)\left(\varphi^2 I+Y^+(k) \right)^{-1}Y^+(k)\right]\tA,
    \end{align*}
    where
    \begin{displaymath}
      Y^+(k) = \mathcal V Y(k) \mathcal V.
    \end{displaymath}
  \end{theorem}
\end{frame}

\begin{frame}{Estimation Performance}
  \begin{theorem}[Cont.]
    Furthermore, the following inequalities hold:
    \begin{displaymath}
      \left( 1+\frac{\|\tA\|}{\varphi}\right)^{-2} \begin{bmatrix}
	\Delta &\mathbf 0\\
	\mathbf 0'&0
      \end{bmatrix}\leq P \leq \left( 1-\frac{\|\tA\|}{\varphi}\right)^{-2} \begin{bmatrix}
	\Delta &\mathbf 0\\
	\mathbf 0'&0
      \end{bmatrix}
    \end{displaymath}
    where
    \begin{displaymath}
      \Delta \triangleq \mathcal Q_2\left[\mathcal Q_2'(I-\tA)^{-1}\mathcal X(I-\tA)^{-1}\mathcal Q_2\right]^{-1} \mathcal Q_2',
    \end{displaymath}
    and $\mathcal X$ is the unique solution of the following Lyapunov equation
    \begin{displaymath}
      \mathcal X = \tA \mathcal X\tA /\varphi^2+  \tA\tC'\tC\tA . 
    \end{displaymath}
  \end{theorem}
\end{frame}

\begin{frame}{Estimation Performance}
  \begin{itemize}
    \item $\rank(P) = n-m-1$. The null space of $P$ is of rank $m+1$.
    \item Agent $n$ can perfectly recover $m+1$ linear combinations of the initial state.
  \end{itemize}
  \begin{center}
    \begin{tikzpicture}
      \node (n4) at (0,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {4};
      \node (n1) at (0,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {1};
      \node (n3) at (1.5cm,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {3};
      \node (n2) at (1.5cm,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {2};
      \draw [semithick] (n1)--(n2)--(n3)--(n4)--(n1);
    \end{tikzpicture}
  \end{center}
  Assume $a_{ii} = a_{ij} = 1/3$, then agent $4$ can perfectly recover
  \begin{displaymath}
    x_4(0),\,x_1(0) + x_2(0) + x_3(0),\,x_1(0) - x_3(0).
  \end{displaymath}
  However, agent $4$ cannot infer $x_1(0)$, $x_2(0)$, $x_3(0)$ perfectly.
\end{frame}

\begin{frame}{Estimation Performance}
  Define the essential neighborhood of sensor $i$ as 
  \begin{displaymath}
    \mathcal N_e(i) \triangleq \{j\in V:j\neq i,\,a_{ij}\neq 0\}.
  \end{displaymath}
  \begin{theorem}
    The $i$th node's privacy is breached if and only if $i = n$ or ${\mathcal N_e}(i)\bigcup \{i\}\subseteq \mathcal N(n)\bigcup\{n\}$.
  \end{theorem}
  The condition is can be checked \alert{locally}. In other words, your initial state can only be leaked to your neighbors.
\end{frame}
\begin{frame}{Estimation Performance}
  \begin{center}
    \begin{tikzpicture}
      \node (n4) at (-1.5cm,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {4};
      \node (n5) at (0,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {5};
      \node (n1) at (0,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {1};
      \node (n3) at (1.5cm,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {3};
      \node (n2) at (1.5cm,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {2};
      \draw [semithick] (n5)--(n1)--(n2)--(n3)--(n5)--(n4);
    \end{tikzpicture}
  \end{center}
  \begin{displaymath}
    \mathcal N(5)\bigcup \{5\} = \{1,3,4,5\}. 
  \end{displaymath}
  \begin{displaymath}
    \mathcal N(4)\bigcup \{4\} = \{4,5\}. \text{ Agent 5 can perfectly infer }x_4(0).
  \end{displaymath}
  \begin{displaymath}
    \mathcal N(1)\bigcup \{1\} = \{1,2,5\}.\text{ Agent 5 cannot perfectly infer }x_1(0)\text{ if }a_{12}\neq 0. 
  \end{displaymath}
\end{frame}
\subsection{Fundamental Limitation}
\begin{frame}{Fundamental Limitation}
  \begin{itemize}
    \item If $P_{ii}=0$ using our average consensus algorithm, it is possible to design another noise sequence $\{w_i(k)\}$ to make $P_{ii}\neq 0$?

    \item Consider a more general noise model:
      \begin{enumerate}
	\item $\mathbb Ew_i(k) = 0$.
	\item $\mathbb Ew_i(k_1)w_j(k_2) = 0$, when $i\neq j$.
      \end{enumerate}
    \item The second condition implies that the sensors are not collaborating.
  \end{itemize}
\end{frame}

\begin{frame}{Fundamental Limitation}
  \begin{theorem}
    $x(k)$ converges to $\bar x$ in the mean squared sense, i.e.,
    \begin{displaymath}
      \lim_{k\rightarrow\infty}\mathbb E  \,\|x(k)-\bar x\|^2 = 0,
    \end{displaymath}
    implies that
    \begin{displaymath}
      \lim_{k\rightarrow\infty}\mathbb E \,\left(\sum_{t=0}^k w_i(t)\right)^2  = 0, \,\forall i=1,\dots,n.
   \end{displaymath}
  \end{theorem}
  Since the sensors are not collaborating, average consensus is achieved if and only if the noise from each sensor sums to 0 asymptotically (in the mean squared sense).
\end{frame}

\begin{frame}{Fundamental Limitation}
  \begin{theorem}
    Suppose that
    \begin{align*}
      \lim_{k\rightarrow\infty}\mathbb E \,\left(\sum_{t=0}^k w_i(t)\right)^2  = 0, \,\forall i=1,\dots,n.
    \end{align*}
    Then $P_{ii} = 0$ \alert{if} $i = n$ or ${\mathcal N_e}(i)\bigcup \{i\}\subseteq \mathcal N(n)\bigcup\{n\}$.
  \end{theorem}

  Comparing it to the previous theorem, we know that our proposed strategy achieves ``minimum'' privacy breach.
\end{frame}

%%\subsection{Differential Privacy}
%%\begin{frame}{Differential Privacy}
%%  \begin{itemize}
%%    \item Denote the probability space generated by $\{v_i(k)\}$ as $(\Omega,\mathcal F,\mathbb P)$.
%%    \item Define $D = \mathbb R^n$ as the set of all possible initial condition $x(0)$.
%%    \item  Define a binary ``adjacency'' relation $\text{Adj}$ on $D$:
%%      \begin{displaymath}
%%	\text{Adj}(x^a(0),x^b(0)) \triangleq \begin{cases}
%%	  1& \text{, if }\|x^a(0)-x^b(0)\|\leq d\\
%%	  0&\text{, otherwise}
%%	\end{cases}.
%%      \end{displaymath}
%%    \item Let $\hat x_i(0|k)$ be the maximum likelihood estimate of the initial condition $x_i(0)$ of agent $i$ by agent $n$, given the information set $\mathcal I(k)$.
%%      \begin{align*}
%%	\hat x_i(0|k) = x_i(0) + \varsigma_i(k),	
%%      \end{align*}
%%      where $\varsigma_i(k)$ is a zero mean normal R.V. with variance $P_{ii}(k)\geq P_{ii}$.
%%    \item Clearly $\hat x_i(0|k)$ is a mapping from $D\times \Omega$ to $\mathbb R$, which can be written as
%%      \begin{displaymath}
%%	\hat x_i(0|k) = M_{i,k}(x(0),\,\omega),\,\text{where }x(0)\in D,\,\omega\in \Omega.
%%      \end{displaymath}
%%  \end{itemize}
%%\end{frame}
%%
%%\begin{frame}{Differential Privacy}
%%  \begin{definition}
%%    The mapping $M_{i,k}$ is called $(\varepsilon,\delta)$-differentially private for $\text{Adj}$ if for all Borel-measureable $S\subseteq\mathbb R$ and adjacent initial conditions $x^a(0),x^b(0)$, the following inequality holds:
%%    \begin{displaymath}
%%      \mathbb P(M_{i,k}(x^a(0),\omega)\in S) \leq e^\varepsilon \mathbb P(M_{i,k}(x^b(0),\omega)\in S) +\delta.
%%    \end{displaymath}
%%  \end{definition}
%%\end{frame}
%%
%%\begin{frame}{Relation with Differential Privacy}
%%  \begin{theorem}
%%    If $\varepsilon>0,0.5>\delta>0$ and
%%    \begin{displaymath}
%%      \frac{d}{2\varepsilon}(K + \sqrt{K^2+2\varepsilon})\leq P_{ii}, 
%%      \label{eq:epsilondelta}
%    \end{displaymath}
%    where $K =  Q^{-1}(\delta)$ and 
%    \begin{displaymath}
%      Q(x) \triangleq \frac{1}{\sqrt{2\pi}}\int_x^\infty \exp\left(-\frac{u^2}{2}\right)du,
%    \end{displaymath}
%    then for any $k\geq 0$, the mapping $M_{i,k}$ is $(\varepsilon,\delta)$-differentially private for $\text{Adj}$. 
%    \label{theorem:diffprivate}
%  \end{theorem}
%\end{frame}
\section{Numerical Example}
\begin{frame}{Numerical Example}
  \begin{center}

    \begin{tikzpicture}
      \node (n4) at (0,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {4};
      \node (n1) at (0,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {1};
      \node (n3) at (1.5cm,0) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {3};
      \node (n2) at (1.5cm,1.5cm) [circle,draw=caltechcolor!50,fill=caltechcolor!20,thick] {2};
      \draw [semithick] (n1)--(n2)--(n3)--(n4)--(n1);
    \end{tikzpicture}
  \end{center}
  \begin{itemize}
    \item $a_{ii} = a_{ij} = 1/3$, for $j \in \mathcal N(i)$.
    \item $\|\tA\| = 0.805$. 
    \item We choose $\varphi = 0.9$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Convergence Result}
  \begin{figure}[ht]
    \begin{center}
      \setlength{\figureheight}{5cm}
      \setlength{\figurewidth}{6cm}
      \inputtikz{convergence}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Numerical Example:Estimation Performance}
  \begin{figure}[ht]
    \begin{center}
      \setlength{\figureheight}{5cm}
      \setlength{\figurewidth}{6cm}
      \inputtikz{variance}
    \end{center}
    \label{fig:variance}
  \end{figure}
\end{frame}

%\section{Conclusion}
%\begin{frame}{Conclusion}
%  \begin{itemize}
%    \item We propose a privacy preserving average consensus algorithm.
%    \item We compute the exact mean square convergence rate of the proposed algorithm.
%    \item We derive the asymptotic estimation performance.
%    \item We provide a topological necessary and sufficient condition, under which the privacy of an agent is breached. 
%    \item We prove that our algorithm achieves minimum privacy breach, under the non-collaborating assumption.
%    \item We prove that our consensus algorithm is differentially private given that $P_{ii}\neq 0$. 
%  \end{itemize}

%\end{frame}
\end{document}
