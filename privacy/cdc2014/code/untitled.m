clc
clear

A = [1 1 0 1;1 1 1 0;0 1 1 1;1 0 1 1]/3;
tA = A(1:3,1:3);
phi = 0.9;
C = [1 0 0 0;0 0 1 0;0 0 0 1];
tC = C(1:2,1:3);
Q2 = null(tC*inv(eye(3)-tA));
Q1 = null(Q2');

S = eye(3);
for T = 1:10
    S = inv(tA)*inv([tC' eye(3)] * inv([tC*tC' tC;tC' eye(3) + S*phi^2]) * [tC;eye(3)])*inv(tA');
end

Delta = Q2 * inv(Q2'*inv(eye(3)-tA)*inv(S)*inv(eye(3)-tA)*Q2)*Q2';
