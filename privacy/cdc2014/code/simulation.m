clc
clear
A = [1 1 0 1;1 1 1 0;0 1 1 1;1 0 1 1]/3;
tA = A(1:3,1:3);
phi = 0.9;
C = [1 0 0 0;0 0 1 0;0 0 0 1];
tC = C(1:2,1:3);
Q2 = null(tC*inv(eye(3)-tA));
Q1 = null(Q2');
X = dlyap(tA/phi,tA*tC'*tC*tA);
Delta = Q2 * inv(Q2'*inv(eye(3)-tA)*X*inv(eye(3)-tA)*Q2)*Q2';
lower = Delta/(1+norm(A,2)/phi)^2;
upper = Delta/(1-norm(A,2)/phi)^2;
%convergence
% T = 51;
% 
% x = zeros(4,T);
% v = randn(4,T);
% w = zeros(4,T);
% 
% x(:,1) = randn(4,1);
% avex = mean(x(:,1));
% w(:,1) = v(:,1);
% for k = 1:T-1
%     x(:,k+1) = A * x(:,k) + w(:,k);
%     w(:,k+1) = phi^(k) * v(:,k+1) - phi^(k-1)*v(:,k);
% end
% for i = 1:4
%     plot(0:(T-1),x(i,:));
%     hold on
% end
% plot([0,T-1],[avex,avex]);
Pii = zeros(3,30);
for T = 1:30
    F = zeros((T+1)*3);
    for i = 0:(T-1)
        F((1+i*3):(3+i*3),(1+i*3):(3+i*3)) = (tA/phi)^2+eye(3);
        F((4+i*3):(6+i*3),(1+i*3):(3+i*3)) = -(tA/phi);
        F((1+i*3):(3+i*3),(4+i*3):(6+i*3)) = -(tA/phi);
    end
    F((3*T+1):(3*T+3),(3*T+1):(3*T+3))=eye(3);
    
    tmp = zeros((T+1)*2,(T+1)*3);
    for i=0:T
        tmp((1+2*i):(2+2*i),(1+3*i):(3+3*i))=tC;
    end
    F = inv(tmp*inv(F)*tmp');
    
    H = zeros(2*(T+1),3);
    for i = 0:T
        H((1+2*i):(2+2*i),:) = tC*inv(eye(3)-tA)/phi^i-tC*tA*(tA/phi)^i*inv(eye(3)-tA);
    end
    Pii(:,T) = diag(inv(H'*F*H));
    XX = inv(H'*F*H);
end
for i = 1:3
    plot(1:T,Pii(i,:));
    hold on
end
plot([1,T],[lower(1,1) lower(1,1)]);
plot([1,T],[lower(2,2) lower(2,2)]);



