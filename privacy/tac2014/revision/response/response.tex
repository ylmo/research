\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\let\emph\textbf

\title{Response Letter}

\author{Yilin Mo, Richard M. Murray}

\begin{document} \maketitle
We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their comments. The detailed responses are listed as below:

\section{Response to Reviewer 2}
\label{sec:review2}

The authors consider an averaging algorithm between multiple agents to reach consensus. The problem here is to determine if agent n say can estimate the state of some other agent exactly as the number of rounds increases to infinity, or if sufficiently large uncertainty on the other agents' initial condition will persist, which is desirable to preserve the privacy of the other agents. A scheme is proposed where a noise perturbation is introduced in the communicated state at each round in such a way that the agents still converge asymptotically to the true mean.

The problem considered here is similar to that in [15]. Overall the analysis and discussion seem rigorous, except for Section V. Indeed in that section the authors attempt to link their work with the notion of differential privacy, but their setup is not correct. Essentially, the authors do not seem to have understood the notion of differential privacy (DP), which has nothing to do with a maximum likelihood estimator (note that the ML estimator assumes something about the information set denoted $\mathcal I(k)$ here, which DP does not do). For DP, in (44) one would have to replace the ML estimator by the output of the algorithm, which is the average. Now it is essentially impossible to release the average of the initial states exactly in a way that is compatible with DP. Indeed, in the DP setup agent $n$ could have access to any information, say the initial states of all agents except the first, and be trying to recover the first agent state (which he can do immediately if he has access to the exact average). As a result, the comparison with [16] is not fair since a different guarantee is provided there.

\textcolor{blue}{We thank the reviewer for the insightful comments. We agree with the reviewer. The section on differential privacy has been removed.}

Besides this main issue, I have a few additional remarks:
\begin{itemize}
 \item the explanations are often quite succinct, the arguments in the proofs could be made a bit more explicit for the reader, without adding much length. I found myself having to extrapolate very often about the exact linear algebra result or specific property of some given matrix used to allow the arguments to go through.

\textcolor{blue}{We have added some comments to provide more insights on the results.}

\item a scheme is presented (display (4)) but not much is said to motivate this particular choice of noise process. Similarly additional motivating discussion could be given in and between the proofs to guide the reader. Overall the paper feels a bit like a technical note with long proofs. 

  \textcolor{blue}{We have modified remark 1 to motivate our noise model. A few comments have been added between Corollary 1 and Theorem 4 to motivate the concept of disclosed vector and disclosed set.}

\item the English can be improved in the abstract and the introduction.
  
\textcolor{blue}{We have proofread the paper to improve the language.}
\end{itemize}

\section{Response to Reviewer 3}
In this paper, the authors propose a privacy protecting algorithm that prevents agents from learning other agents' initial conditions while still achieving consensus. The algorithm specifically suggests adding an additional noise (randomly generated at each agent location) to each agent's state status. The main contributions are:
\begin{enumerate}
\item the authors show that the agents do reach consensus under the algorithm and also give an explicit expression for the convergence rate;
\item the authors characterize upper and lower bounds for the error covariance matrix given a maximum likelihood estimator;
\item they also give certain topological conditions under which the local agent cannot estimate the initial condition perfectly;
\item lastly, they make some connection to the concept of differential privacy.
\end{enumerate}

This paper potentially contains some interesting ideas regarding privacy preserving algorithms for a consensus model, but there are also several significant issues that need to be addressed:
\begin{enumerate}
\item The difference from the Hadjicostis work is that here the authors provide upper and lower bounds for the error covariance matrix. However, the bounds are given in an inexplicit form where one needs to solve some recursive Riccati equation and Lyapunov equation.  Other than this, it is hard to understand what advantages the proposed algorithm could bring compared to Hadjicostis'. The authors need to more carefully justify and motivate their approach.
  
\textcolor{blue}{We thank the reviewer for the insightful comment. First we want to clarify that in our paper we provide the \emph{exact} error covariance matrix as well as the upper and lower bound.}

\textcolor{blue}{The main merits of our work over the work by Manitara and Hadjicostis [17] are:}
\begin{enumerate}
\item \textcolor{blue}{We characterize exactly how much information is leaked to the ``malicious'' agent by characterizing the asymptotic error covariance matrix. In [17], only a qualitative result is given. }
\item \textcolor{blue}{The main theorem (Thm 2) in [17] only provides a sufficient condition for the privacy of a node. In the example section in [17], the authors provide an example where even if the condition in Thm 2 is violated, the privacy of the node is still preserved, which shows that the condition introduced in Thm 2 is not a necessary condition in general. In our paper, we are able to provide a fundamental limit and prove that our algorithm achieves the fundamental limit.}
  \item \textcolor{blue}{The proof of the main theorem 2 in [17] only works for finite time case. (See Equation 9). It is entirely possible that a malicious agent can collect more and more information and be able to infer the state of a benign agent perfectly as time goes to $\infty$. In finite time, this implies that the malicious agent can have a very accurate (although not perfect) estimate of the state of a benign agent. Notice that since no quantitative result is provided in [17], there is no way to rule out this possibility. Therefore, the applicability of [17] in practice is questionable.}
\end{enumerate}

\textcolor{blue}{Finally, we believe that even if our characterization of the error covariance matrix is implicit, as one needs to solve either Riccati or Lyapunov equations, our results are still useful, since many results in control theory depend on the fixed point of Riccati or Lyapunov equations and there exist efficient tools (such as matlab functions dare() and dlyap()) and non-recursive ways to solve them. Otherwise one can argue that the classical infinite horizon LQG control or Lyapunov stability is not useful, since they both involves solving Riccati or Lyapunov equations.}

\item Some of the theorem proofs are not very clear. For example, the proof for Theorem 1 does not explicitly verify that $x(k)$ converges to $\bar{x}$ in a mean square sense. Similarly, the statement of Theorem 6 is not very clear.

  \textcolor{blue}{In the proof of Theorem~1, we provide an explicit formula for the mean squared convergence rate. Since that rate is less that 1, mean squared convergence is guaranteed. We have added the following sentence to clarify this issue: }

  \textcolor{blue}{Since the RHS of (16) is strictly less than 1, we only need to prove (16), since it implies the mean square convergence.}

  \textcolor{blue}{The statement of Theorem~6 has been modified to indicate that the RHS is a subspace of $\mathbb R^n$.}

\item The concept of the disclosed set is not well motivated, and hence it is hard to understand the importance of it.

  \textcolor{blue}{We add the motivation of the disclosed vector and disclosed space/set between corollary 1 and theorem 4. In a nutshell, the concept of disclosed vector is introduced to characterize the privacy breach of the consensus algorithm. For example, if $e_i$ is a disclosed vector, then agent $n$ can perfectly infer $e_i'x(0)=x_i(0)$, which implies that the privacy of agent $i$ is breached. Therefore, it is important to characterize the disclosed space/set to know exactly what can be inferred by agent $n$, and to ensure that $e_i$ does not belong to the disclosed set to protect the privacy of the agent $i$. Notice that for our algorithm, since we require the consensus on the exact average, some information (such as the average of the initial state) will inevitably be leaked to agent $n$.}

\item The authors don't make it clear why it is worthwhile to connect their results to the differential privacy concept.

  \textcolor{blue}{We agree with the reviewer, the differential privacy part has been removed from the revised manuscript.}
\end{enumerate}

\section{Response to Reviewer 5}
A well written paper with significant results on distributed privacy-preserving average consensus problem.  A proposed protocol that enables the components of a distributed system, each with some initial value to reach on the exact average consensus on their initial values, without having to reveal the initial state of each node that participates in the computation of the average value of the network.  

What it is not clear, is the appearance of any ``curious'' nodes that are trying to identify the initial values of the nodes following the proposed protocol or any ``malicious'' nodes that aiming to interfere to the computation of the exact average value of the network with incorrect values.

\textcolor{blue}{We thank the reviewer for this insightful observation. It is worth noticing that the entire section IV.B is built upon the concept of reduced system, which represents the system after removing the influence of the agent $n$. Therefore, even if agent $n$ is ``malicious'' and does not follow the update rules for the consensus, it will still have the same estimation performance $P$. In other words, a ``malicious'' agent essentially get the same amount of information on the initial state as a ``curious'' agent. This can also be seen as a special case of the separation principle, where the estimation of the initial state is independent of the malicious actions (control) from agent $n$. We have added a remark in Section IV.B to clarify this issue.}

\textcolor{blue}{On the other hand, if there exists a malicious agent who does not follow the update equation, then it is possible that the agents cannot reach consensus on the exact average or reach consensus at all using our algorithm. However, in this paper, our main concern is the privacy of the initial state. The problem of ensuring consensus in the presence of malicious agents has been addressed in the following paper: }

\textcolor{blue}{F. Pasqualetti and A. Bicchi and F. Bullo, Consensus Computation in Unreliable Networks: A System Theoretic Approach. IEEE Transactions on Automatic Control, 57(1):90-104, 2012.}

It will be good to include in the paper a pseudo code of the proposed strategy in order to be much easier to read and understand the proposed work of the paper. 

\textcolor{blue}{We have modified the description of our privacy preserving algorithm in algorithm 1 to make it more clear.}

Also a numerical example it will be good to be used and compared with a similar or related work of this topic in order to show the advantages and disadvantages (if any) of the proposed protocol against any other previous work on the topic.

\textcolor{blue}{We have added a comparison between our approach and the one proposed by Huang et al. in the simulation section. In a nutshell, Huang's algorithm can provide more privacy than ours. However, our algorithm ensures the consensus on the exact average, while it is not guaranteed in Huang's algorithm. }

\section{Response to Reviewer 10}
In this work, the authors studied the differential privacy in average consensus and proposed an algorithm that achieves accurate average consensus in the mean square sense which preserving differential privacy. In general, the results presented here are interesting and the paper is well-organized. However, it needs further revision due to the following reasons.

First, there are several typos and errors in the proofs of the theorems. Specifically, the authors are encouraged to check the proof of theorem 3 because the equation right above (52) seems incorrect. In addition, there are typos in (73) and (77).

\textcolor{blue}{We thank the reviewer for the insightful comments. We have corrected the corresponding equations, which is now (50), (72) and (76).}

Furthermore, Section VII seems a little bit too short though the example presented there is relatively simple. The authors should also solve the example analytically using the theorems and compare the results to the simulation.

\textcolor{blue}{The asymptotic $P_{11}$ and $P_{22}$ are solved analytically using Theorem 3 (which correspond to the dashed lines in Fig.3). We have changed the text to make this fact clear. We further added a leaf node to illustrate the topology that can leads to privacy breaches and added a comparison with the algorithm proposed by Huang et al.}

\end{document}
%%% Local Variables:
%%% TeX-command-default: "Latexmk"
%%% End: