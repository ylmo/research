The authors consider an averaging algorithm between multiple agents to
reach consensus. The problem here is to determine if agent n say can
estimate the state of some other agent exactly as the number of rounds
increases to infinity, or if sufficiently large uncertainty on the
other agents' initial condition will persist, which is desirable to
preserve the privacy of the other agents. A scheme is proposed where a
noise perturbation is introduced in the communicated state at each
round in such a way that the agents still converge asymptotically to
the true mean.

The problem considered here is similar to that in [15]. Overall the
analysis and discussion seem rigorous, except for Section V. Indeed in
that section the authors attempt to link their work with the notion of
differential privacy, but their setup is not correct. Essentially, the
authors do not seem to have understood the notion of differential
privacy (DP), which has nothing to do with a maximum likelihood
estimator (note that the ML estimator assumes something about the
information set denoted I(k) here, which DP does not do). For DP, in
(44) one would have to replace the ML estimator by the output of the
algorithm, which is the average. Now it is essentially impossible to
release the average of the initial states exactly in a way that is
compatible with DP. Indeed, in the DP setup agent n could have access
to any information, say the initial states of all agents except the
first, and be trying to recover the first agent state (which he can do
immediately if he has access to the exact average). As a result, the
comparison with [16] is not fair since a different guarantee is
provided there.

Besides this main issue, I have a few additional remarks:
- the explanations are often quite succinct, the arguments in the
proofs could be made a bit more explicit for the reader, without adding
much length. I found myself having to extrapolate very often about the
exact linear algebra result or specific property of some given matrix
used to allow the arguments to go through.
- a scheme is presented (display (4)) but not much is said to motivate
this particular choice of noise process. Similarly additional
motivating discussion could be given in and between the proofs to guide
the reader. Overall the paper feels a bit like a technical note with
long proofs. 
- the English can be improved in the abstract and the introduction.
