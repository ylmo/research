\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\newcommand{\comment}[1]{\textcolor{blue}{#1}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\let\emph\textbf

\title{Response Letter}

\author{Yilin Mo, Richard M. Murray}

\begin{document} \maketitle
We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their comments. The major changes of the paper are highlighted in blue in the revised manuscript. The detailed responses are listed as below:

\section{Response to Reviewer 1}

The paper contains interesting and novel results on distributed privacy-preserving average consensus. The authors propose an average consensus algorithm whose main objective is to preserve the privacy of the initial state of the nodes. The idea is to add noisy values in the network, which eventually get subtracted, so that the network reaches average consensus without exposing the initial values of the nodes. The authors also consider the ability of malicious nodes to estimate the initial values of the nodes, and provide upper and lower bounds for the error covariance matrix.

After the removal of the discussiosn on differential privacy, the algorithm and most of the results emanate from a paper by the same authors that has appeared in the Proceedings of CDC 2014.

A few comments on the write up:
\begin{enumerate}
\item The proposed algorithm that is presented relies on the approach of the work of Manitara and Hadjicostis presented in ECC 2013 (they also add noise in the network and then subtract it, in order to reach the exact average consensus asymptotically).

\textcolor{blue}{We thank the reviewer for the insightful comments. The main merits of our work over the work by Manitara and Hadjicostis [17] are:}
\begin{enumerate}
\item \textcolor{blue}{We characterize \emph{exactly} how much information is leaked to the ``malicious'' agent by characterizing the asymptotic error covariance matrix. In [17], only a qualitative result is given. }
\item \textcolor{blue}{The main theorem (Thm 2) in [17] only provides a sufficient condition for the privacy of a node. In the example section in [17], the authors provide an example where even if the condition in Thm 2 is violated, the privacy of the node is still preserved, which shows that the condition introduced in Thm 2 is not a necessary condition in general. In our paper, we are able to provide a fundamental limit and prove that our algorithm achieves the fundamental limit.}
  \item \textcolor{blue}{The proof of the main theorem 2 in [17] only works for finite time case. (See Equation 9). It is entirely possible that a malicious agent can collect more and more information and be able to infer the state of a benign agent perfectly as time goes to $\infty$. In finite time, this implies that the malicious agent can have a very accurate (although not perfect) estimate of the state of a benign agent. Notice that since no quantitative result is provided in [17], there is no way to rule out this possibility. Therefore, the applicability of [17] in practice is questionable.}
\end{enumerate}

  \item One limitation in the paper is that the value of $\varphi$ is the same for all the nodes in the network. That implies that all the nodes, including the malicious ones, should be aware of this value at initialization. Later on, in Theorem 3, $\varphi$ is also required to lie in the interval $[||\tilde{A}||, 1]$, which also requires global knowledge (the largest singular value of matrix $\tilde{A}$). 

Could the authors comment a bit on this issue? In any case, it seems that the authors should be a bit more forthcoming about this limitation and clarify early that the choice of $\varphi$ needs to be made in this particular way.

Another issue is that usage of $\tilde{A}$ implies knowledge of the malicious node (node $n$ is the malicious node). If such knowledge is not available, should the discussion in the paper consider all possible nodes as being malicious: e.g., $\tilde{A}_1$, $\tilde{A}_2$, …, $\tilde{A}_n$? Also, what happens in the case of multiple (collaborating) malicious nodes?

\comment{We thank the reviewer for pointing this out. We have modified the manuscript to strengthen our results. In the revised manuscript, we prove that the main results on the privacy of our proposed algorithm (Theorem 3) holds for any $\varphi \in (0,1)$ (not only for $\varphi \in (\|\tilde A\|,1))$. As a result, there is no need for the nodes to know $\tilde A$.  We have added a comment after corollary 1 to address this issue. }

    \item What is not very clear in the paper is how the error covariance bounds depend on the topology of the network. For instance, how does the covariance matrix $P$ in Eq. (14) behaves depending on the topology of the network and the position of the malicious node(s)? Are there fundamental limitations on $P$ depending on the topology of the network? This question relates to the upper and lower bounds on the error covariance matrix (obtained in the main section of the paper), but that discussion assumes that the position of the malicious node is known and provides very little intuition about what the fundamental limitations are. Perhaps the authors can provide some commentary on this issue. At least, they should clarify that the analysis is provided assuming (1) knowledge of the position of the malicious node and (2) knowledge of $|| \tilde{A}||$.
      

Note that this is also evident in the example presented in Section VI. The fact that node 4 has as a sole neighbor the malicious node 5, creates problems for node 4. 

\comment{In the revised manuscript, the lower and upper bound on $P$ matrix are removed since we have relaxed the constraints on $\varphi$. In Theorem 4, we provide a qualitative result to relate the topology of the network with the zeros on the diagonal entries of $P$ matrix. Notice that this result does not depend on $\varphi$ in the revised manuscript now. We also provide comments (after Corollary 1) on this issue. In fact, we can assume that every single node is ``malicious'', but they are not cooperative. In that case, if node $i$ wants to keep its initial condition private from all other nodes, then it only needs to check (38) against all its neighbors, which can be done locally. On the other hand, if we know that there are at most $k$ nodes trying to estimate the initial $x(0)$ cooperatively, then node $i$ only needs to insure (39), which can also be done locally. }

\item It might be helpful to the reader if the authors provide more details in the literature review in the introduction. In particular, it might be good to elaborate on the main differences of their work against [15, 17].

  \comment{We have changed the introduction according to the reviewer's suggestions. The main different between our work and [15] is that in [15], the authors consider a double-integrator-network, which has different dynamics than ours. Furthermore, in [15], the system is either noise-free or subject to white Gaussian noise. In our paper, we design a specific correlated noise process to ensure privacy and average consensus are both achieved. }
\end{enumerate}

There are some typos in the example. 
\begin{enumerate}
\item In fig. 4 the black dash line shouldn’t  be P44(k)?
  \item In the last paragraph of the example section, shouldn't be agent 5 and not agent 4 (estimatator)?
\end{enumerate}

  \comment{Fixed.}
 
\section{Response to Reviewer 2}
 
The authors have addressed my comment. Still, there are quite a few typos left, especially in the first few pages.


Also, for the final version, I would like to see toward the beginning a clear definition of what the authors mean here by "Privacy", and a clear problem statement. Indeed, we can observe that Section III "Problem Formulation" does not actually formulate a problem.

\comment{We thank the reviewer to point this out. We have moved the definition of the disclosed space and the relevant discussions to the problem formulation section. We also add a formal definition for the privacy of a node $i$. We have proofread the paper to improve the quality of the language.}
\section{Response to Reviewer 3}
 
The authors have done a good job of addressing my previous concerns.  I think the paper is ready for publication in IEEE-TAC. 
 
\comment{We sincerely thank the reviewer for helping improving the quality of the paper.}
\section{Response to Reviewer 4}
In the revision, the authors address the problems pointed out in the review. The response to the review is reasonable. 

\comment{We sincerely thank the reviewer for helping improving the quality of the paper.}

\end{document}
%%% Local Variables:
%%% TeX-command-default: "Latexmk"
%%% End: