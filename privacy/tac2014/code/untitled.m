clc
clear

n = 5;
m = 3;

C = [1 0 0 0 0;0 0 1 0 0;0 0 0 1 0;0 0 0 0 1];
A = [2 1 0 0 1
    1 2 1 0 0
    0 1 2 0 1
    0 0 0 3 1
    1 0 1 1 1]/4;

tA = A(1:n-1,1:n-1);
phi = 0.4;
tC = C(1:m,1:n-1);
Q2 = null(tC*inv(eye(n-1)-tA));
Q1 = null(Q2');
U = tC'*tC;
V = eye(n-1)-U;

% convergence
%init
x = zeros(n,51);
x(:,1) = randn(n,1);
barx = mean(x(:,1));
vpre = zeros(n,1);
for k = 1:50
    v = randn(5,1);
    %our noise model
    w = phi^(k-1)*v - phi^(k-2)*vpre;
    %Huang's noise model 
    xplus = x(:,k) + w;
    x(:,k+1) = A*xplus;
    vpre = v;
end

for i = 1:n
    plot(0:50,x(i,:))
    hold on
end

plot([0,50],[barx,barx]);

%asymptotoci estimation error covariance
Y = tA*U*tA;

for i = 1:10
    Y = V*Y*V;
    Y = tA*U*tA + tA*(Y-Y*inv(phi^2*eye(n-1)+Y)*Y)*tA/phi^2;
end

tP = Q2 * inv(Q2'*inv(eye(n-1)-tA)*Y*inv(eye(n-1)-tA)*Q2)*Q2';

%finite time est error

tH{1} = tC*tA;
F{1} = tC;
H{1} = tC;
for k = 2:20
    F{k} = [tC zeros(m,(n-1)*(k-1));tH{k-1}/phi F{k-1}];
    tH{k} = [tC*tA;tH{k-1}*tA/phi];
    tmp = (tC/phi^(k-1)-tC*tA*(tA/phi)^(k-1))*inv(eye(n-1)-tA)
    H{k} = [H{k-1}; tmp];
    P{k} = inv(H{k}'*inv(F{k}*F{k}')*H{k});
end
