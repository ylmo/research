\documentclass[14pt, paperwidth=24in, paperheight=36in, portrait, margin=0mm, innermargin=15mm, blockverticalspace=15mm, colspace=15mm, subcolspace=8mm]{tikzposter}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{pgfplots}

\pgfdeclarelayer{backgroundlayer}
\pgfsetlayers{backgroundlayer,main,notelayer}

\newlength\figureheight
\newlength\figurewidth

\newcommand{\tI}{\tilde {\mathcal I}}
\newcommand{\tA}{\tilde A}
\newcommand{\ty}{\tilde y}
\newcommand{\tx}{\tilde x}
\newcommand{\tw}{\tilde w}
\newcommand{\tv}{\tilde v}
\newcommand{\tC}{\tilde C}
\newcommand{\tP}{\tilde P}


\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\definecolor{caltechprimarycolor}{RGB}{255,130,30}
\definecolor{caltechsecondarycolor}{RGB}{170,169,159}
\definecolor{caltechthirdcolor}{RGB}{118,119,123}

\definecolorstyle{caltechcolorstyle} {
\definecolor{colorOne}{named}{caltechprimarycolor}
\definecolor{colorTwo}{named}{caltechsecondarycolor}
\definecolor{colorThree}{named}{caltechthirdcolor}
}{
% Background Colors
\colorlet{backgroundcolor}{colorThree}
\colorlet{framecolor}{black}
% Title Colors
\colorlet{titlefgcolor}{black}
\colorlet{titlebgcolor}{colorOne}
% Block Colors
\colorlet{blocktitlebgcolor}{colorOne}
\colorlet{blocktitlefgcolor}{black}
\colorlet{blockbodybgcolor}{white}
\colorlet{blockbodyfgcolor}{black}
% Innerblock Colors
\colorlet{innerblocktitlebgcolor}{colorOne}
\colorlet{innerblocktitlefgcolor}{black}
\colorlet{innerblockbodybgcolor}{colorTwo!30!white}
\colorlet{innerblockbodyfgcolor}{black}
% Note colors
\colorlet{notefgcolor}{black}
\colorlet{notebgcolor}{colorTwo!50!white}
\colorlet{noteframecolor}{colorTwo}
}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{Privacy Preserving Average Consensus}
\author{Yilin Mo and Richard M. Murray}
\institute{California Institute of Technology} % See Section 4.1

%\usetheme{Desert} % See Section 5
\usecolorstyle{caltechcolorstyle}
\useinnerblockstyle{Envelop}
\usetitlestyle{Default}
\useblockstyle{Basic}
\usebackgroundstyle{Default}

\begin{document}
\maketitle % See Section 4.1
\begin{columns} % See Section 4.4
  \column{0.5} % See Section 4.4
  \block{Average Consensus}{
 \begin{itemize}
    \item We model the network composed of $n$ agents as an \emph{undirected} and \emph{connected} graph $G = \{V,\,E\}$.
    \item Define the neighborhood of sensor $i$ as $\mathcal N(i)$.
    \item Each agent has an initial scalar state $x_{i}(0)$.
    \item Update equation:
      \begin{displaymath}
	x(k+1) = A x(k),
      \end{displaymath}
      where we assume that $A$ is \emph{symmetric}.
    \item The following conditions are necessary and sufficient for all the agents to reach average consensus:
      \begin{enumerate}
	\item[(A1)] $ \lambda_1 = 1$ and  $|\lambda_i| < 1$ for all $i = 2,\ldots, n$.
	\item[(A2)] $A\mathbf 1 = \mathbf 1$, i.e., $\mathbf 1$ is an eigenvector of $A$.
      \end{enumerate}
  \end{itemize}
  \innerblock{Privacy Breachs}{
\begin{center}
      \begin{tikzpicture}
	\node (n4) at (0,0) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {4};
	\node (n1) at (0,3cm) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {1};
	\node (n3) at (3cm,0) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {3};
	\node (n2) at (3cm,3cm) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {2};
	\draw [semithick] (n1)--(n2)--(n3)--(n4)--(n1);
      \end{tikzpicture}
    \end{center}
Agent $4$ can infer $x_1(0)$ and $x_3(0)$ in one step and $x_2(0)$ in two steps.
  }
  }

  \block{Privacy Preserving Average Consensus}{
  \innerblock{PPAC algorithm}{
  \begin{itemize}
    \item At time $k$, each agent generates a standard normal distributed random variable $v_i(k)$ with mean $0$ and variance 1. All the random variables $\{v_i(k)\}_{i=1,\dots,n,\,k=0,1,\dots}$ are jointly independent.
    \item Each agent then adds a random noise $w_i(k)$ to its state $x_i(k)$, where
      \begin{displaymath}
	w_i(k) = \begin{cases}
	  v_i(0)&\text{, if }k = 0\\
	  \varphi^kv_i(k)-\varphi^{k-1}v_i(k-1)&\text{, otherwise}
	\end{cases},
	\label{eq:addednoise}
      \end{displaymath}
      where $0<|\varphi|<1$ is a constant for all agents. Define the new state to be $x_{i}^+(k)$, i.e.,
      \begin{displaymath}
	x_{i}^+(k) = x_i(k) + w_i(k).  
	\label{eq:noisestep}
      \end{displaymath}
    \item Each agent then communicates with its neighbors and update its state to the average value, i.e.,
      \begin{displaymath}
	x_i(k+1) = a_{ii}x_i^+(k)+\sum_{j\in \mathcal N(i)}a_{ij}x_{j}^+(k).
	\label{eq:consensusstep}
      \end{displaymath}
  \end{itemize}
  }
  Each agent only receives a noisy version of the state from its neighbors! 
  }

  \block{Convergence Speed}{

  \begin{itemize}
    \item Define the average vector and error vector as
      \begin{displaymath}
	\bar x\triangleq \frac{\mathbf 1' x(0)}{n} \mathbf 1, \,z(k) \triangleq x(k) - \bar x.
      \end{displaymath}

    \item Define the mean square convergence rate as
  \begin{displaymath}
    \rho \triangleq \limsup_{k\rightarrow\infty} \left( \sup_{z(0)\neq 0}\frac{\mathbb E_v z(k)'z(k)}{z(0)'z(0)}\right)^{1/k},
  \end{displaymath}

  \end{itemize}
  \innerblock[]{Theorem}{
  For any initial condition $x(0)$, $x(k)$ converges to $\bar x$ in the mean square sense. Furthermore, the mean square convergence rate $\rho$ equals
  \begin{displaymath}
    \rho = \max(|\varphi|^2,|\lambda_2|^2,|\lambda_n|^2).
  \end{displaymath}
  }
  }
  \column{0.5}
  \block{Estimation Performance}{
  \begin{itemize}
    \item We consider agent $n$ wants to estimate the initial states of other agents.
    \item Denote the neighborhood of agent $n$ as
      \begin{displaymath}
	\mathcal N(n) = \{j_1,\dots,j_m\}.
      \end{displaymath}
    \item Define
      \begin{displaymath}
	\begin{split}
	  C &\triangleq \begin{bmatrix}
	    e_{j_1}&\dots&e_{j_m}&e_n
	  \end{bmatrix}' \in \mathbb R^{(m+1)\times n},\, y(k)\triangleq \begin{bmatrix}x^+_{j_1}(k)&\dots&x^+_{j_m}(k)&x^+_n(k)
	  \end{bmatrix}=Cx^+(k).
	\end{split}
      \end{displaymath}
    \item The information set for agent $n$ at time $k$ is 
      \begin{displaymath}
	\mathcal I(k) = \{x_n(0),y(0),\dots,y(k)\}. 
      \end{displaymath}
    \item Denote the maximum likelihood estimate of $x(0)$ given $\mathcal I(k)$ as $\hat x(0|k)$, the variance of which is defined as $P(k)$.
    \item $P(k)$ is non-increasing and hence define
      \begin{displaymath}
	P = \lim_{k\rightarrow\infty} P(k).
      \end{displaymath}
    \item Define $\tilde A \in \mathbb R^{(n-1)\times (n-1)}$ as a principal minor of $A$ by removing the last row and column. 
    \item Define $\tilde C \triangleq \begin{bmatrix} 
	\tilde e_{j_1}&\dots&\tilde e_{j_m} 
      \end{bmatrix}'\in\mathbb R^{m\times (n-1)}$, $\mathcal U \triangleq \tC'\tC$, $ \mathcal V \triangleq I - \mathcal U$.
    \item Define $\psi_1,\dots,\psi_{n-1}$ as the eigenvectors of $(I-\tA)^{-1}\mathcal U(I-\tA)^{-1}$, which forms an orthnormal basis. Suppose the corresponding eigenvalues of $\psi_{m+1},\dots,\psi_{n-1}$ are zero. Define $ \mathcal Q \triangleq \begin{bmatrix}
    \psi_{m+1}&\dots&\psi_{n-1}
  \end{bmatrix}$. 

  \innerblock{Theorem}{
  Suppose that $1>\varphi>\|\tA\|$. $P$ is given by the following equality:
  \begin{displaymath}
    P = \begin{bmatrix} 
      \mathcal Q\left[\mathcal Q'(I-\tA)^{-1}Y(I-\tA)^{-1}\mathcal Q\right]^{-1} \mathcal Q' &0\\
      0&0
    \end{bmatrix}
  \end{displaymath}
  where $ Y = \lim_{k\rightarrow\infty} Y(k)$ is the limit of the following recursive Riccati equations:
  \begin{align*}
    Y^+(k)& = \mathcal V Y(k) \mathcal V,\\
    Y(k+1) &=\varphi^{-2}\tA\left[ \varphi^2\mathcal U+Y^+(k) -  Y^+(k)\left(\varphi^2 I+Y^+(k) \right)^{-1}Y^+(k)\right]\tA,
  \end{align*}
  with $ Y(0) = \tA \mathcal U \tA$.
  }
\item Define the essential neighborhood of sensor $i$ as 
  \begin{displaymath}
    \mathcal N_e(i) \triangleq \{j\in V:j\neq i,\,a_{ij}\neq 0\}.
  \end{displaymath}
  \innerblock{Theorem}{
  $P_{ii} = 0$ if and only if $i = n$ or ${\mathcal N_e}(i)\bigcup \{i\}\subseteq \mathcal N(n)\bigcup\{n\}$.
  }
\item The initial state of the agent $i$ is revealed to the agent $j$ if and only if $j$ can listen to the outgoing communications of $i$ and its essential neighbors.
  \end{itemize}
  }
  \block{Relation to Differential Privacy}{
  \begin{itemize}
    \item  Define a binary ``adjacency'' relation $\text{Adj}$ on the space of initial conditions: 
      \begin{displaymath}
	\text{Adj}(x^a(0),x^b(0)) \triangleq \begin{cases}
	  1& \text{, if }\|x^a(0)-x^b(0)\|\leq d\\
	  0&\text{, otherwise}
	\end{cases}.
      \end{displaymath}
    \item The maximum likelihood estimate $\hat x_i(0|k)$ is a random mapping.
  \end{itemize}
  \innerblock{Theorem}{
   If $\varepsilon>0,0.5>\delta>0$ and
    \begin{displaymath}
      \frac{d}{2\varepsilon}(K + \sqrt{K^2+2\varepsilon})\leq P_{ii}, 
    \end{displaymath}
    where $K =  Q^{-1}(\delta)$ and 
    \begin{displaymath}
      Q(x) \triangleq \frac{1}{\sqrt{2\pi}}\int_x^\infty \exp\left(-\frac{u^2}{2}\right)du,
    \end{displaymath}
 then for any $k\geq 0$, the mapping $\hat x_i(0|k)$ is $(\varepsilon,\delta)$-differentially private for $\text{Adj}$. 
  }
  }
  %\note{Notetext} % See Section 4.3
  \end{columns}

\begin{columns} % See Section 4.4
  \column{0.7} % See Section 4.4
  \block{Numerical Examples}{
  \begin{minipage}{0.2\textwidth}
    \begin{center}
      \begin{tikzpicture}
	\node (n4) at (0,0) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {4};
	\node (n1) at (0,3cm) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {1};
	\node (n3) at (3cm,0) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {3};
	\node (n2) at (3cm,3cm) [circle,draw=caltechprimarycolor!50,fill=caltechprimarycolor!20,thick] {2};
	\draw [semithick] (n1)--(n2)--(n3)--(n4)--(n1);
      \end{tikzpicture}
    \end{center}
    \begin{itemize}
      \item $a_{ii} = a_{ij} = 1/3$, for $j \in \mathcal N(i)$.
      \item $\|\tA\| = 0.805$. 
      \item We choose $\varphi = 0.9$.
    \end{itemize}
  \end{minipage}
  \begin{minipage}{0.45\textwidth}
    \setlength{\figureheight}{8cm}
    \setlength{\figurewidth}{10cm}
    \input{convergence.tikz}\;\;\;
    \input{variance.tikz}
  \end{minipage}
  }
  \column{0.3} % See Section 4.4
  \block{Conclusion}{
  \begin{itemize}
    \item We propose a privacy preserving average consensus algorithm.
    \item We compute the exact mean square convergence rate of the proposed algorithm and the asymptotic estimation performance.
    \item We provide a topological necessary and sufficient condition, under which the privacy of an agent is breached. 
    \item We prove that our consensus algorithm is differentially private with proper scaling given that $P_{ii}\neq 0$. 
  \end{itemize}
  }
\end{columns}
\end{document}
