clc
clear
%assuming the following topology
% 4--5--1
%    |  |
%    3--2 

C = [1 0 0 0;0 0 1 0;0 0 0 1];
A = [2 1 0 0 1;
     1 2 1 0 0;
     0 1 2 0 1
     0 0 0 3 1
     1 0 1 1 1]/4;

A = A(1:4,1:4);

C = [1 0 0;0 0 1];
A = [1 1 0;1 1 1;0 1 1];
phi = 0.9;

H{1} = C*A;
F{1} = C;
for k = 2:10
    F{k} = [C zeros(2,3*(k-1));H{k-1}/phi F{k-1}];
    H{k} = [C*A;H{k-1}*A/phi];
    P{k} = inv(H{k}'*inv(F{k}*F{k}')*H{k});
end

% clc
% clear
% 
% A = [1 1 0 1;1 1 1 0;0 1 1 1;1 0 1 1]/3;
% tA = A(1:3,1:3);
% phi = 0.9;
% C = [1 0 0 0;0 0 1 0;0 0 0 1];
% tC = C(1:2,1:3);
% Q2 = null(tC*inv(eye(3)-tA));
% Q1 = null(Q2');
% 
% S = eye(3);
% for T = 1:10
%     S = inv(tA)*inv([tC' eye(3)] * inv([tC*tC' tC;tC' eye(3) + S*phi^2]) * [tC;eye(3)])*inv(tA');
% end
% 
% Delta = Q2 * inv(Q2'*inv(eye(3)-tA)*inv(S)*inv(eye(3)-tA)*Q2)*Q2';
