\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,mindmap,plotmarks}

\usepackage{pgfplots}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}


\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Secure CPS]{Secure Estimation for Cyber-Physical Systems in Adversarial Environment}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{
Control and Dynamical Systems\\ California Institute of Technology\\
}
\date[Feb 11, 2015]{Feb 11, 2015 at Nanyang Technological University}

\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette secondary}{fg=caltechcolor!70!black,bg=gray!15!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Cyber-Physical System}
  \begin{itemize}
    \item Cyber-Physical Systems (CPSs) refer to the embedding of computation, communication and control into physical spaces.
      \begin{center}
	\begin{tikzpicture}[scale=0.45,transform shape,level distance=0cm,
	  level 1 concept/.append style={sibling angle=120,minimum size = 3cm},
	  ]
	  \path [draw=caltechcolor!50,fill=caltechcolor!20,thick,rounded corners] (-10,-4.5) rectangle (10,7);
	  \node at (-9,6) [anchor=north west] {\Huge Physical Space};
	  \path[mindmap,concept color=black,text=white]
	  node[concept] {\Huge CPS}
	  [clockwise from=330]
	  child[concept color=green!50!black] { node[concept](communication) {\huge Comm} }
	  child[concept color=red] { node[concept](control) {\huge Control} }
	  child[concept color=blue] { node[concept](computation) {\huge Comp} };
	\end{tikzpicture}
      \end{center}
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. 
  \end{itemize}
\end{frame}


\begin{frame}{Security Threats for the CPS}
  \begin{itemize}
    \item Currently, CPSs are usually running in dedicated networks.
    \item The next generation CPS: Smart Grids, Smart Buildings, Smart Home, Internet of Things, will make extensive use of widespread sensing and networking.
    \item As the CPSs become ``smarter'', they are also more vulnerable to malicious attacks.
  \end{itemize}
  \begin{figure}[<+htpb+>]
    \begin{center}
      \includegraphics[width=0.5\textwidth]{SmartHome.jpg}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Security Threats for the CPS}
Even a closed CPS can be attacked through compromised supply chains.
\begin{figure}[<+htpb+>]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{boeing.jpg}
  \end{center}
  \caption{Boeing 787 outsourced 70\% of its parts.}
\end{figure}
\end{frame}

\begin{frame}{Security Threats for the CPS}
  \begin{itemize}
    \item Reasons to Attack CPS: Financial gains, cyber warfare, terrorism\dots
  \begin{figure}[<+htpb+>]
    \begin{center}
      \includegraphics[width=0.55\textwidth]{blackout.jpg}
    \end{center}
    \caption{Northeast blackout 2003, 50 million people lost power for up to two days}
  \end{figure}

    \item Cyber warfare is science fiction no more! Stuxnet, the first malware that spies on and subverts CPS, was discoverd in June 2010.
  \end{itemize}
\end{frame}

\begin{frame}{Beyond Cyber Security}
  \begin{itemize}
    \item Cyber security protects the confidentiality, integrity and availability of information.
    \item Tools: Encryption, Digital Signature, Tamper Resistant Device, Software Authentication, etc.
    \item Limitations of cyber security based methods:
      \begin{itemize}
	\item Legacy software
	\item Difficult to guarantee security for every single device
	\item Zero-day attacks: Stuxnet contains 4 zero-day vulnerabilities of Windows 
	\item Physical attacks
      \end{itemize}
   \item A science of cyber-physical security needs to be developed.
  \end{itemize}
\end{frame}

  \begin{frame}{CPS Architecture}
    \begin{center}
      \begin{tikzpicture}[>=latex]
	\draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
	\node at (0,0) {Physical System};
	\draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
	\node at (0,2) {Cyber Infrastructure};
	\draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
	\node at (0,4) {Decision Making and Control};
	\node [anchor=west] at (2,3) {Raw Data};
	\node [anchor=east] at (-2,3) {Control};
	\draw [thick,->] (-2,3.5)--(-2,2.5);
	\draw [thick,->] (-2,1.5)--(-2,0.5);
	\draw [thick,<-] (2,3.5)--(2,2.5);
	\draw [thick,<-] (2,1.5)--(2,0.5);
      \end{tikzpicture}
    \end{center}
  \end{frame}

  \begin{frame}{A Secure CPS Architecture}
    \begin{center}
      \begin{tikzpicture}[>=latex]
	\draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
	\node at (0,0) {Physical System};
	\draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
	\node at (0,2) {Cyber Infrastructure};
	\draw [draw=green!50,fill=green!20,thick] (-4,3.5) rectangle (4,4.5);
	\node at (0,4) {Security Layer};
	\draw [draw=red!50,fill=red!20,thick] (-4,5.5) rectangle (4,6.5);
	\node at (0,6) {Decision Making and Control};
	\node [anchor=west] at (2,3) {Raw Data};
	\node [anchor=west] at (2,5) {Secured Data};
	\node [anchor=east] at (-2,3) {Modified Control};
	\node [anchor=east] at (-2,5) {Control};
	\draw [thick,->] (-2,5.5)--(-2,4.5);
	\draw [thick,->] (-2,3.5)--(-2,2.5);
	\draw [thick,->] (-2,1.5)--(-2,0.5);
	\draw [thick,<-] (2,5.5)--(2,4.5);
	\draw [thick,<-] (2,3.5)--(2,2.5);
	\draw [thick,<-] (2,1.5)--(2,0.5);
      \end{tikzpicture}
    \end{center}
  \end{frame}
 
\begin{frame}{Previous Research: Replay Attack and Stuxnet}
  \begin{itemize}
       \item NY times:``The worm itself now appears to have included two major components. One was designed to send Iran's nuclear centrifuges spinning wildly out of control. Another seems right out of the movies: The computer program also \alert{secretly recorded what normal operations at the nuclear plant looked like, then played those readings back to plant operators}, like a pre-recorded security tape in a bank heist, so that it would appear that everything was operating normally while the centrifuges were actually tearing themselves apart.''
    \item We published the following paper: ``\emph{Secure control against replay attacks}'' in Sept, 2009, while the worm was first discovered in June, 2010. Defending against future attacks!
  \end{itemize}
\end{frame}

\section{Secure State Estimation}
\begin{frame}{Static State Estimation}
  The goal is to estimate the state $x\in \mathbb R^n$ with $m$ sensor measurements $y = [y_1,\dots,y_m]'$.
  \begin{center}
    \begin{tikzpicture}
      [place/.style={circle,draw=caltechcolor!50,fill=caltechcolor!20,thick}]
      \node (s1) at (0,1.8) [place] {$S_1$};
      \node (s2) at (0,0.2) [place] {$S_2$};
      \node (sm) at (0,-1.8) [place] {$S_m$};
      \node (fc)  at (3,0) [place] {$FC$};
      \node (result)  at (6,0) {$\hat x = f(y)$};
      \draw [semithick,->] (s1)-- node[midway,above]{$y_1$}(fc);
      \draw [semithick,->] (s2)-- node[midway,above]{$y_2$}(fc);
      \draw [semithick,->] (sm)-- node[midway,above]{$y_m$}(fc);
      \draw [semithick,->] (fc)--(result);
      \node  at (0,-0.8) {$\vdots$};
    \end{tikzpicture}
  \end{center}
  This problem is strongly related to power system state estimation.
\end{frame}

\begin{frame}{Estimation Problem: the Gaussian Case}
  \begin{itemize}
    \item Assume that 
      \begin{displaymath}
	y_i = x + v_i,
      \end{displaymath}
      where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$. The optimal estimator in a non-adversarial environment:
      \begin{displaymath}
	\hat x=	f(y) = \frac{1}{m+1}\sum_{i=1}^m y_i.
      \end{displaymath}
    \item Suppose one sensor measurement is compromised by an adversary. Then the adversary can change $\hat x$ to an arbitrary value.
    \item On the other hand, the median is a more secure estimate, since it will remain bounded when less than half of the sensors are malicious:
      \begin{displaymath}
	\hat x = \Med(y).	
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the estimation algorithm $f$ (Kerckhoffs' Principle);
    \item the true state $x$;
    \item all measurements $y$.
  \end{itemize}

  The attacker can manipulate up to $l$ measurements arbitrarily.
  \begin{displaymath}
    y^c = y + y^a,
  \end{displaymath}
  where $y^a$ is a sparse vector with no more than $l$ non-zero entries.

  The attacker wants to maximize the mean squared error.
\end{frame}

\begin{frame}{Research Directions: First Step}
  \begin{itemize}
    \item Analysis tools: Given an estimator, what is the optimal attack strategy? What is the ``worst-case'' performance of the estimator? 
    \item Fundamental limitations: Under what condition an adversary can successfully render the state estimation algorithm ``useless''?
    \item Design: How to design an estimator that has the optimal ``worst-case'' performance?
    \item Algorithm: What is the computational complexity of the optimal estimator? If the complexity is high, can we find a suboptimal estimator with low computational complexity?
  \end{itemize}
\end{frame}

\begin{frame}{Dynamic State Estimation}
  \begin{block}{System Description}
      \begin{displaymath}
	\begin{split}
	  x(k+1) &= Ax(k)  + Bu(k)+w(k),\\
	  y^c(k) &= C x(k) + v(k)+y^a(k)
	\end{split}
      \end{displaymath}
    \end{block}
    \begin{itemize}
      \item $x(k) \in \mathbb R^n$ is the state vector.
      \item $y^a(k)$ is a sparse attack vector with no more than $l$ non-zero entries. We further assume that the set of compromised sensors is time invariant. $y^c(k)$ is the ``manipulated'' measurements.
      \item  $u(k) \in \mathbb R^p$ is the control input.
      \item $w(k),v(k),x(0)$ are independent Gaussian noises.
      \item How to do state estimation/control in an adversarial environment?
    \end{itemize}
  \end{frame}

  \begin{frame}{System Diagram}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{systemdiagram}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Research Directions: First Step}
    \begin{itemize}
      \item Leverage what we developed for the static state estimator:
	\begin{figure}[ht]
	  \begin{center}
	    \begin{tikzpicture}[>=stealth',
	      box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick}]
	      \node (y) at (0,0) {$y^c(k)$};
	      \node (static) [box] at (3,0) {Static Estimator};
	      \node (kf) [box] at (7,0) {Kalman Filter};
	      \node (hatx)  at (9,0) {$\hat x(k)$};
	      \draw [->,semithick] (y)--(static);
	      \draw [->,semithick] (static)--node[above]{$\hat \xi(k)$}(kf);
	      \draw [->,semithick] (kf)--(hatx);
	    \end{tikzpicture}
	  \end{center}
	\end{figure}
	We can use the optimal static estimator to compute a state estimate $\hat \xi(k)$ of $x(k)$, based purely on the current measurement. Then we can use a Kalman-like filter to combine the current $\hat \xi(k)$ with the previous ones.
      \item Explore the temporal correlation of $y^a(k)$: Since the set of compromised sensors does not change, the static estimator can use the previous data to infer which sensor is malicious. 
      \item Fundamental limitation: Is there a condition under which there does not exist a stable estimator?
      \item Explore other estimator architectures.
    \end{itemize}
  \end{frame}

  \begin{frame}{Research Directions: Moving Forward}
    \begin{itemize}
      \item Other attack models: 
	\begin{itemize}
	  \item What if the adversary does not have full system information? Can he/she infer the system model from sensory data?
	  \item DoS attacks?
	  \item \dots
	\end{itemize}
      \item Demonstrate both the threats and countermeasures on real systems such as power system testbed or UAVs.
    \end{itemize}
  \end{frame}

\begin{frame}
    \huge{Thank you for your time. Questions?}
  \end{frame}

  \end{document}
