\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,mindmap,plotmarks}

\usepackage{pgfplots}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}


\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Secure CPS]{Secure Control in Cyber-Physical Systems}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{
Control and Dynamical Systems\\ California Institute of Technology\\
}
\date[Feb 9, 2015]{Feb 9, 2015 at Nanyang Technological University\\ \vspace{1cm}
\small Joint Work with Bruno Sinopoli, Richard M. Murray, Sean Weerakkody, Rohan Chabukswar}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette secondary}{fg=caltechcolor!70!black,bg=gray!15!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Cyber-Physical System}
  \begin{itemize}
    \item Cyber-Physical Systems (CPSs) refer to the embedding of computation, communication and control into physical spaces.
      \begin{center}
	\begin{tikzpicture}[scale=0.45,transform shape,level distance=0cm,
	  level 1 concept/.append style={sibling angle=120,minimum size = 3cm},
	  ]
	  \path [draw=caltechcolor!50,fill=caltechcolor!20,thick,rounded corners] (-10,-4.5) rectangle (10,7);
	  \node at (-9,6) [anchor=north west] {\Huge Physical Space};
	  \path[mindmap,concept color=black,text=white]
	  node[concept] {\Huge CPS}
	  [clockwise from=330]
	  child[concept color=green!50!black] { node[concept](communication) {\huge Comm} }
	  child[concept color=red] { node[concept](control) {\huge Control} }
	  child[concept color=blue] { node[concept](computation) {\huge Comp} };
	\end{tikzpicture}
      \end{center}
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. 
  \end{itemize}
\end{frame}

\begin{frame}{Security Threats for the CPS}
  \begin{itemize}
    \item Currently, CPSs are usually running in dedicated networks.
    \item However, the next generation CPS, such as Smart Grids, VANet, Smart Buildings, will make extensive use of widespread networking.
    \item As the CPSs become more interconnected and ``smarter'', they also become more vulnerable to malicious attacks. 
    \item An adversary can even attack those CPSs that are traditionally considered to be secure through compromised supply chain.
    \item Reasons to attack CPS: financial gain, cyber warfare, terrorism\dots 
    \item Stuxnet, the first malware that spies on and subverts CPS, was discoverd in June 2010, which provides a clear sample for the future to come. 
  \end{itemize}
\end{frame}

\begin{frame}{Beyond Cyber Security}
  \begin{itemize}
    \item Cyber security protects the confidentiality, integrity and availability of information.
    \item Tools: Encryption, Digital Signature, Tamper Resistant Device, Software Authentication, etc.
    \item Limitations of cyber security based methods:
      \begin{itemize}
	\item Lack of the model of the physical system
	\item Difficult to guarantee security for every single device
	\item Legacy software
	\item Zero-day attacks: Stuxnet contains 4 zero-day vulnerabilities of Windows. 
	\item Physical attacks
      \end{itemize}
    \item A science of cyber-physical security need to be developed.
  \end{itemize}
\end{frame}

\begin{frame}{Cyber-Physical Security}
  My research focuses on the following problems:
  \begin{itemize}
    \item Analysis Tools: How to quantify the performance of the system under the attack? Is the current system secure?
    \item Intrusion Detection: How to detect the existence of a malicious third party?
    \item Resilient Estimation/Control: How to ensure graceful performance degradation in the presence of the attack?
  \end{itemize}
\end{frame}

\section{Intrusion Detection}

\begin{frame}{System Model}
  \begin{block}{System Description}
      \begin{displaymath}
	\begin{split}
	  x(k+1) &= Ax(k)  + Bu(k)+w(k),\\
	  y(k) &= C x(k) + v(k).
	\end{split}
      \end{displaymath}
    \end{block}
    \begin{itemize}
      \item $x(k) \in \mathbb R^n$ is the state vector.
      \item  $y(k) \in \mathbb R^m$ is the measurements from the sensors.
      \item  $u(k) \in \mathbb R^p$ is the control input.
      \item $w(k),v(k),x(0)$ are independent Gaussian random vectors, and $x(0) \sim \mathcal N(0,\;\Sigma)$, $w(k) \sim \mathcal N(0,\;Q)$ and $v(k) \sim \mathcal N(0,\;R)$.
      \item The system is assumed to be controllable and observable.
    \end{itemize}
  \end{frame}

  \begin{frame}{LQG Controller and Kalman filter}
    \begin{itemize}
      \item We assume that the system operator wants to minimize the following cost function:
	\begin{displaymath}
	  J = \lim_{T\rightarrow \infty}\min_{u(0),\ldots,u(T)}E\frac{1}{T}\left[\sum_{k=0}^{T-1} x(k)'Wx(k)+u(k)'Uu(k)\right],
	\end{displaymath}
	where $W, U$ are positive semidefinite matrices. 
      \item The optimal controller is a fixed gain controller, which takes the following form:
	\begin{displaymath}
	  u(k) =  L^*\hat x(k),
	\end{displaymath}
      \item The optimal estimator (Kalman filter) follows the following update equations:
	\begin{align*}
	  \hat x(k+1|k) &= A\hat x(k)+Bu(k),\\
	  \hat x(k+1) &= \hat x(k+1|k) + K^*\left[y(k+1) - C\hat x(k+1|k)\right],
	\end{align*}
    \end{itemize}
  \end{frame}

  \begin{frame}{$\chi^2$ Failure Detector}
    \begin{itemize}
      \item The residue $z(k)$ of the Kalman filter is defined as
	\begin{displaymath}
	  z(k) \triangleq y(k) - C\hat x(k|k-1),
	\end{displaymath} 
	which is i.i.d. Gaussian distributed with zero mean. 
      \item $\chi^2$ detector triggers an alarm based on the following event:
	\begin{displaymath}
	  g(k)=  z(k)^T\mathcal P^{-1}z(k)> threshold,
	\end{displaymath}
	where $\mathcal P$ is the covariance matrix of $z(j)$ and $\mathcal T$ is the window size.
      \item The alarm rate at time $k$ is 
	\begin{displaymath}
	  \beta(k) = P(g(k) > threshold).
	\end{displaymath}
	If the system is operating normally, then the alarm rate is a constant, which we denote as the false alarm rate $\alpha$.
    \end{itemize}
  \end{frame}

  \begin{frame}{System Diagram}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{systemdiagram}
      \end{center}
    \end{figure}
  \end{frame}

\begin{frame}{Stuxnet}
  \begin{itemize}
    \item NY times:``The worm itself now appears to have included two major components. One was designed to send Iran's nuclear centrifuges spinning wildly out of control. Another seems right out of the movies: The computer program also \alert{secretly recorded what normal operations at the nuclear plant looked like, then played those readings back to plant operators}, like a pre-recorded security tape in a bank heist, so that it would appear that everything was operating normally while the centrifuges were actually tearing themselves apart.''
    \item We published the following paper: ``\emph{Secure control against replay attacks}'' in Sept, 2009, while the worm was first discovered in June, 2010. 
  \end{itemize}
\end{frame}
  \begin{frame}{Replay Attack Model}
    The attacker has the following capabilities:
    \begin{enumerate}
      \item The attacker can read and modify all the measurements $y(k)$ arbitrarily.
      \item It can inject an external control input $u^a(k)$ into the system. 
    \end{enumerate}
    The strategy of the attacker can be divided into two stages:
    \begin{enumerate}
      \item The attacker records a sufficient number of $y(k)$s without injecting any control input $u^a(k)$. 
      \item The attacker injects a sequence of desired control input $u^a(k)$ while replaying the previous recorded $y(k)$s starting from time $0$.
    \end{enumerate}
  \end{frame}

  \begin{frame}{Replay Attack: First Stage}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramone}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Replay Attack: Second Stage}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramtwo}
      \end{center}
    \end{figure}
  \end{frame}
  \begin{frame}{Feasibility of Replay Attacks}
    The update equation of Kalman filter follows:
    \begin{align*}
      \hat{x}(k+1|k)&=A\hat{x}(k)+Bu(k)=\left(A+BL^*\right)\hat{x}(k)\\
      &=\left(A+BL^*\right)\left(I-K^*C\right)\hat{x}(k|k-1)+\left(A+BL^*\right)K^*y(k).
    \end{align*}

    \begin{theorem}
      If $\mathcal A = (A+BL^*)(I-K^*C)$ is stable, then the detection rate of $\chi^2$ detector $\beta^c(k)$ converges to the false alarm rate $\alpha$ during the attack, i.e.,
      \begin{displaymath}
	\lim_{k\rightarrow\infty}\beta^c(k) = \alpha.  
      \end{displaymath}
      On the other hand, if $\mathcal A$ is strictly unstable, then the detection rate $\beta^c(k)$ converges to $1$, i.e.,
      \begin{displaymath}
	\lim_{k\rightarrow\infty}\beta^c(k) = 1.  
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \begin{frame}{Feasibility of Replay Attacks}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramthree}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Countermeasures:  Watermarking and Authentication}
    \begin{itemize}
      \item We design the control law by adding a watermarking signal:
	\begin{displaymath}
	  u(k) = L^*\hat x(k) + \xi(k).
	\end{displaymath}
      \item The watermarking signal $\xi(k)$ acts as a ``challenge'' and the sensor measurement is the ``response''. 
      \item During normal operation, the ``challenge'' and ``response'' are correlated through the system dynamics, while the correlation cease to exist when the replay begins.
    \item The CPS will remain stable. However, we sacrifice LQG performance since the control is not optimal.
    \item How to achieve the best trade-off between the control performance and the detection performance?
    \end{itemize}
  \end{frame}

  \begin{frame}{Optimal Watermarking Signal Design}
    \begin{itemize}
      \item We choose $\xi(k)$ to be zero mean i.i.d. Gaussian with covariance $\mathcal Q$.
      \item We could find the ``optimal'' $\mathcal Q$ by solving the following semidefinite programing problem:
	\begin{align*}
	  &\mathop{\textrm{maximize}}\limits_{\mathcal{Q}}&
	  &\tr\left(C^T\mathcal{P}^{-1}C\mathcal{U}\right)\\
	  &\textrm{subject to}&
	  &\mathcal{U}-B\mathcal{Q}B^T=\mathcal{A}\mathcal{U}\mathcal{A}^T\\
	  &&   
	  &\tr\left[\left(U+B^TSB\right)\mathcal{Q}\right]\leq \Theta\\
	  &&
	  &\mathcal Q \geq 0
	\end{align*}
    \end{itemize}
  \end{frame}

  \begin{frame}{Numerical Example: Physical Authentication}
    \begin{figure}[htpb]
      \setlength\figureheight{4.5cm}
      \setlength\figurewidth{6cm}
      \begin{center}
	\inputtikz{replay1}
      \end{center}
      \caption{Detection rate at each time step}
    \end{figure}
  \end{frame}

  \section{Secure Estimation}

  \begin{frame}{Canonical Estimation Problem}
    \begin{itemize}
      \item The goal is to estimate a scalar state $x\in \mathbb R$ with $m$ sensor measurements $y = [y_1,\dots,y_m]'$.
	\begin{center}
	  \begin{tikzpicture}
	    [place/.style={circle,draw=caltechcolor!50,fill=caltechcolor!20,thick}]
	    \node (s1) at (0,1.8) [place] {$S_1$};
	    \node (s2) at (0,0.2) [place] {$S_2$};
	    \node (sm) at (0,-1.8) [place] {$S_m$};
	    \node (fc)  at (3,0) [place] {$FC$};
	    \node (result)  at (6,0) {$\hat x = f(y)$};
	    \draw [semithick,->] (s1)-- node[midway,above]{$y_1$}(fc);
	    \draw [semithick,->] (s2)-- node[midway,above]{$y_2$}(fc);
	    \draw [semithick,->] (sm)-- node[midway,above]{$y_m$}(fc);
	    \draw [semithick,->] (fc)--(result);
	    \node  at (0,-0.8) {$\vdots$};
	  \end{tikzpicture}
	\end{center}
      \item The optimal estimator with minimum Mean Squared Error (MSE) is:
	\begin{align*}
	  \hat x=f(y)=\mathbb E(x|y).
	\end{align*}
    \end{itemize}
  \end{frame}

  \begin{frame}{Estimation Problem: the Gaussian Case}
    \begin{itemize}
      \item Assume that 
	\begin{displaymath}
	  y_i = x + v_i,
	\end{displaymath}
	where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$.
      \item The optimal estimator (in the MSE sense)
	\begin{displaymath}
	  \hat x=	f(y) = \frac{1}{m+1}\sum_{i=1}^m y_i.
	\end{displaymath}
      \item Suppose one sensor measurement is compromised by an adversary. Then the adversary can change $\hat x$ to an arbitrary value.
      \item On the other hand, the median is a more secure estimate, since it will remain bounded when less than half of the sensors are malicious:
	\begin{displaymath}
	  \hat x = \Med(y).	
	\end{displaymath}
      \item Given a desired level of ``security'', how to find the ``optimal'' estimator? 
    \end{itemize}
  \end{frame}

  \begin{frame}{Attack Model}
    We assume the attacker knows the following:
    \begin{itemize}
      \item the estimation algorithm $f$ (Kerckhoffs' Principle);
      \item the true state $x$;
      \item all measurements $y$.
    \end{itemize}
    The attacker can manipulate up to $l$ measurements arbitrarily.
    \begin{displaymath}
      y^c = y + \gamma\circ y^a,
    \end{displaymath}
    where the \emph{sensor-selection} vector $\gamma$ takes value in 
    \begin{displaymath}
      S_\gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i = l\}
    \end{displaymath}

    The attacker wants to maximize the MSE while the estimator wants to minimize the MSE. 
  \end{frame}

  \begin{frame}{Optimal Estimator: $l\geq m/2$}
    \begin{theorem}
      If $l\geq m/2$, then the optimal estimator $f^*$ is given as $f^* = \mathbb Ex$.
    \end{theorem}
    If the attacker compromises half of the sensors, then the optimal estimator will depend only on the a-prior information.

  \end{frame}

  \begin{frame}{Optimal Estimator: $l< m/2$}
    \begin{theorem}
      The optimal estimator $f$ is of the following form
      \begin{displaymath}
	f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],
      \end{displaymath}
      where $\varphi_{\mathcal K},\,|\mathcal K|=2l$ are the solutions of the following optimization problem:
      \begin{displaymath}
	\mathop{\text{minimize}}\limits_{\varphi_{\mathcal K}}\;\; \mathbb E \left\{\max_{|\mathcal K| = 2l} \left[x - \varphi_{\mathcal K} (y)\right]^2\right\}
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \section{Conclusion and Future Work}

  \begin{frame}{Moving Forward: A Secure CPS Architecture}
    \begin{center}
      \begin{tikzpicture}[>=latex]
	\draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
	\node at (0,0) {Physical System};
	\draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
	\node at (0,2) {Cyber Infrastructure};
	\draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
	\node at (0,4) {Applications: Control Algorithm};
	\node [anchor=west] at (2,3) {Raw Data};
	\node [anchor=east] at (-2,3) {Control Input};
	\draw [thick,->] (-2,3.5)--(-2,2.5);
	\draw [thick,->] (-2,1.5)--(-2,0.5);
	\draw [thick,<-] (2,3.5)--(2,2.5);
	\draw [thick,<-] (2,1.5)--(2,0.5);
      \end{tikzpicture}
    \end{center}
  \end{frame}


  \begin{frame}{Moving Forward: A Secure CPS Architecture}
    \begin{center}
      \begin{tikzpicture}[>=latex]

	\draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
	\node at (0,0) {Physical System};
	\draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
	\node at (0,2) {Cyber Infrastructure};
	\draw [draw=green!50,fill=green!20,thick] (-4,3.5) rectangle (4,4.5);
	\node at (0,4) {Security Layer};
	\draw [draw=red!50,fill=red!20,thick] (-4,5.5) rectangle (4,6.5);
	\node at (0,6) {Applications: Control Algorithm};
	\node [anchor=west] at (2,3) {Raw Data};
	\node [anchor=west] at (2,5) {Secured Data};
	\node [anchor=east] at (-2,3) {Modified Control};
	\node [anchor=east] at (-2,5) {Control Input};
	\draw [thick,->] (-2,5.5)--(-2,4.5);
	\draw [thick,->] (-2,3.5)--(-2,2.5);
	\draw [thick,->] (-2,1.5)--(-2,0.5);
	\draw [thick,<-] (2,5.5)--(2,4.5);
	\draw [thick,<-] (2,3.5)--(2,2.5);
	\draw [thick,<-] (2,1.5)--(2,0.5);
      \end{tikzpicture}
    \end{center}
  \end{frame}

  \begin{frame}{Towards a Science of Cyber-Physical Security}
    \begin{itemize}
      \item Security of cyber-physical systems is of paramount importance
      \item A science of CPS security needs to be developed
      \item System theory is a powerful tool and will play an important role in CPS security.
      \item New challenges:
	\begin{itemize}
	  \item Include security metrics within design goals
	  \item Introduce realistic attack models
	  \item Develop new risk models, analysis and design tools
	  \item \dots
	\end{itemize}
      \item New Opportunities: Leveraging the physical system model to improve security. 
    \end{itemize}
  \end{frame}

  \end{document}
