\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\DeclareGraphicsRule{*}{mps}{*}{}
\usetikzlibrary{matrix,arrows,fit,backgrounds,mindmap}

\usepackage{pgfplots}

\newlength\figureheight
\newlength\figurewidth


\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\input{\tikzdir{#1}}}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\DeclareMathOperator{\E}{\mathbb E}
\let\Tiny\tiny

%for handout
\mode<presentation>

\title[Estm over SN]{Estimation over Sensor Networks}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{
Control and Dynamical Systems\\ California Institue of Technology 
}
\date[Aug 8, 2013]{Murray Group Meeting\\ \vspace{1cm}
\small Joint Work with Bruno Sinopoli, Sean Weerakkody, Ling Shi, Duo Han and Junfeng Wu}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\begin{frame}{Motivation}
  \begin{itemize}
    \item Sensor Networks are becoming ubiquitous, with lots of interesting applications.
    \item Sensors usually have a limited power supply and limited communication channel bandwidth.
    \item Communication is unreliable. Communication packet may be dropped, corrupted or delayed. How to characterize the impact of unreliable communication on the functionality of the system.
    \item Communication costs too much energy. Reduce the number of communications to prolong the lifespan of sensors, while maintaining certain functionality. 
  \end{itemize}
\end{frame}

\section{Preliminary: Kalman Filtering}
\frame{\tableofcontents}
\begin{frame}{Minimum Mean Squared Error Estimator}
  \begin{itemize}
    \item Consider a Gaussian state $x\in \mathbb R^n$:
      \begin{displaymath}
	\E x = \bar x,\,\Cov(x) = \E xx^T = \Sigma.
      \end{displaymath}
    \item Let $y\in\mathbb R^m$ be our observation of $x$, which satisfies:
      \begin{displaymath}
	y = C x + v,
      \end{displaymath}
      where $v\sim \mathcal N(0,R)$ is a Gaussian measurement noise. 
    \item Given the measurement $y$, the optimal state estimate $\hat x$ (with minimum mean squared error) is a linear estimator:
      \begin{displaymath}
	\hat x = \E (x|y) = \bar x + \Sigma C'(C\Sigma C'+R)^{-1}(y-C\bar x).
      \end{displaymath}
    \item The error $e = x - \hat x$ is zero mean Gaussian and
      \begin{displaymath}
	\Cov(e) = (\Sigma^{-1} +  C'  R^{-1}C)^{-1}.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{LTI System}
  Consider the following LTI(Linear Time-Invariant) system:
  \begin{block}{System Description}
      \begin{displaymath}
	x_{k+1} = Ax_k +  w_k,\; y_{k} = C x_k + v_k.
      \end{displaymath}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state at time $k$.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors. 
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
    \end{itemize}
  \end{frame}

  \begin{frame}{MMSE Estimator for LTI System}
    \begin{itemize}
      \item Suppose at time $k$, we want to estimate the current state $x_k$.
      \item We have available the current and the past observations $y_k,y_{k-1},\dots,y_0$.
      \item We could write down the relationship between $y_k,\dots,y_0$ and $x_k$ as follows:
	\begin{displaymath}
	  \begin{split}
	    y_k& = C x_k + v_k \\
	    y_{k-1} & = Cx_{k-1}+v_{k-1} = CA^{-1}x_k -C A^{-1}w_{k-1} + v_{k-1}\\
	    &\vdots
	  \end{split}
	\end{displaymath}
      \item Conceptually, one could use the MMSE estimator to derive $\hat x_k$.
      \item Problem: the time and space complexity grow over time.
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman Filter}
    Define 
    \begin{displaymath}
      \begin{split}
	\hat x_k &\triangleq \E (x_k|y_k,\dots,y_0),\,e_k\triangleq  x_k -\hat x_k, \,P_k \triangleq \Cov(e_k).\\
	\hat x_k^- &\triangleq \E (x_k|y_{k-1},\dots,y_0),\,e_k^-\triangleq  x_k -\hat x_k^-, \,P_k^- \triangleq \Cov(e_k^-).
      \end{split}
    \end{displaymath}
    Kalman filter takes the following form:
    \begin{align*}
      Prediction:&&\\
      &\hat x _{k + 1}^-  = A \hat x_{k} + Bu_k  , \quad P_{k + 1}^-  = AP_{k} A'  + Q ,\\
      Correction:&&\\
      &\hat x_{k} = \hat x_{k}^-  + P_k^- C'(CP_k^- C'+R)^{-1} (y_k  - C \hat x _{k}^- ) , \\
      &P_{k} = \left[(P_{k}^-)^{-1} + C' R^{-1} C \right]^{-1},
    \end{align*}
    with initial condition
    \begin{displaymath}
      \hat x_{0}^-  = \bar x_0 ,\quad P_{0}^-  = \Sigma.
    \end{displaymath}
  \end{frame}

  \begin{frame}{Kalman Filter}
    Define the following Lyapunov operator
    \begin{displaymath}
      h(X) \triangleq AXA'  + Q, 
    \end{displaymath}
    and Riccati operators
    \begin{displaymath}
      \tilde g(X) \triangleq (X^{-1} + C'R^{-1}C)^{-1},\,g \triangleq \tilde g\circ h.
    \end{displaymath}
    Hence,
    \begin{displaymath}
      P_{k+1}^- = h(P_k),\,P_{k+1} =\tilde g(P_{k+1}^-) =  g(P_k). 
    \end{displaymath}
  \end{frame}

  \begin{frame}{Properties of Kalman Filter}
    \begin{itemize}
      \item For KF, we need to keep track on both the state estimate $\hat x_k$ and the ``accuracy'' of the estimate $P_k$.
      \item $P_k$ is deterministic, which does not depends on $y_k$.
      \item If $(A,C)$ is detectable, $\{P_k\}$ is always bounded. In other words, the estimator is stable.
      \item If we further assume that $(A,Q^{1/2})$ is controllable, then $\{P_k\}$ converges to the unique solution of algebraic Riccati equation 
	\begin{displaymath}
	  X = g(X). 
	\end{displaymath}
    \end{itemize}
  \end{frame}

  \section{Kalman Filtering with Intermittent Observations}
  \frame{\tableofcontents[currentsection]}

  \begin{frame}{Classical Kalman Filter in Unreliable Network}
    Kalman filter assumes that all the observations $y_0,y_1,\ldots,y_k$ are available at time $k$.

    However in an unreliable network, observation packets can be lost, corrupted or significantly delayed.
    \begin{figure}[<h>]
      \begin{center}
	\includegraphics{slides.2}
      \end{center}
    \end{figure}
    What is the form of optimal Filter? 
  \end{frame}

  \begin{frame}{Channel Models}  
    \begin{itemize}
      \item Erasure channel: The estimator does not use corrupted or significantly delayed packets (treated as lost). 
      \item Single sensor: All the measurements made at time $k$ are contained in a single packet and hence it is either received in full or lost completely.
	\[
	\widetilde y_k = \gamma_k y_k,\;\gamma_k=0,1
	\]
      \item Memoryless channel: $\gamma_i$s are i.i.d. distributed and $P(\gamma = 1) = p$.
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman Filter with Intermittent Observation}
    (Sinopoli, 04) This problem can still be solved recursively in a similar way as classical Kalman filter. 
    \begin{align*}
      Prediction:&&\\
      &\hat x _{k + 1}^-  = A \hat x_{k} + Bu_k  , \quad P_{k + 1}^-  = h(P_k)\\
      Correction:&&\\
      &\hat x_{k} = \begin{cases}
	\hat x_{k}^-  + P_k^- C'(CP_k^- C'+R)^{-1} (y_k  - C \hat x _{k}^- ) & \gamma_ k = 1\\ 
	\hat x_{k}^-   & \gamma_ k = 0 
      \end{cases}\\
      &P_{k} =\begin{cases}
	\tilde g(P_k^-) & \gamma_k = 1\\
	P_k^-&\gamma_k = 0
      \end{cases}
    \end{align*}
    with initial condition
    \begin{displaymath}
      \hat x_{0}^-  = \bar x_0 ,\quad P_{0}^-  = \Sigma.
    \end{displaymath}
    What about performance?

  \end{frame}

  \begin{frame}{Kalman Filter with Intermittent Observation: Performance}
    $P_k$ satisfies the following equation:
    \begin{equation}
      P_{k+1}= \gamma_k g(P_k) + (1-\gamma_k) h(P_k).
      \label{eq:basicricattieqn}
    \end{equation}
    $P_k$ is now stochastic. 
    \begin{itemize}
      \item (Censi, 11; Kar, 12) Under mild assumptions, $P_k$ converges to a limit distribution.
      \item However, very strong assumptions are needed to compute the exact distribution 
	\begin{itemize}
	  \item (Censi, 09) Non-overlapping condition: $h(X_1)\geq g(X_2)$, for all $X_1,X_2$.
	  \item (Vakili, 09) $C$ is time-varying and random.
	\end{itemize}
      \item What about $\E P_k$?
    \end{itemize}
  \end{frame}

  \begin{frame}{Existence of Critical Value}
    \begin{displaymath}
      \begin{split}
	\E P_k &= \E\left[\gamma_k g(P_k) + (1-\gamma_k) h(P_k)\right]= p\alert{\E g(P_k)} + (1-p) h(\E P_k).
      \end{split}
    \end{displaymath}
    It is impossible to get a recursive equation of $\E P_k$ due to the non-linear nature of Riccati Equation. 

    An even simpler question: whether $\{\E P_k\}$ is uniformly bounded or not?
    \begin{theorem}
      (Sinopoli,04) Under i.i.d. packets loss, if $(A,Q^{\frac{1}{2}})$ is controllable, $(C,A)$ is detectable, and $A$ is unstable, then there exists a $p_c\in[0,1)$ such that
      \begin{eqnarray}\label{eqn:lambdaCrit}
	\lim_{k\rightarrow\infty}\E P_k=+\infty & \mbox{for } 0\leq
	p \leq p_c \; \;
	\mbox{and  \hspace{3mm}$\exists P_0 \geq 0$}&\\
	\E P_k \leq M_{P_0} \;\; \forall k & \mbox{for } p_c <
	p \leq 1 \; \; \mbox{and \hspace{3mm}$\forall P_0 \geq 0$}&
      \end{eqnarray}
      where $M_{P_0} > 0 $  depends on the initial condition $P_0 \geq 0$
    \end{theorem}
  \end{frame}

  \begin{frame}{Lower-Bound for the Critical Value}
    \begin{theorem}
      (Sinopoli,04) The lower bound of $p_c$ is given by
      \begin{displaymath}
	p_c \geq 1-\frac{1}{\rho^2}, 
      \end{displaymath}
      where $\rho$ is the spectral radius of $A$. Furthermore, if $C$ is full column rank, then the equality holds.
    \end{theorem}
    We call a system to be one-step observable if $C$ is full column rank.

    The one-step observability condition can be relaxed to
    \begin{itemize}
      \item (Sinopoli,04) $A$ only has one unstable eigenvalue;
      \item (Plarre,09) $C$ invertible on the observable subspace.
    \end{itemize}
    Roughly speaking, all these conditions imply that $g$ is bounded.
  \end{frame}

  \begin{frame}{A Counter Example where the Lower Bound is not Tight}

    Consider the following LTI system:
    \begin{displaymath}
      \begin{split}
       \left[ {\begin{array}{*{20}c}
	 x_{k+1,1}  \\
	 x_{k+1,2}
      \end{array}} \right] &=
\left[ {\begin{array}{*{20}c}
	0 & 2  \\
	2 & 0 
      \end{array}} \right]  \left[ {\begin{array}{*{20}c}
	 x_{k,1}  \\
	 x_{k,2}
      \end{array}} \right] + w_k,\\
      y_k &= \left[ {\begin{array}{*{20}c}
	1 & 0 
      \end{array}} \right] x_k + v_k.
      \end{split}
    \end{displaymath}
    \begin{center}
      \includegraphics{pic.1}
    \end{center}
  \end{frame}

  \begin{frame}{A Counter Example}
    \begin{itemize} 
      \item $(A,C)$ is observable:
	\begin{displaymath}
	 C = [1\;\;0],\,CA = [0\;\;2]; 
	\end{displaymath}
      \item However, $A^2 =4I$ and $(A^2,C)$ is not observable:
	\begin{displaymath}
	 C = [1\;\;0],\,CA^2 = [4\;\;0]; 
	\end{displaymath}
      \item Half of the observations contains only contain ``redundant'' information.
      \item For a second-order system, let the eigenvalues of $A$ to be $\lambda_1$ and $\lambda_2$. If $|\lambda_1| = |\lambda_2|$ and the angle between $\lambda_1$ and $\lambda_2$ is $2\pi r/q$, then $A^q = const\times I$. Hence $(A^q,C)$ is not observable if $rank(C) = 1$.
      \item $1/q$ of the observations contains only contain ``redundant'' information.
    \end{itemize}
  \end{frame}

  \begin{frame}{A Counter Example}
    \begin{theorem}
      Consider the following unstable observable system:
      \begin{itemize}
	\item  $A$ is diagonalizable, $\rho = |\lambda_1| = |\lambda_2| $;
	\item  $rank(C) = 1$;
      \end{itemize}
      Let
      \begin{displaymath}
	\alpha = \frac{|\arg(\lambda_1)-\arg(\lambda_2)|}{2\pi}.	
      \end{displaymath}
      If $\alpha$ is irrational, then the critical value of the system is given by
      \begin{displaymath}
	p_c = 1-\frac{1}{\rho^2}.
      \end{displaymath}
      If $\alpha = r/q$ is rational, then the critical value is given by
      \begin{displaymath}
	p_c = 1-\frac{1}{\rho^{\frac{2}{1-1/q}}}.
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \begin{frame}{Non-Degeneracy}
    \begin{itemize}
      \item One-step observability: reconstruct the state from $1$ measurements. Too strong. 
      \item Observability: reconstruct the state from $n$ sequential measurements. Too weak.
      \item Let $A = diag(\lambda_1,\ldots,\lambda_n)$, $C = [C_1,\ldots, C_n]$.
      \item Consider index set $\mathcal I = \{i_1,\ldots,i_l\}\subseteq \{1,\ldots,n\}$.
      \item Define $A_{\mathcal I} = diag(\lambda_{i_1},\ldots,\lambda_{i_l})$, $C_{\mathcal I} = [C_{i_1},\ldots,C_{i_l}]$.
      \item We call the subsystem $(A_{\mathcal I}, C_{\mathcal I})$ an equiblock if $\lambda_{i_1}=\ldots=\lambda_{i_l}$. 
      \item We call the subsystem $(A_{\mathcal I}, C_{\mathcal I})$ a quasi-equiblock if $|\lambda_{i_1}|=\ldots=|\lambda_{i_l}|$. 
	\begin{definition}
	  A system is non-degenerate if $A$ is diagonalizable and every quasi-equiblock $(A_{\mathcal I},C_{\mathcal I})$ is one-step observable.
	\end{definition}
    \end{itemize}
  \end{frame}

  \begin{frame}{Non-Degeneracy}
    \begin{itemize}
      \item Consider the following system: 
	\begin{displaymath}
	  \begin{split}
	    A = diag(2,2,3,-3), C = \left[ {\begin{array}{*{20}c}
	      C_1,C_2,C_3,C_4
	    \end{array}} \right].
	  \end{split}
	\end{displaymath}
      \item A system is observable if every equiblock is one-step observable: $C_1$, $C_2$ are linear independent.
      \item A system is non-degenerate if every quasi-equiblock is one-step observable: $C_1$, $C_2$ are linear independent and $C_3$, $C_4$ are linear independent.
      \item One-step observability implies that $C_1$, $C_2$, $C_3$, $C_4$ are linear independent.
      \item Non-degeneracy is stronger than observability, but weaker than one-step observability. 
    \end{itemize}
    \begin{theorem}
      If the system is non-degenerate, then $(A^k,C)$ is observable for any $k\in \mathbb N$. 
    \end{theorem}
  \end{frame}

  \begin{frame}{Critical Value and Tail Distribution for Non-Degenerate System}
    \begin{theorem}
      For a system whose unstable part is non-degenerate, the critical value is given by
      \begin{displaymath}
	p_c = 1-\frac{1}{\rho^2}.
      \end{displaymath}
      Furthermore, $P_k$ converges to a heavy-tailed distribution, i.e.,
      \begin{displaymath}
	\lim_k P(\tr(P_k)\geq M) \sim M^\varphi,
      \end{displaymath}
      where 
      \begin{displaymath}
	\varphi = \frac{\log(1-p)}{2\log \rho}.	
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \section{Event-Based Sensor Scheduling}

  \frame{\tableofcontents[currentsection]}
  \begin{frame}{Sensor Scheduling}
    \begin{itemize}
      \item Communication costs too much energy
      \item Sensors could deliberately drop some packets ($\gamma_k = 0$) to save energy
      \item Define the communication rate $r$ to be
	\begin{displaymath}
	  r = \limsup_{k\rightarrow\infty}\frac{1}{N}\sum_{i=0}^{N-1}\gamma_i 
	\end{displaymath}
      \item Trade-off between energy consumption and estimation quality.
      \item When to send the information?
    \end{itemize}
  \end{frame}

  \begin{frame}{Sensor Scheduling: Off-line Schedule}
      The schedule is based on the statistics of the system, and hence can be determined off-line:
    \begin{itemize}
      \item Deterministic Schedule: send the temperature only at even time. 
      \item Stochastic Schedule: send the temperature with 50\% probability at each time. 
    \end{itemize}
    The optimal filter is the same as KF with intermittent observation.

    If no measurement arrives at time $k$, then the fusion center can only perform a prediction update.
  \end{frame}

  \begin{frame}{Sensor Scheduling: Event-Based Schedule}
    \begin{itemize}
      \item The schedule depends on both the statistics and the realization of the system. 
      \item Fox example, if the temperature of the room is normal distributed around $72.5$, we could design the schedule to be: send the temperature if the temperature is outside $[70,75]$.
      \item Even if no measurement arrives at time $k$, the fusion center can still perform a correction step, since it knows that the temperature is inside $[70,75]$.
    \end{itemize}
  \end{frame}

  \begin{frame}{Optimal Filter for Event-Based Schedule}

    \begin{itemize}
      \item When $\gamma_k = 0$, $y_k$ is a truncated Gaussian.
	\setlength\figureheight{2cm}
	\setlength\figurewidth{3cm}
	\begin{center}
	  \inputtikz{gaussian}$\times$\inputtikz{det}$\Rightarrow$\inputtikz{det2}
	\end{center}
      \item The optimal filter is given by the forward algorithm of hidden Markov model.
      \item Need to keep track of the pdf of $x_k$ given $y_k,\dots,y_0$.
      \item Not linear in general, difficult to analyze the performance.
    \end{itemize}
  \end{frame}

  \begin{frame}{A Stochastic Event-Trigger}
	\setlength\figureheight{2cm}
	\setlength\figurewidth{3cm}
	\begin{center}
	  \inputtikz{gaussian}$\times$\inputtikz{rand}$\Rightarrow$\inputtikz{rand2}
	\end{center}
      \begin{itemize}
	\item At each time $k$, the sensor generates a random variable $\zeta_k\sim U[0,1]$ .
	\item Decide whether to send or not based on the following rule:
	  \begin{displaymath}
	      \gamma_k= \begin{cases}
		0&\zeta_k \leq \Phi(y_k)\\
		1&\zeta_k > \Phi(y_k)
	    \end{cases},
	  \end{displaymath}
	  where $\Phi$ is defined as
	  \begin{displaymath}
	    \Phi(y) = \exp\left( -\frac{1}{2}y'Yy \right). 
	  \end{displaymath}
      \end{itemize}
  \end{frame}

  \begin{frame}{Optimal Filter}
    The optimal filter is similar to the KF:
    \begin{align*}
      Prediction:&&\\
      &\hat x _{k + 1}^-  = A \hat x_{k} + Bu_k  , \quad P_{k + 1}^-  = h(P_k)\\
      Correction:&&\\
      &\hat x_{k} = \begin{cases}
	\hat x_{k}^-  + P_k^- C'(CP_k^- C'+R)^{-1} (y_k  - C \hat x _{k}^- ) & \gamma_ k = 1\\ 
	(I - P_k^-C'(CP_k^-C'+R+Y^{-1})^{-1}C)\hat x_{k}^-   & \gamma_ k = 0 
      \end{cases}\\
      &P_{k} =\begin{cases}
	\left[\left(P_k^-\right)^{-1} + C'\alert{R}^{-1}C\right]^{-1} & \gamma_k = 1\\
	\left[\left(P_k^-\right)^{-1} + C'\left( \alert{R+Y^{-1} }\right)^{-1}C\right]^{-1}  &\gamma_k = 0
      \end{cases}
    \end{align*}
    with initial condition
    \begin{displaymath}
      \hat x_{0}^-  = \bar x_0 ,\quad P_{0}^-  = \Sigma.
    \end{displaymath}
  \end{frame}


  \begin{frame}{Communication Rate}
    \begin{theorem}
      Let $\Pi_k = \Cov(y_k)$, then
      \begin{displaymath}
	P(\gamma_k = 1) = 1-1/\sqrt{\det(I+\Pi_kY)}.
      \end{displaymath}
      If the system is stable, then $\Pi_k\rightarrow \Pi$, which is given by
      \begin{displaymath}
	\Pi = CX C' + R,
      \end{displaymath}
      where $X = \lim_k \Cov(x_k)$ is the solution of the following Riccati equation
      \begin{displaymath}
	X = AXA' + Q. 
      \end{displaymath}
      The communication rate satisfies
      \begin{displaymath}
	r = 1-\frac{1}{\sqrt{\det(I+\Pi Y)}}.
      \end{displaymath}
    \end{theorem}
  \end{frame}


  \begin{frame}{Estimation Performance}
    \begin{itemize}
      \item When no packet arrives, it is equivalent to using a sensor with noise covariance $R + Y^{-1}$.
      \item $P_k$ is always bounded even if no packet arrives. 
      \item $P_k$ is oscillating between $\underline P$ and $\overline P$, which are the solution of the following Riccati equations:
	\begin{displaymath}
	  \begin{split}
	    \overline P &= \left[\left(A\overline PA' + Q\right)^{-1} + C'\left( \alert{R+Y^{-1} }\right)^{-1}C\right]^{-1}\\
	    \underline P &= \left[\left(A\underline PA' + Q\right)^{-1} + C'\alert{R}^{-1}C\right]^{-1}.
	  \end{split}
	\end{displaymath}
      \item Using the concavity and monotonicity of Riccati equation, a lower bound of $\E P_k$ can be derived as the solution of the following Riccati equation:
	\begin{displaymath}
	    P= \left[\left(A PA' + Q\right)^{-1} + C' \alert{R_1}^{-1}C\right]^{-1},
	\end{displaymath}
	where
	\begin{displaymath}
	  R_1 = \left( rR^{-1} + (1-r)(R+Y^{-1})^{-1} \right)^{-1}.
	\end{displaymath}<++>
    \end{itemize}
  \end{frame}

  \begin{frame}{Event-Trigger Parameter Optimization}
    We consider the following optimization problem:
    \begin{align*}
      &\mathop{\textrm{minimize}}\limits_{Y}&
      & r\nonumber\\
      &\textrm{subject to}&
      & \overline P\leq \Delta.
    \end{align*}
    Using matrix inversion lemma, the constraint can be rewritten in SDP form as:
    \begin{align*}
      \left[ {\begin{array}{*{5}c}
	Q^{-1}-M_1  &  Q^{-1}A\\
	A^{\prime}Q^{-1} & A^{\prime}Q^{-1}A + S
      \end{array}} \right] &\ge 0 \nonumber
      \\
      \left[ {\begin{array}{*{5}c}
	R^{-1}-M_2  &  R^{-1}\\
	R^{-1} & R^{-1}+Y
      \end{array}} \right] &\ge 0 \nonumber
      \\ M_1 - S + C^{\prime}M_2C &\ge 0 \nonumber \\
      Y \ge 0, ~ S &\ge \Delta^{-1}
    \end{align*}
  \end{frame}

  \begin{frame}{Event-Trigger Parameter Optimization}
    The communication rate $r$ can be bounded by
    \begin{displaymath}
      1-\frac{1}{\sqrt{1+\tr(\Pi Y)}} \leq  r \leq 1-\exp\left(-\frac{1}{2}\tr(\Pi Y)\right).
    \end{displaymath}
    The optimization problem can be relaxed to
    \begin{align*}
      &\mathop{\textrm{minimize}}\limits_{Y}&
      & \tr(\Pi Y)\nonumber\\
      &\textrm{subject to}&
      & \overline P\leq \Delta.
    \end{align*}
    If the optimal $Y$ of the relaxed problem is rank $1$, then it is also optimal for the original problem.
  \end{frame}

  \begin{frame}{Numerical Examples}
    We consider the following system
    \begin{displaymath}
     A = 0.8,\,C = 1,\, Q = R = 1. 
    \end{displaymath}
	\setlength\figureheight{5cm}
	\setlength\figurewidth{8cm}
    \begin{center}
      \inputtikz{eventbased}
    \end{center}
  \end{frame}

  \section{Conclusion}
  \begin{frame}{Conclusion}
    We consider the problem of state estimation over sensor networks. First we analyze the impact of packet drops on KF:
    \begin{itemize}
      \item We show that for general systems, the lower bound of the critical value is not tight, due to loss of observability.
      \item To avoid loss of observability, we propose a non-degeneracy condition.
      \item For non-degenerate systems, we are able to characterize the critical value and tail distribution.
    \end{itemize}
    Second, we propose an event-based sensor schedule:
    \begin{itemize}
      \item We propose a stochastic event-trigger to maintain the Gaussian property of the measurement.
      \item We derive the optimal filter for our event-trigger and analyze its performance.
      \item We formulate the event-trigger parameter design problem as an optimization problem and relaxed it into a convex form.
    \end{itemize}
  \end{frame}
  \begin{frame}{Future Directions}
    \begin{itemize}
      \item multi-sensors, multi-hop networks
      \item different channel model (Markovian,\dots) 
      \item packet delays
      \item quantization 
      \item close the loop
      \item \dots
    \end{itemize}
  \end{frame}
  \begin{frame}
    \center\Huge{Thank you for your time!}
  \end{frame}
  \end{document}

