\documentclass[11pt,a4paper]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{plain}                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.27in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{9.2in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[utf8]{inputenc}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{subcaption}
\usetikzlibrary{matrix,fit,external,arrows}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi

\newcommand{\alert}[1]{\textcolor{red}{#1}}

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\begin{document}

\title{Secure Estimation for Cyber-Physical Systems in Adversarial Environment}
\author{Yilin Mo}
\maketitle

\section{Motivation and Background}

Cyber-Physical Systems (CPS) refer to the embedding of sensing, networking, computation and control into physical spaces with the goal of making them efficient, reliable and ``smart''. Driven by the miniaturization and integration of sensing, communication and computation in cost effective devices, CPSs have the potential to radically transform industries such as aerospace, transportation, built environments, energy, healthcare and manufacturing, to name a few. However, the opportunities afforded by CPS are accompanied with even greater challenges. Currently, most CPSs use dedicated communication networks, which have so far sheltered systems from the outside world. However, the next generation of smarter CPSs, such as smart grids, vehicle ad-hoc networks, smart buildings, will make extensive use of off-the-shelf networking and computing. This combined with unattended operation of a myriad of devices, provides new entry points for malicious entities to inject attacks on CPSs. A wide variety of motivations exists for launching an attack on CPSs, ranging from financial gains (stealing electricity without paying the bill), all the way to terrorism and cyber-warfare (threatening an entire population by manipulating vital infrastructure). Any attack on safety-critical CPSs, such as power grids, may significantly hamper the economy and even lead to the loss of human life. 

The recent Stuxnet incident should serve as a clear warning of the damage possible. Stuxnet is a complex malware that infected uranium enrichment facilities in Iran, reportedly causing damage to approximately 1,000 centrifuges at these plants~\cite{Fidler2011}. The Stuxnet malware intercepts all input and output signals of the centrifuge. To cause damage, Stuxnet implements control logic to either cause pressure in centrifuges to increase or change the rotational speed. To prevent detection, Stuxnet also delivers previously recorded sensory data to the SCADA system, performing a replay attack~\cite{Langner2013}. As a result, the estimator would still think that the system is operating normally. %The block diagram of the malware is illustrated in Fig~\ref{fig:stuxnet}.

%\begin{figure}[ht]
%    \begin{minipage}[t]{0.5\textwidth}
%      \inputtikz{replaydiagramone}
%      \subcaption{Recording Phase}
%    \end{minipage}
%    \begin{minipage}[t]{0.5\textwidth}
%      \inputtikz{replaydiagramtwo}
%      \subcaption{Replay Phase}
%    \end{minipage}
%  \caption{The block diagram for the Stuxnet malware. (a) Recording Phase. The malware records a sequence of sensory data without interfering the normal system operation. (b) Replay Phase. The malware injects its own control signal to the system, while replaying the previous recorded sensory data to the state estimator.}
%  \label{fig:stuxnet}
%\end{figure}

The state estimation algorithm is essential for designing control algorithm over CPS. It also serves as a fundamental building block for many other applications in CPSs, such as the real-time electricity market of the power grids~\cite{Xie2011}. As a result, the security of the state estimator is of utmost importance for the security of CPS. On the other hand, as is seen in the Stuxnet example, a compromised state estimator enables the adversary to cause severe damage to the system. 

The goal of this proposal is thus to develop a secure state estimation algorithm for CPS in adversarial environment with potentially compromised sensory data, as we believe that a secure state estimator will play a central role in the future research of CPS security.

{\bf Related Work:} There is substantial work in many different fields that try to address the security or similar aspect of CPS. We provide here a short analysis of some of the primary areas of overlap, with the goal of highlighting some methodologies and tools that we hope to leverage and improve upon from these areas. 

\emph{Information Security:} Information security seeks to protect the confidentiality, integrity and availability of information~\cite{Whitman2011}. Tools such as secure communication protocol (e.g., SSL/TLS, IPsec, SSH), software attestation~\cite{Seshadri2004} and tamper resistant devices~\cite{Smith1999} can help to secure the communication between the sensors and the estimator and also the software and hardware of the sensors themselves. However, information security is not well-equipped to protect the system from physical attacks (e.g., bypassing a power meter using a wire to steal electricity). Furthermore, it is difficult to guarantee the security for every single sensor due to various reasons such as outdated firmware, zero-day attacks. As a result, we believe that we should not rely solely on information security to protect \emph{all} sensory data. Instead, information security should be used to ensure that \emph{most} of the sensors are secure, while a secure estimation algorithm should be able to tolerate the rest of the compromised sensors.

\emph{Robust Estimation:} Robust estimators have also been extensively studied in the literature~\cite{robust2009,Kassam1985,robust2006}. However, such kinds of approaches typically assume that the outliers of the data are generate \emph{independently} by some other probability distribution different from the model assumptions. For dynamical systems, techniques such as $\mathcal H_\infty$ estimators have also been an active research area for the past decades. The $\mathcal H_\infty$ estimator can be seen as the worst-case estimator when the disturbance has bounded energy or power spectral density. On the other hand, an intelligent attacker could potentially design its strategy to optimally exploit the vulnerabilities of the system to cause maximal damage. Thus, the ``independence'' or ``boundedness'' assumptions may not hold for an intelligent adversary and the robust techniques need to be reexamined in the context of CPS security. 

\emph{Fault Detection and Identification:} The control community has a long history of research in fault detection and identification in dynamical systems~\cite{Willsky:1975bp}. For static estimation problem, bad data detection and identification techniques, which is based on identifying and truncating the ``atypical'' data, have been widely used in large scaled systems such as power grid~\cite{Abur2004}. While such approaches are very successful in detecting and removing random failures, it has been shown that they are not effective against attacks~\cite{liu2009,henrik2010}.

\emph{Secure Control:} The area of secure control systems has emerged in the last 5 years. A substantial amount of effort~\cite{Mo:2009bj,Pasqualetti2013,wirelesscontrol,Fawzi2012, liu2009} has been dedicated to the detection and identification of malicious behaviors in dynamical systems. These researches provide the fundamental limitation on what can be achieve by a secure state estimator, since a undetectable attack will likely to cause a large or even unbounded estimation error and render the estimator unstable.

{\bf Project Goals on Objectives} The overarching goal of this project is to develop systematic tools for designing secure estimation algorithm of CPS that can work even in adversarial environment. As specific objectives, we seek to develop:
\begin{enumerate}
  \item A \emph{secure state estimation algorithm} for CPS with compromised sensory data. The state estimator should be able to tolerate up to a pre-specify number of compromised sensor and remain stable. 
  \item \emph{Demonstrations} of our methodology via power grid simulation software or real CPS testbeds.
\end{enumerate}

\section{Technical Approach}
The approach we plan to take in this project builds on several specific topics in secure control theory that have been explored by us over the last 5 years. While innovations will be needed to develop systematic tools to design secure estimators described in this proposal, we believe that the initial results provided below give some promising directions for us to advance towards our goal.

A starting point for designing a secure state estimator is to define the system model and the adversary model. In this project, we plan to consider both a static system model, where the state estimation is purely based on the sensory data made at current time, as well as a dynamical system model, where the state estimation is based on the whole history of sensor measurements.

\subsection{Static Estimation}
We first describe the static estimation problem, which is essential for power system state estimation~\cite{Abur2004}. We assume that the state of the system can be described by an $n$-dimensional vector $x\in \mathbb R^n$ and $m$ sensors are measuring the system. The sensory data is an $m$-dimensional vector $y \triangleq [y_1,\dots,y_m]'\in \mathbb R^m$, with $y_i$ being the measurement made by sensor $i$. We assume that $x$ and $y$ satisfies the following linear relationship:
\begin{displaymath}
 y = Cx + v, 
\end{displaymath}
where $v\in \mathbb R^m$ characterizes the measurement noise. We assume that an adversary has the ability to manipulate up to $l$ of the $m$ sensor measurements. Formally, this means that our estimate has to rely on a vector $y^c\in\mathbb R^m$ of \emph{manipulated measurements} defined by
\begin{displaymath}
  y^c = y + y^a,
\end{displaymath}
where the \emph{bias} vector $y^a$ is a sparse vector with no more than $l$ non-zero entries. The state estimator $f: \mathbb R^m\rightarrow\mathbb R^n$ can be seen as a mapping which maps a manipulated measurement to a state estimate $\hat x = f(y+y^a)$. The goal of the attacker is to increase the Mean Squared Error (MSE) $P \triangleq \mathbb E(x - \hat x)(x - \hat x)'$ and the goal of the estimator is to reduce the MSE. We further assume that the attacker knows the exact estimator used by the system. Therefore, the secure estimator design problem becomes a mini-max problem, where one seek to minimize the estimation error against the ``worst'' sensory data manipulation.

A special case of this problem, where the state $x\in \mathbb R$ is a scalar, has been solved in our previous work~\cite{Mo2013a}. The optimal attack strategy is to choose $y^a$ to either maximize $\hat x$ or minimize $\hat x$ depending on the true state $x$. For the optimal estimator design, there exist two cases: 
\begin{enumerate}
  \item If at least half the measurements ($l\geq m/2$) are compromised, then the adversary effectively renders the sensory data useless and the secure estimator should ignore all measurements and be based solely on the a-priori information on $x$. This result illustrates the fundamental limitation on the secure estimator design in the sense that no ``useful'' state estimation can be made if half of the sensors are compromised.
  \item If less than half the measurements ($l<m/2$) are compromised, then the  optimal secure estimator can be computed as the solution of a convex optimization problem. 
\end{enumerate}

There are still many open problems for us to solve before we can generalize our result to multi-dimensional case. First, we must identify the optimal attacker's strategy in multi-dimensional case and then design the optimal secure estimator accordingly. Another problem for the multi-dimensional case is to identify the observable subspace. For one-dimensional case, it is clear that the state is observable unless the adversary compromised no less than half of the sensory data. On the other hand, for multi-dimensional case, it is possible that the adversary can render some states or some linear combinations of the states unobservable to the system by only compromising a small number of sensors~\cite{henrik2010}. As a result, we may only be able to estimate $Qx$ instead of $x$, where the matrix $Q$ indicates the observable space. 

\subsection{Dynamical State Estimation}

We now describe the dynamical state estimation problem of CPS. We adopt the following discrete time linearized model of the CPS:
\begin{displaymath}
  x(k+1) = A x(k)+ Bu(k)+ w(k),
\end{displaymath}
where $x(k)$ , $u(k)$ and $w(k)$ are the state, control input and process noise at time $k$ respectively. Similar to the previous case, we assume that $m$ sensors are measuring the system. The sensory data is given by vector $y(k) \triangleq [y_1(k),\dots,y_m(k)]'\in \mathbb R^m$, with $y_i(k)$ being the measurement made by sensor $i$ at time $k$. We assume that an adversary has the ability to manipulate up to $l$ of the $m$ sensor measurements. Hence, the \emph{manipulated measurements} received by the estimator at time $k$ is given by 
\begin{displaymath}
 y^c(k) = Cx(k) + v(k) + y^a(k), 
\end{displaymath}
where $v(k)$ is the measurement noise and $y^a(k)$ is a sparse vector with less than $l$ non-zero entries. Furthermore, we assume that the set of the compromised sensors remains constant over time.

A starting point for the architecture of the secure estimator we plan to explore is illustrated in Figure~\ref{fig:estimator}. The estimator consists of a static estimator, as is discussed in the previous subsection, and a Kalman filter. The static estimator will first process the manipulated measurement $y(k)$ to generate an static state estimate $\hat \xi(k) = x(k) + \mu(k)$, where $\mu(k)$ denotes the estimation error of the static estimator. If the state $x(k)$ is not reconstructable using $y(k)$, then the static estimator generates $\hat \xi(k) = Qx(k)+\mu(k)$ where $Q$ indicates the one-step observable subspace of $x(k)$. A Kalman filter, whose gain is chosen according to the $Q$ matrix and the statistics of $\mu(k)$, will take the static state estimate $\hat \xi(k)$ as an input to generate the dynamical state estimate $\hat x(k)$.

\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick}]
      \node (y) at (0,0) {$y^c(k)$};
      \node (static) [box] at (3,0) {Static Estimator};
      \node (kf) [box] at (8,0) {Kalman Filter};
      \node (hatx)  at (11,0) {$\hat x(k)$};
      \draw [->,semithick] (y)--(static);
      \draw [->,semithick] (static)--node[above]{$\hat \xi(k)$}(kf);
      \draw [->,semithick] (kf)--(hatx);
    \end{tikzpicture}
  \end{center}
  \caption{A candidate architecture for secure dynamical estimator. The manipulated measurements is first being processed by a static estimator, the state estimate $\hat\xi(k)$ from which is then sent into the Kalman filter.} 
  \label{fig:estimator}
\end{figure}

There are several open problems concerning the candidate architecture. First, it is not clear how to design the Kalman gain since the static estimation error vector $\mu(k)$ can potentially be manipulated by the adversary and hence it is not necessarily Gaussian or even stochastic. If it turns out that $\mu(k)$ is bounded, then we may be able to use an $\mathcal H_\infty$ type of estimator instead of a Kalman filter to guarantee bounded estimation error. Moreover, it would be very useful to derive the fundamental limitations on what is achievable for a secure dynamical estimator and to characterize the conservativeness of the candidate architecture.

\bibliographystyle{plain}
\bibliography{bib}
\end{document}
