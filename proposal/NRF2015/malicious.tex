To achieve average consensus, every agent in the system needs to follow the update rule \eqref{eq:singleupdate}. One malicious node, which does not obey \eqref{eq:singleupdate}, can force the consensus algorithm to converge to an arbitrary value different from the average or can prevent the algorithm from converging at all. As a result, to ensure the security and robustness of the average consensus algorithm, it is of utmost important for the benign agents to
\begin{enumerate}
\item Detect the presence of the malicious agents;
\item Identify the malicious agents;
\item Isolate the malicious agents from the network. In other words, a benign agent should drop the communication with another agent if it has been identified as a malicious agent. 
\end{enumerate}

The first two problems (detection/identification) has been studied by Pasqualetti et al.~\cite{Pasqualetti12}. Suppose that agent $i$ is a benign agent and wants to detect/identify the potential malicious agents in the network. To this end, it has available the information sent to it by its neighbors, which is denoted as $y(k)$:
\begin{displaymath}
y(k) \triangleq \begin{bmatrix}
x_{i_1}(k)&\dots&x_{i_m}(k)&x_i(k)
\end{bmatrix}' = C x(k),
\end{displaymath}
where $\{i_1,...,i_m\}$ is the set of neighbor of the agent $i$. In a nutshell, if all agents are benign, i.e., following the update rule \eqref{eq:singleupdate}, then the trajectory of $y(k)$ will belongs to a linear subspace of all possible trajectory. On the other hand, if some agents does not follow \eqref{eq:singleupdate}, then the trajectory of $y(k)$ may deviates from the nominal subspace, which enables the detection/identification. Based on these observations, Pasqualetti et al. provide necessary and sufficient conditions on the detectability and identifiability of the malicious agents. 

This research focuses on using formal methods to generate a distributed supervisory controller that solved the isolation problem. The proposed control structure is illustrated in Fig~\ref{fig:diagram}: At the lowest level, each (benign) node is running an average consensus algorithm \eqref{eq:singleupdate}. A fault detector as is proposed by Pasqualetti et al. is monitoring the consensus process and can send signals to the supervisory controller. The supervisory controller will decide whether to drop/ignore the communication from a neighboring node and send its decision to the fault detector, the low level consensus algorithm and the neighboring nodes.


The isolation problem proposes additional challenge, for the following reasons:
\begin{enumerate}
\item A benign agent will also deviate from the nominal behavior \eqref{eq:singleupdate} when it drops the communication with a neighboring malicious node. 
\item  The isolation process may not be synchronized. As a result, different benign nodes may drop the communication with a malicious node at different time.
\item The malicious agent may potentially exploit the isolation protocol to its advantage. For example, it may be able to force the network to isolate benign agents.
\end{enumerate}



    \begin{figure}[htpb]
      \begin{center}
\begin{tikzpicture}[box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick},
point/.style={coordinate},
every node/.append style={font=\small}
]
\matrix[row sep = 10mm, column sep = 7mm]{
%first row
\node (p1) [point] {};&
\node (super1) [box] {Supervisory Ctrl};&
\node (super2) [box] {Supervisory Ctrl};&
\node (p2) [point] {};\\
%second row
&
\node (fault1) [box] {Fault Detector};& 
\node (fault2) [box] {Fault Detector};& 
\\
%third row
\node (p3) [point] {};&
\node (consensus1) [box] {Avg Consensus};&
\node (consensus2) [box] {Avg Consensus};&
\node (p4) [point] {};\\
};


\draw [semithick,<->] (super1)--(super2);
\draw [semithick,<->] (super1)--(fault1);
\draw [semithick,<->] (super2)--(fault2);
\draw [semithick,<->] (consensus1)--(consensus2);
\draw [semithick,->] (consensus1)--(fault1);
\draw [semithick,->] (consensus2)--(fault2);
\draw [semithick,->] (super1)--(p1)--(p3)--(consensus1);
\draw [semithick,->] (super2)--(p2)--(p4)--(consensus2);
\node [semithick,draw=blue!50,rectangle,rounded corners,dashed,fit=(p1) (super1)(p3)(consensus1)(fault1),label=above:Node i] {};
\node [semithick,draw=blue!50,rectangle,rounded corners,dashed,fit=(p2) (super2)(p4)(consensus2)(fault2),label=above:Node j] {};

\end{tikzpicture}
      \end{center}
      \caption{Proposed Control Architecture}
      \label{fig:diagram}
    \end{figure}
