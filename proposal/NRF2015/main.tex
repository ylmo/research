\documentclass[12pt,a4paper]{article}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\pagestyle{plain}                                                      %%
%%%%%%%%%% EXACT 1in MARGINS %%%%%%%                                   %%
\setlength{\textwidth}{6.27in}     %%                                   %%
\setlength{\oddsidemargin}{0in}   %% (It is recommended that you       %%
\setlength{\evensidemargin}{0in}  %%  not change these parameters,     %%
\setlength{\textheight}{9.2in}    %%  at the risk of having your       %%
\setlength{\topmargin}{0in}       %%  proposal dismissed on the basis  %%
\setlength{\headheight}{0in}      %%  of incorrect formatting!!!)      %%
\setlength{\headsep}{0in}         %%                                   %%
\setlength{\footskip}{.5in}       %%                                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                                   %%
\newcommand{\required}[1]{\section*{\hfil #1\hfil}}                    %%
\renewcommand{\refname}{\hfil References Cited\hfil}                   %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\usepackage[utf8]{inputenc}
\usepackage{amsfonts,amsmath,amssymb}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{subcaption}
\usetikzlibrary{matrix,fit,external,arrows}
\usepackage{pgfplots}
\usepackage{lmodern}
\renewcommand{\rmdefault}{phv} %Arial
\renewcommand{\sfdefault}{phv} % Arial

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi

\newcommand{\alert}[1]{\textcolor{red}{#1}}

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\begin{document}

\thispagestyle{empty}
\begin{center}
  \phantom{x}
  \vspace{1.6in}
  {\Large\bf Secure Control of Cyber-Physical Systems\\}
  \vspace{2.6in}
  Yilin Mo\\
  \vspace{1in}
  California Institute of Technology\\
  Pasadena, CA\\
  \vspace{.4in}
  Feb, 2015
\end{center}
\vspace*{\fill} 
\noindent{\bf Keywords:}  Security, Cyber-Physical System, Control, Estimation, Robustness
\newpage
\tableofcontents
\newpage
\setcounter{page}{1}
\section*{Executive Summary}
The broad objective of the proposed research is to develop new principles and tools for secure control algorithm design for Cyber-Physical Systems (CPSs). The term CPS refers to the embedding of sensing, networking, computation and control into physical spaces. Driven by the miniaturization and integration of sensing, communication and computation in cost-effective devices, cyber-physical systems have the potential to radically transform industries such as aerospace engineering, transportation, intelligent buildings, energy, healthcare and manufacturing. However, the use of off-the-shelf networking and computing, combined with unattended operation of a myriad of devices, provides new opportunities for malicious entities to launch attacks on cyber-physical systems. The Stuxnet malware, which is discovered in 2010, provides a clear sample of the future to come.

Designing a secure CPS is challenging due to the fact that an intelligent attacker can potentially design its strategy almost arbitrarily, to optimally exploit both cyber and physical vulnerabilities of the system and cause maximal damage. Information security, while being successful in securing cyber systems, cannot provide complete protection of CPS due to the lack of physical system model. On the other hand, system-theory based techniques, such as fault detection and robust control, are developed to protect the system against random and ``benign'' failures and disturbances and hence are not well-equipped to defend against an intelligent adversary.

The goal of this proposal is to develop control algorithms for CPS that functions in adversarial environment. We are planning to address the following aspects of the problem: i) how to estimate the state of the system from potentially compromised sensory data?; ii) how to design control algorithms that enable intrusion detection and isolation?; iii) how to identify the critical components of the CPS? Finally, we are planning to demonstrate the proposed methodology on a power system testbed.  



\newpage
\section{Objectives}

{\bf Project Goals on Objectives} The overarching goal of this project is to develop systematic tools for designing secure control algorithm of CPS that can work even in adversarial environment. As specific objectives, we seek to develop:
\begin{enumerate}
  \item \emph{Secure state estimation algorithms} for CPS with compromised sensory data. The state estimator should be able to tolerate up to a pre-specify percentage of compromised sensory data and remain stable. 
  \item \emph{Secure control algorithms} that enable and improve the system's ability to detect and isolate the malicious devices.
  \item \emph{Risk assessment tools} for analyzing the vulnerabilities of the system and providing insights on which devices are critical to the operation of the system. Therefore, we could best invest our security effort on the critical nodes.
  \item \emph{Demonstrations} of our methodology via power grid simulation software or real CPS testbeds.
\end{enumerate}

\section{Why Singapore?}

The School of Electrical and Electronic Engineering at Nanyang Technological University has a very strong system and control group, with whom I would like to collaborate in the future. Furthermore, there are many professors in EEE working on power systems, upon which I am interested to demonstrate my theoretical results. Beyond the school of Electrical and Electronic Engineering, there are people working on related fields, such as information security and game theory, which I believe will be essential for CPS security. Finally, I feel that my work can find many practical and industrial applications in Singapore (for example, in power grids, oil refinement industry and transportation networks.).

\section{Background and Significance}

Cyber-Physical Systems (CPS) refer to the embedding of sensing, networking, computation and control into physical spaces with the goal of making them efficient, reliable and ``smart''. Driven by the miniaturization and integration of sensing, communication and computation in cost effective devices, CPSs have the potential to radically transform industries such as aerospace, transportation, built environments, energy, healthcare and manufacturing, to name a few. However, the opportunities afforded by CPS are accompanied with even greater challenges. Currently, most CPSs use dedicated communication networks, which have so far sheltered systems from the outside world. However, the next generation of smarter CPSs, such as smart grids, vehicle ad-hoc networks, smart buildings, will make extensive use of off-the-shelf networking and computing. This combined with unattended operation of a myriad of devices, provides new entry points for malicious entities to inject attacks on CPSs. A wide variety of motivations exists for launching an attack on CPSs, ranging from financial gains (stealing electricity without paying the bill), all the way to terrorism and cyber-warfare (threatening an entire population by manipulating vital infrastructure). Any attack on safety-critical CPSs, such as power grids, may significantly hamper the economy and even lead to the loss of human life. 

The recent Stuxnet incident should serve as a clear warning of the damage possible. Stuxnet is a complex malware that infected uranium enrichment facilities in Iran, reportedly causing damage to approximately 1,000 centrifuges at these plants~\cite{Fidler2011}. The Stuxnet malware intercepts all input and output signals of the centrifuge. To cause damage, Stuxnet implements control logic to either cause pressure in centrifuges to increase or change the rotational speed. To prevent detection, Stuxnet also delivers previously recorded sensory data to the SCADA system, performing a replay attack~\cite{Langner2013}. As a result, the system operator would still think that the system is operating normally. The block diagram of the malware is illustrated in Fig~\ref{fig:stuxnet}.

The goal of this proposal is to develop analysis and design tools that enable us to design control algorithms for cyber-physical systems that functions even in adversarial environment.

\begin{figure}[ht]
    \begin{minipage}[t]{0.5\textwidth}
      \inputtikz{replaydiagramone}
      \subcaption{Recording Phase}
    \end{minipage}
    \begin{minipage}[t]{0.5\textwidth}
      \inputtikz{replaydiagramtwo}
      \subcaption{Replay Phase}
    \end{minipage}
  \caption{The block diagram for the Stuxnet malware. (a) Recording Phase. The malware records a sequence of sensory data without interfering the normal system operation. (b) Replay Phase. The malware injects its own control signal to the system, while replaying the previous recorded sensory data to the state estimator.}
  \label{fig:stuxnet}
\end{figure}


\subsection{Related Work}
There is substantial work in many different fields that try to address the security or similar aspect of CPS. We provide here a short analysis of some of the primary areas of overlap, with the goal of highlighting some methodologies and tools that we hope to leverage and improve upon from these areas. 

\emph{Information Security:} Information security seeks to protect the confidentiality, integrity and availability of information~\cite{Whitman2011}. Tools such as secure communication protocol (e.g., SSL/TLS, IPsec, SSH), software attestation~\cite{Seshadri2004} and tamper resistant devices~\cite{Smith1999} can help to secure the software and hardware of different devices of the CPS and the communications between them. However, information security is not well-equipped to protect the system from physical attacks (e.g., bypassing a power meter using a wire to steal electricity). Furthermore, it is difficult to guarantee the security for every single device due to various reasons such as legacy firmware and zero-day attacks. As a result, we believe that we should not rely solely on information security for CPS security. Instead, information security should be used to ensure that \emph{most} of the system are functional, while a secure control algorithm should be able to tolerate the rest of the compromised nodes.

\emph{Robust Estimation and Control:} Robust estimators have also been extensively studied in the literature~\cite{robust2009,Kassam1985,robust2006}. However, such kinds of approaches typically assume that the outliers of the data are generate \emph{independently} by some other probability distribution different from the model assumptions. For control systems, techniques such as $\mathcal H_\infty$ loop shaping~\cite{ZhouDoyle} have also been an active research area for the past decades. The $\mathcal H_\infty$ controller can be seen as the worst-case controller when the disturbance has \emph{bounded} energy or power spectral density. On the other hand, an intelligent attacker could potentially design its strategy to optimally exploit the vulnerabilities of the system to cause maximal damage. Thus, the ``independence'' or ``boundedness'' assumptions may not hold for an intelligent adversary and the robust techniques need to be reexamined in the context of CPS security. 

\emph{Fault Detection and Identification:} The control community has a long history of research in fault detection and identification in dynamical systems~\cite{Willsky:1975bp}. For static estimation problem, bad data detection and identification techniques, which is based on identifying and truncating the ``atypical'' data, have been widely used in large scaled systems such as power grid~\cite{Abur2004}. While such approaches are very successful in detecting and removing random failures, it has been shown that they are not effective against attacks~\cite{liu2009,henrik2010}.

\emph{Secure Control:} The area of secure control systems has emerged in the last 5 years. A substantial amount of effort~\cite{Mo:2009bj,Pasqualetti2013,wirelesscontrol,Fawzi2012, liu2009} has been dedicated to the detection and identification of malicious behaviors in dynamical systems. These researches provide the fundamental limitation on what can be achieve by a control algorithm in adversarial environment.


\section{Technical Approach}

\subsection{Secure Estimation}
A starting point for designing a secure state estimator is to define the system model and the adversary model. In this project, we plan to consider both a static system model, where the state estimation is purely based on the sensory data made at current time, as well as a dynamical system model, where the state estimation is based on the whole history of sensor measurements.

The state estimation algorithm is essential for designing control algorithm over CPS. It also serves as a fundamental building block for many other applications in CPSs, such as the real-time electricity market of the power grids~\cite{Xie2011}. As a result, the security of the state estimator is of utmost importance for the security of CPS. On the other hand, as is seen in the Stuxnet example, a compromised state estimator enables the adversary to cause severe damage to the system. 

The goal of this proposal is thus to develop a secure state estimation algorithm for CPS in adversarial environment with potentially compromised sensory data, as we believe that a secure state estimator will play a central role in the future research of CPS security.


We first describe the static estimation problem, which is essential for power system state estimation~\cite{Abur2004}. We assume that the state of the system can be described by an $n$-dimensional vector $x\in \mathbb R^n$ and $m$ sensors are measuring the system. The sensory data is an $m$-dimensional vector $y \triangleq [y_1,\dots,y_m]'\in \mathbb R^m$, with $y_i$ being the measurement made by sensor $i$. We assume that $x$ and $y$ satisfies the following linear relationship:
\begin{displaymath}
 y = Cx + v, 
\end{displaymath}
where $v\in \mathbb R^m$ characterizes the measurement noise. We assume that an adversary has the ability to manipulate up to $l$ of the $m$ sensor measurements. Formally, this means that our estimate has to rely on a vector $y^c\in\mathbb R^m$ of \emph{manipulated measurements} defined by
\begin{displaymath}
  y^c = y + y^a,
\end{displaymath}
where the \emph{bias} vector $y^a$ is a sparse vector with no more than $l$ non-zero entries. The state estimator $f: \mathbb R^m\rightarrow\mathbb R^n$ can be seen as a mapping which maps a manipulated measurement to a state estimate $\hat x = f(y+y^a)$. The goal of the attacker is to increase the Mean Squared Error (MSE) $P \triangleq \mathbb E(x - \hat x)(x - \hat x)'$ and the goal of the estimator is to reduce the MSE. We further assume that the attacker knows the exact estimator used by the system. Therefore, the secure estimator design problem becomes a mini-max problem, where one seek to minimize the estimation error against the ``worst'' sensory data manipulation.

A special case of this problem, where the state $x\in \mathbb R$ is a scalar, has been solved in our previous work~\cite{Mo2013a}. The optimal attack strategy is to choose $y^a$ to either maximize $\hat x$ or minimize $\hat x$ depending on the true state $x$. For the optimal estimator design, there exist two cases: 
\begin{enumerate}
  \item If at least half the measurements ($l\geq m/2$) are compromised, then the adversary effectively renders the sensory data useless and the secure estimator should ignore all measurements and be based solely on the a-priori information on $x$. This result illustrates the fundamental limitation on the secure estimator design in the sense that no ``useful'' state estimation can be made if half of the sensors are compromised.
  \item If less than half the measurements ($l<m/2$) are compromised, then the  optimal secure estimator can be computed as the solution of a convex optimization problem. 

\end{enumerate}

There are still many open problems for us to solve before we can generalize our result to multi-dimensional case. First, we must identify the optimal attacker's strategy in multi-dimensional case and then design the optimal secure estimator accordingly. Another problem for the multi-dimensional case is to identify the observable subspace. For one-dimensional case, it is clear that the state is observable unless the adversary compromised no less than half of the sensory data. On the other hand, for multi-dimensional case, it is possible that the adversary can render some states or some linear combinations of the states unobservable to the system by only compromising a small number of sensors~\cite{henrik2010}. As a result, we may only be able to estimate $Qx$ instead of $x$, where the matrix $Q$ indicates the observable space. 


We now describe the dynamical state estimation problem of CPS. We adopt the following discrete time linearized model of the CPS:
\begin{displaymath}
  x(k+1) = A x(k)+ Bu(k)+ w(k),
\end{displaymath}
where $x(k)$ , $u(k)$ and $w(k)$ are the state, control input and process noise at time $k$ respectively. Similar to the previous case, we assume that $m$ sensors are measuring the system. The sensory data is given by vector $y(k) \triangleq [y_1(k),\dots,y_m(k)]'\in \mathbb R^m$, with $y_i(k)$ being the measurement made by sensor $i$ at time $k$. We assume that an adversary has the ability to manipulate up to $l$ of the $m$ sensor measurements. Hence, the \emph{manipulated measurements} received by the estimator at time $k$ is given by 
\begin{displaymath}
 y^c(k) = Cx(k) + v(k) + y^a(k), 
\end{displaymath}
where $v(k)$ is the measurement noise and $y^a(k)$ is a sparse vector with less than $l$ non-zero entries. Furthermore, we assume that the set of the compromised sensors remains constant over time.

A starting point for the architecture of the secure estimator we plan to explore is illustrated in Figure~\ref{fig:estimator}. The estimator consists of a static estimator, as is discussed in the previous subsection, and a Kalman filter. The static estimator will first process the manipulated measurement $y(k)$ to generate an static state estimate $\hat \xi(k) = x(k) + \mu(k)$, where $\mu(k)$ denotes the estimation error of the static estimator. If the state $x(k)$ is not reconstructable using $y(k)$, then the static estimator generates $\hat \xi(k) = Qx(k)+\mu(k)$ where $Q$ indicates the one-step observable subspace of $x(k)$. A Kalman filter, whose gain is chosen according to the $Q$ matrix and the statistics of $\mu(k)$, will take the static state estimate $\hat \xi(k)$ as an input to generate the dynamical state estimate $\hat x(k)$.

\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick}]
      \node (y) at (0,0) {$y^c(k)$};
      \node (static) [box] at (3,0) {Static Estimator};
      \node (kf) [box] at (8,0) {Kalman Filter};
      \node (hatx)  at (11,0) {$\hat x(k)$};
      \draw [->,semithick] (y)--(static);
      \draw [->,semithick] (static)--node[above]{$\hat \xi(k)$}(kf);
      \draw [->,semithick] (kf)--(hatx);
    \end{tikzpicture}
  \end{center}
  \caption{A candidate architecture for secure dynamical estimator. The manipulated measurements is first being processed by a static estimator, the state estimate $\hat\xi(k)$ from which is then sent into the Kalman filter.} 
  \label{fig:estimator}
\end{figure}

There are several open problems concerning the candidate architecture. First, it is not clear how to design the Kalman gain since the static estimation error vector $\mu(k)$ can potentially be manipulated by the adversary and hence it is not necessarily Gaussian or even stochastic. If it turns out that $\mu(k)$ is bounded, then we may be able to use an $\mathcal H_\infty$ type of estimator instead of a Kalman filter to guarantee bounded estimation error. Moreover, it would be very useful to derive the fundamental limitations on what is achievable for a secure dynamical estimator and to characterize the conservativeness of the candidate architecture.
\subsection{Secure Control}

\subsection{Risk Assessment}

\section{Milestones and Deliverables}
\begin{itemize}
  \item {\bf Year 1} 
    \begin{itemize}
      \item{\bf Theory:} Develop theory for secure state estimation for
      \item{\bf Platform and data:} Create a dynamical model of airplane electrical system testbed. Identify possible security breaches on the system.
    \end{itemize}
  \item {\bf Year 2} 
    \begin{itemize}
      \item{\bf Theory:} Extend the abstraction algorithm to cover all systems of interest (e.g., hybrid systems, systems with non-linear dynamics). Solve the control logic synthesis problem. Investigate the causality of the synthesized controller. 
      \item{\bf Platform and data:} Implement and test the  algorithms on the power system testbed. 
    \end{itemize}
  \item {\bf Year 4} 
    \begin{itemize}
      \item{\bf Theory:} Wrap up open problems and identify possible future directions.
      \item{\bf Platform and data:} Create a more realistic power system test bed and test our tools on the evolved testbed. If possible, apply the new abstraction and synthesis tools on robotics platforms.
    \end{itemize}
\end{itemize}
\newpage
\setcounter{page}{1}
\bibliographystyle{plain}
\bibliography{bib}
\end{document}
