\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\diag}{diag}
\DeclareMathOperator{\logdet}{log\;det}

%define comments:
\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comments}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comments}[1]{}
\fi

\title{The Solution to Infinite Horizon LQR}

\author{Yilin Mo}

\begin{document} \maketitle

Consider an LTI system that satisfies the following equation:
\begin{align*}
 x(k+1) = Ax(k) + B u(k),\, x(0) = x_0.
\end{align*}
where we assume that $x(k)\in \mathbb R^n$ and $u(k)\in \mathbb R^m$. 
\begin{align*}
  B = \begin{bmatrix}
    I_{m\times m}\\
    0_{(n-m)\times m}
  \end{bmatrix}.
\end{align*}

\comments{Yilin: I think this is the same problem setup as in your paper ``Controllability Metrics, Limitations and Algorithms for Complex Networks''. Since you can relabel the nodes, you can make the first $m$ nodes to be the direct controllable nodes.}

Now consider the following infinite horizon LQR problem:
\begin{align}
  J = \limsup_{T\rightarrow\infty}\min_{u(0),\dots,u(k)}\left( \sum_{k=0}^T x(k)^Tx(k) + \beta u(k)^Tu(k)\right).
  \label{eq:infinitelqg}
\end{align}
where $\beta \geq 0$ is the weight on the control input.

\comments{Yilin: In general, the LQR objective function is $x^TWx + u^TUu$. Here, I just assume that $W$ and $U$ to be identity matrices. It will take some effort to generalize the result to arbitrary $W$ and $U$, but I think it is doable. Although the result will not be very intuitive.}

It is well known if $(A,B)$ is stabilizable, then the optimal control law of \eqref{eq:infinitelqg} is given by
\begin{align*}
  u(k) = -(\beta I + B^TSB)^{-1}B^TSA x(k),
\end{align*}
where $S$ is the unique positive semidefinite fixed point of the following Riccati equation
\begin{equation}
  S = g(S) = A^TSA+I- A^TSB(B^TSB+\beta I)^{-1}B^TSA.
  \label{eq:riccati}
\end{equation}

The optimal $J$ is given by
\begin{align*}
 J = x_0^TSx_0. 
\end{align*}

The goal of this draft is to characterize the solution \eqref{eq:riccati}. To this end, let us first consider the following set\footnote{All the comparison between symmetric matrices are in the positive semidefinite sense.}
\begin{align}
  \mathcal S \triangleq\{X\geq 0:X \geq g(X)\}.
  \label{eq:relax}
\end{align}

Clearly, $S\in\mathcal S$. The following lemma proves that $S$ is in fact the minimum of the set $\mathcal S$:
\begin{lemma}
  For any $X\in \mathcal S$, the following inequality holds:
  \begin{align*}
   S\leq X. 
  \end{align*}
  \label{lemma:fixpointminimum}
\end{lemma}
\begin{proof}
  We know that the Riccati equation $g(S)$ is monotonically non-increasing. As a result, $X\geq g(X)$ implies that
  \begin{align*}
    X\geq g(X)\geq g^{(2)}(X)\geq \dots\geq   g^{(k)}(X)\geq\dots\geq 0
  \end{align*}
  Since $g^{(k)}(X)\geq 0$ is non-increasing in $k$, the following limit is well defined
  \begin{align*}
    X_* = \lim_{k\rightarrow\infty}g^{(k)}(X)\leq X.
  \end{align*}
  Clearly, $X_* = g(X_*)$. Now by the uniqueness of \eqref{eq:riccati}, $X_* = S$ and hence $S\leq X$.
\end{proof}

We will now rewrite the inequality $X\geq g(X)$ as a linear matrix inequality. To this end, let us take the inverse on both side of $X\geq g(X)$ and use matrix inversion lemma, which leads to the following inequality:
\begin{align*}
  X^{-1}\leq I - A^T\left(X^{-1} +AA^T + \beta^{-1}BB^T  \right)^{-1}A.
\end{align*}
Use Schur complement, the above inequality can be rewritten as
\begin{align}
  \begin{bmatrix}
    X^{-1}+AA^T+\alpha BB^T&A\\
    A^T&I-X^{-1}
  \end{bmatrix}\geq 0,
  \label{eq:LMI}
\end{align}
where $\alpha = \beta^{-1}$. First 


Now if we assume that $A$ is invertible \comments{(Yilin: I think if it is not invertible, we can do a decomposition and only work on the invertible part.)}, then \eqref{eq:LMI} is equivalent to
\begin{align}
  \begin{bmatrix}
    A^{-1}&0\\
0&I
  \end{bmatrix}\begin{bmatrix}
    X^{-1}+AA^T+\alpha BB^T&A\\
    A^T&I-X^{-1}
  \end{bmatrix}  \begin{bmatrix}
    A^{-T}&0\\
0&I
  \end{bmatrix} = \begin{bmatrix}
    A^{-1}X^{-1}A^{-T}+I+\alpha A^{-1}BB^TA^{-T}&I\\
    I&I-X^{-1}
  \end{bmatrix} \geq 0,
  \label{eq:LMI2}
\end{align}
 
\end{document}

