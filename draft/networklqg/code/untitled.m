n = 5;
m = 3;
S = eye(n);
A =rand(n)
index = randperm(n);

B = zeros(n,m);
for i = 1:m
    B(index(i),i) = 1;
end

W = eye(n);
U = eye(m);
for i=1:100
    S = A'*inv(inv(S)+B*B')*A+W;
end

cvx_begin sdp
variable T(n,n) symmetric
maximize (trace(T))
subject to
    T >= 0 
    [T+A*A'+B*B' A;A' eye(n)-T] >=0 ;
%    [B*B' zeros(n);zeros(n) eye(n)] + [T zeros(n);zeros(n) zeros(n)] - [-A;eye(n)]*T*[-A' eye(n)] >= 0
cvx_end
T