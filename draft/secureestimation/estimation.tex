%\documentclass[10pt,journal,letterpaper]{IEEEtran}
%double column, 9pt, for final publication of technical note 
%\documentclass[9pt,technote,letterpaper]{IEEEtran}
%single column double space, for journal draft
\documentclass[12pt,draftcls,onecolumn]{IEEEtran}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{url}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\, min\;}     % argmin
\DeclareMathOperator*{\argmax}{arg\, max\;}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\newcommand{\Ic}{{\mathcal I^c}}
\newcommand{\I}{{\mathcal I}}
\newcommand{\J}{{\mathcal J}}
\newcommand{\K}{{\mathcal K}}
\newcommand{\ts}{{\tilde s}}
\newcommand{\tV}{{\tilde V}}

\newcommand{\eq}{&=&}
\newcommand{\BEQ}{\begin{eqnarray*}}
\newcommand{\EEQ}{\end{eqnarray*}}
\newcommand{\BDEF}{\begin{definition}}
\newcommand{\EDEF}{\end{definition}}
\newcommand{\BTHM}{\begin{theorem}}
\newcommand{\ETHM}{\end{theorem}}
\newcommand{\BLEM}{\begin{lemma}}
\newcommand{\ELEM}{\end{lemma}}
\newcommand{\BPF}{\begin{proof}}
\newcommand{\EPF}{\end{proof}}

\newcommand{\alert}[1]{\textcolor{red}{#1}}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi

\title{Secure $\mathcal H_\infty$-type Estimation and Control}

\author{Yorie Nakahira$^*$, Yilin Mo$^*$, Richard M. Murray$^*$
\thanks{$*$: Yorie Nakahira, Yilin Mo and Richard M. Murray are with the Control and Dynamical Systems Department of the California Institute of Technology. Email: {ynakahir@caltech.edu, yilinmo@caltech.edu, murray@cds.caltech.edu}%
}
\thanks{This work is supported in part by IBM and UTC through iCyPhy consortium. }%
}

\begin{document} \maketitle
\begin{abstract}
  abc
\end{abstract}

\section{Introduction}
\label{sec:intro}
\subsection*{Notations}
$\mathbb N$ is the set of non-negative integers. $\mathbb N^+$ is the set of positive integers. For the set $\mathbb N\bigcup \{\infty\}$, we will assume that $\infty = \infty$ and $\infty > a$, for any $a\in \mathbb N$. Finally, for any $a,b\in \mathbb N\bigcup \{\infty\}$, $a \wedge b = \min(a,b)$ and $a\vee b = \max (a,b)$. 

Let $v\in \mathbb R^n$ be a vector, then $\|v\|$ is the 2-norm of $v$. $\|v\|_0$ is the zero ``norm'' of $v$, i.e., the number of non-zero entries of $v$. 

Consider an infinite sequence $\{x(k)\}_{k\in \mathbb N}$, where each $x(k)\in \mathbb R^n$. With slightly abuse of notation, we define the $l_2$ norm of $\{x(k)\}_{k\in\mathbb N}$ as
  \begin{displaymath}
   \|x\| \triangleq  \sum_{k=0}^\infty \|x(k)\|^2 .
  \end{displaymath}

  The space of $l_2$ is defined as the space of infinite sequences with finite $l_2$ norm.

  The $l_0$ ``norm'' $\|x\|_0$ of an infinite sequence $\{x(k)\}_{k\in\mathbb N}$ is defined as the cardinality of the following set:
  \begin{displaymath}
    \{i:\exists k,\text{ such that }x_i(k)\neq 0\} .
  \end{displaymath}
\section{Preliminary}
A ball $B(x,r) \subset \mathbb R^n$ is defined as 
\begin{displaymath}
  B(x,r)  \triangleq \{x'\in \mathbb R^n: \|x'-x\|_2\leq r\}.
\end{displaymath}
Consider a set $S\subseteq \mathbb R^n$. A ball $B(x,r)$ covers $S$ if and only if $S\subseteq B(x,r)$. For any point $x\in \mathbb R^n$, define
\begin{displaymath}
  \rho(x,S) \triangleq \inf\{r\in \mathbb R^+:\,S\subseteq B(x,r)\}
\end{displaymath}
We will assume that the infimum over an empty set is $\infty$. Hence, if $S$ is unbounded, then $\rho(x,S) = \infty$ for any $x$.

For a bounded set $S$, define the radius $r(S)\in \mathbb R^+$ and Chebyshev center $c(S)\in \mathbb R^n$ of $S$ to be
\begin{displaymath}
  r(S) \triangleq \inf_{x\in \mathbb R^n} \rho(x,S), c(S) \triangleq \argmin_{x\in \mathbb R^n} \rho(x,S).
\end{displaymath}
In an essence, $B(c(S),r(S))$ is the smallest radius ball that covers $S$. Notice that $c(S)$ may \emph{not} necessarily belong to $S$. For an unbounded $S$, we define $r(S) = \infty$.

We further define the diameter $d(S)$ of the set $S$ as
\begin{displaymath}
  d(S) \triangleq \sup_{x\in \mathbb S} \rho(x,S).
\end{displaymath}

Notice that in general $d(S)\neq 2r(S)$. For example, for a equilateral triangle with side length 1. Then $d(S) = 1$, while $r(S) = 1/\sqrt{3}$. In general, the following relation between $r(S)$ and $d(S)$ holds:

\begin{theorem}
  Let $S\subset R^n$ be a non-empty and bounded set. Then the following inequality on $r(S)$ and $d(S)$
      \begin{displaymath}
	\frac{d(S)}{2}\leq r(S)\leq\sqrt{\frac{n}{2n+2}}d(S)\leq \frac{1}{\sqrt{2}}d(S).
      \end{displaymath}
  \label{theorem:Jung}
\end{theorem}
\begin{proof}
  The first and third inequality is trivial. The second inequality is from Jung's theorem~\cite{Danzer1963}.
\end{proof}
\section{Static Estimation}
\label{sec:static}
\subsection{Problem Formulation}
Assume that the state of the system $x \in \mathbb R^n$ and $m$ sensors are measuring the system:
\begin{displaymath}
  y_i = h_i x + w_i + a_i, 
\end{displaymath}
where $h_i$ is a row vector and $w_i$ is the measurement noise. $a_i$ is the bias injected by the attacker, such that $a_i\neq 0$ only when the sensor $i$ is compromised. Define $\mathcal S \triangleq \{1,\dots,m\}$ as the set of sensors. Denote
\begin{displaymath}
  y \triangleq \begin{bmatrix}
    y_1\\
    \vdots\\
    y_m
  \end{bmatrix} ,\, H \triangleq \begin{bmatrix}
    h_1\\
    \vdots\\
    h_m
  \end{bmatrix} ,\, w \triangleq \begin{bmatrix}
    w_1\\
    \vdots\\
    w_m
  \end{bmatrix} ,\,a \triangleq \begin{bmatrix}
    a_1\\
    \vdots\\
    a_m
  \end{bmatrix} 
\end{displaymath}
As a result,
\begin{displaymath}
  y = Hx + w + a. 
\end{displaymath}
In this draft, we consider a more general sensor model, defined as
\begin{equation}
  y = Hx + Gw + a,
  \label{eq:sensormodel}
\end{equation}
where $G\in \mathbb R^{m\times m}$ and is assumed to be full row rank. We further assume that the sensor noise $w \in \mathbb R^p$ is 2-norm bounded, i.e.,
\begin{displaymath}
  \|w\|_2\leq \delta. 
\end{displaymath}
We assume that the bias vector $a$ is 0-norm bounded, i.e.,
\begin{displaymath}
  \|a\|_0\leq l, 
\end{displaymath}
where $l$ indicates the number of compromised sensors.

Let us define the set $\mathbb Y$ as the set of all possible measurements, i.e.,
\begin{displaymath}
  \mathbb Y  \triangleq \{y\in \mathbb R^m:\exists \,x,\,w,\,a,\text{ such that }\|w\|_2\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
For any $y\in \mathbb Y$, we can define the set $\mathbb X(y)$ as the set of feasible $x$ that can generate $y$, i.e.,
\begin{displaymath}
  \mathbb X(y) \triangleq \{x\in\mathbb R^n:\exists \,w,\,a,\text{ such that }\|w\|_2\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
An estimator is a function $f:\mathbb Y\rightarrow \mathbb R^n$, where $\hat x = f(y)$. The magnitude of the worst case estimation error is defined as
\begin{displaymath}
  e(f) \triangleq \sup_{y\in \mathbb Y} \rho(f(y),\mathbb X(y)) .
\end{displaymath}

Clearly, from the definition of the Chebyshev center, we know that the optimal estimator with smallest $e(f)$ is given by
\begin{displaymath}
  f^*(y) \triangleq c(\mathbb X(y)). 
\end{displaymath}
The worst case error magnitude is thus given by
\begin{displaymath}
  e(f^*)\triangleq \sup_{y\in\mathbb Y} r(\mathbb X(y)).
\end{displaymath}

In the following subsections, we provides an upper and lower bound for $e(f^*)$. We further derive the optimal estimator. 
\subsection{Performance Bounds for the Optimal Estimator}
This subsection is devoted to analyzing the performance of the optimal estimator. To this end, for any index set $\mathcal I = \{i_1,\dots,i_j\}$, let us define subspace $\mathcal V_\mathcal I \triangleq \text{span}(e_{i_1},\dots,e_{i_j})\subseteq \mathbb R^m$, where $e_{i}\in \mathbb R^m$ is the $i$th canonical basis vector. Define the following set:
\begin{displaymath}
  \mathbb X_{\mathcal I}(y) \triangleq \{x\in\mathbb R^n:\exists \,w,\,a\in \mathcal V_\Ic,\text{ such that }\|w\|_2\leq \delta\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
Hence, $\mathbb X_{\mathcal I}(y)$ represents all possible states that can generate measurement $y$ when the sensors in $\mathcal I$ are good and the sensors in $\mathcal I^c$ are compromised. Thus
\begin{displaymath}
  \mathbb X(y)  = \bigcup_{|\mathcal I|=m-l}\mathbb X_{\mathcal I}(y). 
\end{displaymath}
For any $\mathcal I = \{i_1,\dots,i_j\}$, we can define 
\begin{displaymath}
  H_{\mathcal I} \triangleq \begin{bmatrix}
    h_{i_1}\\
    \vdots\\
    h_{i_j}
  \end{bmatrix},\, G_{\mathcal I} \triangleq \begin{bmatrix}
    g_{i_1}\\
    \vdots\\
    g_{i_j}
  \end{bmatrix},\,y_{\mathcal I}\triangleq \begin{bmatrix}
    y_{i_1}\\
    \vdots\\
    y_{i_j}
  \end{bmatrix},
\end{displaymath}
where $g_i$ is the $i$th row vector of $G$. If $H_{\mathcal I}$ is full column rank, then we define
\begin{align*}
  \Xi_{\mathcal I} &\triangleq G_IG_I^T,\,&
  K_{\mathcal I}&\triangleq \left(H_{\mathcal I}^T\Xi_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1}H^T_{\mathcal I}\Xi_{\mathcal I}^{-1},\\
  P_{\mathcal I} &\triangleq \left(H_{\mathcal I}^T \Xi_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1},\, &
  U_{\mathcal I} &\triangleq (I-H_{\mathcal I}K_{\mathcal I})^T \Xi_{\mathcal I}^{-1}(I-H_{\mathcal I}K_{\mathcal I}).
\end{align*}
The following theorem provides bounds on $e(f^*)$:
\begin{theorem}
  If there exists $|\mathcal K| =m-2l$, such that $H_{\mathcal K}$ is not of full column rank, then $e(f^*) = \infty$. If for all $\mathcal K = m-2l$, $H_{\mathcal K}$ is full column rank, then for all possible $y\in \mathbb Y$, we have
  \begin{equation}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) = 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
    \label{eq:diameter}
  \end{equation}
  Therefore, $e(f^*)$ satisfies
  \begin{equation}
    \max_{|\mathcal K| = m-2l}\delta\sqrt{\sigma(P_{\mathcal K})}\leq e(f^*)\leq \max_{|\mathcal K| = m-2l}\delta\sqrt{2\sigma(P_{\mathcal K})},
    \label{eq:radius}
  \end{equation}
  where $\sigma(P)$ is the spectral radius of $P$.
\end{theorem}
\begin{proof}
  Let us consider a pair of set $\mathcal I,\,\mathcal J$ with cardinality $m-l$. Define $\mathcal K$ as
  \begin{displaymath}
   \mathcal K =  \mathcal I\bigcap \mathcal J .
  \end{displaymath}
  Clearly, $|\mathcal K|\geq m-2l$. Now for any point $x_1 \in \mathbb X(y,\mathcal I)$ and $x_2 \in \mathbb X(y,\mathcal J)$, we have:
  \begin{equation}
    Hx_1 +G w_1 + a_1 = H x_2 + Gw_2 + a_2 = y,  
    \label{eq:difference1}
  \end{equation}
  where $a_1\in \mathcal V_\Ic$ and $a_2\in \mathcal V_{\mathcal J^c}$. Thus, \eqref{eq:difference1} implies that
  \begin{equation}
    H_{\mathcal K}(x_1-x_2)= G_{\mathcal K}(w_2-w_1),
    \label{eq:difference2}
  \end{equation}

  Hence, if $H_{\mathcal K}$ is of full column rank, then by the fact that $\|w_2-w_1\|_2\leq 2\delta$, we have
  \begin{displaymath}
    \sigma(P_{\mathcal K})^{-1}\|x_1-x_2\|^2 \leq (x_1-x_2)^T P_{\mathcal K}^{-1} (x_1-x_2)^T \leq 4\delta^2.
  \end{displaymath}

  Hence, $\|x_1-x_2\|_2\leq 2\sqrt{\sigma(P_{\mathcal K})}$. It is easy to prove that for any $\mathcal K_1\subseteq \mathcal K_2$,
  \begin{displaymath}
    \sigma(P_{\mathcal K_1})\geq \sigma(P_{\mathcal K_2}).
  \end{displaymath}
  Therefore,
  \begin{displaymath}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) \leq 2\delta\max_{|\mathcal K| \geq m-2l}\sqrt{\sigma(P_{\mathcal K})} = 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
  \end{displaymath}
  Now we need to prove that the equality of \eqref{eq:diameter} holds. Suppose that we find $x_1,\,x_2,\,w_1,\,w_2$ that satisfies \eqref{eq:difference1} and $\|x_1-x_2\| = 2\sqrt{\sigma(P_{\mathcal K})}$. We know that
  \begin{displaymath}
    H_{\mathcal K} x_1 + G_{\mathcal K}w_1 =  H_{\mathcal K} x_2 + G_{\mathcal K}w_2.
  \end{displaymath}
  Therefore, let us create $y$, such that
  \begin{displaymath}
    y_{\mathcal K} = H_{\mathcal K}x_1,\,y_{\mathcal I^c} = H_{\mathcal I^c}x_2,\,y_{\mathcal J^c\backslash\mathcal I^c} = H_{\mathcal J^c\backslash\mathcal I^c}x_1.
  \end{displaymath}
  Thus, $x_1\in \mathbb X_{\mathcal I}(y)$ and $x_2\in \mathbb X_{\mathcal J}(y)$, which implies that \eqref{eq:diameter} holds. \eqref{eq:radius} can be proved by applying Theorem~\ref{theorem:Jung}.
\end{proof}
\subsection{Estimator Design}
In this subsection, we first characterize the shape of $\mathbb X_{\mathcal I}(y)$:
\begin{theorem}
  Define the function $V_{\I}(x):\mathbb R^n\rightarrow \mathbb R$ as the solution of the following optimization problem:
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w\in \mathbb R^m}&
    & \|w\|^2\nonumber\\
    &\textrm{subject to}&
    & G_\I w= y_{\I}-H_\I x.  \label{eq:optstatic}
  \end{align}
  Then $V_\I(x)$ is given by
  \begin{equation}
    V_\I(x) = (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))+ \varepsilon_{\I}(y) ,
    \label{eq:valuefunctionstatic}
  \end{equation}
  where
  \begin{equation}
     \hat x_{\I}(y) = K_\I y_\I,
    \label{eq:optstaticsolution}
  \end{equation}
  and 
  \begin{equation}
    \label{eq:optstaticvalue}
    \varepsilon_{\I}(y) = y_{\I}^TU_{\I}y_{\I}.
  \end{equation}
  \label{theorem:valuefunctionstatic}
\end{theorem}
\begin{proof}
  Consider the constraint of the optimization problem~\eqref{eq:optstatic}
  \begin{equation}
    y_{\I} - H_{\I}x = G_{\I}w.
    \label{eq:reducedequation}
  \end{equation}
  As $G$ is full row rank, $G_{\I}$ is also full row rank. Consider the singular value decomposition of $G_\I$, we get
  \begin{displaymath}
    G_\I = Q_1 \begin{bmatrix} 
      \Lambda & \mathbf 0
    \end{bmatrix} Q_2,
  \end{displaymath}
  where $Q_1,Q_2$ are orthogonal matrices with proper dimensions and $\Lambda$ is an invertible and diagonal matrix. Hence, \eqref{eq:reducedequation} is equivalent to 
  \begin{equation}
    \Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I \hat x_\I(y_\I) = \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation2}
  \end{equation}
  where $v = Q_2 w$ and $\|v\|  = \|w\|$. By projecting $\Lambda^{-1} Q_1^T y_\I$ into the subspace $\text{span}(\Lambda^{-1}Q_1^T H_\I)$, we have
  \begin{equation}
    \left[\Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I \hat x_{\I}(y)\right] + \Lambda^{-1} Q_1^T H_\I \left[x - \hat x_{\I}(y) \right]= \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation3}
  \end{equation}
  The first term on the LHS of \eqref{eq:reducedequation3} is perpendicular to the second term. Thus, \eqref{eq:reducedequation3} is equivalent to
  \begin{displaymath}
   \varepsilon_\I(y) +  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))= \left\|\begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v\right\|^2 \leq \|v\|^2 = \|w\|^2.
  \end{displaymath}
  Clearly, the equality holds when $v = \begin{bmatrix}v_1,\dots,v_{|\I|},0,\dots,0\end{bmatrix}$. Hence
  \begin{displaymath}
    V_{\I}(x)= \varepsilon_\I(y) +  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y)).
  \end{displaymath}
\end{proof}

By Theorem~\ref{theorem:valuefunctionstatic}, we immediately have the following corollary:
\begin{corollary}
  If $\varepsilon_\I(y) > \delta^2$, then $\mathbb X_{\I}(y)$ is an empty set. Otherwise, $\mathbb X_{\I}(y)$ is an ellipsoid given by
  \begin{equation}
    \mathbb X_{\I}(y) = \{x:(x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))\leq \delta^2 - \varepsilon_{\I}(y)\} 
  \end{equation}
\end{corollary}
\begin{proof}
  By definition, $x\in \mathbb X_{\I}(y)$ is equivalent to the existence of $w$, such that $\|w\|\leq \delta$ and
  \begin{displaymath}
    y_{\I} = H_{\I}x + G_{\I}w.
  \end{displaymath}
  Hence, the corollary holds by Theorem~\ref{theorem:valuefunctionstatic}. 
\end{proof}
\begin{remark}
  One can view $\varepsilon_{\I}(y)$ as the deviation of the measurement from the attack model, i.e., the adversary compromises the sensors in $\Ic$. Hence, if $\varepsilon_\I(y)\geq \delta^2$, i.e., the deviation cannot be explained by the noise, then $\mathbb X_\I(y)$ is empty, which implies that the compromised sensor set is not $\Ic$. Hence, some sensors in $\I$ must be compromised. On the other hand, a small deviation gives a larger $\mathbb X_\I(y)$.
\end{remark}

Since $\mathbb X(y) = \bigcup_{|\I|=l}\mathbb X_\I(y)$, we know that $\mathbb X(y)$ is a union of ellipsoids. One can refer to \cite{Yildirm2006} for more detailed algorithm to compute the center of $\mathbb X(y)$. 

\comment{Yilin: I think his convex optimization based algorithm will definitely work. The other one should work with some modifications. need to double check. However, this is not our main contribution.}

\section{Dynamic Estimation}

\subsection{Problem Formulation}
Consider the following linear time invariant dynamical system:
\begin{align}
  x(k) &= Ax(k-1) + B_w w(k),\nonumber\\
  y(k) &= C_y x(k)  + D_{yw} w(k) +  a(k).
  \label{eq:linearsystem}
\end{align}
For the above linear system~\eqref{eq:linearsystem}, we assume that $x(0) = 0$ and $\|w\|\leq \delta$. We further assume that $\|a\|_0\leq l$. Moreover, we assume that $\begin{bmatrix} B_w \\D_{yw}\end{bmatrix}$ is of full row rank. For simplicity, we assume that $B_wD_{yw}^T = 0$, i.e., the process noise and measurement noise are uncorrelated.

We can write the relationship between $x(k),y(0),\dots,y(k),w(0),\dots,w(k)$ as
\begin{align}
  \begin{bmatrix}
    y(0)\\
    \vdots\\
    y(k)
  \end{bmatrix} = H(k)x(k) + G(k)\begin{bmatrix}
    w(0)\\
    \vdots\\
    w(k)
  \end{bmatrix},
  \label{eq:dynamicalstack}
\end{align}
where $H(k)$ and $G(k)$ are functions of $A,B_w,C_y,D_{yw}$ matrices. Let us define 
\begin{equation}
  \mathbb X(y,k) \triangleq  \{x(k):\exists\|w\|\leq\delta\text{ and }\|a\|_0\leq l,\text{ such that Equation~\eqref{eq:dynamicalstack} holds} \}.
\end{equation}
Define the optimal state estimation as the Chebyshev center of $\mathbb X(y,k)$:
\begin{equation}
 \hat x(y,k) \triangleq c(\mathbb X(y,k)) .
\end{equation}
Conceptually, we can use a static estimator proposed in Section~\ref{sec:static} to compute $\hat x(y,k)$ of the set $\mathbb X(y,k)$ and thus derive the optimal state estimate. However, this approach is cumbersome since we need to keep track of all the measurements and the matrices $H(k)$ and $G(k)$ grow as $k$ increases. As a result, in the next subsection, we propose a Kalman-like estimator to solve $\hat x(k)$ recursively.

\subsection{Estimator Design}
Before deriving the optimal estimator, let us define the following quantities. For any index set $\mathcal I = \{i_1,\dots,i_j\}$, define
\begin{equation}
   y_{\I}(k) \triangleq \begin{bmatrix}
    y_{i_1}(k)\\
    \vdots\\
    y_{i_j}(k)
  \end{bmatrix},\,C_{y,\I} \triangleq \begin{bmatrix}
    c_{y,i_1}\\
    \vdots\\
    c_{y,i_j}
  \end{bmatrix},\,D_{yw,\I}\triangleq\begin{bmatrix}
    d_{yw,i_1}\\
    \vdots\\
    d_{yw,i_j}
  \end{bmatrix},
\end{equation}
and
\begin{equation}
Q \triangleq B_wB_w^T,\,R_\I \triangleq D_{yw,\I}D_{yw,\I}^T.
\end{equation}

Further define
\begin{align}
  \hat x_\I(y,0) = 0,\,P_\I(0) = 0,\varepsilon_\I(y,0) = y_\I(0)^T R_\I^{-1}y_\I(0).
\end{align}
and
\begin{align}
  \hat x_\I(y,k+1)& = A\hat x_\I(y,k) + K_{\I}(k+1)r_{\I}(y,k+1)\\
  P_\I(k+1) &= \left[\left(AP_\I(k)A^T+Q\right)^{-1}+C_{y,\I}^TR_\I^{-1}C_{y,\I}\right]^{-1},\\
  \varepsilon_\I(y,k+1) &= \varepsilon_\I(y,k) + r_\I(y,k+1)^T\left[R_\I+C\left(AP_\I(k)A^T+Q\right)C^T\right]^{-1}r_\I(y,k+1).
\end{align}
where the gain matrix and residue at time $k+1$ is defined as
\begin{align}
  K_{\I}(k+1) &= \left(AP_\I(k)A^T+Q\right)C^T\left[C\left(AP_\I(k)A^T+Q\right)C^T+R_\I\right]^{-1}\\
  r_\I(y,k+1)&= y_{\I}(k+1) -C_{y,\I} A\hat x_\I(y,k)
\end{align}
\begin{theorem}
  Define the function $V_{\I}(x,k):\mathbb R^n\times \mathbb N^+\rightarrow \mathbb R$ as the solution of the following optimization problem:
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w\in l_2}&
    & \|w\|^2\nonumber\\
    &\textrm{subject to}&
    & x(0) = 0,\,x(k) = x\nonumber\\
    &&&x(t) = Ax(t-1) + B_w w(t),\,t=1,\dots,k\nonumber\\
    &&&y_{\I}(t) = C_{y,\I} x(t)+D_{yw,\I}w(t),\,t=0,\dots,k.\label{eq:optdynamical}
  \end{align}
  Then $V_{\I}(x,k)$ is given by
  \begin{equation}
    V_\I(x,k) = (x -\hat x_\I(y,k))^T P_{\I}(k)^{-1} (x -\hat x_\I(y,k)) + \varepsilon_\I(y,k),
    \label{eq:valuefunctiondynamic}
  \end{equation}
  where $\hat x_\I(y,k),\,P_\I(k),\,\varepsilon_\I(y,k)$ are defined in .
  \label{theorem:valuefunctiondynamic}
\end{theorem}
Before proving Theorem~\ref{theorem:valuefunctiondynamic}, we need the following lemma:
\begin{lemma}
  \label{lemma:quadraticsum}
  Consider the following function
  \begin{displaymath}
   f(x) =  (x - x_0)^T P^{-1} (x-x_0) + (Ax -x_1)^T Q^{-1}(Ax-x_1), 
  \end{displaymath}
  where $x,x_0,x_1$ are vectors and $P,Q,A$ are matrices of proper dimension. Then
  \begin{displaymath}
    f(x) = (x -x_c )^T (P^{-1} + A^TQ^{-1}A )(x-x_c) + (x_1 - Ax_0)^T(APA^T+Q)^{-1}(x_1 - Ax_0)^T,
  \end{displaymath}
  where 
  \begin{displaymath}
    x_c = x_0 + PA^T(Q+APA^T)^{-1}(x_1-Ax_0). 
  \end{displaymath}
\end{lemma}
\begin{proof}
  \comment{add proof here.}
\end{proof}
We are now ready to prove Theorem~\ref{theorem:valuefunctiondynamic}
\begin{proof}
  We will prove the theorem by induction. First, consider $V_{\I}(x,1)$. The optimization problem can be written as
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w\in l_2}&
    & \|w(0)\|^2+\|w(1)\|^2\nonumber\\
    &\textrm{subject to}&
    & x(0) = 0,\,x(1) = x\nonumber\\
    &&& x(1) = Ax(0) + B_w w(1)\nonumber\\
    &&&y_{\I}(0) = C_{y,\I} x(0)+D_{yw,\I}w(0)\nonumber\\
    &&&y_{\I}(1) = C_{y,\I} x(1)+D_{yw,\I}w(1)
    \label{eq:inductioninitial}
  \end{align}
  The constraints are equivalent to
  \begin{align}
    D_{yw,\I} w(0) &=  y_\I(0), \label{eq:noise0}
  \end{align}
  and
  \begin{align}
    \begin{bmatrix}
      x\\
      y_{\I}(1) - C_{y,\I}x
    \end{bmatrix}  = \begin{bmatrix}
      B_w \\
      D_{yw,\I}
    \end{bmatrix}w(1), \label{eq:noise1}
  \end{align}
  Clearly, \eqref{eq:noise0} implies that
  \begin{displaymath}
    \|w(0)\|^2 \geq y_\I(0)^T R^{-1}y_\I(0),
  \end{displaymath}
  where the inequality is tight. By the fact that $B_wD_{yw}^T = 0$, \eqref{eq:noise1} implies that 
  \begin{align}
    \|w(1)\|^2& \geq\begin{bmatrix}
      x\\
      y_{\I}(1) - C_{y,\I}x
    \end{bmatrix} ^T \begin{bmatrix}
      Q^{-1}&\\
      &R_\I^{-1} 
    \end{bmatrix}\begin{bmatrix}
      x\\
      y_{\I}(1) - C_{y,\I}x
    \end{bmatrix}\nonumber \\
    &= x^TQ^{-1}x + (y_{\I}(1) - C_{y,\I}x)^T R_\I^{-1}(y_{\I}(1) - C_{y,\I}x)\nonumber\\
    &= (x-\hat x_\I(1))^T P_\I(1)^{-1}(x-\hat x_\I(1)) +y_\I(1)^T(R_\I+CQC^T)^{-1}y_\I(1).\label{eq:noise1alter}
  \end{align} 
  The last equality holds as a result of Lemma~\ref{lemma:quadraticsum}. The inequality in \eqref{eq:noise1alter} is also tight. Hence,
  \begin{displaymath}
    V_\I(x,1) = (x -\hat x_\I(y,1))^T P_{\I}(1)^{-1} (x -\hat x_\I(y,1)) + \varepsilon_\I(y,1).
  \end{displaymath}

  Now assume that \eqref{eq:valuefunctiondynamic} holds for $k$. By dynamical programming, optimization problem~\eqref{eq:optdynamical} at time $k+1$ can be written as
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w(k+1),x(k)}&
    & V_\I(x(k),k)+\|w(k+1)\|^2\nonumber\\
    &\textrm{subject to}&
    & x(k+1) = x\nonumber\\
    &&& x(k+1) = Ax(k) + B_w w(k+1)\nonumber\\
    &&&y_{\I}(k+1) = C_{y,\I} x(k+1)+D_{yw,\I}w(k+1)
    \label{eq:inductionrecursive}
  \end{align}

  The constraint is equivalent to
  \begin{align}
    \begin{bmatrix}
      x - Ax(k)\\
      y_{\I}(k+1) - C_{y,\I}x
    \end{bmatrix}  = \begin{bmatrix}
      B_w \\
      D_{yw,\I}
    \end{bmatrix}w(k+1),
  \end{align}
  which implies that the objective function of \eqref{eq:inductionrecursive} is given by
  \begin{align}
    V_\I(x,k+1) &= \min_{x(k)}\; V_\I(x(k),k) \nonumber+ \left(x -Ax(k)\right)^TQ^{-1}\left(x-Ax(k)\right)\\
    &+ \left(y_{\I}(k+1) - C_{y,\I}x\right)^T R_\I^{-1}\left(y_{\I}(k+1) - C_{y,\I}x\right)
  \end{align}
  Therefore, by Lemma~\ref{lemma:quadraticsum}, the minimum is achieve when
  \begin{displaymath}
    x(k) = \hat x_\I(y,k) + P_\I(k)A^T(Q+AP_\I(k)A^T)^{-1}(x-A\hat x_\I(y,k)). 
  \end{displaymath}
  Therefore,
  \begin{align}
    V_\I(x,k+1) &= (x-A\hat x_\I(y,k))^T(AP_\I(k)A^T+Q)^{-1} (x-A\hat x_\I(y,k))\nonumber\\
    &+  \left(y_{\I}(k+1) - C_{y,\I}x\right)^T R_\I^{-1}\left(y_{\I}(k+1) - C_{y,\I}x\right)\nonumber\\
    &+\varepsilon_\I(y,k).
  \end{align}
  By Lemma~\ref{lemma:quadraticsum}, we get
  \begin{displaymath}
    V_\I(x,k+1) = (x-\hat x_\I(y,k+1))^TP_\I(k+1)^{-1} (x-\hat x_\I(y,k+1)) + \varepsilon_\I(y,k+1),
  \end{displaymath}
  which finishes the proof.
\end{proof}

Let us define the following set:
\begin{displaymath}
  \mathbb X(y,k) \triangleq  \{x(k):\exists\|w\|\leq\delta\text{ and }a(0),\dots,a(k)\in\mathcal V_{\Ic},\text{ such that Equation~\eqref{eq:dynamicalstack} holds} \}.
\end{displaymath}
We now have the following corollary:

\begin{corollary}
  If $\varepsilon_\I(y,k) > \delta^2$, then $\mathbb X_{\I}(y,k)$ is an empty set. Otherwise, $\mathbb X_{\I}(y,k)$ is an ellipsoid given by
  \begin{equation}
    \mathbb X_{\I}(y,k) = \{x:(x-\hat x_{\I}(y,k))^TP_{\I}(k)^{-1} (x-\hat x_{\I}(y,k))\leq \delta^2 - \varepsilon_{\I}(y,k)\} 
  \end{equation}
\end{corollary}
\begin{proof}
  \comment{Yilin: Trivial}
\end{proof}

Fig~\ref{fig:optdynamicestimator} illustrates the diagram of the optimal estimator. One can see that it contains $N ={ m\choose{l}}$ Kalman-like estimators, which not only keep track of the $\hat x_\I(y,k)$ and $P_\I(k)$, but also compute the deviation $\varepsilon_\I(y,k)$ of the measurements from the attack model.

\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth]
      \node (measurement) at (0,0) {$y(k)$};
      \node (est1) [draw,fill=blue!30,rounded corners]at (3,1.5) {Estimator $\mathcal I_1$};
      \node (est2) [draw,fill=blue!30,rounded corners] at (3,0) {Estimator $\mathcal I_2$};
      \node (est3) [draw,fill=blue!30,rounded corners] at (3,-1.5) {Estimator $\mathcal I_N$};
      \node  at (3,-0.75) {$\vdots$};
      \node (center) [draw,fill=blue!30,rounded corners,text width=2.1cm] at (8,0) {{\centering Chebyshev Center}};
      \node (final) at (11,0) {$\hat x(y,k)$};
      \draw [bend left=20,semithick,->] (measurement) to (est1);
      \draw [semithick,->] (measurement) to (est2);
      \draw [bend right=20,semithick,->] (measurement) to (est3);
      \draw [bend left=20,semithick,->] (est1) to node [anchor=south west]{$\mathbb X_{\mathcal I_1}(y,k)$} (center) ;
      \draw [semithick,->] (est2) to node[anchor=south] {$\mathbb X_{\mathcal I_2}(y,k)$} (center);
      \draw [bend right=20,semithick,->] (est3) to node[anchor=north west] {$\mathbb X_{\mathcal I_3}(y,k)$} (center);
      \draw [semithick,->] (center) to (final);
    \end{tikzpicture}
  \end{center}
  \caption{Diagram of the Optimal Dynamic Estimator}
  \label{fig:optdynamicestimator}
\end{figure}
\subsection{Estimation Error Characteristics}
In this section, we consider the estimation error $e(y,k) =  x(k) - \hat x(y,k)$.

\begin{theorem}
Define $\hat x(y,k)$ to be the Chebyshev Center of {$\cup_{\mathcal I \in T} \;\mathbb X_{\mathcal I_i}(y,k)$}. 
Then, the estimation error $e(y,k) =  x(k) - \hat x(k)$ is in $l_2$ and are bounded by 
\BEQ
\forall k \in N,\;\;\; ||e(y,k)|| \leq {1 \over 2}  \sqrt{\sigma (P_K)} \;\delta 
\EEQ
and
\BEQ 
	\sum\limits_{k = 0}^{\infty} || e(y,k) || \leq \sqrt{\sigma (P_K)}\; |T| \;\delta 
\EEQ
	where $|T|$ is the cardinality of $T$. Equality holds when 
	$
\forall k \in N, \forall \mathcal J_i \neq \mathcal J_j \in T \\
\text{supp}(x_{\mathcal J_i}(k)) \cap \text{supp}(x_{\mathcal J_j}(k)) = \emptyset$, and $\forall k \in N, \forall \mathcal J \in T, || x - x_\mathcal J(k) || = \delta$.\\

\end{theorem} 
\begin{proof}
 By similar argument, we have 
 \BEQ
\sum\limits_{k = 0}^\infty || x(k) - \hat x(k) || ^2 \leq 
\sum\limits_{k = 0}^\infty \text{max}_{J \in T} || x_{\mathcal I}(k) - x_{\mathcal J}(k) ||^2 
\leq \sqrt{\sigma (P_K)}\; |T| \;\delta 
 \EEQ 
the true value of the state $x(k)$ lies in $\mathbb X_{\mathcal I}(k)$. \\
The first is equaltion is a necessary condition for the above to hold. \\
Next, we prove the bound is tight by considering the following problem, 
\begin{equation*}
\begin{aligned}
& \underset{x}{\text{minimize}}
& & \sum\limits_{k = 0}^\infty \text{max}_{J \in T} || x_{\mathcal I}(k) - x_{\mathcal J}(k) ||^2  \\
& \text{subject to}
& & \sum\limits_{k = 0}^\infty || x_{\mathcal J_i}(k) - x_{\mathcal J_j}(k) || ^2 \leq C \text{ for any } J_i,J_j \in T
\end{aligned}
\end{equation*}
The optimal value is achieved when 
$
\forall k \in N, \forall \mathcal J_i \neq \mathcal J_j \in T \\
$
\BEQ 
\text{supp}(x_{\mathcal J_i}(k)) \cap \text{supp}(x_{\mathcal J_j}(k)) = \emptyset
\EEQ
and the optimal value must be less than
\BEQ 
|T|C \eq 2 \sum\limits_{k} || x_{\mathcal I}(k) - \hat x(k) ||^2 
\EEQ 
\end{proof}

\begin{theorem}
Define $\hat x(y,k)$ to be the center of {$\mathbb X_{\mathcal J_i}(y,k)$} for any $J \in T$ such that $\mathbb X_{\mathcal I_i}$ in nonempty at time $k$. 
Then, the estimation error $e(y,k) =  x(k) - \hat x(k)$ is in $l_2$ and are bounded by 
\BEQ
\forall k \in N,\;\;\; ||e(y,k)|| \leq  \sqrt{\sigma (P_K)} \;\delta 
\EEQ
and
\begin{equation}
 \sum\limits_{k = 0}^\infty || e(y,k) || < \sqrt{\sigma (P_K)}\;\delta 
	\end{equation}


\end{theorem} 

\comment{Need to prove that the estimation error is $l_2$.}
\section{Close the Loop}
\comment{$\mathcal H_\infty$ control design here.}

The LQR control objective can be written as 
\BEQ 
&&\sum\limits_k x(k)^T Q x(k)^T + u(k)^T S u(k) \\
\eq \sum\limits_k \hat x(k)^T Q \hat x(k)^T + u(k)^T S u(k) + 2 \hat x(k) Q (x(k) - \hat x(k) ) +  (x(k) - \hat x(k) )^T Q (x(k) - \hat x(k) )\\
\EEQ 

If we use kalman filter,  $x(k)$ and $ (x(k) - \hat x(k) )$ in independent. Thus,
\BEQ
E[ \hat x(k) Q (x(k) - \hat x(k) ) ] = 0 
\EEQ
So we get the separation. However, I guess this is not the case here. 
\bibliographystyle{IEEEtran}
\bibliography{bib}

\appendices
\section{Proof of Theorem ??}
\input{appendix.tex}
\section{Proof of Theorem ??}
\input{appendix2.tex}
\end{document}



