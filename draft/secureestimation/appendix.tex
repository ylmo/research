\comment{Yilin: We need to take into account that some estimator will terminate in finite time.}

Let $T_0,\dots,T_{N-1} \in \mathbb N\cup\{\infty\}$. If $T_i = \infty$, then we define $\{x_i(k)\}_{k\in \mathbb N}$ as an infinite sequence of points in $\mathbb R^n$. Otherwise if $T_i$ is finite, then we define $\{x_i(k)\}_{k=0,\dots,T_i}$ as a finite sequence of length $T_i+1$. 

Define the index set $\mathcal L(k)$ as
\begin{displaymath}
  \mathcal L(k) \triangleq \{i: T_i \geq k\}.
\end{displaymath}

In other words, $\mathcal L(k)$ is the index set of the sequence that does not terminate before $k$.

We can define the center $c(k)$ as
\begin{displaymath}
  c(k) \triangleq \frac{1}{\left|\mathcal L(k)\right|} \sum_{i\in \mathcal L(k)} x_i(k).
\end{displaymath}
By symmetry, we assume that
\begin{displaymath}
  T_0 \geq T_1 \geq \dots\geq T_{N-1}, 
\end{displaymath}
and $T_0 = \infty$. 

\begin{theorem}
If for all $i,j$, the following inequality holds:
\begin{equation}
  \sum_{k=0}^{T_i\wedge T_j} \|x_i(k)-x_j(k)\|^2 \leq 1,
  \label{eq:l2diameter}
\end{equation}
then the following inequality holds:
\begin{equation}
  \sum_{k=0}^\infty \|x_0(k) - c(k)\|^2 \leq \alpha \log N, 
  \label{eq:centerl2error}
\end{equation}
where $\alpha\approx 0.41$ is the solution of the following optimization problem:
\begin{equation}
  \alpha = \min\{\alpha > 0: \sup_{\rho\in[0,1)} \alpha \log(1-\rho) + \rho^2 \leq 0 \}
  \label{eq:logcoefficient}
\end{equation}
\label{theorem:centerl2error}
\end{theorem}
Before proving Theorem~\ref{theorem:centerl2error}, we need the following lemma:
\begin{lemma}
  Let $\varphi(k)$ satisfies the following recursive equation:
  \begin{equation}
    \varphi(k) =\max_{i=1,\dots,k-1}\varphi(k-i) + \frac{i^2}{k^2},
    \label{eq:phirecursive}
  \end{equation}
  with initial condition
  \begin{displaymath}
   \varphi(1) = 0. 
  \end{displaymath}
  Then, $\varphi(k) \leq \alpha \log k$, where $\alpha$ is defined in \eqref{eq:logcoefficient}.
  \label{lemma:dynamicalprogramming}
\end{lemma}
\begin{proof}
We will prove the lemma by induction. If $k = 1$, then clearly
\begin{displaymath}
 \varphi(1) = \alpha \log 1 =0. 
\end{displaymath}
Now assume that $\varphi(t)\leq \alpha \log t$, for all $t = 1,\dots,k-1$. By \eqref{eq:phirecursive}, we know that
\begin{align*}
  \varphi(k) \leq \max_{i=1,\dots,k-1} \alpha \log(k-i) + \frac{i^2}{k^2}\leq \sup_{i\in[0,k)}\alpha \log(k-i) + \frac{i^2}{k^2}.
\end{align*}
Let $i = \rho k$, then we have
\begin{align*}
  \varphi(k)\leq  \alpha \log k + \sup_{\rho\in[0,1)} \alpha \log (1-\rho) + \rho^2.
\end{align*}
Hence, \eqref{eq:logcoefficient} implies that $\varphi(k) \leq \alpha \log k$, which completes the induction.
\end{proof}
We are now ready to prove Theorem~\ref{theorem:centerl2error}.
\begin{proof}
  Let us denote the supremum of the LHS of \eqref{eq:centerl2error} as $M$. Without loss of generality, we can assume that $x_0(k) = 0$ for all $k$. Furthermore, by triangular inequality, we have
  \begin{displaymath}
    \|x_0(k)-c(k)\| \leq \frac{1}{|\mathcal L(k)|} \sum_{i\in \mathcal L(k)}\| x_i(k)\|,
  \end{displaymath}
  which implies that
  \begin{displaymath}
    \|x_0(k)-c(k)\|^2 \leq \frac{1}{|\mathcal L(k)|^2} \left(\sum_{i\in \mathcal L(k)}\| x_i(k)\|\right)^2.
  \end{displaymath}
  
  Consider the following optimization problem:
\begin{align}
  \label{eq:optcenterl2}
  &\mathop{\textrm{maximize}}\limits_{\{x_i(k)\},T_1,\dots,T_{N-1}}&
  & \sum_{k=0}^\infty\frac{1}{|\mathcal L(k)|^2}\left( \sum_{i\in \mathcal L(k)}\|x_i(k)\|\right)^2\\
  &\textrm{subject to}&
  & \sum_{k=0}^{T_i} \|x_i(k)\|^2\leq 1,\,\forall i=1,\dots,N-1.\nonumber
\end{align}
Denote the optimal value of problem~\eqref{eq:optcenterl2} as $M_1$.\comment{ maybe write a little bit more on why?} One can prove that $M\leq M_1$.

Define
\begin{displaymath}
  s(k) \triangleq \sum_{i\in \mathcal L(k)} \|x_i(k)\|^2.
\end{displaymath}
As a result
\begin{equation}
  \sum_{t=k}^\infty s(k) =  \sum_{i\in \mathcal L(k),i\neq 0} \left(\sum_{t=k}^\infty \|x_i(k)\|^2 \right)\leq |\mathcal L(k)| - 1,\,\forall k.
  \label{eq:nodesumineq}
\end{equation}

Since the constraints in Problem~\eqref{eq:optcenterl2} are convex and we are maximizing a convex and increasing function, \eqref{eq:optcenterl2} achieves the maximum only if $\|x_i(k)\|=0$ or $1$, for all $i$ and $k$. Hence, $s(k) \in \mathbb N$. Moreover,
 \begin{displaymath}
  s(k) =  \sum_{i\in \mathcal L(k)} \|x_i(k)\| = \sum_{i\in \mathcal L(k)} \|x_i(k)\|^2.
 \end{displaymath}
 
Therefore, the following optimization problem gives an upper bound of $M_1$:
\begin{align}
  \label{eq:optcenterl2alternative}
  &\mathop{\textrm{maximize}}\limits_{s(k)\in \mathbb N,T_1,\dots,T_{N-1}}&
  & \sum_{k=0}^\infty\frac{s(k)^2}{|\mathcal L(k)|^2} \\
  &\textrm{subject to}&
  & \sum_{t=k}^\infty s(k) \leq  |\mathcal L(k)|-1,\,\forall k\in \mathbb N.\nonumber
\end{align}

By definition, $|\mathcal L(k)|$ is decreasing. Given $T_1,\dots,T_{N-1}$, suppose $|\mathcal L(k)|$ takes $q$ different values. We can define $k_0 = 0$ and the $i$th jump time as the first time that $|\mathcal L(k)|$ is smaller than $|\mathcal L(k_{i-1})|$, i.e.,
\begin{displaymath}
  k_i = \min\{k:|\mathcal L(k)|<|\mathcal L(k_{i-1})|\},\,\forall 1\leq i< q.
\end{displaymath}
We further define $k_{q} = \infty$. Since we that $T_1\geq \dots\geq T_{n-1}$, there exists a one to one correspondence between $0 < k_1 < k_2<\dots<k_{q-1}$, $|\mathcal L(k_1)| > \dots > |\mathcal L(k_{q-1})|$ to $T_1\geq \dots\geq T_{n-1}$.

Furthermore, by the fact that the objective function in \eqref{eq:optcenterl2alternative} is convex, we know that at most one $s(k)$ is positive for any $k\in [k_i,k_{i+1})$, while all the others must be 0. Without loss of generality, we can assume that $s(k_i+1),\dots,s(k_{i+1}-1)$ are all zero. Hence, optimization problem~\eqref{eq:optcenterl2alternative} is equivalent to: 
\begin{align}
  \label{eq:optcenterl2finite}
  &\mathop{\textrm{maximize}}\limits_{s(k_i)\in \mathbb N,|\mathcal L(k_i)|,q}&
  & \sum_{i=0}^{q-1}\frac{s(k_i)^2}{|\mathcal L(k_i)|^2} \\
  &\textrm{subject to}&
  & \sum_{i=j}^{q-1} s(k_i) \leq  |\mathcal L(k_i)|-1,\,\forall j = 0,\dots,q-1.\nonumber\\
  & &
  & N = |\mathcal L(k_0)| > \dots > |\mathcal L(k_{q-1})|.\nonumber
\end{align}

One can see that the optimal value of Problem \eqref{eq:optcenterl2finite} is achieved when $q = N$, i.e., when $|\mathcal L(k)|$ takes all the values from alphabet $\{1,\dots,N\}$. Thus, we have $M_1$ is given by the following optimization problem:
\begin{align}
  \label{eq:optcenterl2final}
  &\mathop{\textrm{maximize}}\limits_{\ts(0),\dots,\ts(N-1)\in \mathbb N}&
  & \sum_{i=0}^{N-1}\frac{\ts(i)^2}{(N-i)^2} \\
  &\textrm{subject to}&
  & \sum_{i=j}^{N-1} \ts(i) \leq  N-j-1,\,\forall j = 0,\dots,N-1.\nonumber
\end{align}
We will solve \eqref{eq:optcenterl2final} by dynamical programming. For any $\mu,\eta\in \mathbb N^+$ and $\eta \leq \mu$, define the value function
\begin{align}
  \label{eq:valuefunctionl2error}
  \tV(\mu,\eta)\triangleq &\mathop{\textrm{maximize}}\limits_{\ts(0),\dots,\ts(\mu-1)\in \mathbb N}&
  & \sum_{i=0}^{\mu-1}\frac{\ts(i)^2}{(\mu-i)^2} \\
  &\textrm{subject to}&
  & \sum_{i=j}^{\mu-1} \ts(i) \leq  \mu-j-1,\,\forall j = 0,\dots,\mu-1.\nonumber\\
  &&
  & \sum_{i=0}^{\mu-1} \ts(i) =  \eta-1.\nonumber
\end{align}

Clearly $\tV$ is an increasing function of $\eta$ and $\tV(1,1) = 0$, $ \tV(N,N) = M_1$. By Bellman equation, we have
\begin{equation}
  \tV(\mu,\eta) = \max_{1\leq j\leq\mu-1,\,j\leq k} \tV(\mu-1,j) +\frac{(\eta-j)^2}{\mu^2}.
  \label{eq:bellmanequation}
\end{equation}

We want to prove that for any $\mu \geq k$,
\begin{equation}
 \tV(\mu,\eta) = \tV(\eta,\eta). 
  \label{eq:tVconstantovermu}
\end{equation}
We will prove \eqref{eq:tVconstantovermu} by induction on $\mu$. Clearly, when $\mu = 1$, $\tV(1,1) = \tV(1,1)$. Now assume that when for any $\mu = 1,\dots,\mu_0$ and $\eta \leq \mu$, we have
\begin{equation}
  \tV(\mu,\eta) = \tV(\eta,\eta),\,\forall \mu = 1,\dots,\mu_0,\text{ and }\eta \leq \mu.
  \label{eq:inductionassumption}
\end{equation}

Now let $\mu = \mu_0 + 1$, if $\eta = \mu = \mu_0 +1$, then \eqref{eq:tVconstantovermu} holds trivially. Now consider $\eta < \mu$, by \eqref{eq:bellmanequation}, we have
\begin{equation}
  \tV(\mu,\eta) = \max_{1\leq j\leq \eta}\; \tV(\mu-1,j) + \frac{(\eta-j)^2}{\mu^2} = \max_{1\leq j\leq \eta}\; \tV(j,j) + \frac{(\eta-j)^2}{\mu^2}.
  \label{eq:tvmueta}
\end{equation}
The second equality is due to the induction assumption. Now consider $\tV(\eta,\eta)$, we have
\begin{equation}
  \tV(\eta,\eta) = \max_{1\leq j\leq \eta-1} \tV(\eta-1,j) + \frac{(\eta-j)^2}{\eta^2} = \max_{1\leq j\leq \eta-1} \tV(j,j) + \frac{(\eta-j)^2}{\eta^2},
  \label{eq:tvetaeta}
\end{equation}
where we use the induction assumption to prove the second equality again. Comparing \eqref{eq:tvmueta} and \eqref{eq:tvetaeta}, by the fact that $\mu > \eta$, we know that the maximum in \eqref{eq:tvmueta} is achieved when $j = \eta$, which implies that \eqref{eq:tVconstantovermu} holds for $\mu = \mu_0 + 1$. Hence, \eqref{eq:tVconstantovermu} holds for all $\mu \geq \eta$. Therefore, we can define $\varphi(\eta) = \tV(\eta,\eta)$. Thus, \eqref{eq:bellmanequation} can be written as
\begin{displaymath}
  \varphi(\eta) = \max_{1\leq j\leq \eta-1} \varphi(j) + \frac{(\eta - j)^2}{\eta^2}, 
\end{displaymath}
with initial condition $\varphi(1) = 0$. Thus, by Lemma~\ref{lemma:dynamicalprogramming}, we know that $M\leq M_1 = \tV(N,N) = \varphi(N) \leq \alpha \log N$, which finishes the proof.
\end{proof}

