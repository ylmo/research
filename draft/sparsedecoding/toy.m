n = 5 
m = 25;
l = 2; 

A = rand(n); 

X = rand(n,m);
for i = 1:m
    [B, I] = sort(X(:,i), 'descend');
    X(I(l+1:n),i) = 0;
end

Y = A*X;

p = 20*n;

Ae = rand(n, p); 

%lasso parameter
l1 = 1;
l2 = 1;

for iter = 1:5
    cvx_begin
        variable Xe(p, m)
        variable Xe2(p)
        minimize (square_pos(norm(Y-Ae*Xe, 'fro'))+l1*sum(sum(abs(Xe))) ...
                + l2*sum(Xe2))
        subject to
        Xe >= 0; %non-negative contraint
        for i = 1: p
            norm(Xe(i, :), 2) <= Xe2(i);
        end
    cvx_end

    Xe = Xe(find(Xe2 >= 0.01), :)

    p = size(Xe,1)
    %re-solve Ae
    cvx_begin
        variable Ae(n,p)
        minimize(norm(Y-Ae*Xe, 'fro'))
    cvx_end

end
