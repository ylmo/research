% double column, for final publication of full paper or cdc
% \documentclass[10pt,final,journal,letterpaper]{IEEEtran}
% double column, 9pt, for final publication of technical note 
% \documentclass[9pt,technote,letterpaper]{IEEEtran}
% single column double space, for journal draft
\documentclass[12pt,draftcls,onecolumn]{IEEEtran}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\title{Resilient Estimator Design}

\author{Yilin Mo$^*$
  \thanks{
    $*$: Yilin Mo is with the School of Electrical and Electronic Engineering, Nanyang Technological University, Singapore. Email: {ylmo@ntu.edu.sg}
  }
}

\begin{document} \maketitle

Consider the state $x \in \mathbb R^n$. Assume that $m$ sensor is measuring the state and the measurement equation for each sensor is given by
\begin{align*}
  y_i = x + w_i + a_i,
\end{align*}
where $y_i\in \mathbb R^n$ is the ``manipulated'' measurement. $w_i\sim\mathcal N(0,I)$ is i.i.d. Gaussian noise and $a_i$ is the bias injected by the adversary. We assume that at most $p$ sensors is compromised, i.e.,
\begin{align*}
  \left|\{i:\|a_i\|\neq 0\}\right| \leq p.
\end{align*}
Moreover, let us define $z_i \triangleq x + w_i$ as the ``true'' measurement.

\section*{Median over all axis}

Consider the following estimator:
\begin{align*}
  \hat x_j = \text{med}(y_{1,j},\dots,y_{m,j}),
\end{align*}
where $\hat x_j$ and $y_{i,j}$ are the $j$th entry of $\hat x$ and $y_i$ respectively.

It can be shown that $\hat x$ is indeed the solution of the following minimization problem:
\begin{align}
  \hat x = \argmin_{\hat x} \sum_i\|\hat x - y_i\|_1.
  \label{eq:median}
\end{align}

\section*{Lasso}
Consider the following optimization problem:
\begin{align}
  &\mathop{\textrm{minimize}}\limits_{\hat x, w_i, a_i}&
  & \sum_i \|w_i\|_2^2 + \lambda \sum_i \|a_i\|_1\nonumber\\
  &\textrm{subject to}&
  & y_1 = \hat x + w_i + a_i\label{eq:lasso}
\end{align}
If we consider the following function
\begin{align}
  f(x) \triangleq &\mathop{\textrm{minimize}}\limits_{w, a}&
  &  \|w\|_2^2 + \lambda  \|a\|_1\nonumber\\
                  &\textrm{subject to}&
  & x =  w + a\label{eq:lasso2}
\end{align}
Then one can easily prove that the optimization problem \eqref{eq:lasso} can be rewritten as
\begin{align*}
  \hat x = \argmin_{\hat x} \sum_i f(\hat x - y_i).
\end{align*}

Moreover, we have that $f(x) \leq \lambda \|x\|_1$. (since $w = 0$ and $a = x$ is a feasible solution for \eqref{eq:lasso2}).

\section*{Geometric Median}
We can also select the estimate to be 

\begin{align}
  \hat x = \argmin_{\hat x} \sum_i \|\hat x - y_i\|_2.
  \label{eq:geomedian}
\end{align}

\section*{A General Robust Estimator}


\begin{theorem}
  Consider the following translation invariant estimator:
  \begin{align}
    \hat x = \argmin_{\hat x} \sum_i f(\hat x - y_i).
    \label{eq:general}
  \end{align}
  Assume the following properties of function $f$:
  \begin{enumerate}
  \item $f$ is convex.
  \item $f$ is symmetric, i.e., $f(v) = f(-v)$.
  \item $f$ is non-negative and $f(0) = 0$.
  \item Define $g_v:\mathbb R^+\rightarrow \mathbb R$, such that $g_v(t) = f(tv)$. Assume the following limit is well defined (not infinity) for all $v\in \mathbb R^n$:
    \begin{align}
      \lim_{t\rightarrow\infty} \frac{g_v(t)}{t} = C(v).
      \label{eq:sublinear}
    \end{align}
  \end{enumerate}
  Then if $p < m-p$, the estimator is robust to the attack.
  \label{theorem:main}
\end{theorem}

\begin{remark}
  \begin{enumerate}
  \item $f$ is convex to make the minimization problem tractable.
  \item If \eqref{eq:sublinear} is violated, then the estimator may not be robust. For example, consider $f(x) = \|x\|_2^2$. The estimator is the average and hence is not robust.
    \item It is not difficult to prove that \eqref{eq:sublinear} is equivalent to the existence of some constant $C$, such that $f(tv)\leq C\|v\|$.
  \end{enumerate}
\end{remark}

Before proving Theorem~\ref{theorem:main}, we need the following lemma:
\begin{lemma}
  The following statements are true:
  \begin{enumerate}
  \item $C(v)$ is a well-defined norm on $R^n$.
  \item For any $v$, define the function
    \begin{align}
      h_v(u,t) = \frac{f(v+tu)-f(v)}{t}.   
    \end{align}
    Then the following point-wise limit holds:
    \begin{align}
      \lim_{t\rightarrow\infty}h_v(u,t)  = C(u).
      \label{eq:deltalimit}
    \end{align}
    Moreover, the convergence is uniform on the set $\{u:\|u\|\leq 1\}$.
  \item For any $v$ and $u$, we have that
    \begin{align}
      f(v+u) - f(v) \leq C(u).
      \label{eq:deltaw}
    \end{align}
  \end{enumerate}
\end{lemma}

\begin{proof}
  \begin{enumerate}
  \item Clearly $C(\alpha v) = \alpha C(v)$, for all $\alpha\in\mathbb R$. Moreover $C(v)$ is convex. By convexity and linearity,
    \begin{align*}
      \frac{1}{2}C(v_1+v_2)= C\left(\frac{v_1+v_2}{2}\right)\leq \frac{1}{2}C(v_1) + \frac{1}{2}C(v_2).
    \end{align*}
    $C(v)$ is a well-defined norm on $\mathbb R^n$.
  \item By convexity,
    \begin{align*}
      f(v+tu)\leq \frac{1}{t+1}f((t+1)v) + \frac{t}{t+1} f((t+1)u).
    \end{align*}
    As a result, we know that
    \begin{align}
      \limsup_{t\rightarrow\infty}\frac{f(v+tu)}{t} \leq C(u).
      \label{eq:limsup}
    \end{align}
    Since $v$ can be arbitrary, the following inequality holds:
    \begin{align}
      \limsup_{t\rightarrow\infty}\frac{f(-v+t u)}{t} \leq C(u).
      \label{eq:limsup2}
    \end{align}
    However, we know that
    \begin{align*}
      f(tu)\leq \frac{1}{2}f(v+tu) + \frac{1}{2}f(-v+tu),   
    \end{align*}
    which implies that
    \begin{align}
      C(u)\leq \frac{1}{2}\liminf_{t\rightarrow\infty}\frac{f(v+tu)}{t} +   \frac{1}{2}\liminf_{t\rightarrow\infty}\frac{f(v+tu)}{t}.
      \label{eq:liminf}
    \end{align}
    Combining \eqref{eq:limsup}, \eqref{eq:limsup2} and \eqref{eq:liminf}, we get
    \begin{align*}
      \lim_{t\rightarrow\infty}h_v(u,t) = C(u).
    \end{align*}
    Notice that the LFS is monotonically increasing with respect to $t$ by the convexity of $f$. Furthermore, $C(u)$ is continuous due to the norm equivalence theorem and the set $\{u:\|u\|= 1\}$ is compact. As a result, by Dini's Theorem, the LFS converges to $C(u)$ uniformly.
  \item By convexity
    \begin{align*}
      f(v+(k+1)u) - f(v+ku)\geq f(v+ku) -  f(v+(k-1)u),
    \end{align*}
    Combining with \eqref{eq:deltalimit} we can prove \eqref{eq:deltaw}.
  \end{enumerate}
\end{proof}
We are now ready to prove Theorem~\ref{theorem:main}:
\begin{proof}
  Define $\min_{\|u\|=1} C(u) =\underline C$. Since $C(u)$ is a well-defined norm, $\underline C > 0$. 

  Due to the uniform convergence of $h_v(u,t)$ to $C(u)$, we can find $\alpha_i$, such that for all $t > \alpha$ and $\|u\| = 1$, the following inequality holds:
  \begin{align}
    h_{-z_i}(u,t) = \frac{f(tu-z_i)-f(-z_i)}{t}\geq C(u) - \frac{\underline C}{m}.
    \label{eq:deltaapprox2}
  \end{align}
  Since
  \begin{align*}
    f(v+(t+1)u)-f(v+tu) \geq \frac{f(v+tu)-f(v)}{t},
  \end{align*}
  we know that for all $t\geq \alpha_i$ and $\|u\|=1$, the following inequality holds:
  \begin{align}
    f((t+1)u-z_i)-f(tu-z_i)\geq C(u) - \frac{\underline C}{m}.
    \label{eq:deltaapprox3}
  \end{align}

  Define $\beta \triangleq \max_{1\leq i\leq m} \alpha_i$. Hence, for any $\|u\| = 1$ and $i = 1,\dots,m$, we have
  \begin{align}
    f((t+1)u-z_i)-f(tu-z_i)\geq C(w) - \epsilon,\,\text{ if }t\geq \beta.
    \label{eq:deltaapprox3}
  \end{align}
  Denote the solution of the optimization problem \eqref{eq:general} to be $\hat x$. We want to prove that $\|\hat x\|\leq \beta +1$. Suppose the opposite. Let us define $u = \hat x/\|\hat x\|$ and $\gamma = \|\hat x\|-1$. We want to prove that $(\gamma-1) u$ is a better solution than $\hat x = \gamma u$. Without loss of generality, let us assume that sensors $1,\dots,p$ are compromised and sensors $p+1,\dots,m$ are benign. By \eqref{eq:deltaw}, we know that
  \begin{align*}
    \sum_{i=1}^p f((\gamma-1) u-y_i)  - \sum_{i=1}^p f(\gamma u - y_i) \leq pC(-u) = pC(u).
  \end{align*}
  On the other hand, by \eqref{eq:deltaapprox3}, we have
  \begin{align*}
    \sum_{i=p+1}^m f(\gamma u-y_i)  - \sum_{i=p+1}^m f((\gamma-1) u - y_i) \geq (m-p)C(u) - \frac{m-p}{m} \underline C
  \end{align*}
  Therefore,
  \begin{align*}
    \sum_{i=1}^m f(\hat x-y_i) - \sum_{i=1}^m f((\gamma-1)u -y_i) \geq (m-2p)C(u) - \frac{m-p}{m}\underline C > 0.
  \end{align*}
  Therefore, $(\gamma-1)u$ is a better solution than $\hat x$, which is a contradiction.

  As a result, we know that $\|\hat x\|\leq \beta+1$ regardless of the adversary's action and hence the estimator is robust to the attack.
\end{proof}

\section*{More Analysis on Lasso}
Assume that $C_1\in\mathbb R^{p\times n}$ and $C_2 \in \mathbb R^{(m-p)\times n}$. The following theorem provide a sufficient condition:
\begin{theorem}
 A sufficient condition for the following inequality to hold for all $u\neq 0$:
 \begin{align}
   \|C_1 u\|_1 < \|C_2 u\|_1
   \label{eq:1normcomparison}
 \end{align}
is that the solution of the following minimization problem is strictly less than $1$:
\begin{align}
   &\mathop{\textrm{minimize}}\limits_{K}&
  & \|C_1K\|_1\nonumber\\
  &\textrm{subject to}&
  & KC_2 = I\label{eq:sufficient}
\end{align}
\end{theorem}
\begin{proof}
 Denote $C_2 u = \mu$, then $u = K \mu$ and $C_1 u = C_1 K \mu$. Therefore, if for all $\mu\neq 0$, $\|C_1 K \mu\|_1 < \|\mu\|_1$, i.e., $\|C_1K\|_1 < 1$, then \eqref{eq:1normcomparison} holds. 
\end{proof}
Notice that \eqref{eq:sufficient} is not necessary since $C_2$ is a tall matrix and $\mu$ cannot take all possible value in $\mathbb R^{m-p}$.

The next theorem provides a necessary condition
\begin{theorem}
  If $\|C_1C_2^+\|_1 > (\sqrt{m-p}+1)/2$, where $C_2^+$ is the Moore-Penrose pseudo inverse of $C_2$, then there exists an $u$, such that $\|C_1u\|_1 > \|C_2 u\|_1$.
  \label{theorem:necessity}
\end{theorem}
The following lemma is needed:
\begin{lemma}
  Let $\mu\in\mathbb R^m$ such that $\mu = \mu_{\parallel} + \mu_{\perp}$, where $\mu_{\|}$ and $\mu_\perp$ are perpendicular to each other. Then the following inequality holds:
  \begin{align}
    \|\mu_\|\|_1 \leq \frac{\sqrt{m}+1}{2}\|\mu\|_1.
    \label{eq:parallel}
  \end{align}
  Moreover the above inequality is achievable when
\begin{align*}
 \mu =  \begin{bmatrix}
1\\
0\\
\vdots\\
0
\end{bmatrix}, \,\mu_\|=\frac{1}{2}\begin{bmatrix}
1+n^{-1/2}\\
n^{-1/2}\\
\vdots\\
n^{-1/2}
\end{bmatrix}, \,\mu_\perp=\frac{1}{2}\begin{bmatrix}
1-n^{-1/2}\\
-n^{-1/2}\\
\vdots\\
-n^{-1/2}
\end{bmatrix}
\end{align*}
\label{lemma:l1decompose}
\end{lemma}
\begin{proof}
  Without loss of generality, let us assume that $\|\mu\|_1 = 1$.  

  $\mu_{\|}$ can be written as $\mu/2 + r$, where $r$ is any vector with 2-norm $\|r\|_2 = \|\mu\|_2 /2$. As a result, we have
\begin{align}
\|\mu_\|\|_1 \leq \frac{1}{2}\|\mu\|_1 + \|r\|_1 \leq \frac{1}{2}\|\mu\|_1 + \sqrt{m}\|r\|_2 = \frac{1}{2}\|\mu\|_1 + \frac{\sqrt{m}}{2}\|\mu\|_2 \leq  \frac{1}{2}\|\mu\|_1 + \frac{\sqrt{m}}{2}\|\mu\|_1,
\end{align}
where we use the fact that for an $m$ dimensional vector $\mu$,
\begin{align*}
  \|\mu\|_2\leq \|\mu\|_1 \leq  \sqrt{m}\|\mu\|_2.
\end{align*}
The achievability of \eqref{eq:parallel} is easy to verify.
\end{proof}

We are now ready to prove Theorem~\ref{theorem:necessity}:
\begin{proof}
  Since $\|C_1C_2^+\|_1>(\sqrt{m-p}+1)/2$, we can find $\mu\in \mathbb R^{m-p}$, such that
\begin{align*}
 \|C_1C_2^+\mu\|_1 >\frac{\sqrt{m-p}+1}{2}\|\mu\|_1.
\end{align*}

Now we can decompose $\mu = \mu_\|+\mu_\perp$, where $\mu_\|$ belongs to the column space of $C_2$ and $\mu_\perp$ is perpendicular to the column space of $C_2$. By the property of Moore-Penrose inverse, $C_2^+\mu_\perp = 0$. Therefore,
\begin{align*}
   \|C_1C_2^+\mu\|_1 = \|C_1C_2^+\mu_\|\|_1 
\end{align*}
On the other hand, since $\mu\in \mathbb R^{m-p}$, by Lemma~\ref{lemma:l1decompose}, we have
\begin{align*}
\frac{\sqrt{m-p}+1}{2}\|\mu\|_1\geq \|\mu_\|\|_1,
\end{align*}
which implies that
\begin{align*}
 \|C_1C_2^+\mu_\|\|_1 >\|\mu_\|\|_1.
\end{align*}
Since $\mu_\|$ belongs to the column space of $C_2$, there exists a $u$, such that $C_2 u = \mu_\|$. Therefore, we find a $u$, such that
\begin{align*}
  \|C_1u\|_1 > \|C_2u\|_1.
\end{align*}
\end{proof}

A similar sufficient condition that is easy to check:
\begin{theorem}
Define $\rho$ to be the solution of the following minimization problem:
\begin{align}
   &\mathop{\textrm{minimize}}\limits_{K}&
  & \|K\|_1\nonumber\\
  &\textrm{subject to}&
  & KC = I\label{eq:sufficient}
\end{align}
If for all column of $C$, the sum of the absolute value of the largest $p$ entries is strictly less than $(2\rho)^{-1}$, then for all possible $C_1$, we have
\begin{align*}
  \|C_1 u\|_1 <  \|C_2 u\|_1
\end{align*}

\end{theorem}
\begin{proof}
  \begin{enumerate}
  \item $\|C_1u\|_1 < \|C_2 u\|_1$ is equivalent to $\|C_1 u\|_1 < 0.5 \|C u\|_1$. 
    \item $\|C_1 K\|_1 \leq \|C_1\|_1\|K\|_1\leq \rho \|C_1\|_1$. As a result, if $\|C_1\|_1 < (2\rho)^{-1}$ then $\|C_1 u\|_1 < 0.5 \|C u\|_1$. 
      \item If the maximum $p$-sum of each column is less than $(2\rho)^{-1}$, then $\|C_1\|_1 < (2\rho)^{-1}$ for all possible $C_1$.
  \end{enumerate}
\end{proof}
\emph{This condition seems very strong from simulation result.}
\end{document}
%%% Local Variables:
%%% TeX-command-default: "Latexmk"
%%% End:
