%double column, for final publication of full paper or cdc
%\documentclass[10pt,final,journal,letterpaper]{IEEEtran}
%double column, 9pt, for final publication of technical note 
%\documentclass[9pt,technote,letterpaper]{IEEEtran}
%single column double space, for journal draft
\documentclass[12pt,draftcls,onecolumn]{IEEEtran}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\title{TDMA Draft}

\author{Yilin Mo$^*$
  \thanks{
    $*$: Yilin Mo is with the School of Electrical and Electronic Engineering, Nanyang Technological University, Singapore. Email: {ylmo@ntu.edu.sg}
  }
}

\begin{document} \maketitle

\begin{abstract}
  
\end{abstract}


Let $\lambda_1 > \lambda_2 > 1$. Let $g_t$ be an i.i.d. Bernoulli random variable, such that
\begin{align*}
  P(g_t = 0) = \epsilon.
\end{align*}

Let $u_t = 1$ or $2$ denote our choice at time $t$ to either transmit state $1$ or state $2$. Let us denote $n_{i,t}$ as
\begin{align*}
  n_{i,k} \triangleq |\{k:k\leq t, u_k = i\text{ and }g_k = 1\}|,\,i=1\text{ or }2.
\end{align*}
As a result, $n_{i,k}$ is the number of successful transmission of state $i$ before time $t$. Moreover, denote $n_t = n_{1,t} + n_{2,t}$.

 At time $t$, the mean squared error of state $1$ + state $2$ is given by
\begin{align*}
  \mathbb E \left(\lambda_1^{2t}\alpha^{n_{1,t}}\right) + \mathbb E \left(\lambda_2^{2t}\alpha^{n_{2,t}}\right),
\end{align*}
where $\alpha = \sigma_n^2/(\sigma_n^2 + P)$.

\section*{Oracle case}

Suppose that there is an oracle who tells us that $n_{t}$ at time $0$. In order to minimize the mean squared error, our best choice would be to make\footnote{Notice that we also need to add the requirement that $n_{i,t}$ are non-negative and integers. }
\begin{align}
 \lambda_1^{2t}\alpha^{n_{1,t}} =   \lambda_2^{2t}\alpha^{n_{2,t}}.
  \label{eq:scheduleobj}
\end{align}

Combining with the fact that
\begin{align*}
  n_{1,t} + n_{2,t} = n_t,
\end{align*}
we have
\begin{align}
 \lambda_1^{2t}\alpha^{n_{1,t}} =   \lambda_2^{2t}\alpha^{n_{2,t}} = \lambda_1^t \lambda_2^t \alpha^{n_t/2}.
\end{align}

Therefore, to ensure that the mean squared error is bounded, we need to ensure that
\begin{align}
 2E(\lambda_1^t\lambda_2^t \alpha^{n_t/2}) 
  \label{eq:meansquarederror}
\end{align}
is bounded.

Since $n_t = \sum_{k = 1^t}g_t$, we have that
\begin{align*}
  P(n_t = n) = {t \choose n} (1-\epsilon)^n \epsilon^{t-n}.
\end{align*}

Therefore, \eqref{eq:meansquarederror} can be calculated as
\begin{align*}
 2E(\lambda_1^t\lambda_2^t \alpha^{n_t/2}) &= 2\lambda_1^t\lambda_2^t\sum_{n=0}^t \alpha^{n/2}{t \choose n} (1-\epsilon)^n \epsilon^{t-n}\\
&=2\lambda_1^t\lambda_2^t\left((1-\epsilon)\sqrt{\alpha} + \epsilon\right)^t
\end{align*}
Therefore, the mean squared error is bounded if and only if
\begin{align*}
  \lambda_1\lambda_2\left[(1-\epsilon)\sqrt{\alpha} + \epsilon\right] \leq 1.
\end{align*}
\begin{remark}
  This is one of the necessary condition you derived. I think the other two can be derived once we add the constraint that $n_{i,t}$ are non-negative. Hence, the objective of our TDMA schedule should be  \eqref{eq:scheduleobj}.
\end{remark}

TODO:
\begin{enumerate}
\item add the constraint that $n_{i,t}$ is non-negative
\item more than 2 state
\item general channel model. 
\end{enumerate}
\section*{Non-oracle case using stopping time}

Now assume that we adopt the following TDMA schedule:
\begin{enumerate}
\item Transmit state 1 until $n_1$ successful transmission.
\item Transmit state 2 until the number of successful transmission $n_2$ satisfies
\begin{align}
n_2 \geq n_1 + 2t\times\frac{\log\lambda_1 - \log\lambda_2}{\log \alpha}.
  \label{eq:stoppingcondition}
\end{align}
\end{enumerate}

Suppose the first time that \eqref{eq:stoppingcondition} holds is $T$, which is a random time.

The mean squared error at $T$ is given by (assuming the equality in \eqref{eq:stoppingcondition})
\begin{align*}
\text{MSE} =  2\alpha^{n_1}\mathbb E\left(\lambda_1^{2T}\right).
\end{align*}

We can write $T = T_1 + T_2$, where $T_1$ is the time that $n_1$ successful transmission of state $1$ occurs and $T_2$ is the time that $n_2$ successful transmission of state $2$ occurs. Notice that $T_1$ and $T_2$ are independent of each other. As a result, (notice we use our stopping condition \eqref{eq:stoppingcondition} here)
\begin{align*}
  \text{MSE} = 2\alpha^{n_1}\mathbb E\left(\lambda_1^{2T_1}\right)\mathbb E\left(\lambda_1^{2T_2}\right).
\end{align*}
First notice that $T_2$ is bounded due to the fact that $\lambda_2 < \lambda_1$. Hence, even if all transmission fails, we still have
\begin{align*}
 T_2 \leq \frac{n_1\log \alpha}{2(\log \lambda_2 - \log \lambda_1)} .
\end{align*}

Now, let us define (may need to use the strong Markov property)
\begin{align}
  S_t = \sum_{k = T_1 + 1}^{T_1+t} g_t.
  \label{eq:strongmarkov}
\end{align}
We argue that
\begin{align*}
 \exp(\theta S_t + b t),   
\end{align*}
is a martingale if 
\begin{align*}
  b = -\log (\mathbb E \exp(\theta g_t)) =- \log \left[(1-\epsilon)e^\theta + \epsilon\right].
\end{align*}
Now due to the fact that $T_2$ is bounded, we can use optional sampling theorem on $S_t$, which yields
\begin{align*}
 \mathbb E\exp(\theta S_{T_2} + b T_2 ) = \mathbb E\exp( \theta S_0 + b \times 0) = 1. 
\end{align*}
However, by our stopping condition, we know that (roughly) 
\begin{align}
S_{T_2} = n_2 = n_1 + 2(T_1+T_2)\times\frac{\log\lambda_1 - \log\lambda_2}{\log \alpha}.
  \label{eq:stopping}
\end{align}
Therefore,
\begin{align*}
 \mathbb E \exp(\theta n_1 + \theta \phi(T_1 + T_2) + bT_2 ) = 1,
\end{align*}
where $\phi = 2(\log\lambda_1 - \log\lambda_2)/(\log \alpha) < 0$, which implies that (need more work here)
\begin{align}
 \mathbb E \left\{\exp \left[(\theta\phi +b)T_2\right]|T_1\right\} = \exp(-\theta n_1 - \theta\phi T_1).
  \label{eq:ost}
\end{align}



Now suppose that we choose $\theta$ and $b$ to make $\theta \phi + b = 2\log \lambda_1$. 

\begin{theorem}
  The equation $\theta \phi + b = 2\log \lambda_1$, where $b = - \log \left[(1-\epsilon)e^\theta + \epsilon\right]$, has a unique negative solution.
\end{theorem}
\begin{proof}
   $b$ is a decreasing function of $\theta$, $\phi < 0$. Hence the $\theta\phi + b$ is a decreasing function of $\theta$. When $\theta = 0$, $\theta\phi+b = 0$ and $2\log \lambda_1 > 0$. When $\theta = -\infty$, $\theta\phi + b = + \infty$. Therefore, $\theta\phi + b$ has a unique negative solution.
\end{proof}

Hence, the MSE is given by
\begin{align*}
 \text{MSE} &= 2\alpha^{n_1}\exp(-\theta n_1 )\times \mathbb E \lambda_1^{2T_1}\exp(- \theta\phi T_1)\\
&= 2 \left(\alpha\exp(-\theta)\times \frac{\lambda_1^2\exp(-\theta\phi)(1-\epsilon)}{1-\lambda_1^2\exp(-\theta\phi)\epsilon}\right)^{n_1}
\end{align*}

We know that
\begin{align*}
  \exp(-\theta\phi) =\frac{1}{\lambda_1^2\left[(1-\epsilon)\exp(\theta)+\epsilon\right]} .
\end{align*}
Therefore,
\begin{align*}
&\alpha\exp(-\theta)\times \frac{\lambda_1^2\exp(-\theta\phi)(1-\epsilon)}{1-\lambda_1^2\exp(-\theta\phi)\epsilon} = \alpha \exp(-2\theta).
\end{align*}

Therefore, to ensure mean squared stability, we need to make
\begin{align*}
 \theta > \frac{1}{2}\log\alpha. 
\end{align*}
Now let us check the equation $\theta\phi+b = \log \lambda_1$. If $\theta = 0.5\log\alpha$, then
\begin{align*}
  b = - \log \left[(1-\epsilon)\sqrt{\alpha} + \epsilon\right].
\end{align*}
Therefore,
\begin{align*}
  \theta \phi + b -2 \log \lambda_1 = -log \left[\lambda_1\lambda_2((1-\epsilon)\sqrt{\alpha} + \epsilon)\right].
\end{align*}
Therefore, if $\theta\phi + b - 2\log \lambda_1 > 0$ when $\theta = 0.5 \log \alpha$, then by the fact that $\theta \phi + b$ is decreasing in $\theta$, we know that the solution of $\theta\phi+b = 2\log\lambda_1$ is greater than $0.5 \log\alpha$, which implies that our TDMA scheme is mean square stable. Hence, the mean square stability is equivalent to
\begin{align*}
  \lambda_1\lambda_2((1-\epsilon)\sqrt{\alpha} + \epsilon) < 1.
\end{align*}

Loop holes in the proof:
\begin{enumerate}
\item the equivalence of stopping time and fixed time
\item what if $n_1 + \phi t < 0$ when we transmitted $n_1$ measurements. Hence, the step 2 terminates immediately. (this should give the other inequality in the necessary conditions)
  \item \eqref{eq:stopping} is not exact. (However, the error is bounded unless the above scenario occurs.)
  \item \eqref{eq:strongmarkov} and \eqref{eq:ost}.

\end{enumerate}

\end{document}

%%% Local Variables:
%%% TeX-command-default: "Latexmk"
%%% End: