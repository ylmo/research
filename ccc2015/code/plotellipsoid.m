function [ output_args ] = plotellipsoid(Q,c)
%PLOTELLIPSOID Summary of this function goes here
%   plot a 2 dimensional ellipsoid of the following form (x-c)'Q(x-c)=1
    theta = 0:0.01:2*pi;
    phi = [sin(theta);cos(theta)];
    Qhalf = sqrtm(Q);
    coordinate = inv(Qhalf)*phi;
    plot(coordinate(1,:)+c(1),coordinate(2,:)+c(2));
end

