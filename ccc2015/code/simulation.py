import numpy as np
from numpy.random import rand
from numpy import zeros, eye, sqrt, sin, cos, matrix, linspace
from numpy.linalg import inv, svd

import matplotlib
import matplotlib.pyplot as plt
import seaborn #for the sake of appearance

from itertools import combinations

import cvxpy as cvx

# returns the boundary of an ellipse: (x-c)'Q(x-c) == 1
def convert2ellipse(c, Q):
    if c.shape == (2, 1) and Q.shape == (2, 2):
        Q = (Q + Q.T)/2
        U, s, V = svd(Q)
        theta = linspace(0, 2 * np.pi, 360)
        ex = U[0, 0] * cos(theta) / sqrt(s[0]) + U[0, 1] * sin(theta) / sqrt(s[1]) + c[0, 0]
        ey = U[1, 0] * cos(theta) / sqrt(s[0]) + U[1, 1] * sin(theta) / sqrt(s[1]) + c[1, 0]
        return ex, ey
    else:
        return None, None

#static parameters
n = 2
m = 4
l = 1
delta = 1.
H = matrix([
    [1, 0],
    [0, 1],
    [1, 1],
    [1, -1]
], 'f')
G = matrix(eye(m))
#generate x, w, a, y randomly
x = matrix(rand(n, 1))
w = matrix(rand(m, 1))
while np.linalg.norm(w) >= 1:
    w = rand(m, 1)

a = matrix([0, 0, 0, 1.5]).T
y = H * x + G * w + a

# Or in this simulation we will use the worst-case y
y = matrix('-0.851;2.753;0.5257;0')

OmegaList = []
for indexset in combinations(range(m), m - l):
    HI = H[indexset, :]
    GI = G[indexset, :]
    yI = y[indexset, :]
    XiI = GI * GI.T
    invXiI = inv(XiI)
    PI = inv(HI.T * invXiI * HI)
    KI = PI * HI.T * invXiI
    tmp = eye(m-l) - HI * KI
    UI = tmp.T * invXiI * tmp
    hatxI = KI * yI
    epsilonI = yI.T * UI * yI
    if delta**2 >= epsilonI: #if the ellipsoid exists
        # Generate the Omega from Theorem 4
        Omega = np.bmat([
            [inv(PI), -1 * inv(PI) * hatxI, zeros((n, n))],
            [-1 * hatxI.T * inv(PI), hatxI.T * inv(PI) * hatxI + epsilonI-delta**2, zeros((1, n))],
            [zeros((n, 2 * n + 1))]
        ])
        OmegaList.append(Omega)
        #Draw the corresponding ellipse
        Q = inv(PI)/(delta ** 2 - epsilonI)
        ex, ey = convert2ellipse(hatxI, Q)
        plt.fill(ex, ey)

# convex optimization problem described in (20)
tau = cvx.Variable(len(OmegaList))
varphi = cvx.Variable()
hatx = cvx.Variable(n)
# X correspond the the matrix
# |  I     -hatx     0  |
# |-hatx'  varphi  hatx'|
# |  0      hatx    -I  |
X = cvx.Symmetric(2*n+1)
constraints = [varphi >= 0,
               tau >= 0,
               X[:n, :n] == eye(n),
               X[:n, n+1:] == zeros((n, n)),
               X[:n, n] == -hatx,
               X[n, n] == -varphi,
               X[n+1:, n] == hatx,
               X[n+1:, n+1:] == -1 * eye(n),]
obj = cvx.Minimize(varphi)
for i in range(len(OmegaList)):
    constraints.append(tau[i] * OmegaList[i] - X >> 0)
prob = cvx.Problem(obj, constraints)
prob.solve()

#plot the corresponding ellipse and its center
ex, ey = convert2ellipse(hatx.value, eye(n)/varphi.value)
plt.plot(ex, ey, 'r--', hatx.value[0], hatx.value[1], 'ro')

# Compute the state estimation given by (21)
hatx = inv(H.T * inv(G*G.T) * H) * H.T * inv(G*G.T) * y

# Find the maximum error, i.e., the minimum radius circle
tau = cvx.Variable(len(OmegaList))
varphi = cvx.Variable()
X = cvx.Symmetric(2*n+1)
constraints = [varphi >= 0,
               tau >= 0,
               X[:n, :n] == eye(n),
               X[:n, n+1:] == zeros((n, n)),
               X[:n, n] == -hatx,
               X[n, n] == -varphi,
               X[n+1:, n] == hatx,
               X[n+1:, n+1:] == -1 * eye(n),]
obj = cvx.Minimize(varphi)
for i in range(len(OmegaList)):
    constraints.append(tau[i] * OmegaList[i] - X >> 0)
prob = cvx.Problem(obj, constraints)
prob.solve(verbose=True)

#plot the corresponding ellipse and its center
ex, ey = convert2ellipse(hatx, eye(n)/varphi.value)
plt.plot(ex, ey, 'k--', hatx[0], hatx[1], 'ks')

plt.axis('equal')
plt.tight_layout()
plt.show()
