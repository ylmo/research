clc
clear
n = 2;
m = 4;
l = 1;
delta = 1;

%generate H,G
H = [1 0;0 1;1 1;1 -1];
G = eye(m);

%generate x, w, a, y
x = rand(n,1);
while true
    w = rand(m,1);
    if norm(w)<= 1
        break
    end
end
a = [0 0 0 1.5]';
y = H*x+G*w+a;

y = [-0.851;2.753;0.5257;0];

%enumerate all possible set
sizeS =  nchoosek(m,l); 
II = [2 3 4;1 3 4; 1 2 4; 1 2 3];

%compute the corresponding matrices
numofellipsoid = 1;
for i = 1:sizeS
    indexset = II(i,:);
    HI = H(indexset,:);
    GI = G(indexset,:);
    yI = y(indexset,:);
    invXiI = inv(GI*GI');
    PI = inv(HI'*invXiI*HI);
    norm(PI,2)
    KI = PI*HI'*invXiI;
    UI = (eye(m-l)-HI*KI)'*invXiI*(eye(m-l)-HI*KI);
    hatxI = KI*yI;
    epsilonI = yI'*UI*yI;
    if delta^2 >= epsilonI %ellipsoid exists
        OmegaI = [inv(PI) -inv(PI)*hatxI zeros(n);
            -hatxI'*inv(PI) hatxI'*inv(PI)*hatxI+epsilonI-delta^2 zeros(1,n);
            zeros(n,2*n+1)];
        ellipsoid(numofellipsoid).Q = inv(PI)/(delta^2-epsilonI);
        ellipsoid(numofellipsoid).c = hatxI;
        ellipsoid(numofellipsoid).Omega = OmegaI;
        ellipsoid(numofellipsoid).index = mat2str(indexset);
        numofellipsoid = numofellipsoid + 1;
    end
end
numofellipsoid = numofellipsoid - 1;

%generate the optimization problem
cvx_begin sdp
    variable tau(numofellipsoid,1)
    variable varphi
    variable c(n,1)
    minimize (varphi)
    subject to
        tau >= 0
        varphi >= 0
        for i = 1:numofellipsoid
            tau(i) * ellipsoid(i).Omega - [eye(n) -c zeros(n,n);-c' -varphi c';zeros(n,n) c -eye(n)] >= 0
        end
cvx_end

a = 1/varphi;
r = sqrt(varphi);
hold on
axis equal
%plot X(y)
for i=1:numofellipsoid
    plotellipsoid(ellipsoid(i).Q,ellipsoid(i).c);
end
%plot the bounding circle
plotellipsoid(a*eye(n),c);

plot(c(1),c(2),'r+');


