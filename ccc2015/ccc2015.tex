\documentclass[english]{cccconf}
\usepackage[comma,numbers,square,sort&compress]{natbib}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usepackage{url}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{assumption}{Assumption}

\theoremstyle{remark}
\newtheorem{remark}{Remark}

\newlength\figureheight
\newlength\figurewidth
\setlength\figureheight{7cm}
\setlength\figurewidth{7cm} 

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\, min\;}     % argmin
\DeclareMathOperator*{\argmax}{arg\, max\;}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\newcommand{\Ic}{{\mathcal I^c}}
\newcommand{\I}{{\mathcal I}}
\newcommand{\J}{{\mathcal J}}
\newcommand{\K}{{\mathcal K}}
\newcommand{\ts}{{\tilde s}}
\newcommand{\tV}{{\tilde V}}

\newcommand{\alert}[1]{\textcolor{red}{#1}}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi

\begin{document}

\title{Multi-dimensional state estimation in adversarial environment}
\author{Yilin Mo\aref{caltech}, 
Richard M. Murray\aref{caltech}}

% Note: the first argument in the \affiliation command is optional.
% It defines a label for the affiliation which can be used in the \aref
% command. If there is only one affiliation for all authors, then the
% optional argument in the \affiliation command should be suppressed,
% and the \aref command should aslo be removed after each author in
% \author command, in this case the affiliation will not be numbered.

\affiliation[caltech]{California Institute of Technology,
1200 E. California Blvd, Pasadena, CA 91125, United States.
        \url{yilinmo@caltech.edu, murray@cds.caltech.edu}}

\maketitle

\begin{abstract}
We consider the estimation of a vector state based on $m$ measurements that can be potentially manipulated by an adversary. The attacker is assumed to have limited resources and can only manipulate up to $l$ of the $m$ measurements. However, it can the compromise measurements arbitrarily. The problem is formulated as a minimax optimization, where one seeks to construct an optimal estimator that minimizes the ``worst-case'' error against all possible manipulations by the attacker and all possible sensor noises. We show that if the system is not observable after removing $2l$ sensors, then the worst-case error is infinite, regardless of the estimation strategy. If the system remains observable after removing arbitrary set of $2l$ sensor, we prove that the optimal state estimation can be computed by solving a semidefinite programming problem. A numerical example is provided to illustrate the effectiveness of the proposed state estimator.
\end{abstract}

\keywords{Security, Estimation}

\footnotetext{This work is supported in part by IBM and UTC through iCyPhy consortium. }%
\section{Introduction}
The increasing use of networked embedded sensors to monitor and control critical infrastructures provides potential malicious agents with the opportunity to disrupt their operations by corrupting sensor measurements. Supervisory Control And Data Acquisition (SCADA) systems, for example, run a wide range of safety critical plants and processes, including manufacturing, water and gas treatment and distribution, facility control, and power grids. A wide variety of motivations exists for launching an attack on the such systems, ranging from financial reasons, e.g., reducing the electricity bill, all the way to terrorism, e.g., threatening the life of possibly an entire population by controlling electricity and other life-critical resources. A successful attack to such kind of systems may significantly hamper the economy, the environment, and may even lead to the loss of human life. The first-ever SCADA system malware (called Stuxnet) was found in July 2010 and rose significant concern about SCADA system security~\cite{Chen2010,Fidler2011}. The research community has acknowledged the importance of addressing the challenge of designing secure detection, estimation and control systems~\cite{challengessecurity}.

We consider a secure estimation problem inspired by security concerns that arise from the possible manipulation of sensor data. We focus our attention on the estimation of a vector state $x\in\mathbb R^n$ from measurements collected by $m$ sensors, with the caveat that the measurements are disturbed by an $L_2$ bounded noise and some of them can be further manipulated by a malicious third party. Limitations in the resources available to the attacker enable it to only manipulate $l$ of the $m$ sensors. However, the attacker has total control over the corrupted sensors, as it can change the measurements of the compromised sensors arbitrarily. To minimize the estimator's performance degradation in the presence of such attacks, we construct minimax estimator that minimize the ``worst-case'' expected cost against all possible noise and attacker's manipulate. 

We show that if the system becomes unobservable after removing $2l$ sensor measurements, then even the optimal state estimator will have a ``worst-case'' unbounded error. For the case where the system remains observable after removing an arbitrary set of $2l$ sensors, we provide the explicit form of the optimal estimation, which is given by the Chebyshev center of a union of ellipsoids, which can be computed via semidefinite programming.

\subsection*{Related Work}
Robust estimators such as M-estimator, L-estimator, R-estimator and etc. have also been extensively studied in the literature~\cite{Kassam1985,robust2006,robust2009}. However, such approaches usually assume that the outliers of the data are generated \emph{independently} by some other probability distribution different from the model assumptions. Furthermore, the robustness is usually measured by breakdown points~\cite{Hampel1971,donoho1983notion} or influence functions~\cite{Hampel1974}. However, the independent assumptions do not hold in security settings. As the attacker can take control over multiple sensors, the compromised measurements from these sensors can be jointly selected by the adversary to maximize the estimation error. As a result, in this paper, we design our estimator to minimize the ``worst-case'' $L_2$ error against all possible attacks. Therefore, the concepts of robustness and security are different from each other. In other words, a robust estimator may not necessarily be secure and thus the techniques developed for robust estimation need to be re-examined before they can be applied in the context of security.

Furthermore, bad data detection and identification techniques, which are based on truncating the ``atypical'' data, have been widely used in large scale systems such as the power grid~\cite{Abur2004}. While such approaches are very successful in detecting and removing random failures, they are not effective against integrity attacks. Liu et al.~\cite{liu2009} illustrate how an adversary can inject a stealthy input into the measurements to change the state estimate, without being detected by the bad data detector. Sandberg et al.~\cite{henrik2010} consider how to find a sparse stealthy input, which enables the adversary to launch an attack with a minimum number of compromised sensors. Xie et al.~\cite{Xie2011} further illustrate that the stealthy integrity attacks on state estimation can lead to a financial gain in the electricity market for the adversary. 

For dynamical systems, a widely used approach is to construct ``failure-sensitive'' filters~\cite{failuredetection}. This detection scheme has been investigated recently in the context of cyber-physical security~\cite{fp-ab-fb:09b,Pasqualetti2011,wirelesscontrol,Fawzi2012}. In these scenarios, the attacker can either arbitrarily perturb the system along certain directions without being detected by any filter or cannot induce any perturbation, without incurring detection. However, in the majority of these contributions, the system model is assumed to be noiseless, which greatly favors the failure detector, since the evolution of the system is deterministic and any deviation from the predetermined trajectory can be detected. A more realistic system model with bounded noise is considered by Pajic et al.~\cite{Pajic2014}. They propose an estimator by solving an $L_0$ norm minimization problem and provide performance bound on the estimation error. In~\cite{Mo2010,moscs10security}, the authors also consider a noisy system, providing an algebraic condition under which an attacker can successfully destabilize the system and characterizing the performance of state estimators in this scenario. 

This paper generalizes our previous works on secure estimation~\cite{Mo2013,Mo2014}, which consider designing the optimal estimator for a scalar state and minimizes the ``worst-case'' mean squared error. In this paper, we derive the optimal estimator for a vector state, with the caveat that the noise is $L_2$ bounded.

The rest of paper is organized as follows: In Section~\ref{sec:preliminary} we provide some preliminary results on the radius and diameter of compact set in Euclidean space. In Section~\ref{sec:problem} we formulate the problem of secure estimation with $l$ manipulated measurements from $m$ total measurements. In Section~\ref{sec:fundamentallimit}, we characterize the performance of the optimal estimator and provide an observability condition under which the estimator can have an unbounded error. In Section~\ref{sec:main}, we provide an algorithm to compute the optimal state estimate via semidefinite programming. In Section~\ref{sec:simulation} we provide a numerical example to illustrate the proposed algorithm. Finally, Section~\ref{sec:conclusion} concludes the paper.
\subsection*{Notation}
Let $x\in \mathbb R^n$ be a vector, then $\|x\|$ is the 2-norm of $x$. $\|x\|_0$ is the zero ``norm'' of $x$, i.e., the number of non-zero entries of $x$. 

All comparisons between matrices are in the positive semidefinite sense.
\section{Preliminary}
\label{sec:preliminary}
A ball $B(x,r) \subset \mathbb R^n$ is defined as 
\begin{displaymath}
  B(x,r)  \triangleq \{x'\in \mathbb R^n: \|x'-x\|_2\leq r\}.
\end{displaymath}
Consider a set $S\subseteq \mathbb R^n$. A ball $B(x,r)$ covers $S$ if and only if $S\subseteq B(x,r)$. For any point $x\in \mathbb R^n$, define
\begin{displaymath}
  \rho(x,S) \triangleq \inf\{r\in \mathbb R^+:\,S\subseteq B(x,r)\}
\end{displaymath}
We will assume that the infimum over an empty set is $\infty$. Hence, if $S$ is unbounded, then $\rho(x,S) = \infty$ for any $x$.

For a bounded set $S$, define the radius $r(S)\in \mathbb R^+$ and Chebyshev center $c(S)\in \mathbb R^n$ of $S$ to be
\begin{align*}
  r(S) &\triangleq \inf_{x\in \mathbb R^n} \rho(x,S),\\
  c(S) &\triangleq \argmin_{x\in \mathbb R^n} \rho(x,S).
\end{align*}
In an essence, $B(c(S),r(S))$ is the smallest radius ball that covers $S$. Notice that $c(S)$ may \emph{not} necessarily belong to $S$. For an unbounded $S$, we define $r(S) = \infty$.

We further define the diameter $d(S)$ of the set $S$ as
\begin{displaymath}
  d(S) \triangleq \sup_{x\in S} \rho(x,S).
\end{displaymath}

Notice that in general $d(S)\neq 2r(S)$. For example, for a equilateral triangle with side length 1, we have $d(S) = 1$, while $r(S) = 1/\sqrt{3}$. In general, the following relation between $r(S)$ and $d(S)$ holds:

\begin{theorem}
  Let $S\subset R^n$ be a non-empty and bounded set, then the following inequalities hold on $r(S)$ and $d(S)$
      \begin{displaymath}
	\frac{d(S)}{2}\leq r(S)\leq\sqrt{\frac{n}{2n+2}}d(S)\leq \frac{1}{\sqrt{2}}d(S).
      \end{displaymath}
  \label{theorem:Jung}
\end{theorem}
\begin{proof}
  The first inequality is due to the fact that $S\subseteq B(c(S),r(S))$, which implies that
  \begin{displaymath}
   d(S)\leq d[B(c(S),r(S))]=2r(S).
  \end{displaymath}
   The second inequality is from Jung's theorem~\cite{Danzer1963}. The third inequality is trivial. 
\end{proof}

\section{Problem Formulation}
\label{sec:problem}
The goal is to estimate the state $x\in \mathbb R^n$ from a vector $y \triangleq [y_1,\ldots,y_m]^T\in\mathbb R^m$ consisting of $m$ sensor measurements $y_i\in\mathbb R$, where the index $i\in\mathcal S \triangleq \{1,2,...,m\}$. The measurements could potentially be compromised by an adversary. Therefore, we assume that $x$ and $y$ satisfies the following equation: 
\begin{equation}
  y = H x + G w+a,
\end{equation}
where $\|w\|\leq \delta$ is the sensor noise, which is assumed to be bounded, and $G\in \mathbb R^{m\times m}$ is assumed to be full rank. The vector $a$ is the bias injected by the attacker. The non-zero entries of $a$ indicates the set of compromised sensors. In this paper, we assume that the attacker can only manipulate up to $l$ sensors. As a result, $\|a\|_0\leq l$.

\begin{remark}
    The parameter $l$ can also be interpreted as a design parameter for the system operator. In general, increasing $l$ will increase the resilience of the estimator under attack. However, a large $l$ could result in performance degradation during normal operation when no sensor is compromised. Therefore, there exists a trade-off between resilience and efficiency (under normal operation), which can be tuned by choosing a suitable parameter $l$.
\end{remark}

Define
\begin{align*}
   H& \triangleq \begin{bmatrix}
    h_1\\
    \vdots\\
    h_m
  \end{bmatrix} ,\, w \triangleq \begin{bmatrix}
    w_1\\
    \vdots\\
    w_m
  \end{bmatrix} ,\, a \triangleq \begin{bmatrix}
    a_1\\
    \vdots\\
    a_m
  \end{bmatrix},
\end{align*}
and define the set $\mathbb Y$ as the set of all possible ``manipulated'' measurements, i.e.,
\begin{align*}
  \mathbb Y  &\triangleq \{y\in \mathbb R^m:\exists \,x,\,w,\,a,\\
  &\text{ such that }\|w\|_2\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{align*}
For any $y\in \mathbb Y$, we can define the set $\mathbb X(y)$ as the set of feasible $x$ that can generate $y$, i.e.,
\begin{align*}
  \mathbb X(y)& \triangleq \{x\in\mathbb R^n:\exists \,w,\,a,\\
  &\text{ such that }\|w\|\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{align*}
An estimator is a function $f:\mathbb Y\rightarrow \mathbb R^n$, where $\hat x = f(y)$. Given $y$, the magnitude of the worst case estimation error is defined as
\begin{displaymath}
  e(y) \triangleq \sup_{x\in\mathbb X(y)} \|f(y)-x\|.
\end{displaymath}

From the definition of the Chebyshev center, we know that the optimal estimator with smallest worst case error $e$ is given by
\begin{displaymath}
  f^*(y) \triangleq c(\mathbb X(y)), 
\end{displaymath}
with worst case error
\begin{align*}
  e(y) = r(\mathbb X(y)).
\end{align*}

Therefore, the worst case error magnitude for all possible $y$ is given by
\begin{displaymath}
  e^* \triangleq \sup_{y\in \mathbb Y} r(\mathbb X(y)).
\end{displaymath}

In the following sections, we provides an upper and lower bound for $e^*$. We further propose an algorithm to compute $c(\mathbb X(y))$ via convex optimization.
\section{Performance Bounds for the Optimal Estimator}
\label{sec:fundamentallimit}
This section is devoted to analyzing the performance of the optimal estimator. To this end, for any index set $\mathcal I = \{i_1,\dots,i_j\}$, define the complement set $\mathcal I^c = \mathcal S\backslash I$ and define subspace $\mathcal V_\mathcal I \triangleq \text{span}(e_{i_1},\dots,e_{i_j})\subseteq \mathbb R^m$, where $e_{i}\in \mathbb R^m$ is the $i$th canonical basis vector. Define the following set:
\begin{align*}
  \mathbb X_{\mathcal I}(y)& \triangleq \{x\in\mathbb R^n:\exists \,w,\,a\in \mathcal V_\Ic,\\
  &\text{ such that }\|w\|_2\leq \delta\text{ and }y=Hx+Gw+a\}.
\end{align*}
Hence, $\mathbb X_{\mathcal I}(y)$ represents all possible states that can generate measurement $y$ when the sensors in $\mathcal I$ are good and the sensors in $\mathcal I^c$ are compromised. By enumerating all possible $\mathcal I$s, it is easy to see that $\mathbb X(y)$ can be written as
\begin{displaymath}
  \mathbb X(y)  = \bigcup_{|\mathcal I|=m-l}\mathbb X_{\mathcal I}(y). 
\end{displaymath}
For any $\mathcal I = \{i_1,\dots,i_j\}$, we can define 
\begin{displaymath}
  H_{\mathcal I} \triangleq \begin{bmatrix}
    h_{i_1}\\
    \vdots\\
    h_{i_j}
  \end{bmatrix},\, G_{\mathcal I} \triangleq \begin{bmatrix}
    g_{i_1}\\
    \vdots\\
    g_{i_j}
  \end{bmatrix},\,y_{\mathcal I}\triangleq \begin{bmatrix}
    y_{i_1}\\
    \vdots\\
    y_{i_j}
  \end{bmatrix},
\end{displaymath}
where $g_i$ is the $i$th row vector of $G$.
\begin{align*}
  F_{\mathcal I} &\triangleq G_{\I}G_{\I}^T.
\end{align*}
Since $G$ is full rank, $G_\I$ is full row rank, which implies that $F_{\I}$ is full rank. Thus, if $H_{\mathcal I}$ is full column rank, we can define
\begin{align*}
  K_{\mathcal I}&\triangleq \left(H_{\mathcal I}^TF_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1}H^T_{\mathcal I}F_{\mathcal I}^{-1},\\
  P_{\mathcal I} &\triangleq \left(H_{\mathcal I}^T F_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1},\\
  U_{\mathcal I} &\triangleq (I-H_{\mathcal I}K_{\mathcal I})^T F_{\mathcal I}^{-1}(I-H_{\mathcal I}K_{\mathcal I}).
\end{align*}
The following theorem provides bounds on $e^*$:
\begin{theorem}
  If there exists an index set $\mathcal K\subset \mathcal S$ with cardinality $m-2l$, such that $H_{\mathcal K}$ is not of full column rank, then $e^* = \infty$. If for all $|\mathcal K| = m-2l$, $H_{\mathcal K}$ is full column rank, then for all possible $y\in \mathbb Y$, we have
  \begin{equation}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) = 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
    \label{eq:diameter}
  \end{equation}
  Therefore, $e^*$ satisfies
  \begin{equation}
    \max_{|\mathcal K| = m-2l}\delta\sqrt{\sigma(P_{\mathcal K})}\leq e^*\leq \max_{|\mathcal K| = m-2l}\delta\sqrt{2\sigma(P_{\mathcal K})},
    \label{eq:radius}
  \end{equation}
  where $\sigma(P)$ is the spectral radius of $P$.
  \label{theorem:fundamentallimit}
\end{theorem}
Before proving Theorem~\ref{theorem:fundamentallimit}, we need the following lemma:
\begin{lemma}
  \label{lemma:moreinfomationisgood}
  If $\K_1\subseteq \K_2 \subseteq \mathcal S$ and $H_{\K_1}$ is full column rank, then the following statement holds:
  \begin{enumerate}
    \item $H_{\K_2}$ is also full column rank.
    \item $P_{\K_1}\geq P_{\K_2}$.
  \end{enumerate}
\end{lemma}
\begin{proof}
  Without loss of generality, let us assume that
  \begin{align}
    H_{\K_2} = \begin{bmatrix}
      H_{\K_1}\\
      H_{\K_2}
    \end{bmatrix}.
    \label{eq:blockH}
  \end{align}
   Therefore, 
  \begin{align*}
    n\geq \text{rank}(H_{\K_2})\geq \text{rank}(H_{\K_1}) = n.
  \end{align*}
  Hence, $H_{\K_2}$ is full column rank, which implies that $P_{\K_2}$ is well-defined.

  To prove $P_{\K_1}\geq P_{\K_2}$, we only need to prove that
  \begin{align}
    H_{\K_1}^T F_{\K_1}^{-1}H_{\K_1}\leq  H_{\K_2}^T F_{\K_2}^{-1}H_{\K_2}.
    \label{eq:moreinformationisgood}
  \end{align}
  From definition of $F_\I$, we can write $F_{\K_2}$ as
  \begin{align*}
    F_{\K_2} = \begin{bmatrix}
      F_{11}&F_{12}\\
      F_{12}'&F_{22},
    \end{bmatrix}
  \end{align*}
  where $F_{11} = F_{\K_1}$. Using Schur complements, we have
  \begin{align*}
    F_{\K_2}^{-1} &= \begin{bmatrix}
      F_{11}^{-1}&0\\
      0&0
    \end{bmatrix} \\
    &+\begin{bmatrix}
      F_{11}^{-1}F_{12}\\
      I
    \end{bmatrix} \left( F_{22}-F_{12}'F_{11}^{-1}F_{12} \right)^{-1} \begin{bmatrix}
      F_{12}' F_{11}^{-1}& I
    \end{bmatrix} 
  \end{align*}
  Combining with \eqref{eq:blockH}, we can prove \eqref{eq:moreinformationisgood}.
\end{proof}
We are now ready to prove Theorem~\ref{theorem:fundamentallimit}:
\begin{proof}[Proof of Theorem~\ref{theorem:fundamentallimit}]
  We first prove \eqref{eq:diameter}. Suppose that for all $\mathcal K\subset \mathcal S$ with cardinality $m-2l$, $H_{\mathcal K}$ is full column rank. Let us consider a pair of set $\mathcal I,\,\mathcal J$ with cardinality $m-l$. Define $\mathcal K$ as
  \begin{displaymath}
   \mathcal K =  \mathcal I\bigcap \mathcal J = \mathcal S\backslash\left(\mathcal I^c\bigcup\mathcal J^c\right)
  \end{displaymath}
  Clearly, $|\mathcal K|\geq m-2l$ and it includes a index set of size $m-2l$. Hence, by Lemma~\ref{lemma:moreinfomationisgood}, $H_\K$ is also full column rank. Now for any point $x_1 \in \mathbb X_\I(y)$ and $x_2 \in \mathbb X_\J(y)$, we have:
  \begin{equation}
    Hx_1 +G w_1 + a_1 = H x_2 + Gw_2 + a_2 = y,  
    \label{eq:difference1}
  \end{equation}
  where $a_1\in \mathcal V_\Ic$ and $a_2\in \mathcal V_{\mathcal J^c}$. Since both $a_1$ and $a_2$ have zero entries on the $i$th entry, where $i\in \mathcal K$, \eqref{eq:difference1} implies that
  \begin{equation}
    H_{\mathcal K}x_1+ G_{\mathcal K}w_1)= H_{\mathcal K}x_2+ G_{\mathcal K}w_2).
    \label{eq:difference2}
  \end{equation}
  which implies that
  \begin{align}
   x_1-x_2 = K_\K G_\K (w_2-w_1). 
    \label{eq:difference3}
  \end{align}
  By the fact that $\|w_2-w_1\|_2\leq 2\delta$, we have
  \begin{displaymath}
    \|x_1-x_2\|\leq 2\|K_\K G_\K\|\delta = 2\delta\sqrt{\sigma(P_\K)},
  \end{displaymath}
  where $\|K_\K G_\K\|$ is the largest singular value of $K_\K G_\K$. Therefore, by Lemma~\ref{lemma:moreinfomationisgood}, we have
  \begin{align*}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) &\leq 2\delta\max_{|\mathcal K| \geq m-2l}\sqrt{\sigma(P_{\mathcal K})}\\
    &= 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
  \end{align*}
  Now we need to prove that the equality of \eqref{eq:diameter} holds. Suppose that we find $x_1,\,x_2$ and $\|w_1\|,\|w_2\|\leq \delta$ that satisfies \eqref{eq:difference2} and $\|x_1-x_2\| = 2\sqrt{\sigma(P_{\mathcal K})}$. We know that
  \begin{align}
    H_{\mathcal K} x_1 + G_{\mathcal K}w_1 =  H_{\mathcal K} x_2 + G_{\mathcal K}w_2.
    \label{eq:constructy}
  \end{align}
  Therefore, let us create a $y$, such that
  \begin{align*}
    y_{\mathcal K} &= H_{\mathcal K}x_1 + G_\K w_1,\\
    y_{\mathcal I\backslash \K} &= H_{\mathcal I\backslash \K}x_1 + G_{\I\backslash \K} w_1,\\
    y_{\mathcal J\backslash\mathcal K} &= H_{\mathcal J\backslash\mathcal K}x_2 + G_{\J\backslash\K}w_2,\\
    y_{\mathcal S\backslash (\I\cup\J)}&=0.
  \end{align*}
  Thus,
  \begin{align*}
    y_{\mathcal K} - H_\K x_1 &=  G_\K w_1, \\
    y_{\mathcal I\backslash \K} - H_{\mathcal I\backslash \K}x_1 &= G_{\I\backslash \K}w_1,
  \end{align*}
  which implies that $x_1\in \mathbb X_\I(y)$. On the other hand,
  \begin{align*}
    y_{\mathcal K} - H_\K x_2 &= H_\K(x_1-x_2)+ G_\K w_1 = G_\K w_2, \\
    y_{\mathcal J\backslash \K} - H_{\mathcal J\backslash \K}x_2 &= G_{\J\backslash\K}w_2,
  \end{align*}
  which implies that $x_2\in \mathbb X_\J(y)$. Therefore, \eqref{eq:diameter} holds. \eqref{eq:radius} can be proved by applying Theorem~\ref{theorem:Jung}.

 Now suppose there exists an $|\K|=m-2l$, such that $H_\K$ is not full column rank. We can find index set $\I,\J$, such that $|\I|=|\J|=m-l$ and $\I\bigcap \J = \K$. Furthermore, we know that there exists $x_1\neq 0$, such that
 \begin{align*}
   H_\K x_1 = 0. 
 \end{align*}
 As a result, if we choose $x_2 = 0$, $w_1=w_2=0$, then \eqref{eq:constructy} holds for $x_1,x_2,w_1,w_2$. Now by the similar argument, we can construct a $y$, such that $x_1\in \mathbb X_{\mathcal I}(y)$ and $x_2=0\in \mathbb X_{\mathcal J}(y)$. Moreover, by linearity, we know that
 \begin{align*}
   \alpha x_1 \in \mathbb X_{\I}(\alpha y) ,\,0 = \alpha x_2 \in \mathbb X_\J(\alpha y).
 \end{align*}
 Hence, by Theorem~\ref{theorem:Jung},
 \begin{align*}
   e^*\geq\sup_{y\in \mathbb Y} \frac{d(\mathbb X(y))}{2} \geq \sup_{\alpha\in \mathbb R}\frac{\|\alpha x_1\|}{2} = \infty.
 \end{align*}
\end{proof}
\section{Estimator Design}
\label{sec:main}
In this section, we first characterize the shape of $\mathbb X_{\mathcal I}(y)$:
\begin{theorem}
  Define the function $V_{\I}(x):\mathbb R^n\rightarrow \mathbb R$ as the solution of the following optimization problem:
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w\in \mathbb R^m}&
    & \|w\|^2\nonumber\\
    &\textrm{subject to}&
    & G_\I w= y_{\I}-H_\I x.  \label{eq:optstatic}
  \end{align}
  Then $V_\I(x)$ is given by
  \begin{equation}
    V_\I(x) = (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))+ \varepsilon_{\I}(y) ,
    \label{eq:valuefunctionstatic}
  \end{equation}
  where
  \begin{equation}
     \hat x_{\I}(y) = K_\I y_\I,
    \label{eq:optstaticsolution}
  \end{equation}
  and 
  \begin{equation}
    \label{eq:optstaticvalue}
    \varepsilon_{\I}(y) = y_{\I}^TU_{\I}y_{\I}.
  \end{equation}
  \label{theorem:valuefunctionstatic}
\end{theorem}
\begin{proof}
  Consider the constraint of the optimization problem~\eqref{eq:optstatic}
  \begin{equation}
    y_{\I} - H_{\I}x = G_{\I}w.
    \label{eq:reducedequation}
  \end{equation}
  As $G$ is full row rank, $G_{\I}$ is also full row rank. Consider the singular value decomposition of $G_\I$, we get
  \begin{displaymath}
    G_\I = Q_1 \begin{bmatrix} 
      \Lambda & \mathbf 0
    \end{bmatrix} Q_2,
  \end{displaymath}
  where $Q_1,Q_2$ are orthogonal matrices with proper dimensions and $\Lambda$ is an invertible and diagonal matrix. Hence, \eqref{eq:reducedequation} implies that
  \begin{equation}
    \Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I  x = \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation2}
  \end{equation}
  where $v = Q_2 w$ and $\|v\|  = \|w\|$. By projecting $\Lambda^{-1} Q_1^T y_\I$ into the subspace $\text{span}(\Lambda^{-1}Q_1^T H_\I)$, we have
  \begin{align}
    \left[\Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I \hat x_{\I}(y)\right] &+ \Lambda^{-1} Q_1^T H_\I \left[x - \hat x_{\I}(y) \right]\nonumber\\
    &= \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation3}
  \end{align}
  The first term on the LHS of \eqref{eq:reducedequation3} is perpendicular to the second term. Thus, \eqref{eq:reducedequation3} is equivalent to
  \begin{align*}
   \varepsilon_\I(y) &+  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))\\
   &= \left\|\begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v\right\|^2 \leq \|v\|^2 = \|w\|^2.
  \end{align*}
  Clearly, the equality holds when $v = \begin{bmatrix}v_1,\dots,v_{|\I|},0,\dots,0\end{bmatrix}$. Hence
  \begin{displaymath}
    V_{\I}(x)= \varepsilon_\I(y) +  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y)).
  \end{displaymath}
\end{proof}

By Theorem~\ref{theorem:valuefunctionstatic}, we immediately have the following corollary:
\begin{corollary}
  If $\varepsilon_\I(y) > \delta^2$, then $\mathbb X_{\I}(y)$ is an empty set. Otherwise, $\mathbb X_{\I}(y)$ is an ellipsoid given by
  \begin{equation}
    \mathbb X_{\I}(y) = \{x:(x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))\leq \delta^2 - \varepsilon_{\I}(y)\} .
  \end{equation}
\end{corollary}
\begin{proof}
  By definition, $x\in \mathbb X_{\I}(y)$ is equivalent to the existence of $w$, such that $\|w\|\leq \delta$ and
  \begin{displaymath}
    y_{\I} = H_{\I}x + G_{\I}w.
  \end{displaymath}
  Hence, the corollary holds by Theorem~\ref{theorem:valuefunctionstatic}. 
\end{proof}
\begin{remark}
  One can view $\varepsilon_{\I}(y)$ as the deviation of the measurement from the attack model. If $\varepsilon_\I(y)\geq \delta^2$, i.e., the deviation cannot be explained by the noise, then $\mathbb X_\I(y)$ is empty, which implies that the good sensor set is not $\I$.
\end{remark}
Let us define set as
\begin{align}
  \mathfrak I \triangleq \{\mathcal I\subset \mathcal S:|\I|=m-l,\,\delta^2\geq \varepsilon_\I(y)\} .
  \label{eq:feasibleindexset}
\end{align}
Since $\mathbb X(y) = \bigcup_{|\I|=l}\mathbb X_\I(y) = \bigcup_{\I\in \mathfrak I} \mathbb X_\I(y)$, we know that $\mathbb X(y)$ is a union of ellipsoids. To check if a ball covers a union of ellipsoids, we have the following theorem~\cite{Yildirm2006}:
\begin{theorem}
  A ball $B(x,r)$ covers $\mathbb X(y)$ if and only if for every index set $|\I| = m-l$, such that
  \begin{align*}
   \delta^2-\varepsilon_\I(y)\geq 0, 
  \end{align*}
  there exists $\tau_\I\geq 0$, such that
  \begin{align}
    \tau_\I\Omega_\I\geq \begin{bmatrix}
      I&-x&0\\
      -x^T&-r^2&x^T\\
      0&x&-I
    \end{bmatrix},
    \label{eq:coveringball}
  \end{align}
  where $\Omega_\I$ is defined as,
  \begin{align*}
    &\Omega_\I =\\
    &\begin{bmatrix}
      P_\I^{-1}&-P_\I^{-1}\hat x_\I(y)&0\\
      -\hat x_\I(y)^TP_\I^{-1}&\hat x_\I(y)^TP_\I^{-1}\hat x_\I(y)+\varepsilon_\I(y)-\delta^2&0\\
      0&0&0
    \end{bmatrix}.
  \end{align*}
  \label{theorem:coveringball}
\end{theorem}
\begin{proof}
  This theorem can be proved by Lemma~2.8 in \cite{Yildirm2006}.
\end{proof}

Therefore, we can derive the optimal state estimate as the solution of the following semidefinite programming problem:
\begin{align}
  &\mathop{\textrm{minimize}}\limits_{\hat x,\varphi,\tau_\I}&
  & \varphi \label{eq:optimization}\\
  &\textrm{subject to}&
  &  \varphi \geq 0,\nonumber\\
  &&
  &\tau_\I\geq 0,\,\forall \I\in \mathfrak I,\nonumber\\
  &&
  &\tau_\I  \Omega_\I\geq \begin{bmatrix}
    I&-\hat x&0\\
    -\hat x^T&-\varphi&\hat x^T\\
    0&\hat x&-I
  \end{bmatrix},\forall \I\in\mathfrak I\nonumber.
\end{align}
where the radius of the Chebyshev ball is $r = \sqrt{\varphi}$.

In conclusion, the optimal state estimation can be computed via the following algorithm:
\begin{enumerate}
  \item Enumerate all possible $|\mathcal I| = m-l$, compute $\hat x_\I(y)$ and $\varepsilon_\I(y)$ via \eqref{eq:optstaticsolution} and \eqref{eq:optstaticvalue}.
  \item Check whether $\varepsilon_\I(y)$ is no greater than $\delta^2$. Compute the index set $\mathfrak I$ via \eqref{eq:feasibleindexset}.
  \item Solve the optimization problem~\eqref{eq:optstatic}.
\end{enumerate}

\section{Numerical Example}
\label{sec:simulation}
In this section, we provide a numerical example to illustrate our estimator design. We assume that $n = 2$, $m = 4$ and one sensor is compromised. The noise is assume to satisfy $\|w\|\leq \delta = 1$. We further assume that
\begin{align*}
  H = \begin{bmatrix}
    1 & 0\\
    0 & 1\\
    1 & 1\\
    1 & -1
  \end{bmatrix}, G = I.
\end{align*}
It is easy to check that for any $|K| = m-2l = 2$, $H_\K$ is full column rank.

We first consider the worst case performance of our estimator. One can verify that 
\begin{align*}
  \max_{|\K|=2} \sigma(P_\K) = 2.618
\end{align*}
where the corresponding $\K = \{1,3\}$. Using the procedure described in the proof of Theorem~\ref{theorem:fundamentallimit}, we choose $\I = \{1,2,3\},\,\J = \{1,3,4\}$. We can then construct the following variables:
\begin{align*}
  x_1 = \begin{bmatrix}
    -1.701\\
    2.753
  \end{bmatrix},\,w_1 =  \begin{bmatrix}
    0.8507\\
    0\\
    -0.5257\\
    0
  \end{bmatrix}.
\end{align*}
and
\begin{align*}
  x_2 = 0,\, w_2 =  \begin{bmatrix}
   - 0.8507\\
    0\\
    0.5257\\
    0
  \end{bmatrix}.
\end{align*}
The corresponding $y$ is given by
\begin{align*}
y = \begin{bmatrix}
    -0.851\\
    2.753\\
    0.5257\\
    0
  \end{bmatrix}.
\end{align*}
The optimal state estimate $\hat x$ and worst-case error $e(y)$ is given by 
\begin{align*}
  \hat x=\begin{bmatrix}
    -0.851\\
    1.376
  \end{bmatrix},\,e(y) = 1.618.
\end{align*}
On the other hand, if the system operator is unaware of the existence of the adversary, it is easy to prove that the optimal estimator designed for the non-adversarial environment is given by
\begin{align}
  \label{eq:optestimationnonadversarial}
  \hat x = \left( H^T (GG^T)^{-1}H\right)^{-1}H^T(GG^T)^{-1} y.
\end{align}

For our case, the state estimate given by \eqref{eq:optestimationnonadversarial} is
  \begin{align*}
  \hat x= \begin{bmatrix}
    -0.108\\
    1.093
  \end{bmatrix},
\end{align*}
for which the worst case error $e(y) = 2.306$.
\begin{figure}[htpb]
  \begin{center}
    \inputtikz{worstcase}
  \end{center}
  \caption{The performance of the optimal state estimator. The green ellipse corresponds to $\mathbb X_{\{1,3,4\}}(y)$ and the red ellipse corresponds to $\mathbb X_{\{1,2,3\}}(y)$. The set $\mathbb X_{\{2,3,4\}}(y)$ and $\mathbb X_{\{1,2,4\}}(y)$ is empty. The black ``+'' is the optimal state estimate while the black dashed line is the Chebyshev ball for $\mathbb X(y)$. The red ``$\times$'' corresponds to the output of the optimal state estimator designed for benign sensors and the red dashed line is the minimum covering ball of $\mathbb X(y)$ centered at ``$\times$''.}
  \label{fig:worstcase}
\end{figure}

\section{Conclusion and Future Work}
\label{sec:conclusion}
We consider the estimation of a vector state $x$ based on $m$ measurements, where $l$ of them are malicious and can be changed arbitrarily by an adversary. We prove that if the system is not observable after removing $2l$ sensor measurements, then the attacker can make the worst case estimation error to be infinite. On the other hand, we provides upper and lower bound for the worst case estimation error when the system remains observable after removing any set of $2l$ sensors. We then derive the optimal state estimation as the solution of a semidefinite programming problem.

In the future, we want to find a near optimal state estimator with lower computation complexity. Furthermore, we would like to consider stochastic noise models. 
\bibliographystyle{IEEEtran}
\bibliography{secureestimation}

\end{document}
