\documentclass{article}
\usepackage{fullpage}
\usepackage{hyperref}
\usepackage{subfigure}
\usepackage{color}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\newcommand{\der}{\frac{\text{d}}{\text{d}t}}
\newcommand{\NTU}{\text{NTU}}
\newcommand{\UA}{\text{UA}}
\newcommand{\MM}{\text{MM}}
\newcommand{\hA}{\text{hA}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\title{Air Management System (AMS) Model Description}

\author{}

\begin{document} \maketitle
\section{Component Description}
We describe each component of the AMS.
\subsection{Valve}
Input variables:
\begin{enumerate}
  \item Inlet pressure $P_i$, unit Pa.
  \item Inlet temperature $T_i$, unit K.
  \item Inlet mass flow rate $W_i$, unit kg/s.
\end{enumerate}
Output variables:
\begin{enumerate}
  \item Outlet pressure $P_o$, unit Pa.
  \item Outlet temperature $T_o$, unit K.
  \item Outlet mass flow rate $W_o$, unit kg/s.
\end{enumerate}
Parameters:
\begin{enumerate}
  \item Maximum valve coefficient $C_{max}$, unit $\text{m}\cdot \text{s}\cdot \text{K}^{0.5}$.
\end{enumerate}
Controlled variables:
\begin{enumerate}
  \item Targeted valve coefficient $C_t$, ranging from $0$ to $C_{max}$, unit $\text{m}\cdot \text{s}\cdot \text{K}^{0.5}$.
\end{enumerate}
State variables:
\begin{enumerate}
  \item True valve coefficient $C$, ranging from $0$ to $C_{max}$, unit $\text{m}\cdot \text{s}\cdot \text{K}^{0.5}$.
\end{enumerate}
Algebraic Equations:
\begin{enumerate}
  \item 
    \begin{displaymath}
     T_o = T_i. 
    \end{displaymath}
  \item 
    \begin{displaymath}
  W_o =  W_i = \begin{cases}
    0& P_o\geq P_i\\
	4.72\times 10^{-4}\times C\left( P_i+2P_o \right)\sqrt{\frac{1}{T_i}\left( 1-\frac{P_o}{P_i} \right)}& 0.5P_i \leq P_o  <   P_i\\
	6.67\times 10^{-4}\times CP_i\sqrt{\frac{1}{T_i}}&  P_o < 0.5 P_i \\
      \end{cases}.
    \end{displaymath}
    \alert{Assuming no backward flow.}
  \item There exists a PID controller that takes the input $C_t$ and changes the valve coefficient $C$. Hence, $C$ and $C_t$ satisfy a differential equation. However, in this draft, it is assumed that the time constant of PID controller is small enough such that
    \begin{displaymath}
     C = C_t. 
    \end{displaymath}
\end{enumerate}
\subsection{Container}

Input variables:
\begin{enumerate}
  \item Inlet pressure $P_i$, unit Pa.
  \item Inlet temperature $T_i$, unit K.
  \item Inlet mass flow rate $W_i$, unit kg/s.
\end{enumerate}
Output variables:
\begin{enumerate}
  \item Outlet pressure $P_o$, unit Pa.
  \item Outlet temperature $T_o$, unit K.
  \item Outlet mass flow rate $W_o$, unit kg/s.
\end{enumerate}
Parameters:
\begin{enumerate}
  \item  Volume $V$, unit $\text{m}^3$.
\end{enumerate}
State variables:
\begin{enumerate}
  \item Temperature of the air inside the container $T$, unit K.
  \item Pressure of the air inside the container $P$, unit Pa. 
\end{enumerate}
Other derived variables:
\begin{enumerate}
  \item Thermal energy of the air inside the container $Q$, unit J.
  \item Mass of the air inside the container $m$, unit kg. 
\end{enumerate}
Algebraic equations:
\begin{enumerate}
  \item 
    \begin{displaymath}
     P_i = P_o = P. 
    \end{displaymath}
  \item
    \begin{displaymath}
     T_o = T. 
    \end{displaymath}
  \item 
    \begin{displaymath}
      P = \frac{mTR}{VM},
    \end{displaymath}
    where $R$ is the ideal gas constant and $M$ is the molar mass of air.
  \item 
    \begin{displaymath}
      Q = C_{v}mT, 
    \end{displaymath}
    where $C_{v}$ is the heat capacity of air.
\end{enumerate}
Differential equations:
\begin{enumerate}
  \item
    \begin{displaymath}
     \der m = W_i - W_o. 
    \end{displaymath}
  \item 
    \begin{displaymath}
      \der Q = C_{v} W_i T_i - C_{v} W_o T_o.
    \end{displaymath}
\end{enumerate}

\subsection{Heat Exchanger(HX)}
Input variables:
\begin{enumerate}
  \item Hot air inlet pressure $P_{hi}$, unit Pa.
  \item Hot air inlet temperature $T_{hi}$, unit K.
  \item Hot air inlet mass flow rate $W_{hi}$, unit kg/s.
  \item Cold air inlet pressure $P_{ci}$, unit Pa.
  \item Cold air inlet temperature $T_{ci}$, unit K.
  \item Cold air inlet mass flow rate $W_{ci}$, unit kg/s.
\end{enumerate}
Output variables:
\begin{enumerate}
  \item Hot air outlet pressure $P_{ho}$, unit Pa.
  \item Hot air outlet temperature $T_{ho}$, unit K.
  \item Hot air outlet mass flow rate $W_{ho}$, unit kg/s.
  \item Cold air outlet pressure $P_{co}$, unit Pa.
  \item Cold air outlet temperature $T_{co}$, unit K.
  \item Cold air outlet mass flow rate $W_{co}$, unit kg/s.
\end{enumerate}
Parameters:
\begin{enumerate}
\item Number of tubes per row $N_t$
\item Number of rows $N_r$
\item Total number of tubes $N$
\item Width $W$, height $H$ and length $L$ of the HX, unit m.
\item Diameter of the tube $D$, unit m.
\item Surface area of the tube $A$, unit m$^2$.
\item Heat transfer coefficient for hot air $\hA_h$, unit J/K.
\item Heat transfer coefficient for hot air $\hA_C$, unit J/K.
\item Mass of the HX $\MM$, unit kg.
\item Heat capacity of the material of the HX $C_{pm}$, unit $\text{J}/(\text{kg}\cdot\text{K})$.
\end{enumerate}
Controlled variables:
\begin{enumerate}
  \item Cold air flow rate $W_c$, unit kg/s.
\end{enumerate}
State variables:
\begin{enumerate}
  \item Hot air outlet temperature $T_{ho}$, unit K.
  \item Cold air outlet temperature $T_{co}$, unit K.
\end{enumerate}
Other derived variables:
\begin{enumerate}
  \item Density of the hot air $\rho_h$, unit kg/$\text{m}^3$.
  \item Density of the cold air $\rho_c$, unit kg/$\text{m}^3$.
\item kinematic viscosity of the hot air $\nu_h$, unit m$^2$/s.
\item kinematic viscosity of the cold air $\nu_c$, unit m$^2$/s.
\item Reynolds number of the hot air $Re_h$, no unit.
\item Reynolds number of the cold air $Re_c$, no unit.
\item velocity of the hot air flow $v_h$, unit m/s.
\item velocity of the cold air flow $v_c$, unit m/s.
\item $\UA$, unit J/K.
\item $\NTU$, no unit.
\item $C_r$, no unit.
\item $\epsilon$, no unit.
\item $T_{hs}$, unit K.
\item $T_{cs}$, unit K.
\end{enumerate}
Static equations:
\begin{enumerate}
\item 
\begin{displaymath}
D = W/N_t.
\end{displaymath}
\item 
\begin{displaymath}
N_r = H/(2D).
\end{displaymath}
\item 
\begin{displaymath}
  A = \pi DL\times N = \frac{\pi WHL}{2D}.
\end{displaymath}
\item 
\begin{displaymath}
  N = N_t\times N_r = \frac{WH}{2D^2}.
\end{displaymath}
\item
\begin{displaymath}
  \frac{1}{\UA} =\frac{1}{\hA_h}+\frac{1}{\hA_c}.
\end{displaymath}
\item \alert{missing the relation between $A$ and the heat transfer coefficients.}
\end{enumerate}
Algebraic equations:
\begin{enumerate}
\item
  \begin{displaymath}
    w_{ho} = w_{hi}.
  \end{displaymath}
  \item \begin{displaymath}
      \rho_h = \frac{P_{hi}M}{RT_{hi}}, 
    \end{displaymath}
    where $M$ is the molar of air and $R$ is the ideal gas constant.
  \item \begin{displaymath}
      v_h = \frac{4w_{hi}}{\pi D^2\rho_h N}. 
    \end{displaymath}
  \item \href{http://en.wikipedia.org/wiki/Viscosity\#Effect_of_temperature_on_the_viscosity_of_a_gas}{Sutherland's Formula}:
    \begin{displaymath}
      \nu_h =1.512\times 10^{-6} \frac{T_{hi}^{1.5}}{\rho_h(T_{hi}+120)} 
    \end{displaymath}
 
  \item 
    \begin{displaymath}
      Re_h = \frac{v_h D}{\nu_h}. 
    \end{displaymath}
  \item 
    \begin{displaymath}
      P_{hi}-P_{ho} = \begin{cases}
	\frac{\rho_hLv_h^2}{2D}\times \frac{64}{Re_h}&Re_h < 1189\\
	\frac{\rho_hLv_h^2}{2D}\times \frac{0.316}{Re_h^{0.25}}&Re_h \in( 1189,10^5]
      \end{cases}
    \end{displaymath}
\item
\begin{displaymath}
  w_{min} = \min(w_{hi}, w_{ci}).
\end{displaymath}
\item
\begin{displaymath}
  w_{max} = \max(w_{hi}, w_{ci}).
\end{displaymath}
\item 
\begin{displaymath}
  \NTU = \frac{\UA}{w_{min} c_p}.
\end{displaymath}
\item 
\begin{displaymath}
  C_r = \frac{w_{min}}{w_{max}};
\end{displaymath}
\item
  \begin{displaymath}
  \epsilon = 1-\exp\left\{ \frac{\NTU^{0.22}[\exp(-C_r\times \NTU^{0.78})-1]}{C_r}\right\}.
\end{displaymath}
\item
\begin{displaymath}
  T_{hs} = T_{hi} - \epsilon w_{min}  (T_{hi}-T_{ci})/w_{hi}.
\end{displaymath}
\item \begin{displaymath}
    w_{ci}=w_{co} = w_c. 
  \end{displaymath}
  \item \begin{displaymath}
      \rho_c = \frac{P_{ci}M}{RT_{ci}}, 
    \end{displaymath}
    where $M$ is the molar of air and $R$ is the ideal gas constant.
  \item \begin{displaymath}
      v_c = \frac{4w_{ci}}{\pi D^2\rho_c}. 
    \end{displaymath}
  \item \href{http://en.wikipedia.org/wiki/Viscosity\#Effect_of_temperature_on_the_viscosity_of_a_gas}{Sutherland's Formula}:
    \begin{displaymath}
      \nu_c =1.512\times 10^{-6} \frac{T_{ci}^{1.5}}{\rho_c(T_{ci}+120)} 
    \end{displaymath}
  \item 
    \begin{displaymath}
      Re_c = \frac{v_c D}{\nu_c}. 
    \end{displaymath}
  \item 
    \begin{displaymath}
      P_{ci}-P_{co} = \begin{cases}
	\frac{\rho_cLv_c^2}{2D}\times \frac{64}{Re_c}&Re_c < 1189\\
	\frac{\rho_cLv_c^2}{2D}\times \frac{0.316}{Re_c^{0.25}}&Re_c \in( 1189,10^5]
      \end{cases}
    \end{displaymath}
  \item
\begin{displaymath}
  T_{cs} = T_{ci} - \epsilon w_{min} (T_{ci}-T_{hi})/w_{ci}.
\end{displaymath}
\end{enumerate}
Differential equations:
\begin{enumerate}
\item \begin{displaymath}
  \der T_{ho}= \frac{\UA}{\MM C_{pm}}(T_{hs}-T_{ho});
\end{displaymath}
\item
\begin{displaymath}
  \der T_{co}= \frac{\UA}{\MM C_{pm}}(T_{cs}-T_{co});
\end{displaymath}


\end{enumerate}
\subsection{Mixer}
Input variables:
\begin{enumerate}
\item Inlet pressure from the $k$th source $P_i[k]$, unit Pa.
\item Inlet temperature from the $k$th source $T_i[k]$, unit K.
\item Inlet mass flow rate from the $k$th source $W_i[k]$, unit kg/s.
\end{enumerate}
Output variables:
\begin{enumerate}
\item Outlet pressure $P_o$, unit Pa. 
\item Outlet temperature $T_o$, unit K.
\item Outlet mass flow rate $W_o$, unit kg/s.
\end{enumerate}
Algebraic equations:
\begin{enumerate}
\item 
\begin{displaymath}
P_o =  P_i[k] ,\forall k. 
\end{displaymath}
\item 
\begin{displaymath}
W_o = \sum_{k} W_i[k].
\end{displaymath}
\item
\begin{displaymath}
W_oT_o = \sum_{k} W_i[k] T_i[k].
\end{displaymath}
\end{enumerate}
\subsection{Cabin}
Input variables:
\begin{enumerate}
\item Inlet pressure $P_i$, unit Pa.
\item Inlet temperature $T_i$, unit K.
\item Inlet mass flow rate $W_i$, unit kg/s.
\end{enumerate}
Output variables:
\begin{enumerate}
\item Outlet pressure $P_o$, unit Pa. 
\item Outlet temperature $T_o$, unit K.
\item Outlet mass flow rate $W_o$, unit kg/s.
\end{enumerate}
Parameters:
\begin{enumerate}
\item Volume of the cabin $V$, unit $\text{m}^3$
\item Maintained pressure of the cabin $P$, unit Pa.
\item Number of passenger $pass$.
\item per second heat generated by each passenger $Q_p$, unit J/s.
\item per second heat exchanged with the environment $\Delta Q$, unit J/s.
\end{enumerate}
State Variables:
\begin{enumerate}
\item Temperature of the air inside the cabin $T$, unit K.
\end{enumerate}
Other derived variables:
\begin{enumerate}
\item Thermal energy of the air inside the cabin $Q$, unit J.
\item Mass of the air inside the cabin $m$, unit kg. 
\item Mass flow rate of the air to the environment to maintain constant pressure $W_a$, unit kg/s.
\end{enumerate}
Algebraic equations:
\begin{enumerate}
\item 
\begin{displaymath}
P_i = P_o = P. 
\end{displaymath}
\item
\begin{displaymath}
T_o = T. 
\end{displaymath}
\item 
\begin{displaymath}
P = \frac{mTR}{VM},
  \end{displaymath}
  where $R$ is the ideal gas constant and $M$ is the molar mass of air.
  \item 
  \begin{displaymath}
  Q = C_{v}mT =\frac{C_{v}PVM}{R} = \textrm{constant}.
  \end{displaymath}
  where $C_{v}$ is the heat capacity of air at constant volume, $R$ is the ideal gas constant and $M$ is the molar mass of air.
  \item 
  \begin{displaymath}
  0=\der Q = C_{v} W_i T_i - C_{v}W_oT_o - C_{v}W_a T + Q_p\times pass + \Delta Q .
  \end{displaymath}
  \end{enumerate}
  Differential equations:
  \begin{enumerate}
  \item 
  \begin{displaymath}
  \der m = W_i - W_o - W_{a}. 
  \end{displaymath}
  \end{enumerate}
  \subsection{Fan}
  Input variables:
  \begin{enumerate}
  \item Inlet pressure $P_i$, unit Pa.
  \item Inlet temperature $T_i$, unit K.
  \item Inlet mass flow rate $W_i$, unit kg/s.
  \end{enumerate}
  Output variables:
  \begin{enumerate}
  \item Outlet pressure $P_o$, unit Pa.
  \item Outlet temperature $T_o$, unit K.
  \item Outlet mass flow rate $W_o$, unit kg/s.
  \end{enumerate}
Parameters:
\begin{enumerate}
\item  Fan mass flow rate $W_f$, unit kg/s.
\end{enumerate}
Algebraic equations:
\begin{enumerate}
\item 
\begin{displaymath}
W_i = W_o = W_f. 
\end{displaymath}
\item
\begin{displaymath}
T_o = T_i. 
\end{displaymath}
\end{enumerate}

\section{Simplified AMS Model}

The diagram of the simplified AMS model is shown in Fig~\ref{fig:amssimple}. The pressure, temperature for pipe $i$ are denoted as $P_i,\,T_i$ respectively. The direction of the flow is denoted by the arrow along side the pipe. The mass flow rate for a pipe $i$ without any branches is denoted as $W_i$. Otherwise, the mass flow rate of branch $k$ is denoted as $W_{i,k}$.

\begin{figure}[ht]
\caption{Simplified AMS Model}
\begin{center}
\rotatebox{90}{\inputtikz{flowchart}}
\end{center}
\label{fig:amssimple}
\end{figure}

In Fig~\ref{fig:amsreduced}, we plot the diagram of the simplified AMS model, after removing the variables which are equal. 

Next we describe the parameters and connection equations of the simplified AMS model.

\subsection{Physical Constants}
\begin{itemize}
\item Ideal gas constant $R = 8.314472\,\text{J}/(\text{mol}\cdot\text{K})$. 
\item Molar mass of the air $M = 0.02897\,\text{kg/mol}$.
\item Heat capacity of the air at constant volume $C_{v} = 716.75\,\text{J}/(\text{kg}\cdot\text{K})$.
\item Heat capacity of the air at constant pressure $C_{p} = 1005\,\text{J}/(\text{kg}\cdot\text{K})$.
\end{itemize}
\subsection{Engine and Ambient Air}
\begin{itemize}
\item Engine Temperature $T_1$ ranges from $297.22$K to $505.55$K.
\item Engine Pressure $P_1$:
\begin{table}[h]
\centering
\begin{tabular}{c|ccccc}
Altitude(m)&  0&1524&3048&4572&6096\\
		Pressure(Pa)&	411617.01&	394380.11&	379901.12&	367490.56&	357148.42\\\hline
		Altitude(m)  &7620&9144&10668&12192\\
			Pressure(Pa)&	313711.45&	271653.43&	265448.15&	259932.34
			\end{tabular}
			\caption{Ambient Air Pressure v.s. Altitude}
			\end{table}
			\item Ambient Temperature $T_4$:
			\begin{table}[h]
			\centering
			\begin{tabular}{c|ccccc}
			Altitude(m)&  0&6096&12192\\\hline
			Temperature(K)&	233.33&	216.67& 216.67	
			\end{tabular}
			\caption{Ambient Air Temperature v.s. Altitude on Cold Day}
			\end{table}
			\begin{table}[h]
			\centering
			\begin{tabular}{c|ccccc}
			Altitude(m)&  0&12192\\\hline
			Temperature(K)&	272.22&	230.55
			\end{tabular}
			\caption{Ambient Air Temperature v.s. Altitude on Hot Day}
			\end{table}
			\end{itemize}
			\subsection{Valve 1}
			\begin{itemize}
			\item The maximum valve coefficient of Valve 1 is $C_{max} = 0.3\,\text{m}\cdot \text{s}\cdot \text{K}^{0.5}$.
			\item The following connection equations hold:
			\begin{displaymath}
			T_i = T_1,\,P_i = P_1,\,W_i = W_1,\,T_o = T_2,\,P_o = P_2,\,W_o = W_2,\, C_t = C_1. 
			\end{displaymath}
			\end{itemize}
			\subsection{Container}
			\begin{itemize}
			\item Volume $V = 0.0049\,\text{m}^3$.
			\item The following connection equations hold:
			\begin{displaymath}
			T_i = T_2,\,P_i = P_2,\,W_i = W_2,\,T_o = T_3,\,P_o = P_3,\,W_o = W_{3,1}.
			\end{displaymath}
			\end{itemize}

			\subsection{Valve 2}
			\begin{itemize}
			\item The maximum valve coefficient of 2 is $C_{max} = 0.3\,\text{m}\cdot \text{s}\cdot \text{K}^{0.5}$.
			\item The following connection equations hold:
			\begin{displaymath}
			T_i = T_3,\,P_i = P_3,\,W_i = W_{3,2},\,T_o = T_5,\,P_o = P_5,\,W_o = W_5,\, C_t = C_2. 
			\end{displaymath}
			\end{itemize}
			\subsection{Heat Exchanger(HX)}
			\begin{itemize}
\item Number of tubes per row $N_t$
\item Number of rows $N_r$
\item Total number of tubes $N$
\item Width $W$, height $H$ and length $L$ of the HX, unit m.
\item Diameter of the tube $D$, unit m.
\item Surface area of the tube $A$, unit m$^2$.
\item Heat transfer coefficient for hot air $\hA_h=5274\,\text{J}/\text{K}$.
\item Heat transfer coefficient for hot air $\hA_C=3516\,\text{J}/\text{K}$.
\item Mass of the HX $\MM=22.68\,\text{kg}$.
\item Heat capacity of the material of HX $C_{pm}=837\,\text{J}/(\text{kg}\cdot\text{K})$.
		\end{itemize}
		\subsection{Cabin}
		\begin{itemize}
		\item Volume $V =  141.58\,\text{m}^3$.
		\item Maintained cabin pressure:
		\begin{table}[h]
		\centering
		\begin{tabular}{c|ccccc}
		Altitude(m)&  0&6096&12192\\\hline
		Pressure(Pa)&	101352.93&	75152.85& 75152.85	
		\end{tabular}
		\caption{Cabin Air Pressure v.s. Altitude}
		\end{table}

		\item Number of passenger $pass = 200$.
		\item per second heat generated by each passenger $Q_p = 90$\,J/s.
		\item per second heat exchanged with the environment $\Delta Q$ ranges from $-9000$\,J/s to $9000$\,J/s.
		\item The following connection equations hold:
		\begin{displaymath}
		T_i = T_8,\,P_i = P_8,\,W_i = W_8,\,T_o = T_9,\,P_o = P_9,\,W_o = W_9. 
		\end{displaymath}
		\end{itemize}
		\subsection{Fan}
		\begin{itemize}
		\item  Fan mass flow rate $W_f$ ranges from $0.378$\,kg/s to $1.13$\,kg/s.
		\item The following connection equations hold:
		\begin{displaymath}
		T_i = T_9,\,P_i = P_9,\,W_i = W_9,\,T_o = T_7,\,P_o = P_7,\,W_o = W_7.
		\end{displaymath}
		\end{itemize}
		\subsection{Additional Flow Equation}
		For pipe 3, the following equation on mass flow rate holds
		\begin{displaymath}
		W_{3,1} = W_{3,2}+W_{3,3}.
		\end{displaymath}
		\bibliographystyle{IEEEtran}
		\bibliography{ams}

		\begin{figure}[ht]
		\caption{AMS Model after Removing Equal Variables}
		\begin{center}
		\rotatebox{90}{\inputtikz{flowchart2}}
		\end{center}
		\label{fig:amsreduced}
		\end{figure}

		\end{document}

