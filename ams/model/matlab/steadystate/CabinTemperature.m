function Tca = CabinTemperature( W4,T1,T4,W32,W33,Tref)
%Compute the Cabin temperature - Tref
% used to compute the fixed point of W4

%physical constant
M = 0.02897;
R = 8.3145;
Cv = 716.75;
Cp = 1005;


wmin = min([W33 W4]);
wmax = max([W33 W4]);
UA = (1/5274+1/3516)^-1;
NTU = UA/(wmin*Cp);
Cr = wmin/wmax;
epsilon = 1-exp((NTU^0.22*(exp(-Cr*NTU^0.78)-1))/Cr);
Th = T1 - epsilon*wmin*(T1-T4)/W33;

%Cabin temperature, ref point is Tref
Tca = (W32*T1 + W33*Th)/(W32+W33)-Tref;

end

