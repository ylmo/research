function flow = flowrateValve(C,Pin,T,Pout,Wref)
%Compute the flow rate of the Value - Wref

if Pout >= Pin
    W = 0;
elseif Pout >= 0.5*Pin
    W = 0.000472*C*(Pin+2*Pout)*sqrt((1-Pout/Pin)/T);
else
    W = 0.000472*sqrt(2)*C*Pin*sqrt(1/T);
end

flow = W - Wref;

end

