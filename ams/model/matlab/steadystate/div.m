function dy = div(t,y,T1v,T1t,T4v,T4t,W32t,W32v,W33t,W33v,W4t,W4v)
%DIV Summary of this function goes here
%   Detailed explanation goes here

%physical constant
M = 0.02897;
R = 8.3145;
Cv = 716.75;
Cp = 1005;
V = 141.58;

%Cabin pressure is constant
P5 = 75152;

W4 = interp1(W4t,W4v,t);
T1 = interp1(T1t,T1v,t);
T4 = interp1(T4t,T4v,t);
W32 = interp1(W32t,W32v,t);
W33 = interp1(W33t,W33v,t);

Tho = y(1);
Tco = y(2);
T7 = y(3);

%hx heat transfer
wmin = min([W33 W4]);
wmax = max([W33 W4]);
UA = (1/5274+1/3516)^-1;
NTU = UA/(wmin*Cp);
Cr = wmin/wmax;
epsilon = 1-exp((NTU^0.22*(exp(-Cr*NTU^0.78)-1))/Cr);
Ths = T1 - epsilon*wmin*(T1-T4)/W33;
Tcs = T4 - epsilon*wmin*(T4-T1)/W4;

dy = zeros(3,1);

dy(1) = UA/(22.68*837)*(Ths-y(1));
dy(2) = UA/(22.68*837)*(Tcs-y(2));

mc = P5*V*M/R/y(3);

dy(3) = (y(1)*W33 - y(3)*(W33+W32) + T1*W32)/mc;

end

