function flow = flowrateHX(Pin,T,Pout,Wref)
%compute the flow rate of the heat exchanger - Wref

%physical constant
M = 0.02897;
R = 8.3145;
Cv = 716.75;
Cp = 1005;

D = 0.001;
L = 1;
HW = 0.25;
tubes = HW/D^2;

rho = Pin*M/(R*T);

nu = 1.512*10^-6*T^1.5/(rho*(T+120));

%assume Reh is less than 1189
v = D^2*(Pin-Pout)/(32*nu*rho*L);
Re = v*D/nu;

%if Re is greater than 1189, then use the other formula
if Re > 1189
    v = (D^1.25*1189^0.75*(Pin-Pout)/(32*nu^0.25*rho*L))^(4/7);
end
W = pi*D^2*rho*v/4*tubes;

flow = W - Wref;

end