Attached is the boundary condition and requirements model and parameters.


P1_Vol is the volume of the duct upstream of the hx and valve 2 and downstream of valve 1.

AE_hx is the effective area of the hx.

Cab_Vol is the volume of the cabin.

HA1 & HA2 are the outputs of the heat exchanger performance map

T1, T2, W1, W2 are the inputs of the heat exchanger performance map

W_Fan is the recirc fan flow

Watts_per_human is the assumed watts per human

hx_mass is the mass of the heat exchanger involved in transferring heat

hx_cp is the specific heat of the exchanger metal.

 

Alt is aircraft altitude

DayType is a unitless scalar between cold day and hot day

Wa is the cold side flow of the heat exchanger

Wref is the flow reference for the fresh air cabin inflow

Q is the cabin heat load

Tin is valve 1 inlet temperature

Pass is the number of passengers

Aev1 & Aev2 are the effective areas of valves 1 and 2

Ta is the inlet temperature to the heat exchanger on the cold side and the ambient temperature outside the cabin.

Tcab is the temperature of the cabin

Pduct is the pressure upstream of the heat exchanger and valve 2 (downstream of valve 1)

Thx is the metal temperature of the heat exchanger.

