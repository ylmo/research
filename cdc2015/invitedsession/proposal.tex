\documentclass[11pt]{article}
\usepackage{palatino}
\usepackage{tikz}

\pagestyle{plain}
\parskip=\medskipamount

% PAGE parameters
\usepackage{vmargin}
\setpapersize{USletter}
\setmarginsrb{2.5cm}{2.5cm}{2.5cm}{2.5cm}%
{12pt}{12pt}{12pt}{24pt}

% \renewcommand{\baselinestretch}{.97}
% \setlength{\parskip}{7pt}

%% Enumerate environment
\renewcommand{\theenumi}{(\roman{enumi})}
\renewcommand{\labelenumi}{\theenumi}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\begin{document}

\begin{center}
  {\Large Proposal for Invited Session}\\
  $54^{th}$ IEEE Conference on Decision and Control\\
  Osaka, Japan, December 15-18, 2015\\[2ex]
  {\Large ``Security in Cyber-Physical Systems''}\\
\end{center}

\hrule

\section*{Session Organizers}

\begin{itemize}
  \item Yilin Mo \\
    Control and Dynamical Systems\\
    California Institute of Technology \\
    Pasadena, CA 91125, USA \\
    Tel:  +1 (626) 395-3503, Email: \texttt{yilinmo@caltech.edu}
  \item Bruno Sinopoli\\
    Department of Electrical and Computer Engineering\\
    Carnegie Mellon University\\
    Pittsburgh, PA 15213, USA\\
    Tel: +1 (412) 268-9432, Fax: +1 (412) 268-3890, Email: \texttt{brunos@ece.cmu.edu}

\end{itemize}
\section*{Motivation for the Session}
Cyber-Physical Systems refer to the embedding of widespread sensing, networking, computation, and control into physical spaces with the goal of making them safer, more efficient and reliable. Applications of cyber-physical systems include aerospace, transportation, built environments, energy, health care and manufacturing. A wide variety of motivations exists for launching an attack on cyber-physical systems, ranging from economic reasons such as drawing a financial gain, all the way to terrorism. Due to the crucial role of cyber-physical systems in everyday life, cyber-physical security needs to be promptly addressed.

Designing control systems that can detect and tolerate exogenous inputs are not new, as the numerous manuscripts on fault detection and isolation, robust estimation and control theory testify. Cyber-physical systems, however, suffer from specific vulnerabilities which do not affect classical control systems. Recent studies and real world incidents have demonstrated the inability of the existing detection, estimation and control algorithm to ensure safe and reliable functionality of the cyber-physical infrastructures against various types of attacks. As a result, many existing techniques need to be reexamined in the cyber-physical security context and new methodologies need to be developed. Because of the interdisciplinary nature of the field, open dialogue between researchers is essential for its continued advancement. The motivation of this invited session ``Security in Cyber-Physical Systems'', is to bring together contributions that develop new theoretical tools for designing and analyzing the performance of security algorithm for cyber-physical systems. The contributions of this proposed invited session will touch upon several aspects of aforementioned challenges, ranging from theory to practice.

\section*{Relevance of the Session}

The session consists of six papers presenting the audience with a snapshot of some recent achievements in the area of cyber-physical system security. The presentations will be given by well-known experts from all over the globe who are actively contributing to the rapid developments in the area. The session focuses on analysis and design of efficient and secure algorithms for cyber-physical systems. Specific topics addressed in this session include:
\begin{enumerate}
  \item intrusion detection and isolation;
  \item resilient estimation algorithm design in the presence of compromised sensory data;
  \item secure control algorithm design.
\end{enumerate}

Because of its wide coverage, the session should attract a wide range of CDC attendees. The core material will be of central interest to the growing community of control engineers and theorists dealing with cyber-physical systems and security. As the covered topic is relevant for many industrial applications but also include new theoretical challenges, the session should attract both practitioners as well as academic researchers.

\section*{Proposed Session Contributions}
\begin{enumerate}
  \item \textit{Detecting Integrity Attacks on Control Systems using a Moving Target Approach}
    \begin{description}
      \item[Authors:] Sean Weerakkody and Bruno Sinopoli (Carnegie Mellon University)

      \item[Abstract]: Maintaining the security of control systems in the presence of integrity attacks is a significant challenge. In literature, several possible attacks against control systems have been formulated including replay, false data injection, and zero dynamics attacks. The detection and prevention of these attacks require that the defender have a particular subset of trusted communication channels. For instance, a trivial zero dynamic attack for an adversary who has access to all  communication channels is to subtract one's influence from the sensor outputs. In the case of watermarking detection of replay attacks, an adversary can simulate the outputs of a control system in response to the noisy inputs. However, these attacks require that an adversary have complete knowledge of the  defender's system model. In this paper, we consider an adversary who has the ability to modify and read all sensor and actuator channels. To thwart this adversary, we introduce external states dependent on the state of the control system with linear time varying dynamics unknown to the adversary and sensors to measure these states. The presence of unknown time varying dynamics is leveraged to detect an adversary who simultaneous aims to identify the system and inject stealthy outputs. Detection results and bounds on the attacker's performance are provided.
    \end{description}


  \item \textit{A Divide-and-Conquer Approach to Distributed Attack Identification}
    \begin{description}
      \item[Authors:] Fabio Pasqualetti (University of California, Riverside), Florian D\"{o}rfler (University of California, Los Angeles) and Francesco Bullo (University of California, Santa Barbara)
      \item[Abstract]: Identifying attacks is key to ensure security in cyber-physical systems. In this paper we remark upon the computational complexity of the attack identification problem by showing how conventional approximation techniques may fail to identify attacks. Then, we propose decentralized and distributed monitors for attack identification with performance guarantees and low computational complexity. The proposed monitors rely on a geometric control framework, yet they require only local knowledge of the system dynamics and parameters. We exploit a divide-and-conquer approach, where first the system is partitioned into disjoint regions, then corrupted regions are identified via distributed computation, and finally corrupted components are isolated within regions.
    \end{description}

  \item \textit{Fake-Acknowledgment Attack on ACK-based Sensor Schedule for Remote State Estimation}
    \begin{description}
      \item[Authors:] Yuzhe Li (Hong Kong University of Science and Technology), Daniel E. Quevedo (University of Newcastle), Subhrakanti Dey (The University of Melbourne) and Ling Shi (Hong Kong University of Science and Technology)

      \item[Abstract]: We consider a potential class of malicious attacks against remote state estimation. A sensor with limited resources adopts an ACK-based online power schedule to improve the remote state estimation performance. A malicious attacker can modify the ACKs from the remote estimator and convey fake information to the sensor. When the capability of the attacker is limited, we derive the optimal strategy for the attacker and analyze the corresponding effect on the estimation performance. The possible responses of the sensor are studied and the condition for the sensor to switch online schedule to offline schedule is provided.
    \end{description}

  \item \textit{$\mathcal L_1$-Type of Estimator Design in Adversarial Environment}
    \begin{description}
      \item[Authors:] Yorie Nakahira, Yilin Mo and Richard M. Murray (California Institute of Technology)
      \item[Abstract]: In this article, we consider the state estimation problem of a linear time invariant system in adversarial environment. We assume that the process noise and measurement noise are $\mathcal L_\infty$ functions. The adversary compromises at most $\gamma$ sensors, the set of which is unknown to the estimation algorithm, and can change their measurements arbitrarily. We first prove that if after removing a set of $2\gamma$ sensors, the system is undetectable, then there exists a destabilizing noise process and attacker's input to render the estimation error unbounded. For the case where the system remains detectable after removing an arbitrary set of $2\gamma$ sensors, we construct a resilient estimator and provide an upper bound on the $\mathcal L_\infty$ norm of the estimation error. Finally, a numerical example is provided to illustrate the effectiveness of the proposed estimator design.
    \end{description}



  \item \textit{Attack-resilient State Estimation in the Presence of Noise and Modeling Errors}
    \begin{description}
      \item[Authors:] Miroslav Pajic, Insup Lee and George J. Pappas (University of Pennsylvania)

      \item[Abstract]: We consider the problem of state estimation in the presence of attacks, for dynamical systems with noise and modeling errors. We present an integer programing-based method for attack-resilient state estimation and provide an upper bound on the state-estimation error as a function of the system dynamics and size of the noise; specifically we show that the error is inversely proportional to the minimal eigenvalue of the observability matrix for a subset of sensors that guarantee system observability. Furthermore, we show how the presented attack-resilient state estimator can be used for attack detection and identification, and provide conditions on the size of attack vectors that will ensure correct identification of compromised sensors. 
    \end{description}

  \item \textit{Secure Controller Design against Known-Plaintext Attack}
    \begin{description}
      \item[Authors:] Ye Yuan (University of Cambridge), Yilin Mo and Richard M. Murray (California Institute of Technology) 
      \item[Abstract]: A substantial amount of research on the security of feedback control systems assumes that the adversary knows system models. In this paper, we argue that even if the system model is unknown, the adversary might be able to identify it by observing the control input and sensory data of the system. In such a setup, attack with knowledge of input-output data can be categorised as a Known-Plaintext Attack (KPA) in the information security literature. We first provide a necessary and sufficient condition, under which the adversary can successfully identify the system transfer function, which illustrates that the widely used LQG control scheme are indeed vulnerable to KPA. We then provide a low-rank controller design which renders the system unidentifiable to the adversary, while trading off the LQG performance. Finally, a numerical example is provided to illustrate the effectiveness of the proposed controller design. 
    \end{description}


 \end{enumerate}
\end{document}
