\section{Main results}
\label{sec:main}
In this section, we consider the problem of computing the reachable set $\mathcal R_k$ and $\mathcal R$. In Section~\ref{sec:recursivedef}, we provide a recursive definition of $\mathcal R_k$ based on the concept of controlled invariant set. We further devote Section~\ref{sec:ellipsoidalapprox} to the numerical approximation of $\mathcal R_k$ and $\mathcal R$.
\subsection{Recursive Definition of $R_k$}\label{sec:recursivedef}
 Before continuing on, we need to introduce the concept of reach set and one-step set:
\begin{definition}
  Define the reach set $\Rch(\mathcal S)$ of set $\mathcal S\subseteq \mathbb R^{2n}$ to be
  \begin{align}
    \Rch(\mathcal S)& \triangleq \{\tilde x^+\in\mathbb R^{2n}: \exists\zeta\in \mathbb R^{p+l},\tilde x^c\in \mathcal S,\nonumber\\
   & s.t.,\tilde A\tilde x^c+\tilde B\zeta = \tilde x^+,\,\|\tilde C\tilde x^c+\tilde D\zeta\|\leq 1\} .
  \end{align}
\end{definition}

\begin{definition}
  Define the one-step set $\Pre(\mathcal S)$ of set $\mathcal S\subseteq \mathbb R^{2n}$ to be
  \begin{align}
    \Pre(\mathcal S) &\triangleq \{\tilde x^c\in\mathbb R^{2n}:\exists\zeta\in \mathbb R^{p+l},\nonumber\\
    &s.t.,\tilde A\tilde x^c+\tilde B\zeta \in \mathcal S,\,\|\tilde C\tilde x^c+\tilde D\zeta\|\leq 1\} .
  \end{align}
\end{definition}

\begin{remark}
The reach set of $\mathcal S$ indicates all the states $\tilde x^c(k+1)$ that can be reached with a one-step admissible attacker's input $\zeta(k)$, when the current state $\tilde x^c(k)$ is in $\mathcal S$. On the other hand, the one-step set of $\mathcal S$ indicates all the previous states $\tilde x^c(k-1)$ that can be driven into $\mathcal S$ with a one-step admissible $\zeta(k-1)$. 
\end{remark}
At the first glance, it seems that $\mathcal R_k$ can be recursively defined as $\mathcal R_{k+1} = \Rch(\mathcal R_k)$. However, the reach set only guarantees that $\|\tilde C\tilde x^c(k) + \tilde D\tilde \zeta(k)\|\leq 1$ for the current $k$, not for the future $k$s. %Thus, it is possible that $\Rch(\mathcal R_k)$ is strictly larger than $\mathcal R_{k+1}$.
To define $\mathcal R_{k}$ recursively, we need to introduce the concept of controlled invariant set.

\begin{definition}
  A set $\mathcal C\subseteq \mathbb R^{2n}$ is called controlled invariant if for all $\tilde x^c \in \mathcal C$, there exists a $\zeta$, such that the following inequalities hold:
  \begin{equation}
    \tilde A\tilde x^c + \tilde B\zeta\in \mathcal C,\,\|\tilde C \tilde x^c + \tilde D\zeta\|\leq 1.
    \label{eq:invariantdef}
  \end{equation}
\end{definition}
In other words, if the current state $\tilde x^c(k)$ belongs to $\mathcal C$, then the attacker can always use a admissible $\zeta(k)$ to enforce that the next state $\tilde x^c(k+1)$ and hence all the future states to be in $\mathcal C$. The following proposition characterizes several important properties of the reach set, the one-step set and the controlled invariant set:
\begin{proposition}
  \label{proposition:positiveinvariance}
  The following statements hold for the operator $\Pre,\,\Rch$ and the controlled invariant set:
  \begin{enumerate}
    \item $\Pre$ and $\Rch$ are monotonically nondecreasing, i.e., if $\mathcal S_1\subseteq \mathcal S_2$, then 
      \begin{equation}
      \Pre(\mathcal S_1)\subseteq \Pre(\mathcal S_2),\,\Rch(\mathcal S_1)\subseteq \Rch(\mathcal S_2).
      \end{equation}
    \item Let $\mathcal C$ to be a controlled invariant set, then $\mathcal C \subseteq \Pre(\mathcal C)$.
    \item There exists the maximum controlled invariant set $\mathcal C_{\infty}$, such that $\mathcal C\subseteq \mathcal C_{\infty}$ for all controlled invariant set $\mathcal C$.
    \item Let $\mathcal C_0 = \mathbb R^{2n}$ and $\mathcal C_{k+1} = \Pre(\mathcal C_k)$. Then the following equality holds:
      \begin{equation}
	\mathcal C_\infty = \bigcap_{k=0}^\infty \mathcal C_k.
	\label{eq:maxcontrolledinvariant}
      \end{equation}
  \end{enumerate}
\end{proposition}

\begin{proof}
  The proof of the first three properties can be found in \cite{Kerrigan2000}, while the proof of the last property is quite technical and is reported in the appendix to improve legibility\footnote{It is worth noticing that for general systems and feasibility constraints, \eqref{eq:maxcontrolledinvariant} is not necessarily true\cite{Bertsekas1972}. }.
\end{proof}

We are now ready to provide a recursive definition of $\mathcal R_k$:
\begin{theorem}
  \label{theorem:recursiver}
  $\mathcal R$ is controlled invariant, and hence $\mathcal R \subseteq \mathcal C_{\infty}$. Furthermore $\mathcal R_k$ satisfies the following recursive equation 
  \begin{equation}
    \mathcal R_{k+1} = \Rch(\mathcal R_k)\bigcap \mathcal C_{\infty},\text{ with }\mathcal R_{0} = \{0\}.
    \label{eq:reachablesetrecursive}
  \end{equation}
\end{theorem}

\begin{proof}
  First we need to proof that $\mathcal R$ is controlled invariant. By definition, for any $\tilde x^c\in \mathcal R$, there exists $k$ and a feasible $\zeta^\infty$, such that $ \tilde x^c =  \tilde x^c(k,\zeta^\infty)$. As a result, $\zeta(k)$ is the admissible control input to ensure that \eqref{eq:invariantdef} holds, which implies that $\mathcal R$ is controlled invariant. Thus, $\mathcal R \subseteq \mathcal C_\infty$ due to the maximality of $\mathcal C_\infty$.

  We now prove \eqref{eq:reachablesetrecursive} by induction. Since the attack starts at time $0$, $\mathcal R_{0} = \{0\}$. Now assume that \eqref{eq:reachablesetrecursive} holds for $k$. From the definition of $\mathcal R_{k+1}$, and the fact that $\mathcal R_{k+1}\subseteq \mathcal R\subseteq \mathcal C_\infty$, it is trivial to prove that $\mathcal R_{k+1} \subseteq \Rch(\mathcal R_k)\bigcap \mathcal C_\infty$. Therefore, we only need to prove opposite side of the set inclusion, i.e., for all $\tilde x^c\in \Rch(\mathcal R_k)\bigcap \mathcal C_\infty$, there exists a feasible $\zeta^\infty$, such that $\tilde x^c = \tilde x^c(k+1,\zeta^\infty)$. 
      
      From the induction assumption, we know that there exist $\zeta(0),\ldots,\zeta(k)$ and $\tilde x^c(0),\dots,\tilde x^c(k),\tilde x^c(k+1) = \tilde x^c$, such that
      \begin{displaymath}
	\tilde x^c(i+1) = \tilde A \tilde x^c(i)+\tilde B\zeta(i), \|\tilde C\tilde x^c(i) +\tilde D \zeta(i)\|\leq 1,i = 0,\dots,k.
      \end{displaymath}
      Since $\tilde x^c\in \mathcal C_\infty$, one can find an admissible control $\zeta(k+1)$, such that \eqref{eq:invariantdef} holds. Now since $\tilde x^c(k+2) = \tilde A \tilde x^c + \tilde B \zeta(k+1)$ also belongs to $\mathcal C_{\infty}$, we can repeat the procedure above to find $\zeta(k+2)$, $\zeta(k+3),\dots$, to ensure \eqref{eq:invariantdef} holds for all $k$. Therefore, $\zeta^\infty = (\zeta(0),\dots,\zeta(k),\zeta(k+1),\dots)$ is the required feasible sequence, which concludes the proof.
\end{proof}

Proposition~\ref{proposition:positiveinvariance} and Theorem~\ref{theorem:recursiver} enable the computation of $\mathcal C_k$ and $\mathcal R_k$ by recursively applying the operator $\Pre$ and $\Rch$. However, computing the exact shapes of these sets is numerically intractable as $k$ goes to infinity. One standard technique to attack this problem is to compute the inner and outer approximation of $\mathcal C_k$ and $\mathcal R_k$, using ellipsoids or polytopes. In this paper, we use an ellipsoidal approximation procedure similar to the one proposed in \cite{Angeli20083113}. The detailed approach is presented in the next subsection.

\subsection{Ellipsoidal Approximation of $R_k$}\label{sec:ellipsoidalapprox}
 This section is devoted to constructing an ellipsoidal inner and outer approximation of $\mathcal C_k$ and $\mathcal R_k$. To this end, let us assume that $\mathcal C_k$ and $\mathcal R_k$ are approximated by the following ellipsoids:
\begin{equation}
  \begin{split}
  \mathcal E_{2n}(C^{in}(k))&\subseteq \mathcal C_k \subseteq \mathcal E_{2n}(C^{out}(k)), \\
  \mathcal E_{2n}(R^{in}(k))&\subseteq \mathcal R_k \subseteq \mathcal E_{2n}(R^{out}(k)),
  \end{split}
\end{equation}
where $C^{in}(k),\, C^{out}(k),\, R^{in}(k),\,R^{out}(k) \in \mathbb S_+^{2n}$ , and $\mathcal E_{2n}(S)$ is defined as the following $2n$ dimensional ellipsoid
\begin{equation}
  \mathcal E_{2n}(S) \triangleq\{\tilde x^c\in\mathbb R^{2n}:(\tilde x^c)^TS\tilde x^c\leq 1\}.
\end{equation}

To compute $\mathcal C_k$ and $\mathcal R_k$, we focus on the ellipsoidal inner and outer approximations of set intersection and the operators $\Pre$ and $\Rch$, which are provided by the following proposition and theorem:
\begin{proposition}
  \label{proposition:ellipsoidintersec}
  Let $S_1,S_2\in \mathbb R^{2n}$ be positive semidefinite, then the following set inclusions hold:
  \begin{equation}
    \mathcal E_{2n} (S_1+S_2) \subseteq \mathcal E_{2n}(S_1)\bigcap \mathcal E_{2n}(S_2)\subseteq \mathcal E_{2n}(S_1/2+S_2/2).
  \end{equation}
\end{proposition}

\begin{theorem}
  Let $S\in \mathbb R^{2n\times 2n}$ be a positive semidefinite matrix. Then the following set inclusions hold:
  \begin{align}
    \mathcal E_{2n}(S_p^{in}) &\subseteq \Pre(\mathcal E_{2n}(S))\subseteq  \mathcal E_{2n}(S_p^{out}), \\
    \mathcal E_{2n}(S_r^{in}) &\subseteq \Rch(\mathcal E_{2n}(S))\subseteq  \mathcal E_{2n}(S_r^{out}),
  \end{align}
  where
  \begin{align}
    S_p^{in} &= f(S),\, S_p^{out} = f(S)/2, \label{eq:preapprox}\\
    S_p^{in} &= h(S), \,S_p^{out} = h(S)/2, \label{eq:rchapprox}
  \end{align}
  and $f(S), h(S)$ are defined as the following Riccati equations:
  \begin{displaymath}
    \begin{split}
      f(S) &\triangleq \tilde A^TS \tilde A +  \tilde C^T\tilde C \\
      &- (\tilde A^TS\tilde B +  \tilde C^T\tilde D) (\tilde B^TS\tilde B + \tilde D^T\tilde D)^+ (\tilde B^TS \tilde A + \tilde D^T\tilde C),\\
      h(S) &\triangleq \hat A^TS \hat A +  \hat C^T\hat C \\
      &- (\hat A^TS\hat B +  \hat C^T\hat D) (\hat B^TS\hat B + \hat D^T\hat D)^+ (\hat B^TS \hat A + \hat D^T\hat C).
    \end{split}
  \end{displaymath}

  The matrices $\hat A\in \mathbb R^{2n\times 2n},\,\hat B\in \mathbb R^{2n\times (q+l+2n)},\,\hat C\in \mathbb R^{m\times 2n},\,\hat D\in \mathbb R^{m\times (q+l+2n)}$ are defined as
  \begin{align}
    \hat A &\triangleq \tilde A^+,&
    \hat B  &\triangleq    \left[{\begin{array}{*{20}c}
      -\tilde A^+\tilde B,& I_{2n} - \tilde A^+\tilde A
    \end{array}}\right]\nonumber ,\\
    \hat C & \triangleq \tilde C\tilde A^+,&
    \hat D &\triangleq \left[{\begin{array}{*{20}c}
      \tilde D-\tilde C\tilde A^+\tilde B,& \tilde C -\tilde C \tilde A^+\tilde A
    \end{array}}\right]. 
  \end{align}
  \label{theorem:ellipsoidrch}
  \label{theorem:ellipsoidpre}
\end{theorem}
\begin{proof}[Proof of Theorem~\ref{theorem:ellipsoidpre}]
  We first prove \eqref{eq:preapprox}. Consider the augmented set $\mathcal S_a\subseteq \mathbb R^{2n+q+l}$ of both the state $\tilde x^c$ and attacker's action $\zeta$:
  \begin{displaymath}
    \mathcal S_a = \left\{ \left[{\begin{array}{*{20}c}
      \tilde x^c\\
      \zeta
    \end{array}}\right]:\tilde {A}\tilde x^c+\tilde {B}\zeta\in \mathcal E_{2n}(S),\,\|\tilde{C}\tilde x^c+\tilde {D}\zeta\|\leq 1\right\}.
  \end{displaymath}
  It is easy to see that $\tilde {A}\tilde x^c+\tilde {B}\zeta\in \mathcal E_{2n}(S)$ is equivalent to the following inequality:
  \begin{displaymath}
    \left[{\begin{array}{*{20}c}
      \tilde x^c \\ \zeta
    \end{array}}\right]^T   \left[{\begin{array}{*{20}c}
      \tilde A^TS\tilde A & \tilde A^TS\tilde B\\
      \tilde B^TS\tilde A & \tilde B^TS\tilde B
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde x^c \\ \zeta
    \end{array}}\right]\leq 1. 
  \end{displaymath}
  Moreover, $\|\tilde{C}\tilde x^c+\tilde {D}\zeta\|\leq 1$ is equivalent to
  \begin{displaymath}
    \left[{\begin{array}{*{20}c}
      \tilde x^c \\ \zeta
    \end{array}}\right]^T \left[{\begin{array}{*{20}c}
      \tilde C^T\tilde C & \tilde C^T\tilde D\\
      \tilde D^T\tilde C & \tilde D^T\tilde D
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde x^c \\ \zeta
    \end{array}}\right]\leq 1. 
  \end{displaymath}
  Therefore, the augmented set $\mathcal S_a$ is the intersection of the following two $2n+q+l$ dimension ellipsoids:
  \begin{equation}
    \begin{split}
      \label{eq:ellipsoidintersection}
      \mathcal S_a &=  \mathcal E_{2n+q+l}\left(\left[{\begin{array}{*{20}c}
	\tilde A^TS\tilde A & \tilde A^TS\tilde B\\
	\tilde B^TS\tilde A & \tilde B^TS\tilde B
      \end{array}}\right] \right)\\
      &\bigcap \mathcal E_{2n+q+l}\left( \left[{\begin{array}{*{20}c}
	\tilde C^T\tilde C & \tilde C^T\tilde D\\
	\tilde D^T\tilde C & \tilde D^T\tilde D
      \end{array}}\right]\right).
    \end{split}
  \end{equation}
  Thus, by Proposition~\ref{proposition:ellipsoidintersec},
  \begin{displaymath}
    \mathcal E_{2n+q+l}( S_a^{in})\subseteq \mathcal S_a \subseteq\mathcal E_{2n+q+l} ( S_a^{out}),
  \end{displaymath}
  where
  \begin{displaymath}
    \begin{split}
      S^{in}_a &=\left[{\begin{array}{*{20}c}
	\tilde A^TS\tilde A & \tilde A^TS\tilde B\\
	\tilde B^TS\tilde A & \tilde B^TS\tilde B
      \end{array}}\right] + \left[{\begin{array}{*{20}c}
	\tilde C^T\tilde C & \tilde C^T\tilde D\\
	\tilde D^T\tilde C & \tilde D^T\tilde D
      \end{array}}\right],\\
      S^{out}_a&=S^{in}_a/2.
    \end{split}
  \end{displaymath}

  Using the Schur complement, we can project the two high dimensional ellipsoids from $\mathbb R^{2n+q+l}$ back to $\mathbb R^{2n}$ to obtain \eqref{eq:preapprox}.

  We now prove \eqref{eq:rchapprox}. From the definition of $\Rch$, for any $\tilde x^c\in \Rch(\mathcal E_{2n}(S))$, there exist $\tilde x^-\in \mathcal E_{2n}(S)$ and $\zeta$, such that
  \begin{align}
    \label{eq:rcheq}
    &\tilde x^c = \tilde A \tilde x^- + \tilde B \zeta,\\
    \label{eq:rchineq}
    &\|\tilde C\tilde x^- + \tilde D \zeta\| \leq 1.
  \end{align}
  By the properties of the pseudoinverse, we know that $I_{2n} - \tilde A^+\tilde A$ is a projection from $\mathbb R^{2n}$ onto the kernel of $A$. Thus, \eqref{eq:rcheq} can be written as
  \begin{displaymath}
    \tilde x^- = \tilde A^+ \tilde x^c - \tilde A^+\tilde B \zeta + (I_{2n} - \tilde A^+\tilde A)\tilde x_0,\\
  \end{displaymath}
  where $\tilde x_0\in\mathbb R^{2n}$ is an arbitrary vector. Since $\tilde x^- \in \mathcal E_{2n}(S)$, we know that
  \begin{equation}
    \tilde A^+ \tilde x^c - \tilde A^+\tilde B \zeta +(I_{2n} - \tilde A^+\tilde A)\tilde x_0\in \mathcal E_{2n}(S). 
    \label{eq:rkellipsod1}
  \end{equation}
  Furthermore, \eqref{eq:rchineq} can be written as
  \begin{equation}
    \left\|\tilde C\tilde A^+ \tilde x^c + ( \tilde D- \tilde C\tilde A^+\tilde B) \zeta + (\tilde C  -\tilde C \tilde A^+\tilde A)\tilde x_0\right\|\leq 1 .
    \label{eq:rkellipsod2}
  \end{equation}
  By the same argument as the proof of Theorem~\ref{theorem:ellipsoidrch}, we can obtain \eqref{eq:rchapprox}.
\end{proof}
The monotonicity of the $f$ and $h$ function is proved in the following theorem:
\begin{theorem}
  \label{theorem:fmonotone}
  For any $X \geq Y \geq 0$, $f(X)\geq f(Y),\,h(X)\geq h(Y)$.
\end{theorem}
\begin{proof}
  Let
  \begin{displaymath}
    \begin{split}
      X_a &=\left[{\begin{array}{*{20}c}
	\tilde A^TX\tilde A & \tilde A^TX\tilde B\\
	\tilde B^TX\tilde A & \tilde B^TX\tilde B
      \end{array}}\right] + \left[{\begin{array}{*{20}c}
	\tilde C^T\tilde C & \tilde C^T\tilde D\\
	\tilde D^T\tilde C & \tilde D^T\tilde D
      \end{array}}\right],\\
      Y_a &=\left[{\begin{array}{*{20}c}
	\tilde A^TY\tilde A & \tilde A^TY\tilde B\\
	\tilde B^TY\tilde A & \tilde B^TY\tilde B
      \end{array}}\right] + \left[{\begin{array}{*{20}c}
	\tilde C^T\tilde C & \tilde C^T\tilde D\\
	\tilde D^T\tilde C & \tilde D^T\tilde D
      \end{array}}\right].
    \end{split}
  \end{displaymath}
  Clearly $X_a \geq Y_a$, which implies that $\mathcal E_{2n+q+l}(X_a)\subseteq \mathcal E_{2n+q+l}(Y_a)$. Define a projection matrix $ M$ as
      \begin{displaymath}
      M \triangleq  \left[{\begin{array}{*{20}c}
	  I_{2n}&0_{2n\times (q+l)}
	\end{array}}\right]\in \mathbb R^{2n\times (2n+q+l)}.
      \end{displaymath}
 	which implies that $f(X)\geq f(Y)$. Similarly, one can prove that $h(X)\geq h(Y)$.
\end{proof}

We are now ready to describe a recursive algorithm to compute the ellipsoidal approximations $C^{in}(k)$, $C^{out}(k)$, $R^{in}(k)$, $R^{out}(k)$. By Theorem~\ref{theorem:ellipsoidpre}, we know that $C^{in}(k)$, $C^{out}(k)$ can be evaluated recursively as
\begin{equation}
  C^{in}(k+1) = f(C^{in}(k)), C^{out}(k+1) = f(C^{out}(k))/2.
\end{equation}
Since $C^{in}(1) \geq C^{out}(1) \geq C^{in}(0) = C^{out}(0)=0$, it is easy to prove by induction that $\{C^{in}(k)\}$ and $\{C^{out}(k)\}$ are monotonically increasing and hence the limits for both sequences exists. Let us denote the limits as $C^{in}_\infty$ and $C^{out}_\infty$ respectively. Hence, $R^{in}(k)$ and $R^{out}(k)$ can be computed recursively as
\begin{equation}
  \begin{split}
    R^{in}(k+1) &= h(R^{in}(k)) + C^{in}_\infty,\\
    R^{out}(k+1) &= \left[h(R^{out}(k))/2 + C^{out}_\infty\right]/2.\\
  \end{split}
\end{equation}
\begin{remark}
  It is worth noticing that other than the ellipsoidal approximation techniques~\cite{Angeli20083113}, algorithms such as polyhedral approximation~\cite{Asarin2000} can also be adopted to compute $\mathcal R$.
\end{remark}

