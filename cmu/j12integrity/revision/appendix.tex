\section{Proof of Proposition~\ref{proposition:positiveinvariance}}
This section is devoted to the proof of the last statement in Proposition~\ref{proposition:positiveinvariance}, which requires several intermediate results:
  \begin{proposition}
  \label{proposition:subspacedecompose}
  Let $\mathcal S\subseteq\mathbb R^{n}$ be a closed, convex and symmetric set. Then $\mathcal S$ can be decomposed as
\begin{displaymath}
 \mathcal S = \mathcal K + \mathcal V. 
\end{displaymath}
where $\mathcal V$ is a subspace of $\mathbb R^m$ and $\mathcal K$ is a compact, convex and symmetric set, which is orthogonal to $\mathcal V$.
\end{proposition}
\begin{proof}
  The proposition can be proved using Corollary 2.1 in \cite{Goberna2002}.
\end{proof}
\begin{proposition}
  \label{proposition:compactset}
  The following statements are true:
  \begin{enumerate}
    \item Let $\mathcal K\subset \mathbb R^{n}$ be a compact (closed and bounded) set and $\mathcal S_0\subseteq \mathbb R^{n}$ be a closed set. Then $\mathcal S = \mathcal K+\mathcal S_0$ is a closed set.
    \item Let $\mathcal K\subset\mathbb R^{n}$ be a compact set and $f$ be a continuous function, then $f(\mathcal K)$ is also compact.
  \end{enumerate}
\end{proposition}
\begin{proof}
  The proof can be found in \cite{Schaefer1972}.
\end{proof}

\begin{lemma}
  \label{lemma:backwardoperator}
     Let $\mathcal S\in \mathbb R^{n}$ be a closed, convex and symmetric set, then 
      \begin{displaymath}
	\Pre(\mathcal S)  = \{\tilde x^c \in \mathbb R^{2n}:\exists \zeta,\,s.t.\, \tilde {A}\tilde x^c+\tilde {B}\zeta\in \mathcal S,\|\tilde{C}\tilde x^c+\tilde {D}\zeta\|\leq 1\}.
      \end{displaymath}
      is also closed, convex and symmetric. 
\end{lemma}
\begin{proof}
     One can verify that $\Pre(\mathcal S)$ is convex and symmetric. Hence, we only need to prove that $\Pre(\mathcal S)$ is closed. To this end, define: 
      \begin{displaymath}
	\mathcal S_a \triangleq \left\{ \left[{\begin{array}{*{20}c}
	  \tilde x^c\\
	  \zeta
	\end{array}}\right]\in \mathbb R^{2n+q+l}:\tilde {A}\tilde x^c+\tilde {B}\zeta\in \mathcal S,\|\tilde{C}\tilde x^c+\tilde {D}\zeta\|\leq 1\right\}.
      \end{displaymath}
      Since $\tilde {A}\tilde x^c+\tilde {B}\zeta$ and $\tilde{C}\tilde x^c+\tilde {D}\zeta$ are continuous with respect to $\tilde x^c$ and $\zeta$, $\mathcal S_a$ is also closed, convex and symmetric. By Proposition~\ref{proposition:subspacedecompose}, we know that $\mathcal S_a = \mathcal K_a + \mathcal V_a$, where $\mathcal K_a$ is compact and $\mathcal V_a$ is a subspace. Now define a projection matrix $M$:
      \begin{displaymath}
      M \triangleq  \left[{\begin{array}{*{20}c}
	  I_{2n}&0_{2n\times (q+l)}
	\end{array}}\right]\in \mathbb R^{2n\times (2n+q+l)}.
      \end{displaymath}
	Thus, $\Pre(\mathcal S) = M\mathcal S_a = M\mathcal K_a + M\mathcal V_a$. By Proposition~\ref{proposition:compactset}, $M\mathcal K_a$ is compact. Furthermore, $M\mathcal V_a$ is a subspace of $\mathbb R^{n}$ and thus closed. Hence, by Proposition~\ref{proposition:compactset}, $\Pre(\mathcal S) = M\mathcal K_a+ M\mathcal V_a $ is closed.
\end{proof}
We are now ready to prove Proposition~\ref{proposition:positiveinvariance}:
\begin{proof}
  Since $\mathcal C_{\infty}$ is controlled invariant, $ \mathcal C_{\infty}\subseteq \Pre(\mathcal C_{\infty})$. On the other hand, one can verify that $\mathcal C_1 = \Pre(\mathbb R^{2n})\subseteq \mathcal C_0$.

  Thus, by the monotonicity of $\Pre$, we know that
  \begin{equation}
    \mathcal C_{\infty} \subseteq \dots\subseteq \mathcal C_1\subseteq \mathcal C_0.
    \label{eq:recursivet}
  \end{equation}
  Hence, we only need to prove that $\bigcap_{i=0}^\infty \mathcal C_i\subseteq \mathcal C_{\infty}$.  Let $\tilde x^c \in \bigcap_{i=0}^\infty \mathcal C_i$. From definition, there exist $\zeta_i,\,i\in\mathbb N$, such that
  \begin{displaymath}
    \tilde A \tilde x^c + \tilde B \zeta_i \in \mathcal C_{i-1},\,\|\tilde C\tilde x^c+\tilde D\zeta_i\| \leq 1.
  \end{displaymath}
  Such $\zeta_i$s may not be unique. As a result, we will choose those $\zeta_i$s with minimum norm. By Lemma~\ref{lemma:backwardoperator} and \ref{proposition:subspacedecompose}, we know that $\mathcal C_i$ can be written as $  \mathcal C_i = \mathcal K_i + \mathcal V_i$, where $\mathcal K_i$ is compact and $\mathcal V_i$ is a subspace. Now by \eqref{eq:recursivet}, 
  \begin{displaymath}
   \mathcal V_0\supseteq \mathcal V_1\supseteq \mathcal V_2\supseteq \dots
  \end{displaymath}
  Let us define subspace 
  \begin{displaymath}
   \mathcal V \triangleq \bigcap_{i=0}^\infty \mathcal V_i. 
  \end{displaymath}
   Since $\mathcal V_i$ is of finite dimension, there must exists an $N$, such that $\mathcal V_i = \mathcal V$ for all $i \geq N$, which further implies that $ \mathcal K_{i}\supseteq \mathcal K_{i+1},\,i\geq N $. Hence, $\mathcal K_i$ is uniformly bounded.

  Now we want to prove that $\|\zeta_i\|$ is bounded. Consider the opposite. By Bolzano-Weierstrass Theorem, there exists a subsequence $\{\zeta_{i_j}\}$, such that
  \begin{displaymath}
    \lim_{j\rightarrow\infty} \|\zeta_{i_j}\| = \infty ,\,\lim_{j\rightarrow\infty}\zeta_{i_j}/ \|\zeta_{i_j}\| = v. 
  \end{displaymath}
Hence
\begin{displaymath}
  \tilde B\frac{\zeta_{i_j}}{\|\zeta_{i_j}\|} \in \mathcal V_i + \frac{1}{\|\zeta_{i_j}\|}(\mathcal K_i - \tilde A\tilde x^c),\, \left\|\tilde D  \frac{\zeta_{i_j}}{\|\zeta_{i_j}\|}\right\|\leq\frac{1}{\|\zeta_{i_j}\|}(1+\|\tilde C\tilde x^c\|),
\end{displaymath}
which implies that $ \tilde Bv \in \mathcal V, \tilde Dv = 0$. Therefore, for any $\alpha\in \mathbb R$,
\begin{displaymath}
    \tilde A \tilde x^c + \tilde B (\zeta_i + \alpha v)\in \mathcal C_{i-1},\,\|\tilde C\tilde x^c+\tilde D(\zeta_i+\alpha v)\| \leq 1.
\end{displaymath}
As a result, the fact that $\|\zeta_i\|$ is unbounded contradicts with the minimality of $\|\zeta_i\|$. Thus, $\|\zeta_i\|$ must be bounded. Now by Bolzano-Weierstrass Theorem, there exists a subsequence $\{\zeta_{i_j}\}$, such that
\begin{displaymath}
  \lim_{j\rightarrow\infty} \zeta_{i_j}  = \zeta.
\end{displaymath}
It is easy to see that $\|\tilde C\tilde x^c + \tilde D \zeta\| \leq 1$. On the other hand, for any $j > i$, $\tilde A\tilde x^c + \tilde B \zeta_j \in \mathcal C_{j-1}\subseteq \mathcal C_i$. Since $\mathcal C_i$ is closed, we know that
\begin{displaymath}
  \tilde A\tilde x^c + \tilde B\zeta \in \bigcap_{i=0}^\infty \mathcal C_i .
\end{displaymath}
Thus, $\bigcap_{i=0}^\infty \mathcal C_i$ is controlled invariant. Since $\mathcal C_\infty$ is the largest controlled invariant set, $\bigcap_{i=0}^\infty \mathcal C_i\subseteq \mathcal C_\infty$, which concludes the proof.
\end{proof}
