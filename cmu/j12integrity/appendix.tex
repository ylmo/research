\section{Proof of Proposition~\ref{proposition:positiveinvariance}}
This section is devoted to the proof of the last statement in Proposition~\ref{proposition:positiveinvariance}, which requires several intermediate results:
\begin{proposition}
  \label{proposition:compactset}
  The following statements are true:
  \begin{enumerate}
    \item Let $\mathcal K\subset \mathbb R^{n}$ be a compact (closed and bounded) set and $\mathcal S_0\subseteq \mathbb R^{n}$ be a closed set. Then $\mathcal S = \mathcal K+\mathcal S_0$ is a closed set.
    \item Let $\mathcal K\subset\mathbb R^{n}$ be a compact set and $f$ be a continuous function, then $f(\mathcal K)$ is also compact.
  \end{enumerate}
\end{proposition}
\begin{proof}
  The proof can be found in \cite{Schaefer1972}.
\end{proof}
\begin{lemma}
  \label{lemma:subspace}
  Let $\mathcal S\subseteq \mathbb R^{n}$ be a closed, convex and symmetric set. If $\mathcal S$ is unbounded, then $\mathcal S$ contains a subspace $\lspan(v)$, for some $v\in \mathbb R^n$.
\end{lemma}
\begin{proof}
  Since $S$ is unbounded, there exists a sequence $\{x_i\},\,i\in \mathbb N$, such that
  \begin{displaymath}
    \lim_{i\rightarrow\infty} \|x_i\| = \infty.
  \end{displaymath}
  By Bolzano-Weierstrass Theorem, without loss of generality we could assume that 
  \begin{displaymath}
    \lim_{i\rightarrow\infty} x_i/\|x_i\|  = v.
  \end{displaymath}
  Now we want to prove that $\alpha v \in S$, for all $\alpha \in \mathbb R$. Due to symmetry, we only need to consider $\alpha \geq 0$. It is trivial to see that $0\in S$ as a result of symmetry and convexity. For any $\alpha > 0$, since $\|x_i\|\rightarrow\infty$, we could find an $N$, such that $\|x_i\| \geq \alpha$ if $i \geq N$. Now by convexity, we know that
  \begin{displaymath}
   \alpha \frac{x_i}{\|x_i\|} =  \frac{\alpha}{\|x_i\|}\times x_i + \left(1-\frac{\alpha}{\|x_i\|}\right) \times 0 \in \mathcal S.
  \end{displaymath}
  Take the limit on the left hand side and by the fact that $\mathcal S$ is closed, we know that $\alpha v \in \mathcal S$, which finises the proof. 
\end{proof}

\begin{lemma}
  \label{lemma:subspaceplus}
  Let $\mathcal S\subseteq \mathbb R^{n}$ be a closed and convex set. Suppose that $\lspan(v)\subseteq \mathcal S$. Then for any point $x\in \mathcal S$, $x + \lspan(v) \subseteq \mathcal S$.
\end{lemma}
\begin{proof}
  Without loss of generality, we only need to prove that $x + v \in \mathcal S$, as $\lspan(v) = \lspan(\alpha v)$ for any $\alpha \neq 0$. By convexity and the fact that $kv\in \mathcal S$, we know that, 
  \begin{displaymath}
    \frac{k}{k+1} x +\frac{1}{k+1} kv = \frac{k}{k+1}(x+v)\in \mathcal S. 
  \end{displaymath}
  Now let $k\rightarrow\infty$ and by the fact that $\mathcal S$ is closed, $x+v \in \mathcal S$, which finishes the proof.
\end{proof}

\begin{lemma}
  \label{lemma:subspacedecompose}
  Let $\mathcal S\subseteq\mathbb R^{n}$ be a closed, convex and symmetric set. Then $\mathcal S$ can be decomposed as
\begin{displaymath}
 \mathcal S = \mathcal K + \mathcal V. 
\end{displaymath}
where $\mathcal V$ is a subspace of $\mathbb R^m$ and $\mathcal K$ is a compact, convex and symmetric set, which is orthogonal to $\mathcal V$.
\end{lemma}
\begin{proof}
  Let $\mathcal V$ be the maximal subspace\footnote{$\mathcal V = \{0\}$ if $\mathcal S$ does not contain any subspace and the proof is trivial.} contained in $\mathcal S$ and denote $\mathcal K$ as
  \begin{displaymath}
    \mathcal K = \{x \in \mathcal S: x\text{ is orthogonal to }\mathcal V\} = \mathcal S\bigcap \mathcal V^\perp.
  \end{displaymath}
  By Lemma~\ref{lemma:subspaceplus}, we know that $\mathcal S = \mathcal K + \mathcal V$. Furthermore, it is easy to prove that $\mathcal K$ must be closed, convex and symmetric. Therefore, we only need to prove that $\mathcal K$ is bounded. Suppose the opposite, then by Lemma~\ref{lemma:subspace}, $\mathcal K$ must contain a subspace $\mathcal V'$. Thus, $\mathcal K$ can be decomposed as 
  \begin{displaymath}
    \mathcal K = \mathcal K' + \mathcal V', 
  \end{displaymath}
  which further implies that
  \begin{displaymath}
   \mathcal S = (\mathcal K'+\mathcal V')+\mathcal V = \mathcal K' + (\mathcal V'+\mathcal V).
  \end{displaymath}
  Since $\mathcal K$ is orthogonal to $\mathcal V$, $\mathcal V'$ is also orthogonal to $\mathcal V$. Therefore, $\mathcal V$ is a proper subspace of $\mathcal V'+\mathcal V$, which contradicts with the fact that $\mathcal V$ is the maximal subspace contained in $\mathcal S$. Hence, $\mathcal K$ must be bounded, which finishes the proof.
\end{proof}

\begin{lemma}
  \label{lemma:backwardoperator}
     Let $\mathcal S\in \mathbb R^{n}$ be a closed, convex and symmetric set, then 
      \begin{displaymath}
	\Pre(\mathcal S)  = \{\tilde x \in \mathbb R^{2n}:\exists \zeta,\,s.t.\, \tilde {A}\tilde x+\tilde {B}\zeta\in \mathcal S,\|\tilde{C}\tilde x+\tilde {D}\zeta\|\leq 1\}.
      \end{displaymath}
      is also closed, convex and symmetric. 
\end{lemma}
\begin{proof}
     It is trivial to verify that $\Pre(\mathcal S)$ is convex and symmetric. As a result, we only need to prove that $\Pre(\mathcal S)$ is closed. To this end, let us define the augmented set: 
      \begin{displaymath}
	\mathcal S_a = \left\{ \left[{\begin{array}{*{20}c}
	  \tilde x\\
	  \zeta
	\end{array}}\right]\in \mathbb R^{2n+q+l}:\tilde {A}\tilde x+\tilde {B}\zeta\in \mathcal S,\|\tilde{C}\tilde x+\tilde {D}\zeta\|\leq 1\right\}.
      \end{displaymath}
      Since $\tilde {A}\tilde x+\tilde {B}\zeta$ and $\tilde{C}\tilde x+\tilde {D}\zeta$ are continuous with respect to $\tilde x$ and $\zeta$, we know that $\mathcal S_a$ is also closed, convex and symmetric. By Lemma~\ref{lemma:subspacedecompose}, we know that
      \begin{displaymath}
	\mathcal S_a = \mathcal K_a + \mathcal V_a,
      \end{displaymath}
      where $\mathcal K_a$ is compact and $\mathcal V_a$ is a subspace. Now define a projection matrix $M \in \mathbb R^{2n\times(2n+q+l)}$, such that
      \begin{displaymath}
	M \triangleq  \left[{\begin{array}{*{20}c}
	  I_{2n}&0_{2n\times (q+l)}
	\end{array}}\right]
      \end{displaymath}
      Thus, 
      \begin{displaymath}
	\Pre(\mathcal S) = M\mathcal S_a = M(\mathcal K_a + \mathcal V_a) = M\mathcal K_a + M\mathcal V_a.
      \end{displaymath}
      By Proposition~\ref{proposition:compactset}, $M\mathcal K_a$ is compact. Furthermore, $M\mathcal V_a$ is a subspace of $\mathbb R^{n}$ and thus closed. Hence, by Proposition~\ref{proposition:compactset}, $\Pre(\mathcal S) = M\mathcal K_a+ M\mathcal V_a $ is closed.
\end{proof}
We are now ready to prove Proposition~\ref{proposition:positiveinvariance}:
\begin{proof}
  Since $\mathcal C_{\infty}$ is controlled invariant, 
  \begin{displaymath}
    \mathcal C_{\infty}\subseteq \Pre(\mathcal C_{\infty}). 
  \end{displaymath}
  On the other hand, it is trivial to see that
  \begin{displaymath}
   \mathcal C_1 = \Pre(\mathbb R^{2n})\subseteq \mathbb R^{2n} = \mathcal C_0
  \end{displaymath}
  Thus, by the monotonicity of $\Pre$, we know that
  \begin{equation}
    \mathcal C_{\infty} \subseteq \dots\subseteq \mathcal C_1\subseteq \mathcal C_0.
    \label{eq:recursivet}
  \end{equation}
  Hence, we only need to prove that
  \begin{displaymath}
    \bigcap_{i=0}^\infty \mathcal C_i\subseteq \mathcal C_{\infty}.
  \end{displaymath}
  Let $\tilde x \in \bigcap_{i=0}^\infty \mathcal C_i$. From definition, there exist $\zeta_i,\,i\in\mathbb N$, such that
  \begin{displaymath}
    \tilde A \tilde x + \tilde B \zeta_i \in \mathcal C_{i-1},\,\|\tilde C\tilde x+\tilde D\zeta_i\| \leq 1.
  \end{displaymath}
  Such $\zeta_i$s may not be unique. As a result, we will choose those $\zeta_i$s with minimum norm. By Lemma~\ref{lemma:backwardoperator} and \ref{lemma:subspacedecompose}, we know that $\mathcal C_i$ can be written as
  \begin{displaymath}
    \mathcal C_i = \mathcal K_i + \mathcal V_i,
  \end{displaymath}
  where $\mathcal K_i$ is compact and $\mathcal V_i$ is a subspace. Now by \eqref{eq:recursivet}, 
  \begin{displaymath}
   \mathcal V_0\supseteq \mathcal V_1\supseteq \mathcal V_2\supseteq \dots
  \end{displaymath}
  Let us define subspace $\mathcal V$ to be
  \begin{displaymath}
    \mathcal V = \bigcap_{i=0}^\infty \mathcal V_i. 
  \end{displaymath}
  Since $\mathcal V_i$ is of finite dimension, there must exists an $N$, such that $\mathcal V_i = \mathcal V$ for all $i \geq N$, which further implies that
  \begin{displaymath}
    \mathcal K_{i}\supseteq \mathcal K_{i+1},\,i\geq N. 
  \end{displaymath}
  Hence, $\mathcal K_i$ is uniformly bounded.

  Now we want to prove that $\|\zeta_i\|$ is bounded. Consider the opposite. By Bolzano-Weierstrass Theorem, there exists a subsequence $\{\zeta_{i_j}\}$, such that
  \begin{displaymath}
    \lim_{j\rightarrow\infty} \|\zeta_{i_j}\| = \infty ,\,\lim_{j\rightarrow\infty}\zeta_{i_j}/ \|\zeta_{i_j}\| = v. 
  \end{displaymath}
Hence
\begin{displaymath}
  \tilde B\frac{\zeta_{i_j}}{\|\zeta_{i_j}\|} \in \mathcal V_i + \frac{1}{\|\zeta_{i_j}\|}(\mathcal K_i - \tilde A\tilde x),\, \left\|\tilde D  \frac{\zeta_{i_j}}{\|\zeta_{i_j}\|}\right\|\leq\frac{1}{\|\zeta_{i_j}\|}(1+\|\tilde C\tilde x\|),
\end{displaymath}
which implies that
\begin{displaymath}
 \tilde Bv \in \mathcal V, \tilde Dv = 0. 
\end{displaymath}
Therefore, for any $\alpha\in \mathbb R$,
\begin{displaymath}
    \tilde A \tilde x + \tilde B (\zeta_i + \alpha v)\in \mathcal C_{i-1},\,\|\tilde C\tilde x+\tilde D(\zeta_i+\alpha v)\| \leq 1.
\end{displaymath}
As a result, the fact that $\|\zeta_i\|$ is unbounded contradicts with the minimality of $\|\zeta_i\|$. Thus, $\|\zeta_i\|$ must be bounded. Now by Bolzano-Weierstrass Theorem, there exists a subsequence $\{\zeta_{i_j}\}$, such that
\begin{displaymath}
  \lim_{j\rightarrow\infty} \zeta_{i_j}  = \zeta.
\end{displaymath}
It is easy to see that
\begin{displaymath}
 \|\tilde C\tilde x + \tilde D \zeta\| \leq 1. 
\end{displaymath}
On the other hand, for any $j > i$,
\begin{displaymath}
  \tilde A\tilde x + \tilde B \zeta_j \in \mathcal C_{j-1}\subseteq \mathcal C_i.
\end{displaymath}
Since $\mathcal C_i$ is closed, we know that
\begin{displaymath}
  \tilde A\tilde x + \tilde B\zeta \in \bigcap_{i=0}^\infty \mathcal C_i 
\end{displaymath}
Thus, $\bigcap_{i=0}^\infty \mathcal C_i$ is controlled invariant, which implies that 
\begin{displaymath}
  \bigcap_{i=0}^\infty \mathcal C_i\subseteq \mathcal C_{\infty},
\end{displaymath}
which concludes the proof.
\end{proof}
