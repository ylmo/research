\section{System Description}
\label{sec:dynamicsysdescription}

We model the system as a linear control system, which is equipped with a linear filter, a linear feedback controller and a $\chi^2$ failure detector. We assume that the physical system follows:
\begin{equation}
  x(k+1) = Ax(k) + Bu(k) +  w(k), 
  \label{eq:systemdescription}
\end{equation}
where $x(k) \in \mathbb R^n$ is the vector of state variables at time $k$, $u(k) \in \mathbb R^p$ is the control input, $w(k)\in \mathbb R^n$ is the process noise at time $k$ and $x(0)$ is the initial state. $w(k),\,x(0)$ are independent Gaussian random variables, and $x(0) \sim \mathcal N(0,\;\Sigma)$, $w(k) \sim \mathcal N(0,\;Q)$. 

A sensor network is deployed to monitor the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are collected and sent to a centralized estimator. The observation equation can be written as  
\begin{equation}
  y(k) = C x(k) + v(k),
  \label{eq:sensordescription}
\end{equation}
where $y(k)  = [y_{1}(k),\ldots,y_{m}(k)]^T \in \mathbb R^m$ is a vector of sensor measurements, and $y_{i}(k)$ is the measurement made by sensor $i$ at time $k$. $v(k) \sim \mathcal N(0,\;R)$ is i.i.d. measurement noise independent of $x(0)$ and $w(k)$. 

A linear filter is used to compute state estimation $\hat x(k)$ from observations $y(k)$:
\begin{equation}
  \begin{split}
  \hat x(k+1) &= A \hat x(k) + B u(k) + \\
  &K \left\{y(k+1) - C\left[A\hat x(k) + Bu(k)\right]\right\}.
  \label{eq:kalmanestimator}
  \end{split}
\end{equation}
Define the residue $z(k)$ and the estimation error $e(k)$ at time $k$ as
\begin{equation}
  z(k) \triangleq y(k) - C(A\hat x(k)+Bu(k)),\,e(k) \triangleq x(k)-\hat x(k).
  \label{eq:residue}
\end{equation}
We assume that an LTI feedback controller is used to stabilize the system, which takes the following form:
\begin{equation}
  u(k) = L\hat x(k)
  \label{eq:feedbackcontrol}
\end{equation}

It is well known that the closed-loop system is stable if and only if both $A-KCA$ and $A+BL$ are stable~\cite{Hespanha2009}. For the rest of the discussion we only focus on systems that are closed-loop stable and in steady state.

Consider the CPS consisting of the physical system, the linear filter and controller. We can immediately identify $x(k)$ as the ``physical'' state and $\hat x(k)$ as the ``cyber'' state. Thus, we define the state of the system $\tilde x(k)$ as:
\begin{equation}
  \tilde x(k) \triangleq \left[{\begin{array}{*{20}c}
    x(k)\\
    e(k)
  \end{array}}\right]  = \left[{\begin{array}{*{20}c}
    I_n & 0 \\
    I_n & - I_n
  \end{array}}\right] \left[{\begin{array}{*{20}c}
    x(k)\\
    \hat x(k)
  \end{array}}\right] \in \mathbb R^{2n}
  \label{eq:statecps}
\end{equation}
%The system dynamics can be then written as
%\begin{equation}
%  \begin{split}
%  \tilde x(k+1) &= \left[{\begin{array}{*{20}c}
%    A+BL&-BL\\
%    0&A-KCA 
%  \end{array}}\right]\tilde x(k) \\
%  &+ \left[{\begin{array}{*{20}c}
%    I&0\\
%    I-KC&-K
%  \end{array}}\right]\left[{\begin{array}{*{20}c}
%    w(k)\\
%    v(k+1)
%  \end{array}}\right]
%  \end{split}
%  \label{eq:cpsdynamic}
%\end{equation}
\subsection{$\chi^2$ Failure Detector}
Failure detectors are often used to detect anomalous operations. We assume that a $\chi^2$ failure detector (\cite{Mehra:1971fl},\cite{Greenwood:6tw}) is deployed, which computes the following quantity
\begin{equation}
  \label{eq:chisquredetector}
  g(k) = z(k)^TP_z^{-1}z(k),
\end{equation}
where $P_z$ is the covariance matrix of the residue $z(k)$ is a constant matrix since we assume the system is in steady state. Define $P_z^{-\frac{1}{2}}$ to be a symmetric matrix such that $P_z^{-\frac{1}{2}}\times P_z^{-\frac{1}{2}} = P_z^{-1}$. Thus, \eqref{eq:chisquredetector} can be rewritten as $g(k) = \|P_z^{-\frac{1}{2}}z(k)\|^2$.

Since $z(k)$ is Gaussian distributed~\cite{Mehra:1971fl}, $g(k)$ is $\chi^2$ distributed with $m$ degrees of freedom. The $\chi^2$ failure detector compares $g(k)$ with a threshold $\eta$ and triggers ad alarm if $g(k)$ is greater than $\eta$. Let us define the probability of triggering an alarm at time $k$ as 
\begin{equation}
  \beta(k) \triangleq P(g(k) \geq \eta). 
  \label{eq:alarmrate}
\end{equation}
When the system is operating normally, $\beta(k)$ is a constant, which is defined as the false alarm rate $\alpha$. In common practice, $\alpha$ is small since false alarms tend to increase operation cost. 

%Figure~\ref{fig:lticontrolsystem} shows the diagram of the CPS. 
%\begin{figure}[htpb]
%  \begin{center}
%    \inputtikz{systemdiagram}
%  \caption{System Diagram}
%  \label{fig:lticontrolsystem}
%  \end{center}
%\end{figure}
