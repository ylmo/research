\documentclass[10pt,oneside,a4paper]{article}

\usepackage{amsmath}
\usepackage{amsfonts,amssymb,amsthm}
\usepackage{tikz} %add back

\DeclareMathOperator{\Pre}{Pre}
\DeclareMathOperator{\Rch}{Rch}
\newcommand{\alert}[1]{\textcolor{red}{#1}}
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\author{Yilin Mo, Bruno Sinopoli}

\title{Answers to AE's and reviewer's comments}

\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their remarks. The major changes are highlighted by \comment{blue} color in the manuscript. The detailed response to each reviewer is attached below.

\section{Response to Reviewer \#1}
The authors have addressed some of the comments satisfactorily, and the overall presentation is improved. However, the proof of Theorem 2 is still quite problematic. In particular, the part on page 11 is carelessly written with confusing logic. 

\comment{We thank the reviewer for the insightful comments. We have modified the proof of Theorem 2 to make it more readable.}

Some other issues include the following:
\begin{enumerate}
  \item Page 4, it is better to add a reference for ``it is well known that the closed-loop system is stable if and only if both $A - KCA$ and $A + BL$ are stable''. 

    \comment{A reference has been added to support this statement.}
  \item Page 8, in remark 1, it sounds like speculation when saying ``However, it is entirely possible that the attacker, although cannot inject anything when enforcing $\delta= 0$, can inflict a large perturbation into the system with a small $\delta$, which is hardly detectable in a noisy system.'' It is better to provide a simple example. 

    \comment{We modified the simulation part to highlight this phenomenon.}
\end{enumerate}
\newpage
\section{Response to Reviewer \#3}

In the previous report I raised two main concerns: 
\begin{enumerate}
  \item[(I)] In regard to the objective of the paper and the proposed approach, I asked that how the stochasticity of the system dynamics may allow the attacker to introduce a stealthy intrusion which does {\bf NOT} work in the deterministic setting (i.e., in the absence of the process noise);
  \item[(II)] Clarify the contributions of the paper, in particular from the theoretical viewpoint, in comparison with the existing literature in the field.
\end{enumerate}


In response to (I), the authors modified Remark 1 and explicitly mentioned that: 
\begin{enumerate}
  \item  The adversary cannot inject anything without violating the nominal false alarm rate.
  \item  However, the adversary can inflict a large perturbation with minimum increase in the detection rate.
\end{enumerate}
Item 1 is exactly what I mentioned in my previous report. In fact, this is not the case iff the deterministic version of the system is also vulnerable to this attack, i.e., the attack signal is not detectable even in the absence of noise and when $\alpha = 0$. 

I believe item 2 is one of the questions that the paper should address. This issue is discussed in Theorem 1, but in an asymptotic fashion. That is, it is essentially shown that the mapping from $\zeta(\dot)$ (attack signal) to $g(\dot)$ (residue signal) is continuous when the spaces are equipped with the infinity norm. This is not really surprising as you are working with linear bounded systems with Gaussian process noise (or any other noise with atomless measure), which render everything continuous.  

Let me highlight once again: one may {\bf EXACTLY} follow your argument and claim that for any arbitrary small violation of $\alpha = 0$, say $\varepsilon$, there exists $\delta$ such that if (18) holds then $\beta(k) \leq \alpha + \varepsilon = \varepsilon$. Consequently, he can also continue with "without loss of generality, $\delta = 1$ ..." . If you meant this, I believe you are {\bf NOT }exploiting the fact that the system is stochastic; you are just using the {\bf LINEARITY} of the dynamics to split the attack contribution in the residue and normalize it. In this view, I believe you need a more explicit quantification in Theorem 1, which in my opinion is plausible since you know the distribution of $g(.)$ (Gaussian with known mean and variance). 

\comment{We thank the reviewer for the constructive comments. We have changed Theorem~1 to provide a better characterization of the relationship between $\delta$ and $\beta(k)$.}

For the simulation, since the paper aims to utilize the randomness of the system to synthesize a stealthy attack, it would be more appropriate if it provides an example in which the deterministic setting is ``safe'' while the existence of system and/or measurements noise renders the system vulnerable to the proposed attack. It seems to me that in this case you may not be able to achieve an unbounded reachable set, but this is exactly what makes this paper distinguished from the existing results in the literature. The attack with unbounded set in your setting would probably require the attack being undetectable even when the original system is noiseless with perfect measurements.  

\comment{We have modified the simulation section to highlight the case where a stealthy attack with an unbounded reachable set is possible for noisy system but not possible for deterministic system.}

In response to (II), the authors said that 

``We believe that the main contribution of this paper is to formulate the stealthy attack design problem as an constrained control problem, which enables the usage of the existing tools to characterize the performance degradation of the system.''

And on the theoretical side mentioned 

``In our attack model, we assume that the attacker can inject an arbitrary large input as long as it compromises the sensors. Hence, the techniques developed in [25]''

The authors should clarify what is new in this formulation comparing to their earlier works [14] and [15]; they apparently introduced the same attack scenario and the corresponding modeling structure. 

\comment{In [14] and [15], we only consider the attack on sensors, i.e., $B^a = 0$. It is simpler due to the fact that the attacker cannot inject an arbitrarily large bias on the sensor measurements. However, in this article, we consider the attack on both sensing and actuating. In this setting, the adversary can inject arbitrary large control and sensor bias, as long as they cancel each other in the residue. Therefore, the analysis is arguably more complicated.}

Toward the theoretical contributions of the reachability problems, let me remark that the setting in [19] is for uncertain nonlinear dynamics, which is significantly more general than the discussion in this paper on deterministic linear systems. In addition, note that [19] also discussed the uncertain linear finite dimensional case with the computational tools for the ellipsoid regions based on a recursive matrix equation of the Riccati type. I believe the paper lacks more direct comparisons with [19] and clear explanations of differences; these information should be included in the paper rather than one sentence in the response letter. 

\comment{We have added the comparison between our work and [19] in the paper. To summarize, in [19], to compute the $R^*(X)$ (the set $\mathcal C_\infty$ in our case) , the set $C_n(X)$ needs to be compact, which essentially implies that $R^*(X)$ must be compact (See Proposition 4 in [19]). However, in this article, we only require the set to be closed, symmetric and convex (See the proof of Proposition 1 in Appendix A). This can be seen as a generalization of the [19] to the unbounded case. }

\comment{On the other hand, in [19], the author only provide an inner approximation of the $R^*(X)$ (the set $\mathcal C_\infty$ in our case), while in our paper, we provide both inner and outer approximation. Furthermore, we not only consider the invariant set, but also the computation of the reachable set (i.e., those states that can be reached from $0$ without exiting the invariant set), which is not presented in [19].}

Overall, given that the IEEE TAC is one of the best (if not the best) journal in control, I think the paper needs some more works. 

Typos: 
\begin{itemize}
  \item Page 2: to be bening 
  \item Page 8: As a consequence (18) 
  \item Page 10: Proof. First we need to proof …
  \item Page 14: the limits for both sequences exists
\end{itemize}
\comment{Fixed} 
\newpage
\section{Response to Reviewer \#4}
Compared to its first version, the paper is now clearer and more readable. The authors have addressed all the reviewer's comments, and the paper is now ready for publication.

Typos: 
\begin{enumerate}
  \item before equation (8), should eta be $\eta$? ; 
  \item in the proof of Theorem 2, ``First we need to proof'' should it be ``prove''? 
\end{enumerate}

\comment{We thank the reviewer for the valuable comments. We have fixed the typos and checked the rest of the paper.}
\end{document}
