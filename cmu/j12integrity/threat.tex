\section{Threat Model}
\label{sec:threat}
In this section we describe the integrity attack model on the CPS. We assume that an adversary has the following capabilities:
\begin{enumerate}
  \item The adversary knows the static parameters of the system, namely $A,\,B,\,C,\,K,\,L$ and the statistics of the measurement and process noise $Q$ and $R$.
  \item The adversary compromises a subset $\{i_1,\dots,i_l\}\subseteq\{1,\dots,m\}$ of sensors. The adversary can add arbitrary bias to the readings of compromised sensors. Let us define the sensor selection matrix $\Gamma$ as
    \begin{equation}
      \Gamma \triangleq \left[e_{i_1},\dots,e_{i_l}\right]\in \mathbb R^{m\times l},
    \end{equation}
    where $e_i$ is the $i$th vector of the canonical basis of $\mathbb R^{m}$. As a consequence, $\Gamma$ is full column rank. Let us further define the bias introduced by the attacker $y^a(k)\in \mathbb R^l$ as
    \begin{displaymath}
      y^a(k) \triangleq [y^a_{i_1}(k),\dots,y^a_{i_l}(k)]^T,
    \end{displaymath}
    where $y^a_i(k)$ indicates the bias on sensor $i$ at time $k$. Thus, the modified reading received by the estimator can be written as
    \begin{equation}
      y(k) = Cx(k) + \Gamma y^a(k) + v(k),
      \label{eq:modifiedreading2}
    \end{equation}

  \item The adversary can inject external control inputs to the system. As a result, the system equation becomes 
    \begin{equation}
      x(k+1) = Ax(k) + Bu(k)+ B^au^a(k)+ w(k), 
      \label{eq:compromisedsystemdescription}
    \end{equation}
    where $B^a\in \mathbb R^{n\times q}$ characterizes the direction of control inputs the attacker can inject to the system. Without loss of generality, we assume that $B^a$ is of full column rank.
  \item We assume the injection of control inputs starts at time $0$ and the manipulation of sensor measurements starts at time $1$. In other words,
    \begin{displaymath}
      u^a(k) = 0,\forall k \leq -1,\text{ and }y^a(k) = 0,\forall k\leq 0.
    \end{displaymath}
\end{enumerate}

Since the system is linear, the cyber-physical state $\tilde x(k)$ can be decomposed into two parts: $\tilde x^n(k)$, the state generated by noise and $\tilde x^c(k)$, the state generated by the attacker's action. In particular, one can prove that during the attack
\begin{equation}
  \begin{split}
  \tilde x^c&(k+1) = \left[{\begin{array}{*{20}c}
    A+BL&-BL\\
    0&A-KCA 
  \end{array}}\right] \tilde x^c(k) \\
  &+ \left[{\begin{array}{*{20}c}
    B^a&0\\
    B^a - KCB^a&-K\Gamma
  \end{array}}\right]\left[{\begin{array}{*{20}c}
    u^a(k)\\
    y^a(k+1)
  \end{array}}\right],
  \label{eq:diffdynamic}
  \end{split}
\end{equation}
\begin{equation}
  \begin{split}
  \tilde x^n&(k+1) = \left[{\begin{array}{*{20}c}
    A+BL&-BL\\
    0&A-KCA 
  \end{array}}\right] \tilde x^n(k)\\
  &+  \left[{\begin{array}{*{20}c}
    I&0\\
    I-KC&-K
  \end{array}}\right]\left[{\begin{array}{*{20}c}
    w(k)\\
    v(k+1)
  \end{array}}\right].
  \end{split}
\end{equation}
Similarly, the residue vector $z(k)$ can be decomposed in to $z^c(k)$ and $z^n(k)$ as follows
\begin{equation}
  z^c(k+1)= \left[{\begin{array}{*{20}c}
    0& CA 
  \end{array}}\right] \tilde x^c(k) + \left[{\begin{array}{*{20}c}
    CB^a&\Gamma
  \end{array}}\right] \left[{\begin{array}{*{20}c}
    u^a(k)\\
    y^a(k+1)
  \end{array}}\right],
  \label{eq:diffoutput}
\end{equation}
\begin{equation}
  z^n(k+1)= \left[{\begin{array}{*{20}c}
    0& CA 
  \end{array}}\right] \tilde x^n(k) + \left[{\begin{array}{*{20}c}
    C & I_m 
  \end{array}}\right]\left[{\begin{array}{*{20}c}
    w(k)\\
    v(k+1)
  \end{array}}\right].
\end{equation}
To simplify notations, let us define the following matrices:
\begin{align}
  \tilde {A} & \triangleq \left[{\begin{array}{*{20}c}
    A+BL & -BL\\
    0 & A-KCA 
  \end{array}}\right] \in \mathbb R^{2n\times 2n}, \\
  \tilde {B}  &\triangleq \left[{\begin{array}{*{20}c}
    B^a & 0 \\
    B^a - KCB^a & -K\Gamma
  \end{array}}\right]\in \mathbb R^{2n\times (q+l)},\\
  \tilde {C} & \triangleq P_z^{-1/2} \left[{\begin{array}{*{20}c}
    0 & CA 
  \end{array}}\right]\in \mathbb R^{m\times 2n},\\
  \tilde {D}  &\triangleq P_z^{-1/2}\left[{\begin{array}{*{20}c}
    CB^a & \Gamma
  \end{array}}\right] \in \mathbb R^{m\times (q+l)}.
\end{align}
and the attacker's input $\zeta(k)$ at time $k$ as
\begin{equation}
  \zeta(k) \triangleq \left[{\begin{array}{*{20}c}
    u^a(k)\\
    y^a(k+1)
  \end{array}}\right].
\end{equation}
We further define the attacker's action $\zeta^\infty \triangleq (\zeta(0),\zeta(1),\dots)$ as an infinite sequence of $\zeta(k)$s\footnote{If the attack stops at time $T$, then $\zeta(k) = 0$ for all $k > T$.}. It is clear that $\tilde x^c(k)$ and $z^c(k)$ are functions of $\zeta^\infty$. Thus, we can write them as $\tilde x^c(k,\zeta^\infty)$ and $z^c(k,\zeta^\infty)$ respectively. However, for simplicity, we will omit $\zeta^\infty$ when there is no confusion.

Our goal is to characterize the evolution of the state $\tilde x(k)$ during the integrity attack. It is easy to verify that $\tilde x^n(k)$ is a stationary Gaussian process, which has the same statistics as $\tilde x(k)$ in the absence of the attacker. Consequently we focus on $\tilde x^c(k)$, i.e., the state generated by the attacker's action. 
%Therefore,
%\begin{equation}
%  \begin{split}
%    \mathbb E \tilde x^c(k+1) =\tilde{A} \mathbb E \tilde x^c(k) + \tilde{B}\zeta^a(k),
%  \end{split}
%  \label{eq:diffdynamicsimple}
%\end{equation}
%and
%\begin{equation}
%  \mathbb E z^c(k+1)= \tilde{C} \mathbb E \tilde x^c(k) + \tilde{D} \zeta^a(k).
%  \label{eq:diffoutputsimple}
%\end{equation}

%It is clear that $\mathbb E z^c(k),\mathbb E \tilde x^c(k)$ are functions of the attacker's actions $(\zeta^a(0),\zeta^a(1),\ldots)$. Let us define $\zeta^a = (\zeta^a(0),\zeta^a(1),\ldots)$ as the infinite sequence of the attacker's actions. As a result, we can write $\mathbb E z^c(k),\mathbb E \tilde x^c(k)$ as $\mathbb E z^c(k,\zeta^a),\,\mathbb E \tilde x^c(k,\zeta^a)$ respectively. We will omit the parameter $\zeta^a$ when there is no confusion.

It is clear that without any constraint on the attacker's action, the reachable region of $\tilde x^c(k)$ is the reachable subspace of $(\tilde A,\tilde B)$. However, if the adversary does not design its input $\zeta(k)$ cautiously, an alarm may be triggered and the attack will be stopped by the system operator before the attacker achieves his goal. As a result, we restrict our attention to stealthy attacks. In other words, the attacker wishes to minimize the probability of triggering the failure detector. Ideally, to achieve this goal, the attacker would choose its action $\zeta^\infty$, such that the following condition holds for all $k = 0,1,\ldots$:
\begin{equation}
  \beta(k) = P(g(k)\geq \eta) =  P(\|P_z^{-1/2}z(k)\|^2\geq \eta) \leq p^a,
  \label{eq:idealineq}
\end{equation}
where $p^a$ is a threshold probability chosen by the attacker. However, a closed form solution of $\beta(k)$ is not attainable since it involves integrating a Gaussian distribution over an ellipsoid. Hence, \eqref{eq:idealineq} is difficult to enforce by the attacker. As a result, we assume that attacker constrains his action $\zeta^\infty$ to satisfy the following condition instead:
\begin{equation}
  \|P_z^{-1/2} z^c(k)\| = \|\tilde C\tilde x^c(k) + \tilde D\zeta(k)\|\leq \delta.\,\forall k=0,1,\ldots
  \label{eq:feasiblecondition}
\end{equation}
where $\delta$ is a design parameter of the attacker. We assume that the attacker's goal is to enforce \eqref{eq:feasiblecondition} for all $k\geq 0$, o as not to be detected at all. Since $z(k) = z^c(k) + z^n(k)$ and $z^n(k)$ has the same distribution as $z(k)$ in the absence of the attack, the adversary can make $z(k)$ very similar to the ``normal'' $z(k)$ by enforcing that $z^c(k)$ is small. Such an observation is quantified by the following theorem:
\begin{theorem}
  \label{theorem:adversaryconstraint}
  For any $\varepsilon > 0$, there exists a $\delta > 0$, such that if \eqref{eq:feasiblecondition} holds for all $k$, i.e., 
  \begin{displaymath}
    \|P_z^{-1/2} z^c(k)\|  \leq \delta.\,\forall k=0,1,\ldots
  \end{displaymath}
  then
  \begin{displaymath}
    \beta(k)\leq \alpha + \varepsilon, k=0,1,\dots,
  \end{displaymath}
  where $\alpha$ is the false alarm rate of the $\chi^2$ detector.
\end{theorem}
\begin{proof}
  First, by the fact that $P_z$ is positive definite, we can prove that for any $\delta_1 > 0$, the following inequality holds:
  \begin{displaymath}
    \begin{split}
    2(z^c(k))^T &P_z^{-1}z^n(k)\\
    &\leq \delta_1 (z^n(k))^TP_z^{-1}z^n(k) + 1/\delta_1 (z^c(k))^TP_z^{-1}z^c(k).
    \end{split}
  \end{displaymath}
  As a result, if \eqref{eq:feasiblecondition} holds for all $k$, then
  \begin{align*}
    g(k)& \leq(1+\delta_1) (z^n(k))^TP_z^{-1} z^n(k) + (1+1/\delta_1)\delta,\, \forall \delta_1 > 0.
  \end{align*}
  Therefore,
  \begin{displaymath}
    \beta(k)   \leq P(( z^n(k))^TP_z^{-1} z^n(k)\geq \frac{\eta-(1+1/\delta_1)\delta}{1+\delta_1} ) .
  \end{displaymath}

  One can verify that $z^n(k)$ is i.i.d. Gaussian distributed with zero mean and covariance of $P_z$ as it is identical to $z(k)$ in the absence of the attacker. Thus $(z^n(k))^TP_z^{-1} z^n(k)$ is $\chi^2$ distributed with $m$ degree of freedom, which implies that
  \begin{displaymath}
    P(( z^n(k))^TP_z^{-1} z^n(k)\geq \eta)= \alpha .
  \end{displaymath}
  By choosing $\delta,\delta_1$, we can make $(\eta-(1+1/\delta_1)\mathcal T\delta)/(1+\delta_1) $ arbitrarily close to $\eta$. Thus, $\beta(k)$ can be made arbitrarily close to $\alpha$, which finishes the proof.
\end{proof}
\begin{remark}
 By Theorem~\ref{theorem:adversaryconstraint}, if the adversary chooses a small $\delta$, the detection rate $\beta(k)$ will be close to the false alarm rate $\alpha$, i.e., the alarm rate during normal operation. In other words, the failure detector can hardly distinguish between a ``normal'' system and a system that is under attack. Since $\alpha$ is in general very small for practical systems, the attack can go on for an extended period of time without incurring detection.
\end{remark}
As a consequence of Theorem~\ref{theorem:adversaryconstraint}, we can model the attacker's strategy as a constrained control problem, where the system equation is given by:
\begin{equation}
\tilde x^c(k+1) = \tilde A \tilde x^c(k) + \tilde B \zeta(k),  
\end{equation}
and the constraint is as follows: 
\begin{equation}
\|\tilde C\tilde x(k) + \tilde D \zeta(k)\| \leq \delta,\,\forall k =0,1,\dots
\end{equation}

Our goal is to compute the reachable region of the state $\tilde x^c(k)$, which indicates the resilience of the system against integrity attacks. Due to the linearity of the system, we assume, without loss of generality, that $\delta = 1$ for the rest of the paper, leading to the following definition: 
\begin{definition}
  The attacker's action $\zeta^\infty$ is called feasible if \eqref{eq:feasiblecondition} holds for all $k$ and $\delta = 1$. 
\end{definition}
We now define the reachable region of $\tilde x^c(k)$:
\begin{definition}
 The reachable region $\mathcal R_{k}$ of $\tilde x^c(k)$ at time $k$ is defined as
\begin{equation}
  \mathcal R_{k} \triangleq \{\tilde x\in \mathbb R^{2n}:\tilde x = \tilde x^c(k,\zeta^\infty) ,\textrm{ for some feasible }\zeta^\infty\}.
\end{equation}
The union of all $\mathcal R_k$ is defined as:
\begin{equation}
  \mathcal R \triangleq \bigcup_{k=0}^\infty\mathcal R_{k}.  
\end{equation}
\end{definition}
Thus, $\mathcal R$ indicates all possible biases that an attacker can introduce to the system. 

\begin{remark}
  For a noiseless system model considered in \cite{fp-ab-fb:09b,Pasqualetti2011,wirelesscontrol,Fawzi2012}, the adversary has to enforce that \eqref{eq:feasiblecondition} holds for $\delta = 0$ to avoid being detected, as even a small deviation from the nominal behavior of the system will result in an alarm. Thus, it is easy to verify that $\mathcal R$ is a subspace of $\mathbb R^{2n}$. On the other hand, for our threat model, small deviations are indistinguishable from noise. As a result, the attacker has more freedom to manipulate the system and hence the reachable region $\mathcal R$ is a superset of the reachable region discussed in \cite{fp-ab-fb:09b,Pasqualetti2011,wirelesscontrol,Fawzi2012}.
\end{remark}
