\section{System Description}
\label{sec:dynamicsysdescription}

We model the system as a linear control system, which is equipped with a steady-state Kalman filter, a linear feedback controller and a $\chi^2$ failure detector. We assume that the physical system also has LTI dynamics, which take the following form:
\begin{equation}
  x(k+1) = Ax(k) + Bu(k) +  w(k), 
  \label{eq:systemdescription}
\end{equation}
where $x(k) \in \mathbb R^n$ is the vector of state variables at time $k$, $u(k) \in \mathbb R^p$ is the control input, $w(k)\in \mathbb R^n$ is the process noise at time $k$ and $x(0)$ is the initial state. $w(k),\,x(0)$ are independent Gaussian random variables, and $x(0) \sim \mathcal N(0,\;\Sigma)$, $w(k) \sim \mathcal N(0,\;Q)$. 

A sensor network is deployed to monitor the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are collected and sent to a centralized estimator. The observation equation can be written as  
\begin{equation}
  y(k) = C x(k) + v(k),
  \label{eq:sensordescription}
\end{equation}
where $y(k)  = [y_{1}(k),\ldots,y_{m}(k)]^T \in \mathbb R^m$ is a vector of sensor measurements, and $y_{i}(k)$ is the measurement made by sensor $i$ at time $k$. $v(k) \sim \mathcal N(0,\;R)$ is i.i.d. measurement noise independent of $x(0)$ and $w(k)$. 

A Kalman filter is used to compute state estimation $\hat x(k)$ from observations $y(k)$:
\begin{align*}
  \hat x(k)& = \hat x(k|k - 1)  + K(k) (y(k)  - C \hat x (k|k - 1) ) ,\\
  P(k) &= P(k|k - 1)  -  K(k) CP(k|k - 1),
\end{align*}
where
\begin{align*}
  \hat x (k + 1|k) & = A \hat x(k) +Bu(k)  , \, P(k + 1|k)  = AP(k) A^T  + Q,\,\nonumber\\
  K(k)&= P(k|k - 1) C^T (CP(k|k - 1) C^T  + R)^{ - 1},  \\
  \hat x(0| -1) & =0 ,\, P(0|-1)  = \Sigma.
\end{align*}
Although the Kalman filter uses a time varying gain $K(k)$, it is well known that this gain will converge if the system is detectable. In practice the Kalman gain usually converges in a few steps. It is a common practice to use the steady state gain in place of the dynamic one as it still guarantees asymptotic optimality. As a consequence we can safely assume the Kalman filter to be already in steady state. Let us define
\begin{equation}
  P \triangleq \lim_{k\rightarrow\infty}P(k|k-1),\,K \triangleq P C^T (CP C^T  + R)^{ - 1}.
\end{equation}
The update equations of Kalman filter are as follows:
\begin{equation}
  \begin{split}
  \hat x(k+1) &= A \hat x(k) + B u(k)\\
  &+ K \left\{y(k+1) - C\left[A\hat x(k) + Bu(k)\right]\right\}.
  \label{eq:kalmanestimator}
  \end{split}
\end{equation}
Since the system is in steady state, we further assume that $\Sigma = P$. Let us define the residue $z(k)$ and estimation error $e(k)$ at time $k$ to be
\begin{equation}
  z(k) \triangleq y(k) - C\hat x(k|k-1),\,e(k) \triangleq x(k)-\hat x(k).
  \label{eq:residue}
\end{equation}
We assume that an LTI feedback controller is used to stabilize the system, which takes the following form:
\begin{equation}
  u(k) = L\hat x(k)
  \label{eq:feedbackcontrol}
\end{equation}

It is well known that the closed-loop system is stable if and only if both $A-KCA$ and $A+BL$ are stable. For the rest of the discussion we will only focus on systems that are closed-loop stable. 

Consider the CPS consisting of the physical system, Kalman filter and linear controller. We can immediately identify $x(k)$ as the ``physical'' state and $\hat x(k)$ as the ``cyber'' state. Thus, we define the state of the system $\tilde x(k)$ as\footnote{We use $e(k)$ instead of $\hat x(k)$ to simplify \eqref{eq:cpsdynamic}.}
\begin{equation}
  \tilde x(k) \triangleq \left[{\begin{array}{*{20}c}
    x(k)\\
    e(k)
  \end{array}}\right]  = \left[{\begin{array}{*{20}c}
    I_n & 0 \\
    I_n & - I_n
  \end{array}}\right] \left[{\begin{array}{*{20}c}
    x(k)\\
    \hat x(k)
  \end{array}}\right] \in \mathbb R^{2n}
  \label{eq:statecps}
\end{equation}
The system dynamics can be then written as
\begin{equation}
  \begin{split}
  \tilde x(k+1) &= \left[{\begin{array}{*{20}c}
    A+BL&-BL\\
    0&A-KCA 
  \end{array}}\right]\tilde x(k) \\
  &+ \left[{\begin{array}{*{20}c}
    I&0\\
    I-KC&-K
  \end{array}}\right]\left[{\begin{array}{*{20}c}
    w(k)\\
    v(k+1)
  \end{array}}\right]
  \end{split}
  \label{eq:cpsdynamic}
\end{equation}
\subsection{$\chi^2$ Failure Detector}
Failure detectors are often used to detect anomalous operations. We assume that a $\chi^2$ failure detector (\cite{Mehra:1971fl},\cite{Greenwood:6tw}) is deployed, which computes the following quantity
\begin{equation}
  \label{eq:chisquredetector}
  g(k) = z(k)^TP_z^{-1}z(k),
\end{equation}
where $P_z = CPC^T+R$ is the covariance matrix of the residue $z(k)$. To simplify the notation, we define $P_z^{-1/2}$ to be a symmetric matrix such that the following equation holds:
\begin{displaymath}
  P_z^{-1/2}\times P_z^{-1/2} = P_z^{-1}.
\end{displaymath}
Thus, \eqref{eq:chisquredetector} can be rewritten as
\begin{equation}
  g(k) = \|P_z^{-1/2}z(k)\|^2. 
\end{equation}
Since $z(k)$ is i.i.d. Gaussian~\cite{Mehra:1971fl}, $g(k)$ is $\chi^2$ distributed with $m$ degrees of freedom. As a result, $g(k)$ cannot be far away from $0$. The $\chi^2$ failure detector compares $g(k)$ with a certain threshold. If $g(k)$ is greater than the threshold, then an alarm is triggered. Let us define the probability of detection at time $k$ as 
\begin{equation}
  \beta(k) \triangleq P(g(k) \geq \eta), 
  \label{eq:alarmrate}
\end{equation}
where $\eta$ is the threshold designed by the system operator. When the system is operating normally, $\beta(k)$ equals the false alarm rate \footnote{Since the system is already in steady state, $\alpha$ is time invariant.} $\alpha$, which is small in common practice since false alarms tend to increase operation cost. 

Figure~\ref{fig:lticontrolsystem} shows the diagram of the CPS. 
\begin{figure}[htpb]
  \begin{center}
    \inputtikz{systemdiagram}
  \caption{System Diagram}
  \label{fig:lticontrolsystem}
  \end{center}
\end{figure}
