\section{Main results}
\label{sec:main}
In this section, we consider the problem of computing the reachable set $\mathcal R_k$ and $\mathcal R$. In Section~\ref{sec:recursivedef}, we provide a recursive definition of $\mathcal R_k$ based on the concept of controlled invariant set. We further devote Section~\ref{sec:ellipsoidalapprox} to the numerical approximation of $\mathcal R_k$ and $\mathcal R$.
\subsection{Recursive Definition of $R_k$}\label{sec:recursivedef}
 Before continuing on, we need to introduce the concept of reach set and one-step set:
\begin{definition}
  Define the reach set $\Rch(\mathcal S)$ of set $\mathcal S\subseteq \mathbb R^{2n}$ to be
  \begin{align}
    \Rch(\mathcal S)& \triangleq \{\tilde x^c_+\in\mathbb R^{2n}: \exists\zeta\in \mathbb R^{p+l},\tilde x^c\in S,\nonumber\\
   & s.t.,\tilde A\tilde x^c+\tilde B\zeta = \tilde x^c_+,\,\|\tilde C\tilde x^c+\tilde D\zeta\|\leq 1\} .
  \end{align}
\end{definition}

\begin{definition}
  Define the one-step set $\Pre(\mathcal S)$ of set $\mathcal S\subseteq \mathbb R^{2n}$ to be
  \begin{align}
    \Pre(\mathcal S) &\triangleq \{\tilde x^c\in\mathbb R^{2n}:\exists\zeta\in \mathbb R^{p+l},\nonumber\\
    &s.t.,\tilde A\tilde x^c+\tilde B\zeta \in \mathcal S,\,\|\tilde C\tilde x^c+\tilde D\zeta\|\leq 1\} .
  \end{align}
\end{definition}

\begin{remark}
The reach set of $\mathcal S$ indicates all the states $\tilde x^c(k+1)$ that can be reached with a one-step admissible attacker's input $\zeta(k)$, when the current state $\tilde x^c(k)$ is in $\mathcal S$. On the other hand, the one-step set of $\mathcal S$ indicates all the previous states $\tilde x^c(k-1)$ that can be driven into $\mathcal S$ with a one-step admissible $\zeta(k-1)$. 
\end{remark}
At a first glance, it seems that $\mathcal R_k$ can be recursively defined as $\mathcal R_{k+1} = \Rch(\mathcal R_k)$. However, the reach set only guarantees that $\|P_z^{-1/2}z^c(k)\|\leq 1$ for the current $k$, not for the future $k$s. Thus, it is possible that $\Rch(\mathcal R_k)$ is strictly larger than $\mathcal R_{k+1}$. To define $\mathcal R_{k}$ recursively, we need to introduce the concept of controlled invariant set.

\begin{definition}
  A set $\mathcal C\subseteq \mathbb R^{2n}$ is called controlled invariant if for all $\tilde x^c(k) \in \mathcal C$, there exists a $\zeta(k)$, such that the following inequalities hold:
  \begin{equation}
    \tilde A\tilde x^c(k) + \tilde B\zeta(k)\in \mathcal C,\,\|\tilde C \tilde x^c(k) + \tilde D\zeta(k)\|\leq 1. 
  \end{equation}
\end{definition}
In other words, if the current state $\tilde x^c(k)$ belongs to $\mathcal C$, then the attacker can always use a feasible $\zeta(k)$ to enforce that the next state $\tilde x^c(k+1)$ and hence all the future states are in $\mathcal C$. The following proposition characterizes several important properties of the reach set, the one-step set and the controlled invariant set:
\begin{proposition}
  \label{proposition:positiveinvariance}
  The following statements hold for the operator $\Pre,\,\Rch$ and the controlled invariant set:
  \begin{enumerate}
    \item $\Pre$ and $\Rch$ are monotonically increasing, i.e., if $\mathcal S_1\subseteq \mathcal S_2$, then 
      \begin{equation}
      \Pre(\mathcal S_1)\subseteq \Pre(\mathcal S_2),\,\Rch(\mathcal S_1)\subseteq \Rch(\mathcal S_2).
      \end{equation}
    \item Let $\mathcal C$ to be a controlled invariant set, then $\mathcal C \subseteq \Pre(\mathcal C)$
    \item There exists the maximum controlled invariant set $\mathcal C_{\infty}$, such that $\mathcal C\subseteq \mathcal C_{\infty}$ for all controlled invariant set $\mathcal C$.
    \item Let $\mathcal C_0 = \mathbb R^{2n}$ and $\mathcal C_{k+1} = \Pre(\mathcal C_k)$. Then the following equality holds:
      \begin{equation}
	\mathcal C_\infty = \bigcap_{k=0}^\infty \mathcal C_k.
	\label{eq:maxcontrolledinvariant}
      \end{equation}
  \end{enumerate}
\end{proposition}

\begin{proof}
  The proof of the first three properties can be found in \cite{Kerrigan2000}, while the proof of the last property is quite technical and is reported in the appendix to improve legibility\footnote{It is worth noticing that for general systems and feasibility constraints, e.g., non-linear systems with constraints other than \eqref{eq:feasiblecondition}, \eqref{eq:maxcontrolledinvariant} is not necessarily true\cite{Bertsekas1972}. For general settings, a sufficient condition~\cite{Vidal00} to guarantee \eqref{eq:maxcontrolledinvariant} is $\mathcal C_k = \mathcal C_{k+1}$ for some $k$.}.
\end{proof}

We are now ready to provide a recursive definition of $\mathcal R_k$:
\begin{theorem}
  \label{theorem:recursiver}
  $\mathcal R$ is controlled invariant, and hence $\mathcal R \subseteq \mathcal C_{\infty}$. Furthermore $\mathcal R_k$ satisfies the following recursive equation 
  \begin{equation}
    \mathcal R_{k+1} = \Rch(\mathcal R_k)\bigcap \mathcal C_{\infty}.
    \label{eq:reachablesetrecursive}
  \end{equation}
  with initial condition $\mathcal R_{0} = \{0\}$.
\end{theorem}

\begin{proof}
     First we need to proof that $\mathcal R$ is controlled invariant. To this end, let us choose an arbitrary $\tilde x\in \mathcal R$. By the definition of $\mathcal R$, we know that
      \begin{displaymath}
	\tilde x =  \tilde x^c(k,\zeta^\infty),
      \end{displaymath}
      for some $k$ and feasible $\zeta^\infty$. As a result,
      \begin{displaymath}
	\tilde {A}\tilde x+\tilde {B}\zeta(k) = \tilde A \tilde x^c(k,\zeta^\infty) + \tilde B\zeta(k) = \tilde x^c(k+1,\zeta^\infty)\in \mathcal R,
      \end{displaymath}
      and 
      \begin{displaymath}
	\|\tilde{C}\tilde x+\tilde {D}\zeta(k)\| = \|P_z^{-1/2} z^c(k+1,\zeta^\infty)\|\leq 1,
      \end{displaymath}
      which implies that $\mathcal R$ is controlled invariant. Thus, $\mathcal R \subseteq \mathcal C_\infty$ due to the maximality of $\mathcal C_\infty$.

     We now prove \eqref{eq:reachablesetrecursive} by induction. Since the attack starts at time $0$, $\mathcal R_{0} = \{0\}$. Now assume that \eqref{eq:reachablesetrecursive} holds for $k$. From the definition of $\mathcal R_{k+1}$, and the fact that $\mathcal R_{k+1}\subseteq \mathcal R\subseteq \mathcal C_\infty$, it is trivial to prove that
      \begin{displaymath}
	\mathcal R_{k+1} \subseteq \Rch(\mathcal R_k)\bigcap \mathcal C_\infty.
      \end{displaymath}
      Therefore, we only need to prove opposite side of the set incursion. In other words, we need to prove that for all $\tilde x\in \Rch(\mathcal R_k)\bigcap \mathcal C_\infty$, there exists a feasible $\zeta^\infty$, such that $\tilde x = \tilde x^c(k+1,\zeta^\infty)$.
      
      From the induction assumption and the fact that $\tilde x \in \Rch(\mathcal R_k)$, we know that there exist $\zeta(0),\ldots,\zeta(k)$ and $\tilde x^c(0),\dots,\tilde x^c(k),\tilde x^c(k+1) = \tilde x$, such that
      \begin{displaymath}
	\tilde x^c(i+1) = \tilde A \tilde x^c(i)+\tilde B\zeta(i), \|\tilde C\tilde x^c(i) +\tilde D \zeta(i)\|\leq 1,i = 0,\dots,k.
      \end{displaymath}
      Since $\tilde x\in \mathcal C_\infty$, which is controlled invariant, we can find $\zeta(k+1)$, such that
      \begin{displaymath}
	\tilde x^c(k+2)=\tilde A \tilde x^c(k+1)+\tilde B\zeta(k+1)\in \mathcal C_{\infty},
      \end{displaymath}
      and
      \begin{displaymath}
	 \|\tilde C\tilde x^c(k+1) +\tilde D \zeta(k+1)\|\leq 1.
      \end{displaymath}
      Since $\tilde x^c(k+2)$ also belongs to $\mathcal C_{\infty}$, we can repeat the procedure above to find $\zeta(k+2)$, $\zeta(k+3)$, \dots. Therefore, $\zeta^\infty = (\zeta(0),\dots,\zeta(k),\zeta(k+1),\dots)$ is the required feasible sequence, which concludes the proof.
\end{proof}

Proposition~\ref{proposition:positiveinvariance} and Theorem~\ref{theorem:recursiver} enable computation of $\mathcal C_k$ and $\mathcal R_k$ by recursively applying the operator $\Pre$ and $\Rch$. However, Computing the exact shapes of these sets is numerically intractable as $k$ goes to infinity. To render the problem tractable, one standard technique is to compute the inner and outer approximation of $\mathcal C_k$ and $\mathcal R_k$, using ellipsoids or polytopes. In this paper, we use an ellipsoidal approximation procedure similar to the one proposed in \cite{Angeli20083113}. The detailed approach is presented in the next subsection.

\subsection{Ellipsoidal Approximation of $R_k$}\label{sec:ellipsoidalapprox}
 This section is devoted to constructing an ellipsoidal inner and outer approximation of $\mathcal C_k$ and $\mathcal R_k$. To this end, let us assume that $\mathcal C_k$ and $\mathcal R_k$ are approximated by the following ellipsoids:
\begin{equation}
  \begin{split}
  \mathcal E_{2n}(C^{in}(k))&\subseteq \mathcal C_k \subseteq \mathcal E_{2n}(C^{out}(k)), \\
  \mathcal E_{2n}(R^{in}(k))&\subseteq \mathcal R_k \subseteq \mathcal E_{2n}(R^{out}(k)),
  \end{split}
\end{equation}
where $C^{in}(k),\, C^{out}(k),\, R^{in}(k),\,R^{out}(k) \in \mathbb S_+^{2n}$ are positive semidefinite, and $\mathcal E_{2n}(S)$ is defined as the following $2n$ dimensional ellipsoid
\begin{displaymath}
  \mathcal E_{2n}(S) \triangleq\{\tilde x\in\mathbb R^{2n}:\tilde x^TS\tilde x\leq 1\}  .
\end{displaymath}

Since $\mathcal C_k$ is defined recursively as 
\begin{displaymath}
  \mathcal C_{k+1} = \Pre(\mathcal C_k),
\end{displaymath}
and $\mathcal R_k$ can be evaluated recursively as
\begin{displaymath}
  \mathcal R_{k+1} = \Rch(\mathcal R_k)\bigcap \mathcal C_\infty, 
\end{displaymath}
we focus on the ellipsoidal inner and outer approximations of set intersection and the operators $\Pre$ and $\Rch$, which are provided by the following theorems:
\begin{theorem}
  \label{theorem:ellipsoidintersec}
  Let $S_1,S_2\in \mathbb R^{2n}$ be positive semidefinite, then the following set incursions hold:
  \begin{equation}
    \mathcal E_{2n} (S_1+S_2) \subseteq \mathcal E_{2n}(S_1)\bigcap \mathcal E_{2n}(S_2)\subseteq \mathcal E_{2n}(S_1/2+S_2/2).
  \end{equation}
\end{theorem}

\begin{theorem}
  Let $S\in \mathbb R^{2n\times 2n}$ be a positive semidefinite matrix. Then the following set incursions hold:
  \begin{equation}
    \mathcal E_{2n}(S^{in}) \subseteq \Pre(\mathcal E_{2n}(S))\subseteq  \mathcal E_{2n}(S^{out}),
  \end{equation}
  where
  \begin{equation}
    \begin{split}
    \label{eq:preinner}
    S^{in} &= \tilde A^TS \tilde A +  \tilde C^T\tilde C\\
    &- (\tilde A^TS\tilde B +  \tilde C^T\tilde D) (\tilde B^TS\tilde B + \tilde D^T\tilde D)^+ (\tilde B^TS \tilde A + \tilde D^T\tilde C)
    \end{split}
  \end{equation}
  and
  \begin{equation}
    \label{eq:preouter}
    S^{out} = S^{in}/2.
  \end{equation}
  \label{theorem:ellipsoidpre}
\end{theorem}
\begin{theorem}
  Let $S\in \mathbb R^{2n\times 2n}$ be a positive semidefinite matrix. Then the following set incursions hold:
  \begin{equation}
    \mathcal E_{2n}(S^{in}) \subseteq \Rch(\mathcal E_{2n}(S))\subseteq  \mathcal E_{2n}(S^{out}),
  \end{equation}
  where
  \begin{equation}
    \begin{split}
    \label{eq:rchinner}
    S^{in} &= \hat A^TS \hat A +  \hat C^T\hat C \\
    &- (\hat A^TS\hat B +  \hat C^T\hat D) (\hat B^TS\hat B + \hat D^T\hat D)^+ (\hat B^TS \hat A + \hat D^T\hat C)
    \end{split}
  \end{equation}
  and
  \begin{equation}
    \label{eq:rchouter}
    S^{out} = S^{in}/2.
  \end{equation}
  The matrices $\hat A\in \mathbb R^{2n\times 2n},\,\hat B\in \mathbb R^{2n\times (q+l+2n)},\,\hat C\in \mathbb R^{m\times 2n},\,\hat D\in \mathbb R^{m\times (q+l+2n)}$ are defined as
  \begin{align}
      \hat A &\triangleq \tilde A^+,&
      \hat B  &\triangleq    \left[{\begin{array}{*{20}c}
	-\tilde A^+\tilde B,& I_{2n} - \tilde A^+\tilde A
      \end{array}}\right]\nonumber ,\\
      \hat C & \triangleq \tilde C\tilde A^+,&
      \hat D &\triangleq \left[{\begin{array}{*{20}c}
	\tilde D-\tilde C\tilde A^+\tilde B,& \tilde C -\tilde C \tilde A^+\tilde A
      \end{array}}\right]. 
  \end{align}
  \label{theorem:ellipsoidrch}
\end{theorem}
\begin{proof}[Proof of Theorem~\ref{theorem:ellipsoidintersec}]
  Since $S_1$ and $S_2$ are positive semidefinite, for all $\tilde x \in \mathbb R^{2n}$, the following inequalities hold:
  \begin{align*}
    \tilde x^T(S_1+S_2)\tilde x \geq \max(\tilde x^TS_1\tilde x,\tilde x^TS_2\tilde x )\geq \tilde x^T(S_1+S_2)\tilde x /2.
  \end{align*}
  Furthermore, it is easy to verify that the following conditions are equivalent:
  \begin{displaymath}
    \tilde x \in  \mathcal E_{2n}(S_1)\bigcap \mathcal E_{2n}(S_2) \Leftrightarrow   \max(\tilde x^TS_1\tilde x,\tilde x^TS_2\tilde x )\leq 1.
  \end{displaymath}
  Hence, 
  \begin{displaymath}
    \begin{split}
     & \tilde x \in \mathcal E_{2n}(S_1+S_2)\Leftrightarrow \tilde x^T(S_1+S_2)\tilde x\leq 1\\
      &\Rightarrow \max(\tilde x^TS_1\tilde x,\tilde x^TS_2\tilde x )\leq 1\in  \mathcal E_{2n}(S_1)\bigcap \mathcal E_{2n}(S_2),
    \end{split}
  \end{displaymath}
  which proves the first set incursion. The second set incursion can be proved by similar arguments.
\end{proof}
\begin{proof}[Proof of Theorem~\ref{theorem:ellipsoidpre}]
  Let us consider the augmented set $\mathcal S_a\subseteq \mathbb R^{2n+q+l}$ of both the state $\tilde x$ and attacker's action $\zeta$:
  \begin{displaymath}
    \mathcal S_a = \left\{ \left[{\begin{array}{*{20}c}
      \tilde x\\
      \zeta
    \end{array}}\right]:\tilde {A}\tilde x+\tilde {B}\zeta\in \mathcal E_{2n}(S),\,\|\tilde{C}\tilde x+\tilde {D}\zeta\|\leq 1\right\}.
  \end{displaymath}
  It is easy to see that $\tilde {A}\tilde x+\tilde {B}\zeta\in \mathcal E_{2n}(S)$ is equivalent to the following inequality:
  \begin{displaymath}
    \left[{\begin{array}{*{20}c}
      \tilde x^T & \zeta^T
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde A^TS\tilde A & \tilde A^TS\tilde B\\
      \tilde B^TS\tilde A & \tilde B^TS\tilde B
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde x \\ \zeta
    \end{array}}\right]\leq 1. 
  \end{displaymath}
  Moreover, $\|\tilde{C}\tilde x+\tilde {D}\zeta\|\leq 1$ is equivalent to
  \begin{displaymath}
    \left[{\begin{array}{*{20}c}
      \tilde x^T & \zeta^T
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde C^T\tilde C & \tilde C^T\tilde D\\
      \tilde D^T\tilde C & \tilde D^T\tilde D
    \end{array}}\right] \left[{\begin{array}{*{20}c}
      \tilde x \\ \zeta
    \end{array}}\right]\leq 1. 
  \end{displaymath}
  Therefore, the augmented set $\mathcal S_a$ is the intersection of the following two $2n+q+l$ dimension ellipsoids:
  \begin{equation}
    \begin{split}
    \label{eq:ellipsoidintersection}
    \mathcal S_a &=  \mathcal E_{2n+q+l}\left(\left[{\begin{array}{*{20}c}
      \tilde A^TS\tilde A & \tilde A^TS\tilde B\\
      \tilde B^TS\tilde A & \tilde B^TS\tilde B
    \end{array}}\right] \right)\\
    &\bigcap \mathcal E_{2n+q+l}\left( \left[{\begin{array}{*{20}c}
      \tilde C^T\tilde C & \tilde C^T\tilde D\\
      \tilde D^T\tilde C & \tilde D^T\tilde D
    \end{array}}\right]\right).
    \end{split}
  \end{equation}
  %The minimum volume ellipsoidal outer approximation of the intersection of two ellipsoid is given by the following theorem, the proof of which is reported in the appendix.

  %\begin{theorem}
  %  The minimum volume ellipsoidal outer approximation of the region
  %  \begin{displaymath}
  %    \{x^T S_1 x \leq 1\} \bigcap \{x^T S_2 x \leq 1\} ,
  %  \end{displaymath}
  %  where $S_1,\,S_2$ are positive semidefinite, is
  %  \begin{displaymath}
  %    \{x^T S_3 x \leq 1\},
  %  \end{displaymath}
  %  where $S_3$ is the solution of the following optimization problem:
  %\begin{align}
  %  \label{eq:optimalouter}
  %  &\mathop{\text{maximize}}\limits_{a,b}&
  %  &\logdet(S_3)\\
  %  &\text{subject to}&
  %  &S_3 = aS_1+bS_2\notag\\
  %  &&   
  %  &a+b=1,\,a,\,b\geq 0\notag
  %\end{align}
  %\label{theorem:optouter}
  %\end{theorem}
  %\begin{remark}
  %  Since $\logdet$ is a concave function on the set of positive semidefinite matrices \cite{Boyd2004}, problem \eqref{eq:optimalouter} is a convex optimization problem and thus can be solved efficiently.
  %\end{remark}
  %Denote $a_i,b_i$ as the solution of \eqref{eq:optimalouter}, where 
  %\begin{displaymath}
  %  S_1 = \left[{\begin{array}{*{20}c}
  %    \tilde A^T\tilde T_i\tilde A & \tilde A^T\tilde T_i\tilde B\\
  %    \tilde B^T\tilde T_i\tilde A & \tilde B^T\tilde T_i\tilde B
  %  \end{array}}\right] ,\,S_2 =  \left[{\begin{array}{*{20}c}
  %    \tilde C^T\tilde C & \tilde C^T\tilde D\\
  %    \tilde D^T\tilde C & \tilde D^T\tilde D
  %  \end{array}}\right].
  %\end{displaymath}
  Thus, by Theorem~\ref{theorem:ellipsoidintersec},
  \begin{displaymath}
   \mathcal E_{2n+q+l}( S_a^{in})\subseteq \mathcal S_a \subseteq\mathcal E_{2n+q+l} ( S_a^{out}),
  \end{displaymath}
   where
  \begin{displaymath}
    \begin{split}
       S^{in}_a &=\left[{\begin{array}{*{20}c}
	\tilde A^TS\tilde A & \tilde A^TS\tilde B\\
	\tilde B^TS\tilde A & \tilde B^TS\tilde B
      \end{array}}\right] + \left[{\begin{array}{*{20}c}
	\tilde C^T\tilde C & \tilde C^T\tilde D\\
	\tilde D^T\tilde C & \tilde D^T\tilde D
      \end{array}}\right],\\
      S^{out}_a&=S^{in}_a/2.
    \end{split}
  \end{displaymath}

  Finally, using the Schur complement, we can project high dimensional ellipsoids $\mathcal S^{in}_a$ and $\mathcal S^{out}_a$ from $\mathbb R^{2n+q+l}$ back to $\mathbb R^{2n}$ to obtain \eqref{eq:preinner} and \eqref{eq:preouter}, which concludes the proof.
\end{proof}

\begin{proof}[Proof of Theorem~\ref{theorem:ellipsoidrch}]
  From the definition of the reach set, we know that for any $\tilde x\in \Rch(\mathcal E_{2n}(S))$, there exist $\tilde x^-\in \mathcal E_{2n}(S)$ and $\zeta$, such that
  \begin{align}
    \label{eq:rcheq}
    &\tilde x = \tilde A \tilde x^- + \tilde B \zeta,\\
    \label{eq:rchineq}
    &\|\tilde C\tilde x^- + \tilde D \zeta\| \leq 1.
  \end{align}
  By the properties of the pseudoinverse, we know that $I_{2n} - \tilde A^+\tilde A$ is a projector onto the kernel of $A$. Thus, \eqref{eq:rcheq} can be written as
  \begin{displaymath}
    \tilde x^- = \tilde A^+ \tilde x - \tilde A^+\tilde B \zeta + (I_{2n} - \tilde A^+\tilde A)\tilde x_0,\\
  \end{displaymath}
  where $\tilde x_0\in\mathbb R^{2n}$ is an arbitrary vector. Since $\tilde x^- \in \mathcal E_{2n}(S)$, we know that
  \begin{equation}
    \tilde A^+ \tilde x - \tilde A^+\tilde B \zeta +(I_{2n} - \tilde A^+\tilde A)\tilde x_0\in \mathcal E_{2n}(S). 
    \label{eq:rkellipsod1}
  \end{equation}
  Furthermore, \eqref{eq:rchineq} can be written as
  \begin{equation}
    \left\|\tilde C\tilde A^+ \tilde x + ( \tilde D- \tilde C\tilde A^+\tilde B) \zeta + (\tilde C  -\tilde C \tilde A^+\tilde A)\tilde x_0\right\|\leq 1 .
    \label{eq:rkellipsod2}
  \end{equation}
  By the same argument as the proof of Theorem~\ref{theorem:ellipsoidrch}, we can obtain \eqref{eq:rchinner} and \eqref{eq:rchouter}.
\end{proof}

We are now ready to describe a recursive algorithm to compute the ellipsoidal approximations $C^{in}(k)$, $C^{out}(k)$, $R^{in}(k)$, $R^{out}(k)$. By Theorem~\ref{theorem:ellipsoidpre}, we know that $C^{in}(k)$, $C^{out}(k)$ can be evaluated recursively as
\begin{equation}
    C^{in}(k+1) = f(C^{in}(k)), C^{out}(k+1) = f(C^{out}(k))/2,
\end{equation}
where $f(S)$ is defined as the following Riccati equation:
\begin{displaymath}
  \begin{split}
 f(S) &\triangleq \tilde A^TS \tilde A +  \tilde C^T\tilde C \\
  &- (\tilde A^TS\tilde B +  \tilde C^T\tilde D) (\tilde B^TS\tilde B + \tilde D^T\tilde D)^+ (\tilde B^TS \tilde A + \tilde D^T\tilde C).
  \end{split}
\end{displaymath}
Let us denote the limit of $C^{in}(k)$ and $C^{out}(k)$ as $C^{in}_\infty$ and $C^{out}_\infty$ respectively. By Theorem~\ref{theorem:ellipsoidintersec} and \ref{theorem:ellipsoidrch}, $R^{in}(k)$ and $R^{out}(k)$ can be computed recursively as
\begin{equation}
  \begin{split}
    R^{in}(k+1) &= h(R^{in}(k)) + C^{in}_\infty,\\
    R^{out}(k+1) &= \left[h(R^{out}(k)) + C^{out}_\infty\right]/2,\\
  \end{split}
\end{equation}
where $h(S)$ is defined as the following Riccati equation:
\begin{displaymath}
  \begin{split}
 h(S) &\triangleq \hat A^TS \hat A +  \hat C^T\hat C \\
  &- (\hat A^TS\hat B +  \hat C^T\hat D) (\hat B^TS\hat B + \hat D^T\hat D)^+ (\hat B^TS \hat A + \hat D^T\hat C).
  \end{split}
\end{displaymath}


