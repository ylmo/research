In order to show the effectiveness of the proposed method we apply our stochastic sensor selection algorithm to a numerical example in which a sensor network is deployed to monitor a diffusion process in a planar closed region, whose model is given by \vspace{-0.1 cm}
\begin{equation}
	\frac{\partial u}{\partial t} = \alpha \left(\frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2}\right)
	\label{eq:diffussionpdfcont}\vspace{-0.2 cm}
\end{equation}
with boundary conditions\vspace{-0.1 cm}
\begin{equation}
  \left.\frac{\partial u}{\partial x_1}\right|_{t,0,x_2}= \left.\frac{\partial u}{\partial x_1}\right|_{t,l,x_2}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,0}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,l}= 0, \vspace{-0.1 cm}
\end{equation}
where  $x_1,x_2$ indicate the coordinates of the region; $u(t,x_1,x_2)$ denotes the temperature at time $t$ at location $(x_1,x_2)$ and $\alpha$ indicates the speed of the diffusion process.

We use the finite difference method to discretize this model and we assume that the planar region is a square with $l$ meters long edges. As a result, a $N \times N$ grid is achieved with grid step $h = l/(N-1)$. We also sample the system in time with frequency of $1$ Hz. Applying the finite difference method, equation \eqref{eq:diffussionpdfcont} becomes
\vspace{-0.3 cm}
\begin{align} \label{eq:diffusiondisc}
	&u(k+1,i,j) - u(k,i,j) = \alpha/h^2 [u(k,i-1,j)+u(k,i,j-1) \nonumber\\[-0.25 cm]
	&+u(k,i+1,j)+u(k,i,j+1)-4u(k,i,j)]
\end{align}
\vspace{-0.4 cm}
where $u(k,i,j)$ denotes the temperature at time $k$, at location $(ih,jh)$.
\begin{remark}
If either $i$ or $j$ is greater than $N$ or lower than $0$, then the location is outside the grid and then $u(k,i,j)$ is replaced by the value of its nearest neighbor in the grid as a consequence of the boundary conditions imposed.
\end{remark}
If we group all temperature values at $k$ in the vector $U_k = [u(k,0,0),\ldots,u(k,0,N-1),u(k,1,0),\ldots,u(k,N-1,N-1)]^T$, we can write the evolution of the discretized system as $U_{k+1} = A U_{k}$, where the $A$ matrix can be computed from \eqref{eq:diffusiondisc}.  If we introduce process noise, $U_k$ will evolve according to \vspace{-0.25 cm}
\begin{equation} \label{sys}
	U_{k+1} = A U_{k} + w_k, \vspace{-0.25 cm}
\end{equation}
where $w_k \in \mathcal N(0,\;Q)$ is the process noise.

We suppose that the fusion center is located in the bottom left corner at position $(0,0)$. We assume that $m$ sensors are distributed in the region and each sensor measures a linear combination of temperature of the grid around it\footnote{We do not require the sensors to be placed at grid points}. In particular, if we suppose the location of sensor $l$ of coordinates  $(a_1,a_2)$ is in the cell $[i,j]$, i.e. $a_1 \in [i,i+1)$ and $a_2 \in [j,j+1)$, the measurement of this sensor is
\begin{equation}
	\begin{split}
	 y_{k,l} = \left[\right.(1-\Delta a_1)(1-\Delta a_2) &u(k,i,j)+  \Delta a_1(1-\Delta a_2) u(k,i+1,j)+\\
	 (1-\Delta a_1)\Delta a_2 &u(k,i,j+1)+  \Delta a_1\Delta a_2 u(k,i+1,j+1)\left.\right]/h^2+ v_{k,l}  .
	\end{split}
	\label{eq:diffusionmeasure}
\end{equation}
where $\Delta a_1 =a_1-i$, $\Delta a_2 =a_2-j$ and $v_{k,l}$ is the measurement noise of sensor $l$ at time $k$. Indicating with $Y_k$ the vector of all the measurements at time $k$, it follows that: \vspace{-0.25 cm}
\begin{equation}\label{sensors}
Y_k =  C U_k + v_k, \vspace{-0.25 cm}
\end{equation}
where $v_k$ denotes the measurement noise at time $k$ assumed to have normal distribution $\mathcal N(0,\;R)$ and $C $ is the observation matrix. Finally, we assume that the sensor network is fully connected and the communication cost from sensor $i$ to $j$ is \vspace{-0.25 cm}
\[
cost(e_{i,j}) = c+d_{i,j}^2 \vspace{-0.25 cm}
\]
where $d_{ij}$ is the Euclidean distance from sensor $i$ to sensor $j$ and $c$ is a constant related to the sensing energy consumption\footnote{$c$ models the fact that as the distance goes to zero the communication cost does not}. For the simulations, we impose the following parameters:
\vspace{-0.15 cm}
\begin{itemize}
\item $l=3\; m$ and $N=4 \Rightarrow$ grid size $h = 1 \;m$. \vspace{-0.15 cm}
  \item $m = 16 = N^2$, $\alpha = 0.1 \; m^2/s$. \vspace{-0.15 cm}
\item $Q = I \in \mathbb R^{16\times 16}$, $R = I \in \mathbb R^{16\times16}$, $\Sigma = 4I \in \mathbb R^{16\times 16}$. \vspace{-0.15 cm}
\item $\mathcal E_d = 6$,$c=1$. \vspace{-0.15 cm}
\end{itemize}
\noindent

For the first simulation, we assume the sensors are put on the grid. We compute the minimum spanning tree of the graph and force sensors to only communicate with its parent in the minimum spanning tree. We compare the performance of the deterministic greedy algorithm with the stochastic algorithm proposed in Section~\ref{sec:main}. Figure~\ref{fig:topology} shows the topology of the network and Figure~\ref{fig:evo} shows the evolution of the traces of the upper-bound, lower-bound and expected error covariance. The expected error covariance is computed by averaging $1000$ sample paths. It is easy to see that the expected trace always lies between the traces of the upper and lower bound, which shows that our formulation is correct. Figure~\ref{fig:evo2} shows the trace of $P_k$ for the optimal solution of Problem~\ref{opt_dyn}, which is found by exhaustive search, together with the trace of $P_k$ from a sample path of the stochastic schedule and the $EP_k$ of the stochastic schedule. The optimal deterministic schedule is worse than $EP_k$, which shows that the stochastic formulation is better than the deterministic formulation in the expected sense, as discussed in Remark~\ref{remark:stochasticvsdeterministic}. Figure~\ref{fig:energy} illustrates the energy consumption of deterministic schedule, one sample path of the stochastic schedule and expected energy consumption of the stochastic schedule computed by averaging $1000$ sample paths. Also the average energy consumption over the time of that specific sample path we are showing is $6.4$. It is worth noticing that the computation time of Problem~\ref{randoptlowerfinal} is 0.9 seconds on Intel Core2 2GHz CPU using Matlab R2006b and CVX 1.2, while the exhaustive search takes 15 seconds to be completed. We want to mention that a network of $16$ sensors in not a large network, and since the complexity of exhaustive search grows exponentially, it will be intractable for a moderately large network. One the other hand, the complexity of the proposed algorithm is $O(m^3)$ as discussed in Section~\ref{sec:main}.

For the next simulation, we still assume that the topology showed in Fig~\ref{fig:topology} and we compare the performance of the fixed stochastic schedule found in Section~\ref{sec:asymptotic}, with the performance of a fixed deterministic scheduler. Figure~\ref{fig:fixed} shows the trace of $P_k$ for the optimal deterministic fixed schedule, which is found by exhaustive search, together with the trace of $P_k$ from a sample path of the stochastic fixed schedule and the $EP_k$ of the stochastic fixed schedule.% Figure~\ref{fig:ergodic} shows the histogram of average trace over the time. The blue dashed line is the expected asymptotic trace of estimation error from stochastic schedule. The black dot line is the asymptotic trace of estimation error from deterministic schedule.
It is easy to see that the deterministic one performs even worse, since it is forced to choose the same set of sensors over time.
%Moreover, we can see that the average trace over time is very close to the expected asymptotic trace, which is a consequence of Theorem~\ref{theorem:ergodic}.

Next we assume that the sensors are randomly distributed in the region and the topology is given by the minimum spanning tree. We compare the performance of the optimal fixed stochastic schedule with optimal fixed deterministic schedule. Figure~\ref{fig:fixed} shows the histogram of the ratio between $trace(P_\infty)$ of deterministic schedule and $trace(EP_\infty)$ of stochastic schedule, which is generated by 100 random experiments. The blue dashed line is the average ratio. It can be seen that the deterministic schedule is always worse than the stochastic one. 

Finally we test our algorithm in fully connected graph for randomly placed sensors. Fig~\ref{fig:generalgraph} shows the average trace of the optimal solution for Problem~\ref{generalgraphProb}, normalized by the trace of the lower bound given by Problem~\ref{generalgraphProblower}, versus the number of representing trees used. The average is calculated by averaging 100 random sensor placements. It can be seen that the gap between our solution and the lower bound is within 8\% after using 10 spanning trees. It is also worth noticing that the total number of spanning trees in this case is $10^8$, given by Cayley's Formula.

\begin{figure}[<+htpb+>]
  \centering
  \begin{minipage}[t]{0.45\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure2.eps}
      \caption{Network Topology}
      \label{fig:topology}
    \end{center}
  \end{minipage}
  \begin{minipage}[t]{0.45\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure1.eps}
      \caption{Evolution of the Trace of $U_k$, $L_k$ and $EP_k$}
      \label{fig:evo}
    \end{center}
  \end{minipage}
\end{figure}

\begin{figure}[<+htpb+>]
  \centering
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure3.eps}
      \caption{Evolution of the Trace of $P_k$ Given by Optimal Deterministic Schedule for Problem~\ref{opt_dyn}, Sample $P_k$ for Stochastic Schedule, $EP_k$ for Stochastic Schedule}
      \label{fig:evo2}
    \end{center}
  \end{minipage}
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure4.eps}
      \caption{Energy Consumption of Deterministic Schedule, Sample Energy Consumption of Stochastic Schedule and Expected Energy Consumption of Stochastic Schedule}
      \label{fig:energy}
    \end{center}
  \end{minipage}
\end{figure}

\begin{figure}[<+htpb+>]
  \centering
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure5.eps}
      \caption{Evolution of the Trace of $P_k$ Given by Optimal Deterministic Fixed Schedule, Sample $P_k$ for Stochastic Fixed Schedule, $EP_k$ for Stochastic Fixed Schedule}
      \label{fig:fixed}
    \end{center}
  \end{minipage}
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure8.eps}
      \caption{Histogram of the Ratio between $trace(P_\infty)$ of deterministic schedule and $trace(EP_\infty)$ of stochastic schedule}
      \label{fig:ergodic}
    \end{center}
  \end{minipage}
\end{figure}

\begin{figure}[<+htpb+>]
  \begin{center}
    \includegraphics[width=0.4\textwidth]{figure9.eps}
    \caption{The Trace of the Optimal Solution for Problem~\ref{generalgraphProb}, Normalized by the Trace of the Lower Bound, versus Number of Representing Spanning Trees used}
    \label{fig:generalgraph}
  \end{center}
\end{figure}


