\subsection{System Description}
%
Consider the following discrete-time LTI system
   \vspace{-0.25 cm}
\begin{equation}
    x_{k+1} = Ax_k + w_k
  \label{eq:systemdescription}    \vspace{-0.25 cm}
\end{equation}
where  $x_k \in \mathbb R^n$ represents the state and $w_k\in \mathbb R^n$ the disturbance. It is assumed that $w_k$ and $x_0$ are independent Gaussian random vectors, $x_0 \sim \mathcal N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$, where $\Sigma,\,Q > 0$ are positive definite matrices. A wireless sensor network composed of $m$ sensing devices $s_1,\ldots,s_m$ and one fusion center $s_0$ is used to monitor the state of system \eqref{eq:systemdescription}. The measurement equation is
   \vspace{-0.25 cm}
\begin{equation}
    y_{k} = C x_k + v_k,
  \label{eq:sensordescription}    \vspace{-0.25 cm}
\end{equation}
where $y_k = [y_{k,1}' , y_{k,2}' ,\ldots, y_{k,m}']' \in \mathbb R^m$ is the measurement vector\footnote{The $'$ on a matrix always means transpose.}. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$. $C = \left[C_1',\ldots,C_m'\right]'$ is the observation matrix and the matrix pair $(C,\,A)$ is assumed observable\footnote{The assumption of observability is without loss of generality since we could perform Kalman decomposition and only consider the observable space even if the system is not observable.}. $v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed to be independent of $x_0$ and $w_k$. We also assume that the covariance matrix $R = diag(r_1,\ldots,r_m) $ is diagonal, which means that the measurement noise at each sensor is independent of all others and nonsingular, that is $r_i > 0, i=1,...,m$.

Let's introduce an oriented communication graph $G = \{V\,,E\}$ in order to model the communication amongst nodes, where the vertex set $V = \{s_0,\,s_1,\ldots,s_m\}$ contains all sensor nodes, including the fusion center. The set of edges $E \subseteq V \times V$ represents the available connections, i.e.  $(i,j) \in E$ implies that the node $s_i$ may send information to the node $s_j.$ Moreover, it is assumed that each node of the sensor network acts as a gateway for a specific number of other nodes, which means that every time it communicates with another node it sends, in a single packet, its own measurements collected together with all data received from the other nodes.

Until Sections \ref{sec:generalgraph} we will always assume that, for every sensor in the network, there exists one and only one communication path to the fusion center, i.e. the sensor network has a directed tree topology. Moreover, we assume that each link has an associated weight $c(e_{i,j})$ which indicates the energy consumed when $s_i$ directly transmits a packet to $s_j$. For the sake of legibility, we will sometimes abbreviate $c(e_{i,j})$ as $c_i$, $i= 1,\ldots,m$ because, in the assumed topology, each sensor node has only one outgoing edge.
   \vspace{-0.4 cm}
\begin{remark}
  The tree topology assumption may be a restrictive hypothesis in the general case where usually one sensor can communicate with several nearby nodes. However, it is worth to remark that typical communication network graphs can be approximated by a collection of ``representative" spanning trees (e.g. the first $m$ spanning trees of the spanning tree enumeration \cite{EnumerateSpanningTrees}). Details will be discussed in Section~\ref{sec:generalgraph}.
\end{remark}
   \vspace{-0.6 cm}
\subsection{The Goal: Optimal Sensor Selection \& Packet Routing}    \vspace{-0.15 cm}
%
Because sensor measurements usually contain redundant information, in order to reduce the energy consumption it would be highly desirable to use a minimal subset of sensors at each sampling time. However, in a tree topology, we cannot select arbitrary subsets of nodes but we are forced to select nodes (and connections) such that, for each selected node, there exists a communication path to the fusion node. As a result, any possible transmission topology of $G$ will be a subtree $T = \{V_T\,,E_T\}$, with $s_0 \in V_T$,  $V_T\subseteq V$ and $E_T\subseteq E$. Hereafter, $V_T$ will denote the selected subset of sensors and $E_T$ the communication paths used by the sensors to transmit observations to the fusion center. We also denote by $\mathcal T$ the set of all possible transmission topologies $T$ (i.e. the set of all possible subtrees of $G$ containing $s_0$).

It is straightforward to show that, for a transmission tree $T$, the total transmission energy consumption
is given by\footnote{Here we assume that $cost(e_{i,j})$ is constant regardless of number of observations contained in the packet. This is realistic in most of the cases, especially when measurements are of simple type, such as low precision scalar values, and the transmission overhead, e.g. header, handshaking protocol, dominates the payload.}
\begin{displaymath}
  \mathcal E(T) = \sum_{e \in E_T} c(e).    \vspace{-0.15 cm}
\end{displaymath}

Suppose now that at time $k$, a particular transmission subtree $T_k$ is used with  $V_{T_k} = \{s_0,\,s_{i_1},\ldots,s_{i_j}\}$ and let us define two matrices $C_{T_k}$ and $R_{T_k}$ to be
\begin{eqnarray}
  C_{T_k} & \triangleq & [C_{i_1}',\,C_{i_2}',\ldots,C_{i_j}']',  R_{T_k}  \triangleq   diag(r_{i_1},\ldots,r_{i_j}).
\end{eqnarray}
It is well known (see \cite{Gupta_sensorselection_automatica}) that the Kalman filter with a time varying observation matrix $C_{T_k}$ is still the optimal filter in the case of sensor selection. Hence, once the fusion center collects all the observations at time $k$, it will use the Kalman filter to compute the optimal estimate of current state $\hat x_{k}$ and the estimation error covariance matrix $P_{k}$, which satisfy the following equations
\vspace{-0.25 cm}
\begin{align}\label{eq:Kalman_filter}
  \hat x_{k} &= K_k (y_k -C_{T_k}\hat x_{k|k-1}) + \hat x_{k|k-1}, \\[-0.2 cm]
  P_{k}      &= P_{k|k-1}  -  K_k C_{T_k}P_{k|k-1}, \\[-0.2 cm]
%\end{align}
%\vspace{-0.30 cm}
%where
%\vspace{-0.25 cm}
%\begin{align}
    \hat x_{k|k-1} &= A \hat x_{k-1}, \\[-0.2 cm]
    P_{k|k-1}  &= A P_{k-1} A'  + Q , \\[-0.2 cm]
    K_k        &= P_{k|k-1} C_{T_k}' (C_{T_k}P_{k|k-1} C_{T_k}'  + R_{T_k})^{ - 1}.
\end{align}
\vspace{-0.25 cm}
\noindent Next, consider the information form of the Kalman filter, which will be of use in the next section. To this end, let $Z_k$ be the information matrix at time $k$, defined as
\vspace{-0.25 cm}
\begin{equation}
  Z_k \triangleq P_k^{-1},
  \label{eq:defofinformationmatrix} \vspace{-0.25 cm}
\end{equation}
which satisfies the following recursive equation
\vspace{-0.25 cm}
\begin{equation}
  Z_k = Z_{k|k-1} + C_{T_k}'R_{T_k}^{-1}C_{T_k},
  \label{eq:informationfilter} \vspace{-0.25 cm}
\end{equation}
with \vspace{-0.25 cm}
\begin{equation}
  Z_{k|k-1} =   (AZ_{k-1}^{-1}A'+Q)^{-1}. \vspace{-0.25 cm}
\end{equation}
Because $R$ has been assumed diagonal, it is easy to verify that \eqref{eq:informationfilter} can be rewritten as
\vspace{-0.25 cm}
\begin{equation}
  Z_k = Z_{k|k-1} + \sum_{s_i \in V_{T_k},\, s_i\neq s_0} \frac{C_iC_i'}{r_i} \vspace{-0.2 cm}
\end{equation}
%
In order to simplify the notation, let us define the following function $g_T(X)$
\begin{equation}
  g_T(X) \triangleq \left[(AXA'+Q)^{-1} + \sum_{s_i \in V_{T},\, s_i\neq s_0} \frac{C_iC_i'}{r_i}\right]^{-1},
\end{equation}
where $X\in \mathbb R^{n\times n}$ is positive semidefinite, with $T\in \mathcal T$. Then, it is easy to show that
\begin{displaymath}
  P_k = g_{T_k}(P_{k-1}).
\end{displaymath}
Ideally, our goal is to compute a long schedule $T_1,\ldots,T_N$ that solves the following $N$-step sensor selection problem:
\vspace{-0.2 cm}
\begin{prob}[$N$-step Sensor Selection over Transmission Trees]\label{optdynnstep}
Determine a sequence of transmission trees $T_1,\ldots,T_N$ that minimizes the average trace of the estimation error covariance matrix under
the constraint that the average transmission cost is lower than the energy budget $\mathcal E_d$, i.e.
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{T_1,\ldots,T_N}&
  &trace\sum_{k=1}^N P_k/N \\[-0.3 cm]
  &\textrm{subject to}&
 &\sum_{k=1}^N\mathcal E(T_k)/N\leq \mathcal E_d, \;
\end{align*}
\end{prob}
\begin{remark}
  In Problem~\ref{optdynnstep} we require that the average energy consumption from steps $1$ through $N$ does not exceed a certain energy budget. In real applications different constraints may be considered (e.g. requirements on the sensor lifetime). However, it can be shown (see e.g. \cite{Joshi}) that many of these constraints can be easily integrated into the above framework.
\end{remark}
\vspace{-0.3 cm}
It can be proved that the above problem is an integer programming problem and the size of the solution space is $|\mathcal T|^N$, which grows exponentially with $N$. As a result, the above problem is intractable even for moderately large networks and horizons $N$. For this reason, a greedy approach has been often used and studied in the literature (see e.g. \cite{oshmansensor} and \cite{Mo_allerton09}). Such an approach can be formulated as follows:
%
\begin{enumerate}
  \item Let $k = 1$ and $P_{k-1} = P_0 = \Sigma$.% \vspace{-0.15 cm}
  \item Determine the transmission tree $T_k$ that minimizes the trace of the one-step estimation error covariance matrix $P_k$ such that the transmission
  cost at time $k$ is lower than the average energy budget. This can be determined by solving the following problem:
    \begin{prob}[One-Step Sensor Selection over Transmission Trees]\label{opt_dyn}
\vspace{-0.1 cm}
      \begin{align*}
  &\mathop{\textrm{minimize}}\limits_{T_k\in \mathcal T}&
	&trace(P_k) \left(= trace(g_{T_k}(P_{k-1}))\right) \\%[-0.5 cm]
  &\textrm{subject to}&
	&   \mathcal E(T_k)\leq\mathcal E_d. \; \vspace{-0.3 cm}
      \end{align*}
    \end{prob} \vspace{-0.15 cm}
  \item Let $k \Leftarrow k+1$.\vspace{-0.15 cm}
  \item Repeat Step 2 and 3 until $k = N+1$.
\end{enumerate}

The schedule of the greedy algorithm may change over time, which could bring certain implementation issues in some applications. For example, the sensors may need to store a long sequence of the schedule, which may cost a lot in terms of memory requirements. As a result, in some cases it is more desirable to use a fixed schedule over the time. To this end, let us consider the following function recursions
\begin{equation}
  g_{T}^{(1)}(X) \triangleq g_T(X),\, g_T^{(k)}(X) \triangleq g_T(g_T^{(k-1)}(X)),
\end{equation}
From the classical theory of Kalman filters, we know that if the system $(C_T,\,A)$ is detectable, then $g_T^{(k)}(X)$ will converge as $k\to\infty$. Otherwise, $g_T^{(k)}(X)$ will be unbounded. Hence, denote with
\begin{equation}
  g_T^{\infty}(X) = \lim_{k\rightarrow\infty}g_T^{(k)}(X),
\end{equation}
such a limit when it exists, with $g_T^{\infty}(X) = \infty$ when it does not exist. It is easy to see that if a constant transmission tree $T$ is always used then
\begin{displaymath}
  \lim_{N\rightarrow\infty} \sum_{k=1}^N P_k/N = \lim_{N\rightarrow\infty} \sum_{k=1}^N g_T^{(k)}(\Sigma)/N = g_T^\infty(\Sigma).
\end{displaymath}

As a result, in order to find a fixed schedule $T$ that minimizes Problem~\ref{optdynnstep} we can solve the following problem:
\begin{prob}[Fixed Deterministic Schedule that Optimizes Asymptotic Performance]\label{detoptasymptotic}
  \vspace{-0.1 cm}
  \begin{align*}
  &\mathop{\textrm{minimize}}\limits_{T\in\mathcal T}&
   &trace(g_T^\infty(\Sigma))\\%[-0.5 cm]
  &\textrm{subject to}&
    & \mathcal E(T)\leq\mathcal E_d. \; \vspace{-0.3 cm}
  \end{align*}
\end{prob}

%It is worth noticing that the formulation of one step sensor selection in Problem~\ref{opt_dyn} and \ref{detoptasymptotic} does not involve any loss of generality w.r.t. Problem~\ref{optdynnstep}. In fact, if a horizon of length $l$ is of interest, one can always extend the state space so that $\tilde x_k = [x_k',\,\ldots,\,x_{k+l-1}']'$. Then, an $l$-step sensor selection for the original system can be shown to be equivalent to an one-step sensor selection for the extended system.

It is worth noticing that in both approaches, the total energy is equally divided and distributed over each sampling period. This fact follows from the intuition that no one single step is significantly more important than the others. This is important to be remarked in order to understand why stochastic sensor selections, being allowed to use more energy at one single sampling period, can achieve better performance than the above deterministic formulation.
%
Finally note that, even if the horizon is reduced from $N$ to $1$, the numerical burden required to solve Problem~\ref{opt_dyn} and \ref{detoptasymptotic} with an exhaustive search over the space $\mathcal T$ may be still huge even for a small sensor network. In order to give an idea, consider the case of a small sensor network $G$ organized as a perfect binary tree with $63$ nodes. Then, the number of candidate solutions, i.e. all possible transmission subtrees $T$, is of order $10^{11}$.
%
In order to address the impracticality of exhaustive searches and to improve the performance of the deterministic formulation, we propose the novel stochastic selection strategy that will be discussed in the next section.
%
