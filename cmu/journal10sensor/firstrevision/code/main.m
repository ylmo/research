%clc
%clear
rand('twister',sum(clock)*100)
for expe = 1:100
    expe
    %generate network graph
    gridlength = 4;
    n = gridlength^2 ;
    m = 16;
    xaxis = (gridlength-1) * rand(1,m) + 1;
    yaxis = (gridlength-1) * rand(1,m) + 1;
    xaxis(1) = 1;
    yaxis(1) = 1;
    graph = zeros(m);
    for i = 1:m
        for j = 1:m
            graph(i,j) = (xaxis(i)-xaxis(j))^2 + (yaxis(i)-yaxis(j))^2;
        end
    end
    baseenergy = 1;
    graph = graph + baseenergy;

    %generate A matrix for heat transfer system
    coef = 0.1;
    A = eye(n);
    for i = 1:gridlength
        for j = 1:gridlength
            k = (i-1)*gridlength + j;
            if i >= 2
                l = (i-2)*gridlength + j;
                A(k,k) = A(k,k) - coef;
                A(k,l) = coef;
            end

            if i <= gridlength - 1
                l = i * gridlength + j;
                A(k,k) = A(k,k) - coef;
                A(k,l) = coef;
            end

            if j >= 2
                l = (i-1)*gridlength + j - 1;
                A(k,k) = A(k,k) - coef;
                A(k,l) = coef;
            end

            if j <= gridlength - 1
                l = (i-1)*gridlength + j + 1;
                A(k,k) = A(k,k) - coef;
                A(k,l) = coef;
            end
        end
    end

    %generate C matrix
    C = zeros(m,n);
    for i = 1:m
        x = xaxis(i);
        y = yaxis(i);
        xgrid = floor(x);
        ygrid = floor(y);
        for j = 0:1
            for k = 0:1
                l = (xgrid+j-1) * gridlength + ygrid + k;
                C(i,l) = (1-abs(xgrid+j-x))*(1-abs(ygrid+k-y));
            end
        end
    end

    %generate noise
    Q = eye(n);
    R = eye(m);
    Sigma =4*eye(n);

    %generate graph

    MST = graphminspantree(sparse(graph));

    [dist path parent] = graphshortestpath(MST+MST',1,'directed',false);

    cost = zeros(1,m);
    for i = 2:m
        cost(i) = graph(i,parent(i));
    end

    %optimization
    threshold = 6;
    L = Sigma;
    for k = 1:10
        Ltmp = A*L*A'+Q;
        Ztmp = Ltmp^(-1);
        cvx_begin 
        cvx_solver sdpt3
        cvx_quiet(true)
        variable p(1,m);
        variable Z(n,n) symmetric;
        minimize (trace_inv(Z));
        subject to
        Z == semidefinite(n);
        Z == Ztmp + C'*diag(p)/R*C;
   %     Z - inv(L) == semidefinite(n);
        p(1) >= 0;
        p(1) <= 1;
        for i = 2:m,
            p(i) >= 0;
            p(i) <= 1;
            p(i) <= p(parent(i));
        end
        p*cost' <= threshold;
        cvx_end
        L = inv(Z);
    end

    %simulation
    P = Sigma;
    horizon = 200;
    sampletrace = zeros(1,horizon);
    for k = 1:horizon
        Ptmp = A*P*A' + Q;
        Ztmp = inv(Ptmp);
        alpha = rand();
        gamma = (alpha <= p);
        Ztmp = Ztmp + C'*diag(gamma)/R*C;
        P = inv(Ztmp);
        sampletrace(k) = trace(P);
    end

    asymsto = mean(sampletrace([10:horizon]));

    %find all enumeration
    j = 1;
    enu = {};
    for i = 1:2^15-1;
        index = 1;
        tmpi = i;
        for k = 2:16
            if mod(tmpi,2) == 1
                index = [index k];
                tmpi = (tmpi-1)/2;
            else
                tmpi = tmpi/2;
            end
        end
        subgraph = MST(index,index);
        subgraph = subgraph + subgraph';

        if graphconncomp(subgraph,'DIRECTED',false) > 1
            continue;
        end
        tmpcost = sum(sum(graphminspantree(subgraph)));
        if tmpcost <= threshold
            enu{j} = index;
            j = j+1;
        end
    end

    %deterministic optimal

    opttrace = inf;

    for index = enu
        P = Sigma;
        gamma = zeros(1,16);
        gamma(index{1}) = 1;
        for k = 1:20
            Ptmp = A*P*A' + Q;
            Ztmp = inv(Ptmp);
            Ztmp = Ztmp + C'*diag(gamma)/R*C;
            P = inv(Ztmp);
        end
        if trace(P) < opttrace
            opttrace = trace(P);
        end
    end
    ratio(expe)=opttrace/asymsto
end
