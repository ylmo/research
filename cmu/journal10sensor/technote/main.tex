A lower-bound $L_k$ to $\mathbb EP_k$ is first derived by means of the following theorem, whose proof is reported in a technical report~\cite{mo2011}.
\begin{theorem}\label{theorem:lowerbound}
  Let $L_0 =  P_0$ and
  \begin{equation}
	  L_k = \left(L_{k|k-1} ^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_iC_i'}{r_i}\right)^{-1},
	  \label{eq:lowerbound1}
  \end{equation}
  where $L_{k|k-1} = AL_{k-1}A' + Q.$ The following inequalities hold:
  \begin{equation}
    \mathbb EP_k \geq L_k.
  \end{equation}
\end{theorem}

\noindent To further improve the legibility, let us define the function
\begin{equation}
  L(X,\mathbf p) \triangleq \left[(AXA' + Q)^{-1} + \sum_{i=1}^m p_{i} \frac{C_iC_i'}{r_i}\right]^{-1},
\end{equation}
where $X \in \mathbb R^{n\times n}$ is positive semidefinite and $\mathbf p = [p_1,\ldots,p_m]'\in \mathbb R^m$. Moreover, let us define,
\begin{equation}
 L^{(1)}(X,\mathbf p) = L(X,\mathbf p), L^{(k)} (X,\mathbf p) = L(L^{(k-1)}(X,\mathbf p),\mathbf p),
\end{equation}
with
\begin{equation}
  L^{\infty}(X,\mathbf p) = \lim_{k\rightarrow\infty} L^{(k)}(X,\mathbf p),
  \label{eq:asymptoticlowerbound}
\end{equation}
when the limit exists. Hence \eqref{eq:lowerbound1} can be simplified as
\begin{equation}
  L_k = L(L_{k-1}, \mathbf p_k).
  \label{eq:lowerbound3}
\end{equation}
By replacing the objective function in Problem~\ref{randoptasymptotic} with its lower bound, we obtain
\begin{prob}[Asymptotic Lower Bound for Random Transmission Tree Selection]\label{randoptasymptoticlower}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\pi_{T},\,\mathbf p}&
  &trace(L^\infty(\Sigma,\mathbf p)) \\
  &\textrm{subject to}&
  &\sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d, \\
  &&&\pi_{T}\geq 0,\,\sum_{T\in\mathcal T} \pi_{T} = 1,\,p_{i} = \sum_{s_i \in V_T} \pi_{T}.
\end{align*}
\label{randoptlower}
\end{prob}
There are two drawbacks in the above formulation: 1) the optimization problem still has the $\pi_T$ terms as optimization variables, the number of which grows more than polynomially with respect to $m$ in general; 2) $L^\infty$ is still not an explicit function of the optimization variables.

Let us first drop the dependence on $\pi_{T}$. To this end, define the set of feasible $\mathbf p$ for Problem \ref{randoptlower}:
\begin{displaymath}
  \begin{split}
  \mathcal P \triangleq & \left\{\mathbf p\in \mathbb R^m\left|\exists \pi,\, \sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d,\right.\right. \\
  &\left.\pi_{T}\geq 0,\,\sum_{T\in\mathcal T} \pi_{T} = 1,\, p_{i} = \sum_{s_i \in V_T} \pi_{T}\right\} .
  \end{split}
\end{displaymath}
%
The following results can be easily proved:

\begin{proposition}
  The energy cost of a given collection of tree selection probabilities $\pi_{k,T}, \forall T \in {\mathcal{T}}$ is a linear function of the resulting marginal probability:
  \begin{equation}
    \sum_{T \in \mathcal T} \pi_{T} \mathcal E(T)= \sum_{i = 1}^m c_i p_{i}.
  \end{equation}
  \label{proposition:energy}
\end{proposition}

\begin{proposition}
  \label{proposition:existence}
  If $p_{i}\in [0,1]$ and if it satisfies
  \begin{equation}
    p_{i} \leq  p_{j},\qquad \textrm{if $j$ is a parent of $i$}
  \label{eq:descritpion1}
  \end{equation}
  then there exists at least one collection of tree selection probabilities $\pi$, such that
  \begin{equation}
  \pi_{T}\geq 0,\; \sum_{T\in\mathcal T} \pi_{T} = 1,\; p_{i} = \sum_{s_i \in V_T} \pi_{T}.
  \label{eq:descritpion2}
  \end{equation}
  Conversely, if there exists  $\pi_k$ such that \eqref{eq:descritpion2} holds, then  $p_{k,i}\in [0,1]$ and satisfies \eqref{eq:descritpion1} .
\end{proposition}

\noindent By exploiting the above Propositions we can reformulate the feasible set $\mathcal P$ as follows
\begin{equation}
  \mathcal P =\left\{\mathbf p\left|p_i\in[0,\,1],\,\sum_{i = 1}^m c_i p_{i}\leq \mathcal E_d,\,  p_{i} \leq  p_{j},\textrm{if $j$ is parent of $i$}\right. \right\},
  \label{eq:treecondition}
\end{equation}
and we can rewrite Problem~\ref{randoptasymptoticlower} as
\begin{prob}[Asymptotic Lower Bound for Random Transmission Tree Selection]\label{randoptasymptoticlowerfinal}
\begin{align*}
  &\mathop{\textrm{mininize}}\limits_{\mathbf p \in \mathbb R^m}&
  &trace(L^\infty(\Sigma,\mathbf p)) \\
  &\textrm{subject to}&
  &\mathbf p \in \mathcal P.
\end{align*}
\end{prob}
The main difficulty in solving the above problem is that $L^\infty(X,\mathbf p)$ is in general not convex in $\mathbf p$ and its exact form is unknown. Here we propose the use of the following heuristic:
\begin{enumerate}
  \item Define
     $ \mathbf p_0 =  \left({\mathcal E_d}/{\left(\sum_{i=1}^m c_i\right)}\right) \mathbf 1_m, $
    where $\mathbf 1_m \in \mathbb R^m$ is a vector with '1' in all entries and choose the matrix
    $  L_0 = L^{\infty}(I_n,\mathbf p_0).  $
  \item Let $L_k$ and $\mathbf p_k$ be the solution of the following optimization problem
\begin{prob}[Random Sensor Selection with Descent Constraint]\label{greedylower}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_k \in \mathbb R^m}&
  &trace(L_k)(=trace(L(L_{k-1},\mathbf p_k)))\\
  &\textrm{subject to}&
  & L_k \leq L_{k-1},\,\mathbf p_k \in \mathcal P.
\end{align*}
\end{prob}
  \item Choose $\mathbf p^*$ as an accumulation point of $\mathbf p_k$\footnote{An accumulation point of a sequence is the limit of a converging subsequence}. Then $L^{\infty}(X, \mathbf p^*) = \lim_{k\rightarrow \infty} L_k$ for any $X\geq 0$.
\end{enumerate}

Before proving the feasibility of the above algorithm, we want to point out that our algorithm is greedy. In fact, we try to minimize the lower bound for the next step in the hope of reducing the final asymptotic lower bound. As a result, it is suboptimal by nature. The following theorem~\cite{mo2011} gives a characterization of the main features of the proposed algorithm.

\begin{theorem}
The following statements are true for the proposed algorithm:
\begin{enumerate}
  \item $L_0$ exists.
  \item $L(X,\mathbf p)$ is convex with respect to $\mathbf p$ and it is concave and monotonically increasing with respect to $X$.
  \item Problem~\ref{greedylower} is always feasible.
  \item $\mathbf p^*$ exists and $\mathbf p^*\in\mathcal P$.
  \item $L_\infty = \lim_{k\rightarrow \infty}L_k$ exists.
  \item $L_\infty = L^{\infty}(X,\mathbf p^*)$ for all positive semidefinite $X$.
\end{enumerate}
\label{theorem:algorithm}
\end{theorem}

\begin{proof}
  \begin{enumerate}
    \item The proof is reported in \cite{mo2011}.
    \item The proof is reported in \cite{mo2011}.
    \item Suppose that the Problem~\ref{greedylower} is feasible up to time $k$. To prove the problem is also feasible at time $k+1$, we only need to find one $\mathbf p\in \mathcal P$ and $L(L_k,\mathbf p)\leq L_k$. If we choose $\mathbf p = \mathbf p_k$ then, since $\mathbf p_k$ is the solution at time $k$, it follows that $\mathbf p_k\in\mathcal P$. It remains to show that $L(L_k,\mathbf p_k)\leq L_k$, which can be proved by noticing that $L_k = L(L_{k-1},\mathbf p_k)  \leq L_{k-1}$ and $L(X,\mathbf p)$ is monotonically increasing with respect to $X$.
      Similarly, Problem~\ref{greedylower} is also feasible at time $1$ and then, by induction, Problem~\ref{greedylower} is always feasible.
\item It is easy to see that $\mathbf p_k$ is bounded because $p_{k,i}\in [0,1]$. By means of the Bolzano-Weierstrass Theorem, this implies that there always exists an accumulation point $\mathbf p^*$. Moreover, since $\mathbf p_k\in\mathcal P$ and $\mathcal P$ is closed, $\mathbf p^*\in \mathcal P$.
\item The limit must exist because $\{L_k\}$ is decreasing and $L_k \geq 0$ for all $k$, 
\item The proof is reported in \cite{mo2011}.
  \end{enumerate}
\end{proof}
\begin{remark}
  Due to the convexity of $L$ and $\mathcal P$, Problem~\ref{greedylower} is a convex optimization problem with $O(m)$ optimization variables and $O(m)$ constraints. Thus, it can be solved efficiently. For example, if interior-points methods are used, then the complexity is $O(m^3)$~\cite{BOYD:Convex}.
\end{remark}
\begin{remark}
	It is worth noticing that in general there may exist more than one set of $\pi_{T}, \forall T \in \mathcal{T} $ with the same marginal probabilities. One possible way to determine a feasible $\pi_{T}$ is as follows:
	\begin{enumerate}
		\item Sort the marginal probability $p_{i}$, suppose that $p_{i_1}\geq p_{i_2}\geq\ldots\geq p_{i_m}$.
		\item Define $T_0 = \{s_0\}$, $T_j=T_{j-1}\bigcup \{s_{i_j}\}$.
		\item Choose $\pi_{T_0} = 1-p_{i_1}$, $\pi_{T_1}=p_{i_1}-p_{i_2},\pi_{T_2}=p_{i_2}-p_{i_3},\ldots,\pi_{T_m}=p_{i_m}$.
	\end{enumerate}
	One can easily verify that $T_i\in \mathcal T$ and $\pi_{T}$ are compatible with the marginal probability.
\end{remark}

