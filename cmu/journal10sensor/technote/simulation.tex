In order to show the effectiveness of the proposed method we apply our stochastic sensor selection algorithm to a numerical example in which a sensor network is deployed to monitor a diffusion process in a $l\times l$ planar closed region, whose model is given by
\begin{equation}
  u_t = \alpha \triangledown^2 u.
	\label{eq:diffussionpdfcont}
\end{equation}
where $\triangledown^2$ is the Laplace operator. The term $u(t,x_1,x_2)$ denotes the temperature at location $(x_1,x_2)$ at time $t$ and $\alpha$ indicates the speed of the diffusion process.

We use the finite difference method to discretize this model by gridding the region into $1\;meter\times 1\;meter$ tiles and time steps of $1s$. If we group all temperature values at time $k$ in the vector $U_k = [u(k,0,0),\ldots,u(k,0,N-1),u(k,1,0),\ldots,u(k,N-1,N-1)]^T$, we can write the evolution of the discretized system as $U_{k+1} = A U_{k}$, where the $A$ matrix can be computed from discretization. If we introduce process noise, $U_k$ will evolve according to $U_{k+1} = A U_{k} + w_k,$ where $w_k \in \mathcal N(0,\;Q)$ is the process noise.

We suppose that the fusion center is located in the bottom left corner at position $(0,0)$. We assume that $m$ sensors are randomly distributed in the region and that each sensor measures a linear combination of temperature on the grid vertices around it\footnote{We do not require the sensors to be placed at grid points}. In particular, if we suppose the location of sensor $l$ of coordinates  $(a_1,a_2)$ is in the cell $[i,j]$, i.e. $a_1 \in [i,i+1)$ and $a_2 \in [j,j+1)$, the measurement of this sensor is given by
\begin{displaymath}
	\begin{split}
	 &y_{k,l} = \left[\right.(1-\Delta a_1)(1-\Delta a_2) u(k,i,j)+  \Delta a_1(1-\Delta a_2) u(k,i+1,j)+\\
	 &(1-\Delta a_1)\Delta a_2 u(k,i,j+1)+  \Delta a_1\Delta a_2 u(k,i+1,j+1)\left.\right]/h^2+ v_{k,l}  .
	\end{split}
\end{displaymath}
where $\Delta a_1 =a_1-i$, $\Delta a_2 =a_2-j$ and $v_{k,l}$ is the measurement noise of sensor $l$ at time $k$. Indicating with $Y_k$ the vector of all measurements at time $k$, it follows that: $Y_k =  C U_k + v_k,$ where $v_k$ denotes the measurement noise at time $k$ assumed to have normal distribution $\mathcal N(0, R)$ and $C$ is the observation matrix. Finally, we assume that the sensor network admits a minimum spanning tree topology with communication cost from sensor $i$ to $j$ given by
\[
cost(e_{i,j}) = c+d_{i,j}^2,
\]
where $d_{ij}$ is the Euclidean distance from sensor $i$ to sensor $j$ and $c$ is a constant related to the energy consumption for sensing\footnote{$c$ models the fact that as the distance goes to zero the communication cost does not}. For the simulations, we impose the following parameters: $l=3\; meters$, $m = 16$, $\alpha = 0.1$, $Q = I =R = I \in \mathbb R^{16\times16}$, $\Sigma = 4I \in \mathbb R^{16\times 16}$, $\mathcal E_d = 6$,$c=1$.

%For the next simulation, we still assume that the topology showed in Fig~\ref{fig:topology} and we compare the performance of the fixed stochastic schedule found in Section~\ref{sec:asymptotic}, with the performance of a fixed deterministic scheduler. % Figure~\ref{fig:ergodic} shows the histogram of average trace over the time. The blue dashed line is the expected asymptotic trace of estimation error from stochastic schedule. The black dot line is the asymptotic trace of estimation error from deterministic schedule.
%It is easy to see that the deterministic one performs even worse, since it is forced to choose the same set of sensors over time.
%Moreover, we can see that the average trace over time is very close to the expected asymptotic trace, which is a consequence of Theorem~\ref{theorem:ergodic}.

We compare the performance of the proposed fixed stochastic schedule with the optimal fixed deterministic one found by exhaustive search. Figure~\ref{fig:fixed} shows the histogram of the ratio between $trace(P_\infty)$ of the deterministic schedule and $trace(EP_\infty)$ of the stochastic one computed used the proposed algorithm, computed over 100 random sensor placements. For each placement we computed the empirical expectation. The blue dashed line is the average ratio. It can be seen that the deterministic schedule is always worse than the stochastic one, which yields over 35\% improvement over the deterministic optimum in the average. Figure~\ref{fig:ergodic} shows the trace of $P_k$ for the optimal deterministic fixed schedule, together with the trace of $P_k$ from a sample path and $EP_k$ of the stochastic fixed schedule for one placement. The figure clearly shows how the average savings are due to the fact that the stochastic schedule is not constrained to stay within the energy budget at each step.

\begin{figure}[<+htpb+>]
  \centering
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure8.eps}
      \caption{Histogram of the ratio between $trace(P_\infty)$ of deterministic schedule and $trace(EP_\infty)$ of stochastic schedule}
      \label{fig:fixed}
    \end{center}
  \end{minipage}
  \hspace{0.5cm}
  \begin{minipage}[t]{0.4\textwidth}
    \begin{center}
      \includegraphics[width=0.9\textwidth]{figure5.eps}
      \caption{Evolution of $trace(P_k)$}
      \label{fig:ergodic}
    \end{center}
  \end{minipage}
\end{figure}
