\noindent First, let us state the following Proposition:
\begin{proposition}\label{proposition:convexity}
  Define functions $f(X),h(X)$ to be 
  \begin{align}
    f(X) &= X^{-1},\\
    h(X) &=(AX^{-1}A' + Q)^{-1}.
  \end{align}
where $X \in R^{n\times n}$  is positive definite and $T\in\mathcal T$. Then the following statements hold:
  \begin{enumerate}
    \item $f(X)$ is convex and monotone decreasing; 
    \item $h(X)$ is concave and monotone increasing; 
  \end{enumerate}
\end{proposition}

%
\begin{proof}[Proof of Theorem~\ref{theorem:lowerbound}]
	By the definition of $L_k$, we know that 
 \begin{align*}
   L^{-1}_k  = L^{-1}_{k|k-1} + \sum_{i=1}^m p_{k,i} \frac{C_i'C_i}{r_i},\, L^{-1}_{k|k-1} =(AL_{k-1}A' + Q)^{-1}.
 \end{align*}

 Let us define $Z_k \triangleq P_k^{-1},\,Z_{k|k-1}\triangleq P_{k|k-1}^{-1}$. We will first prove $L_k^{-1} \geq \mathbb EZ_k$ by induction. When $k=0$, $L_0^{-1} = Z_0 = P_0^{-1}$. Suppose that $L_{k-1}^{-1} \geq \mathbb EZ_{k-1}$, since $P_{k|k-1} = AP_{k-1}A'+Q$, we know that 
  \begin{equation}
      Z_{k|k-1} =(AZ_{k-1}^{-1}A' + Q)^{-1} = h(Z_{k-1}). 
  \end{equation}
  By taking the expectation on both sides, we get 
  \begin{equation}
    \mathbb EZ_{k|k-1} = \mathbb Eh(Z_{k-1}) \leq h(\mathbb EZ_{k-1}) \leq h(L^{-1}_{k-1}) = L^{-1}_{k|k-1}. 
  \end{equation}
  The first inequality is a consequence of the concavity of $h(X)$ and Jensen's inequality. The second inequality is derived from the monotonicity of $h(X)$ and from the fact that $L^{-1}_{k-1} \geq \mathbb EZ_{k-1}$. Now, by \eqref{eq:informationmatrix}, we know that 
  \begin{equation}
    \mathbb EZ_k = \mathbb EZ_{k|k-1} + \sum_{i=1}^m p_{k,i}\frac{C_i'C_i}{r_i} \leq  \mathbb EL^{-1}_{k|k-1} + \sum_{i=1}^m p_{k,i}\frac{C_i'C_i}{r_i} = L^{-1}_k. 
  \end{equation}
  Hence, for all $k$, $L^{-1}_k\geq \mathbb EZ_k$. Now, by the definition of $Z_k$, we know that
 \begin{displaymath}
   P_k = Z_k^{-1} = f(Z_k). 
 \end{displaymath}
 Since $f$ is convex, by Jensen's inequality, the following inequalities result 
 \begin{equation}
   \mathbb EP_k  = \mathbb Ef(Z_k) \geq f(\mathbb EZ_k) = (\mathbb EZ_k)^{-1} \geq L_k. 
 \end{equation}
\end{proof}

\begin{proof}[Proof of Theorem~\ref{theorem:lfunction}]
Fix $X$, 
\[
L(X,\mathbf p) = f\left((AXA^T+Q)^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right). 
\]
Since, $f$ is convex and $(AXA^T+Q)^{-1} + \sum_{i=1}^m p_i C_iC_i/r_i$ is linear with respect to $\mathbf p$, $L$ is convex with respect to $\mathbf p$. Once $\mathbf p$ is fixed, it is easy to see that $L$ is of the same form as $h$. By similar arguments, $L$ is concave and monotone decreasing with respect to $X$.
\end{proof}

Before proving Theorem~\ref{theorem:algorithm}, we need the following lemmas:

\begin{lemma}
  \label{lemma:existence1}
   Consider matrix $C_{\mathbf p} = [\sqrt{p_1}C_1',\ldots, \sqrt{p_m}C_m']'$. If the pair $(C_{\mathbf p}, A)$ is detectable, then the following limit exists for all positive semidefinite matrices $X$: 
  \begin{displaymath}
     L^{\infty}(X,\mathbf p) = \lim_{k\rightarrow\infty} L^{(k)}(X,\mathbf p). 
  \end{displaymath}
  Moreover, if the pair $(A,\,Q^{1/2})$ is controllable, then the above limit is unique regardless of $X$.
\end{lemma}

\begin{proof}
Let us build a linear system whose dynamics are given by 
  \begin{displaymath}
  \begin{array}{lcr}
    \tilde x_{k+1} & = & A \tilde x_k + \tilde w_k, \\
     \tilde y_k & = & C_{\mathbf p} \tilde x_k + \tilde v_k.
  \end{array}
  \end{displaymath}
where $\tilde x_0 \sim \mathcal N(0,X)$, $\tilde w_k \sim \mathcal N(0,Q)$, $\tilde v_k \sim \mathcal N(0,R)$ and all of them are mutually independent of each other. Consider now the covariance matrix of the Kalman filter for the above system, which is given by 
\begin{eqnarray}
  \tilde P_{0} &=& X, \\
   \tilde P_{k+1|k} &= & A\tilde P_{k}A^T + Q,\, \\
   \tilde P_{k+1} &=& (\tilde P_{k+1|k}^{-1} + c_{\mathbf{p}}R^{-1} c_{\mathbf{p}}')^{-1} = \left(\tilde P_{k+1|k}^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right)^{-1}.
  \label{eq:alterkalman} 
\end{eqnarray}
By construction, such a covariance matrix satisfies $\tilde P_{k} = L^{(k)}(X,p)$ and hence the limit $\tilde P_\infty = \lim_{k\rightarrow \infty}\tilde P_k$ exists if $(C_{\mathbf p}, A)$ is detectable. Moreover, the limit is unique regardless of $\tilde P_0$ if $(A,\,Q^{1/2})$ is controllable.
\end{proof}
\noindent Another theorem on the uniqueness of the limit can also be provided:

\begin{lemma}
  \label{lemma:existence2}
Let $Q > 0$ be a strictly positive definite matrix. If there exists a fixed point $X_0$ satisfying 
\begin{displaymath}
  X_0 = L (X_0 , \mathbf p), 
\end{displaymath}
then $L^\infty(X,\mathbf p)$ exists and moreover 
\begin{displaymath}
  L^\infty(X,\mathbf p) = X_0, \,\,\textrm{for all }X\textrm{ positive semidefinite}. 
\end{displaymath}
\end{lemma}

\begin{proof}
First, we want to show that $L(X,\mathbf p)$ is strictly positive for any $X \geq 0$. By definition we have 
\begin{displaymath}
   L (X , \mathbf p) = \left[(AXA^T+Q)^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right]^{-1} \geq  \left(Q^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right)^{-1}>0 . 
\end{displaymath}
In particular, this implies that $X_0 > 0$. Now, because $L(X,\mathbf p)$ is concave in $X$, we obtain: 
\begin{displaymath}
  \frac{1}{\alpha}L(\alpha X_0,\mathbf p)< \frac{1}{\alpha  }L(\alpha X_0,\mathbf p) + \frac{\alpha -1}{\alpha }L(0,\mathbf p) \leq L(X_0,\mathbf p) = X_0.\;\forall \alpha > 1 
\end{displaymath}
As a result, $L(\alpha X_0,\mathbf p) < \alpha X_0$ and, exploiting the monotonicity of $L(X,\mathbf p)$, the following inequality holds 
\begin{displaymath}
 0 < L^{(k+1)}(\alpha X_0,\mathbf p) < L^{(k)}(\alpha X_0,\mathbf p). 
\end{displaymath}
Then $L^{(k)}(\alpha X_0,\mathbf p)$ is bounded regardless of $k$. Because $X_0 > 0$ for any $X$ positive semidefinite, there exists a scalar $\alpha_x>1$, such that $X \leq \alpha _x X_0$, then, using again the monotonicity of $L(X,\mathbf p)$, one can prove that $ L^{(k)}(X,\mathbf p) < L^{(k)}(\alpha_x X_0,\mathbf p)$ is also bounded regardless of $k$. Hence, the pair $(C_{\mathbf p}, A)$ must be detectable, which implies that $L^\infty(X,\mathbf p)$ exists for all $X$. Moreover, since $Q > 0$, the limit is unique and it must be $X_0$.
\end{proof}

Now we are ready to prove Theorem~\ref{theorem:algorithm}
\begin{proof}
  \begin{enumerate}
    \item It is easy to check that $C_{\mathbf p_0} =\sqrt{ \mathcal E_d /(\sum_{i=1}^m c_i)}C$ and $\mathbf p_0\in \mathcal P$. Since $(C,\,A)$ is detectable, $(\sqrt{ \mathcal E_d /(\sum_{i=1}^m c_i)}C,A)$ is also detectable and then $L_0$ exists. 
    \item[5)] By the definition of accumulation point, there is a subsequence $\mathbf p_{i_1} ,\mathbf p_{i_2},\ldots$ which converges to $\mathbf p^*$. For each index $i_k$ we have 
  \begin{displaymath}
    L(L_{i_k-1},\mathbf p_{i_k}) = L_{i_k}. 
  \end{displaymath}
  If we take the limit on both side and exploit the fact that $L(X,\mathbf p)$ is continuous, we obtain 
   \begin{displaymath}
   L(L_\infty,\mathbf p^*) = L_\infty, 
  \end{displaymath}
  and finally by Lemma~\ref{lemma:existence2}, the limit is unique. 
  \end{enumerate}
\end{proof}
