The goal of this section is to extend the results of Section~\ref{sec:main} to communication networks having a general graph topology. As it will be clear soon, though an exact solution can be given for small networks, its computational complexity grows exponentially with the number of sensors. For such a reason we will propose a heuristic algorithm and, in order to evaluate the heuristic's performance, we will provide a method to compute a lower bound to the solution of the original problem.

Let us consider the problem of determining a sensor selection strategy in the case $G=\{V,E\}$ is a generic graph. To this end,
%One can check that all the analysis holds in Section~\ref{sec:main}, except Proposition~\ref{proposition:energy}, \ref{proposition:existence} and Equation~\eqref{eq:treecondition}. In particular, the formulation of Problem~\ref{randoptlower} is still valid. However in Problem~\ref{randoptlower}, the number of optimization variables and constraints are not polynomical with respect to the number of sensors in general. As a result, to render Problem~\ref{randoptlower} tractable for general topology, we need to reduce the number of constraints and optimization variables.
%
let us first introduce the concept of spanning tree. A tree $T =\{V_T,\,E_T\}$ with $E_T $ is a spanning tree of $G$ if $V_T = V$ and $E_T \subseteq E$. We will denote with $\mathcal T_{span} = \{T_{span,1},\ldots,T_{span,|\mathcal T_{span}|}\} \subset \mathcal T $ the set of all spanning trees of the graph $G$. The following theorem \cite{graphtheory} provides a formula to compute the cardinality of $|\mathcal T_{span}|$.
\begin{theorem}[Kirchoff's Theorem]
  Let the non-zero eigenvalues of the Laplacian matrix of graph $G$ be $\lambda_1,\ldots,\lambda_l$. Then, the number of spanning trees of the graph is \vspace{-0.15 cm}
  \begin{displaymath}
    |\mathcal T_{span}| = \frac{1}{l} \prod_{i=1}^l\lambda_i.
  \end{displaymath}
\end{theorem}
%
It is worth noticing that although $|\mathcal T_{span}|$ may be not polynomial with respect to number of vertices, it is in general much smaller than $|\mathcal T|$. For instance, consider the perfect binary tree of $63$ nodes. The only spanning tree is the graph itself while the total number of subtrees is, as already seen, on the order of $10^{11}$.

Now, similarly to the case of tree topologies, let us introduce the variable $p_{k,i,s}\in [0,1]$ representing the probability that, at time $k$, the $i$-th node will send information exploiting a subtree of the spanning tree $T_{span,s}$. Let us denote with $parent(i,s)$ the parent of the $i$-th node in the tree $T_{span,s}$, i.e.
\vspace{-0.25 cm}
\[
parent(i,s)=j\,\,\,\, if\,\,\,(i,j) \in T_{span,s}. \vspace{-0.25 cm}
\]
In order to maintain connectivity to the root via the spanning tree $T_{span,s}$ we need to impose that the parent nodes have an higher probability than children \vspace{-0.2 cm}
\begin{equation}\label{constr1prob5}
  p_{k,i,s} \leq p_{k,parent(i,s),s},\,\,i=1,...,m,\, s=1,...,|\mathcal T_{span}|. \vspace{-0.2 cm}
\end{equation}
Moreover, we have to make sure that the marginal probability for each sensor being selected is less then one
\vspace{-0.2 cm}
\begin{equation}\label{constr2prob5}
  p_{k,i} =  \sum_{s=1}^{|\mathcal T_{span}|} p_{k,i,s}  \leq 1,\,\,i=1,...,m. \vspace{-0.2 cm}
\end{equation}
The latter two observations allow us to reformulate Problem \ref{randoptlowerfinal} in the case of general networks as follows:
\vspace{-0.35 cm}
\begin{prob}[Exact Algorithm for General Graph ]\label{generalgraphtheoricprob}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_k,p_{k,i,s}}&
  &trace(L_k)=trace(L(L_{k-1},\mathbf p_k))&& \\[-0.2 cm]
  &\textrm{subject to}&
  &\sum \limits_{s = 1}^{|\mathcal T_{span}|}  \sum \limits_{i = 1}^m c_{i,parent(i,s)} p_{k,i,s}\leq \mathcal E_d&&\\[-0.2 cm]
  &&&\sum \limits_{s=1}^{|\mathcal T_{span}|} p_{k,i,s} = p_{k,i}\leq 1,&&i=1,...,m,\\[-0.2 cm]
  &&&0\leq p_{k,i,s}\leq 1,&&i=1,...,m,\, s=1,...,|\mathcal T_{span}|\\[-0.2 cm]
  &&& p_{k,i,s} \leq p_{k,parent(i,s),s},& & i=1,...,m,\, s=1,...,|\mathcal T_{span}|.
\end{align*}
\end{prob}
\vspace{-0.15 cm}
\noindent Conceptually this solution corresponds to re-mapping the original graph into a big tree obtained by merging all its spanning trees into a single graph and by fusing the root node of any spanning tree. A graphical depiction for a very simple general graph is given in Figure~\ref{fig:eqtree}. In this viewpoint, the only additional constraints w.r.t. Problem \ref{randoptlowerfinal} are the inequalities (\ref{constr2prob5}), which are needed to take into account the fact that nodes of this new tree are just copies of a smaller set of nodes and then their overall transmission probability cannot be bigger than one.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width=0.7\textwidth]{GraphFormulation.eps}
  \caption{Equivalent tree of a general graph}
  \label{fig:eqtree}
  \end{center}
\end{figure}

The main drawback of this approach is that the total number of spanning trees of a graph, which can be computed by Kirchoff's Theorem, is usually not polynomial w.r.t. the number of sensors. However, the proposed algorithm can be used as a basis to build up a heuristic algorithm which doesn't consider all spanning trees but only a ``representative subset'' of them. The problem of enumerating the spanning trees of a general graph has been studied. In \cite{EnumerateSpanningTrees}, the authors design an algorithm that enumerates the spanning trees in an increasing order of their total cost. Random spanning tree generation algorithms based on random walks have also been studied by several authors \cite{randomtree}. As a result, we assume that we can enumerate $n_s$ representative spanning trees and use them to build up the following heuristic:
\begin{prob}[Heuristic Algorithm for General Graphs]\label{generalgraphProb}
\begin{align*}
 &\mathop{\textrm{minimize}}\limits_{\mathbf p_k,p_{k,i,s}}&
  &trace(L_k) (=trace(L(L_{k-1},\mathbf p_k)))&&\\[-0.2 cm]
  &\textrm{subject to}&
  &\sum \limits_{s = 1}^{n_s}  \sum \limits_{i = 1}^m c_{i,parent(i,s)} p_{k,i,s}\leq \mathcal E_d&&\\[-0.2 cm]
  &&&\sum \limits_{s=1}^{n_s} p_{k,i,s} = p_{k,i}\leq 1&
  &i=1,...,m,\\[-0.2 cm]
  &&&0\leq p_{k,i,s}\leq 1,&
  &i=1,...,m,\, s=1,...,n_s\\[-0.2 cm]
  &&& p_{k,i,s} \leq p_{k,parent(i,s),s}, &
  & i=1,...,m,\, s=1,...,n_s.
\end{align*}
\end{prob}
\vspace{-0.25 cm}
\noindent Obviously, the solution for Problem~\ref{generalgraphProb} is an upper bound for Problem~\ref{generalgraphtheoricprob} and the more spanning tree we choose, the less conservative the heuristic is. In order to evaluate gap between Problem~\ref{generalgraphtheoricprob} and \ref{generalgraphProb}, we will provide a computationally efficient algorithm to compute a lower bound to Problem \ref{generalgraphtheoricprob}. Let us define the random variable $\mathbb I^{(i,j,l)}_k$ as an indicator function such that $\mathbb I^{(i,j,l)}_k = 1$ if and only if node $i$ is selected and the directed edge $e_{j,l}$ is on the path from node $i$ to the root node at time $k$. It is easy to check that the following proposition holds:
\begin{proposition}
  The following equalities and inequalities on $\mathbb I_k^{(i,j,l)}$ and $\gamma_{k,i}$ hold
  \vspace{-0.25cm}
      \begin{align}
	\mathbb I_k^{(i,j,l)} &\leq \mathbb I_k^{(j,j,l)}, &
	&\forall i,\,j,\,l =0,\ldots,m. 	
	\label{eq:indicator1}\\
	 \mathbb I_k^{(i,j,i)}& = 0, &
	 &\forall i,\,j=1,\ldots,m\textrm{ and }i\neq j .	
	\label{eq:indicator2}\\
	\sum_{j\neq i,j=0,\ldots,m} \mathbb I_k^{(i,i,j)}& = \gamma_{k,i}, & &\forall i = 1,\ldots,m.&
	&
	\label{eq:indicator4}\\
\mathcal E(T_k) &= \sum_{i,j=0,\ldots,m,\,i\neq j}\mathbb I_k^{(i,i,j)}c(e_{i,j}).	 &
&
	\label{eq:indicator5}
      \end{align}
\end{proposition}
Now define $p_k^{(i,j,l)} = \mathbb E\mathbb I_k^{(i,j,l)}$. If we take the expectation on both side of \eqref{eq:indicator1}, \eqref{eq:indicator2}, \eqref{eq:indicator4} and \eqref{eq:indicator5}, it is trivial to prove that the following theorem holds.
\begin{theorem}
 The following equalities and inequalities on $p_k^{(i,j,l)}$ and $p_{k,i}$ holds
  \vspace{-0.25cm}
      \begin{align}
	p_k^{(i,j,l)} &\leq p_k^{(j,j,l)},&&\forall i,\,j,\,l =0,\ldots,m, 	\\%[-0.2 cm]
	p_k^{(i,j,i)} &= 0,& &\forall i,\,j=1,\ldots,m\textrm{ and }i\neq j, 	\\%[-0.2 cm]
	\sum_{j\neq i,j=0,\ldots,m} p_k^{(i,i,j)} &= p_{k,i}, &&\forall i = 1,\ldots,m,\\%[-0.2 cm]
	\mathbb E\mathcal E(T_k) &= \sum_{i,j=0,\ldots,m,\,i\neq j}p_k^{(i,i,j)}c(e_{i,j}).&&	
      \end{align}
\end{theorem}
%\vspace{-0.3 cm}
The above theorem provides a necessary condition for $\mathbf p_k\in \mathcal P$. Therefore, the following optimization problem will be a lower bound for Problem~\ref{generalgraphtheoricprob}:
\begin{prob}[Lower Bound for General Graphs]\label{generalgraphProblower}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_k,\,p^{(i,j,l)}_k}&
  &trace(L_k) &&\\[-0.3 cm]
  &\textrm{subject to}&
  &L_k  = L(L_{k-1}, \mathbf p_k)&&\\[-0.2 cm]
&&&p_k^{(i,j,l)} \leq p_k^{(j,j,l)},&&\forall i,\,j,\,l =0,\ldots,m, 	\\[-0.2 cm]
&&&	p_k^{(i,j,i)} = 0,&&\forall i,\,j=1,\ldots,m\textrm{ and }i\neq j, 	\\[-0.2 cm]
&&&	\sum_{j\neq i,j=0,\ldots,m} p_k^{(i,i,j)} = p_{k,i} ,&&\forall i = 1,\ldots,m,\\[-0.2 cm]
&&&	\sum_{i=0,\ldots,m}\sum_{j\neq i,j=0,\ldots,m}p_k^{(i,i,j)}c(e_{i,j})\leq \mathcal E_d,	&& \\[-0.2 cm]
&&& p_{k}^{(i,j,l)}\in[0,1],\,p_{k,i}\in [0,1],&&\forall i=1,\ldots,m\textrm{ and }j,l=0,\ldots,m.
\end{align*}
\vspace{-1 cm}
\begin{remark}
  Note that the number of optimization variables and constraints in Problem~\ref{generalgraphProblower} is $O(m^3)$. Hence, the above problem can be solved in polynomial time with respect to the number of sensors.
%
%  We would like to comment that Problem~\ref{generalgraphProblower} provides a lower bound of Problem~\ref{randoptlower} in generic graphs, while Problem~\ref{generalgraphProb} gives an upper bound. Hence, the solution of Problem~\ref{generalgraphProblower} will indicate how close the solution of Problem~\ref{generalgraphProb} is to the real optimal.
%
  Moreover, note that although in the general case the solution of Problem~\ref{generalgraphProblower} does not provide a feasible solution for Problem \ref{generalgraphtheoricprob}, it may happen that the optimal solution of Problem~\ref{generalgraphProblower} has a simple topology and is feasible. In such a case, that schedule is the optimal solution for Problem~\ref{generalgraphtheoricprob}.
\end{remark}
\end{prob}
