\subsection{System Description}
%
Consider the following discrete-time LTI system
\begin{equation}
    x_{k+1} = Ax_k + w_k,
    \label{eq:systemdescription}
\end{equation}
where $x_k \in \mathbb R^n$ represents the state and $w_k\in \mathbb R^n$ the disturbance. It is assumed that $w_k$ and $x_0$ are independent Gaussian random vectors, $x_0 \sim \mathcal N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$, where $\Sigma,\,Q > 0$ are positive definite matrices. A wireless sensor network composed of $m$ sensing devices $s_1,\ldots,s_m$ and one fusion center $s_0$ is used to monitor the state of system \eqref{eq:systemdescription}. The measurement equation is
\begin{equation}
  y_{k} = C x_k + v_k,
\end{equation}
where $y_k = [y_{k,1}, y_{k,2} ,\ldots, y_{k,m}]' \in \mathbb R^m$ is the measurement vector\footnote{The $'$ on a matrix always means transpose.}. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$, $C = \left[C_1',\ldots,C_m'\right]'$ is the observation matrix and the matrix pair $(C,\,A)$ is assumed observable\footnote{The assumption of observability is without loss of generality since we could perform Kalman decomposition and only consider the observable space even if the system is not observable.}, $v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed to be independent of $x_0$ and $w_k$. We also assume that the covariance matrix $R = diag(r_1,\ldots,r_m) $ is diagonal, which means that the measurement noise at each sensor is independent of all others and nonsingular, that is $r_i > 0, i=1,...,m$.

In order to model the communication amongst the nodes we introduce an oriented communication graph $G = \{V\,,E\}$ where the vertex set $V = \{s_0,\,s_1,\ldots,s_m\}$ contains all sensor nodes, including the fusion center. The set of edges $E \subseteq V \times V$ represents the available connections, i.e.  $(i,j) \in E$ implies that the node $s_i$ may send information to the node $s_j.$ Moreover we assume that each node of the sensor network acts as a gateway for a specific number of other nodes, which means that every time it communicates with another node it sends, in a single packet, its own measurements collected together with all data received from the other nodes.

We always assume that, for every sensor in the network, there exists one and only one communication path to the fusion center, i.e. the sensor network has a directed tree topology. Moreover, we assume that each link has an associated weight $c(e_{i,j})$ which indicates the energy consumed when $s_i$ directly transmits a packet to $s_j$. For the sake of legibility, we sometimes abbreviate $c(e_{i,j})$ as $c_i$, $i= 1,\ldots,m$ because, in the assumed topology, each sensor node has only one outgoing edge.
\begin{remark}
  The tree topology assumption may be a restrictive hypothesis for the general cases where one sensor can communicate with several nearby nodes. However, it is worth to remark that typical communication network graphs can be approximated by a collection of ``representative" spanning trees (e.g. the first $m$ spanning trees of the spanning tree enumeration \cite{EnumerateSpanningTrees}). Space constraints are forcing the authors to defer this discussion to future works.
  \end{remark}
\subsection{Stochastic v.s. Deterministic Sensor Selection}
%
In order to reduce the energy consumption it is desirable to use a subset of sensors at each sampling time because of the redundancy in sensor measurements. However, in a tree topology, we cannot select arbitrary subsets of nodes as we are forced to select nodes to guarantee that there exists a communication path to the fusion center for each selected node. As a result, any possible transmission topology of $G$ is a subtree $T = \{V_T\,,E_T\}$, with $s_0 \in V_T$,  $V_T\subseteq V$ and $E_T\subseteq E$. Hereafter, $V_T$ denotes the selected subset of sensors and $E_T$ the communication links used by the sensors to transmit observations to the fusion center. We also denote by $\mathcal T$ the set of all possible transmission topologies $T$ (i.e. the set of all possible subtrees of $G$ containing $s_0$).
%
It is straightforward to show that, for a transmission tree $T$, the total transmission energy consumption is given by\footnote{Here we assume that $cost(e_{i,j})$ is constant regardless of the number of observations contained in the packet. This is realistic in most of the cases, especially when measurements are of simple type, such as low precision scalar values, and the transmission overhead, e.g. header, handshaking protocol, dominates the payload.}
\begin{displaymath}
  \mathcal E(T) = \sum_{e \in E_T} c(e).
\end{displaymath}

Suppose that at each time $k$ we randomly select a tree $T$ from $\mathcal T$  and each sensor in $T$ transmits its observation back to the fusion node according to the topology $T$. Let $\pi_{k,T}$ be the probability that the transmission tree $T$ is selected at time $k$. Then, we define
\begin{equation}
  p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T}
\end{equation}
%
as the marginal probability that sensor $i$ is selected at time $k$. Further, let us define the aggregate vector $\mathbf p_k = [p_{k,1},\ldots,p_{k,m}]'$ and $\mathbf \pi_k=[\pi_{k,T_1},\ldots,\pi_{k,T_{|\mathcal T|}}]'$ containing all $p_{k,i}$s and $\pi_{k,T}$s respectively. We introduce also a binary random variable $\delta_{k,T}$ such that $\delta_{k,T}=1$ if the transmission tree $T$ is selected at time $k$ and $\delta_{k,T}=0$ otherwise. Similarly, the binary random variable $\gamma_{k,i}$ will take value $1$ if the $i$-th sensor is selected at time $k$ and $0$ otherwise. It is well known that the Kalman filter is still optimal \cite{Gupta_sensorselection_automatica} for the above time-varying system structure. Suppose next that $V_T = \{s_0,\,s_{i_1},\ldots,s_{i_j}\}$. Then, we can define
\begin{eqnarray}
  C_{T} & \triangleq & [C_{i_1}',\,C_{i_2}',\ldots,C_{i_j}']',  R_{T}  \triangleq   diag(r_{i_1},\ldots,r_{i_j}).
\end{eqnarray}
It can be shown that the estimation error covariance matrix $P_k$ of the Kalman filter satisfies the following recursive equations:
%and the information matrix $Z_k$\footnote{The information matrix is the inverse of the estimation error covariance matrix} 
\begin{align}
  P_k &= \left(P_{k|k-1}^{-1} + C_T'R_T^{-1}C_T\right)^{-1},
\label{eq:informationmatrix}
\end{align}
where $ P_{k|k-1}= AP_{k-1}A' + Q.$
%
Let us define $\mathbf g_{\mathbf \pi_k,k}$ as a random operator such that
\begin{equation}
  \mathbf g_{\mathbf \pi_k,k}(X) \triangleq \sum_{T\in\mathcal T}\delta_{k,T} g_T(X),
\end{equation}
where $P(\delta_{k,T} = 1) = \pi_{k,T}$, and
\begin{equation}
  g_T(X) \triangleq \left[(AXA'+Q)^{-1} + \sum_{s_i \in V_{T},\, s_i\neq s_0} \frac{C_iC_i'}{r_i}\right]^{-1}.
\end{equation}
We have
\begin{equation}
  P_k = \mathbf g_{\mathbf \pi_k,k}(P_{k-1}).
\end{equation}
In this paper we are interested in determining a time-invariant stochastic schedule $\pi$, i.e. a schedule where the probability of choosing a tree is constant over time. Hence, let us define
\begin{equation}
  \mathbf g^{\infty}_\pi(X)\triangleq \lim_{k\rightarrow\infty}\mathbb E (g_{\pi,k}\circ g_{\pi,k-1} \circ\cdots\circ g_{\pi,1})(X),
\end{equation}
when the limit exists and infinity otherwise. Note that $\mathbf g^{\infty}_\pi$ is a deterministic function which indicates what asymptotic performance of the stochastic sensor selection scheme is to be expected. It is easy to see that
\begin{displaymath}
  \lim_{k\rightarrow\infty} \mathbb EP_k = \mathbf g^{\infty}_\pi(\Sigma),
\end{displaymath}
when $\pi$ is used and $ \mathbf g^{\infty}_\pi(\Sigma)<\infty$.

 Because the transmission trees are randomly selected, $P_k$ is a random matrix. Thus, we minimize the asymptotic expected estimation error covariance matrix while requiring that the expected energy consumption does not exceed a designated threshold $\mathcal E_d$. Such problem can be formulated as follows:
\begin{prob}[Fixed Random Schedule that Optimizes Expected Asymptotic Performance]\label{randoptasymptotic}
  \begin{align*}
    &\mathop{\textrm{minimize}}\limits_{\mathbf \pi }&
    &trace(\mathbf g_\pi^\infty(\Sigma))\\%[-0.5 cm]
 &\textrm{subject to}&
 &   \sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d,\, \pi_{T}\geq 0,\, \sum_{T\in\mathcal T} \pi_{T} = 1.
  \end{align*}
\end{prob}
%
Please note that if, instead of a stochastic schedule, we want to look for a fixed deterministic one, the problem we need to solve is the same Problem 1 with the further constraints that $\pi_T$s are forced to be either $0$ or $1$, namely :
\begin{prob}[Fixed Deterministic Schedule that Optimizes Asymptotic Performance]\label{detoptasymptotic}
  \begin{align*}
    &\mathop{\textrm{minimize}}\limits_{\mathbf \pi }&
    &trace(\mathbf g_\pi^\infty(\Sigma))\\%[-0.5 cm]
 &\textrm{subject to}&
 &   \sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d,\, \pi_{T}=0\;or\;1,\, \sum_{T\in\mathcal T} \pi_{T} = 1.
  \end{align*}
\end{prob}

\begin{remark}
  In Problem~\ref{randoptasymptotic} we require that the expected energy consumption does not exceed a certain energy budget. In real applications different constraints may be considered (e.g. requirements on sensor lifetime). However, it can be shown (see e.g. \cite{Joshi}) that many of these constraints can be easily integrated into the present framework.
\end{remark}

\begin{remark}
It is worth noticing that at each sampling time, the energy cost of the deterministic schedule cannot exceed the designated threshold $\mathcal E_d$. Intuitively, this is the reason why stochastic sensor selections, being allowed to use more energy at one single sampling period, can achieve better performance than the above deterministic formulation. It is also worth noticing that this formulation can be used to derive a periodic schedule by enlarging the state space. As a consequence, all the results in this section can be generalized to periodic schedules. However, due to space constraints, Section~\ref{sec:main} focuses exclusively on time-invariant schedules. Such an extension will be discussed in future work.
\end{remark}
% Finally note that, even if the horizon is reduced from $N$ to $1$, the numerical burden required to solve Problem~\ref{detoptasymptotic} with an exhaustive search over the space $\mathcal T$ may be still huge even for a small sensor network. In order to give an idea, consider the case of a small sensor network $G$ organized as a perfect binary tree with $63$ nodes. Then, the number of candidate solutions, i.e. all possible transmission subtrees $T$, is of order $10^{11}$.
% In order to address the impracticality of exhaustive searches and to improve the performance of the deterministic formulation, we propose the novel stochastic selection strategy that will be discussed in the next subsection.
%

\begin{remark}
  \label{remark:stochasticvsdeterministic}
  A main difference between Problem~\ref{randoptasymptotic} and Problem~\ref{detoptasymptotic} is that, while the search space of deterministic scheduling is discrete, the one for the stochastic problem  is continuous and convex. This brings several advantages. First, the deterministic schedule can be seen as a particular kind of random schedule, where $\pi_{T}$s are binary-valued. As a result, stochastic sensor selection strategies always yield better or equal performance, in the expected sense, when compared to the deterministic one. The second advantage is that the feasible set $\pi_{T}$ is convex, which allows us to further manipulate the objective function to obtain a convex optimization problem.
\end{remark}

As mentioned above, the expected performance of the optimal stochastic schedule is equal to or better than its deterministic counterpart. As a consequence, if we denote $\pi^*$ as the optimal stochastic schedule and $\pi^*_d$ the optimal deterministic schedule, we can assert that
\begin{displaymath}
  trace(\mathbf g_{\pi^*}^\infty(\Sigma)) = \lim_{k\rightarrow\infty} \mathbb E\, trace(P_k(\pi^*))  \leq \lim_{k\rightarrow\infty}  trace(P_k(\pi_d^*)),
\end{displaymath}
which implies that
\begin{displaymath}
  \lim_{N\rightarrow\infty}\sum_{k=1}^N\frac{1}{N} \mathbb E\, trace(P_k(\pi^*))  \leq  \lim_{N\rightarrow\infty}\sum_{k=1}^N\frac{1}{N} trace(P_k(\pi_d^*)),
\end{displaymath}
In other words, the optimal stochastic schedule performs better than the optimal deterministic one in the expected sense. To strengthen this result, the following theorem states that almost surely the stochastic schedule is better than the deterministic schedule almost surely.
\begin{theorem}
  Suppose that the fixed schedule $\mathbf \pi^*$ is the solution of Problem~\ref{randoptasymptotic}. If the linear system and $\mathbf \pi^*$ satisfy the following assumptions:
  \begin{enumerate}
    \item $A$ is invertible, $(A,Q^{1/2})$ is controllable;
    \item there exists a transmission topology $T$ with $\pi^*_{T} > 0$ such that  $(C_T,\,A)$ is observable
\item the stochastic process $\{P_k\}$ satisfies: $P_k = \mathbf g_{\pi^*,k} (P_{k-1}),\, P_0 = \Sigma,$
  \end{enumerate}
 then almost surely the following inequality holds
  \begin{equation}
    \lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(P_{k})  \leq trace(\mathbf g_{\pi^*}^\infty(\Sigma)).
    \label{eq:ergodic}
  \end{equation}
  \label{theorem:ergodic}
\end{theorem}
\vspace{-0.5cm}
\begin{proof}
  It is easy to check that all assumptions in Theorem 3.4 of \cite{randomriccati} hold true. As a result, there exists an ergodic stationary process $\{\overline P_k\}$ which satisfies $\overline P_k = \mathbf g_{ {\pi^*},k}(\overline P_{k-1})$. Moreover,
  \begin{displaymath}
    \lim_{k\rightarrow \infty} \|P_k -\overline P_k\| = 0.\,a.s.
  \end{displaymath}
  We want to prove that $\mathbb E(trace(\overline P_0))$ is lower than or equal to $trace(\mathbf g_\pi^\infty(\Sigma))$ and hence bounded. Because $\overline P_k$ is ergodic, and $P_k$ converges to $\overline P_k$ almost surely, we know that
  \begin{align*}
    &\lim_{N\rightarrow\infty}\frac{1}{N}\sum_{k=1}^N \min(trace(P_k),M) =  \lim_{N\rightarrow\infty}\frac{1}{N}\sum_{k=1}^N \min(trace(\overline P_k),M)\\
    &=\mathbb E[\min(trace(\overline P_0),M)],\;\;\;a.s.
  \end{align*}
  where $M > 0$ is a constant. By the definition of $\mathbf g_\pi^\infty$, we know that
  \begin{displaymath}\begin{split}
   & trace(\mathbf g_\pi^{\infty}(\Sigma))  \geq \lim_{N\rightarrow\infty} \mathbb E\left[\frac{1}{N} \sum_{k=1}^N \min(trace(P_k),M)\right]\\
    & = \mathbb E\left[ \lim_{N\rightarrow\infty} \frac{1}{N} \sum_{k=1}^N \min(trace(P_k),M)\right] = \mathbb E[\min(trace(\overline P_0),M)].
  \end{split}
  \end{displaymath}
The second equality follows from the Dominated Convergence Theorem. Now, let $M\rightarrow \infty$. By the Monotone Convergence Theorem, it results that
\begin{displaymath}
  \mathbb E[trace(\overline P_0)] = \lim_{M\rightarrow\infty}\mathbb E[\min(trace(\overline P_0),M)]\leq trace(\mathbf g_\pi^{\infty}(\Sigma)),
\end{displaymath}
which proves that $\mathbb E[trace(\overline P_0)] \leq trace(\mathbf g_\pi^\infty(\Sigma))$. Hence, by ergodicity, we obtain
  \begin{align*}
    &\lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(P_{k}) =  \lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(\overline  P_{k}) = \mathbb E(trace(\overline P_0))\\
    &\leq trace(\mathbf g_\pi^\infty(\Sigma)),\;\;\;a.s.
  \end{align*}
\end{proof}

\begin{remark}
  Combining Remark~\ref{remark:stochasticvsdeterministic} with Theorem~\ref{theorem:ergodic}, we conclude that the average performance of almost every sample path of the optimal stochastic schedule has a lower cost than its deterministic counterpart.
\end{remark}

Before moving forward, it is worth noticing that Problem~\ref{randoptasymptotic} is still numerically intractable for the following reasons:
\begin{enumerate}
  \item it is usually difficult to express $\mathbb EP_{\infty}$ as an explicit function of $\pi_{1,T},\,\ldots,\,\pi_{k,T}$;\footnote{The readers can refer to \cite{sinopoli} for more information.}
  \item since $|\mathcal T|$ is large, the number of optimization variables and constraints may be not polynomial with respect to the number of nodes.
\end{enumerate}
In the next section, we devise a way to overcome the above two problems.