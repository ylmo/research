The stochastic sensor selection algorithm is based on the idea of optimally assigning the probability that a certain transmission subtree $T$ is selected at time $k$, denoted by $\pi_{k,T}$. Suppose that at each time $k$ we randomly select a tree $T$ from $\mathcal T$  and each sensor in $T$ transmits its observation back to the fusion node according to the topology $T$. Let $\pi_{k,T}$ be the probability that the transmission tree $T$ is selected at time $k$. Then, we may define
\vspace{-0.25 cm}
\begin{equation}
  p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T} \vspace{-0.25 cm}
\end{equation}
%
the marginal probability that sensor $i$ is selected at time $k$. Further, let us define $\mathbf p_k = [p_{k,1},\ldots,p_{k,m}]'$ and $\mathbf \pi_k=[\pi_{k,T_1},\ldots,\pi_{k,T_{|\mathcal T|}}]'$ to be the vectors of all $p_{k,i}$s and $\pi_{k,T}$s respectively. We can introduce the binary random variable $\delta_{k,T}$ such that $\delta_{k,T}=1$ if the transmission tree $T$ is selected at time $k$ and $\delta_{k,T}=0$ otherwise. Similarly, let us also define the binary random variable $\gamma_{k,i}$ to be $1$ if sensor $i$ is selected at time $k$ and $0$ otherwise. The estimation error covariance $P_k$ and the information matrix $Z_k$ satisfy the following recursive equations:
\vspace{-0.25 cm}
\begin{align}
  P_k &= P_{k|k-1}\! -\!\! \sum_{T\in\mathcal T}\delta_{k,T}  P_{k|k-1}C_T'(C_TP_{k|k\!-\!1}C_T'\!+\!R_T)^{-1}C_TP_{k|k\!-\!1},\label{eq:covariancematrix} \\[-0.15 cm]
  Z_k & = Z_{k|k-1} + \sum_{i=1}^m \gamma_{k,i} \frac{C_i'C_i}{r_i}\label{eq:informationmatrix},
\end{align}
\vspace{-0.25 cm}
where
\vspace{-0.05 cm}
\begin{equation}
  P_{k|k-1}= AP_{k-1}A' + Q,\, Z_{k|k-1} =(AZ_{k-1}^{-1}A' + Q)^{-1}\label{eq:informationmatrix2}.
\end{equation}
%
Let us define $\mathbf g_{\mathbf \pi}$ as a random operator such that
\begin{equation}
  \mathbf g_{\mathbf \pi_k, k}(X) \triangleq \sum_{T\in\mathcal T}\delta_{k,T} g_T(X),
\end{equation}
where $P(\delta_{k,T} = 1) = \pi_{k,T},$ we have
\begin{equation}
  P_k = \mathbf g_{\mathbf \pi_k,k}(P_{k-1}).
\end{equation}
Further, let us define
\begin{equation}
  \mathbf g^{\infty}_\pi(X)\triangleq \lim_{k\rightarrow\infty}\mathbb E (g_{\pi,k}\circ g_{\pi,k-1} \circ\cdots\circ g_{\pi,1})(X),
\end{equation}
when the limit exists. Otherwise, $\mathbf g^{\infty}_\pi(X)$ is infinity. Note that $\mathbf g^{\infty}_\pi$ is a deterministic function, which indicates the limit performance of stochastic sensor selection when schedule $\pi$ is always used.

Since transmission trees are randomly selected, $P_k$ becomes a random matrix. Thus, we can only minimize the expected estimation error covariance matrix $\mathbb EP_k$ while requiring that the expected energy consumption does not exceed the designated threshold $\mathcal E_d$. Based on these considerations, we can reformulate the stochastic version of the greedy algorithm seen in the previous Section as follows:
\vspace{-0.15 cm}
\begin{enumerate}
  \item Let $k = 1$ and $\mathbb EP_{k-1} = \mathbb EP_0 = \Sigma$. \vspace{-0.15 cm}
  \item Determine the distribution $\pi_{k,T}, \forall T \in \mathcal T$ which minimizes the trace of the expected one-step estimation error covariance matrix $EP_k$ subject to the constraint that the expected transmission energy consumption at time $k$ is lower than the one-step energy budget, i.e. \vspace{-0.15 cm}
\begin{prob}[Random Transmission Tree Selection]\label{randopt}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf \pi_{k}}&
  &trace(\mathbb EP_k) \left(= trace(\mathbb E \mathbf g_{\pi_k,k}(P_{k-1}))\right) \\[-0.25 cm]
  &\textrm{subject to}&
  &\sum_{T\in \mathcal T} \pi_{k,T}\mathcal E(T)\leq\mathcal E_d, \\[-0.25 cm]
  &&&\pi_{k,T}\geq 0,\, \sum_{T\in\mathcal T} \pi_{k,T} = 1.
\end{align*}
\end{prob} \vspace{-0.10 cm}
  \item Let $k \Leftarrow k+1$. \vspace{-0.15 cm}
  \item Repeat Step 2 and 3 until $k = N+1$.\vspace{-0.15 cm}
\end{enumerate}

In a similar way, the problem of finding the optimal fixed schedule that minimizes the expected asymptotic estimation error covariance matrix can be formulated as
\begin{prob}[Fixed Random Schedule that Optimizes Expected Asymptotic Performance]\label{randoptasymptotic}
  \vspace{-0.1 cm}
  \begin{align*}
    &\mathop{\textrm{minimize}}\limits_{\mathbf \pi }&
    &trace(\mathbf g_\pi^\infty(\Sigma))\\%[-0.5 cm]
 &\textrm{subject to}&
 &   \sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d, \\[-0.25 cm]
 && &\pi_{T}\geq 0,\, \sum_{T\in\mathcal T} \pi_{T} = 1.
  \end{align*}
\end{prob}

\begin{remark}
  \label{remark:stochasticvsdeterministic}
  The main difference between Problem~\ref{randopt} (Problem~\ref{randoptasymptotic}) and Problem~\ref{opt_dyn} (Problem~\ref{detoptasymptotic}) is that, instead of optimizing over a discrete space $\mathcal T$, we are optimizing over $\pi_{k,T}$ which is continuous. This brings several advantages. First, the deterministic schedule can be seen as a particular kind of random schedule, where $\pi_{k,T}$s are binary. Hence, the search space of Problem~\ref{randopt} (Problem~\ref{randoptasymptotic}) contains the search space of Problem~\ref{opt_dyn} (Problem~\ref{detoptasymptotic}). As a result, stochastic sensor selection strategies can actually improve the sensor selection performance (at least in the expected sense). The second advantage is that the feasible set $\pi_{k,T}$ is convex, which allows us to further manipulate the problem into a convex form.
\end{remark}

As is commented above, the expected performance of the optimal stochastic schedule is better than the deterministic counter part. To strength this result, the following theorem states that if the optimal stochastic schedule is allowed to run for a long time, then almost every sample path of the stochastic schedule is potentially better than deterministic one.
\begin{theorem}
  Suppose that the fixed schedule $\mathbf \pi$ is the solution of Problem~\ref{randoptasymptotic}. If the linear system and $\mathbf \pi$ satisfy the following assumptions:
  \begin{enumerate}
    \item $A$ is invertible, $(A,Q^{1/2})$ is controllable;
    \item there exists a transmission topology $T$ with $\pi_{T} > 0$ such that  $(C_T,\,A)$ is observable
  \end{enumerate}
and the stochastic process $\{P_k\}$ satisfies
  \begin{displaymath}
    P_k = \mathbf g_\pi (P_{k-1}),\, P_0 = \Sigma,
  \end{displaymath}
  then almost surely the following inequality holds
  \begin{equation}
    \lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(P_{k})  \leq trace(\mathbf g_\pi^\infty(\Sigma)).
    \label{eq:ergodic}
  \end{equation}
  \label{theorem:ergodic}
\end{theorem}
\begin{proof}
  It is easy to check that all the assumptions in the Theorem 3.4 of \cite{randomriccati} hold. As a result, there exists an ergodic stationary process $\{\overline P_k\}$ which satisfies
  \begin{displaymath}
    \overline P_k = \mathbf g_{\pi,k}(\overline P_{k-1}).
    \label{eq:recursiverandriccati}
  \end{displaymath}
  Moreover,
  \begin{displaymath}
    \lim_{k\rightarrow \infty} \|P_k -\overline P_k\| = 0,\;a.s.
  \end{displaymath}
  We want to prove that $\mathbb E(trace(\overline P_0))$ is less than or equal to $trace(\mathbf g_\pi^\infty(\Sigma))$ and hence is finite. Because $\overline P_k$ is ergodic, and $P_k$ converges to $\overline P_k$ almost surely, we know that
  \begin{displaymath}
    \lim_{N\rightarrow\infty}\frac{1}{N}\sum_{k=1}^N \min(trace(P_k),M) =  \lim_{N\rightarrow\infty}\frac{1}{N}\sum_{k=1}^N \min(trace(\overline P_k),M) =\mathbb E[\min(trace(\overline P_0),M)],\,a.s.
  \end{displaymath}
  where $M > 0$ is a constant. By the definition of $\mathbf g_\pi^\infty$, we know that
  \begin{displaymath}\begin{split}
    trace(\mathbf g_\pi^{\infty}(\Sigma)) &= trace(\lim_{k\rightarrow \infty} \mathbb E(P_k)) \geq \lim_{N\rightarrow\infty} \mathbb E\left[\frac{1}{N} \sum_{k=1}^N \min(trace(P_k),M)\right]\\
    & = \mathbb E\left[ \lim_{N\rightarrow\infty} \frac{1}{N} \sum_{k=1}^N \min(trace(P_k),M)\right] = \mathbb E[\min(trace(\overline P_0),M)].
  \end{split}
  \end{displaymath}
The second equality follows from the Dominated Convergence Theorem. Now, let $M\rightarrow \infty$. By Monotone Convergence Theorem it results that
\begin{displaymath}
  \mathbb E[trace(\overline P_0)] = \lim_{M\rightarrow\infty}\mathbb E[\min(trace(\overline P_0),M)]\leq trace(\mathbf g_\pi^{\infty}(\Sigma)),
\end{displaymath}
which proves that $\mathbb E[trace(\overline P_0)] \leq trace(\mathbf g_\pi^\infty(\Sigma))$. Hence, by ergodicity, we obtain
  \begin{displaymath}
    \lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(P_{k}) =  \lim_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(\overline  P_{k}) = \mathbb E(trace(\overline P_0))\leq trace(\mathbf g_\pi^\infty(\Sigma)).\,a.s.
  \end{displaymath}
\end{proof}

\begin{remark}
  Combining Remark~\ref{remark:stochasticvsdeterministic} with the results of Theorem~\ref{theorem:ergodic}, we can conclude that the average performance of almost every sample path of the optimal stochastic schedule will be better than its deterministic counterpart.
\end{remark}

Before moving forward, it is worth pointing out that Problem~\ref{randopt} and Problem~\ref{randoptasymptotic} are still numerical intractable. In fact:
\vspace{-0.15 cm}
\begin{enumerate}
  \item it is usually difficult to express $\mathbb EP_k$ as an explicit function of $\pi_{1,T},\,\ldots,\,\pi_{k,T}$;\footnote{The readers can refer to \cite{sinopoli} for more information.} \vspace{-0.15 cm}
  \item since $|\mathcal T|$ is large, the number of optimization variables and constraints may be not polynomial with respect to the number of nodes. \vspace{-0.15 cm}
\end{enumerate}
In the next two sections, we will devise a possible relaxation method that allows one to overcome the above two problems.
