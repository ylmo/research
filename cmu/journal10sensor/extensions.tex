In Section~\ref{sec:main}, the marginal probability distribution $\mathbf p_k$ is time-varying because $L_{k-1}$ is not in general a constant matrix. Such changes in the marginal probability distribution may cause implementation issues in the network as discussed in Section~\ref{sec:problem}. In this Section, we will focus on finding the optimal fixed schedule for Problem~\ref{randoptasymptotic}.

As discussed in Section~\ref{sec:main}, we will approximate the expected estimation error covariance with its lower bound $L_k$ as it is hard to compute $\mathbb EP_k$. Let us define the function recursions \vspace{-0.25 cm}
\begin{equation}
 L^{(1)}(X,\mathbf p) = L(X,\mathbf p), L^{(k)} (X,\mathbf p) = L(L^{(k-1)}(X,\mathbf p),\mathbf p),
\end{equation}
with \vspace{-0.25 cm}
\begin{equation}
  L^{\infty}(X,\mathbf p) = \lim_{k\rightarrow\infty} L^{(k)}(X,\mathbf p),
  \label{eq:asymptoticlowerbound} \vspace{-0.25 cm}
\end{equation}
when the limit exists.
%If the right limit does not exist, then we define $L^{\infty} (X,\mathbf p) = \infty$.
\\
The following theorem shows that $L^{\infty}$ exists under mild assumptions.
\vspace{-0.15 cm}
\begin{theorem}
  \label{theorem:existence1}
   Consider matrix $C_{\mathbf p} = [\sqrt{p_1}C_1',\ldots, \sqrt{p_m}C_m']'$. If the pair $(C_{\mathbf p}, A)$ is detectable, then the following limit exists for all positive semidefinite matrices $X$: \vspace{-0.25 cm}
  \begin{displaymath}
     L^{\infty}(X,\mathbf p) = \lim_{k\rightarrow\infty} L^{(k)}(X,\mathbf p). \vspace{-0.25 cm}
  \end{displaymath}
  Moreover, if the pair $(A,\,Q^{1/2})$ is controllable, then the above limit is unique regardless of $X$.
\end{theorem}
\vspace{-0.25 cm}
\begin{proof}
Let us build a linear system whose dynamics are given by \vspace{-0.25 cm}
  \begin{displaymath}
  \begin{array}{lcr}
    \tilde x_{k+1} & = & A \tilde x_k + \tilde w_k, \\[-0.25 cm]
     \tilde y_k & = & C_{\mathbf p} \tilde x_k + \tilde v_k.
  \end{array}\vspace{-0.25 cm}
  \end{displaymath}
where $\tilde x_0 \sim \mathcal N(0,X)$, $\tilde w_k \sim \mathcal N(0,Q)$, $\tilde v_k \sim \mathcal N(0,R)$ and all of them are mutually independent of each other. Consider now the covariance matrix of the Kalman filter for the above system, which is given by \vspace{-0.25 cm}
\begin{eqnarray}
  \tilde P_{0} &=& X, \\[-0.2 cm]
   \tilde P_{k+1|k} &= & A\tilde P_{k}A^T + Q,\, \\[-0.2 cm]
   \tilde P_{k+1} &=& (\tilde P_{k+1|k}^{-1} + c_{\mathbf{p}}R^{-1} c_{\mathbf{p}}')^{-1} = \left(\tilde P_{k+1|k}^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right)^{-1}.
  \label{eq:alterkalman} \vspace{-0.25 cm}
\end{eqnarray}
By construction, such a covariance matrix satisfies $\tilde P_{k} = L^{(k)}(X,p)$ and hence the limit $\tilde P_\infty = \lim_{k\rightarrow \infty}\tilde P_k$ exists if $(C_{\mathbf p}, A)$ is detectable. Moreover, the limit is unique regardless of $\tilde P_0$ if $(A,\,Q^{1/2})$ is controllable.
\end{proof}
\noindent Another theorem on the uniqueness of the limit can also be provided:
\vspace{-0.35 cm}
\begin{theorem}
  \label{theorem:existence2}
Let $Q > 0$ be a strictly positive definite matrix. If there exists a fixed point $X_0$ satisfying \vspace{-0.15 cm}
\begin{displaymath}
  X_0 = L (X_0 , \mathbf p), \vspace{-0.25 cm}
\end{displaymath}
then $L^\infty(X,\mathbf p)$ exists and moreover \vspace{-0.15 cm}
\begin{displaymath}
  L^\infty(X,\mathbf p) = X_0, \,\,\textrm{for all }X\textrm{ positive semidefinite}. \vspace{-0.25 cm}
\end{displaymath}
\end{theorem}
\vspace{-0.25 cm}
\begin{proof}
First, we want to show that $L(X,\mathbf p)$ is strictly positive for any $X \geq 0$. By definition we have \vspace{-0.25 cm}
\begin{displaymath}
   L (X , \mathbf p) = \left[(AXA^T+Q)^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right]^{-1} \geq  \left(Q^{-1} + \sum_{i=1}^m p_i \frac{C_iC_i'}{r_i}\right)^{-1}>0 . \vspace{-0.1 cm}
\end{displaymath}
In particular, this implies that $X_0 > 0$. Now, because $L(X,\mathbf p)$ is concave in $X$, we obtain: \vspace{-0.15 cm}
\begin{displaymath}
  \frac{1}{\alpha}L(\alpha X_0,\mathbf p)< \frac{1}{\alpha  }L(\alpha X_0,\mathbf p) + \frac{\alpha -1}{\alpha }L(0,\mathbf p) \leq L(X_0,\mathbf p) = X_0.\;\forall \alpha > 1 \vspace{-0.15 cm}
\end{displaymath}
As a result, $L(\alpha X_0,\mathbf p) < \alpha X_0$ and, exploiting the monotonicity of $L(X,\mathbf p)$, the following inequality holds \vspace{-0.25 cm}
\begin{displaymath}
 0 < L^{(k+1)}(\alpha X_0,\mathbf p) < L^{(k)}(\alpha X_0,\mathbf p). \vspace{-0.25 cm}
\end{displaymath}
Then $L^{(k)}(\alpha X_0,\mathbf p)$ is bounded regardless of $k$. Because $X_0 > 0$ for any $X$ positive semidefinite, there exists a scalar $\alpha_x>1$, such that $X \leq \alpha _x X_0$, then, using again the monotonicity of $L(X,\mathbf p)$, one can prove that $ L^{(k)}(X,\mathbf p) < L^{(k)}(\alpha_x X_0,\mathbf p)$ is also bounded regardless of $k$. Hence, the pair $(C_{\mathbf p}, A)$ must be detectable, which implies that $L^\infty(X,\mathbf p)$ exists for all $X$. Moreover, since $Q > 0$, the limit is unique and it must be $X_0$.
\end{proof}
In the latter theorems we proved that, under some mild assumption, $L^{\infty}(X,\mathbf p)$ exists and is unique. Under those assumptions the problem of finding the optimal fixed marginal distribution $\mathbf p$ that minimizes the asymptotic lower bound can be formulated as
\vspace{-0.35 cm}
\begin{prob}[Asymptotic Lower Bound for Random Transmission Tree Selection]\label{randoptasymptoticlower}
\begin{align*}
  &\mathop{\textrm{mininize}}\limits_{\mathbf p \in \mathbb R^m}&
  &trace(L^\infty(\Sigma,\mathbf p)) \\[-0.4 cm]
  &\textrm{subject to}&
  &\mathbf p \in \mathcal P.
\end{align*}
\end{prob}
\vspace{-0.3 cm}
where $\mathcal P$ is defined in (\ref{eq:treecondition}).
\\
One of the main difficulties to solve the above problem is that $L^\infty(X,\mathbf p)$ is in general not convex in $\mathbf p$. Moreover, the exact form of $L^\infty(X,\mathbf p)$ is unknown. To overcome those limitations, we propose the following algorithm:
\begin{enumerate}
  \item Define
     $ \mathbf p_0 =  \left({\mathcal E_d}/{\left(\sum_{i=1}^m c_i\right)}\right) \mathbf 1_m,
    $
    where $\mathbf 1_m \in \mathbb R^m$ is a vector with all one entries and choose the matrix
    $  L_0 = L^{\infty}(I_n,\mathbf p_0).
    $ \vspace{-0.15 cm}
  \item Let $L_k$ and $\mathbf p_k$ be the solution of the following optimization problem
\begin{prob}[Random Sensor Selection with Descend Constraint]\label{greedylower}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_k \in \mathbb R^m}&
  &trace(L_k)(=trace(L(L_{k-1},\mathbf p_k)))\\[-0.3 cm]
  &\textrm{subject to}&
  & L_k \leq L_{k-1},\\[-0.3 cm]
  &&&\mathbf p_k \in \mathcal P.
\end{align*}
\end{prob} \vspace{-0.25 cm}
  \item Choose $\mathbf p^*$ as an accumulation point of $\mathbf p_k$\footnote{An accumulation point of a sequence is the limit of a converging subsequence}. Then $L^{\infty}(X, \mathbf p^*) = \lim_{k\rightarrow \infty} L_k$ for any positive semidefinite matrix $X$. \vspace{-0.05 cm}
\end{enumerate}
Before proving the feasibility of the above algorithm, we want to make a few comments. First it is worth to notice that the optimization Problem~\ref{greedylower} is different from Problem~\ref{randoptlowerfinal} because we require $L_k \leq L_{k-1}$, i.e. $\{L_k\}$ is a descending sequence. Also it is worth pointing out that our algorithm is greedy. In fact, we try to minimize the lower bound for the next step in the hope of reducing the final asymptotic lower bound. As a result, it is suboptimal by nature. The following theorem gives a characterization of the main features of the proposed algorithm.
%
\vspace{-0.2 cm}
\begin{theorem}
The following statements are true for the proposed algorithm:
\begin{enumerate}
  \item $L_0$ exists. \vspace{-0.1 cm}
  \item Problem~\ref{greedylower} is always feasible. \vspace{-0.1 cm}
  \item $\mathbf p^*$ exists and $\mathbf p^*\in\mathcal P$. \vspace{-0.1 cm}
\item $L_\infty = \lim_{k\rightarrow \infty}L_k$ exists. \vspace{-0.1 cm}
\item $L_\infty = L^{\infty}(X,\mathbf p^*)$ for all positive semidefinite $X$. \vspace{-0.1 cm}
\end{enumerate}
\end{theorem}
\vspace{-0.15 cm}
\begin{proof}
  \begin{enumerate}
    \item It is easy to check that $C_{\mathbf p_0} =\sqrt{ \mathcal E_d /(\sum_{i=1}^m c_i)}C$ and $\mathbf p_0\in \mathcal P$. Since $(C,\,A)$ is detectable, $(\sqrt{ \mathcal E_d /(\sum_{i=1}^m c_i)}C,A)$ is also detectable and then $L_0$ exists. \vspace{-0.10 cm}
    \item Suppose that the Problem~\ref{greedylower} is feasible up to time $k$. To prove the problem is also feasible at time $k+1$, we only need to find one $\mathbf p\in \mathcal P$ and $L(L_k,\mathbf p)\leq L_k$. If we choose $\mathbf p = \mathbf p_k$ then, becasue $\mathbf p_k$ is the solution at time $k$, it follows that $\mathbf p_k\in\mathcal P$. It remains to prove that $L(L_k,\mathbf p_k)\leq L_k$, which can be proved by noticing that $L_k = L(L_{k-1},\mathbf p_k)  \leq L_{k-1}$ and $L(X,\mathbf p)$ is monotonically increasing with respect to $X$.
      Similarly, Problem~\ref{greedylower} is also feasible at time $1$ and then, by induction, Problem~\ref{greedylower} is always feasible. \vspace{-0.10 cm}
\item It is easy to see that $\mathbf p_k$ is bounded because $p_{k,i}\in [0,1]$. By means of the Bolzano-Weierstrass Theorem, this implies that there always exists an accumulation point $\mathbf p^*$. Moreover, because $\mathbf p_k\in\mathcal P$ and $\mathcal P$ is closed, $\mathbf p^*\in \mathcal P$. \vspace{-0.10 cm}
\item Because $\{L_k\}$ is decreasing and $L_k \geq 0$ for all $k$, the limit must exists. \vspace{-0.10 cm}
\item By the definition of accumulation point, there is a subsequence $\mathbf p_{i_1} ,\mathbf p_{i_2},\ldots$ which converges to $\mathbf p^*$. For each index $i_k$ we have \vspace{-0.25 cm}
  \begin{displaymath}
    L(L_{i_k-1},\mathbf p_{i_k}) = L_{i_k}. \vspace{-0.25 cm}
  \end{displaymath}
  If we take the limit on both side and exploit the fact that $L(X,\mathbf p)$ is continuous, we obtain \vspace{-0.25 cm}
   \begin{displaymath}
   L(L_\infty,\mathbf p^*) = L_\infty, \vspace{-0.25 cm}
  \end{displaymath}
  and finally by Theorem~\ref{theorem:existence2}, the limit is unique. \vspace{-0.20 cm}
  \end{enumerate}
\end{proof}
\vspace{-0.20 cm}
Finally, it is worth to remark again that our algorithm has a greedy nature and in general represents a suboptimal solution to Problem~\ref{randoptasymptoticlower}. However, in the following theorem we prove that, at least in the particular case of scalar states, the greedy approach finds the optimal solution for Problem~\ref{randoptasymptoticlower}:
\vspace{-0.25 cm}
\begin{theorem}
  Assume that $x_k\in\mathbb R^n$ and $n = 1$. Then, the optimal solution $\mathbf p^*$ obtained by means of the proposed algorithm is the optimal solution for Problem~\ref{randoptasymptoticlower}.
\end{theorem}
\begin{proof}
Suppose that the optimal solution for Problem~\ref{randoptasymptoticlower} is $\mathbf p_{opt}$. Because $L^\infty (\Sigma, \mathbf p_{opt})$ is a fixed point for iteration $L(X,\mathbf p_{opt})$, by Theorem~\ref{theorem:existence2}, the limit is unique and then \vspace{-0.25 cm}
  \begin{displaymath}
    L^{\infty}(L_0,\mathbf p_{opt}) = \lim_{k\rightarrow\infty} L^{(k)}(L_0,\mathbf p_{opt}). \vspace{-0.25 cm}
  \end{displaymath}
  Because $n = 1$, we can simplify Problem~\ref{greedylower} as follows \vspace{-0.25 cm}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_k \in \mathbb R^m}&
  &L_k \\[-0.2 cm]
  &\textrm{subject to}&
  &L_k = L(L_{k-1},\mathbf p_k),\,\mathbf p_k\in\mathcal P, \vspace{-0.25 cm}
\end{align*}
which proves that $L_1 \leq L^{(1)}(L_0,\mathbf p_{opt})$. Similarly, it is also easy to prove that \vspace{-0.25 cm}
  \begin{displaymath}
    L^{(k)}(L_0,\mathbf p_{opt}) \geq L_k, \forall k. \vspace{-0.25 cm}
  \end{displaymath}
   If we take the limit on both sides, we obtain \vspace{-0.25 cm}
  \begin{displaymath}
    L^{\infty}(L_0,\mathbf p_{opt}) \geq \lim_{k\rightarrow\infty} L_k = L^{\infty}(L_0,\mathbf p^*), \vspace{-0.25 cm}
  \end{displaymath}
  and by the uniqueness of the limit we have \vspace{-0.25 cm}
  \begin{displaymath}
   L^{\infty}(\Sigma,\mathbf p_{opt}) =  L^{\infty}(L_0,\mathbf p_{opt}) \geq L^{\infty}(L_0,\mathbf p^*) =   L^{\infty}(\Sigma,\mathbf p^*). \vspace{-0.25 cm}
  \end{displaymath}
  Finally, because of the optimality of $\mathbf p_{opt}$, the left side must be equal to the right side, which proves that $\mathbf p^*$ is optimal.
\end{proof}

Before continue on to the next section, we would like to summarize all the optimization problems we have proposed so far. Our ultimate goal is to solve Problem~\ref{optdynnstep}. However, due to the discrete nature of the search space, it is intractable to find the true optimal solution. As a result, two heuristic, Problem~\ref{opt_dyn} and \ref{detoptasymptotic} are often considered in the literature. In Section~\ref{sec:egrodic} we propose Problem~\ref{randopt} and \ref{randoptasymptotic} as the stochastic counterparts of Problem~\ref{opt_dyn} and \ref{detoptasymptotic} respectively and prove that the stochastic formulation has several unique advantages over the deterministic one. To render the stochastic formulation solvable, we relax Problem~\ref{randopt} to Problem~\ref{randoptlower} and \ref{randoptlowerfinal} into Section~\ref{sec:main} and Problem~\ref{randoptasymptotic} to Problem~\ref{randoptasymptoticlower} and \ref{greedylower} into Section~\ref{sec:extension}.
\vspace{-0.15 cm}
