In this section we consider a convex relaxation of Problem~\ref{randopt}. To this end, let us define an upper bound $U_k$ and a lower bound $L_k$ to $\mathbb EP_k$ by means of the following theorems, whose proofs are reported in the Appendix.
\begin{theorem}\label{theorem:lowerbound}
  Let $L_0 =  P_0$ and
\vspace{-0.25 cm}
  \begin{equation}
	  L_k = \left(L_{k|k-1} ^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_iC_i'}{r_i}\right)^{-1},
	  \label{eq:lowerbound1} \vspace{-0.25 cm}
  \end{equation}
  where \vspace{-0.25 cm}
  \begin{equation}
    L_{k|k-1} = AL_{k-1}A' + Q.
	  \label{eq:lowerbound2} \vspace{-0.25 cm}
  \end{equation}
  The following inequalities hold: \vspace{-0.25 cm}
  \begin{equation}
    L_k^{-1} \geq \mathbb EZ_k,\, \mathbb EP_k \geq (\mathbb EZ_k)^{-1} \geq L_k. \vspace{-0.25 cm}
  \end{equation}
\end{theorem}
\vspace{-0.3 cm}
\begin{theorem}\label{theorem:upperbound}
  Let $U_0 = P_0$ and \vspace{-0.25 cm}
  \begin{equation}
    \begin{split}
    U_k& = U_{k|k-1}  - \sum_{T\in\mathcal T}\pi_{k,T}  U_{k|k-1}C_T'(C_TU_{k|k-1}C_T'+R_T)^{-1}C_TU_{k|k-1},
    \end{split}
\vspace{-0.25 cm}
  \end{equation}
  where \vspace{-0.25 cm}
  \begin{equation}
    U_{k|k-1} = AU_{k-1}A'+Q. \vspace{-0.25 cm}
  \end{equation}
  The following inequality holds: \vspace{-0.25 cm}
  \begin{equation}
  U_k \geq \mathbb EP_k. \vspace{-0.25 cm}
  \end{equation}
\end{theorem}
\vspace{-0.3 cm}
\noindent To further improve the legibility, let us define the function \vspace{-0.10 cm}
\begin{equation}
  L(X,\mathbf p) \triangleq \left[(AXA' + Q)^{-1} + \sum_{i=1}^m p_{i} \frac{C_iC_i'}{r_i}\right]^{-1}, \vspace{-0.10 cm}
\end{equation}
where $X \in \mathbb R^{n\times n}$ is positive semidefinite and $\mathbf p = [p_1,\ldots,p_m]'\in \mathbb R^m$. Hence \eqref{eq:lowerbound1} and \eqref{eq:lowerbound2} can be simplified as \vspace{-0.25 cm}
\begin{equation}
  L_k = L(L_{k-1}, \mathbf p_k). \vspace{-0.25 cm}
  \label{eq:lowerbound3}
\end{equation}
%
The proof of the following theorem can be also found in the Appendix.
\vspace{-0.3 cm}
\begin{theorem}
  \label{theorem:lfunction}
$L(X,\mathbf p)$ is convex with respect to $\mathbf p$ and it is concave and monotonically increasing with respect to $X$.
\end{theorem}
\vspace{-0.3 cm}
\noindent Compared to $\mathbb EP_k$, matrices $U_k$ and $L_k$ are deterministic and explicit functions of $\pi_{k,T}$.  Moreover, while $U_k$ depends on $\pi_{k,T}$, the number of which may be not polynomial in the number of sensors, $L_k$ only depends on the marginal probabilities $p_{k,i}, i=1,...,m$.
%
For such a reason, in order to relax Problem~\ref{randopt}, we approximate the trace $\mathbb EP_k$ with the trace of its lower-bound $L_k$. Then, the following optimization problem is obtained:
\vspace{-0.30 cm}
\begin{prob}[Lower Bound for Random Transmission Tree Selection]\label{randoptlower}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\pi_{k,T},\,\mathbf p_{k}}&
  &trace(L_k)(=trace(L(L_{k-1},\mathbf p_k))) \\[-0.2 cm]
  &\textrm{subject to}&
  &\sum_{T\in \mathcal T} \pi_{k,T}\mathcal E(T)\leq\mathcal E_d, \\[-0.2 cm]
  &&&\pi_{k,T}\geq 0,\,\sum_{T\in\mathcal T} \pi_{k,T} = 1,\\[-0.2 cm]
  &&&p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T}.
\end{align*}
\end{prob}
\vspace{-0.4 cm}
\begin{remark}
It is worth to remark that once we have an optimal solution to the above problem we can easily compute the upper bound $U_k$ to have a measure of the tightness of the solution.
\end{remark}
\vspace{-0.25 cm}
\noindent The only drawback of the above formulation is that the optimization still has a number of constraints and variables depending on $|\mathcal T|$, a number which is not, in the general case, polynomial with respect to $m$. Interestingly enough, such a dependence can be easily removed. To this end, let us define the set of feasible $\mathbf p$ for Problem \ref{randoptlower}:
\begin{equation}
  \begin{split}
  &\mathcal P \triangleq \left\{\mathbf p\in \mathbb R^m\left|\exists \pi,\, \sum_{T\in \mathcal T} \pi_{T}\mathcal E(T)\leq\mathcal E_d,\, \pi_{T}\geq 0,\,\sum_{T\in\mathcal T} \pi_{T} = 1,\, p_{i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{T}\right.\right\}.
  \end{split}
  \label{eq:feasibleset}
\end{equation}
%
The following results can be easily proved:
\vspace{-0.35 cm}
\begin{proposition}
  The energy cost of a given collection of tree selection probabilities $\pi_{k,T}, \forall T \in {\mathcal{T}}$ is a linear function of the resulting marginal probability: \vspace{-0.10 cm}
  \begin{equation}
    \sum_{T \in \mathcal T} \pi_{k,T} \mathcal E(T)= \sum_{i = 1}^m c_i p_{k,i}. \vspace{-0.25 cm}
  \end{equation}
  \label{proposition:energy}
\end{proposition}
\vspace{-1.1 cm}
\begin{proposition}
  \label{proposition:existence}
  If $p_{k,i}\in [0,1]$ and if it satisfies \vspace{-0.25 cm}
  \begin{equation}
    p_{k,i} \leq  p_{k,j},\qquad \textrm{if $j$ is a parent of $i$}
  \label{eq:descritpion1} \vspace{-0.25 cm}
  \end{equation}
  then there exists at least one collection of tree selection probabilities $\pi_{k}$\footnote{It is worth noticing that in general it may exist more the one set of $\pi_{k,T}, \forall T \in \mathcal{T} $ with the same marginal probabilities.}, such that \vspace{-0.15 cm}
  \begin{equation}
  \pi_{k,T}\geq 0,\; \sum_{T\in\mathcal T} \pi_{k,T} = 1,\; p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T}.
  \label{eq:descritpion2} \vspace{-0.15 cm}
  \end{equation}
  Conversely, if there exists  $\pi_k$ such that \eqref{eq:descritpion2} holds, then  $p_{k,i}\in [0,1]$ and satisfies \eqref{eq:descritpion1} .
\end{proposition}
\vspace{-0.35 cm}
\noindent By exploiting the above Propositions we can reformulate the feasible set (\ref{eq:feasibleset}) as follows \vspace{-0.05 cm}
\begin{equation}
  \mathcal P =\left\{\mathbf p\left|p_i\in[0,\,1],\,\sum_{i = 1}^m c_i p_{i}\leq \mathcal E_d,\,  p_{i} \leq  p_{j},\textrm{if $j$ is parent of $i$}\right. \right\},
  \label{eq:treecondition} \vspace{-0.05 cm}
\end{equation}
and finally we can rewrite Problem~\ref{randoptlower} as
\vspace{-0.3 cm}
\begin{prob}[Lower Bound for Random Transmission Tree Selection]\label{randoptlowerfinal}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_{k}}&
  &trace(L_k) \left(= trace (L(L_{k-1},\mathbf p_k)) \right) \\[-0.4 cm]
  &\textrm{subject to}&
  &\mathbf p_{k} \in \mathcal P.
\end{align*}
\end{prob}
\vspace{-0.2 cm}
\noindent  Due to the convexity of $L$ and $\mathcal P$, Problem~\ref{randoptlowerfinal} is a convex optimization problem with $O(m)$ optimization variables and  $O(m)$ constraints. Thus, it can be solved efficiently. For example, if interior-points methods is used, then the complexity is $O(m^3)$. For detailed discussions about the computational burdens, please refer to \cite{Joshi}.
