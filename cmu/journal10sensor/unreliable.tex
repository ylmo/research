In all previous developments  we implicitly assumed that, when a sensor sends a packet to a parent node, the packet will be always reliably received. Such an assumption is not realistic in many applications because the probability that a packet can be lost or significantly delayed is often not negligible. The unreliability of communication channels often places a great challenge for researchers. On the contrary, in this section we will show that the presence of unreliable channels can be easily incorporated into our framework with only a few modifications.

First, let us assume that all channels in the network are erasure channels. In other words, a packet can be either successfully delivered or dropped. The assumption of erasure channels enables us to define binary random variable $\eta_{k,e}$ to indicate the status of the channel, where $e$ belongs to the edge set $E$. In particular, $\eta_{k,e}$ is $1$ if and only if the communication is possible through the edge $e$ at time $k$. Note that even if $\eta_{k,e} = 1$, the edge may still be inactive due to the sensor schedule. If the node $i$ is selected at time $k$, then the probability that the fusion center will correctly receive observation $i$ is given by \vspace{-0.00 cm}
\begin{displaymath}
  \rho_{k,i} = P\left(\prod_{e\in path(i,0)}\eta_{k,e} = 1\right) \vspace{-0.10 cm}
\end{displaymath}
where $path(i,0)$ is the set of all edges on the path from sensor $i$ to the fusion center.
%
We further assume that $\rho_{k,i}$ is constant over time. Therefore, $\rho_{k,i}$ can be simplified as $\rho_i$.
%
%Hereafter we assume the channel is memoryless, which means that $\gamma_{i,e_\alpha}$ and $\gamma_{j,e_\beta}$ are %independent if $i\neq j$. It is worth noticing that we do not require $\gamma_{k,e_\alpha}$ and $\gamma_{k,e_\beta}$ %to be independent. In other words, in one sampling period the channel failure may be correlated.
%
Under such an assumption, the information filter equation can be rewritten as \vspace{-0.20 cm}
\begin{displaymath}
  Z_k  = Z_{k|k-1} + \sum_{i=1}^m \prod_{e\in path(i,0)} \eta_{k,e}\gamma_{k,i} \frac{C_i'C_i}{r_i} \vspace{-0.20 cm}
\end{displaymath}
where $\gamma_{k,i}$ is the binary random variable that indicates whether sensor $i$ is selected at time $k$, and \vspace{-0.20 cm}
\begin{displaymath}
  Z_{k|k-1} =(AZ_{k-1}^{-1}A' + Q)^{-1}. \vspace{-0.20 cm}
\end{displaymath}
%
\noindent Because channel failure $\eta_{k,e}$ is independent of the sensor schedule $\gamma_{k,i}$, the lower bound in the case of unreliable channel is \vspace{-0.20 cm}
\[
  L_k = L(L_{k-1}, [p_{k,1}\rho_1,\ldots,p_{k,m}\rho_m]' ),\,L_0 = \Sigma.
\]
\vspace{-0.20 cm}
\noindent and then the optimal sensor selection problem in lossy networks can be formulated as
\vspace{-0.1 cm}
\begin{prob}[Lower Bound for Random Transmission Tree Selection with Packet Losses]\label{randoptlowerpackloss}
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{\mathbf p_{k}}&
  &trace(L_k) \\[-0.30 cm]
  &\textrm{subject to}&
  &L_k = L(L_{k-1}, [p_{k,1}\rho_{1},\ldots,p_{k,m}\rho_{m}]^T). \\[-0.30 cm]
  &&&\mathbf p_k \in \mathcal P.
\end{align*}
\end{prob}
\vspace{-0.35 cm}
%\begin{remark}
%Please note that packet losses only change the objective function of Problem \ref{randoptlowerfinal} but does not interfere with constraints.
%%  The stochastic sensor selection strategy can easily take into account of random channel failure with only small modification, which shows another advantage over the deterministic sensor scheduling.
%\end{remark}
