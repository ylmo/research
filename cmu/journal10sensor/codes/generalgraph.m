clc
clear

%generate network graph
gridlength = 4;
n = gridlength^2 ;
m = 16;

%generate A matrix for heat transfer system
coef = 0.1;
A = eye(n);
for i = 1:gridlength
    for j = 1:gridlength
        k = (i-1)*gridlength + j;
        if i >= 2
            l = (i-2)*gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if i <= gridlength - 1
            l = i * gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j >= 2
            l = (i-1)*gridlength + j - 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j <= gridlength - 1
            l = (i-1)*gridlength + j + 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
    end
end

rand('twister',sum(clock)*100)

xaxis =[1,2,3,4,1,2,3,4,1,2,3,4,1,2,3,4];
yaxis =[1,1,1,1,2,2,2,2,3,3,3,3,4,4,4,4];
xaxis(1) = 1;
yaxis(1) = 1;
graph = zeros(m);
for i = 1:m
    for j = 1:m
        graph(i,j) = (xaxis(i)-xaxis(j))^2 + (yaxis(i)-yaxis(j))^2;
    end
end
baseenergy = 1;
graph = graph + baseenergy;

%generate C matrix
%     C = zeros(m,n);
%     for i = 1:m
%         x = xaxis(i);
%         y = yaxis(i);
%         xgrid = floor(x);
%         ygrid = floor(y);
%         for j = 0:1
%             for k = 0:1
%                 l = (xgrid+j-1) * gridlength + ygrid + k;
%                 C(i,l) = (1-abs(xgrid+j-x))*(1-abs(ygrid+k-y));
%             end
%         end
%     end
C = eye(m);

%generate noise
Q = eye(n);
R = eye(m);
Sigma = 4*eye(n);
threshold = 6;
%generate graph


% MST = graphminspantree(sparse(graph));
%
% [dist path parent] = graphshortestpath(MST+MST',1,'directed',false);
%
% cost = zeros(1,m);
% for i = 2:m
%     cost(1,i) = graph(i,parent(i));
% end

for k = 1:10
    MST = graphminspantree(sparse(graph+rand(m)));
    [dist path parent(k,:)] = graphshortestpath(MST+MST',1,'directed',false);
    for i = 2:m
        cost(k,i) = graph(i,parent(k,i));
    end
end

%enumerate feasible det schedule
tic
j = 1;
enu = {};
for i = 1:2^15-1;
    index = 1;
    tmpi = i;
    for k = 2:15
        if mod(tmpi,2) == 1
            index = [index k];
            tmpi = (tmpi-1)/2;
        else
            tmpi = tmpi/2;
        end
    end
    subgraph = graph(index,index);
    
    tmpcost = sum(sum(graphminspantree(sparse(subgraph))));
    if tmpcost <= threshold
        enu{j} = index;
        j = j+1;
    end
end
t1=toc

L = Sigma;
P = Sigma;

optrnd = {};
optdet = {};

for hor = 1:10
    %lower bound
%     L = Sigma;
%     Ltmp = A*L*A' + Q;
%     Ztmp = inv(Ltmp);
%     cvx_begin
%     cvx_solver sdpt3
%     cvx_quiet(true)
%     variable p(1,m);
%     variable q(m,m,m);
%     variable r(m,m);
%     variable Z(n,n) symmetric;
%     minimize (trace_inv(Z));
%     subject to
%     Z == semidefinite(n);
%     Z == Ztmp + C'*diag(p)/R*C;
%     for i = 1:m
%         for j=1:m
%             for l=1:m
%                 q(i,j,l) <= q(j,j,l);
%                 q(i,j,l) >= 0;
%                 q(i,j,l) <= 1;
%             end
%             q(i,j,i) == 0;
%             r(i,j) == q(i,i,j);
%         end
%         if i ~= 1
%             sum(q(i,i,:)) == p(i);
%         end
%     end
%     sum(sum(r.*graph)) <= threshold;
%     for i = 1:m,
%         p(i) >= 0;
%         p(i) <= 1;
%     end
%     cvx_end
%     
%     Llower = inv(Z);
%     tracedata(exp,1)=trace(Llower)

    %optimization

    Ltmp = A*L*A'+Q;
    Ztmp = Ltmp^(-1);
    tic
    cvx_begin
    cvx_solver sdpt3
    %cvx_quiet(true)
    variable p(1,m);
    variable q(10,m);
    variable Z(n,n) symmetric;
    minimize (trace_inv(Z));
    subject to
    Z == semidefinite(n);
    Z == Ztmp + C'*diag(p)/R*C;
    for i = 1:m
        p(i) <= 1;
        for j = 1:10
            q(j,i) >= 0;
            if i~= 1
                q(j,i) <= q(j,parent(j,i));
            end
        end
        sum(q(:,i)) == p(i);
    end
    sum(sum(q.*cost(1:10,:))) <= threshold;
    cvx_end
    t2=toc
    L = inv(Z);
    optrnd(hor)={q};

    
    %deterministic optimal
    Ptmp = A*P*A' + Q;
    Ztmp = inv(Ptmp);
    opttrace = inf;
    for index = enu
        gamma = zeros(1,16);
        gamma(index{1}) = 1;
        Ztmp2 = Ztmp + C'*diag(gamma)/R*C;
        Ptmp2 = inv(Ztmp2);
        if trace(Ptmp2)< opttrace
            opttrace = trace(Ptmp2);
            optindex = index;
        end
    end
    gamma = zeros(1,16);
    gamma(optindex{1}) = 1;
    Z = Ztmp + C'*diag(gamma)/R*C;
    P = inv(Z);
    optdet(hor)=optindex;
    optdettrace(hor)=opttrace;
end

optrndtrace=zeros(1,10);
for exp = 1:10000
    P = Sigma;
    for hor = 1:10
        Ptmp = A*P*A' + Q;
        Ztmp = inv(Ptmp);
        
        q = optrnd{hor};
        p = zeros(1,10);
        for i = 1:10
            p(i) = sum(q(1:i,1));
        end
        tree = min(find(p >= rand()));
        
        qtmp = q(tree,:);
        qtmp = qtmp/qtmp(1);
        
        index = find(qtmp >= rand());
        
        gamma = zeros(1,16);
        gamma(index) = 1;
        Z = Ztmp + C'*diag(gamma)/R*C;
        P = inv(Z);
        optrndtrace(hor)=optrndtrace(hor)+trace(P);
    end
end
optrndtrace = optrndtrace/10000;