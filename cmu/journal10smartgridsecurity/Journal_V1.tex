\documentclass[journal]{IEEEtran}

\usepackage{cite}
\usepackage{amssymb, amsmath, graphicx}
\usepackage{algorithm}%
\newtheorem{defn}{Defnition}
\newtheorem{theorem}{Theorem}
\newtheorem{lemma}{Lemma}
\newtheorem{remarks}{Remarks}

\hyphenation{op-tical net-works semi-conduc-tor}

\begin{document}
\title{Malicious Data Attacks in Power Market Operations}

\author{Le Xie,~\IEEEmembership{Member,~IEEE},~Yilin Mo,~\IEEEmembership{Student Member,~IEEE,}~ Bruno Sinopoli,~\IEEEmembership
{Member,~IEEE}
\thanks{This work was supported in part by Texas Engineering Experiment Station, and in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office.}
\thanks{Le Xie is with the Departments
of Electrical and Computer Engineering, Texas A\&M University, College Station, TX 77843 USA email: lxie@mail.ece.tamu.edu}
\thanks{Yilin Mo and Bruno Sinopoli are with the Department
of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA 15213 USA e-mail: {ymo}@andrew.cmu.edu, {brunos}@ece.cmu.edu}%
}
 \maketitle

\begin{abstract}
We study the impact of a potential class of malicious cyber attack, named \emph{false data injection attack}, on electric power market operations. In particular, we show that with the knowledge of the power transmission system topology, attackers may circumvent the bad data detection algorithms equipped in system operators' state estimation functions, and lead to profitable financial misconducts such as virtual bidding. From the attackers perspective, the profit maximization by compromising limited number of measurement sensors is formulated. From the system operators' perspective, the potential economic loss due to such a class of cyber attacks is examined. We illustrate the potential economic impact of such a class of cyber attacks on an IEEE 14-bus system.
\end{abstract}

\section{Introduction}
\label{introduction}

The electric power industry is undergoing profound changes as our society increasingly emphasizes the importance of a smarter grid in support of sustainable energy utilization. Technically, enabled by the advances in sensing, communication, and actuation, power system operations are likely to involve many more real-time information gathering and processing devices such as Phasor Measurement Units (PMUs) \cite{Amin}. Institutionally, the increasing presence of demand response programs may open the door to more integrated SCADA network and end-user networks \cite{Wu_ControlCenter}. Given the stronger coupling between cyber components (sensors and communication networks, in particular) and physical operations in power systems, smart grid of the future must cope with a variety of anomalies in this cyber-physical system.

Furthermore, the deregulation of electric power industry has unbundled the generation, transmission and distribution. In most regions, the operation of the wholesale level electricity markets and the underlying physical power systems are organized in Regional Transmission Organizations (RTOs) such as Independent System Operators (ISO) New England , Pennsylvania-New Jersey-Maryland (PJM) and California Independent System Operator (CAISO). Market operations have become an important aspect of RTOs' responsibilities in addition to engineering physically reliable electricity services. Market participants such as independent power producers (IPPs), load serving entities (LSEs), and virtual traders submit their bids and offers to a multi-time-scale power markets such as day-ahead and real-time markets. The market operations are performed using software packages named as Market Management System (MMS). The real-time balancing electricity market operation relies on faithful characterization of the actual power system status. In order to ensure that real-time market operations are aligned with underlying physical states of the power system, MMS obtains the physical information of the electric power grid from another software Energy Management System (EMS), which is routinely used in the control centers.

As more and more advanced cyber technologies are getting integrated into the EMS, potential cyber-security threats are becoming an increasing concern for RTOs. One of the key functions in EMS is the state estimation\cite{Scheweppe}, which provides robust estimation of the entire power grid's real-time information according to remote field sensor measurements. The estimated physical states in the system are then passed to many operating decision tools and real-time market pricing models. In \cite{Liu2009} the possibility of false data injection attack against power grid state estimation was first conceived. By leveraging the knowledge of the power network topology, it was shown that  false data injection attack could bypass the bad data detection in today's Supervisory Control and Data Acquisition (SCADA) system. In \cite{Sandberg} two possible indices are proposed in order to quantify required efforts to implement such a class of attack. As a function of the system topology, the indices could reveal the least effort needed
to achieve attack goals while avoiding bad-data alarms in the control center. In \cite{Kosut1} and \cite{Kosut2} efforts have been made to develop computationally efficient heuristics to detect these false data attacks against state estimators. However, the potential risks and costs of such a class of cyber attack are not well understood yet. In \cite{Gross2009} a four-layer conceptual framework is proposed to quantify potential impact of cyber attacks in deregulated electricity markets.


While most literature focus on the physical impact of cyber attacks to the power system, this paper presents a novel integrated study which analyzes the economic impact of such a class of cyber attacks. In particular, this paper shows the potential profitable financial miscouducts that one may benefit from malicious data injection attacks. In addition to the catastrophic electric power system blackout scenarios that cyber attacks can provoke, the proposed scheme results more undetectable, consistent financial losses to the social welfare. The objective of this paper is to reveal such potential risks, and to propose a starting formulation to analyze and prevent such a class of cyber attacks. An interdisciplinary approach with power, control, and communication aspects would lead to important findings to prevent such cyber attacks from happening in the future. In summary, the main contributions of this paper are threefold:
\begin{itemize}
\item We formulate the problem of the data injection attack against state estimation in deregulated electricity markets, which leads to
financial misconduct.
\item We provided a heuristic for finding profitable attack by compromising limited sensors, which can be formulated as a convex optimization problem.
\item We analyzed the economic impact of such a class of attacks on real-time electricity market operations.
\end{itemize}

The rest of this paper is organized as follows. Section \ref{backgrouds} provides basic background of how electricity markets are operated in major RTOs. The possible false data injection attack model is then elaborated in Section \ref{attacks}. In Section \ref{MainResults} we present from the attacker's perspective how a false virtual bidding could be performed in conjunction with a false data injection attack. Furthermore in Section \ref{Limited} we analyze the optimal attack strategy by compromising limited number of measurement sensors. Numerical example in an IEEE standard system and discussions are presented in Section \ref{examples}. Concluding remarks and future work are drawn in Section \ref{conclusion}.


\section{Preliminaries}
\label{backgrouds}

In deregulated electricity markets, the nodal price of electric energy is determined at the Regional Transmission Organizations (RTOs). The electric energy market consists of several look-ahead forward markets and real-time spot market. In the real-time spot market, the Market Operations will calculate the ex-post locational marginal price (LMP) based on the actual state estimation from the SCADA system, the results of which will be the final settlement price. In this section we will briefly introduce state estimation algorithm in power grid and how it will affect the ex-post market.

\subsection{Notations}
We first summarize the notations which will be used throughout this paper in Table~\ref{tab:notations}.  We will also use superscript to indicate the contexts of variables. For example $Pg_i^*$ denotes the optimal generation power at bus $i$ given by the Ex-Ante Solution. $Pg_i$ denotes the real time generation power and $\hat Pg_i$ is the estimated real time generation power.
\begin{table}[h]
\centering \caption{Notations} \label{tab:notations} \centering
\begin{tabular}
{|c|l|} \hline $i$ & Index for generators $i$ \\
      \hline $j$ & Index for load buses $j$ \\
      \hline $l$ & Index for transmission line $l$ \\
      \hline $k$ & Time $k$ \\
      \hline $I$ & Total number of generators \\
      \hline $J$ & Total number of load buses \\
      \hline $L$ & Total number of transmission lines\\
      \hline $Ld_j$ & Load at bus $j$ during run time \\
      \hline $Pg_i$ & Generation at $i$ during run time\\
      \hline $x$ & A vector consists of all $Pg_i$ and $Ld_j$\\
      \hline $z$ & Collection of sensor measurements\\
      \hline $C_i(Pg_{i})$ & Generation cost of producing $Pg_i$\\
      \hline $Pg_i^{min(max)}$ & Minimum (maximum) available power from generator $i$ \\
      \hline $\lambda_{i}$ & Electricity price at bus $i$ \\
      \hline $F_l$ & Transmission flow at line $l$\\
      \hline $F_l^{max}$ & Maximum allowed transmission flow at line $l$ \\
      \hline $F_l^{min}$ & Minimum allowed Transmission flow at line $l$ \\
\hline
\end{tabular}
\end{table}

\subsection{Ex-Ante Market}
The Ex-Ante market is used to compute optimal generation power $Pg_i^*$ with minimum total cost for each generator given the predicted load $Ld_j^*$. Moreover, the optimal solution needs to satisfy certain safety and operation constraints. First, due to the inertia of generator, $Pg_i^*$ cannot deviate too large from the current generation power. Thus, the constraints can be expressed as
\begin{displaymath}
 Pg_i^{min}\leq  Pg_i^* \leq Pg_i^{max}, \; \forall i = 1,\ldots,I \\
\end{displaymath}
where $Pg_i^{min},\,Pg_i^{max}$ are functions of the current generation power and ramping rate of the generator. Second, the power flow for each transmission line must remain within the capacity, which implies that
\begin{displaymath}
F_l^{min} \leq F_l^* \leq F_l^{max},\;\forall l =1,\ldots,L.\\
\end{displaymath}
Based on the linearized DC-power flow model, the line flow vector is a linear function of nodal injection vector:
\begin{eqnarray}
  \label{eq:systemmodel}
  F=H\begin{bmatrix}Ld\\ Pg\end{bmatrix}
\end{eqnarray}
where $H$ is the distribution factor matrix of the nodal injection vector. For future analysis, we would like to define the $j$th column of $H$ to be $H_j$.

By all the previous argument, the Ex-Ante market will try to solve the following optimization problem

\textbf{Ex-Ante Formulation}:
\begin{align*}
  &\mathop {\textrm{minimize}}\limits_{Pg_i^*}&
&\sum\limits_{i=1}^{I}C_i(Pg_{i}^*)&
&\\
&\textrm{subject to}&
&\sum_{i=1}^{I}Pg_{i}^*=\sum_{j=1}^{J}Ld_{j}^*&
&\\
&&& Pg_i^{min}\leq  Pg_i^* \leq Pg_i^{max}  &
&\forall i = 1,...,I \\
&&& F_l^{min} \leq F_l^* \leq F_l^{max}&
&\forall l=1,\ldots,L
\end{align*}

The solution of the above minimization problem will be published and sent to each generator.

\subsection{Real-time Market Model and State Estimation}
Due to the stochastic nature of demand $Ld_j$, the real time $Pg,\,Ld,\,F$ may be different from the optimal value $Pg^*,\,Ld^*,\,F^*$. Hence, it is necessary to measure the real time system in order to estimate the real-time state variables. To simplify notation, let us denote the state $x$ as the vector consists of net power injection at each bus \footnote{Under the assumption of DC power flow, there exists bijective relationship between bus voltage phase angle and net power injection \cite{Folk}.}. Since the real-time states are different from the optimal value, we have the following equations
\begin{displaymath}
 x = x^* + w,\, F = H(x^* + w),
\end{displaymath}
where $w$ is the deviation of run time states from the scheduled optimal states. In this paper we will assume that $w$ is a Gaussian random variable with zero mean and covariance $Q$.  We assume that $I+J+L$ number of sensors are deployed to measure $Pg_i,\,Ld_j,\,F_l$ respectively. As a result, the observation equation can be written in the matrix form as follows:
\begin{eqnarray}
z=\begin{bmatrix}I \\ H\end{bmatrix}x+e = Cx+e,
\end{eqnarray}
where $e$ is the measurement error which is also assumed to be Gaussian with zero mean and covariance $R$.

Given $z$, a minimum mean square error estimator is used to estimate the state $x$ based on the following criterion:
\begin{equation}
  \hat x = argmin_{\hat x} \mathbb E\|x - \hat x\|^2_2 .
\end{equation}

Since we assume the observation equations and flow model are linear, one can proof that the solution of the minimum mean square error estimator is given by
\begin{equation}
  \hat x =  (C'RC)^{-1}C'R z = Pz.
\end{equation}

We also assume that a detector is used to detect abnormality in the measurements. Let us define the residue $r$ to be
\begin{equation}
  r \triangleq z - C\hat x.
  \label{eq:residue}
\end{equation}
We will assume the detector triggers an alarm based by comparing the norm of $r$ with certain threshold, i.e. an alarm is triggered if the following event happens:
\begin{equation}
 \|r\|_2 =  \|z - C\hat x\|_2 > threshold.
\end{equation}

\subsection{Ex Post Market}
Since the run time states $Pg,\,Ld,\,F$ is different from the optimal states given by Ex-Ante market, it is reasonable to recalculated the nodal price based on the run time data. In this paper we will use the Ex-Post market model proposed by \cite{Fangxing}. Let us first define the positive congestion set to be
\begin{displaymath}
  cl_+ = \{l: \hat F_l \geq F_l^{max}\} ,
\end{displaymath}
 the negative congestion set to be
\begin{displaymath}
  cl_- = \{l:\hat F_l \leq  F_l^{min}\},
\end{displaymath}
and the non congestion set to be
\begin{displaymath}
  cl_0 = \{l:l\notin cl_+,\,l\notin cl_-\},
\end{displaymath}

The Ex-Post Market will try to solve the following optimization problem:

\textbf{Ex-Post Formulation:}
\begin{align*}
  &\mathop {\textrm{minimize}}\limits_{\Delta Pg_i}&
&\sum\limits_{i=1}^{I}C_i(\Delta Pg_{i} + \hat Pg_i)&
&\\
&\textrm{subject to}&
&\sum_{i=1}^{I}\Delta Pg_{i}=0&
&\\
&&& \Delta Pg_i^{min}\leq  \Delta Pg_i \leq \Delta Pg_i^{max}  &
&\forall i = 1,...,I \\
&&& \Delta F_l \leq 0&
&\forall l\in cl_+\\
&&& \Delta F_l \geq 0&
&\forall l\in cl_-,
\end{align*}
where $\Delta Pg_i^{max}$ and $\Delta Pg_i^{min}$ is usually chosen to be $0.1MWh$ and $-2MWh$ respectively. The Lagrangian of the above minimization problem is defined as
\begin{align}
  \mathcal L &= \sum_{i=1}^I C_i(\Delta Pg_i + \hat Pg_(i)) - \lambda  \sum_{i=1}^I \Delta Pg_i \nonumber\\
  &+ \sum_{i=1}^I\mu_{i,max} (\Delta Pg_i - \Delta Pg_i^{max})\nonumber\\
  &+\sum_{i=1}^I \mu_{i,min}(\Delta Pg_i^{min}-\Delta Pg_i)\nonumber\\
  &+\sum_{l\in cl_+} \eta_{l} \Delta F_l\nonumber + \sum_{l\in cl_-}\zeta_{l}(- \Delta F_l).
\end{align}

It is well known that the optimal solution of the optimization problem must satisfies the KKT conditions. In particular, we know that the following holds:
\begin{align}
  \eta_l \geq 0,\, \zeta_l \geq 0.
  \label{eq:lagrangianmultiplier}
\end{align}
To simply notation, we define $\eta_l = 0$ if $l\notin cl_+$, $\zeta_l = 0$ if $l\notin cl_-$. After solving the above optimization problem and computing the Lagrangian multiplier $\lambda, \mu_{i,max}, \mu_{i,min}, \eta_{l}, \zeta_{l}$, we can define the nodal price at each load bus of the network, which is given by
\begin{equation}
  \lambda_j = \lambda + \sum_{l=1}^L (\eta_{l}- \zeta_l)\frac{\partial F_l}{\partial Ld_j}.
  \label{eq:nodalprice}
\end{equation}
More details of the derivation of nodal price can be found in \cite{Scheweppe}. Now let us write \eqref{eq:nodalprice} in a more compact matrix form. Let us define $\eta = [\eta_1,\ldots,\eta_L]'\in \mathbb R^{L}$ to be a vector of all $\eta_l$ and $\zeta  = [\zeta_1,\ldots,\zeta_L]'$. By \eqref{eq:systemmodel}, we know that $\partial F_l/\partial Ld_j = H_{lj}$, where $H_{lj}$ is the element on the $l$th row and $j$th column of $H$. Hence, \eqref{eq:nodalprice} can be simplified as
\begin{equation}
 \lambda_j = \lambda + H_j^T (\eta - \zeta),
  \label{eq:nodalpricematrix}
\end{equation}
where $H_j$ is the $j$th column of $H$ matrix. The difference of price at two nodes $j_1$ and $j_2$ is given by
\begin{equation}
   \lambda_{j_1} - \lambda_{j_2} =  (H_{j_1} - H_{j_2})^T(\eta-\zeta).
  \label{eq:differencenodalprice}
\end{equation}

\section{Attack Model}\label{attacks}
In this section we assume a malicious third party want to attack the system and make a profit from the market, by compromising certain number of sensors and sending bogus measurement to the RTO. We assume the attacker has the following capabilities:
\begin{enumerate}
  \item The attacker has full knowledge the underlying system model and price model.
  \item The attacker knows the optimal states $Pg^*,\,Ld^*,\,F^*$ published by the RTO from the Ex-Ante market.
  \item The attacker compromised several sensors and can manipulate their readings arbitrarily. Let us define matrix $\Gamma = diag(\gamma_1,\ldots,\gamma_{I+J+L})$, where $\gamma_i$ is a binary variable and $\gamma_i = 1$ if and only if sensor $i$ is compromised. Hence, the corrupted measurements received by the RTO can be written as $z' = z + z^a$, where $z^a$, which lies in the column space of $\Gamma$, is the bias introduced by the attacker.
\end{enumerate}
Based on the above assumptions, the state estimation equations can be written as
\begin{align}
\hat x' &= Pz' = \hat x + P z^a.
\end{align}
Thus, the new residue becomes $r' = r + (I - CP) z^a$. By triangular inequality, 
\begin{displaymath}
 \|r'\|_2 \leq \|r\|_2 + \|(I-CP)  z^a\|_2.
\end{displaymath}
As a result, if $\|(I-CP)\Delta z^a\|_2$ is small, then with a large probability the detector cannot distinguish $r'$ and $r$. In the limit case, if $(I-CP)\Delta z = 0$, then $r'$ will pass the detector whenever $r$ passes the detector. Based on these arguments, we give the following definition:
\begin{defn}
  The attacker's input $z^a$ is called $\varepsilon$-feasible if $\|(I-CP) z^a\|_2 \leq \varepsilon$.
\end{defn}
\begin{remarks}
  $\varepsilon$ is a design parameter for the attacker depending on how subtle it wants the attack to be. An attack with smaller $\varepsilon$ will be more likely to be undetected by the RTO. However, the magnitude of attacker inputs, and hence the attacker's ability to manipulate the state estimation, will be limited. In the rest of the paper we will assume $\varepsilon$ is pre-determined by the attacker.
\end{remarks}

Besides being unnoticeable, the attack must also be profitable to the attacker. In this paper, we assume that the attacker will exploit the virtual bidding mechanism to make a profit. In many RTOs such as New England and PJM, virtual bidding activities are legitimate financial instruments in electricity markets. A market participant purchase/sell a certain amount of virtual power $P$ at location $i$ in day-ahead forward market, and will be obliged to sell/purchase the exact same amount in the corresponding real-time market. Therefore, the attacker's action can be summarized as
\begin{itemize}
  \item In day-ahead forward market, buy and sell virtual power $P$ at locations $j_1$ and $j_2$ at price $\lambda_{j_1}^{DA},\,\lambda_{j_2}^{DA}$, respectively.
\item Inject $z^a$ to manipulate the nodal price of Ex-Post market.
\item In Ex-Post market, sell and buy virtual power $P$ at locations $j_1$ and $j_2$ at price $\lambda_{j_1},\,\lambda_{j_2}$, respectively.
\end{itemize}
The profit that the attacker could obtain from this combination of virtual trading is
\begin{align*}
Profit&=(\lambda_{j_1}-\lambda_{j_1}^{DA})P+(\lambda_{j_2}^{DA}-\lambda_{j_2})P\\
&=(\lambda_{j_1}-\lambda_{j_2}+\lambda_{j_2}^{DA}-\lambda_{j_1}^{DA})P
\end{align*}
Let us define
\begin{equation}
p =   \lambda_{j_1}-\lambda_{j_2}+\lambda_{j_2}^{DA}-\lambda_{j_1}^{DA}.
\label{eq:profit}
\end{equation}
Combining \eqref{eq:differencenodalprice}, \eqref{eq:profit} can be written as
\begin{displaymath}
  p(z') = (H_{j_1}-H_{j_2})^T (\eta(z') - \zeta(z')) + \lambda_{j_2}^{DA} - \lambda_{j_1}^{DA}.
\end{displaymath}

Ideally, the attacker would like to enforce that $p(z')>0$. However, since the system is stochastic and the $z'$ vector is partially unknown to the attacker, it can only try to guarantee that $\mathbb Ep(z')>0$, i.e., the attack is profitable in the expected sense. Such a problem is still quite hard since the relationship between $\eta,\,\zeta$ and $z'$ is given by the Lagrangian multiplier and hence implicit. As a result, Monte Carlo method may be used in order to compute $\mathbb Ep(z')$. In the next section, we will exploit the structure of the Ex-Post formulation and develop a heuristic for the attacker.

\section{Main Result}\label{MainResults}
In this section, we will develop a heuristic for the attacker, which can be effectively formulated as a convex optimization problem and solved efficiently. First let us define the set
\begin{displaymath}
 L_+ = \{l:H_{l,j_1}>H_{l,j_2}\},
\end{displaymath}
and
\[
L_- = \{l:H_{l,j_1}<H_{l,j_2}\}.
\]

As a result, $p(z')$ can be written as
 \begin{equation}
   \begin{split}
   &p(z') = \sum_{l\in L+}(H_{l,j_1}-H_{l,j_2})(\eta_l(z') - \zeta_l(z'))\\
   &+\sum_{l\in L-}(H_{l,j_2}-H_{l,j_1})(\zeta_l(z') - \eta_l(z')) \\
   &+ \lambda_{j_2}^{DA} - \lambda_{j_1}^{DA}.
  \label{eq:profittable2}
   \end{split}
 \end{equation}
Now by the fact that $\eta_l$($\zeta_l$) is non-negative and is $0$ if the line is not positive(negative) congested, we know that the following conditions are sufficient for $p(z')>0$
\begin{enumerate}
  \item $\lambda_{j_2}^{DA} > \lambda_{j_1}^{DA}$.
  \item $\hat F_l' < F_l^{max}$ if $l \in L_-$, i.e. the line is not positive congested.
  \item $\hat F_l' > F_l^{min}$ if $l \in L_+$, i.e. the line is not negative congested.
\end{enumerate}

The first condition can be easily satisfied in the day-ahead market. As a result, the attacker needs to manipulate the measurement $z'$ to make sure that the last two conditions hold. Following such intuition, we give the following definition:
\begin{defn}
 An attack input $z^a$ is called $\delta$-profitable if the following holds
 \begin{displaymath}
   \begin{split}
 \mathbb E \hat F_l' &\leq F_l^{max} - \delta,\, \forall l\in L_-, \\
 \mathbb E \hat F_l' &\geq F_l^{min} + \delta,\, \forall l\in L_+,
   \end{split}
 \end{displaymath}
where $\mathbb EF' = F^* + HPz^a$.
\end{defn}
\begin{remarks}
Since the real $\hat F'$ is a Gaussian random variable with mean $\mathbb E\hat F'$, a large margin $\delta$ will guarantee that with large probability the last two conditions are not violated.
\end{remarks}

Hence, the attacker's strategy during the run time is to find an $\varepsilon$ feasible $z^a$ such that the margin $\delta$ is maximized, which can be formulated as

\begin{align*}
  &\mathop {\textrm{maximize}}\limits_{z^a\in span(\Gamma)}&
&\delta&
&\\
&\textrm{subject to}&
&\|(I-CP)z^a\|_2 \leq \varepsilon&
&\\
&&&  \mathbb E \hat F_l' \leq F_l^{max}-\delta  &
&  \forall l\in L_-\\
&&&  \mathbb E \hat F_l' \geq F_l^{min}+\delta  &
&  \forall l\in L_+\\
&&& \delta > 0 &&
\end{align*}

\begin{remarks}
It is possible that the above convex optimization problem is infeasible. In other words there may not be a $\delta$ profitable solution given the operating point. In that case, we can relax the above optimization problem by adding a penalty on those lines that are congested in the undesirable directions. The new formulation is as follows:
\begin{align*}
  &\mathop {\textrm{maximize}}\limits_{z^a\in span(\Gamma)}&
  &\delta - D\sum_{l=1}^l \beta_l&
&\\
&\textrm{subject to}&
&\|(I-CP)z^a\|_2 \leq \varepsilon&
&\\
&&&  \mathbb E \hat F_l' \leq F_l^{max}-\delta +\beta_l  &
&  \forall l\in L_-\\
&&&  \mathbb E \hat F_l' \geq F_l^{min}+\delta-\beta_l  &
&  \forall l\in L_+\\
&&& \delta > 0 &&\\
&&& \beta_l > 0 &&\forall l = 1,\ldots,l,\\
\end{align*}
where $D > 0$ is the weight of the penalty and $\beta_l$ is the relaxation variable.
\end{remarks}


\section{Limited Resources to Compromise Sensor}\label{Limited}
In this section, we would like to consider an alternative scenario, where the attacker would choose which set of sensors it want to compromise. However, due to limited resources, the total number of compromised sensor cannot exceed certain threshold $N$. As a result, not only does the attacker need to design an optimal input to system, but also it need to choose the optimal set of sensors to compromise.

Followed by the previous argument, we can write the optimization problem as
\begin{align*}
  &\mathop {\textrm{maximize}}\limits_{z^a}&
&\delta&
&\\
&\textrm{subject to}&
&\|(I-CP)z^a\|_2 \leq \varepsilon&
&\\
&&&  \mathbb E \hat F_l' \leq F_l^{max}-\delta  &
&  \forall l\in L_-\\
&&&  \mathbb E \hat F_l' \geq F_l^{min}+\delta  &
&  \forall l\in L_+\\
&&& \delta > 0 &&\\
&&&\|z^a\|_0\leq N,
\end{align*}
where $\|\cdot\|_0$ is the zero norm, which is defined as the number of non-zero elements in a vector. Note that in this formulation we do not require that $z^a$ lies in the span of $\Gamma$, but instead we require $z^a$ to have no more than $N$ non-zero elements. The non-zero elements of $z^a$ will correspond to the sensors the attacker need to compromise.

However, the above formulation is a hard combinatorial problem, since it involves zero norm of a vector, which is not convex. To render the problem solvable, we will relax it in to a convex optimization problem, using the method developed in ``Enhancing sparsity by reweighted l1 minimization E. Candes, M. Wakin, and S. Boyd''. According to this method, the $L_0$ norm is substituted with a weighted $L_1$ norm, where the weights are chosen to avoid the penalization, given by the $L_1$ norm, of the bigger coefficients. In that paper, the authors propose an iterative algorithm that alternates between an estimation phase and a redefinition the weights, based on the empiric consideration that the weights should relate inversely to the true signal magnitudes. In the following we propose this algorithm to relax the optimization problem $(P_0')$. The algorithm is composed of $4$ steps:

\begin{enumerate}
\item Set the iteration count $c$ to zero and set the weights vector to $w_i^{0}=1$ for $i=1,...., I+J+L$
\item Solve the weighted $L_1$ minimization problem
\begin{align*}
  &\mathop {\textrm{maximize}}\limits_{z^a}&
&\delta&
&\\
&\textrm{subject to}&
&\|(I-CP)z^a\|_2 \leq \varepsilon&
&\\
&&&  \mathbb E \hat F_l' \leq F_l^{max}-\delta  &
&  \forall l\in L_-\\
&&&  \mathbb E \hat F_l' \geq F_l^{min}+\delta  &
&  \forall l\in L_+\\
&&& \delta > 0 &&\\
&&&\sum_i |z^a_iw_i^c|\leq N,
\end{align*}
Let the solution be $z^{a,c}_1,\ldots,z^{a,c}_{I+J+L}$.
\item Update the weights
\[
w_i^{c+1}=\frac{1}{z^{a,c}_{i}+\epsilon}\quad,i=1, \dots ,I+J+L
\]
\item Terminate on convergence or when $c$ reaches a specified maximum number of iterations $c_{max}$. Otherwise, increment $c$ and go to step 2.
\end{enumerate}

\begin{remarks}
  As in the original paper, we introduce the parameter $\epsilon >0$ in step 3 in order to avoid inversion of zero-valued component in $z^a$.
\end{remarks}


\section{Illustrative Examples}
\label{examples}

In this Section we illustrate the economic impact of malicious false data injection attacks using a standard IEEE 14-bus system, which is shown in Figure \ref{fig:14bus}. In this system there are a total of five generators. We study three cases, which are described in Table \ref{Cases}. In Case I, only one line is congested and two line flow sensors are assumed to be compromised by the attackers. In Case II and III, there are three lines being congested. The difference between Case II and Case III is that limited (only two) sensors are allowed to be compromised in Case III. 


In Cases I and II, a malicious attacker follows the procedure described in the end of Section \ref{attacks} with the purpose of gaining profit from virtual bidding. In Case III, the malicious attacker follow the limited sensor attack algorithm described in Section \ref{Limited}. At the pair of the nodes that are pre-specified in the third column of Table \ref{Cases}, the attacker buys and sells the same amount of virtual power in day-ahead market at nodes $j_1$ and $j_2$, respectively. Based on historical trends, the attacker will buy at the lower price node and sell at the higher price node \footnote{The choice of pairs of nodes does not necessarily have to be between a congested transmission line \cite{Folk}. As long as the pair of nodes exhibit consistent nodal price differences, this pair of nodes could be a candidate.}. In the real-time market corresponding to the same time interval, the attacker compromises the selected line flow sensors by  injecting false data, the result of which leads to a few congested transmission lines appear not congested anymore in EMS state estimation functions. This attack, in turn, will result a different vector of real-time ex-post LMPs compared with the day-ahead LMPs \footnote{To illustrate the effect of the attacks on ex-post market clearing prices, we assume that the load forecast at day-ahead is perfect. In other words, if there were no cyber attacks, the day-ahead LMP will be the same as the ex-post LMP.}.

\begin{figure}[t]
\centering \setlength{\floatsep} {1pt plus 3pt minus 2pt}
\includegraphics[width=3.5in]{14bus600.eps}\hfill%\vspace{-.in}
\caption{IEEE standard 14-bus system} \label{fig:14bus}
\end{figure}

\begin{table*}
\centering \caption{Case Description} \label{Cases} \centering
\begin{tabular}
{|c|c|c|c|} \hline  & congested lines in day-ahead (from bus-to bus) & virtual bidding nodes & compromised sensors\\
\hline Case I & 1-2  & 2 and 4 & line flow sensors 1-2, 3-4  \\
\hline Case II & 1-2, 2-4, 2-5 & 1 and 2 & line flow sensors 1-2, 2-3, 2-4 \\
\hline Case III & 1-2, 2-4, 2-5 & 1 and 2 & line flow sensors 1-2, 2-3 \\
\hline
\end{tabular}
\end{table*}



\begin{figure}[t]
\centering \setlength{\floatsep} {1pt plus 3pt minus 2pt}
\includegraphics[width=3.8in]{LMP1.eps}\hfill%\vspace{-.in}
\caption{LMP with and without cyber attacks (only one line congestion)} \label{LMP1}
\end{figure}


\begin{figure}[t]
\centering \setlength{\floatsep} {1pt plus 3pt minus 2pt}
\includegraphics[width=3.8in]{LMP2.eps}\hfill%\vspace{-.in}
\caption{LMP with and without cyber attacks (three congested lines)} \label{LMP2}
\end{figure}

In Case I, only one transmission line (from bus 1 to bus 2) is congested. The attacker chooses to buy virtual power at bus 4 and sells virtual power at bus 2 in day-ahead market. By compromising two line flow measurement sensors with false data injection, the transmission line congestion appears to be relieved in real-time EMS, based from which the real-time MMS gets a system-wide uniform ex-post LMP. Figure \ref{LMP1} shows the LMPs with and without the cyber attacks. Based on (12), the profit of such transaction is about \$2/MWh. In Case II, there are three congested lines in the day-ahead market. By compromising three line flow sensors, the desired pair of nodes (buses 1 and 2) have the same LMP in ex-post real-time market. The reason is that the cyber attacks maliciously lower the estimated line flow information, thereby setting the shadow prices of the actual congested lines to be zero. The profit of such transaction is approximately \$8.2/MWh. In Case III, we assume that an attacker could compromise at most two sensors. By applying the algorithm described in Section \ref{Limited}, the attacker chooses to compromise line flow sensors between nodes 1-2, and nodes 2-3. Only compromising these two sensors could not make all the congested lines appear uncongested in real-time operations. However, as shown in Figure \ref{LMP2}, by only compromising two sensors can still lead to attackers' profit of \$6.0/MWh. 


In Table \ref{AttackCompare} we compare the attack efforts and expected financial profits for all the three cases. We use the norm infinity of $z_a$ with respect to the norm infinity of $z$ as an indicator of the attack efforts. As the system congestion becomes more complex, the potential of gaining financial profits by maliciously placing false data attack is also higher. One could observe from the comparison between Case II and Case III that if the attacker could only compromise a limited set of sensors, then the expected profits would also decrease. However, even compromising a very small number of sensors (e.g. two sensors in the Case III) could still benefit the attack significantly consistently over the time. In other words, the economic impact due to even a small false data injection attack is not negligible over the long run.



\begin{table}
\centering \caption{Attack Efforts and Profits ($\varepsilon= 1$MWh)} \label{AttackCompare} \centering
\begin{tabular}
{|c|c|c|} \hline & relative efforts ($\frac{\|z_a\|_{\infty}}{\|z\|_{\infty}}$) & profits (\% of transaction cost)\\
\hline Case I & 1.23\% & 2.40\% \\
\hline Case II & 1.41\% & 9.46\% \\
\hline Case III & 1.31\% & 7.54\% \\
\hline
\end{tabular}
\end{table}

\section{Conclusions and Future Work}
\label{conclusion}

In this paper we examine the possible economic impact due to false data injection attacks in electric power market operations. We show that an attacker could manipulate the nodal price of Ex-Post market while being undetected by the system operator. Combining with virtual bidding, such attack could bring consistent financial profit to the attacker. A heuristic is developed to compute the optimal injection of the attacker, which can be formulated as a convex optimization problem and thus solved efficiently by the attacker. An illustrative example is further provided to show the effect of false data injection attacks on the IEEE $14$-bus systems. 

In our future work, effective countermeasures to mitigate the financial impact of false data injection attacks will be investigated. We also plan to investigate the robustness of different ex-post LMP pricing models subject to such a class of false data injection. Last but not least, state estimation algorithms that are robust subject to false/malicious data attacks is also an important research direction. 


\section*{Acknowledgments}

We thank Dr. Feng Zhao of ISO-New England for informative discussion on the electricity market pricing models. This work was supported in part by Texas Engineering Experiment Station, and in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, Texas A\&M, Carnegie Mellon, or the U.S. Government or any of its agencies.

\begin{thebibliography}{35}

\bibitem{Amin}
S.~M.~Amin and B.~F.~Wollenberg, ``Toward a smart grid," \emph{IEEE
Power \& Energy Magazine}, Vol. 3, Issue 5, pp.34-41, Sep/Oct 2005.

\bibitem{Wu_ControlCenter}
F.~F.~Wu, K.~Moslehi, and A.~Bose, ``Power system control centers:
past, present, and future," \emph{Proceedings of the IEEE}, Vol. 93,
Issue 11, pp.1890-1908, Nov 2005.



\bibitem{Scheweppe}
F.~C~Schweppe, J.~Wildes, and D.~B.~Rom, ``Power system static state estimation, Parts I, II and III,"
\emph{IEEE Transactions on Power Apparatus and Systems}, Vol. 89, Issue 1, pp.
120-135, Jan 1970.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{WuSurvey}
F.~F.~Wu, ``Power system state estimation: a survey,"
\emph{International Journal of Electrical Power \& Energy Systems}, Vol. 12, Issue 2, pp.
80-87, Apr 1990.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Fangxing}
F.~Li, Y.~Wei, and S.~Adhikari, ``Improving an unjustified common practice in ex post LMP calculation,"
\emph{IEEE Transactions on Power Systems}, Vol. 25, Issue 2, pp.
1195-1197, May 2010.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Gross2009}
M.~Negrete-Pincetic, F.~Yoshida, and G.~Gross, ``Towards quantifying the impacts of cyber attacks in the competitive electricity market environment,"
\emph{Proceedings of IEEE PowerTech}, Jul 2009.\hskip1em plus 0.5em minus 0.4em\relax


\bibitem{CA2008}
D.~Salem-Natarajan, L.~Zhao, W.~Shao, M.~Varghese, S.~Ghosh, M.~Subramanian, G.~Lin, H. Chiang, and H.~Li, ``State estimator for CA ISO market and security applications-relevance and readiness,"
\emph{Proceedings of IEEE Power and Energy Society General Meeting}, Jul 2008.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Liu2009}
Y.~Liu, M.~K.~Reiter, and P.~Ning, ``False data injection attacks against state estimation in electric power grids,"
\emph{Proceedings of the 16th ACM Conference on Computer and Communications Security}, 2009.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Sandberg}
H.~Sandberg, A.~Teixeira, and K.~H.~Johansson, ``On security indices for state estimators in power networks,"
\emph{First Workshop on Secure Control Systems, CPSWEEK 2010}, Apr 2010.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Kosut1}
O.~Kosut, L.~Jia, R.~Thomas, and L.~Tong, ``Limiting false data attacks on power system state estimation,"
\emph{Proceedings of Conference on Information Sciences and Systems}, Mar 2010.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Kosut2}
O.~Kosut, L.~Jia, R.~Thomas, and L.~Tong, ``Malicious Data Attacks on Smart Grid State Estimation: Attack Strategies and Countermeasures,"
\emph{Proceedings of First IEEE Smart Grid Communication Conference}, Oct 2010.\hskip1em plus 0.5em minus 0.4em\relax

\bibitem{Folk}
F.~F.~Wu, P.~Varaiya, P.~Spiller, and S.~Oren, ``Folk theorems on transmission access: proofs and counterexamples,"
\emph{Journal of Regulatory Economics}, Vol. 10, Issue 1, pp. 5-23, Jul 1996.


\end{thebibliography}

\end{document}

