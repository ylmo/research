This section is devoted to manipulating and relaxing the
optimization Problem~\ref{opt_dyn} to make it explicit respect to
the optimization variables and convex. 
In the first subsection we reformulate the estimation performance constraint $P_k(V_T)<P_d$ to make it explicit function of the sensors $V_T$. In the second subsection, we relax the objective function $\mathcal E(E_T)$ to make the optimization problem numerically solvable by means convex programming.
\subsection{Manipulation of $P_k(V_T)$}

This subsection is devoted to finding an explicit form of $P_k(V)$.

Let us indicate with $\gamma_{k,j},\;j=1,\ldots,m$ the binary variable such that $\gamma_{k,i}  = 1$ if and only if $s_i \in V$ at time $k$. By the definition of $\gamma_{k,i}$, the fusion center will receive at time $k$ the readings $[\gamma_{k,1}y_{k,1},\ldots,\gamma_{k,m}y_{k,m}]^T$. In order to find the relationship between $P_k$ and $\gamma_{k,i}$s, we need to define the following quantities:
\begin{align}
  \Gamma_k &\triangleq diag(\gamma_{k,1},\ldots,\gamma_{k,m}) &\in& \mathbb R^{m\times m},\\
  \vec \gamma_k &\triangleq [\gamma_{k,1},\ldots,\gamma_{k,m}]^T &\in& \mathbb R^{m},\\
  \hat x^*_{k} &\triangleq E\left(x_k|y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0} \right)&\in&\mathbb R^{n},\\
  P^*_{k} &\triangleq Cov\left(x_k|y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0}\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k} &\triangleq E\left(x_k|\Gamma_k y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0}\right)&\in&\mathbb R^{n},\\
  P_{k} &\triangleq Cov\left(x_k|\Gamma_k y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0} \right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}
where $\tilde y_i$, with $i=0,1,\dots k-1$, is the measurement already received by the fusion center at time $i$.

Let us first consider the case that all the sensors are used at time $k$. The Kalman filter for the system takes the following form
\begin{align}\label{eq:Kalman_filter}
    \hat x_{k|k-1} &= A \hat x_{k-1} \nonumber\\
    P_{k|k-1}  &= A P_{k-1} A^T  + Q  \nonumber\\
    K_k^*        &= P_{k|k-1} C^T (CP_{k|k-1} C^T  + R)^{ - 1} \nonumber \\
    \hat x^*_{k} &= K_k^* (y_k -C\hat x_{k|k-1}) + \hat x_{k|k-1} \nonumber\\
    P^*_{k}      &= P_{k|k-1}  -  K^*_k CP_{k|k-1},
  \end{align}
  where $\hat x_{k}^*$ is the estimate of the state at time $k$ and $P_{k}^*$ is the estimation error covariance matrix.

Now, let us consider any linear filter with gain $K_k$ under the sensor scheduling scheme. Please note that this filter is not necessarily optimal. We denote the estimate produced by this filter as $\check x_{k}$. It is easy to show that
\begin{equation}
  \check x_{k} = K_k\Gamma_k (y_k - C\hat x_{k|k-1}) + \hat x_{k|k-1},
  \label{eq:kalmanestimationforselection}
\end{equation}
where $K_k$ is the linear gain under the sensor selection scheme $\Gamma$. In general, the optimal gain $K_k$ is different from $K^*_k$ as it depends on the choice of the sensor selection strategy $\Gamma$. By exploiting the optimality and linearity of the Kalman filter, the following theorem holds.

\begin{thm}
  \label{theorem:covarianceafterscheduling}
  The estimation error covariance matrix $P_{k}$ verifies the following inequality
  \begin{equation}
    P_{k} \leq P_{k}^* + (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T,
  \end{equation}
  where $S_k = C(AP_{k-1}A^T+Q)C^T + R$, and $\mathcal K_k = K_k\Gamma$, where $K_k$ is any gain. Furthermore, there exists a $\mathcal K_k$ such that the equation holds.
\end{thm}

\begin{pf}
The error covariance matrix of a linear filter under sensor scheduling scheme is given by
\begin{equation} \label{cov_matrix}
  \begin{split}
    & Cov(\check x_{k} - x_k) = Cov(\check x_{k} - \hat x^*_k + \hat x^*_k - x_k)\\
    & = Cov [(K_k\Gamma_k - K_k^*) (y_k-C\hat x_{k|k-1})  +  \hat x^*_{k} - x_k ]\\
    & = Cov [(K_k\Gamma_k - K_k^*) (y_k-C\hat x_{k|k-1})]  + Cov( \hat x^*_{k} - x_k )\\
    & = (K_k\Gamma_k - K_k^*) Cov(y_k-C\hat x_{k|k-1})(K_k\Gamma_k - K_k^*)^T  + P^*_{k}.\\
  \end{split}
\end{equation}
The decomposition of the covariance matrix in the third equation of \eqref{cov_matrix} is possible because, by the optimality of Kalman filter, we know $x_k - \hat x_{k}^*$ is orthogonal to $y_k-C\hat x_{k|k-1}$. We also know that
\begin{align}
  &y_k - C\hat x_{k|k-1} = C(Ax_{k-1} + w_{k-1}) + v_k - CA\hat x_{k-1}\\
  &=CA(x_{k-1} - \hat x_{k-1}) + Cw_{k-1} + v_k. \label{innovation}
\end{align}
It is easy to check that the first term in \eqref{innovation} only depends on $w_0,\ldots,w_{k-2},v_0,\ldots,v_{k-1}$. Thus, the three terms in the above equation are independent. As a result,
\begin{equation}
  Cov(y_k - C\hat x_{k|k-1}) = CAP_{k-1}A^TC^T + CQC^T + R = S_k.
\end{equation}

Since the Kalman filter is the optimal linear filter, we know that
  \begin{displaymath}
    P_{k} \leq P_{k}^* + (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T,
  \end{displaymath}
  and there exists a $\mathcal K_k$ such that the equality holds.
\end{pf}

Applying the result of theorem~\ref{theorem:covarianceafterscheduling}, it is straightforward to see that the constraint $P_k \leq P_d$ can be rewritten as
\begin{equation}
    \exists \mathcal K_k, \; s.t.\;(\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d - P_{k}^*\,.
\end{equation}

\begin{remark}
Considering the definition of $\mathcal K_k = K_k \Gamma_k$, if we denote the $j$th column of $\mathcal K_k$ as $\vec{\mathcal K}_{k,j}$, the following inequality holds
  \begin{equation}
    \gamma_{k,j} \geq \left\| \left\| \vec{\mathcal K}_{k,j}\right\|_1 \right\|_0,
  \end{equation}
  where $\left\|\vec v\right\|_0$ is simply the number of non-zero elements of $\vec v$.
\end{remark}

Combining all the previous results, we can conclude that the following problem is equivalent to Problem~\ref{opt_dyn}.


\begin{prob}\label{opt_dyn2}
\begin{align*}
  &\min_{T\in \mathcal T,\,\mathcal K_k}&&\mathcal E(E_T) \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{k,i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0 \quad i=1\dots m.
\end{align*}
\end{prob}
\begin{remark}
In Problem \ref{opt_dyn2} the objective function still depends on the transmission tree $T$ while the constraints are expressed in terms of the optimization variables $\mathcal K_k$ and the vector $\vec \gamma_k$.
\end{remark}

\subsection{Manipulation of $\mathcal E(E_T)$}
This subsection is devoted to manipulating and relaxing the objective function $\mathcal E(E_T)$ to make it an explicit function of $V_T$ and hence of the sensor selection vector $\vec \gamma_k$.

Given a transmission tree $T$, let us indicate the Steiner Tree generated by the sensors $V_T$ as $ST(V_T) = \{V_{ST}(V_T),\,E_{ST}(V_T)\}$.

\begin{remark}
 Note that the Steiner Tree $ST(V_T)$ is a function of $V_T$ and hence it is also a function of $\vec \gamma_k$.
 Thus, we can write $ST(V_T) = ST(\vec \gamma_k) = \{V_{ST}(\vec \gamma_k),\,E_{ST}(\vec \gamma_k)\}$.
\end{remark}

Since the Steiner Tree is the minimum cost tree that connects all the vertices in $V_T$, the following problem is equivalent to Problem~\ref{opt_dyn} and \ref{opt_dyn2}.

\begin{prob}[Energy minimization over $\vec \gamma_k$]\label{opt_dyn3}
\begin{align*}
  &\min_{\vec \gamma_k\,,\,\mathcal K_k}&&\mathcal E(E_{ST}(\vec \gamma_k)) \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{k,i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0  \quad i=1\dots m.
\end{align*}
\end{prob}

The objective function is now a function of the selected sensor set $V$. However, given a vertex set $V$, constructing the edge set $E_{ST}(V)$ is in general a NP-hard problem. As a result, we approximate $\mathcal E(E_{ST}(\vec \gamma))$ with a new function.

Let us indicate with $\overline{d}_i$, with $i=1,\dots m$, the cost of the shortest path from sensor $s_i$ to the fusion center $s_0$ and let us define the vector
$\vec{\overline{d}} = [\overline d_1,\ldots,\,\overline d_m]^T$ \footnote{$\overline d_i$ can be efficiently computed by Dijkstra's algorithm}.
Let us also define $\underline{d}_i = \min_j\{cost(e_{i,j})|e_{i,j} \in E\}$ and $\vec{\underline{d}}= [\underline d_1,\ldots,\underline d_m]^T$.
The following lemma provides an upper and lower bound for $\mathcal E(E_{ST}(\vec \gamma_k))$.
\begin{lem} \label{lem:relaxation}
  The following inequality for $\mathcal E(E_{ST}(\vec\gamma_k))$ holds
  \begin{equation}
    \vec{\underline{d}}^T\vec \gamma_k \leq \mathcal E(E_{ST}(\vec\gamma_k)) \leq \vec{\overline{d}}^T\vec\gamma_k
    \label{eq:basicinequality}
  \end{equation}
\end{lem}
\begin{pf}
Assume a transmission tree $T$ is fixed and a sensor $s_i\in V_T$. Then, there exists at least one sensor or the fusion center $s_j\in V_{ST}(V_T)$ such that $e_{ij} \in E_{ST}(V_T)$.
Since, for each sensor $s_i$, $\underline d_{i}$ is the minimum cost among all edges connecting $s_i$ to another sensor or fusion center, the lower inequality holds.

For the upper bound, it is sufficient to observe that the union of all the shortest paths connecting the sensors in $V_T$ to fusion center is a tree with root in $s_0$. Hence, by the definition of Steiner tree, also the upper inequality holds.
\end{pf}

%The solution of the following problems will be the lower and upper bound of Problem~\ref{opt_dyn}.
Lemma \ref{lem:relaxation} gives us two possible relaxation of Problem~\ref{opt_dyn}.


\begin{prob}[Relaxation 1 for energy minimization problem]\label{opt_dynlower}
\begin{align*}
  &\min_{\vec \gamma_k \,,\,\mathcal K_k}&&\vec{\underline{d}}^T \vec \gamma_k \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{k,i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0 \quad i=1\dots m.
\end{align*}
\end{prob}
\begin{prob}[Relaxation 2 for energy minimization problem]\label{opt_dynupper}
\begin{align*}
  &\min_{\vec \gamma_k \,,\,\mathcal K_k}&&\vec{\overline{d}}^T \vec \gamma_k \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{k,i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0 \quad i=1\dots m.
\end{align*}
\end{prob}

In general, sensor selections are hard combinatorial
problems by nature as they involve binary constraints.
For large systems the problem becomes computationally
infeasible. Solutions in this case are based on heuristic
methods. As a result, Problems \ref{opt_dynlower} and \ref{opt_dynupper} are no exception, as they are also
combinatorial problems, since the last constraint
contains a zero norm. However, these formulations, and in particular the zero norm constraint, can be easily relaxed into a convex problem, as shown in \cite{boydreweight}.


%
%\subsection{Algorithm}
%
%This subsection is devoted to clearly describing the algorithm to approximately solve problem~\ref{opt_dyn}. The algorithm goes as follows:
%\begin{algorithm}[H] \label{Alg}
%  \caption{Algorithm for Approximately Solve Problem~\ref{opt_dyn}}
%  %\algsetup{indent=2cm}
%  \begin{algorithmic}[1]
% %   \STATE $V_{old} \Leftarrow S$
%    \STATE $V_{new} \Leftarrow S$
%    \REPEAT
%    \STATE Let us define the graph $G_{old}=\{V_{old}\,,E_{old}\}$ where
%            \begin{align*}
%            V_{old}& \Leftarrow V_{new} \\
%            E_{old}&=\{e_{ij}\in E \,:\,i,j \in V_{old} \}
%            \end{align*}
%    \STATE Solve Problem~\ref{opt_dynlower} (Problem \ref{opt_dynupper}) on subgraph $G_{old}$.
%            Indicate with $V$ the solution of the minimization problem
%
%    \STATE Evaluate an approximation of the Steiner tree $ST(V)$ generated by $V$ and set
%    \[
%     V_{new}=V_{ST}(V)
%    \] \label{step3}
%    \UNTIL {$\mathcal E(E_{ST}(V_{old}))== \mathcal E(E_{ST}(V))$}
%    \RETURN $ST (V)$
%  \end{algorithmic}
%\end{algorithm}
%
%\begin{remark}
%	The algorithm must stop since $\mathcal E(E_{ST}(V)) \leq \mathcal E(E_{ST}(V_{old}))$ and there are only finitely possible vertex sets $V$.
%\end{remark}
%\begin{remark}
%Algorithm $1$ needs, in Step \ref{step3}, the evaluation of the Steiner tree. We will consider just an approximation of the Steiner tree given by the Matlab function \textit{sfo\_pspiel\_get\_cost.m} of the Matlab Toolbox \textit{SFO}.
%\end{remark}
%To solve Problem~\ref{opt_dynlower} and \ref{opt_dynupper}, we use a reweighted $L_1$ approach to relax the zero norm constraint to and $L_1$ norm constraint \cite{boydreweight}. The details of this relaxation approach are described below:
%\begin{enumerate}
%\item Set the iteration count $l$ to zero and set the weights vector to $w_j^{1}=1$ for $j=1.... m$.
%\item Solve the weighted $\textit{L}_1$ Problem
%\begin{align*} \label{conv_opt}
%	&&& \min \vec d^T \vec \gamma \\
%	& s.t.&&  (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d - P_{k}^*, \\
%\end{align*}
%with
%\[
%\gamma_{k,j}  \geq w_j^{l}  \|\vec {\mathcal K}_{k,j}\|_1.
%\]
%Suppose the solution is $\gamma_{k,1}^l,\ldots,\gamma_{k,m}^l$
%\item Update the weights
%\[
%w_j^{l+1}=\frac{1}{\gamma_{k,j}^l+\epsilon}\quad,j=1,\ldots,m,
%\]
%where $\epsilon$ is a small constant.
%\item Terminate on convergence or when $l$ attains a specified maximum number of iterations $l_{max}$. Otherwise, increment $l$ and go to step 2.
%\end{enumerate}
