\subsection{System and Sensor Network Model}

Consider the following LTI system whose state dynamics are given by
\begin{equation}
    x_{k+1} = Ax_k + w_k
  \label{eq:systemdescription}
\end{equation}
where  $x_k \in \mathbb R^n$ represents the state, $w_k,x_0$ are independent Gaussian random vectors, $x_0 \sim \mathcal N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$. A wireless sensor network composed of $m$ sensing devices $s_1,\ldots,s_m$  and a fusion center $s_0$ is used to monitor the state of system \eqref{eq:systemdescription}. The measurement equation is
\begin{equation}
    y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}]^T \in \mathbb R^m$ is the measurement vector and $v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed independent of $x_0$ and $w_k$. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$.

We model the wireless sensor network as an undirected graph $G = \{V\,,E\}$ where the vertex set $V = \{s_0,\,s_1,\ldots,s_m\}$ contains all sensor nodes including the fusion center; an edge $e_{i,j}$ belongs to the edge set $E$ if and only if $s_i$ can directly transmit a packet to $s_j$ and vice versa. Let $cost:E \rightarrow \mathbb R^+$ be a non-negative function defined on the edge set $E$ such that $cost(e_{i,j})$ indicates the total energy consumed if $s_i$ directly transmits a packet to $s_j$\footnote{$cost(e_{i,j})$ includes both the transmission cost of the sensor $s_i$ and the receiving cost of the sensor $s_j$. We also assume the transmission between $s_i$ and $s_j$ to be symmetric.}.

\subsection{Energy cost minimization problem}

Given a wireless sensor network $G$, we define a transmission graph of $G$ as any possible tree $T = \{V_T\,,E_T\}$ with root in the fusion center $s_0 \in V_T$ such that $V_T\subseteq V$ and $E_T\subseteq E$. We also define the transmission set of $G$ to be $\mathcal T$, i.e. the set of all transmission graphs $T$.

Since $T$ is a tree, a partial order can be assigned on its vertex set $V_T$. In particular, given $s_j,s_i\in V_T$, we consider $s_j \leq s_i$ if and only if the unique path from $s_0$ to $s_i$ passes thorough $s_j$. For a fixed $T \in \mathcal T$, given $s_i\in V_T$, we define the parent set of $s_i$ as
\begin{displaymath}
  \mathcal P(s_i) = \{s_j \in V_T|e_{i,j}\in E_T,\,s_j \leq s_i\},
\end{displaymath}
and the children set of $s_i$ as
\begin{displaymath}
  \mathcal C(s_i) = \{s_j \in V_T| s_i\;is\;the\;parent\;of\;s_j\}.
\end{displaymath}
A leaf node of $T$ is a node with no children.

The transmission graph $T$ defined above uniquely defines a routing scheme over the wireless sensor network: at each time step $k$, all the sensors in $V_T$ collects their measurements. The leaf nodes then forward their observations to their parents in $T$. A non-leaf node will wait until it has received packets from all its children\footnote{Due to the packet loss, in reality one may want to implement some timeout mechanism to prevent a parent from waiting permanently, thus generating a deadlock situation.}. Once a parent has collected all the measurements from its children, it will forward the measurements, including his own, to its parent. Since $T$ is a tree with $s_0$ as root, this scheme guarantees that the fusion center will receive all the measurement from sensor nodes in $V_T$. For real time estimation it is important that the measurements arrive in time at the fusion center to allow timely estimation. We believe this problem will be taken care of at design time, where the time constants of the system will dictate the maximum number of allowable hops. Hence, each transmitted packet will reach the fusion center before the next sampling period. The transmission process is illustrated in the Fig~\ref{fig:trantopology}.

\begin{figure}[H]
  \begin{center}
    \includegraphics[width = 0.3\textwidth]{pic.1}
  \end{center}
  \caption{Transmission graph $V = \{s_0,\,s_1,\,s_2,\,s_3,\,s_5\}$ and $E_T = \{e_{5,1},\,e_{3,1},\,e_{1,0},\,e_{2,0}\}$}
  \label{fig:trantopology}
\end{figure}
It is straightforward to see that, for a fixed transmission tree $T$, the total energy cost, at each time interval, for the transmission over the network is\footnote{Here we need to assume that $cost(e_{i,j})$ is constant regardless of number of observations contained in the packet. This is often the case in reality, if measurements are of simple type, such low precision scalar values, as the transmission overhead, e.g. header, hand shaking protocol, dominates the payload.}
\begin{displaymath}
  \mathcal E(E_T) = \sum_{e \in E_T} cost(e).
\end{displaymath}

Once the fusion center collects all the observations, it will use the Kalman filter to compute the optimal estimate of current state $\hat x_{k}$ and the estimation error covariance matrix $P_{k}$. Considering that, in our case, $P_{k}$ is a function of $V_T$, we will denote it by $P_k(V_T)$.

Assume the requirement on the estimation performance is in terms of the error covariance matrix $P_k(V_T)$ and, in particular, a positive definite matrix $P_d$ is fixed as upper bound for $P_k(V_T)$ \footnote{The comparison is in the positive semidefinite sense}. \\
Hence, the one step network energy minimization problem can be formulated as follows:
\begin{prob}[Energy minimization over transmission tree]\label{opt_dyn}
\begin{align*}
  &\min_{T\in \mathcal T }&&\mathcal E(E_T) \\
	& s.t.&&P_k(V_T) \leq P_d, \;
\end{align*}
\end{prob}

\begin{remark}\label{computational_prob}
Solving Problem~\ref{opt_dyn} with an exhaustive search of space $\mathcal T$ is a very hard problem even for a small network.
In particular, given a fully connected network of $m$ sensors, the number of candidate solutions, i.e. all possible transmission graph $T$, is
\[
|\mathcal T|=\sum_{p=1}^m N(p+1) \binom mp.
\]
where $N(p)$ is the number of spanning trees in a complete graph of $p$ nodes. The value of $N(p)$ is given by Cayley's formula
\[
N(p)=p^{p-2}.
\]
Hence, for a network of $m=20$ sensors, the possible solution space is of the order of $10^{25}$.
\end{remark}
