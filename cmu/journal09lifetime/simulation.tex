
In this section, we will apply the algorithm developed in Section~\ref{sec:main} to a numerical example in which a sensor network is deployed to monitor the temperature inside a planar closed region.

\subsection{Model definition}

We model the heat process in a planar closed region as
\begin{equation}
	\frac{\partial u}{\partial t} = \alpha \left(\frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2}\right),
	\label{eq:diffussionpdfcont}
\end{equation}
with boundary conditions
\begin{equation}
  \left.\frac{\partial u}{\partial x_1}\right|_{t,0,x_2}= \left.\frac{\partial u}{\partial x_1}\right|_{t,l,x_2}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,0}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,l}= 0,
\end{equation}
where  $x_1,x_2$ indicate the coordinates of the region; $u(t,x_1,x_2)$ denotes the temperature at time $t$ at location $(x_1,x_2)$ and $\alpha > 0$ indicates the speed of the diffusion process.

We use a simple finite difference method to discretize this model. We assume the planar region is a square of side $l$ meters and we discretize it with a $N \times N$ grid with grid size $h = l/(N-1)$. Also we sample the system in time with frequency of $1$ Hz. By use of the finite difference method, equation \eqref{eq:diffussionpdfcont} becomes
\begin{align}
	u(k+1,i,j) - u(k,i,j) =& \alpha/h^2 [u(k,i-1,j)+u(k,i,j-1) \nonumber \\
	&+u(k,i+1,j)+u(k,i,j+1)-4u(k,i,j)],
	\label{eq:diffusiondisc}
\end{align}
where $u(k,i,j)$ denotes the temperature at time $k$, at location $(ih,jh)$.
%\begin{remark}
%If either $i$ or $j$ is greater than $N$ or less than $0$, which means the location is outside the grid, then $u(k,i,j)$ is replaced by the value of its nearest neighbor in the grid as a consequence of the boundary conditions we imposed.
%\end{remark}

If we group all the temperature values at $k$ in the vector \\ $U_k = [u(k,0,0),\ldots,u(k,0,N-1),u(k,1,0),\ldots,u(k,N-1,N-1)]^T$, we can write the evolution of the discretized system as follows
\begin{equation}
	U_{k+1} = A U_{k}+ w_k,	
\end{equation}
where the $A$ matrix can be calculated from \eqref{eq:diffusiondisc} and  $w_k$ is the process noise which we assume to be $\mathcal N(0,\;Q)$.
%By introducing process noise then $U_k$ will eveolve according to
%\begin{equation} \label{sys}
%	U_{k+1} = A U_{k} + w_k,
%\end{equation}
%where $w_k$ is the process noise which we assume to be $\mathcal N(0,\;Q)$.

Let us suppose that $m$ sensors are randomly distributed in the region and each sensor measures a linear combination of temperature of the grid around it\footnote{We do not require the sensors to be placed at grid points}. In particular, indicate the location of sensor $\hat s$ with $(\hat x_1,\hat x_2)$ such that $\hat x_1 \in [i,i+1)$ and $\hat x_2 \in [j,j+1)$. Let us define $\Delta \hat x_1 =\hat x_1-i$ and $\Delta \hat x_2 =\hat x_2-j$. We assume the measurement of this sensor is
\begin{align}
%	\begin{split}
	 y_{\hat s} =& \left[\right.(1-\Delta \hat x_1)(1-\Delta \hat x_2) u(k,i,j)+  \Delta \hat x_1(1-\Delta \hat x_2) u(k,i+1,j)+\nonumber \\
	&(1-\Delta \hat x_1)\Delta \hat x_2 u(k,i,j+1)+ \Delta \hat x_1\Delta \hat x_2 u(k,i+1,j+1)\left.\right]/h^2+ noise  .
%	\end{split}
	\label{eq:diffusionmeasure}
\end{align}
Suppose that $Y_k$ is the vector of all the measurements at time $k$, it follows
\begin{equation}\label{sensors}
Y_k = C U_k + v_k,
\end{equation}
where $v_k$ denotes the measurement noise at time $k$ assumed to have normal distribution $\mathcal N(0,\;R)$ and $C$ is the observation matrix that can be computed from \eqref{eq:diffusionmeasure}.

Finally, we assume that the sensor network is fully connected and the communication cost from sensor $i$ to sensor $j$ is
\[
cost(e_{i,j}) = c+d_{i,j}^2
\]
where $d_{ij}$ is the Euclidean distance from sensor $i$ to sensor $j$ and $c$ is a constant related to the sensing energy consumption.
\begin{figure*}
  \begin{center}
    \includegraphics[width = 1\textwidth]{fig_25nodes}
  \end{center}
  \caption{Transmission graphs for the energy consumption minimization of a wireless sensor network monitoring the heat process in a planar closed region. On the left the optimal-like transmission graph is shown, the solution of Algorithm $1$ implemented for both Problem \ref{opt_dynlower} and Problem \ref{opt_dynupper} is shown respectively on the right and in the center} \label{fig:25nodes}
\end{figure*}

For the simulations, we impose the following value for the
parameters:
\begin{itemize}
\item $\alpha = 0.1 \; m^2/s$
\item $l=4\; m$ and $N=5 \Rightarrow$ grid size $h = 1 \;m$
\item $\Sigma = R = Q = I \in \mathbb R^{25\times 25}$
\item $c=1$ and $P_d = 7I + \bf 1$, where $\bf 1$ is a all one matrix
\end{itemize}
We also suppose that the fusion center is located in the center of the room $(2,2)$ and no sensing is performed until $100 \; s$.

\subsection{Example 1}


To evaluate the performance of the proposed algorithm, we compare it with the optimal-like solution of the sensor energy minimization problem. Such solution is not optimal because it does not compute the Steiner tree, (we already know that such a problem is infeasible even for a small size sensor network) but it approximates it in the same manner it is done in our proposed scheme.
In particular, the algorithm considers all possible subsets of the sensors $V_i$, for $i=1 \dots \sum_{i=1}^m\binom mi$, generated by the network and, for each of them, evaluates the approximated Steiner tree using the Matlab function  \textit{sfo\_pspiel\_get\_cost.m} of the Matlab toolbox \textit{SFO}.
The optimal like transmission tree is $ST(V^*)$ where $V^*$ is given by
\begin{align*}
V^*= &arg\min_{V_i} &&\mathcal{E}(E_{ST(V_i)}) \\
        &s.t. && P_k(V_{ST(V_i)})<P_d
\end{align*}
To evaluate the performance of the proposed algorithm, we consider the discretized model of the heat process in a planar closed region and we choose to randomly place $m=25$ sensors in the closed region, i.e. we fix the matrix $C$ in \eqref{sensors}.
In figure \ref{fig:25nodes} we compare the optimal-like transmission graph to the transmission graphs given by both Problem \ref{opt_dynlower} and Problem \ref{opt_dynupper}. Even though the transmission graphs  appear very different, the results in term of energy consumption are very similar. In particular, for the considered example, we observe a maximum loss of performance of $20\%$, while lowering dramatically computation time. It is important to recognize that the solution space of the optimal-like solution in case of $25$ sensors is of the order of $2^{25} \approx 3\times 10^7$, so this solution is really impractical to evaluate for medium to large sensor networks.

\subsection{Example 2}

We also compare our algorithm with a greedy method that, starting from the fusion center $s_0$, selects the minimum cost sensors stopping when the requirement on the estimation performance $P_k<P_d$ is satisfied.

\begin{figure}
  \begin{center}
    \includegraphics[width = 0.45\textwidth]{fig_comparison}
  \end{center}
  \caption{Mean value, over $50$ random generated placements of the sensors, of the energy consumption of Algorithm $1$ normalized respect to the greedy algorithm for $m=30,35,40,45,50$} \label{fig:comparison}
\end{figure}

We consider again the discretized model of the heat process in a planar closed region. In Figure \ref{fig:comparison}, we compare the performance of our method, implemented for both Problem \ref{opt_dynlower} and Problem \ref{opt_dynupper}, with the greedy method. In particular, we evaluate over $50$ random generated placements of $m$ sensors the mean value of energy consumption of the optimal solution of Algorithm $1$ normalized with respect to the greedy algorithm. We repeat this comparison for different numbers $m$ of sensors. Figure \ref{fig:comparison} clearly shows that the proposed algorithm performs much better than the greedy algorithm and that performance gap tends to increase the number of available sensors.
