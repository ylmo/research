\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Sensor Selection]{A Convex Optimization Approach of Sensor Selection}
\author[Yilin Mo]{Yilin Mo$^*$, Roberto Ambrosino$^{\dag}$ and Bruno Sinopoli$^*$}
\institute[Carnegie Mellon University]{$*$: Department of Electrical and Computer Engineering,\\ Carnegie Mellon University\\
$\dag$: Dipartimento per le Tecnologie,\\ Universit\`{a} degli Studi di Napoli Parthenope\\}
\date[Allerton Conference 2009]{Allerton 2009}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\begin{frame}{Wireless Sensor Networks}
  \begin{itemize}
    \item Wireless Sensor Networks (WSNs) are becoming ubiquitous, with lots of interesting applications. 
    \item Sensors are usually battery powered. 
    \item Transmission costs too much energy.
    \item Solutions: new devices, energy efficient physical layer, topology control, \alert{sensor selection}\dots
  \end{itemize}
\end{frame}
\begin{frame}{Motivation}
  \begin{itemize}
    \item Suppose $m$ unique sensors measuring the same quantity, the error covariance of the estimation is the covariance of each measurement divided by $m$.
      \begin{displaymath}
	\sigma^2 = \sigma_y^2 / m.
      \end{displaymath}
    \item The energy cost is at least proportional to $m$.
      \pause
    \item Depending on application, not all sensors are needed!!
    \item Put sensors to sleep to save energy.
  \end{itemize}
  \begin{figure}[H]
    \begin{center}
      \includegraphics{diagram.1}
      \caption{Sensors Selected at time $k$} \label{fig:sensor_topology_example}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{System Model}
  We consider the WSN is monitoring the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{equation}
	\begin{split}
	  x_{k+1} &= Ax_k +  w_k,\\
	  y_{k} &= C x_k + v_k.
	\end{split}
	\label{eq:systemdiscription}
      \end{equation}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors. $y_{k,i}$ is the measurement of sensor $i$ at time $k$.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman filter}
       A Kalman filter is implemented at the fusion center to perform state estimation, which takes the following form:	
	\begin{block}{Kalman Filter}
\begin{align}
    \hat x_{0|0} & = \bar x_0 ,\quad P_{0|0}  = \Sigma\\
    \hat x _{k + 1|k} & = A \hat x_{k|k}  , \quad P_{k + 1|k}  = AP_{k|k} A^H  + Q \nonumber\\
    K_k& = P_{k|k - 1}  C^H\gamma_k (\gamma_k CP_{k|k - 1}  C^H \gamma_k + \gamma_k R\gamma_k)^{ - 1}  \nonumber\\
    \hat x_{k|k}& = \hat x_{k|k - 1}  + K_k\gamma_k (y_k  - C \hat x _{k|k - 1}^* )  \nonumber\\
    P_{k|k}& = P_{k|k - 1}  -  K_k \gamma_k CP_{k|k - 1}  \nonumber,
  \end{align}
	\end{block}
	where $\hat x_{k|k}$ is the state estimation at time $k$, $P_{k|k}$ is the estimation error covariance matrix.
\end{frame}


  \begin{frame}{Problem Formulation}
    \begin{itemize}
  \item Define $\gamma_{k,i}$ as the binary variable such that $\gamma_{k,i}=1$ if sensor $i$ transmits during the time step $k$.
      \item Define $\gamma_k \triangleq diag(\gamma_{k,1},\ldots,\gamma_{k,m})$. 
      \item Define $\vec \gamma \in \mathbb R^{mT}$ to be a vector of $\gamma_{k,i},\;k=1,\ldots,T,\,i=1,\ldots,m$.
    \end{itemize}
    \begin{block}{Sensor Selection Problem}
      \begin{align*}\label{opt_prob}
	 \min_{\vec\gamma}&\quad \quad \sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T),\\
	s.t.&\quad  \quad  H \vec \gamma \leq b, \nonumber\\
	& \quad \quad \gamma_{k,i} = 0\;or\;1,\;k=1,\ldots,T,\;i=1,\ldots,m \nonumber
      \end{align*}
      where $\mathcal Q_k \in \mathbb R^{n' \times n}$ and is of full row rank,  $H \in R^{h \times mT}$ and $b\in R^h$.
    \end{block}
  \end{frame}

  \begin{frame}{Possible Objective Functions}
    \begin{itemize}
      \item Minimization of the final estimation error:
	\[\mathcal Q_1,\ldots,\mathcal Q_{T-1} = 0,\;\mathcal Q_T = I \]
	\[\Rightarrow 
	\sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T)=trace(P_{T|T})
	\]
      \item Minimization of the average estimation error:
	\[\mathcal Q_k = I, k = 1,\ldots,T\]
	\[\Rightarrow 
	\sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T)=\sum_{k=1}^T  trace(P_{k|k})
	\]      
    \end{itemize}
  \end{frame}

  \begin{frame}{Possible Objective Functions}
    \begin{itemize}
      \item Minimization of objective function of LQG control
	\begin{displaymath}
	  J_T = E\left[x_T^T W_T x_T + \sum_{k=1}^{T-1} (x_k^TW_kx_k+u_k^TU_ku_k)\right],
	\end{displaymath}
	The optimal value of cost function given by LQG controller is 
	\begin{displaymath}
	  \begin{split}
	    J_T^* &=  \sum_{k=1}^{T-1}trace(S_{k+1}Q)+ \sum_{k=1}^{T-1}trace[(A^TS_{k+1}A+W_k-S_k)P_{k|k}]\\
	   &+ trace(S_1\Sigma),
	  \end{split}
	\end{displaymath}
where $S_k$ is defined recursively as
    \begin{displaymath}
      S_{k-1} = A^TS_{k}A+W_k- A^TS_{k}B(B^TS_{k}B+U_k)^{-1}B^TS_{k}A,\,S_T=W_T.
    \end{displaymath}
	To find the optimal sensor selection schedule, we can let \[\mathcal Q_k^T\mathcal Q_k = A^TS_{k+1}A + W_k - S_k , \mathcal Q_T = 0.\]
    \end{itemize}
  \end{frame}

  \begin{frame}{Possible Constraints}
    \begin{itemize}
      \item Fixed number of sensors to be used at each time step
	\[\sum_{i=1}^m\gamma_{k,i} \leq p, \;k=1,\ldots,T\]
	where $p$ is the number of sensor selected in each step.
      \item Sensor energy constraints
	\[e_i\sum_{k=1}^T \gamma_{k,i} \leq \mathcal E_i,\;i =1,\ldots,m\]
	where $\mathcal E_i$ and $e_i$ are respectively the initial energy and the transmission energy consumption of the sensor $i$. 
      \item Topology Constraints
	\[\gamma_{k,i} \geq \gamma_{k,j},\;k =1,\ldots,T,\;j \in \mathcal C_i\]
	where $\mathcal C_i$ is the set of child nodes of sensor $i$.  
    \end{itemize}
  \end{frame}

  \begin{frame}{Possible Constraints}
    \begin{itemize}
      \item  Limited number of switches

	Suppose that we only want sensor $i$ to switch between sleep state and active state no more than $q$ times 

	\begin{displaymath}
	  \sum_{k=1}^{m-1}\left|\gamma_{k+1,i} -\gamma_{k,i} \right| \leq q
	\end{displaymath}
    \end{itemize}

    \begin{block}{Sensor Selection Problem}
      \begin{align*}\label{opt_prob}
	 \min_{\vec\gamma}&\quad \quad \sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T),\\
	s.t.&\quad  \quad  H \vec \gamma \leq b, \nonumber\\
	& \quad \quad \gamma_{k,i} = 0\;or\;1,\;k=1,\ldots,T,\;i=1,\ldots,m \nonumber
      \end{align*}
      where $\mathcal Q_k \in \mathbb R^{n' \times n}$ and is of full row rank,  $H \in R^{h \times mT}$ and $b\in R^h$.
    \end{block}
  \end{frame}
  
  \begin{frame}{Reformulation of Estimation Error Covariance}
    \begin{enumerate}
      \item Let us first only consider estimation error covariance $P_{1|1}$.
	\begin{block}{}
	  \begin{align}
	    K_1& = P_{1|0}  C^H\gamma_1 (\gamma_1 CP_{1|0}  C^H \gamma_1 + \gamma_1 R\gamma_1)^{ - 1}  \nonumber\\
	    P_{1|1}& = P_{1|0}  -  K_1 \gamma_1 CP_{1|0}  \nonumber,
	  \end{align}
	\end{block}
      \item The relation between $P_{1|1}$ and $\gamma_1$ is very complicated
      \item However, we find that
	\begin{equation}
	  P_{1|1} = P_{1|1}^* + (K_1\gamma_1 - K_1^*)Cov(y_1)(K_1\gamma_1 - K_1^*)^T,
	\end{equation}
	where $K_1^*,\,P_{1|1}^*$ are the Kalman gain and error covariance matrix when all the sensor data are used, i.e., $\gamma_1 = I$.
    \end{enumerate}
  \end{frame}

  \begin{frame}{Reformulation of Estimation Error Covariance}
    \begin{theorem}
      Consider the LTI system. The estimation error covariance matrix $P_{k|k}$ satisfies the following equality
      \begin{equation}
	P_{k|k} = P_{k|k}^* + (G_k\Gamma_k-G_k^*)Cov(Y_k)(G_k\Gamma_k-G_k^*)^T,
      \end{equation}
      where $P_{k|k}^*$ is the estimation error covariance when all the sensor data are used, $Y_k = [y_1^T,\ldots,y_k^T]^T$, $\Gamma_k = diag(\gamma_1,\ldots,\gamma_k)$. $G_k,\,G_k^*$ satisfy the following recursive equations
      \begin{align*}
	G_{1}^* &= K^*_1,\,&G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*],\\
	G_{1} &= K_1,\,&G_{k+1} = [(A-K_{k+1}CA)G_k,\;K_{k+1}].
      \end{align*}
    \end{theorem}
  \end{frame}

  \begin{frame}{Minimization Problem}
    We can relax the original problem into
    \begin{block}{}
      \begin{align*}
	\min_{\mathcal G_k,\vec{\gamma}}& \quad \quad \sum_{k=1}^T trace\left[ (\mathcal G_k - \mathcal Q_kG_k^*)Cov(Y_k)(\mathcal G_k  - \mathcal Q_kG_k^*)^T\right],\\
	s.t.& \quad \quad  H \vec \gamma \leq b \\ \nonumber
	&\quad \quad \gamma_{k,i} \geq \left\|\sum_{j = k}^T \left\|{\vec {\mathcal G}}_{j,(k-1)m+i}\right\|_1\right\|_0,
      \end{align*}
    \end{block}
    where $\mathcal G_k =\mathcal Q_k G_k\Gamma_k$.
  \end{frame}


  \begin{frame}{Illustrative Example}
    Consider a system composed of
    \begin{itemize}
      \item  $m=30$ sensors 
      \item $n=20$ states 
      \item $A,\,C$ randomly chosen matrices 
      \item $Q,\,R$ randomly chosen symmetric positive defined matrices 
    \begin{block}{Sensor Selection Problem}
      \begin{align*}\label{opt_prob}
	 \min_{\vec\gamma}&\quad \quad  trace(P_{1|1}),\\
	 s.t.&\quad  \quad  \sum_i^m \gamma_{1,i} \leq p, \nonumber\\
	& \quad \quad \gamma_{k,i} = 0\;or\;1,\;k=1,\ldots,T,\;i=1,\ldots,m \nonumber
      \end{align*}
    \end{block}   
    \end{itemize}
  \end{frame}

  \begin{frame}{Illustrative Example}
    We consider the estimation performance both of our strategy and a random strategy and we normalize them respect to the optimal one.
    \begin{figure}[H]
      \begin{center}
	\includegraphics[width=0.6\textwidth]{performancegap.eps}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Conclusion}
In this presentation,  we considered the sensor selection problem.
\begin{itemize}
  \item  A convex optimization framework to solve the problem is provided.
  \item  A numerical example illustrates that the proposed strategy is close to the optimal one.
\end{itemize}
Possible future works:
\begin{itemize}
  \item Distributed sensor selection strategies
  \item Robust sensor selection strategies in case of lossy networks
\end{itemize}
  \end{frame}

  \begin{frame}
    \begin{center}
      \Huge{ Thank you for your time!!\\
      Questions?}
    \end{center}
  \end{frame}

  \end{document}

