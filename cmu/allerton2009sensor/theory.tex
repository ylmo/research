In this section we would like to prove several useful facts about multi-step sensor selection. 

\subsection{Improvement of Multi-step}
Let us consider the following  two sensor selection problems\footnote{For simplicity we just choose $\mathcal Q_k= I$}:

\begin{align*}
  (\dag):&\min_{\vec\gamma} && \sum_{k=1}^{T} trace(P_{k|k}),\\
  &s.t.&& H\vec \gamma \leq b ,\nonumber\\
  &&& \gamma_i = 0\;or\;1,\;i=1,\ldots,mT, \nonumber
\end{align*}

\begin{align*}
  (\ddag):&\min_{\vec\gamma} && \sum_{k=1}^{2T} trace(P_{k|k}),\\
  &s.t.&&  \left[ {\begin{array}{*{20}c}
	H&0\\
	0&H
  \end{array}} \right]  \vec \gamma \leq \left[ {\begin{array}{*{20}c}
	b\\
	b	
  \end{array}} \right] , \nonumber\\
  &&& \gamma_i = 0\;or\;1,\;i=1,\ldots,2mT, \nonumber
\end{align*}

Suppose we want to find the optimal sensor selection scheme $\gamma_1,\ldots,\gamma_{2mT}$ from time $1$ to time $2T$ for problem $(\ddag)$. We can directly solve problem $(\ddag)$. Or we can first solve problem $(\dag)$ from time $1$ to time $T$, then solve problem $(\dag)$ again for time $T+1$ to $2T$, which will lead to a suboptimal solution. We would like to know how much performance we lose for the suboptimal schedule $(\dag)$ with respect to the optimal schedule $(\ddag)$.

Let us denote the $P_{k,k},\,\underline P_{k|k}$ to be the estimation error covariance at time $k$ of schedule $(\dag),\,(\ddag)$ respectively. Define 
\begin{equation}
  \rho = \inf\{r \in \mathbb R^+ |P_{T|T}\leq (r+1)\underline P_{T|T}\}.
\end{equation}
\begin{equation}
  \alpha = \sup\{r \in \mathbb R^+ |Q \geq r (A\underline P_{k|k}A^T),\,k=T+1,\ldots,2T\}.
\end{equation}
\begin{equation}
  \beta = \sup\{trace(\underline P_{k|k})|k=T+1,\ldots,2T\}
\end{equation}

We have the following theorem to characterize the loss of performance of schedule $(\dag)$ with respect to $(\ddag)$:
\begin{theorem}
  \label{theorem:multigain}
 The loss of performance of schedule $(\dag)$ with respect to $(\ddag)$ satisfies the following inequality:
 \begin{equation}
   \sum_{k=1}^{2T}trace(P_{k|k}) - \sum_{k=1}^{2T} trace(\underline P_{k|k}) \leq \frac{\rho\beta}{\alpha}
 \end{equation}
\end{theorem}

Before we continue, we need the following Lemmas:
\begin{lemma}
  Let $g(X,S) = X+S$, where $X,S \in \mathbb R^{n\times n}$ are positive semidefinite. Then \footnote{All the comparison between matrices in this section is in the sense of positive semidefinite}
  \begin{equation}
    g( (1+\rho) X,S) \leq (1+\frac{\rho}{1+\alpha})g(X,S),
  \label{eq:ginequality}
  \end{equation}
  if $S \geq \alpha X$.
\end{lemma}
\begin{proof}
  \begin{align*}
    &g( (1+\rho) X,S) - (1+\frac{\rho}{1+\alpha})g(X,S)\\
    &=(1+\rho)X+S - (1+\frac{\rho}{1+\alpha})(X+S)\\
    &=\frac{\rho}{1+\alpha}(\alpha X - S) \leq 0
  \end{align*}
\end{proof}

\begin{lemma}
  Let $h(X,S) = (X^{-1}+S)^{-1}$, where $X,S \in \mathbb R^{n\times n}$ are positive semidefinite. Then
  \begin{equation}
  h((1+\rho) X,S) \leq (1+\rho) h(X,S), \forall \rho > 0
  \label{eq:hinequality}
  \end{equation}
\end{lemma}
\begin{proof}
  Since $X,S$ are both positive semidefinite, we know that there exist matrix $U \in \mathbb R^{n\times n}$ that can diagonalize $X^{-1},\,S$ simultaneously, which means that
  \begin{equation}
    X^{-1} = U^{-1} (U^{-1})^T,\;S = U^{-1}\Lambda (U^{-1})^T ,
  \end{equation}
  where $\Lambda = diag(\lambda_1,\ldots,\lambda_n)$ is diagonal and $\lambda_i \geq 0$. Hence
  \begin{align*}
&  h((1+\rho) X,S) - (1+\rho) h(X,S)\\
& = U^T\left\{\left[(1+\rho)^{-1}I + \Lambda\right]^{-1} - (1+\rho)(I+\Lambda)^{-1}   \right\}U \\
& = U^T diag(\ldots,\frac{1+\rho}{1+(1+\rho)\lambda_i}-\frac{1+\rho}{1+\lambda_i},\ldots) U 
\end{align*}
It is easy to see that the diagonal matrix in the above equation is negative semidefinite. Thus,
  \begin{displaymath}
  h((1+\rho) X,S) \leq (1+\rho) h(X,S), \forall \rho > 0
  \end{displaymath}
\end{proof}

\begin{lemma}
  Suppose that $X,Y \in \mathbb R^{n\times n}$ are positive semidefinite and $X \geq Y$, then
  \begin{equation}
  g(X,S) \geq g(Y,S),\;h(X,S) \geq h(Y,S).  
  \label{eq:ghincreas}
  \end{equation}
\end{lemma}
\begin{proof}
  The proof is trivial from the definition of positive semidefinite matrix.
\end{proof}

Now we are ready to prove Theorem~\ref{theorem:multigain}.

\begin{proof}
  We construct the third possible sensor schedule $(\star)$. The $(\star)$ schedule uses the same schedule as $(\dag)$ from time $1$ to time $T$, and from time $T+1$ to time $2T$ it uses the schedule as $(\ddag)$. We denote the estimation error covariance of schedule $(\star)$ at time $k$ to be $\overline P_{k|k}$.
  
By the definition problem $(\dag)$, we know that from time $1$ to $T$, the schedule $(\dag)$ is better than schedule $(\ddag)$, which means that
\begin{equation}
  \sum_{k=1}^T trace(\overline P_{k|k}) = \sum_{k=1}^T trace(P_{k|k}) \leq \sum_{k=1}^T trace(\underline P_{k|k}).  
  \label{eq:windowone}
\end{equation}

From time $T+1$ to time $T$, the $(\dag)$ schedule is the optimal schedule for initial condition $P_{T|T}$. Since the $(\star)$ schedule have the same initial condition $\overline P_{T|T} = P_{T|T}$, it is worse than $(\dag)$ schedule, which implies that
\begin{equation}
  \sum_{k=T+1}^{2T} trace(\overline P_{k|k}) \geq \sum_{k=T+1}^{2T} trace(P_{k|k}).
  \label{eq:windowtwo}
\end{equation}

Combining \eqref{eq:windowone} and \eqref{eq:windowtwo}, we get
\begin{align*}
   &\sum_{k=1}^{2T}trace( P_{k|k}) - \sum_{k=1}^{2T} trace(\underline P_{k|k})\\
   &\leq  \sum_{k=T+1}^{2T} trace(\overline P_{k|k})-\sum_{k=T+1}^{2T} trace(\underline P_{k|k}).
\end{align*}

Staring at time $T$, we know that $\overline P_{T|T} = P_{T|T} \leq (1+\rho) \underline P_{T|T}$. The predict update at time $T+1$ is
\begin{align*}
  \overline P_{T+1|T} &= A\overline P_{T|T}A^T + Q = g(A\overline P_{T|T}A^T, Q)\\
  \underline P_{T+1|T} &= A\underline P_{T|T}A^T + Q = g(A\underline P_{T|T}A^T, Q)
\end{align*}
Hence,
\begin{align*}
  &\overline P_{T+1|T} = g(A\overline P_{T|T}A^T, Q)\\
  & \leq g( (1+\rho) A\underline P_{T|T}A^T, Q) \leq (1+\frac{\rho}{1+\alpha})g(A\underline P_{T|T}A^T,Q)\\
  &=(1+\frac{\rho}{1+\alpha})\underline P_{T+1|T}
\end{align*}
The first and second inequality are direct sequence of \eqref{eq:ghincreas} and \eqref{eq:ginequality}.

At time $T+1$, by definition of $(\star)$ schedule, the $(\ddag)$ and $(\star)$ schedule choose the same sensors. Suppose the corresponding measurement is 
\begin{equation}
  y_{T+1} = C_{T+1}x_{T+1} + v_{T+1},
\end{equation}
where $v_{T+1} \sim \mathcal N(0,R_{T+1})$. Using information filter, we can write the measurement update at time $T+1$ as  
\begin{align*}
  \overline P_{T+1|T+1} &= \left[\overline P_{T|T}^{-1} + C_{T+1}^TR_{T+1}^{-1}C_{T+1}\right]^{-1}\\& = h(\overline P_{T+1|T}, C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  \underline P_{T+1|T+1} &= \left[\underline P_{T|T}^{-1} + C_{T+1}^TR_{T+1}^{-1}C_{T+1}\right]^{-1} \\&= h(\underline P_{T+1|T}, C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
\end{align*}
Hence,
\begin{align*}
  &\overline P_{T+1|T+1}  = h(\overline P_{T+1|T}, C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  &\leq h( (1+\frac{\rho}{1+\alpha})\underline P_{T+1|T}, C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  &\leq (1+\frac{\rho}{1+\alpha})h( \underline P_{T+1|T}, C_{T+1}^TR_{T+1}^{-1}C_{T+1})= (1+\frac{\rho}{1+\alpha})\underline P_{T+1|T+1}
\end{align*}
The first and second inequality are direct sequence of \eqref{eq:ghincreas} and \eqref{eq:hinequality}.

By induction, we know that
\begin{equation}
  \overline P_{T+k|T+k}\leq \left[1+\frac{\rho}{(1+\alpha)^k}\right]\underline P_{T+k|T+k},\,k=1,\ldots,T .
\end{equation}

Hence,
\begin{align*}
  &\sum_{k=T+1}^{2T} trace(\overline P_{k|k})-\sum_{k=T+1}^{2T} trace(\underline P_{k|k}) \\
  &\leq \sum_{k=1}^{T} \frac{\rho}{(1+\alpha)^k}trace(\underline P_{T+k|T+k})\\
  &\leq \sum_{k=1}^{T} \frac{\rho}{(1+\alpha)^k}\beta \leq \frac{\rho\beta}{\alpha}
\end{align*}
\end{proof}
