\documentclass[letterpaper,10 pt,conference]{IEEEconf}
% use above line letter sized paper
\overrideIEEEmargins
% Needed to meet printer requirements.

%DEBUG:
\IEEEoverridecommandlockouts

\usepackage{color}
\usepackage[dvips]{graphicx} 
\DeclareGraphicsRule{*}{mps}{*}{}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{collary}{Collary}


\title{\LARGE \bf {Multi-Step Sensor Selection via Convex Optimization}}

\author{Yilin Mo, Roberto Ambrosino, Bruno Sinopoli
\thanks{
This research was supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
\thanks{
Yilin Mo and Bruno Sinopoli are with the ECE department of Carnegie Mellon University, Pittsburgh, PA {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
Roberto Ambrosino is with the Dipartimento per le Tecnologie, Universit\`{a} degli Studi di Napoli Parthenope, Centro Direzionale di Napoli, Isola C4, 80143 Napoli, Italy 
}
}

\begin{document}
\maketitle
\begin{abstract}
  put abstract here
\end{abstract}
\section{Introduction}
put motivation, previous work and system diagram here

\section{Problem Formulation}
This section is devoted to derive a general framework which can capture a large number of sensor selection problems.

Consider a linear system
\begin{equation}
  \begin{split}
    x_{k+1} &= Ax_k + w_k,\\
    y_{k} &= C x_k + v_k.
  \end{split}
  \label{eq:systemdiscription}
\end{equation}
$w_k,v_k,x_1$ are independent Gaussian random variables, and $x_1 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. We suppose that $x_k \in \mathbb R^n$ and $y_k = [y_{k,1} ; y_{k,2} ,\ldots, y_{k,m}  ]^T \in \mathbb R^m$ is the vector of the measurements from the sensors. Each element $y_{k,i}$ represents the measure of the sensor $i$ at time $k$. 

Let us indicate with $\gamma_{j}$  for $j=1\dots mT$ the set the binary variable such that $\gamma_{m(k-1)+i}=1$ if sensor $i$ successfully transmits during the step time $k$, for $i=1 \dots m$ and $k=1 \dots T$; otherwise it is $0$. Thus, the at time $k$, the estimator receives readings $[\gamma_{m(k-1)+1}y_{k,1},\ldots,\gamma_{m(k-1)+m}y_{k,m}]^T$.

The aim of this paper is to find a sequence of sensor selection scheme $\gamma_1,\ldots,\gamma_{mT}$ from time $1$ to time $T$, which gives the best estimation performance while satisfying certain energy and topology constraints.

In order to define a multi-step Kalman filter with sensor selection scheme, let us introduce the following quantities
\begin{align}
  Y_k &\triangleq  [y_1^T,y_2^T,\ldots,y_k^T]^T &\in& \mathbb R^{mk},\\
  \Gamma_k &\triangleq diag(\gamma_1,\ldots,\gamma_{mk}) &\in& \mathbb R^{mk\times mk},\\
  \vec \gamma &\triangleq [\gamma_1,\ldots,\gamma_{mk}]^T &\in& \mathbb R^{mk},\\
  \hat x^*_{k|k} &\triangleq E\left(x_k|Y_k\right)&\in&\mathbb R^{n},\\
  P^*_{k|k} &\triangleq Cov\left(x_k|Y_k\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k|k} &\triangleq E\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n},\\
  P_{k|k} &\triangleq Cov\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}

The multi-step sensor selection problem can be formulated as the following optimization problem
\begin{align*}
  &\min_{\vec\gamma} && \sum_{k=1}^T trace(T_kP_{k|k}T_k^T),\\
  &s.t.&&  H \vec \gamma \leq b,\\
  &&& \gamma_i = 0\;or\;1,\;i=1,\ldots,mT,
\end{align*}
where $T_k \in \mathbb R^{n' \times n}$ and is of full row rank,  $H$ is a matrix and $b$ is a vector. Although the formulation seems to be very restrictive at the first glance, a lot of problems can actually be taken into this framework. We would like to discuss several typical objective and constraints.
\begin{enumerate}
  \item Minimizing the estimation error of single state: 
    Suppose that at time $k$ we are only interested in the $i$th state $x_{k,i}$ of the system. Thus, we can choose \[T_k = [\delta_{1,i},\ldots,\delta_{n,i}],\] where $\delta_{i,j} = 0$ if $i \neq j$, it is $1$ otherwise.
  \item Minimizing the final estimation error:
    Suppose that we only want to minimize the estimation error at time $T$. Thus, we can let \[T_1,\ldots,T_{T-1} = 0,\;T_T = I.\]  
  \item Minimizing the average estimation error:
    Suppose that we want to minimize the average estimation error from time $1$ to time $T$. Thus, we can choose \[T_k = I, k = 1,\ldots,T.\]
  \item Minimizing the objective function of finite horizon LQG problem:
    Suppose that we have the following LQG cost function  
    \begin{displaymath}
      J_T = E\left[x_T^T W_T x_T + \sum_{k=1}^{T-1} (x_k^TW_kx_k+u_k^TU_ku_k)\right],
    \end{displaymath}
    where $W_k, U_k$ are some positive semidefinite matrices. Given a sensor selection schedule, it is well known that the optimal controller and estimator, which minimize the LQG cost function, can be designed separately. The optimal control law is 
    \begin{displaymath}
      \begin{split}
	S_k &= A^TS_{k+1}A+W_k\\
	&- A^TS_{k+1}B(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A,\\
	u_k &= -(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A\hat x_{k|k},
      \end{split}
    \end{displaymath}
where $S_T = W_T$. The optimal estimator is still the Kalman filter. The optimal value of cost function is
\begin{equation}
  \begin{split}
  J_T^* &= trace(S_1\Sigma)+ \sum_{k=1}^{T-1}trace(S_{k+1}Q) 
  \\&+ \sum_{k=1}^{T-1}trace[(A^TS_{k+1}A+W_k-S_k)P_{k|k}],
\end{split}
\end{equation}
Thus, in order to find the best sensor selection schedule which minimizes $J_T^*$, we can let \[T_k^TT_k = A^TS_{k+1}A + W_k - S_k , T_T = 0.\]
  \item Fix number of sensors at each time step:
    Suppose that at each time step we only want to select no more than $p < m $ sensors. Thus, the constraints can be written as 
    \[
    \sum_{i=1}^m\gamma_{m(k-1)+i} \leq p, \;k=1,\ldots,T
    \]
  \item Ensure the life time of network:
    Suppose that each sensor has a initial energy of $\mathcal E_1,\ldots,\mathcal E_m$ and each observation will consume $e_1,\ldots,e_m$ energy respectively. We want to make sure that no sensor dies at time $T$. Thus, the energy constraints can be written as 
\[
e_i\sum_{k=1}^T \gamma_{m(k-1)+i} \leq \mathcal E_i,\;i =1,\ldots,m
\]

  \item Multi-hop sensor networks:
    Consider that the sensor network has a tree structure and the estimator is the root node. Define the set of child nodes of sensor $i$ to be $\mathcal C_i$. We can write the topology constraints of the network as
    \[
    \gamma_{m(k-1)+i} \geq \gamma_{m(k-1)+j},\;k =1,\ldots,T,\;j \in \mathcal C_i 
    \]
\end{enumerate}
\end{document}
