\documentclass{ifacconf}
\usepackage{color}
\usepackage{ifpdf}
\ifpdf
  \usepackage[pdftex]{graphics}
  \DeclareGraphicsRule{*}{mps}{*}{}
\else
  \usepackage[dvips]{graphicx}
\fi

\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\interdisplaylinepenalty=2500
\usepackage{amssymb}
%\usepackage{cite}
\usepackage{natbib}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\begin{document}
\begin{frontmatter}

\title {Kalman Filtering with Intermittent Observations: Critical Value for Second Order System}

\thanks{
This research was supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
\author[First]{Yilin Mo, Bruno Sinopoli}
\address[First]{
Carnegie Mellon University, Pittsburgh, PA 15232, USA (e-mail: ymo@andrew.cmu.edu, brunos@ece.cmu.edu)}


\begin{abstract}
  \cite{Sinopoli:04} analyze the problem of optimal estimation for linear Gaussian systems where packets containing observations are dropped according to an i.i.d.  Bernoulli process, modeling a memoryless erasure channel. In this case the authors show that the Kalman Filter is still the optimal estimator, although boundedness of the error depends directly upon the channel arrival probability, $p$. In particular they also prove the existence of a critical value, $p_c$, for such probability, below which the Kalman filter will diverge. While it has been shown that the critical value for diagonalizable systems with eigenvalues of different magnitude coincides with the lower bound determined by \cite{Yilin08}, the problem is still open in the case where some eigenvalues have equal magnitude. This paper provides a complete characterization of the critical arrival probability for diagonalizable second order systems with eigenvalues of equal magnitude. In general the critical value for these systems is higher than the lower bound, unless the transmission from the sensor includes both the current and the previous measurement. In this case it is possible to construct a filter that whose critical value achieves the lower bound. Although clearly restrictive, the analysis of second order systems presented herein can be used to bound the critical value of higher dimensional systems of this kind. 
\end{abstract}
\begin{keyword}
	Linear Estimation, Stochastic Systems
\end{keyword}
\end{frontmatter}

\section{Introduction}
\label{sec:introduction}
A large wealth of applications demands wireless communication among small embedded devices. Wireless Sensor Network (WSN) technology provides the architectural paradigm to implement systems with a high degree of temporal and spatial granularity. Applications of sensor networks are becoming ubiquitous, ranging from environmental monitoring and control to building automation, surveillance and many others \citep{wireless_sensor_network}. Given their low power nature and the requirement of long lasting deployment, communication between devices is limited in range and reliability. Changes in the environment, such as the simple relocation of a large metal object in a room or the presence of people, will inevitably affect the propagation properties of the wireless medium. Channels will be time-varying and unreliable. Spurred by this consideration, our effort concentrates on the design and analysis of estimation and control algorithms over unreliable networks. 

A substantial body of literature has been devoted to such issues in the past few years. In this paper, we want to briefly revisit the paper of ~\cite{Sinopoli:04}. In that paper, the authors analyze the problem of optimal state estimation for discrete-time linear Gaussian systems, under the assumption that observations are sent to the estimator via a memoryless erasure channel. This implies the existence of a non-unitary arrival probability associated with each packet. Consequently some observations will inevitably be lost. In this case although the discrete Kalman Filter \citep{kalmanoriginal} is still the optimal estimator, the boundedness of its error depends on the arrival probabilities of the observation packets. In particular ~\cite{Sinopoli:04} prove the existence of a critical arrival probability $p_c$, below which the expectation of the estimation error covariance matrix $P_k$ of the Kalman filter will diverge.  The authors were not able to compute the actual value of this critical probability for general linear systems, but could provide upper and lower bounds. For special cases, e.g. when $C$ matrix is invertible, they were able to show that the upper and lower bounds coincide and hence the exact critical value is obtained.

%add references to gupta, hespanha, murray, Kumar, Elia, Bullo
%Philosophically such a phenomenon is qualitatively related to the well known uncertainty principle \citep{Athans:77, Ku:77}, which states that the optimal long-range control of a linear system with uncertain parameters does not exist if the uncertainty exceeds certain threshold. For Kalman filtering with intermittent observations, the uncertainty is caused by the random packet loss and the optimal Kalman filter becomes unstable (i.e. the expectation of $P_k$ is bounded) if too many packets are dropped. 

A substantial amount of research has been made to analyze the system with intermittent observations. One interesting direction is to characterize critical value for more general linear systems. \cite{kp-fb:07j} relax the invertible condition on $C$ matrix to $C$ only invertible on the observable subspace. \cite{Yilin08} prove that if the eigenvalues of system $A$ matrix have distinct absolute values, then the lower bound is indeed the critical value. The authors also provide a counter example to show that in general the lower bound is not tight. 

Another way to characterize the performance of Kalman filtering with intermittent observation is to directly calculate the probability distribution of estimation error covariance matrix $P_k$ instead of only considering the boundedness of its expectation. \cite{censi09} gives a closed-form expression for cumulative distribution function of $P_k$ when the system satisfies non-overlapping conditions. \cite{Ali:09} provide a numerical method to calculate the eigen-distribution of $P_k$ under the assumption that the observation matrix $C$ is random and time varying. \cite{shilingprobability} consider the probability of $P(P_k\leq M)$, and derive upper and lower bound on such probability. However, only in specific cases these upper and lower bounds coincide.

Other variations of the original problem are also considered. \cite{Xu:05} introduce smart sensors, which send the local Kalman estimation instead of raw observation. In \cite{Craig:07}, a similar scenario is discussed where the sensor sends a linear combination of the current and previous measurement.  A Markovian packet dropping model is introduced by \cite{Minyi:07} and a stability criterion is given. \cite{Liu:04} study the case where the observation at each time splits into two parts, which are sent to the Kalman filter through two independent erasure channels. A more general model, which considers packet drop, delay and quantization of measurements at the same time, is introduced by \cite{xielihua09}.

As is shown by \cite{Yilin08}, the problem with computing the critical value complicates when eigenvalues have the same absolute value. In this paper we wish to give a complete characterization of the critical value for second order systems, including the case where eigenvalues are located on the same circle. Even though the limitation to second order system may look restrictive, we argue that the critical value for such systems provides a lower bound for the critical value of larger systems characterized by more complex eigenstructure. Computing the critical value for all possible eigenstructures is a formidable problem since, as shown in the paper, the critical value itself is a surprisingly complicated function of $A$ and $C$ matrices, which is not even continuous. Nonetheless, we think the derivation of critical value for second order systems provides a valuable insight for understanding the one of general linear systems.

The paper is organized in the following manner: Section~\ref{sec:formulation} formulates the problem and reviews some results from the literature. Section~\ref{sec:main} provides a complete characterization of the critical value for second order systems and Section~\ref{sec:conclusion} concludes the paper. Several proofs are reported in the Appendix for the sake of legibility.

\section{Problem Formulation} \label{sec:formulation}
Consider the following linear system
\begin{equation}
  \begin{split}
    x_{k + 1}  &= A x_k  + w_k , \\
    y_k  &= C x_k  + v_k  ,\\
  \end{split}
  \label{eq:systemdiscription}
\end{equation}
where $x_k\in \mathbb {R}^n$ is the state vector, $y_k \in \mathbb{R}^m$ is the output vector, $w_k \in \mathbb{R}^n$ and $v_k \in \mathbb{R}^m$ are Gaussian random vectors with zero mean and covariance matrices $Q > 0$ and $R > 0$, respectively. Assume that the initial state, $x_0$ is also a Gaussian vector of mean $\bar x_0$ and covariance matrix $\Sigma_0 > 0$. Let $w_i,v_i,x_0$ to be mutually independent. Define $|\lambda_1| \geq |\lambda_2| \geq \cdots \geq |\lambda_n|$ as the eigenvalues of $A$.

Throughout this paper we always assume that
\begin{enumerate}
	\item $A$ is unstable\footnote{If $A$ is stable, we always have a bounded estimation error covariance matrix even no packet arrives.}. 
	\item $(C,A)$ is detectable.
	\item $Q,\,R,\,\Sigma_0$ are positive definite.
	\item $A$ is diagonalizable\footnote{It is possible that certain systems, such as the double integrator, do not satisfy the diagonalizability requirement. However, we believe that most of the interesting systems are diagonalizable.}. As a result, we assume the system is already in its diagonal form.
\end{enumerate}

Consider the case where observations are sent to the estimator via a memoryless erasure channel, where their arrival is modeled by a Bernoulli independent process $\{\gamma_k\}$.  According to this model, the measurement $y_k$ sent at time $k$ reaches its destination if $\gamma_k=1$; it is lost otherwise. Let $\gamma_k$ be independent of $w_k,v_k,x_0$, i.e. the communication channel is independent of both process and measurement noises and let $P(\gamma_k=1)=p$.

The Kalman Filter equations for this system were derived in~\cite{Sinopoli:04} and take the following form:
\[
\begin{split}
  \hat x_{k|k}  &= \hat x_{k|k - 1}  + \gamma _k K_k (y_k  - C\hat  x_{k|k - 1} ) ,\\
  P_{k|k}  &= P_{k|k - 1}  - \gamma _k K_k CP_{k|k - 1}  ,\\
\end{split}
\]
where
\[
\begin{split}
  \hat x_{k + 1|k}  &= A\hat x _{k|k}  , \quad P_{k + 1|k}  = AP_{k|k} A^T  + Q ,\\
  K_k  &= P_{k|k - 1} C^T (CP_{k|k - 1} C^T  + R)^{ - 1}  ,\\
  \hat x_{0| - 1}  &= \bar x_0 ,\quad P_{0| - 1}  = \Sigma _0  .\\
\end{split}
\]
In the hope to improve the legibility of the paper we slightly abuse the notation, by substituting $P_{k|k-1}$ with $P_k$. The error covariance follows:
\begin {equation}
\label{eq:recursive}
P_{k+1}=AP_k A^T+Q-\gamma_k A P_k C^T(C P_k C^T+R)^{-1}C P_k A^T.
\end {equation}

Due to packet drops, $P_k$ is now a stochastic matrix as it depends on Bernoulli variables $\gamma_0,\ldots,\gamma_{k-1}$. In this paper we are interested in the impact of packet drop on the performance of Kalman filtering. \cite{Sinopoli:04} proved the following existence theorem of a critical arrival probability:
\begin{theorem}\label{theorem:lambdaCrit}
  If $(A,Q^{\frac{1}{2}})$ is controllable, $(C,A)$ is detectable, and $A$ is unstable, then there exists a $p_c\in[0,1)$ such that \footnote{We use the notation $\sup_{k}A_k=+\infty$ when the sequence $A_k\geq 0$ is not bounded; i.e., there is no matrix $M\geq 0$ such that $A_k\leq M, \forall k$.}$^,$\footnote{Note that all the comparisons between matrices in this paper are in the sense of positive definite unless otherwise stated.}
  \begin{eqnarray}\label{eqn:lambdaCrit}
    \exists P_0,\,s.t.\,\sup_{k}EP_k=+\infty & \mbox{for } 0\leq p \leq p_c \; ,&\\   
    \forall P_0,\,\forall k,\,EP_k \leq M_{P_0} \;\;  & \mbox{for } p_c < p\leq 1 ,  &
  \end{eqnarray}
  where $M_{P_0} > 0 $  depends on the initial condition $P_0 \geq 0$.
\end{theorem}

For simplicity, we say that $EP_k$ is unbounded if $\sup_{k} EP_k = +\infty$ or $EP_k$ is bounded if there exists a uniform bound independent of $k$.

The exact critical value can be computed in three cases from the previous literature, which is given by the following theorem:
\begin{theorem}
	\label{theorem:nondegcase}
	The critical value $p_c$ is 
	\begin{equation}
		p_c = 1-|\lambda_1|^{-2},	
	\end{equation}
	if one of the following conditions holds:
	\begin{enumerate}
		\item $C$ matrix is full column rank \citep{Sinopoli:04};
		\item $A$ matrix has only one unstable eigenvalue \citep{Sinopoli:04};
		\item The eigenvalues of $A$ matrix have distinct absolute values \citep{Yilin08}.
	\end{enumerate}
\end{theorem}

Moreover, \cite{Yilin08} prove the following theorem on the critical value:
\begin{theorem}
	\label{theorem:independent}
	If $Q,\,R,\,\Sigma_0 > 0$, then the critical value of a system is a function of just $A$ and $C$, which is independent of $Q,\,R,\,\Sigma_0$.
\end{theorem}
As a result, we can write the critical value $p_c$ as $p_c = f_c(A,C)$.

In this paper we consider the second order system, where $A$ is a 2 by 2 matrix. At a first glance this seems to be a very restrictive choice. However, the following theorem shows that the critical value of second order systems can be used as the lower bound for the ones of larger systems.
\begin{theorem}
  \label{theorem:criticalfunction}
  Define $f_c(A,C)$ as the critical value for system $(A,C)$. If $A = diag(\lambda_1,\ldots,\lambda_n)$ diagonal and $C = [C_1,\ldots,C_n]$, then 
  \begin{equation}
    f_c(A,C) \geq  f_c(A_{\mathcal I},C_{\mathcal I}),
  \end{equation}
  where $\mathcal I = \{i_1,\ldots,i_j\}\subset \{1,\ldots,n\}$ is the index set, $A_{\mathcal I} =diag(\lambda_{i_1},\ldots,\lambda_{i_j})$ and $C_{\mathcal I} = [C_{i_1},\ldots,C_{i_j}]$.
\end{theorem}
\begin{pf}
The proof is omitted due to space constraints.
\end{pf}
\begin{remark}
  Due to Theorem~\ref{theorem:criticalfunction}, we know that the critical value of a system is larger than that of any second order subsystem of itself. As a result, a complete characterization of critical value for second order system can provide a lower bound of critical value for any system. 
\end{remark}
%Moreover, the critical value for second order system is surprisingly complicated, as be shown in the next section.

\section{Main Result}\label{sec:main}
In this section we give a complete characterization of the critical value for diagonalizable second order systems. From Theorem~\ref{theorem:nondegcase}, we know that the only case we need to deal with is
\begin{enumerate}
	\item $A = diag(\lambda_1,\lambda_2)$, where $|\lambda_1| = |\lambda_2| > 1$ and $\lambda_1 = \lambda_2 \exp(j\varphi)$. $j$ is the imaginary unit and $\varphi \in (0,2\pi)$.
	\item $rank(C) = 1$.
\end{enumerate}

\begin{remark}
	$\lambda_1$ cannot be equal to $\lambda_2$, otherwise the system is not detectable with a  $C$ matrix of rank $1$.
\end{remark}


To simplify the notation, let us define $\lambda \triangleq |\lambda_1| = |\lambda_2|$, $z \triangleq \exp(j\varphi)$.

Before proving the main result, we need the following theorem:

\begin{theorem}
	\label{theorem:boundednesscondition}
	If $Q,\,R,\,\Sigma_0 > 0$, and $A$ is unstable, then $EP_k$ of the Kalman filter is bounded if and only if 
	\begin{displaymath}
		E\left(\sum_{i=1}^k \gamma_{k+1-i} A^{-iH} C^H CA^{-i} + A^{-(k+1)H}A^{-k+1}\right)^{-1}	
	\end{displaymath}
	is uniformly bounded for all $k$.
\end{theorem}
\begin{pf}
	The theorem is easy to derive from Theorem~3 in \citet{Yilin08}. 
\end{pf}

The main difficulty to apply Theorem~\ref{theorem:boundednesscondition} is that we need to find a uniform bound for all $k$. In the following theorem, we prove that we only need to check whether the expectation is bounded as $k$ goes to infinity.
\begin{theorem}
	\label{theorem:uniformbound}
	If $Q,\,R,\,\Sigma_0 > 0$, $A = diag(\lambda_1,\lambda_2)$, and $|\lambda_1|\geq |\lambda_2| >1$, then $EP_k$ of the Kalman filter is bounded if and only if 
	\begin{displaymath}
		E\left(\sum_{i=1}^\infty \gamma_{i} A^{-iH} C^H CA^{-i} \right)^{-1}< \infty.
	\end{displaymath}
\end{theorem}
\begin{pf}
	The proof is reported in the appendix for the sake of legibility.
\end{pf}

We are now ready to characterize the critical value for second order systems. We consider two cases depending on whether $\varphi/2\pi$ is rational or irrational. 

\begin{theorem}
  \label{theorem:criticalvaluefordegsystem}
  If a second order system satisfies:
  \begin{enumerate}
	\item $A = diag(\lambda_1,\lambda_2)$, where $|\lambda_1| = |\lambda_2| > 1$ and $\lambda_1 = \lambda_2 \exp(j\varphi)$. 
	\item $\varphi/2\pi = r/q$ , $q > r$ and $r,q \in \mathbb N$ are irreducible.
	\item $rank(C) = 1$ and $(C,A)$ is detectable.
	\item $Q,\,R,\,\Sigma_0$ is positive definite.
  \end{enumerate}
  then the critical value of the system is 
  \begin{equation}
    p_c = 1-|\lambda_1|^{-2q/(q-1)}.
    \label{eq:critvaluefor2degenerate}
  \end{equation}
\end{theorem}
For the case where $\varphi/2\pi$ is irrational, we have:
\begin{theorem}
  \label{theorem:criticalvaluefordegsystem2}
  If a second order system satisfies:
  \begin{enumerate}
	\item $A = diag(\lambda_1,\lambda_2)$, where $|\lambda_1| = |\lambda_2| > 1$ and $\lambda_1 = \lambda_2 \exp(j\varphi)$. 
	\item $\varphi/2\pi $ is irrational. 
	\item $rank(C) = 1$ and $(C,A)$ is detectable.
	\item $Q,\,R,\,\Sigma_0$ is positive definite.
  \end{enumerate}
  \begin{equation}
    p_c = 1-|\lambda_1|^{-2}.
    \label{eq:critvaluefor2degenerate2}
  \end{equation}
\end{theorem}

\begin{remark}
	If $\varphi/2\pi = r/q$, then one can prove that the system $(C,A^{kq})$ is not observable. Due to the loss of observability, the critical value for such system is in general higher than the other cases.
\end{remark}

Combining all the results, we have:

\begin{theorem}
  For a detectable system with $A = diag(\lambda_1,\lambda_2)$, $|\lambda_1| \geq |\lambda_2|$, $|\lambda_1| > 1$ and $R,\,Q,\,\Sigma_0 > 0$, the critical value is
  \begin{equation}
    p_c = f_c(A,C) = 1-|\lambda_1|^{-2},
  \end{equation}
  if one of the following conditions holds
  \begin{enumerate}
    \item $|\lambda_1| > |\lambda_2|$,
    \item $rank(C) = 2$.
  \end{enumerate}
  Otherwise the critical value of the system is given by
  \begin{equation}
    p_c = f_c(A,C) =1-|\lambda_1|^{-\frac{2}{1-D_M(\varphi/2\pi)}},
  \end{equation}
  where $\lambda_1 = \lambda_2 \exp(j\varphi)$, and $D_M(x)$ is the modified Dirichlet function defined as
  \begin{equation}
    D_M(x) = \left\{ \begin{array}{*{20}l}
      0 & \textrm{for $x$ irrational}\\
      1/q & \textrm{for $x = r/q$, $r,q \in \mathbb Z$ and irreducible.}\\
    \end{array}\right..
  \end{equation}
\end{theorem}
\begin{remark}
	It is worth mentioning that if $C$ is of rank 1 and the eigenvalues of $A$ matrix are located on the same circle, then the critical value of the system could be higher than $1-|\lambda_1|^{-2}$, which indicates an increase in the estimation error. In reality, such kind of system is not uncommon, since the $A$ matrix may have complex eigenvalues which are conjugate of each other and only one sensor is used to observe the system, i.e. $C$ matrix has only one row. To improve the performance of Kalman filtering, the sensors could pack both $y_{k-1}$ and $y_k$ into the packet at time $k$, which essentially makes $C$ a rank 2 matrix and it can be proved that the critical value is $1-|\lambda_1|^{-2}$ in this scenario\footnote{The formal proof is omitted due to space limit.}. 
\end{remark}

\begin{pf}[Proof of Theorem~\ref{theorem:criticalvaluefordegsystem}]
  Since $rank(C) =1$, we know that $rank(C^H C) = 1$. Thus, 
  \[C^HC = \left[{\begin{array}{*{20}c}
	  |a|^2 &  ab\\
	  \overline {ab} & |b|^2\\
  \end{array}}\right],\]
  where $a,b$ are constants. It can be also shown that $a,b \neq 0$ due to the detectability assumption on $(C,A)$. Since $A = diag(\lambda_1,\lambda_2) = diag(\lambda_1,\lambda_1z) $, we know that
  \begin{equation}
    \begin{split}
      &\sum_{i=1}^\infty \gamma_i A^{-iH}C^HCA^{-i} = \sum_{i=1}^\infty \gamma_i \left[{\begin{array}{*{20}c}
	a^2\lambda^{-2i} & ab \lambda^{-2i} z^{-i}\\
	ab\lambda^{-2i}z^i & b^2 \lambda^{-2i}\\
      \end{array}}\right]\\ &= \left[{\begin{array}{*{20}c}
	a & \\
	& \bar b\\
      \end{array}}\right]\left( \sum_{i=1}^\infty \gamma_i \lambda^{-2i} \left[{\begin{array}{*{20}c}
	1 &  z^{-i}\\
	z^i & 1\\
      \end{array}}\right]\right)  \left[{\begin{array}{*{20}c}
	\bar a & \\
	& b\\
      \end{array}}\right].
    \end{split}
  \end{equation}

  Since $a,b \neq 0$, we know that $E(\sum_{i=1}^\infty \gamma_i A^{-iH}C^HCA^{-i})^{-1}$ is bounded if and only if
  \[E\left(\sum_{i=1}^\infty \gamma_i \lambda^{-2i} \left[{\begin{array}{*{20}c}
    1 &  z^{-i}\\
    z^i & 1\\
  \end{array}}\right]\right)^{-1} < \infty.\]

  Define
  \begin{displaymath}
   P \triangleq \sum_{i=1}^\infty \gamma_i \lambda^{-2i} \left[{\begin{array}{*{20}c}
      1 &  z^{-i}\\
      z^i & 1\\
    \end{array}}\right] .
  \end{displaymath}

  It is easy to show that
  \begin{equation}
    \begin{split}
      \label{eq:traceanddet42degsys}
     & trace(P) = 2\sum_{i=1}^\infty \gamma_i\lambda^{-2i},\\
     & \det(P) = \left(\sum_{i=1}^\infty \gamma_i \lambda^{-2i}\right)^2 - \left(\sum_{i=1}^{\infty} \gamma_i \lambda^{-2i} z^i\right)  \left(\sum_{i=1}^{\infty} \gamma_i \lambda^{-2i} z^{-i}\right)\\
      &=\sum_{i=1}^{\infty} \gamma_i \lambda^{-4i} + 2\sum_{i=1}^{\infty} \sum_{j=i+1}^{\infty} \gamma_i\gamma_j \lambda^{-2i}\lambda^{-2j}\\
      &- \sum_{i=1}^{\infty} \gamma_i \lambda^{-4i} -\sum_{i=1}^{\infty} \sum_{j=i+1}^{\infty} \gamma_i\gamma_j \lambda^{-2i}\lambda^{-2j}(z^{i-j}+z^{j-i})\\
      & = \sum_{i=1}^{\infty} \sum_{j=i+1}^{\infty} \gamma_i\gamma_j \lambda^{-2i}\lambda^{-2j}(2-z^{i-j}-z^{j-i}).\\
    \end{split}
  \end{equation}

  Define the set $\mathbb S_{q,\infty} = \{l \in \mathbb N|l \neq kq, k \in \mathbb N\}$ and $\mathbb S_{q,i} = \{l \in \mathbb S_{q,\infty}|l < i\}$. Since $z = exp(2r\pi/q)$ and $q,r$ are irreducible, $z^{j-i} = 1$ if and only if $ |j-i| \notin \mathbb S_{q,\infty}$. It is easy to show that 
  \[
  \inf\{2-z^{i-j}-z^{j-i}||j - i| \in \mathbb S_{q,\infty}\} = 2 - 2\cos(\frac{2\pi}{q}) > 0,
  \]
  and
  \[
  \sup\{2-z^{i-j}-z^{j-i}||j - i| \in \mathbb S_{q,\infty}\}  < 4.
  \]
  Thus, 
  \begin{equation}
    \begin{split}
      \label{eq:boundfordet1}
      &[2-2\cos(2\pi/q)] \sum_{i=1}^\infty \sum_{j-i \in \mathbb S_{q,\infty}} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j} \\
      &\leq det(P) = \sum_{i=1}^\infty \sum_{j-i \in \mathbb S_{q,\infty}} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j}(2-z^{i-j}-z^{j-i})\\
      &\leq 4 \sum_{i=1}^\infty \sum_{j-i \in \mathbb S_{q,\infty} } \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j}.
    \end{split}
  \end{equation}

  Define stopping time $\tau_1 = \inf\{i \in \mathbb N|\gamma_i = 1\}$ and $\tau_2 = \inf \{j\in \mathbb N| j - \tau_1 \in \mathbb S_{q,\infty},\gamma_j = 1\}$. Therefore,
  \begin{equation}
	  \begin{split}
    \label{eq:boundfortrace}
    \lambda^{-2\tau_1} &\leq trace(P) = \sum_{i=1}^{\infty}\gamma_i\lambda^{-2i} = \sum_{i = \tau_1}^{\infty} \gamma_i\lambda^{-2i} \\
    &\leq \sum_{i=\tau_1}^{\infty} \lambda^{-2i} = \frac{1}{1-\lambda^{-2}}\lambda^{-2\tau_1}.
    \end{split}
  \end{equation}

  Now consider two indices $c,d$ such that $d > c, d-c \in \mathbb S_{q,\infty}$ and $\gamma_c = \gamma_d = 1$. By the definition of $\tau_1$, we know that $\tau_1 \leq c$. Suppose that $d < \tau_2$, therefore $\tau_1 \leq c < d < \tau_2$. By the definition of $\tau_2$, $c-\tau_1 = k_c q ,d - \tau_1 = k_d q$. As a result, $d-c = (k_d-k_c)q$, which contradicts with the fact that $d-c\in \mathbb S_{q,\infty}$. Therefore we can conclude that $\tau_2 \leq d$. 

  Hence, for all $\gamma_c\gamma_d = 1, d-c \in \mathbb S_{q,\infty}$,$\tau_1 \leq c,\tau_2 \leq d$, which gives
  \begin{equation}
    \begin{split}
      \label{eq:boundfordet2}
      \lambda^{-2\tau_1}\lambda^{-2\tau_2} &\leq \sum_{i=1}^\infty \sum_{j-i \in \mathbb S_{q,\infty}} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j} \\
      &= \sum_{i = \tau_1}^\infty \sum_{j \geq \tau_2,j - i \in \mathbb S{q,\infty}} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j}\\
      & \leq \sum_{i=\tau_1}^{\infty}\sum_{j=\tau_2}^{\infty} \lambda^{-2i}\lambda^{-2j}= \frac{1}{(1-\lambda^{-2})^2}\lambda^{-2\tau_1}\lambda^{-2\tau_2}.
    \end{split}
  \end{equation}

  Define $\sigma_1,\sigma_2$ to be the eigenvalues of $P$. As a result,
  \begin{equation}
    \label{eq:traceeqtraceoverdet}
    trace(P^{-1}) = \sigma_1^{-1} + \sigma_2^{-1} = \frac{\sigma_1 + \sigma_2}{\sigma_1\sigma_2} = \frac{trace(P)}{\det(P)}.
  \end{equation}

  By inequality \eqref{eq:boundfordet1}, \eqref{eq:boundfortrace} and \eqref{eq:boundfordet2}, it is easy to see that $E trace(P^{-1}) < \infty$ if and only if 
  \begin{displaymath}
    E\frac{\lambda^{-2\tau_1}}{\lambda^{-2\tau_1}\lambda^{-2\tau_2}} = E\lambda^{2\tau_2}  =  E(\lambda^{2\tau_1}\lambda^{2(\tau_2-\tau_1)})< \infty.
  \end{displaymath}

  Now we need to compute the distribution of $\tau_1$ and $\tau_2 - \tau_1$. By definition, the event $\{\tau_1 = i\}$ is equivalent to $\{\gamma_1 = \ldots = \gamma_{i-1} = 0, \gamma_i = 1\}$. The event $\{\tau_2 - \tau_1 = i\}$, where $i \in \mathbb S_{q,\infty}$, is equivalent to $\{\gamma_{\tau_1 + j} = 0, \gamma_{\tau_2} = 1 \}$, for all $j \in \mathbb S_{q,i}$. Since $\tau_2 - \tau_1$ only depends on $\gamma_{\tau_1+i}, i \in \mathbb S_{q,\infty}$, $\tau_2 - \tau_1$ is independent of $\tau_1$. The distributions of $\tau_1,\tau_2 - \tau_1$ are characterized by the following equations:
  \begin{equation}
    P(\tau_1 = i) = P(\gamma_1=\ldots=\gamma_{i-1} = 0, \gamma_i = 1) = (1-p)^{i-1}p,
  \end{equation}
  and
  \begin{equation}
    P(\tau_2 - \tau_1 = i) = P(\gamma_{\tau_1 + j} = 0,\gamma_{\tau_2} = 1) = (1-p)^{|\mathbb S_{q,i}|}p,
  \end{equation}
  where $j \in \mathbb S_{q,i}, i \in \mathbb S_{q,\infty}$ and $|\mathbb S_{q,i}|$ is the number of elements in $\mathbb S_{q,i}$. Thus,
  \begin{equation}
	  \begin{split}
    &E\left(\lambda^{2\tau_1}\lambda^{2(\tau_2-\tau_1)}\right)=E\lambda^{2\tau_1} \times E\lambda^{2(\tau_2-\tau_1)} \\
    &= \sum_{i = 1}^{\infty}p (1-p)^i\lambda^{2i} \times \sum_{i \in \mathbb S_{q,\infty}} p(1-p)^{|\mathbb S_{q,i}|}\lambda^{2i}.
    \label{eq:seriesproductfordegsystem}
	  \end{split}
  \end{equation}

  The first series is a simple geometric series which is bounded if and only if $p > 1-1/\lambda^2$. Using the root test of convergence, $\sum_{i \in \mathbb S_{q,\infty}} p(1-p)^{|\mathbb S_{q,i}|}\lambda^{2i}$ is bounded if and only if 
  \begin{equation}
    \limsup_{|\mathbb S_{q,i}| \rightarrow \infty} \sqrt[|\mathbb S_{q,i}|]{p(1-p)^{|\mathbb S_{q,i}|}\lambda^{2i}} = (1-p)\limsup_{|\mathbb S_{q,i}| \rightarrow \infty}\lambda^{2\frac{i}{|\mathbb S_{q,i}|}} < 1.
  \end{equation}

  Since $|\mathbb S_{q,i}| = \lceil (i-1)(q-1)/q \rceil$, where $\lceil x \rceil$ is the minimal integer that is not less that $x$, $\limsup_{|\mathbb S_{q,i} \rightarrow \infty|} \left(i/|\mathbb S_{q,i}|\right) = q/(q-1)$. As a result, the second series converges if and only if 
  \begin{equation}
    (1-p)\lambda^{2q/(q-1)} < 1,
  \end{equation}
  which is equivalent to $p > 1-\lambda^{-2q/(q-1)}$. Therefore, we can conclude that the critical arrival probability is 
  \begin{equation}
    p_c = 1-\lambda^{-\frac{2q}{q-1}}.
  \end{equation}
\end{pf}

\begin{pf}[Proof of Theorem~\ref{theorem:criticalvaluefordegsystem2}]
  The proof is quite similar to the proof of Theorem~\ref{theorem:criticalvaluefordegsystem}. The reasoning follows the previous one verbatim up to equation \eqref{eq:traceanddet42degsys}. However, in \eqref{eq:boundfordet1} we need to change the set $\mathbb S_{q,\infty}$. Define the set $\mathbb T_{\varepsilon,\infty} = \{l \in \mathbb N|2 - z^{l} - z^{-l} > \varepsilon\}$, where $\varepsilon > 0$. Also define $\mathbb T_{\varepsilon,i} = \{l \in \mathbb T_{\varepsilon,\infty}| l < i\}$. Therefore, \eqref{eq:boundfordet1} becomes
  \begin{equation}
	  \begin{split}
    \label{eq:boundfordet3}
    &\varepsilon \sum_{i=1}^\infty \sum_{j-i \in \mathbb T_{\varepsilon,\infty}} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j} \leq det(P) \\
    &= \sum_{i=1}^\infty \sum_{j > i} \gamma_i\gamma_j\lambda^{-2i}\lambda^{-2j}(2 - z^{i-j} -z ^{j-i}) .
    \end{split}
  \end{equation}
Equations \eqref{eq:boundfortrace} and \eqref{eq:traceeqtraceoverdet} still hold if we replace each set $\mathbb S_{q,i}$ with $\mathbb T_{\varepsilon,i}$. However only the left hand side inequality in \eqref{eq:boundfordet2} holds, because there is no guarantee that, for all $a,b$, $b - a \in \mathbb T_{\varepsilon,\infty}, \gamma_a = \gamma_b = 1$, $\tau_1 \leq a,\tau_2 \leq b$ will always hold. Also in Inequality \eqref{eq:boundfordet3}, we only prove the left hand side inequality of \eqref{eq:boundfordet1}. As a result, $E\lambda^{2\tau_2} < \infty$ will only be a sufficient condition for the boundedness of estimation error covariance. Following the rest of the proof, it can be derived that
  \begin{equation}
    p \geq 1 - \limsup_{|\mathbb T_{\varepsilon,i}| \rightarrow \infty }\lambda^{-2\frac{i}{|\mathbb T_{\varepsilon,i}|}}
  \end{equation}
  is sufficient for bounded estimation error. Since $\varepsilon$ can be any positive real number, we can conclude that
  \begin{equation}
    \label{eq:upperboundfor2degsys}
    p_c \leq 1- \lim_{\varepsilon \rightarrow 0^+} \limsup_{|\mathbb T_{\varepsilon,i}|\rightarrow \infty} \lambda^{-2\frac{i}{|\mathbb T_{\varepsilon,i}|}}.
  \end{equation}

  Now we only need to estimate $i/|\mathbb T_{\varepsilon,i}|$. Manipulating the equations we have 
  \[
  2 - z^i - z^{-i} = 2 - 2\cos(i\varphi).
  \]
  Thus,
  \[
  \begin{split}
   & 2 - 2 \cos (i\varphi) \geq \varepsilon\\
    &\Leftrightarrow i \varphi \notin [2k\pi - 2\pi\Delta_\varepsilon,2k\pi+2\pi\Delta_\varepsilon], k \in \mathbb Z\\
    &\Leftrightarrow i(\varphi/2\pi) \notin [k - \Delta_\varepsilon, k+\Delta_\varepsilon], k \in \mathbb Z,
  \end{split}
  \]
  where $\Delta_\varepsilon = \arccos(1-\varepsilon/2)/2\pi$.

  Define $N_\varepsilon = \inf \{i\in \mathbb N| i(\varphi/2\pi) \in [k - 2\Delta_\varepsilon, k + 2\Delta_\varepsilon], k \in \mathbb Z\}$. Suppose that $c(\varphi/2\pi),d(\varphi/2\pi),d > c,$ both belong to $[k-\Delta_\varepsilon,k+\Delta_\varepsilon],k\in \mathbb Z$. Thus, $(d-c)(\varphi/2\pi) \in [k-2\Delta_\varepsilon,k+2\Delta_\varepsilon], k \in \mathbb Z$. By the definition of $N_\varepsilon$, we can conclude that $d - c \geq N_\varepsilon$, which implies that if $c(\varphi/2\pi) \in [k-\Delta_\varepsilon,k+\Delta_\varepsilon]$, then $(c+1)(\varphi/2\pi),\ldots, (c+N_\varepsilon -1)(\varphi/2\pi) \notin [k-\Delta_\varepsilon,k+\Delta_\varepsilon]$. Therefore, if $c \notin \mathbb T_{\varepsilon,\infty}$, then $c+1,\ldots,c+N_\varepsilon-1 \in \mathbb T_{\varepsilon,\infty}$. As a result,
  \begin{equation}
    \frac{N_\varepsilon}{N_\varepsilon - 1} \geq \limsup_{|\mathbb T_{\varepsilon,i}| \rightarrow \infty} \frac{i}{|\mathbb T_{\varepsilon,i}|} \geq 1.
  \end{equation}

  Since $\varphi/2\pi$ is irrational, $\lim_{\varepsilon\rightarrow 0^+} N_\varepsilon = \infty$. Therefore,
  \begin{equation}
    \label{eq:limforirrantionalnumberproximation}
    \lim_{\varepsilon\rightarrow 0^+}\limsup_{|\mathbb T_{\varepsilon,i} |\rightarrow \infty} \frac{i}{|\mathbb T_{\varepsilon,i}|} = 1.
  \end{equation}

  Using \eqref{eq:upperboundfor2degsys} and \eqref{eq:limforirrantionalnumberproximation}, we can conclude that the critical arrival probability $p_c$ satisfies
  \begin{displaymath}
    p_c \leq 1-\lambda^{-2},
  \end{displaymath}
  which is exactly the lower bound in \cite{Sinopoli:04}, and therefore proving necessity as well.
  \end{pf}

\section{Conclusions}
\label{sec:conclusion}
This paper provides a complete characterization of the critical arrival probability related to the Kalman filtering with intermittent observations for diagonalizable second order systems with eigenvalues of equal magnitude. In particular we compute the critical value for such systems, showing that it is higher than the lower bound achieved in \cite{Sinopoli:04} if the angle between the two eigenvalues is a rational factor of $2\pi$. In this case we show how the critical value can be reduced to the lower bound if the sensor includes both the current and the previous measurements in each transmission. Although clearly restrictive the analysis of second order systems presented herein can be used to bound the critical value of higher dimensional systems of this kind. 

\input{appendix.tex}
\bibliography{ifackalman}
\end{document}
