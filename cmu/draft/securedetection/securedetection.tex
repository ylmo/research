\documentclass[12pt,draftcls,onecolumn]{IEEEtran}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage{slashbox}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\title{\LARGE \bf {Resilience Detection against Integrity Attacks}}

\author{Yilin Mo$^*$, Jo\~{a}o Hespanha$^\dag$, Bruno Sinopoli$^*$
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation and grant NGIT2009100109 from Northrop Grumman Information Technology Inc Cybersecurity Consortium. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
\thanks{
$*$: Yilin Mo and Bruno Sinopoli are with the ECE department of Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu, brunos@ece.cmu.edu}
}
\thanks{
$\dag$: Jo\~{a}o Hespanha is with the ECE department of University of California, Santa Barbara, CA. Email: {hespanha@ece.ucsb.edu} 
}
}

\begin{document}
\maketitle
\begin{abstract}
  to do.
\end{abstract}
\section{Introduction}
to do
\section{Problem Formulation}
Define $\theta$ as a random variable, such that $\theta = -1$ with $p^-$ probability and $\theta = 1$ with $p^+$ probability, where $p^-,\,p^+\geq 0$ and $p^-+p^+ = 1$. Without loss of generality, we assume that $p^+ \geq p^-$.

Suppose there are $m$ sensors. Let $y_i\in \mathbb R$ be the measurement from sensor $i$, which is independent from each other. Define the vector $y = [y_1,\ldots,y_m]'\in\mathbb R^m$. We assume that the probability density (mass) function of $y$ is given by
\begin{displaymath}
  P(y\in dx|\theta) = F(x|\theta)dx.
\end{displaymath}

For future analysis, let us define the likelihood ratio as
\begin{displaymath}
  \Lambda(y) \triangleq \frac{F(y|\theta =1)}{F(y|\theta=-1)}.
\end{displaymath}

After receiving all the measurements, the detector computes an estimate of $\theta$. Let us define $f:\mathbb R^m\rightarrow\{-1,1\}$. The estimate of $\theta$ can be written as
\begin{displaymath}
  \hat \theta = f(y). 
\end{displaymath}

An attacker wants to increase the detection error. We assume that the attacker knows the underlying $\theta$, the measurements $y_1,\ldots,y_m$ and the function $f$. Moreover, the attacker is able to change measurements from any $n$ number of sensors, where $n$ is known also to the detector. Let us define $\gamma_i$ as a binary variable, which satisfies $\sum_{i=1}^m \gamma_i =n$ and the vector $\gamma = [\gamma_1,\ldots,\gamma_m]'\in\mathbb R^m$. Moreover, let us define the set 
\begin{displaymath}
  \Gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i = n\}. 
\end{displaymath}

In the presence of the attacker, the detector receives the measurement $y_i'$s, which are defined as 
\begin{equation}
  y_i' = y_i + \gamma_i u_i, 
  \label{eq:compromise}
\end{equation}
where $u_i$ is the bias introduced by the attacker. Let us define $u = [u_1,\ldots,u_m]'\in \mathbb R^m$. We can write \eqref{eq:compromise} as
\begin{equation}
  y' = y + \gamma\circ u. 
  \label{eq:compromisevector}
\end{equation}
where $\circ$ means entrywise product.

For future analysis, let us define function $\overline f$ and $\underline f$ as
\begin{displaymath}
  \overline f(y) \triangleq \max_{u\in\mathbb R^m,\gamma\in \Gamma}f(y+\gamma\circ u), \underline f(y) \triangleq \min_{u\in\mathbb R^m,\gamma\in \Gamma}f(y+\gamma\circ u).
\end{displaymath}
The $\min$ and $\max$ are attainable since $f$ only takes $-1$, $1$.

Given $\theta,\,y,\,f$, the optimal strategy for the attacker is
\begin{enumerate}
  \item If $\theta = 1$, then choose $u$ and $\gamma$ such that $f(y+\gamma\circ u) = \underline f(y). $
  \item If $\theta = -1$, then choose $u$ and $\gamma$ such that $f(y+\gamma\circ u) = \overline f(y).$ 
\end{enumerate}

Let us define the following sets:
\begin{displaymath}
  Y^+(f) \triangleq \{y\in \mathbb R^m:\underline f(y)=1\},\, Y^-(f) \triangleq \{y\in \mathbb R^m:\overline f(y)=-1\}
\end{displaymath}

As a result, the detector can successfully estimate the right $\theta$, if and only if one of the following happens:
\begin{enumerate}
  \item $\theta = 1$ and $\underline f(y) = 1$.
  \item $\theta = -1$ and $\overline f(y) = -1$.
\end{enumerate}
In other words, if $(y,\theta)$ lies in the following set
\begin{displaymath}
  (y,\theta)\in \{(y,\theta):y\in Y^+(f),\theta = 1\}\bigcup\{(y,\theta):y\in Y^-(f),\theta = -1\}.
\end{displaymath}
However, the set on the right hand set may not be measurable, even if $f$ is measurable. As a result, we use inner measure and outer measure to define the probability of false alarm $\alpha$ and the probability of detection $\beta$ for the detector:
\begin{displaymath}
  \alpha(f) \triangleq \inf\{P(y\in S|\theta =-1):S\in \mathcal B(\mathbb R^m),\mathbb R^m\backslash Y^-(f)\subseteq S\},
\end{displaymath}
and
\begin{displaymath}
  \beta(f) \triangleq \sup\{P(y \in S|\theta =1):S\in \mathcal B(\mathbb R^m),S\subseteq Y^+(f)\},
\end{displaymath}
where $\mathcal B(\mathbb R^m)$ is the Borel $\sigma$-algebra on $\mathbb R^m$. Let us define the probability of error as
\begin{displaymath}
  P_e(f) \triangleq (1-\beta(f))P(\theta = 1) +\alpha(f) P(\theta = -1)=(1-\beta(f))p^+ +\alpha(f)p^-. 
\end{displaymath}

The goal of the defender is to find the optimal detector $f$, such that the probability of error is minimized. In other words, the defender wants to find an $f$ which minimizes the following:
\begin{displaymath}
  P_e^* = \inf_f P_e(f).
\end{displaymath}

From the definition of $P_e$, it is easy to see the following theorem holds:
\begin{theorem}
  For any two functions $f,g:\mathbb R^m\rightarrow\{-1,1\}$, if $Y^+(f)\subseteq Y^+(g)$ and $Y^-(f)\subseteq Y^-(g)$, then $P_e(f)\geq P_e(g)$.
  \label{theorem:errorandset}
\end{theorem}
\section{Optimal Detector Design for $n \geq m/2$}
In this section, we find the optimal detector when no less than half of the measurements are corrupted. We will show that the optimal detector $f_*$ does not depends on measurements $y_i$s.

The following theorem characterizes the relationship between $Y^-(f)$ and $Y^+(f)$:
\begin{theorem}
  \label{theorem:exclusion}
  If $n\geq m/2$, then $Y^-(f)\neq \phi$ implies that $Y^+(f) = \phi$.
\end{theorem}
\begin{proof}
  We prove that by contradiction. First it can be easily seen that $m-n \leq n$. 
  Now suppose neither $Y^+$ nor $Y^-$ is empty. As a result, we can find 
  \[
  y^+ = [y_{1}^+,\ldots,y_{m}^+]'\in Y^+,
  \] 
  and 
  \[
  y^- = [y_{1}^-,\ldots,y_{m}^-]'\in Y^-.
  \]
  Now let us consider 
  \[
  y = [y_{1}^+,\ldots,y_{n}^+,y_{n+1}^-,\ldots,y_m^-]'.
  \]
  Thus,
  \begin{displaymath}
    y = y^+ +\gamma_1\circ(y^--y^+),
  \end{displaymath}
  where 
  \[
  \gamma_1 = [\underbrace{0,\ldots,0}_{n},\underbrace{1,\ldots,1}_{m-n}]'.
  \]
  Therefore, $\gamma_1\in \Gamma$ and thus by the definition of $Y^+$, $f(y) = 1$. On the other hand,
  \begin{displaymath}
    y = y^- +(\mathbf 1-\gamma_1)\circ(y^+-y^-),
  \end{displaymath}
  where $\mathbf 1$ is an all one vector. It can be proved that $\mathbf 1-\gamma_1 \in \Gamma$. Hence, $f(y) = -1$ from the definition of $Y^-$, which contradicts with the fact $f(y) = 1$.
\end{proof}

We can now find the optimal $f_*$ by the following theorem:
\begin{theorem}
  If $n \geq m/2$, then the optimal $f_*$ is given by
  \begin{displaymath}
    f_* = 1. 
  \end{displaymath}
  The optimal set is given by
  \begin{displaymath}
    Y^+(f_*) =\mathbb R^m, 
  \end{displaymath}
  and
  \begin{displaymath}
    Y^-(f_*) =\phi. 
  \end{displaymath}
\end{theorem}
\begin{proof}
  By Theorem~\ref{theorem:exclusion}, we know that one of $Y^+$ and $Y^-$ must be empty. First suppose that $Y^-$ is empty and hence $\alpha(f) = 1$. As a result
  \begin{displaymath}
    P_e(f) =  p^+(1-\beta(f)) + p^-.
  \end{displaymath}
  The minimum is achieved when $Y^+ = \mathbb R^m$, which implies that $f = 1$ and $P_e(f) = p^-$.

  On the other hand, if $Y^+$ is empty, then the optimal $Y^-=\mathbb R^m$, $f = -1$ and $P_e(f) = p^+$. Since we assume that $p^+ \geq p^-$, the optimal $f$ is $f_* = 1$ and optimal sets are $Y^+(f_*) = \mathbb R^m$ and $Y^-(f_*) = \phi$.
\end{proof} 

\section{Optimal Detector Design for $n< m/2$}
In this section, we find the optimal detector for $n<m/2$. We first prove that the optimal detector must lies in a special set of functions $\mathcal F$. Then we provide the optimal detector for the case where $n=(m-1)/2$. Finally we propose a heuristic detector in $\mathcal F$ for general $n$, and prove that heuristic filter is asymptotic optimal when $m\rightarrow\infty$.
\subsection{Optimal Detector}
Before continuing on, let us define the projection function. Suppose that $\mathcal I = \{i_1,\ldots,i_j\}\subseteq\{1,\ldots,m\}$ is an index set. We define the projection function $Proj_{\mathcal I}:\mathbb R^m\rightarrow\mathbb R^{|\mathcal I|}$, such that
\begin{displaymath}
  Proj_{\mathcal I} (y) = [e_{i_1},\ldots,e_{i_j}]'\times y, 
\end{displaymath}
where $e_{i} = [\delta_{1,i},\ldots,\delta_{m,i}]$ and $\delta$ is the delta function.

Moreover, we can also define the projection of a set $Y$ as
\begin{displaymath}
  Proj_{\mathcal I}(Y) = \{z\in \mathbb R^{|\mathcal I|}:\exists y\in Y, s.t.\;z = Proj_{\mathcal I}(y)\}. 
\end{displaymath}

We write the projected set of $Y^-$ and $Y^+$ as $Y^-_{\mathcal I}$ and $Y^+_{\mathcal I}$. For simplicity, when $\mathcal I = \{i\}$ contains only one element, we write the projected set as $Y^-_{i}$ and $Y^+_{i}$.

First we prove a theorem on the set $Y^-$ and $Y^+$:
\begin{theorem}
  When $n < m/2$, if $|\mathcal I| = m-2n$, then the projection sets $Y_{\mathcal I}^+$ and $Y_{\mathcal I}^-$ cannot intersect, which means 
  \begin{displaymath}
    Y^-_{\mathcal I}\bigcap Y^+_{\mathcal I} = \phi. 
  \end{displaymath}
  \label{theorem:exclusion3}
\end{theorem}
\begin{proof}
  We prove the statement by contradiction. Without loss of generality, we assume that $\mathcal I = \{1,\ldots,m-2n\}$, and
  \begin{displaymath}
    Y^-_{\mathcal I}\bigcap Y^+_{\mathcal I} \neq \phi. 
  \end{displaymath}
  As a result, there exist
  \[
  y^+ = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^+,\ldots,y_{m}^+]'\in Y^+,
  \] 
  and 
  \[
  y^- = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^-,\ldots,y_{m}^-]'\in Y^-,
  \]
  Now let us consider 
  \[
  y = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^+\ldots,y_{m-n}^+,y_{m-n+1}^-,\ldots,y_m^-]'.
  \]
  It can be easily seen that there are $n$ elements in $y$ that differ from $y^+\in Y^+$ and another $n$ elements in $y$ that differ from $y^-\in Y^-$. As a result, $f(y) = 1$ and $f(y) = -1$, which is impossible.
\end{proof}

For any $f$ with $Y^-(f)$ and $Y^+(f)$, let us consider the set 
\begin{displaymath}
  R_{\mathcal I}(f) \triangleq Proj_{\mathcal I}(Y^-(f)).
\end{displaymath}
It is easy to see that
\begin{displaymath}
  Y^-(f)\subseteq \mathcal Y^-(f) = \{y\in \mathbb R^m:Proj_{\mathcal I}(y) = R_{\mathcal I}(f), \forall |I|=m-2n\} ,
\end{displaymath}
From Theorem~\ref{theorem:exclusion3}, we can also conclude that $Y^+$ is upper bounded by
\begin{displaymath}
  Y^+(f) \subseteq \mathcal Y^+(f)= \{y\in \mathbb R^m:Proj_{\mathcal I}(y) = \mathbb R^{m-2n}\backslash R_{\mathcal I}(f), \forall |I|=m-2n\},
\end{displaymath}
We will prove that there exists a function $g$ such that $\mathcal Y^-(f)\subseteq Y^-(g)$ and $\mathcal Y^+(f)\subseteq Y^-(g)$. First, let us define the metric function $d:\mathbb R^m\times \mathbb R^m \rightarrow \mathbb R$ as
\begin{displaymath}
  d(x,y) \triangleq \|x - y\|_0, 
\end{displaymath}
where $\|x\|_0$ is the zero norm of $x$, which is defined as the number of non-zero elements in vector $x$. It is easy to prove that $d$ is a well defined metric. Now suppose that $X,\,Y\subset \mathbb R^{m}$ are two subset of $\mathbb R^m$, we define the distance between $X$ and $Y$ as
\begin{equation}
  d(X,Y) \triangleq \inf_{x\in X,\,y\in Y}d(x,y). 
  \label{eq:distanceset}
\end{equation}
Moreover, the distance between $x \in \mathbb R^m$ and $Y \subset \mathbb R^m$ is defined as
\begin{displaymath}
  d(x,Y) \triangleq d(\{x\},Y). 
\end{displaymath}
For convenience, we define the distance from any set to empty set to be infinity. In other words, $d(X,\phi) = \infty$.

We now prove that the infimum in \eqref{eq:distanceset} is attainable, which is given by the following lemma:
\begin{lemma}
  Suppose that $d(X,Y) = d_0 < \infty$, then there exist $x_0\in X$ and $y_0\in Y$, such that $d(x_0,y_0) = d_0$.
  \label{lemma:attainable}
\end{lemma}
\begin{proof}
  First it is easy to see that $d(X,Y)$ can only be non-negative integer. Therefore $d_0$ is an integer. From \eqref{eq:distanceset}, we know that there exists $x_0$ and $y_0$, such that
  \begin{displaymath}
    d_0\geq d(x_0,y_0) \geq d_0 - 0.5. 
  \end{displaymath}
  However, $d(x_0,y_0)$ is also an integer, which implies that $d(x_0,y_0)= d_0$.
\end{proof}

Next we prove an inequality on the distance between $\mathcal Y^-(f)$ and $\mathcal Y^+(f)$, which is given by the following lemma:
\begin{lemma}
  \label{lemma:distanceineq1}
  The distance between $\mathcal Y^-(f)$ and $\mathcal Y^+(f)$ is no less than $2n+1$. In other words,
  \begin{displaymath}
    d(\mathcal Y^-(f),\mathcal Y^+(f))\geq 2n+1.
  \end{displaymath}
\end{lemma}
\begin{proof}
  We prove the statement by contradiction. Without loss of generality, we assume that $\mathcal Y^-(f)$ and $\mathcal Y^+(f)$ are not empty. For any $y^- \in \mathcal Y^-(f),\,y^+\in \mathcal Y^+(f)$, suppose that $d(y^-,y^+)\leq 2n$, which implies that there are no more than $2n$ elements in $y^-$ and $y^+$ that are not the same. Therefore, there are no less than $m-2n$ elements in $y^-$ and $y^+$ that are the same. As a result, there exists $\mathcal I = \{i_1,\ldots,i_{m-2n}\}$, such that
  \begin{displaymath}
    y^-_i = y^+_i,\,\forall i\in \mathcal I,
  \end{displaymath}
  which further implies that
  \begin{displaymath}
    Proj_{\mathcal I}(y^-) = Proj_{\mathcal I}(y^+).
  \end{displaymath}
  Thus
  \begin{displaymath}
    Proj_{\mathcal I}(\mathcal Y^-(f)) \bigcap Proj_{\mathcal I}(\mathcal Y^+(f))\neq \phi,
  \end{displaymath}
  which contradicts with the definition of $\mathcal Y^+(f)$ and $\mathcal Y^-(f)$.
\end{proof}
The following lemma characterizes the relation between $d(y,\mathcal Y^+(f))$ and $d(y,\mathcal Y^-(f))$.
\begin{lemma}
  $d(y,\mathcal Y^-(f)) + d(y,\mathcal Y^+(f)) \geq 2n+1$.
  \label{lemma:distanceineq2}
\end{lemma}
\begin{proof}
  Without loss of generality, we can assume that $\mathcal Y^-(f)$ and $\mathcal Y^+(f)$ are not empty. By Lemma~\ref{lemma:attainable}, we know that there exist $y^- \in \mathcal Y^-(f)$ and $y^+ \in \mathcal Y^+(f)$, such that
  \begin{displaymath}
    d(y,\mathcal Y^-(f)) = d(y,y^-),\,d(y,\mathcal Y^+(f)) = d(y,y^+). 
  \end{displaymath}
  By Lemma~\ref{lemma:distanceineq1}, 
  \begin{displaymath}
    d(y^-,y^+) \geq d(\mathcal Y^-(f),\mathcal Y^+(f)) \geq 2n+1. 
  \end{displaymath}
  Now by triangle inequality
  \begin{displaymath}
    d(y,\mathcal Y^-(f)) + d(y,\mathcal Y^+(f))= d(y,y^-) + d(y,y^+)\geq d(y^-,y^+) \geq 2n+1. 
  \end{displaymath}
\end{proof}
Now we can prove the main theorem:
\begin{theorem}
  \label{theorem:generalcase}
  Consider function $g:\mathbb R^m\rightarrow\{-1,1\}$, which is defined as
  \begin{displaymath}
    g(y) = sgn(d(y,\mathcal Y^-(f))-d(y,\mathcal Y^+(f))).
  \end{displaymath}
  Then
  \begin{displaymath}
    Y^-(f)\subseteq\mathcal Y^-(f)\subseteq Y^-(g) ,\, Y^+(f)\subseteq\mathcal Y^+(f)\subseteq Y^+(g) . 
  \end{displaymath}
\end{theorem}
\begin{proof}
  We first prove that
  \begin{displaymath}
    \mathcal Y^{-}(f)\subseteq Y^-(g).
  \end{displaymath}
  Consider an arbitrary $y\in \mathcal Y^{-}(f)$, we need to prove that for any $u \in \mathbb R^{m}$ and $\gamma\in \Gamma$, $g(y +\gamma\circ u) = -1$. From definition,
  \begin{displaymath}
    d(y+\gamma\circ u,\mathcal Y^-(f)) \leq d(y+\gamma\circ u, y)\leq n. 
  \end{displaymath}

  By Lemma~\ref{lemma:distanceineq2},
  \begin{displaymath}
    d(y+\gamma\circ u,\mathcal Y^+(f)) + d(y+\gamma\circ u,\mathcal Y^-(f)) \geq 2n+1, 
  \end{displaymath}
  which implies that 
  \begin{displaymath}
    d(y+\gamma\circ u,\mathcal Y^+(f)) \geq n+1. 
  \end{displaymath}
  As a result,
  \begin{displaymath}
    d(y+\gamma\circ u,\mathcal Y^-(f))-d(y+\gamma\circ u,\mathcal Y^+(f)) \leq -1.
  \end{displaymath}
  Therefore $g(y+\gamma\circ u) = -1$, which concludes that $\mathcal Y^-(f) \subseteq Y^-(g)$.

  Similarly, one can prove that 
  \begin{displaymath}
    \mathcal Y^{+}(f)\subseteq Y^+(g),
  \end{displaymath}
  which concludes the proof.
\end{proof}

By Theorem~\ref{theorem:errorandset}, we know that $Y^-(f)\subseteq Y^-(g)$ and $Y^+(f)\subseteq Y^+(g)$ implies that $P_e(f)\geq P_e(g)$. As a result, we have the following Theorem:
\begin{theorem}
  The optimal $f_*$ must lies in set $\mathcal F$, where $\mathcal F$ is defined as
  \begin{displaymath}
    \mathcal F\triangleq\{f:f(y) = sgn(d(y,X^-)-d(y,X^+))\}. 
  \end{displaymath}
  where,
  \begin{displaymath}
    X^- = \{y\in \mathbb R^m:Proj_{\mathcal I}(y) = S_{\mathcal I}, \forall |I|=m-2n\},
  \end{displaymath}
  and
  \begin{displaymath}
    X^+ = \{y\in \mathbb R^m:Proj_{\mathcal I}(y) =\mathbb R^{m-2n}\backslash S_{\mathcal I}, \forall |I|=m-2n\}.
  \end{displaymath}
  Moreover $\forall f\in \mathcal F$, $X^+\subseteq Y^+(f)$ and $X^-\subseteq Y^-(f)$.
  \label{theorem:optimalfunction3}
\end{theorem}

In the next theorem, we prove that if $X^-$ and $X^+$ satisfies certain conditions, then $Y^+(f)$ and $Y^-(f)$ are measurable.
\begin{theorem}
  For a function $f\in \mathcal F$, if for all $|\mathcal I|=m-2n$,
  \begin{displaymath}
    Proj_{\mathcal I}(X^-) = S_{\mathcal I},\,Proj_{\mathcal I}(X^+) = \mathbb R^{m-2n}\backslash S_{\mathcal I},
  \end{displaymath}
  then $Y^-(f) = X^-$ and $Y^+(f) = X^+$.

  Moreover, if $S_{\mathcal I}$ is measurable for all $|\mathcal I| =m-2n$, then $Y^-(f)$ and $Y^+(f)$ are measurable and
  \begin{equation}
  \alpha(f) = P(y\notin Y^-(f)|\theta =-1), \beta(f) = P(y\in Y^+(f)|\theta = 1)
    \label{eq:probabilityerror}
  \end{equation}
  \label{theorem:optimalset}
\end{theorem}
\begin{proof}
  \begin{enumerate}
    \item By Theorem~\ref{theorem:optimalfunction3}, we know that 
      \begin{displaymath}
	X^+\subseteq Y^+(f),\,X^-\subseteq Y^-(f).
      \end{displaymath}
      Consider $Proj_{\mathcal I}( Y^-(f))$ with $|\mathcal I|=m-2n$, we know that
      \begin{displaymath}
	S_\mathcal I = Proj_{\mathcal I} (X^-) \subseteq Proj_{\mathcal I} (Y^-(f)).
      \end{displaymath}
      By Theorem~\ref{theorem:exclusion3}, we have
      \begin{equation}
	Proj_{\mathcal I}(Y^+(f) )\subseteq \mathbb R^{m-2n}\backslash S_\mathcal I.
	\label{eq:subset}
      \end{equation}
      Since \eqref{eq:subset} is true for every $|\mathcal I| = m-2n$, we know that $Y^+(f)\subseteq X^+$, which implies that $Y^+(f) = X^+$. Similarly we can prove that $Y^-(f) = X^-$.
    \item Suppose that $\mathcal S_{\mathcal I}$ is measurable for all $|\mathcal I| = m-2n$, $X^-$ can be written as
      \begin{displaymath}
	X^- = \bigcap_{|\mathcal I|=m-2n} \{y\in \mathbb R^m:Proj_{\mathcal I}(y)\in S_{\mathcal I}\}.
      \end{displaymath}
      As a result, $X^-$ is the intersection of finitely many measurable sets, which implies that $X^-$ is measurable. By the same argument we can conclude that $X^+$ is also measurable.  

      \eqref{eq:probabilityerror} follows directly from the fact that $Y^+(f)=X^+$ and $Y^-(f)=X^-$ are measurable and hence the inner and outer measure equal to the measure.
  \end{enumerate}
\end{proof}

\subsection{Optimal Detector for $n = (m-1)/2$}
In this section, we prove the optimal detector for the case where $n=(m-1)/2$. It is clear that the function $f\in \mathcal F$ is completely characterized by the sets $S_{\mathcal I}$s. Since $n = (m-1)/2$, $m-2n = 1$. As a result, we know that 
\begin{displaymath}
  X^- = \{y\in \mathbb R^m:Proj_i(y) = S_i, \forall i=1,\ldots,m\} = \prod_{i=1}^m S_i
\end{displaymath}
and
\begin{displaymath}
  X^+ = \{y\in \mathbb R^m:Proj_i(y) =\mathbb R\backslash S_i, \forall i=1,\ldots,m\} = \prod_{i=1}^m \mathbb R\backslash S_i,
\end{displaymath}
where $\prod_i S_i$ means Cartesian product. Since $X^-$ and $X^+$ can be written as a Cartesian product, it is easy to see the following lemma holds:
\begin{lemma}
  If $X^+\neq \phi$ and $X^-\neq \phi$, then $Proj_i(X^-) = S_i$ and $Proj_i(X^+) = \mathbb R\backslash S_i$.
  \label{lemma:nonempty}
\end{lemma}
Moreover, we have:
\begin{lemma}
  Let
  \begin{displaymath}
    \alpha_i = \inf \{P(y_i\in S|\theta = -1):S\in \mathcal B(\mathbb R),\mathbb R\backslash S_i\subseteq S\},
  \end{displaymath}
  and 
  \begin{displaymath}
    \beta_i = \sup \{P(y_i\in S|\theta = 1):S\in \mathcal B(\mathbb R),S\subseteq \mathbb R\backslash S_i\}.
  \end{displaymath}
  If $X^-,\,X^+\neq \phi$, then the following holds:
  \begin{displaymath}
    \alpha = 1-\prod_{i=1}^m (1-\alpha_i),\,\beta = \prod_{i=1}^m\beta_i. 
  \end{displaymath}
  \label{lemma:product}
\end{lemma}
\begin{proof}
  The proof is reported in the appendix for the sake of legibility.
\end{proof}

Let us define the set $T_i(\eta)$ and $\partial T_i(\eta)$ as
\begin{displaymath}
  T_{i}(\eta) \triangleq \{y_i\in \mathbb R:\log(\Lambda(y_i)) = \log\left(\frac{F(y_i|\theta_i=1)}{F(y_i|\theta_i = -1)}\right)< \eta\},
\end{displaymath}
and
\begin{displaymath}
  \partial T_{i}(\eta) \triangleq \{y_i\in \mathbb R:\log(\Lambda(y_i)) = \log\left(\frac{F(y_i|\theta_i=1)}{F(y_i|\theta_i = -1)}\right)= \eta\}.
\end{displaymath}
For simplicity, we define $T_i(\infty) = \mathbb R$ and $T_i(-\infty) = \phi$. In the next theorem, we prove what is the optimal form of $S_i$ by leveraging the independence of $y_i$.

\begin{theorem}
  The optimal $S_{i}$ must be of the following form: 
  \begin{equation}
    S_{i} = T_i(\eta_i) + U_i, 
    \label{eq:marginallikelihood}
  \end{equation}
  \label{theorem:optimalset2}
  where $U_i$ is a subset of $\partial T_i(\eta_i)$.
\end{theorem}
\begin{proof}
  It is easy to see that if $X^+$ ($X^-$) is empty, then $f = -1$ ($f=1$), which implies that $S_i = T_i(\infty)$ ($S_i = T_i(-\infty)$). Now assume that $X^+$ and $X^-$ are not empty, by Lemma~\ref{lemma:product}, 
  \begin{displaymath}
    P_e(f) = 1-p^+\prod_{i=1}^m \beta_i - p^-\prod_{i=1}^m(1-\alpha_i).
  \end{displaymath}
  Suppose the optimal $\alpha_i,\,\beta_i$ are $\alpha_i^*,\,\beta_i^*$. As a result, we know that
  \begin{displaymath}
    \begin{split}
      (\alpha_i^*,\beta_i^*) &= argmin_{(\alpha_i,\beta_i)}\;  1-\left[p^+\prod_{j\neq i} \beta_j^*\right]\beta_i - \left[p^-\prod_{j\neq i}(1-\alpha_j^*)\right](1-\alpha_i)\\
      & = argmin_{(\alpha_i,\beta_i)}\; a_i^*\alpha_i - b_i^* \beta_i+c_i^*,
    \end{split}
  \end{displaymath}
  where
  \begin{displaymath}
    a_i^* =   \left[p^-\prod_{j\neq i}(1-\alpha_j^*)\right], b_i^* = \left[p^+\prod_{j\neq i} \beta_j^*\right],\,c_i^*=1-a_i^*.
  \end{displaymath}
  By the Bayes Risk Criterion, the optimal $S_i$ must be of form \eqref{eq:marginallikelihood}, with $\eta_i = log(a_i^*/b_i^*)$.
\end{proof}
\begin{remark}
  It can be proved that $\beta_i$ is a function of $\alpha_i$, when $S_i$ is of the form \eqref{eq:marginallikelihood}. Due to Theorem~\ref{theorem:optimalset2}, we know that 
  \begin{displaymath}
    P_e^* =\min_{\alpha_i} 1-p^+\prod_{i=1}^m \beta_i - p^-\prod_{i=1}^m(1-\alpha_i),
  \end{displaymath}
  which can be solved numerically. 
\end{remark}
\section{Optimal Detector for Gaussian Model}
\section{Optimal Detector for Binary Model}

\section{Appendix}
\begin{proof}[Proof of Lemma~\ref{lemma:product}]
  We only prove that $\beta = \prod_{i=1}^m \beta_i$. The other equality follows the same argument. Since $X^-$ and $X^+$ are not empty, by Theorem~\ref{theorem:optimalset} and Lemma~\ref{lemma:nonempty}, we know that $X^- = Y^-(f)$ and $X^+ = Y^+(f)$. Therefore
  \begin{displaymath}
    \beta = \sup \{P(y\in Q|\theta = 1): Q\subseteq X^+,\, Q\in \mathcal B(\mathbb R^m)\}.
  \end{displaymath}
  Consider $R_i\subseteq \mathbb R\backslash S_i$ and $R_i\in \mathcal B(\mathbb R)$. It is trivial to see that
  \begin{displaymath}
    \prod_{i=1}^m R_i \subseteq \prod_{i=1}^m \mathbb R\backslash S_i = X^+.
  \end{displaymath}
  Hence, 
  \begin{displaymath}
    \beta \geq P(y\in\prod_{i=1}^m R_i|\theta = 1) = \prod_{i=1}^m P(y_i\in R_i|\theta = 1). 
  \end{displaymath}
  Taking the supremum on the right hand side over all possible $R_i$s, we have
  \begin{displaymath}
    \beta \geq \prod_{i=1}^m \beta_i. 
  \end{displaymath}
  On the other hand, suppose that $Q\subseteq X^+$ and $Q\in \mathcal B(\mathbb R^m)$. Therefore
  \begin{displaymath}
    Proj_i(Q)\subseteq Proj_i(X^+) = S_i.
  \end{displaymath}
  Moreover it can be proved that $Proj_i(Q)\in \mathcal B(\mathbb R)$, which implies that
  \begin{displaymath}
    P(y\in Q|\theta = 1)\leq P(y_i\in Proj_i(Q),\forall i|\theta = 1) = \prod_{i=1}^m P(y_i\in Proj_i(Q)|\theta = 1)\leq \prod_{i=1}^m \beta_i.
  \end{displaymath}
  Take the supremum on the left hand side over all possible $Q$s, we have $\beta \leq \prod_{i=1}^m \beta_i$, which concludes the proof.
\end{proof}
\begin{proof}[Proof of Lemma~\ref{lemma:distance}]
  Without loss of generality, let us assume that
  \begin{displaymath}
    \Lambda(y_{1})\geq  \Lambda(y_{2})\geq\ldots\geq\Lambda(y_{m}).
  \end{displaymath}
  It is trivial to prove that $n^-(y)+n^+(y) = 2n+1$. To prove $n^-(y) = d(y,X^-)$, we first show that $n^-(y) \geq d^-(y,X^-)$. Let 
  \[
  M = (m-2n-1)\max_i |\log(\Lambda(y_i))|+\eta.
  \]
  Due to Assumption 1, we know that $M < \infty$. Consider another vector $y^-\in\mathbb R^m$. If $i\leq n^-(y)$, then we choose $y_i^-$ such that 
  \[
  \log(\Lambda(y_i^-))< -M,\,i=1,\ldots,n^-(y),
  \] 
  and
  \[
  \log(\Lambda(y_i^-))\leq\log(\Lambda(y_{i-1}^-)) ,\,i=2,\ldots,n^-(y),
  \] 
  which is always possible due to Assumption 2. If $i > n^-(y)$, we choose $y_i^- = y_i$. It is easy to see that $d(y^-,y) = n^-(y)$. As a result, we only need to prove that $  y^- \in X^-$. From the construction of $y^-$, 
  \begin{equation}
    \log(\Lambda(y^-_{n^-(y)+1}))\geq \log(\Lambda(y^-_{n^-(y)+2})) \geq\ldots\geq  \log(\Lambda(y^-_{m}))\geq  \log(\Lambda(y^-_{1}))\geq\ldots\log(\Lambda(y^-_{n^-(y)})),
    \label{eq:descend}
  \end{equation}
  Since 
  \begin{displaymath}
    X^- = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}\log(\Lambda(y_i))<\eta,\forall |\mathcal I|=m-2n\} .
  \end{displaymath}
  We know that $y^-\in X^-$ if the sum of the first $m-2n$ terms in \eqref{eq:descend} is less than $\eta$. First suppose that $n^-(y)<2n+1$, which implies that $n^-(y) + m-2n\leq m$. Therefore
  \begin{displaymath}
    sgn(\sum_{i=1}^{m-2n} \log(\Lambda(y^-_{n^-(y)+i})) -\eta)=sgn(\sum_{i=1}^{m-2n} \log(\Lambda(y_{n^-(y)+i})) -\eta)= h_{n^-(y)+1}(y).
  \end{displaymath}
  From the definition of $n^-(y)$ and monotonicity of $h_k(y)$, we know that $h_1(y),\ldots,h_{n^-(y)}(y) = 1$ and $h_{n^-(y)+1},\ldots,h_{2n+1}(y) = -1$. Therefore
  \begin{displaymath}
    \sum_{i=1}^{m-2n} \log(\Lambda(y^-_{n^-(y)+i})) -\eta<0,
  \end{displaymath}
  which implies that $y^-\in X^-$. Now suppose that $n^-(y) = 2n+1$. It can be proved that
  \begin{displaymath}
    \begin{split}
      \sum_{i=m-2n+2}^m \log(\Lambda(y_i^-))+\log(\Lambda(y_1^-))&=\sum_{i=m-2n+2}^m \log(\Lambda(y_i))+\log(\Lambda(y_1^-))\\
      &< (m-2n-1)\max_i|\log(\Lambda(y_i))| - M \leq \eta,
    \end{split}
  \end{displaymath}
  which implies that $y^-\in X^-$. Hence, $n^-(y)\geq d(y,X^-)$. Similarly one can prove that $n^+(y)\geq d(y,X^+)$. By Lemma~\ref{lemma:distanceineq2}, we have
  \begin{displaymath}
    n^-(y) + n^+(y) = 2n+1 \leq d(y,X^-) + d(y,X^+),\, n^-(y) \geq d(y,X^-),\,n^+(y) \geq d(y,X^-).
  \end{displaymath}
  Therefore, $n^-(y) = d(y,X^-)$, $n^+(y) = d(y,X^+)$.
\end{proof}

\end{document}

