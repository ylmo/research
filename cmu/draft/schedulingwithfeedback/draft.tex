\documentclass[12pt,draftcls,onecolumn]{IEEEtran}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage{slashbox}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\title{\LARGE \bf {LQG, Sensor Scheduling, Acknowledgement and \dots}}
\author{Yilin Mo, Ling Shi, Bruno Sinopoli}
\begin{document}
\maketitle
Consider the following linear system:
\begin{equation}
  \begin{split}
    x_{k+1}& = Ax_k + Bu_k+ w_k,\\
  y_k& = Cx_k+v_k,
  \end{split}
  \label{eq:systemeq}
\end{equation}
where $x_k \in \mathbb R^{n}$ is the state vector, $y_k\in \mathbb R^m$ is the observation vector and $u_k$ is the control input. $w_k$ and $v_k$ are i.i.d. white Gaussian noise with mean $0$ and variance $Q,\,R$ respectively. We assume that the initial state $x_0 \in \mathbb R^n$ is also Gaussian with mean $0$ and variance $\Sigma$.

Suppose that a smart sensor is deployed to observe the system. At each time, the sensor computes the optimal estimate of $x_k$ as
\begin{displaymath}
 \hat x_k = E(x_k|y_0,\ldots,y_k). 
\end{displaymath}
The sensor can choose to send $\hat x_k$ to the controller or not. Let us define binary variables $\gamma_k$s such that $\gamma_k = 1$ if the sensor sends $\hat x_k$ to the controller and $\gamma_k = 0$ if otherwise. 

We also assume that the communication channel between the sensor and controller is a memoryless erasure channel. Let us define binary variables $\lambda_k$s such that $\lambda_k = 1$ if the channel is in good state at time $k$ and $\lambda_k = 0$ if otherwise. Hence, the controller can only receive $\hat x_k$ when both $\lambda_k = 1$ and $\gamma_k = 1$. We also assume that
\begin{displaymath}
 P(\lambda_k = 1) \triangleq p \in (0,1). 
\end{displaymath}

We will also assume that at each time step, the controller will send an ACK or NAK to the smart sensor, indicating whether $\hat x_k$ is received or not. Moreover we assume that ACK or NAK will be received without any loss since the controller has enough energy to ensure reliable communication.

Suppose that due to the energy or bandwidth constraints, the smart sensor cannot always send local estimates to the controller. Let us define the average communication rate 
\begin{displaymath}
  q \triangleq  \limsup_{N\rightarrow\infty} \frac{\sum_{k=0}^{N-1}\gamma_k}{N}.
\end{displaymath}
We will assume that $q \leq q_0\in (0,1)$, where $q_0$ is a design variable.

Let us also define the infinite horizon LQG cost to be
\begin{displaymath}
  J \triangleq  \limsup_{N\rightarrow\infty} \frac{\sum_{k=0}^{N-1}x'_kWx_k + u'_kUu_k}{N}.
\end{displaymath}

The goal of the smart meter is to determine the optimal $\gamma_k$ such that $J$ is minimized while satisfying the communication rate constraints.

\subsection{Optimal Controller}
The optimal controller is a linear feedback controller of the following form:
\begin{displaymath}
 u_k = L\tilde x_k,
\end{displaymath}
where 
\begin{displaymath}
  \tilde x_k =\begin{cases} 
    \hat x_k & \gamma_k = 1\textrm{ and }\lambda_k = 1\\
    A\tilde x_{k-1} + Bu_{k-1} & \gamma_k = 0\textrm{ or }\lambda_k = 0\\
  \end{cases}
\end{displaymath}
The gain $L$ is given by  
\begin{displaymath}
  L = -(B'SB+U)^{-1} B'SA,
\end{displaymath}
where $S$ is the solution of the following Riccati equation
\begin{equation}
  \label{eq:lqgcontrolS}
    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
\end{equation}
Therefore, $J$ can be computed as
\begin{displaymath}
 J = trace(SQ) + \limsup_{N\rightarrow\infty} \frac{\sum_{k=0}^{N-1}trace[(A'SA+W-S)P_k]}{N}, 
\end{displaymath}
where 
\begin{displaymath}
 P_k =  \begin{cases} 
    P & \gamma_k = 1\textrm{ and }\lambda_k = 1\\
    AP_{k-1}A' + Q & \gamma_k = 0\textrm{ or }\lambda_k = 0\\
  \end{cases}
\end{displaymath}
where $P$ is the solution of the following Riccati equation
\begin{displaymath}
  P = \left[\left( APA'+Q \right)^{-1} + C'R^{-1}C\right]^{-1} 
\end{displaymath}
Let us define 
\begin{displaymath}
\tau_k = k - \max\left(\{i\leq k:\gamma_i\lambda_i\neq 0\}\bigcup \{0\}\right).
\end{displaymath}
Therefore, $\tau_k$ indicates the interval between the last successful communication and the current time. Moreover, let us also define
\begin{displaymath}
  h^0(X) = X,\;h(X) = AXA' + Q,\;h^k(X) = h^{k-1}(h(X)),
\end{displaymath}
and
\begin{displaymath}
 f(k) =  trace[(A'SA+W-S)h^k(P)].
\end{displaymath}
Therefore, it is easy to see that the LQG cost can be written as
\begin{displaymath}
 J = trace(SQ) + \limsup_{N\rightarrow\infty} \frac{\sum_{k=0}^{N-1}f(\tau_k)}{N}, 
\end{displaymath}
\subsection{Optimal Scheduling}
\begin{conjecture}
  The optimal scheduling of the smart sensor only depends on $\tau_{k-1}$.
\end{conjecture}
For each $\tau_{k-1}$, the smart sensor will randomly choose the send a packet with probability $\alpha(\tau_{k-1})$. Therefore, $\tau_k$ satisfies the following transition probability
\begin{align*}
  \tau_{k} = \begin{cases}
    0&\textrm{with probability }\alpha(\tau_{k-1})p\\
    \tau_{k-1}+1&\textrm{with probability }1-\alpha(\tau_{k-1})p\\
\end{cases}
\end{align*}
Therefore, the state $\tau_k$ becomes a stationary Markov chain. Suppose that the stationary distribution is
\begin{displaymath}
 P(\tau_k = n) = \beta_n. 
\end{displaymath}
Therefore, the LQG cost can be written as
\begin{displaymath}
 J = trace(SQ) + \sum_{n=0}^\infty f(n)\beta_n.
\end{displaymath}
The average communication rate is
\begin{displaymath}
  q = \sum_{n = 0}^\infty \alpha(n)\beta_n. 
\end{displaymath}
\emph{Question: Is there a way to find the optimal $\alpha(k)$?}

\end{document}
