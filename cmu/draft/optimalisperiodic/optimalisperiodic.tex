\documentclass[12pt,draftcls,onecolumn]{IEEEtran}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage{slashbox}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\begin{document}

Consider the following discrete-time LTI system
\begin{equation}
    x_{k+1} = Ax_k + w_k,
    \label{eq:systemdescription}
\end{equation}
where $x_k \in \mathbb R^n$ represents the state and $w_k\in \mathbb R^n$ the disturbance. It is assumed that $w_k$ and $x_0$ are independent Gaussian random vectors, $x_0 \sim \mathcal N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$, where $\Sigma,\,Q \geq 0$. A wireless sensor network composed of $m$ sensing devices $S = \{s_1,\ldots,s_m\}$ and one fusion center $s_0$ is used to monitor the state of system \eqref{eq:systemdescription}. The measurement equation is
\begin{equation}
  y_{k} = C x_k + v_k,
\end{equation}
where $y_k = [y_{k,1}, y_{k,2} ,\ldots, y_{k,m}]' \in \mathbb R^m$ is the measurement vector\footnote{The $'$ on a matrix always means transpose.}. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$, $C = \left[C_1',\ldots,C_m'\right]'$ is the observation matrix and the matrix pair $(C,\,A)$ is assumed observable, $v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed to be independent of $x_0$ and $w_k$. 

Suppose that due to various constraints, only a subset of sensors can be chosen to send their measurements to the fusion center. Denote the collection of all possible subsets as $\mathcal S \subseteq 2^S$.

For any $\mathcal I =\{s_{i_1},\ldots,s_{i_l}\} \subseteq S$, we define
\begin{displaymath}
  \Gamma(\mathcal I) \triangleq = \left[e_{i_1},\ldots,e_{i_l}\right]' ,\, C(\mathcal I) \triangleq \Gamma(\mathcal I)C,\, R(\mathcal I) \triangleq \Gamma(\mathcal I)R\Gamma(\mathcal I)',
\end{displaymath}
where $e_i$ is an all zero vector except the $i$th elements to be one.

Define $g(X,\mathcal I)$ as
\begin{displaymath}
  g(X,\mathcal I) \triangleq \left[ (AXA' + Q)^{-1} + C(\mathcal I)'R(\mathcal I)^{-1}C(\mathcal I)\right]^{-1} 
\end{displaymath}

A schedule is defined as an infinite sequence of $\sigma = \{\mathcal I_k\},k\in \mathbb N$, where $\mathcal I_k \in \mathcal S$. It is easy to prove that the estimation error of Kalman filter satisfies the following equation if schedule $\sigma$ is used:
\begin{displaymath}
  P_{k} = g(P_{k-1},\mathcal I_k).
\end{displaymath}

A schedule if called feasible if for any initial condition $P_0$, $P_k(\sigma)$ is upper bounded by $M_{P_0}$.

Let us define $J(\sigma,P_0)$ as
\begin{displaymath}
  J(\sigma,P_0) \triangleq \limsup_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N trace(P_k(\sigma)).
\end{displaymath}

\begin{theorem}
  If $\sigma$ is a feasible schedule, then $J$ does depends on $P_0$.
\end{theorem}
\begin{proof}
Theorem 2, Corollary 2 in the attached paper.
\end{proof}

As a result, we can simply write $J(\sigma,P_0)$ as $J(\sigma)$. Define the optimal schedule $\sigma^*=\{\mathcal I_k^*\}$ as
\begin{displaymath}
  \sigma^* = argmin_{\sigma \textrm{ feasible}} J(\sigma). 
\end{displaymath}

$\sigma = \{\mathcal I_k\}$ is called periodic if there exists $T \in \mathbb N$ such that $\mathcal I_{k+T} = \mathcal I_k$ for all $k$. Now we can state the main theorem:
\begin{theorem}
  For any $\varepsilon > 0$, there exists a periodic $\sigma$, such that
  \begin{displaymath}
   J(\sigma)\leq J(\sigma^*)+\varepsilon. 
  \end{displaymath}
\end{theorem}
\begin{proof}
To simplify notations, let us define $q = J(\sigma^*)$. Since $J$ is independent of $P_0$, we can choose $P_0 = qI_n$.

By the definition of $J$, we know that there must exists infinitely many $k$, such that $trace(P_k(\sigma^*))\leq q$. However, since $P_k$ is always positive semi-definite, $trace(P_k(\sigma^*))\leq q$ implies that $P_k(\sigma^*)\leq P_0=qI_n$.

Again by the definition of $J$, there exists $K$, such that
\begin{displaymath}
  \frac{1}{N}\sum_{k=1}^N trace(P_k(\sigma^*)) \leq q+\varepsilon, 
\end{displaymath}
for all $N \geq K$. As a result, we can choose $T$ such that
\begin{displaymath}
  \frac{1}{T}\sum_{k=1}^T trace(P_k(\sigma^*)) \leq q+\varepsilon,\, P_T(\sigma^*)\leq P_0.
\end{displaymath}

Now define a periodic schedule as $\sigma = \{\mathcal I_k\}$, such that
\begin{displaymath} 
  \mathcal I_k = \mathcal I_{k-i T}^*,
\end{displaymath}
when $i T < k \leq (i+1)T$. It is easy to see that since we simply copy the first $T$ step from the optimal schedule to the new schedule,
\begin{displaymath}
 P_T(\sigma) = P_T(\sigma^*) \leq P_0.
\end{displaymath}
Since $g(X,\mathcal I)$ is decreasing with respect to $X$, it is easy to prove the following:
\begin{displaymath}
  P_{i T}(\sigma) \leq P_0,\forall  i\in \mathbb N.
\end{displaymath}
Therefore, it is trivial to prove that
\begin{displaymath}
  P_{iT+j}(\sigma)\leq P_j,\forall i,j\in \mathbb N. 
\end{displaymath}
Therefore, $\sigma$ must be feasible (Lemma 2 in the attached paper) and
\begin{displaymath}
  J(\sigma)\leq \frac{1}{T}\sum_{k=1}^T trace(P_k(\sigma)) = \frac{1}{T}\sum_{k=1}^T trace(P_k(\sigma^*))\leq q+\varepsilon,
\end{displaymath}
which finishes the proof.
\end{proof}

\end{document}

