clc
clear
n = 1;
m = 1;
A = diag(rand(n,1))/0.9; % generate a stable A
C = rand(m,n);
Q = eye(n);
sqrtQ = sqrtm(Q);
R = eye(m);
sqrtR = sqrtm(R);

Sigma = eye(n);
P = Sigma;

x = sqrtm(Sigma)*randn(n,1);
hatx = zeros(n,1);

Px = zeros(n); %asymptotic covariance of x
for k = 1:100
    Px = A * Px * A' + Q;
end
Py = C * Px * C' + R; %asymptotic covariance of y

tmp = rand(m);
Y = tmp * tmp';
invY = eye(m)/Y;

rate = 1 - det(eye(m) + Py*Y)^(-0.5); %communication rate

N = 100000;
empiricalmse = zeros(n); %average error*error'
mse = zeros(n); %average P_k
empiricalrate = 0;

for k = 1:N
    %linear dynamic
    x = A * x + sqrtQ * randn(n,1);
    y = C * x + sqrtR * randn(m,1);
    %prediction step
    hatx = A * hatx; 
    P = A * P * A' + Q;
    %uniform random variable
    zeta = rand();
    threshold = exp(-0.5*y'*Y*y);
    if zeta < threshold % not send y
        L = P * C' / ( C*P*C' + R + invY);
        hatx = (eye(n) - L*C)*hatx;
        P = P - L*C*P;
    else % send y
        L = P * C' / ( C*P*C' + R);
        hatx = (eye(n) - L*C)*hatx + L*y;
        P = P - L*C*P;
        empiricalrate = empiricalrate + 1;
    end
    mse = mse + P;
    error = x - hatx;
    empiricalmse = empiricalmse + error * error';
end

mse = mse / N;
empiricalmse = empiricalmse / N;
empiricalrate = empiricalrate / N;
