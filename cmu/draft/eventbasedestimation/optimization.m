n = 5;
m = 5;
A = rand(n);
Q = rand(n);
Q = Q * Q';
C = rand(m,n);
R = rand(m);
R = R * R';
Delta = 5*eye(n);
cvx_begin sdp
variable M1(n,n) symmetric
variable S(n,n) symmetric
variable M2(m,m) symmetric
variable Y(m,m) symmetric diagonal
minimize(trace(Y))
Y >= 0
S >= inv(Delta)
M1 >= 0
M2 >= 0
M1-S+C'*M2*C >= 0
[inv(Q)-M1 inv(Q)*A;A'*inv(Q) A'*inv(Q)*A+S] >= 0
[inv(R)-M2 inv(R);inv(R) inv(R)+Y] >= 0
cvx_end