clc
clear
n = 1;
m = 1;
A = 0.5; % generate a stable A
C = 1;
Q = eye(n);
sqrtQ = sqrtm(Q);
R = eye(m);
sqrtR = sqrtm(R);

Sigma = eye(n);
x = sqrtm(Sigma)*randn(n,1);

hatx = zeros(n,1);
P = Sigma;

Px = dlyap(A,Q);      %asymptotic covariance of x
Py = C * Px * C' + R; %asymptotic covariance of y

%communication rate
for index = 10:10
    
    rate = index/10;


    N = 1000;
    empiricalmse = zeros(n); %average error*error'
    mse = zeros(n); %average P_k
    empiricalrate = 0;

    for k = 1:N
        %linear dynamic
        x = A * x + sqrtQ * randn(n,1);
        y = C * x + sqrtR * randn(m,1);
        %prediction step
        hatx = A * hatx;
        P = A * P * A' + Q;
        %uniform random variable
        zeta = rand();
        threshold = 1 - rate;
        if zeta < threshold % not send y
            hatx = hatx;
            P = P;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y;
            P = P - L*C*P;
            empiricalrate = empiricalrate + 1;
        end
        mse = mse + P;
        error = x - hatx;
        empiricalmse = empiricalmse + error * error';
    end

    mse = mse / N;
    empiricalmse = empiricalmse / N;
    empiricalrate = empiricalrate / N;
    
    performance(index) = mse;
end
