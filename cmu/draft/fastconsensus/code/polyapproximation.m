clc
clear
%assume lambda_2 = a, lambda_n = 0
a = 0.7;
N = 5;
point = [0:N]'*a/N;
point(N+2) = 1;


A = zeros(N+2);
for i = 0:N
    A(:,i+1) = point.^(N-i);
end

for i = N+1:-1:1
    A(i,N+2) = (-1)^(N-i);
end

v = zeros(N+2,1);
v(N+2)=1;

for step = 1:5
    x = (A\v)';
    p = x(1:N+1);
    maxima = sort(roots(polyder(p)));
    for i = 0:N
        A(2:N,i+1) = maxima.^(N-i);
    end
end

epsilon = x(N+2);
ratio = log(epsilon)/log(a)/N;