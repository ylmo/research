\documentclass{IEEEtran}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}
\usepackage{slashbox}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\newcommand{\R}{\mathbb R}
\newcommand{\here}{\bigskip\centerline{\bf\Large Here !!!}\bigskip}
%\renewenvironment{proof}[1][Proof]{\par\normalfont
%\relax\trivlist\item[\hskip\labelsep \itshape #1.]\ignorespaces}{\endtrivlist}
% Lemma
\newenvironment{proof-lemma}[1]
{\nopagebreak \parindent=0ex \begin{IEEEproof}[Proof of Lemma #1]}
  {\end{IEEEproof} }

  % Theorem
  \newenvironment{proof-theorem}[1]
  {\nopagebreak  \parindent=0ex \begin{IEEEproof}[Proof of Theorem #1]}
    {\end{IEEEproof}}

    \def\qedsymbol{\ensuremath{\Box}}      % qed symbol(requires latexsym)
    \def\qed{\ifhmode\unskip\nobreak\fi\quad\qedsymbol}     % qed symbol
    \def\frqed{\ifhmode\nobreak\hbox to5pt{\hfil}\nobreak%
    \hskip 0pt plus1fill\nobreak\fi\quad\qedsymbol\renewcommand{\qed}{}} % flushed right qedsymbol

    \def\QEDsymbol{\vrule width.6em height.5em depth.1em\relax}% QED symbol
    \def\frQED{\ifhmode\nobreak\hbox to5pt{\hfil}\nobreak%
    \hskip 0pt plus1fill\nobreak\fi\quad\QEDsymbol\renewcommand{\qed}{}} % flushed right QED symbol
    \def\QED{\ifhmode\unskip\nobreak\fi\quad\QEDsymbol}     % QED symbol
    \DeclareMathOperator*{\argmin}{arg\; min}     % argmin
    \DeclareMathOperator*{\argmax}{arg\; max}     % argmax
    \DeclareMathOperator{\sgn}{sgn}           % sign
    \DeclareMathOperator{\Proj}{Trunc}           % sign
    \newcommand{\matt}[1]{\begin{bmatrix}#1\end{bmatrix}}

      \newcommand{\jph}[1]{\footnote{\color{magenta}\sl Joao's comment: #1}}

      \title{\LARGE \bf {Robust Detection against Integrity Attacks: A Heuristic Detector Design}}

      \author{Yilin Mo$^*$, Jo\~{a}o Hespanha$^\dag$, Bruno Sinopoli$^*$
      \thanks{
      This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation and grant NGIT2009100109 from Northrop Grumman Information Technology Inc Cybersecurity Consortium. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
      \thanks{
      $*$: Yilin Mo and Bruno Sinopoli are with the ECE department of Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu, brunos@ece.cmu.edu}
      }
      \thanks{
      $\dag$: Jo\~{a}o Hespanha is with the ECE department of University of California, Santa Barbara, CA. Email: {hespanha@ece.ucsb.edu}
      }
      }

      \begin{document} \maketitle

      \begin{abstract}
	We consider the detection of a binary random state based on $m$ noisy measurements that can be manipulated by an attacker. The attacker is assumed to have full information about the true value of the state to be estimated as well as the values of all the measurements. However, the attacker can only manipulate $n$ of the $m$ measurements. The detection problem is formulated as a minimax optimization, where one seeks to construct an optimal detector that minimizes the ``worst-case'' probability of error against all possible manipulations by the attacker. In our previous work \cite{Yilinacc2012}, we show the optimal detector for $m\leq 2n$  and $m = 2n+1$. In this paper, we provide a heuristic detector design for the case where $m> 2n$, and prove the asymptotic optimality of the detector when $m\rightarrow \infty$. A numerical example is provided to illustrate the performance of the proposed detector. 
      \end{abstract}
      \section{Introduction}
      The increasing use of networked embedded sensors to monitor and control critical infrastructures provides potential malicious agents with the opportunity to disrupt their operations by corrupting sensor measurements. Supervisory Control And Data Acquisition (SCADA) systems, for example, run a wide range of safety critical plants and processes, including manufacturing, water and gas treatment and distribution, facility control and power grids. A successful attack to such kind of systems may significantly hamper the economy, the environment, and may even lead to the loss of human life. The first-ever SCADA system malware (called Stuxnet) was found in July 2010 and rose significant concern about SCADA system security~\cite{Chen2010,Fidler2011}. While SCADA systems are currently mostly isolated, next generation SCADA will make extensive use of widespread sensing and networking, both wired and wireless, making critical infrastructures susceptible to cyber security threats. The research community has acknowledged the importance of addressing the challenge of designing secure detection, estimation and control systems~\cite{challengessecurity}.

      We consider a robust detection problem inspired by security concerns that arise from the possible manipulation of sensor data. We focus our attention on the detection of a binary random variable $\theta$ from independent measurements collected by $m$ sensors, with the caveat that some of these measurements can be manipulated by an attacker. The attacker is assumed to have full information about the true value of $\theta$ and all the measurements and uses this information to manipulate the data available to the detector. %Limitations in the resources available to the attacker enable him to only manipulate $n$ of the $m$ sensors. 

     Minimax robust detection problems have been extensively studied in the past decades\cite{Huber1965,Huber1973,Kassam1985}. A classical approach assumes that the conditional distribution of sensor measurements under each hypothesis lies in a set of probability distributions, which is called an uncertainty class. One then identifies a pair of ``least favorable distributions'' (LFDs) from the uncertainty class, which conceptually represents the most similar and hardest to distinguish pair of distributions. The robust detector is then designed as a naive-Bayes or Neymann-Pearson detector between the LFDs. The main difficulty in applying the LFD-based method to our scenario is that there is no systematic procedure to construct the LFDs and the corresponding detector. As a result, in this paper, we attempt to directly compute the optimal detector instead of seeking LFDs.

      Basar et al. \cite{Basar1986,Bansal1989} consider the problem of transmitting and decoding Gaussian signals over a communication channel with unknown input from a so-called ``jammer''. The unknown input is assumed to be mean square bounded by a constant, which depends upon the capability of the ``jammer''. Although this set-up is reasonable for analog communications where the attacker is energy-constrained, it is not practical for cyber attacks on digital communications, where the attacker can change the data arbitrarily when the integrity of the sensor is compromised.

      In our previous work \cite{Yilinacc2012}, we provide explicit ways to construct the optimal detector for the case $m\leq 2n$ and $m = 2n+1$. In this paper, we provide a heuristic detector design for general $m > 2n$ based on truncated sum, which achieves asymptotic optimality when $m$ goes to $\infty$. 

      The rest of paper is organized as follows: In Section~\ref{sec:problem} we formulate the problem of robust detection with $n$ manipulated measurements from $m$ total measurements. In Section~\ref{sec:optimal2}, we prove that the optimal detector takes a special form based on a Hamming-like distance and cast the optimal detector design problem as an optimization problem. In Section~\ref{sec:heuristic} we propose a heuristic detector design and prove its asymptotic optimality. In Section~\ref{sec:example} we provide a numerical example of i.i.d. Gaussian signals. Section~\ref{sec:conclusion} finally concludes the paper.

      \section{Problem Formulation}
      \label{sec:problem}

      In this section, we formulate the optimal detector design problem as a minimax problem. It is worth noticing that the problem formulation is similar to that of our previous work \cite{Yilinacc2012}, which is included here for the sake of completeness. Our goal is to detect a binary random variable (r.v.) $\theta$ with distribution
      \begin{align*}
	\theta=\begin{cases}
	  -1 &\text{w.p.~}p^-\\
	  +1 &\text{w.p.~}p^+\\
	\end{cases}
      \end{align*}
      where $p^-,\,p^+\geq 0$ and $p^-+p^+ = 1$. Without loss of generality, we assume that $p^+ \geq p^-$. To detect $\theta$ we have available a vector $y \triangleq [y_1,\ldots,y_m]'\in\mathbb R^m$ of $m$ sensor measurements $y_i\in\R$, $i\in\{1,2,...,m\}$,  where
      \begin{displaymath}
	y_i = \theta + v_i,
      \end{displaymath}
      and $v_i$ are assumed to be i.i.d. Gaussian noise with zero mean and variance of $r$. Moreover we assume that $v_i$s are independent of $\theta$.

      We assume that an attacker wants to increase the probability that we make an error in detecting $\theta$. To this end, the attacker has the ability to manipulate $n$ of the $m$ sensor measurements, but we do not know which $n$ of the $m$ measurements have been manipulated. Formally, this means that our estimate of $\theta$ has to rely on a vector $y'\in\R^m$ of \emph{manipulated measurements} defined by
      \begin{align}  \label{eq:compromisevector}
	y' = y + \gamma \circ u,
      \end{align}
      where the attacker chooses the \emph{sensor-selection} vector $\gamma$
      taking values in
      \begin{displaymath}
	\Gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i \leq n\}
      \end{displaymath}
      and the \emph{bias} vector $u$ taking values in $\R^m$. The nonzero values of $\gamma$ correspond to  the $n$
      sensors being manipulated. The ``magnitude'' of manipulation is
      determined by $u$.

      The detection problem is formalized as a minimax problem where one wants to select an optimal detector
      \begin{align}\label{eq:estimate}
	\hat \theta = f(y') =f(y+\gamma\circ u)
      \end{align}
      so as to minimize the probability of error, for the worst case manipulation by the adversary. Following Kerckhoffs' Principle~\cite{kerckhoffs1883} that security should not rely on the obscurity of the system, our goal is to design the detector $f:\R^m\to\{-1,1\}$ assuming that $f$ is known to the attacker. We also take the conservative approach that the attacker has full information about the state of the system. Namely, the underlying $\theta$ and all the measurements $y_1,\ldots,y_m$ are assumed to be known to the attacker. In addition the attacker can manipulate up to $n$ of the $m$ sensors. We assume that the defender knows how many sensors may be compromised, but cannot identify them. Our goal is to analyze the problem for different values of $n$, ranging from $1$ to $m$.

      To compute the worst-case probability of error that we seek to minimize, we consider given values of $\theta$, $y$ and an detector $f$, for which an optimal policy for the attacker can be written as follows:
      \begin{align*}
	(u,\gamma)= \begin{cases}
	  \displaystyle \argmin_{u\in\R^m,\gamma\in\Gamma} f(y+\gamma\circ u) & \theta =1\\
	  \displaystyle \argmax_{u\in\R^m,\gamma\in\Gamma} f(y+\gamma\circ u) & \theta =-1,
	\end{cases}
      \end{align*}
      where the selection of the manipulation pair $(u,\gamma)$ tries to get $\hat \theta$ in \eqref{eq:estimate} as low as possible when $\theta=1$ (ideally as low as $-1$) or as high as possible when $\theta=-1$ (ideally as high as $1$). The min and max are attainable since $f$ only takes $\pm 1$.

      Under this worst-case attacker policy, a correct decision will be made only when the pair $(\theta,y)$ belongs to the set
      \begin{align}\label{eq:error-set}
	\Big\{ (-1,y): y\in Y^-(f) \Big\} \cup \Big\{ (+1,y): y\in Y^+(f) \Big\}
      \end{align}
      where $Y^+(f)$ and $Y^-(f)$ denote the set of measurement values
      $y\in\R^m$ for which the attacker cannot force the estimate to be $-1$
      and $+1$, respectively, i.e.,
      \begin{align*}
	Y^+(f) &\triangleq \big\{y\in \mathbb R^m:f(y+\gamma\circ u)=1, \; \forall u\in\R^m,\gamma\in\Gamma\big\},\\
	Y^-(f) &\triangleq \big\{y\in \mathbb R^m: f(y+\gamma\circ u)=-1,\; \forall u\in\R^m,\gamma\in\Gamma\big\}.
      \end{align*}

      For a given detector $f$, the worst-case probability of error
      $P_e(f)$ is then given by the measure of the set defined in
      \eqref{eq:error-set} and can be expressed as
      \begin{align}\label{eq:prob-error}
	P_e(f) &\triangleq (1-\beta(f))P(\theta = 1) +\alpha(f) P(\theta = -1)\notag\\
	&=(1-\beta(f))p^+ +\alpha(f)p^-,
      \end{align}
      with
      \begin{align*}
	\alpha(f) &\triangleq 1-\sup\{P(y\in S|\theta = -1):S\in \mathcal B(\mathbb R^m),S\subseteq  Y^-(f)\},\\
	\beta(f) &\triangleq \sup\{P(y\in S|\theta = 1):S\in \mathcal B(\mathbb R^m),S\subseteq Y^+(f)\},
      \end{align*}
      where $\mathcal B(\mathbb R^m)$ is the Borel $\sigma$-algebra on $\mathbb
      R^m$. One should think of $\alpha(f)$ as the measure of the set
      $\R^m\setminus Y^-(f)$ conditioned to $\theta=-1$ and of $1-\beta(f)$ as the
      measure of the set $\R^m\setminus Y^+(f)$ conditioned to $\theta=+1$. The
      more complicated definitions of $\alpha(f)$ and $\beta(f)$ use inner measures
      to make sure that $P_e$ is well defined even if these sets are not measurable\footnote{Even if the original function $f$ is measurable, the sets $Y^+$ and $Y^-$ may not be necessarily measurable.}.

      Formally, the problem under consideration is to determine the optimal
      detector $f$ in \eqref{eq:estimate} that minimizes the worst-case
      probability of error in \eqref{eq:prob-error}:
      \begin{displaymath}
	P_e^* = \inf_f P_e(f).
      \end{displaymath}

      From the discussion above, we can recognize $Y^+(f)$ and $Y^-(f)$ as
      ``good'' sets for the detector, in the sense that when measurements
      fall in these sets the attacker cannot induce errors. From this
      perspective, good detection policies obviously correspond to these sets being
      large. This statement is formalized, without proof, in the following lemma:
      \begin{lemma}
	Given two functions $f,g:\mathbb R^m\rightarrow\{-1,1\}$, if $Y^+(g)\supseteq
	Y^+(f)$ and $Y^-(g)\supseteq Y^-(f)$, then $P_e(g)\le P_e(f)$.
	\label{theorem:errorandset}
      \end{lemma}

      \section{Optimal Detector Design }
      \label{sec:optimal2}

      In this section, we show that the optimal detector is a threshold rule based on a Hamming-like distance between the (manipulated) measurement vector and two appropriately defined sets.

      It is clear from Lemma~\ref{theorem:errorandset}, that in order to find the optimal detector $f$, we should try to maximize the ``volume'' of good set $Y^+(f)$ and $Y^-(f)$. However, it is also easy to see that there is a trade-off between $Y^+(f)$ and $Y^-(f)$. In other words, enlarging one set usually results in shrinking the other set. To characterize such trade-off, we need to introduce the following notation: We denote by $d:\mathbb R^m\times \mathbb R^m \rightarrow \mathbb N_0$ the metric induced by the ``zero-norm,'' i.e.,
      \begin{displaymath}
	d(x,y) \triangleq \|x - y\|_0,
      \end{displaymath}
      where $\|x\|_0$ is the ``zero-norm'' of $x$, which is defined as the number of non-zero entries of the vector $x$. While the ``zero-norm'' is not a norm, it is easy to verify that the function $d$ defined above is a metric. In fact, $d$ can be viewed as an extension of the Hamming distance to continuous-valued vectors. The metric $d$ can be generalized to sets in the usual way: given an element $x$ and two subsets $X,Y$ of $\mathbb R^m$, we define
      \begin{align}
	d(X,Y) &\triangleq \min_{x\in X,\,y\in Y}d(x,y) &
	d(x,Y) &\triangleq d(\{x\},Y).
	\label{eq:distanceset}
      \end{align}
      For convenience, we define the distance from any set to the empty set to be infinity: $d(X,\emptyset) = \infty$. The minimum in \eqref{eq:distanceset} is always attainable since $d$ takes only integer values.

      Now we have the following theorem to characterize the relationship between $Y^+(f)$ and $Y^-(f)$:
      \begin{theorem}
	For any $f$, the following inequality holds:
	\begin{equation}
	  d(Y^+(f),Y^-(f))\geq 2n+1.
	  \label{eq:distancenecessary}
	\end{equation}
	On the contrary, for any two sets $X^+,\,X^-\subseteq \mathbb R^m$, if $d(X^+,X^-)\geq 2n+1$, then for the following $f$ defined as:
	\begin{equation}
	  f = \begin{cases}
	    1 & d(y, X^-)\geq d(y, X^+),\\
	    -1 & d(y,X^-)< d(y,X^+),
	  \end{cases}
	  \label{eq:ffunction}
	\end{equation}
	the following inequality hold:
	\begin{equation}
	  X^+\subseteq Y^+(f),\,X^-\subseteq Y^-(f). 
	  \label{eq:setincursion}
	\end{equation}
	\label{theorem:distance}
      \end{theorem}

      \begin{proof}
	We will first prove \eqref{eq:distancenecessary} by contradiction. Suppose that $d(Y^+(f),Y^-(f))\leq 2n$, which implies that both $Y^+(f)$ and $Y^-(f)$ are non-empty. As a result, there exists $y^+\in Y^+(f)$ and $y^-\in Y^-(f)$, such that $d(y^+,y^-) \leq 2n$, which implies that at least $m-2n$ entries of $y^-$ and $y^+$ are the same. Without loss of generality, we assume that the first $m-2n$ entries of $y^+$ and $y^-$ are the same, which implies that  
	\[
	y^+ = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^+,\ldots,y_{m}^+]',
	\]
	and
	\[
	y^- = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^-,\ldots,y_{m}^-]'.
	\]
	Now let us consider another $y$ defined as
	\[
	y = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^+\ldots,y_{m-n}^+,y_{m-n+1}^-,\ldots,y_m^-]'.
	\]
	It can be easily seen that there are at most $n$ elements in $y$ that differ from $y^+$ and at most $n$ elements in $y$ that differ from $y^-$. As a result, $f(y) = 1$ from the definition of $Y^+(f)$ and $f(y) = -1$ from the definition of $Y^-(f)$, which is a contradiction.

	Now we want to prove that the $f$ defined in \eqref{eq:ffunction} satisfies inequality \eqref{eq:setincursion}. First if $X^+$ ($X^-$) is empty, then the corresponding $f = -1$ ($f=1$), the corresponding $Y^-(f)$ ($Y^+(f)$) is $\mathbb R^m$, which always contains $X^-$ ($X^+$). As a result, we will only consider non-empty $X^+$ and $X^-$. Suppose $X^+,X^-\subset \mathbb R^m$ and $d(X^+,X^-)\geq 2n+1$. By triangular inequality, we know that for all $y$, the following inequality holds:
	\begin{align}
	  d(y,X^-) + d(y, X^+)  &= d(y,y^-)+d(y,y^+)\nonumber\\
	  &\geq d(y^-,y^+)\geq d(X^-,X^+)\geq 2n+1,\label{eq:distance2}
	\end{align}
	where we use the fact that $d(y,X^-) = d(y,y^-)$ for some $y^-\in X^-$. Now Consider an arbitrary $y\in X^{-}$. We need to prove that for any $u \in \mathbb R^{m}$ and $\gamma\in \Gamma$, $f(y +\gamma\circ u) = -1$. From the definition,
	\begin{displaymath}
	  d(y+\gamma\circ u,X^-) \leq d(y+\gamma\circ u, y)= \|\gamma\circ u\|_0\leq n.
	\end{displaymath}
	Since
	\begin{displaymath}
	  d(y+\gamma\circ u,X^+) + d(y+\gamma\circ u,X^-) \geq 2n+1,
	\end{displaymath}
	we know that
	\begin{displaymath}
	  d(y+\gamma\circ u, X^+) \geq n+1.
	\end{displaymath}
	As a result,
	\begin{displaymath}
	  d(y+\gamma\circ u,X^-)< d(y+\gamma\circ u,X^+).
	\end{displaymath}
	Therefore $f(y+\gamma\circ u) = -1$, which implies that $X^- \subseteq Y^-(f)$. Similarly, one can prove that
	\begin{displaymath}
	  X^{+}\subseteq Y^+(f).
	\end{displaymath}
	%\frQED
	\end{proof}

	If $m \leq 2n$, then any two non-empty sets have distance less than $2n+1$. As a result, the optimal detector is $f = 1$, as is proved in our previous work \cite{Yilinacc2012}. Therefore, we will only focus on the case where $m > 2n$. The following corollary casts the design of the optimal detector as an optimization problem:
	\begin{corollary}
	  \label{corollary:optimization}
	  Define function $\alpha',\beta':\mathcal P(\mathbb R^m)\rightarrow [0,1]$ as follows\footnote{$\mathcal P(S)$ is the power set of $S$, i.e., the set of all subsets of $S$.}:
	  \begin{displaymath}
	    \begin{split}
	      \alpha'(X) &\triangleq 1-\sup\{P(y\in S|\theta = -1):S\in \mathcal B(\mathbb R^m),S\subseteq X\},\\
	      \beta'(X) &\triangleq \sup\{P(y\in S|\theta = 1):S\in \mathcal B(\mathbb R^m),S\subseteq X\}.
	    \end{split}
	  \end{displaymath}
	  The optimal $f_*$ takes the following form:
	  \begin{equation}
	  f_*(y) = \begin{cases}
	    1 & d(y, X^-_*)\geq d(y, X^+_*)\\
	    -1 & d(y,X^-_*)< d(y,X^+_*).
	  \end{cases}
	  \end{equation}
	  The optimal sets $X^{+}_*$ and $X^{-}_*$ of $f_*$ are the solutions of the following optimization problem:
	  \begin{align*}
	    &\mathop{\textrm{minimize}}\limits_{X^+,X^-}&
	    & (1-\beta'(X^+))p^++\alpha'(X^-)p^-\\
	    &\textrm{subject to}&
	    &d( X^+,\,X^-)\geq 2n+1.\\
	  \end{align*}
	\end{corollary}
	\begin{IEEEproof}
	  Suppose that $X^{+}_*$ and $X^{-}_*$ are the optimal solutions. By Theorem~\ref{theorem:distance}, we know that
	  \begin{displaymath}
	    \begin{split}
	    P_e(f^*)&=(1-\beta'(Y^{+}(f_*))p^++\alpha'(Y^{-*}(f_*))p^-\\
	   & \leq  (1-\beta'(X^{+*}))p^++\alpha'(X^{-*})p^-.
	    \end{split}
	  \end{displaymath}
	  Now pick an arbitrary $f$. By Theorem~\ref{theorem:distance}, we know that $d(Y^+(f),Y^-(f))\geq 2n+1$. Therefore,
	  \begin{displaymath}
	    \begin{split}
	    P_e(f)&=(1-\beta'(Y^{+}(f))p^++\alpha'(Y^{-*}(f))p^-\\
	   & \geq  (1-\beta'(X^{+*}))p^++\alpha'(X^{-*})p^- \geq P_e(f_*).
	    \end{split}
	  \end{displaymath}
	  
	    which concludes the proof.
	  \end{IEEEproof}
	  The key challenge in applying Theorem~\ref{theorem:distance} and Corollary~\ref{corollary:optimization} is that they do not provide a method to construct the true optimal sets $X^+,\,X^-$ that lead to the optimal $f^*$, potentially requiring one to search for the optimal detector by ranging over all possible pairs of sets that are $2n+1$ away from each other. In general this result does not yield a computationally viable way to compute the optimal detector, excluding some special cases. In the next section, we propose the design of a heuristic detector of form \eqref{eq:ffunction} and prove that our design is asymptotically optimal when $n$ is fixed and $m$ goes to infinity.

	  \section{A Heuristic Detector for General $n$}
	  \label{sec:heuristic}
	  In this section we propose a heuristic detector, where we design the set 
	  \begin{equation}
	    X^- = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i<\eta,\forall |\mathcal I|=m-2n\} ,
	    \label{eq:heuristicxminus}
	  \end{equation}
	  and
	  \begin{equation}
	    X^+ = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i\geq\eta,\forall |\mathcal I|=m-2n\} ,
	    \label{eq:heuristicxplus}
	  \end{equation}
	  where $\eta = r\log(p^-/p^+)/2$ and $\mathcal I\subset \{1,\ldots,m\}$ ranges over all index sets of size $m-2n$. We first show that the corresponding $f$ has a simple structure. Moreover, we prove that such detector is asymptotically optimal when $n$ is fixed and $m\rightarrow \infty$.
	  
	  First let us prove that our choice of $X^-$ and $X^+$ satisfies the distance constraints, which is given by the following lemma:
	  \begin{lemma}
	    The $X^-$ and $X^+$ defined in \eqref{eq:heuristicxminus}, \eqref{eq:heuristicxplus} satisfies the following inequality:
	    \begin{displaymath}
	      d(X^-,X^+) \geq 2n+1.
	    \end{displaymath}
	  \end{lemma}
	  \begin{proof}
	    We will prove the lemma by contradiction. Suppose there exist $y^-\in X^-$ and $y^+\in X^+$, such that
	  \begin{displaymath}
	    d(y^-,y^+) \leq 2n.
	  \end{displaymath}
	  Therefore, at least $m-2n$ entries of $y^-$ and $y^+$ are the same. Without loss of generality, we assume the first $m-2n$ entries are the same, which means we can write $y^-$ and $y^+$ as
	\[
	y^- = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^-,\ldots,y_{m}^-]'.
	\]
	and
	\[
	y^+ = [y_{1},\ldots,y_{m-2n},y_{m-2n+1}^+,\ldots,y_{m}^+]',
	\]
	Now let $\mathcal I= \{1,\ldots,m-2n\}$. Since $y^-\in X^-$, 
	\begin{displaymath}
	  \sum_{i=1}^{m-2n} y_i < \eta. 
	\end{displaymath}
	Similarly, since $y^+\in X^+$, we have
	\begin{displaymath}
	  \sum_{i=1}^{m-2n} y_i \geq \eta,
	\end{displaymath}
	which is a contradiction. Therefore, $d(X^-,X^+)\geq 2n+1$.
      \end{proof}
      Now we want to prove that the heuristic detector based on $X^-$ and $X^+$ defined in \eqref{eq:heuristicxminus}, \eqref{eq:heuristicxplus} takes a simple form. To this end, let us arrange $y_i$s as $y_{i_1},\ldots,y_{i_m}$, such that
	  \begin{displaymath}
	    y_{i_1}\geq  y_{i_2}\geq\ldots\geq y_{i_m}.
	  \end{displaymath}
	  Also define the index set
	  \begin{displaymath}
	    \mathcal I_k(y) = \{i_k,\ldots,i_{k+m-2n-1}\},\,k=1,\ldots,2n+1.
	  \end{displaymath}
	  It is easy to check that $|\mathcal I_k(y)| = m-2n$. Now let us define function $h_k(y)$ as
	  \begin{align}
	    h_k(y) \triangleq \begin{cases}-1&\sum_{i\in \mathcal I_k(y)} y_i<\eta\\
	      1&\sum_{i\in \mathcal I_k(y)} y_i\geq \eta.\end{cases}
	    \end{align}
	    The following theorem claims that the heuristic detector has the following form:
	    \begin{theorem}
	      Consider a heuristic detector $f_0$, such that 
	      \begin{align}\label{eq:fheuristic}
		f_0(y) = \begin{cases}
		  1 & d(y,X^-)\ge d(y,X^+)\\
		  -1 & d(y,X^-)< d(y,X^+),
		\end{cases}
	      \end{align}
	      with
	      \begin{displaymath}
		X^- = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i<\eta,\forall |\mathcal I|=m-2n\} ,
	      \end{displaymath}
	      and
	      \begin{displaymath}
		X^+ = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i\geq\eta,\forall |\mathcal I|=m-2n\} .
	      \end{displaymath}
	      The heuristic detector can be computed as
	      \begin{displaymath}
		f_0(y) = h_{n+1}(y). 
	      \end{displaymath}
	      \label{theorem:heuristicdetector}
	    \end{theorem}
	    The proof of Theorem~\ref{theorem:heuristicdetector} is quite technical and hence is reported in the appendix for the sake of legibility. We would like to remark that the function $\sum_{i\in\mathcal I_{n+1}(y)}y_i$ is the truncated sum of measurements since it is the sum of $m-2n$ measurements whose values are in the middle. As a result, $h_{n+1}(y)$ can be computed very efficiently by the following procedures:
	    \begin{itemize}
	      \item The detector sorts all measurements $y_i$ in descending order.
	      \item The detector throws away $n$ largest measurements and $n$ smallest measurements.
	      \item The detector sums the remaining $m-2n$ $y_i$s and compares it to $\eta$. The detector chooses $\hat \theta = -1$ if the truncated sum is less than $\eta$, otherwise the detector chooses $\hat \theta = 1$.
	    \end{itemize}
	    The complexity of such detector is $O(mlog(m))$, which is the complexity for sorting the likelihood ratio. 
	    \subsection{Asymptotic Optimality}

	    In this subsection, we prove that the heuristic detector $f_0$ is asymptotically optimal. Let us define $\alpha_{m,n}$ and $\beta_{m,n}$ as the probability of false alarm and detection of detector $f_0$ with $m$ sensors and $n<m/2$ corrupted measurements.

	    Moreover we define
	    \begin{displaymath}
	      P(m,n) \triangleq p^+(1-\beta_{m,n})+p^-\alpha_{m,n},
	    \end{displaymath}
	    and $P^*(m,n)$ as the probability error of the optimal detector with $m$ sensors and $n$ corrupted measurements. Since when $n=0$, i.e. no measurement is corrupted, $f_0$ is the optimal detector by the Bayes rule, we have the following lemma:
	    \begin{lemma}
	      $P(m,n)\geq P^*(m,n)\geq P^*(m,0)= P(m,0)$.
	      \label{lemma:asymptoticoptimal}
	    \end{lemma}

	    Let us define the rate function as
	    \begin{displaymath}
	      \overline I_n \triangleq \limsup_{m\rightarrow\infty}\frac{-\log(P(m,n))}{m},\,\, \underline I_n \triangleq \liminf_{m\rightarrow\infty}\frac{-\log(P(m,n))}{m}.
	    \end{displaymath}
	    Moreover let us define $I_n \triangleq \overline I_n$ when $\overline I_n = \underline I_n$. Similarly one can define the rate function of the optimal detector. 
	    \begin{remark}
	      If $I_n$ exists, then from definition
	      \begin{displaymath}
		e^{(-I_n-\delta)m} \leq P(m,n)\leq e^{(-I_n+\delta)m},
	      \end{displaymath}
	      for arbitrary small $\delta$ and sufficiently large $m$. As a result, $P(m,n)$ converges to $0$ as ``fast'' as the exponential function $e^{-I_nm}$. Therefore, larger $I_n$ indicates better asymptotic performance. Moreover, if $I_n = I_n^*$, then the probability of error for the heuristic detector converges to $0$ as ``fast'' as that of the optimal detector. 
	    \end{remark}
	    It is well known that $I_0$ exists, which is formalized by the following lemma~\cite{chernoff1952}:
	    \begin{lemma}[Chernoff Lemma]
	      Let $\Lambda(y) = 2y/r$ be the log-likelihood ratio of $y_i$s. The optimal decay rate for $n = 0$ is given by
	      \begin{displaymath}
		I_0 = \inf_{0< t< 1}\log\left[E\left(e^{t\Lambda(y)}|\theta = -1\right)\right] = (2r)^{-1}.
	      \end{displaymath}
	    \end{lemma}
	    The following theorem proves that the heuristic detector decays as ``fast'' as the optimal detector:
	    \begin{theorem}
	      $I_n$ and $I_n^*$ exist. Moreover, the following equality holds:
	      \begin{equation}
		I_n = I^*_n =  I_0.
		\label{eq:sameconvergencerate}
	      \end{equation}
	      \label{theorem:asymptoticoptimal}
	    \end{theorem}
	    \begin{IEEEproof}
	      Due to Lemma~\ref{lemma:asymptoticoptimal}, we know that $\overline I_n\leq \overline I_n^*\leq I_0$. As a result, we only need to prove that $\underline I_n \geq I_0$. From the definition of $\alpha_{m,n}$ and the fact that $X^-\subseteq Y^-(f_0)$\footnote{We use the fact that $X^-$ and $X^+$ defined in \eqref{eq:heuristicxminus},\eqref{eq:heuristicxplus} are measurable.},
	      \begin{displaymath}
		\alpha_{m,n} \leq P(y\in \mathbb R^m\backslash X^-|\theta=-1).
	      \end{displaymath}
	      Now by the definition of $X^-$, we know that
	      \begin{displaymath}
		\begin{split}
		  &P(y\in\mathbb R^m\backslash X^-|\theta = -1) \\
		  &=  P(\bigcup_{|\mathcal I| = m-2n}\{\sum_{i\in \mathcal I}y_i\geq \eta\}|\theta = -1)\\
		  &\leq  \sum_{|\mathcal I|=m-2n} P(\sum_{i\in \mathcal I}y_i\geq \eta|\theta = -1).
		\end{split}
	      \end{displaymath}
	      Since $y_i$s are i.i.d. distributed, we know that
	      \begin{displaymath}
		\begin{split}
		  \alpha_{m,n}&\leq\left( {\begin{array}{*{20}c}
		    m  \\
		    m-2n
		  \end{array}} \right) P(\sum_{i=1}^{m-2n}y_i\geq \eta|\theta = -1) \\
		  &\leq m^{2n}\alpha_{m-2n,0}
		\end{split}
	      \end{displaymath}
	      Similarly, one can prove that
	      \begin{displaymath}
		1-\beta_{m,n}\leq m^{2n}(1-\beta_{m-2n,0}).
	      \end{displaymath}
	      Therefore,
	      \begin{displaymath}
		P(m,n) = p^-\alpha_{m,n} + p^+(1-\beta_{m,n})\leq m^{2n}P(m-2n,0),
	      \end{displaymath}
	      which implies that
	      \begin{displaymath}
		\begin{split}
		  \underline I_n &= \liminf_{m\rightarrow \infty}\frac{-\log(P(m,n))}{m}\\
		  &\geq -\limsup_{m\rightarrow\infty} \frac{2n\log(m)}{m} +  \liminf_{m\rightarrow\infty} \frac{-\log(P(m-2n,0))}{m}\\
		  &= 0 + \liminf_{m\rightarrow\infty} \frac{-\log(P(m-2n,0))}{m-2n}\times \frac{m-2n}{m} = I_0.
		\end{split}
	      \end{displaymath}
	      As a result, $I_n = I_n^* = I_0$.
	    \end{IEEEproof}
	    \begin{remark}
	      Theorem~\ref{theorem:asymptoticoptimal} claims that the heuristic detector $f_0$ achieves the same convergence rate as the optimal detector $f^*$. As a result, if $m\gg n$, which means a small percentage of the measurements are corrupted, then the heuristic detector is a good approximation of the optimal detector and should be used due to its low computational complexity.
	    \end{remark}

	    \section{Numerical Examples}
	    \label{sec:example}

	    In this section, we provide a numerical example to illustrate the performance of our heuristic detector. We assume that $r = 1$ and $p^+ = p^- = 0.5$. Figure~\ref{fig:heuristicdetector} shows the probability of error $P(m,n)$ versus $m$ for the heuristic detector proposed in Section~\ref{sec:heuristic} and a constant detector $f = 1$. The $P(m,n)$ is computed as the empirical probability by averaging $10^8$ random experiments. It can be seen that when $n$ is close to $m/2$, the heuristic detector is worst than constant detector, which shows that the heuristic detector is not necessarily optimal. However, as $m$ goes to infinity. $P(m,n)$ decays as ``fast'' as $P(m,0)$, which illustrates that the heuristic detector is asymptotically optimal and thus is preferable when $m \gg n$.
  \begin{figure}[<+htpb+>]
	      \begin{center}
		\includegraphics[width=0.45\textwidth]{figure4.eps}
	      \end{center}
	      \caption{Probability of Error v.s. Number of Sensors($m$).  The green solid line corresponds to the probability of error of the heuristic detector $f_0$ when $n = 0$. The red dashed line corresponds to $P_e(f_0)$ when $n = 1$. The black dotted line corresponds to $P_e(f_0)$ when $n = 2$. The blue dash-dotted line corresponds to $P_e(f_0)$ when $n =3$. The dashed horizontal line corresponds to the probability of error of the constant detector $f = 1$.}
	      \label{fig:heuristicdetector}
	    \end{figure}
	  
	    \section{Conclusion}
	    \label{sec:conclusion}
	    In this paper we consider the problem of designing detectors to minimize the probability of error with $n$ corrupted measurements due to integrity attacks on a subset of the sensor pool. The problem is posed as a minimax optimization where the goal is to design the optimal detector against all possible attacker's strategies. When the attacker can manipulate less than of half of the measurements ($n<m/2$), we show that the optimal detector is a threshold rule based on a Hamming-like distance between the manipulated measurement vector and two appropriately defined sets. Based on this result, we design a heuristic detector, which is asymptotically optimal when $m\rightarrow\infty$, for general $m>2n$. 

	    \section{Appendix:Proof of Theorem~\ref{theorem:heuristicdetector}}
	    \label{sec:appendix}

	    Before proving Theorem~\ref{theorem:heuristicdetector}, let us define
	    \begin{displaymath}
	      n^-(y) \triangleq\sum_{k=1}^{2n+1}\mathbb I_{h_k(y) = 1}, n^+(y) \triangleq\sum_{k=1}^{2n+1}\mathbb I_{h_k(y) =-1},
	    \end{displaymath}
	    where $\mathbb I$ is the indicator function. We have the following lemma:
	    \begin{lemma}
	      $n^-(y) + n^+(y) = 2n+1$, $n^-(y) = d(y,X^-)$, $n^+(y) = d(y,X^+)$.
	      \label{lemma:distance}
	    \end{lemma}
	    \begin{IEEEproof}
	      Without loss of generality, let us assume that
	      \begin{displaymath}
		y_{1}\geq  y_{2}\geq\ldots\geq y_{m}.
	      \end{displaymath}
	      It is trivial to prove that $n^-(y)+n^+(y) = 2n+1$. To prove $n^-(y) = d(y,X^-)$, we first show that $n^-(y) \geq d^-(y,X^-)$. Let 
	      \[
	      M = (m-2n-1)\max_i |y_i|+\eta.
	      \]
	      Consider another vector $y^-\in\mathbb R^m$. If $i\leq n^-(y)$, then we choose $y_i^-$ such that 
	      \[
	      y_i^-< -M,\,i=1,\ldots,n^-(y),
	      \] 
	      and
	      \[
	      y_i^-\leq y_{i-1}^- ,\,i=2,\ldots,n^-(y).
	      \] 
	       If $i > n^-(y)$, we choose $y_i^- = y_i$. It is easy to see that $d(y^-,y) = n^-(y)$. As a result, we only need to prove that $  y^- \in X^-$. From the construction of $y^-$, 
	      \begin{equation}
		\begin{split}
		  y^-_{n^-(y)+1}&\geq \ldots\geq  y^-_{m}\\
		  &\geq  y^-_{1}\geq\ldots y^-_{n^-(y)}.
		\end{split}
		\label{eq:descend}
	      \end{equation}
	      Since 
	      \begin{displaymath}
		X^- = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i<\eta,\forall |\mathcal I|=m-2n\} .
	      \end{displaymath}
	      We know that $y^-\in X^-$ if the sum of the largest $m-2n$ terms in \eqref{eq:descend} is less than $\eta$. First suppose that $n^-(y)<2n+1$, which implies that $n^-(y) + m-2n\leq m$. Therefore
	      \begin{displaymath}
		\sum_{i=1}^{m-2n} y^-_{n^-(y)+i} =\sum_{i=1}^{m-2n} y_{n^-(y)+i} .
	      \end{displaymath}
	      From the definition of $n^-(y)$ and monotonicity of $h_k(y)$\footnote{$h_k(y)$ is monotonically deceasing.}, we know that $h_1(y),\ldots,h_{n^-(y)}(y) = 1$ and $h_{n^-(y)+1},\ldots,h_{2n+1}(y) = -1$. Since,
	      \begin{displaymath}
		h_{n^-(y)+1}(y) = \begin{cases}-1&\sum_{i=1}^{m-2n} y^-_{n^-(y)+i}<\eta\\
		  1&\sum_{i=1}^{m-2n} y^-_{n^-(y)+i}\geq \eta.\end{cases}
		\end{displaymath}
		we know that
		\begin{displaymath}
		  \sum_{i=1}^{m-2n} y^-_{n^-(y)+i} -\eta<0,
		\end{displaymath}
		which implies that $y^-\in X^-$. Now suppose that $n^-(y) = 2n+1$. It can be proved that
		\begin{displaymath}
		  \begin{split}
		    \sum_{i=m-2n+2}^m y_i^-&+y_1^-=\sum_{i=m-2n+2}^m y_i+y_1^-\\
		    &< (m-2n-1)\max_i|y_i| - M \leq \eta,
		  \end{split}
		\end{displaymath}
		which implies that $y^-\in X^-$. Hence, $n^-(y)\geq d(y,X^-)$. Similarly one can prove that $n^+(y)\geq d(y,X^+)$. By Theorem~\ref{theorem:distance}, we have
		\begin{displaymath}
		  \begin{split}
		    n^-(y) &+ n^+(y) = 2n+1 \leq d(y,X^-) + d(y,X^+),\\
		    n^-(y) &\geq d(y,X^-),\,n^+(y) \geq d(y,X^-).
		  \end{split}
		\end{displaymath}
		Therefore, $n^-(y) = d(y,X^-)$, $n^+(y) = d(y,X^+)$.
	      \end{IEEEproof}
	      Now we are ready to prove Theorem~\ref{theorem:heuristicdetector}:
	      \begin{IEEEproof}[Proof of Theorem~\ref{theorem:heuristicdetector}]
		By Lemma~\ref{lemma:distance},
		\begin{displaymath}
		  f_0(y) = \begin{cases}
		    1 & n^-(y)\ge n^+(y)\\
		    -1 & n^-(y)< n^+(y).
		  \end{cases}
		\end{displaymath}
		Since $n^-(y) + n^+(y) = 2n+1$, we know that
		\begin{displaymath}
		  f_0(y) = \begin{cases}
		    1 & n^-(y)\ge n+1\\
		    -1 & n^-(y)< n.
		  \end{cases}
		\end{displaymath}
		Due to monotonicity of $h_k(y)$, $n^-(y) \geq n+1$ if and only if $h_{n+1}(y)= 1$. Therefore
		\begin{displaymath}
		  f_0(y) = h_{n+1}(y),
		\end{displaymath}
		which concludes the proof.
	      \end{IEEEproof}
	      \bibliographystyle{IEEEtran}
	      \bibliography{IEEEfull,securedetection}
	      \end{document}

