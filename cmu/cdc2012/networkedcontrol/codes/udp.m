clc
clear
% generate all periodic schedule of period 10
gamma = zeros(1024,10);
for i = 0:2^10-1
    tmp = i;
    for j = 1:10
        gamma(i+1,j) = mod(tmp,2);
        tmp = (tmp - mod(tmp,2))/2;
    end
end
%evaluate the cost
optimalcost = zeros(1,10)+inf;
optimalgamma = zeros(10,10);
for i = 1:2^10
    numofcomm = sum(gamma(i,:));
    if numofcomm >=6 && numofcomm <= 9 
        cost = udpeva(gamma(i,:));
        if cost <= optimalcost(numofcomm)
            optimalcost(numofcomm) = cost;
            optimalgamma(numofcomm,:) = gamma(i,:);
        end
    end
end

plot([0.6:0.1:0.9],optimalcost(6:9),'*')
axis([0.5 1 0 9])
set(gca,'XTick',[0.5:0.1:1])
set(gca,'YTick',[0:2:10])
