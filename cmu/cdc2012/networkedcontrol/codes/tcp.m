clc
clear
%compute f(n)
P = 1/sqrt(2);
f(1) = P;
for i = 1:100
    f(i+1) = f(i)*2+1;
end

index = 1;
cost = 0;
for rstar = 0.2:0.01:0.9
    %compute alpha
    n0 = floor((1-rstar)/(0.75*rstar)) + 1;
    q = (1-0.75*n0*rstar)*0.75;
    cost(index) = 0;
    for i = 1:101
        if i <= n0
            cost(index) = cost(index) + rstar*0.75*f(i);
        else
            cost(index) = cost(index) + q*0.25^(i-n0-1)*f(i);
        end
    end
    index = index + 1;
end
plot(0.2:0.01:0.9,cost)
axis([0.15 1 0 35])
set(gca,'XTick',[0.2:0.1:1])
set(gca,'YTick',[0:10:35])