function J = udpeva(gamma)

period = size(gamma,2);
P = 1/sqrt(2);
tildeP = P;
traceperiod = zeros(1,period);
for i = 1:100
    for j = 1:period
        tildeP = 2*tildeP+1;
        if gamma(j) == 1
            tildeP = 0.25*tildeP+0.75*P;
        end
        traceperiod(j) = trace(tildeP);
    end
end
J = mean(traceperiod);
end
