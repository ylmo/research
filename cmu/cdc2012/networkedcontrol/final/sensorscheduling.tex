\documentclass{ieeeconf}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\newlength\figureheight
\newlength\figurewidth

\title{\LARGE \bf {Infinite-Horizon Sensor Scheduling for Estimation over Lossy Networks}}
\author{Yilin Mo$^*$, Bruno Sinopoli$^*$, Ling Shi$^\dag$, Emanuele Garone$^\ddag$
\thanks{$^*$: Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu, brunos@ece.cmu.edu}
}
\thanks{$\dag$: Electronic and Computer Engineering, Hong Kong University of Science and Technology. Email: eesling@ust.hk}
\thanks{$^\ddag$:  Service d'Automatique et d'Analyse des Syst\`{e}mes  Universit\`{e} Libre de Bruxelles, 50 Av. F.D. Roosevelt, B-1050 Brussels, Belgium. Email: {egarone@ulb.ac.be}}
\thanks{The work by Y.~Mo and B.~Sinopoli is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation.}
\thanks{The work by L.~Shi is supported by a HK RGC CRF grant HKUST11/CRF/10 and a HK RGC GRF grant GRF618612.}
\thanks{The work by E.~Garone is supported by the Belgian Network DYSCO (Dynamical Systems, Control and Optimization) funded by the Interuniversity Attraction Poles.}
}

\begin{document}
\maketitle
\begin{abstract}
In this paper, we consider the problem of infinite-horizon sensor scheduling for estimation of a discrete-time linear Gaussian system, where the sensor information may be dropped during communication over a memoryless erasure channel, modeled as an independent Bernoulli process. We assume that due to energy or bandwidth constraints, the sensor can only maintain an average communication rate of $r$. The optimal communication schedule is thus chosen by the sensor to minimize the average estimation error of the receiver. We prove necessary and sufficient conditions for the feasibility of the optimization problem for two classes of protocols, one where deterministic acknowledgments are transmitted from the central location back to the sensor (ACK) and and the other where the acknowledgement structure is absent (No-ACK). We prove that for the No-ACK case, the optimal schedule can be approximated by a periodic schedule with arbitrarily close performance. For the ACK case, we provide the explicit optimal sensor schedule. A numerical example is provided to illustrate the effectiveness of the proposed strategy.
\end{abstract}
\section{Introduction}

Advances in fabrication, modern sensor and communication technologies, and computer architecture have boosted the development of sensor networks which have a wide range of applications, including environment and habitat monitoring, health care, home and office automation, and traffic control~\cite{wireless_sensor_network}.  In many of these applications, there is an economic incentive towards using off-the-shelf sensors which are typically battery powered. As a consequence, the individual hardware components might be of relatively low quality and the communication resources are quite limited. Therefore, sensors may not be able to communicate with the remote fusion center at each time step. Hence, one of the key design problems is to determine a schedule of sensors to balance the consumption of communication or energy resources and the estimation quality.

Sensor network energy consumption minimization and, consequently, lifetime maximization problems have been active areas of research over the past few years, as researchers realized that energy limitations constitute one of the major obstacles to the extensive adoption of such a technology. Sensor networks energy minimization is typically accomplished via efficient MAC protocols~\cite{mac_sensor_network} or via efficient scheduling of sensor states~\cite{wake_up_scheme}. In \cite{lifetime_analysis}, Xue and Ganz showed that the lifetime of sensor networks is influenced by transmission schemes, network density and transceiver parameters with different constraints on network mobility, position awareness and maximum transmission ranges.  Chamam and Pierre~\cite{optimal_state_scheduling} proposed a sensor scheduling scheme capable of optimally putting sensors in active or inactive modes. Shi et al.~\cite{Ling_cdc07} considered sensor energy minimization as a mean to maximize the network lifetime while guaranteeing a desired quality of the estimation accuracy. %Moreover in~\cite{shi-febid08}, they proposed a sensor tree scheduling algorithm which leads to longer network lifetimes.

Conversely, optimizing the performance of sensor networks under given energy constraints, which can be seen as the dual problem of network energy minimization, has also been studied by several researchers. Such a constrained optimization problem has been studied for continuous-time linear systems in \cite{stochasticcontrol} and \cite{sensorprecision}. In \cite{hmmsensorselection}, the author computed the optimal sensor scheduling for the estimation of a Hidden Markov Model based system. For discrete-time linear systems, methods like dynamic programming \cite{dynmaicprogrammingsensorselection} or greedy algorithms \cite{oshmansensor} have been proposed to find the optimal sensor scheduling over long time horizons. Another important contribution on the topic is the work of Joshi and Boyd \cite{Joshi}, where a general single-step sensor selection problem was formulated and solved by means of convex relaxation techniques. %Such a paper provides a very general framework that can handle various performance criteria and energy and topology constraints. 
Following this work, Mo et al. \cite{Mo2011} showed that multi-step sensor selection problems can also be relaxed into convex optimization problems and thus efficiently solved.

For lossy networks, Nair et al. \cite{Nair2010} consider the problem of transmitting multiple redundant packets to compensate for packet drop. However, they are only able to find a suboptimal policy when the channel state is known to the sensor.

In this paper, we consider the problem of scheduling a smart sensor, to send local estimates instead of raw measurements, over a memoryless erasure communication channel. The goal of the sensor is to minimize the average estimation error of the remote fusion center over an infinite horizon while satisfying average communication rate constraints. We consider both No-ACK and ACK protocols% and prove necessary and sufficient conditions for the feasibility of sensor scheduling problem, i.e., there exists a schedule with finite average estimation error. We further prove that for the No-ACK case, the optimal sensor schedule can be arbitrarily approximated by periodic schedules. For the ACK case, we are able to provide the exact optimal schedule. 
The contribution of the paper is threefold:
\begin{enumerate}
  \item We provide necessary and sufficient conditions for the existence of a feasible schedule for both ACK and No-ACK cases.
  \item We are able to prove the exact optimal schedule for ACK case.
  \item We prove that the optimal schedule for No-ACK case can be arbitrarily approximated by periodic schedules, which effectively reduces the search space from all possible schedules to periodic schedules.
\end{enumerate}

The rest of the paper is organized as follows: In Section~\ref{sec:problem}, we formulate the infinite-horizon sensor scheduling problem. In Section~\ref{sec:No-ACK} and \ref{sec:ACK}, we prove necessary and sufficient conditions on the feasibility of the sensor scheduling problem for No-ACK and ACK protocols respectively. Moreover, in Section~\ref{sec:No-ACK} we prove that for No-ACK protocols the optimal schedule can be arbitrarily approximated by periodic schedules. In Section~\ref{sec:ACK}, we provide an explicit solution to the optimization problem for ACK protocols. A numerical example is provided in Section~\ref{sec:simulation} to illustrate the effectiveness of the proposed strategy. Finally, Section~\ref{sec:conclusion} concludes the paper.

\section{Problem Formulation}
\label{sec:problem}
Consider the following linear system:
\begin{equation}
  \begin{split}
    x_{k+1}& = Ax_k + w_k,\\
  y_k& = Cx_k+v_k,
  \end{split}
  \label{eq:systemeq}
\end{equation}
where $x_k \in \mathbb R^{n}$ is the state vector, $y_k\in \mathbb R^m$ is the observation vector. $w_k$ and $v_k$ are i.i.d. white Gaussian random variables with zero mean and covariance $Q > 0,\,R > 0$ respectively\footnote{All the comparison between symmetric matrices in this paper are in the positive semidefinite sense.}. We assume that the initial state $x_0 \in \mathbb R^n$ is also Gaussian with zero mean and covariance $\Sigma > 0$. We further assume that the system is observable.

Suppose that a smart sensor is deployed to observe the system. At each time step, the sensor computes the optimal estimate of $x_k$ as
\begin{displaymath}
 \hat x_k = E(x_k|y_1,\ldots,y_k). 
\end{displaymath}
The corresponding estimation error covariance matrix is defined as
\begin{displaymath}
  P_k \triangleq Cov(x_k - \hat x_k).
\end{displaymath}
It is well known that $\hat x_k$ and $P_k$ can be computed recursively by means of a Kalman filter, which takes the following form:
\begin{align*}
  \hat x_{k}  &= \hat x_{k|k - 1}  +  K_k (y_k  - C\hat  x_{k|k - 1} ) ,\\
  P_{k}  &= P_{k|k - 1}  -  K_k CP_{k|k - 1}  ,
\end{align*}
where
\begin{align*}
  K_k  &= P_{k|k - 1} C' (CP_{k|k - 1} C'  + R)^{ - 1}  ,\\
  \hat x_{k + 1|k}  &= A\hat x _{k}  , \; P_{k + 1|k}  = AP_{k} A'  + Q ,\; \hat x_{0}  = 0 ,\; P_{0}  = \Sigma.
\end{align*}
To simplify notations, let us define 
\begin{equation}
  h(X) \triangleq AXA' + Q,\; g(X) \triangleq (h^{-1}(X) + C'R^{-1}C)^{-1}.
  \label{eq:hfunction}
\end{equation}
One can prove that both $h(X)$ and $g(X)$ are monotonically increasing with respect to $X$ and
\begin{displaymath}
  P_{k+1} = g(P_k). 
\end{displaymath}

Moreover if the system is observable and $(A,Q^{1/2})$ is controllable, then the estimation error covariance $P_k$ of the Kalman filter converges. We denote with $P$ the limit of $P_k$, i.e.,
\begin{equation}
  P \triangleq \lim_{k\rightarrow \infty} P_k.
\end{equation}

At each time step $k$, the sensor can choose to send $\hat x_k$ to the fusion center or not. Let us define binary variables $\gamma_k$s such that $\gamma_k = 1$ if the sensor sends $\hat x_k$ to the fusion center and $\gamma_k = 0$ if otherwise. For simplicity, we define $\gamma = (\gamma_1,\gamma_2,\ldots)$ as the infinite sequence of the sensor schedule. 

We also assume that the communication channel between the sensor and the fusion center is a memoryless erasure channel. Let us define independent Bernoulli random variables $\eta_k$s such that $\eta_k = 1$ if the channel is in good state at time $k$ and $\eta_k = 0$ if otherwise. Hence, the fusion center can only receive $\hat x_k$ when both $\eta_k = 1$ and $\gamma_k = 1$. We further assume that
\begin{displaymath}
 P(\eta_k = 1) \triangleq p \in (0,1). 
\end{displaymath}

Since the fusion center can only receive $\hat x_k$ when both $\gamma_k = 1$ and $\eta_k = 1$, the information set of the fusion center can be defined as
\begin{displaymath}
  \mathcal I_k \triangleq \{\gamma_1\eta_1,\,\ldots,\,\gamma_k\eta_k,\, \gamma_1\eta_1\hat x_1,\,\ldots,\,\gamma_k\eta_k\hat x_k\}.
\end{displaymath}
Let us define the state estimation and estimation covariance for the fusion center at time $k$ as
\begin{displaymath}
  \tilde x_k \triangleq E(x_k | \mathcal I_k),\,\tilde P_k \triangleq Cov(x_k-\tilde x_k).
\end{displaymath}
It is easy to prove that since the smart sensor sends the local estimations instead of raw measurements, $\tilde x_k$ and $\tilde P_k$ follows the following recursive equations:
\begin{align}
  \label{eq:stateestimationupdate}
  \tilde x_{k} &= \begin{cases}
    A\tilde x_{k-1}, & \textrm{when }\gamma_k\eta_k = 0,\\
    \hat x_k, & \textrm{when }\gamma_k\eta_k = 1.
  \end{cases}\\
  \label{eq:estimationcovarianceupdate}
  \tilde P_k & =  \begin{cases}
     A\tilde P_{k-1}A' + Q,& \textrm{when }\gamma_k\eta_k = 0,\\
    P_k, & \textrm{when }\gamma_k\eta_k = 1.
  \end{cases}
\end{align}
with the initial conditions:
\begin{equation}
  \tilde x_{-1} = 0,\,\tilde P_0 = \Sigma.
\end{equation}

Suppose that due to energy or bandwidth constraints, the smart sensor cannot always communicate with the fusion center. Let us define the average communication rate as
\begin{displaymath}
  r(\gamma) \triangleq  \limsup_{N\rightarrow\infty} \frac{1}{N}\sum_{k=1}^{N}\gamma_k.
\end{displaymath}
We will assume that the sensor schedule needs to satisfy the pre-designed average communication rate constraints, i.e., $r(\gamma) \leq r_*\in (0,1)$, where $r_*$ is a design variable. Let us also define the average estimation error as
\begin{equation}
  J(\gamma,\Sigma) = \limsup_{N\rightarrow \infty}\frac{1}{N}\sum_{k=1}^N trace(E\tilde P_k).
  \label{eq:costfunction}
\end{equation}

We first consider No-ACK protocols, where the fusion center does not send acknowledgement back to the sensor. Therefore, the sensor does not know whether $\hat x_k$ arrives or not. The goal of the smart sensor is to find an infinite schedule $(\gamma_1,\gamma_2,\ldots)$ which solves the following optimization problem:
\begin{align}\label{eq:optprob}
  \min_{\gamma}&&
  & J(\gamma,\Sigma)\nonumber\\
  s.t.&&
  &r(\gamma)\leq r_*,\; J(\gamma,\Sigma)<\infty.
\end{align}
We will call a schedule $\gamma$ to be feasible if it satisfies the constraints in \eqref{eq:optprob}.
\section{Sensor Scheduling for No-ACK Protocols}

\label{sec:No-ACK}
In this section, we discuss the optimal sensor scheduling for No-ACK protocols, where the fusion center does not send acknowledgement back to the smart sensor. We will first prove necessary and sufficient conditions for feasibility of \eqref{eq:optprob}.  Next we prove that we can approximate any feasible schedule $\gamma$ by periodic schedules, which effectively reduces the search space of the optimal schedule.

\subsection{Feasibility of \eqref{eq:optprob} for No-ACK Protocols}
In this subsection, we provide necessary and sufficient conditions for the feasibility of \eqref{eq:optprob}. We assume that the matrix $A$ is unstable throughout this subsection. Otherwise for a stable $A$ matrix, $\gamma_k = 0$ is a trivial feasible solution. Let us first consider $E\tilde P_k$. By taking the expectation on both sides of \eqref{eq:estimationcovarianceupdate}, we get
\begin{equation}
  E\tilde P_k = \begin{cases}
    h(E\tilde P_{k-1})& \textrm{when }\gamma_k = 0,\\
    (1-p)h(E\tilde P_{k-1}) + pP_k&\textrm{when }\gamma_k = 1.
  \end{cases}
  \label{eq:expectationrecursive}
\end{equation}
%We can also write \eqref{eq:expectationrecursive} in a compact form as
%\begin{equation}
%  \begin{split}
%  E\tilde P_k& = (1-\gamma_k)h(E\tilde P_{k-1}) + \gamma_k(1-p)h(E\tilde P_{k-1}) + \gamma_k p P_k\\
% & =(1-\gamma_k p)h(E\tilde P_{k-1}) + \gamma_k p P_k.
%  \label{eq:expectationrecursive2}
%  \end{split}
%\end{equation}

First we prove the necessary condition for the existence of a feasible schedule.
\begin{theorem}
  There exists a feasible schedule for the No-ACK protocols only if
  \begin{displaymath}
    r_* \geq -\frac{2\log(\rho(A))}{\log(1-p)},
  \end{displaymath}
  \label{theorem:No-ACKnecessary}
  where $\rho(A)$ is the spectral radius of $A$.
\end{theorem}
\begin{proof}
We will prove the theorem by contradiction. Suppose that $\gamma$ is feasible and 
\begin{equation}
    r(\gamma) \leq r_* \leq -\frac{2\log(\rho(A))}{\log(1-p)}-2\delta,
    \label{eq:gap}
\end{equation}
where $\delta > 0$ is a constant.  Define $\lambda_1(A)$ as the eigenvalue of $A$, which has the largest absolute value. Therefore, $|\lambda_1(A)| = \rho(A)$. Define $v$ as the corresponding eigenvector. Since we assume that $\Sigma > 0 $ is positive definite, we can find a constant $\varphi>0$, such that
\begin{displaymath}
  \tilde P_{0} = \Sigma \geq \varphi vv^*, 
\end{displaymath}
where $v^*$ indicates the Hermitian transpose of $v$. Hence,
\begin{displaymath}
  h(vv^*) \geq (Av)(Av^*)' = \lambda_1(A)\lambda_1(A)^*vv^* = \rho(A)^2vv^*.
\end{displaymath}

Now by induction, we can prove that
\begin{equation}
  \begin{split}
  E\tilde P_k &\geq \left\{\prod_{i=1}^k \rho(A)^{2(1-\gamma_i)}[(1-p)\rho^2(A)]^{\gamma_i}\right\}\varphi vv^*\\
  & =\varphi \left[\rho^2(A)(1-p)^{\frac{\sum_{i=1}^k\gamma_i}{k}}\right]^{k} vv^*
  \end{split}
\end{equation}
By \eqref{eq:gap}, we know that there exists $N$, such that for all $k \geq N$,
\begin{displaymath}
  \frac{1}{k}\sum_{i=1}^k \gamma_i \leq -\frac{2\log(\rho(A))}{\log(1-p)}-\delta,
\end{displaymath}
which implies that
\begin{displaymath}
  (1-p)^{\frac{\sum_{i=1}^k\gamma_i}{k}}\geq \frac{1}{\rho(A)^{2}(1-p)^\delta}.
\end{displaymath}
Therefore, for all $k \geq N$, we have
\begin{displaymath}
  E\tilde P_k \geq \varphi (1-p)^{-k\delta} vv^*.
\end{displaymath}
Thus $E\tilde P_k$ goes to infinity as $k\rightarrow \infty$, which contradicts with the fact that $J(\gamma,\Sigma) < \infty$.
\end{proof}

Next we prove the sufficient condition for the existence of a feasible solution.
\begin{theorem}
  There exists a feasible schedule for the No-ACK protocols if
  \begin{equation}
    r_* > -\frac{2\log(\rho(A))}{\log(1-p)}.
    \label{eq:noacksufficient}
  \end{equation}
  \label{theorem:No-ACKsufficient}
\end{theorem}
\begin{proof}
  We provide a constructive proof. Let us consider the following schedule $\gamma$, where
  \begin{displaymath}
    \gamma_k = \begin{cases}
      0& \textrm{if }\sum_{i=1}^{k-1}\gamma_i \geq r_*k\\
      1& \textrm{if }\sum_{i=1}^{k-1}\gamma_i < r_*k\\
    \end{cases}.
  \end{displaymath}
  Therefore, by induction, it is easy to prove that
  \begin{displaymath}
    r_*k \leq \sum_{i=1}^k \gamma_i \leq r_*k+1. 
  \end{displaymath}
  Hence, for all $1\leq k_1 < k_2$, we have
  \begin{align}
    \sum_{i=k_1+1}^{k_2}\gamma_i &= \sum_{i=1}^{k_2}\gamma_i - \sum_{i=1}^{k_1}\gamma_i \geq r_*(k_2-k_1)-1.
  \end{align}

  Now use the fact that the estimation error of the Kalman filter $P_k$ converges to $P$, we can find a constant $\phi$, such that
  \begin{displaymath}
    \phi I \geq Q,\,\phi I \geq \Sigma,\,\phi I\geq P_k,\,\forall k.
  \end{displaymath}
  Now by induction, we can prove that
  \begin{align*}
    E\tilde P_k &\leq\phi \sum_{i = 0}^{k} (1-p)^{\sum_{j=i+1}^k\gamma_j}A^{k-i+1}\left(A^{k-i+1}\right)'\\
    &\leq \phi \left[\sum_{i = 0}^{k} (1-p)^{r_*(k-i)-1}A^{k-i+1}\left(A^{k-i+1}\right)'\right]\\
    &= \frac{\phi}{1-p}A\left[\sum_{i = 0}^{k} \mathcal A^{k-i}\left(\mathcal A^{k-i}\right)'\right]A',
  \end{align*}
  where $\mathcal A = (1-p)^{r_*/2}A$, which is strictly stable as a result of \eqref{eq:noacksufficient}. As a result, $E\tilde P_k$ is bounded, which finishes the proof.
\end{proof}

\begin{remark}
It can be seen that $-2\log(\rho(A))/\log(1-p)$ is the critical communication rate for the No-ACK case. Therefore, when designing the communication protocol, $r_*$ should always be chosen such that $r_* >  -2\log(\rho(A))/\log(1-p)$.
\end{remark}
\subsection{Approximation of Feasible Schedules by Periodic Schedules}
In this section, we prove that given any schedule $\gamma$ and $\delta_1,\delta_2 > 0$, we can always find a periodic schedule $\gamma'$, such that the following inequalities hold:
\begin{displaymath}
 r(\gamma') \leq r(\gamma)+\delta_1,\,J(\gamma',\Sigma)\leq J(\gamma,\Sigma)+\delta_2.
\end{displaymath}
As a result, we only need to focus on finding the optimal periodic schedule instead of searching all possible schedules. Before proving the main theorem, we have following lemma: 
\begin{lemma}
  Suppose there exists $X > 0$ such that $J(\gamma, X) < \infty$, then for all initial conditions $\Sigma$, 
  \[
  J(\gamma,\Sigma) = J(\gamma,X).
  \]
  \label{lemma:initialstate}
\end{lemma}
\begin{proof}
  The proof is based on contracting properties of Ricatti equation. The details are omitted due to space limit.
\end{proof}
\begin{remark}
  Since the value of the cost function is independent of the initial $\Sigma$, we could write $J(\gamma,\Sigma)$ as $J(\gamma)$.
\end{remark}
Now we are ready to prove the main theorem:
\begin{theorem}
  For any schedule $\gamma$ with finite cost function and any positive number $\delta_1,\delta_2 >0$, there exists a periodic schedule $\gamma'$, such that
  \begin{displaymath}
 r(\gamma') \leq r(\gamma)+\delta_1,\,J(\gamma')\leq J(\gamma)+\delta_2.
  \end{displaymath}
  \label{theorem:periodic}
\end{theorem}
\begin{proof}
  By Lemma~\ref{lemma:initialstate}, the initial $\Sigma$ does not affect the cost function. As a result, let us choose a $\Sigma$, such that 
  \begin{displaymath}
   \Sigma > (J(\gamma)+\delta_2) I_n, \,\Sigma > P,
  \end{displaymath}
  where $P$ is the limit estimation error covariance matrix of the smart sensor, and $I_n\in\mathbb R^{n\times n}$ is the identity matrix. By the definition of $r(\gamma)$ and $J(\gamma)$, we know that there exists an $N_1$, such that for all $k \geq N_1$,
  \begin{equation}
    \frac{1}{k}\sum_{i=1}^k \gamma_i \leq r(\gamma)+\delta_1. 
    \label{eq:rate1}
  \end{equation}
  \begin{equation}
    \frac{1}{k}\sum_{i=1}^k trace(E\tilde P_i(\gamma)) \leq J(\gamma)+\delta_2. 
    \label{eq:cost1}
  \end{equation}
  Note that here we denote $E\tilde P_k$ as a function of schedule $\gamma$ in order to avoid possible confusions. Moreover, there must exist infinitely many $E\tilde P_k(\gamma)$s, which satisfy the following inequality
  \begin{displaymath}
    trace(E\tilde P_k(\gamma))\leq J(\gamma)+\delta_2,
  \end{displaymath}
  which further implies that
  \begin{equation}
    E\tilde P_k(\gamma)\leq \Sigma = (J(\gamma)+\delta_2)I_n.
    \label{eq:cost2}
  \end{equation}
  Finally, since $P_k \rightarrow P$, we know there exists $N_3$, such that for all $k \geq N_3$,
  \begin{equation}
    P_{k} \leq \Sigma. 
    \label{eq:estimationerror}
  \end{equation}
  Therefore, we can find an $N$, such that \eqref{eq:rate1}, \eqref{eq:cost1}, \eqref{eq:cost2} and \eqref{eq:estimationerror} hold at the same time. %, i.e.,
 % \begin{displaymath}
 %   \begin{split}
 %   \frac{1}{N}\sum_{k=1}^N \gamma_k &\leq r(\gamma)+\delta_1,\\
 %   \frac{1}{N}\sum_{k=1}^N trace(E\tilde P_k(\gamma)) &\leq J(\gamma)+\delta_2,\\
 %   E\tilde P_N(\gamma)&\leq \Sigma, P_{N}\leq \Sigma .
 %   \end{split}
 % \end{displaymath}
  Now we design the periodic schedule $\gamma'$ to be the same as the original schedule $\gamma$ from time $1$ to time $N$ and then repeat itself from time $N+1$. In other words,  
  \begin{displaymath}
    \gamma'_{kN+j} = \gamma_j,\,k\in \mathbb N_0,1\leq j\leq N. 
  \end{displaymath}
  It is easy to see that
  \begin{displaymath}
    r(\gamma') \leq r(\gamma) + \delta_1.
  \end{displaymath}
  To prove $J(\gamma')\leq J(\gamma)+ \delta_2$, we only need to prove that
  \begin{equation}
    E\tilde P_{kN+j}(\gamma')\leq E\tilde P_j(\gamma') ,\;\forall k\in \mathbb N,\,1\leq j\leq N,
    \label{eq:decreasing}
  \end{equation}
  which can be proved by induction. The details of the proof are omitted due to space limit.
  %We will prove \eqref{eq:decreasing} by induction. First suppose that $kN+j = N+1$. Since
  %\begin{displaymath}
  %  \begin{split}
  %  E\tilde P_{N+1}(\gamma') &= (1-\gamma'_{N+1}p)h( E\tilde P_N(\gamma')) + \gamma'_{N+1} p P_{N+1}\\
  %  &=(1-\gamma_1p)h( E\tilde P_N(\gamma)) + \gamma _1 p P_{N+1}\\
  %  &=(1-\gamma_1p)h( E\tilde P_N(\gamma)) + \gamma _1 p g(P_{N}),
  %  \end{split}
  %\end{displaymath}
  %where we use the fact that $E\tilde P_N(\gamma) =E\tilde P_N(\gamma')$ since the first $N$ $\gamma_k$s are the same. On the other hand, we have
  %\begin{displaymath}
  %  \begin{split}
  % E\tilde P_1(\gamma') &= (1-\gamma_1 p)h(\Sigma)+\gamma_1 p P_1\\
  % & = (1-\gamma_1 p)h(\Sigma)+\gamma_1 p g(\Sigma). 
  %  \end{split}
  %\end{displaymath}
  %$E\tilde P_N(\gamma)\leq \Sigma$, $P_N\leq \Sigma$ and $h,g$ are monotonically increasing, we know that 
  %\begin{displaymath}
  %  E\tilde P_{N+1}(\gamma') \leq E\tilde P_1(\gamma'). 
  %\end{displaymath}
  %Similarly, we can write $E\tilde P_{kN+j}(\gamma')$ as
  %\begin{displaymath}
  %  \begin{split}
  %  E\tilde P_{kN+j}(\gamma') &= (1-\gamma_{j}p)h( E\tilde P_{kN+j-1}(\gamma'))\\
  %  &+ \gamma_{j} p g^{j}(g^{kN}(\Sigma))
  %  \end{split}
  %\end{displaymath}
  %and $E\tilde P_j(\gamma')$ as
  %\begin{displaymath}
  %  E\tilde P_j(\gamma') = (1-\gamma_j p)h(E\tilde P_{j-1}(\gamma'))+\gamma_1 p g^j(\Sigma). 
  %\end{displaymath}
  %Since $P_N = g^N(\Sigma)\leq\Sigma$, we know that 
  %\begin{displaymath}
  %  \Sigma \geq g^N(\Sigma)\geq g^{2N}(\Sigma)\geq \cdots.
  %\end{displaymath}
  %Therefore, if 
  %\begin{displaymath}
  %  E\tilde P_{kN+j-1}(\gamma')\leq E\tilde P_{j-1}(\gamma'),
  %\end{displaymath}
  %then
  %\begin{displaymath}
  %  E\tilde P_{kN+j}(\gamma')\leq E\tilde P_{j}(\gamma'),
  %\end{displaymath}
  %which concludes the proof.
\end{proof}
\begin{remark}
  Due to Theorem~\ref{theorem:periodic}, we only need to focus on periodic schedules for the No-ACK case, as any schedule can be arbitrarily approximated by periodic schedules.  
\end{remark}
\section{Sensor Scheduling for ACK Protocols}
\label{sec:ACK}
In this section, we consider sensor scheduling for ACK case, where the fusion center sends an acknowledgement whenever it receives a packet. We further assume that the acknowledgement can be reliably received by the sensor. Therefore, the smart sensor knows $\gamma_1\eta_1,\ldots,\gamma_{k-1}\eta_{k-1}$ at time $k$. We first provide an explicit optimal solution and then we prove necessary and sufficient conditions for the feasibility of the problem. Without loss of generality, throughout the section, we will assume that the initial condition $\Sigma = P$. 

Let us define the following random variable $\tau_k$ as
\begin{displaymath}
\tau_k = k - \max\left(\{i\leq k:\gamma_i\eta_i=1\}\bigcup \{0\}\right).
\end{displaymath}
Therefore, $\tau_k$ indicates the interval between the last successful communication and the current time and is known by the smart sensor. Since we assume $\Sigma = P$, it is easy to see that 
\begin{displaymath}
  \tilde P_k = \begin{cases}
    h(\tilde P_{k-1})&\gamma_k\eta_k = 0\\
    P&\gamma_k\eta_k = 1
  \end{cases},\; \tilde P_0 = P,
\end{displaymath}
which implies that
\begin{displaymath}
  \tilde P_k = h^{\tau_k}(P). 
\end{displaymath}
Let us define
\begin{displaymath}
 f(k) \triangleq  trace[h^k(P)].
\end{displaymath}
The following proposition characterizes the monotonicity of $f$
\begin{proposition}
  $f(k)$ is monotonically increasing.
\end{proposition}
%\begin{proof}
% Since $P$ is the fixed point of $g(X)$ and $h(X) \geq g(X)$, we have
%  \begin{displaymath}
%   P = g(P) \leq h(P). 
%  \end{displaymath}
%  Now by the monotonicity of $h$, 
%  \begin{displaymath}
%  P\leq h(P)\leq h^2(P)\leq\cdots,
%  \end{displaymath}
%  which concludes the proof.
%\end{proof}
It is easy to see that $\tau_k$ depends only on the previous $\tau_{k-1}$ and the choice of the sensor $\gamma_k$. To be specific, if $\gamma_k = 0$, then 
\begin{equation}
  \tau_k = \tau_{k-1} + 1.
  \label{eq:transitionprob1}
\end{equation}
If $\gamma_k = 1$, then
\begin{equation}
  \tau_k =\begin{cases}
    \tau_{k-1} + 1&\textrm{with probability } 1-p\\
    0&\textrm{with probability }p
  \end{cases}
  \label{eq:transitionprob2}
\end{equation}
For ACK protocols, we will only consider the following class of schedules: At time $k$, the sensor randomly decides to send $\hat x_k$ with probability $\alpha(\tau_{k-1})\in [0,1]$. In other words, the sensor makes a decision only based on $\tau_{k-1}$. In this scenario, $\tau_k$ is a Markov chain with countable states. We will only consider the case where the chain is positively recurrent\footnote{If the chain is not positively recurrent, one can prove that $r(\gamma) = 0$ and $J(\gamma) = \lim_{n\rightarrow\infty} f(n)$.}. Suppose the limit distribution of $\tau_k$ is
\begin{displaymath}
  \lim_{k\rightarrow\infty} P(\tau_k = n) = \pi(n). 
\end{displaymath}
The average communication rate is then given by
\begin{displaymath}
  r(\gamma) = \sum_{n = 0}^\infty \alpha(n)\pi(n),
\end{displaymath}
and the cost function is
\begin{displaymath}
  J(\gamma) = \sum_{n=0}^\infty f(n)\pi(n). 
\end{displaymath}
As a result, the sensor scheduling problem can be written as
\begin{align}\label{eq:optprobACK}
  \min_{\alpha(n)}&&
  &\sum_{n=0}^\infty f(n)\pi(n) \nonumber\\
  s.t.&&
  &\sum_{n = 0}^\infty \alpha(n)\pi(n)\leq r_*,\;\sum_{n=0}^\infty f(n) \pi(n)<\infty.
\end{align}
Next we provide an explicit solution to \eqref{eq:optprobACK}. To this end, we will first change the optimization variable from $\alpha(n)$ to $\pi(n)$, which is given by the following lemma:
\begin{lemma}
  \label{lemma:alphapi}
  For all $\alpha(n)$ satisfies
  \begin{equation}
    \sum_{n = 0}^\infty \alpha(n)\pi(n)\leq r_*,\label{eq:communicationrateACK}
  \end{equation}
  the following inequalities hold
  \begin{align}
    \label{eq:alphatop1}
    \pi(0)&\leq r_*p,\,\\
    \pi(n-1)&\geq\pi(n)\geq (1-p)\pi(n-1). 
    \label{eq:alphatop2}
  \end{align}
  On the contrary, if $\pi(n)$ is a well-defined distribution that satisfies \eqref{eq:alphatop1} and \eqref{eq:alphatop2}, then $\pi(n)$ is the stationary distribution of the following $\alpha(n)$:
  \begin{equation}
    \alpha(n)= \frac{\pi(n)-\pi(n+1)}{p\pi(n)}.  
  \end{equation}
  Moreover, $\alpha(n)$ satisfies \eqref{eq:communicationrateACK}.
\end{lemma}
\begin{proof}
  The lemma can be easily derived from the transition probability \eqref{eq:transitionprob1} and \eqref{eq:transitionprob2}.
\end{proof}
As a result of Lemma~\ref{lemma:alphapi}, equation \eqref{eq:optprobACK} can be written as
\begin{align}\label{eq:optprobACK2}
  \min_{\pi(n)}&&
  &\sum_{n=0}^\infty f(n)\pi(n) \nonumber\\
  s.t.&&
  &\pi(0)\leq r_*p,\,\pi(n)\geq 0,\,\sum_{n=0}^\infty \pi(n) = 1,\\
  &&
  &\pi(n)\geq \pi(n+1)\geq (1-p)\pi(n),\nonumber\\
  &&
  &\sum_{n=0}^\infty f(n) \pi(n)<\infty.\nonumber
\end{align}
Now we are ready to prove the main theorem:
\begin{theorem}
  Let $n_0$ to be the largest integer which satisfies the following inequality
  \begin{displaymath}
    n\leq \frac{1-r_*}{r_*p} + 1. 
  \end{displaymath}
  Let $q=(1-n_0r_*p)p$. The stationary distribution of the optimal schedule is given by
  \begin{equation}
    \pi(n) = \begin{cases}
      r_*p&n\leq n_0\\
      q(1-p)^{n-n_0-1}&n>n_o.
    \end{cases}
    \label{eq:optimaldistribution}
  \end{equation}
  The corresponding $\alpha(n)$ is given by
  \begin{equation}
    \alpha(n) = \begin{cases}
      0&n<n_0\\
      \frac{r_*p-q}{r_*p^2}&n=n_0\\
      1&n>n_0+1.
    \end{cases}
    \label{eq:optimalaction}
  \end{equation}
\end{theorem}
\begin{remark}
  It is worth noticing that the strategy of the sensor is to send $\hat x_k$ when more than $n_0+1$ sequential packets are dropped. On the other hand, the sensor does not send $\hat x_k$ if less than $n_0$ sequential packets are dropped. If there are exactly $n_0$ sequential packets drop, then the sensor will choose a probability that makes the average communication rate to be exactly $r_*$. 
\end{remark}
\begin{proof}
  It is easy to check that $\pi(n)$ is a well defined probability distribution generated by $\alpha(n)$. Now let us consider another stationary distribution $\pi'(n)$ generated by some $\alpha'(n)$. It is trivial to see that for all $n\leq n_0$,
\begin{equation}
 \pi'(n) \leq \pi'(n-1)\leq\cdots\leq \pi'(0)\leq r^*p = \pi(n). 
 \label{eq:head}
\end{equation}
Let us denote $n_1$ as the first time such that
\begin{displaymath}
  \pi'(n) \geq \pi(n).
\end{displaymath}
By \eqref{eq:head}, we know that $n_1 > n_0$. We want to prove that for all $n\geq n_1$, $\pi'(n) \geq \pi(n)$ by induction. By \eqref{eq:alphatop2}, we have
\begin{displaymath}
 \pi'(n+1) \geq (1-p)\pi'(n).  
\end{displaymath}
Since $n\geq n_1 > n_0$, we have
\begin{displaymath}
  \pi(n+1) = q(1-p)^{n-n_0-1} = (1-p)\pi(n). 
\end{displaymath}
As a result, $\pi'(n)\geq \pi(n)$ implies that $\pi'(n+1)\geq \pi(n+1)$. Therefore, for all $n\geq n_1$, $\pi'(n)\geq \pi(n)$. By the definition of $n_1$, $\pi'(n)\leq \pi(n)$ for all $n< n_1$. Since 
\begin{displaymath}
  J(\gamma) = \sum_{n=1}^\infty f(n)\pi(n), 
\end{displaymath}
and $f(n)$ is monotonically increasing and $\sum_{n=1}^\infty \pi(n) =1$, we know that
\begin{displaymath}
  \sum_{n=1}^\infty f(n)\pi(n)\leq \sum_{n=1}^\infty f(n)\pi'(n), 
\end{displaymath}
which concludes the proof.
\end{proof}

The following theorem characterizes the feasibility of \eqref{eq:optprobACK}:
\begin{theorem}
  If 
  \begin{equation}
    1 > -\frac{2\log(\rho(A))}{\log(1-p)},
    \label{eq:ACKoriginal}
  \end{equation}
  then \eqref{eq:optprobACK} is feasible. On the other hand, if
  \begin{equation}
    1 < -\frac{2\log(\rho(A))}{\log(1-p)},
    \label{eq:ACKconverse}
  \end{equation}
  then \eqref{eq:optprobACK} is not feasible.
\end{theorem}
\begin{remark}
  It can be seen that in the ACK case, the feasibility of the optimization problem does not depend on communication rate $r_*$.   
\end{remark}
\begin{proof}
  We first prove \eqref{eq:ACKoriginal}. Let us consider the schedule defined in \eqref{eq:optimaldistribution}, \eqref{eq:optimalaction}. The cost function is given by
  \begin{displaymath}
    \sum_{n=0}^\infty f(n)\pi(n).
  \end{displaymath}
  Since $h(X)$ is a Lyapunov operator, we know that $f(n) = O(\rho^{2n})$. Therefore, $f(n)\pi(n) = O(\rho^{2n}(1-p)^n)$. Thus, if
  \begin{displaymath}
   \rho^2(A)(1-p) < 1\Rightarrow 1 > -\frac{2\log(\rho(A))}{\log(1-p)},
  \end{displaymath}
  then the cost function is finite. 

  We then prove \eqref{eq:ACKconverse}. Consider a schedule $\gamma_*$ where all $\gamma_k = 1$ (such schedule does not satisfy the average communication constraints). It is clear that $\gamma_*$ should outperform all possible schedules. Now by Theorem~3 in \cite{Sinopoli04}, we know that if
  \begin{equation}
    p < 1-\frac{1}{\rho^2(A)}, 
    \label{eq:ACKconverse2}
  \end{equation}
  then even $E\tilde P_k(\gamma_*)$ goes to infinity as $k\rightarrow \infty$, which implies that there is no feasible schedule. Manipulating \eqref{eq:ACKconverse2}, one can easily show that it is equivalent to \eqref{eq:ACKconverse}, which concludes the proof.
\end{proof}
\section{Simulation}
\label{sec:simulation}
In this section, we provide a numerical example for the proposed sensor schedules. Let us choose the following parameters:
\begin{displaymath}
  A = \sqrt{2},\,C = Q = R =1. 
\end{displaymath}
We choose the initial condition $\Sigma = P = 1/\sqrt{2}$ and the packet arrival probability to be $p = 0.75$. By Theorem~\ref{theorem:No-ACKnecessary}, in order to make the scheduling problem feasible in the No-ACK case, $r_*$ should satisfies the following inequality:
\begin{displaymath}
  r_* \geq 2\log(\sqrt{2})/\log(1-0.75) = 0.5 
\end{displaymath}
For ACK case, we know that the scheduling problem is always feasible. Figure~\ref{fig:ACK} shows the optimal cost function versus $r_*$ for both ACK and No-ACK cases. The red dots are the No-ACK cost function of the optimal periodic schedule with period $10$ evaluated at $r_* = 0.6,\,0.7,\,0.8,\,0.9$, respectively. The blue line is the ACK cost function.
\begin{figure}[htpb]
  \setlength\figureheight{3.5cm}
  \setlength\figurewidth{6cm}
  \begin{center}
    \input{tcpudp.tikz}
  \end{center}
  \caption{$J(\gamma)$ v.s. $r_*$ for ACK and No-ACK cases}
  \label{fig:ACK}
\end{figure}

It can be seen that ACK protocols always outperform No-ACK protocols. However, as $r_*$ goes to $1$, the difference between ACK and No-ACK protocols becomes very small. Therefore, when the communication rate is high, No-ACK protocols may be preferable due to simplicity.
\section{Conclusion}
In this paper, we consider the problem of infinite-horizon sensor scheduling over of a discrete-time linear Gaussian system over a lossy network. We prove necessary and sufficient conditions under which there exists a schedule with finite average estimation error for both No-ACK protocols and ACK protocols. For No-ACK case, we prove that the optimal schedule can be arbitrarily approximated by periodic schedules. For the ACK case we were able to provide the optimal schedule explicitely.  
\label{sec:conclusion}
\bibliographystyle{IEEEtran}
\bibliography{SC_Letters09-reference}
%\section{Appendix: Proof of Lemma~\ref{lemma:initialstate}}
%\input{appendix.tex}
\end{document}
