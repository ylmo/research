\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,calc}

\usepackage{pgfplots}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{../tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\let\Tiny\tiny

%for handout
\mode<presentation>

\title[Sensor Scheduling]{Infinite-Horizon Sensor Scheduling for Estimation over Lossy Networks}
\author[Yilin Mo]{Yilin Mo$^*$, Bruno Sinopoli$^*$, Ling Shi$^\dag$, Emanuele Garone$^\ddag$}
\institute[Carnegie Mellon University]{ 
$*$: Dept of Electrical and Computer Engineering\\
Carnegie Mellon University\\
$\dag$: Dept of Electronic and Computer Engineering\\
Hong Kong University of Science and Technology\\
$^\ddag$:  Service d'Automatique et d'Analyse des Syst\`{e}mes\\
Universit\`{e} Libre de Bruxelles}
\date[CDC 2012]{Conference on Decision and Control, 2012}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\begin{frame}{Wireless Sensor Networks}
  \begin{itemize}
    \item Wireless Sensor Networks (WSNs) are becoming ubiquitous, with lots of interesting applications. 
    \item Sensors are usually battery powered. 
    \item Transmission costs too much energy.
    \item Solutions: new devices, energy efficient physical layer, topology control, \alert{sensor scheduling}\dots
  \end{itemize}
\end{frame}

\section{Problem Formulation}

\begin{frame}{System Model}
  We consider a smart sensor is used to monitor the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{displaymath}
	x_{k+1} = Ax_k +  w_k,\; y_{k} = C x_k + v_k.
      \end{displaymath}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors. $y_{k,i}$ is the measurement of sensor $i$ at time $k$.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman filter}
    A Kalman filter is implemented by the smart sensor to perform state estimation, which takes the following form:	
    \begin{block}{Kalman Filter}
      \begin{align*}
	\hat x_{0} & = 0 ,\quad P_{0}  = \Sigma\\
	\hat x _{k + 1|k} & = A \hat x_{k}  , \quad P_{k + 1|k}  = AP_{k} A'  + Q \\
	K_k& = P_{k|k - 1}  C' (CP_{k|k - 1}  C' + R)^{ - 1}  \\
	\hat x_{k}& = \hat x_{k|k - 1}  + K_k(y_k  - C \hat x _{k|k - 1}^* )  \\
	P_{k}& = P_{k|k - 1}  -  K_k CP_{k|k - 1}  ,
      \end{align*}
    \end{block}
    where $\hat x_{k}$ is the state estimation at time $k$, $P_{k}$ is the estimation error covariance matrix.
  \end{frame}

  \begin{frame}{Smart Sensor}
    \begin{itemize}
      \item At each time step, the smart sensor decides whether to send the current state estimate $\hat x_k$ to the fusion center. 
      \item Let us define binary variable $\gamma_k$, such that $\gamma_k = 1$ if and only if the sensor sends $\hat x_k$ at time $k$.
      \item We further define $\gamma = (\gamma_1,\gamma_2,\ldots)$ as the infinite sequence of the sensor schedule. 
      \item We model the communication channel between the sensor and the fusion center as a memoryless erasure channel. 
      \item Let us define binary random variable $\eta_k$, such that $\eta_k = 1$ if the channel is in the good state at time $k$. $\eta_k = 0$ otherwise. 
      \item As a result, the fusion center receives $\hat x_k$ if and only if $\gamma_k = \eta_k = 1$.
    \end{itemize}
    \begin{center}
      \begin{tikzpicture}
	\node[rectangle,draw] (plant) at (0,0) {Plant};
	\node[rectangle,draw] (sensor) at (5,0) {Smart Sensor}
	edge [stealth-,thick] (plant);
	\node[rectangle,draw] (fc) at (10,0) {Fusion Center}
	edge [stealth-,thick] node[auto]{$\eta_k$} (sensor);
	\coordinate (A) at (7.2,0);
	\coordinate (B) at (7.8,0);
	\draw[white,thick] (A)--(B);
	\draw[thick] (A)--($(A) ! 1 ! 30:(B)$);
	\draw[stealth-] (7.7,0) arc (0:45:0.5);
      \end{tikzpicture}
    \end{center}
  \end{frame}

  \begin{frame}{Fusion Center}
    \begin{itemize}
      \item The optimal state estimate of the fusion center is
	\begin{displaymath}
	  \tilde x_{k} = \begin{cases}
	    A\tilde x_{k-1}, & \textrm{when }\gamma_k\eta_k = 0,\\
	    \hat x_k, & \textrm{when }\gamma_k\eta_k = 1.
	  \end{cases}, \tilde x_0 = 0.
	\end{displaymath}
      \item The corresponding estimation covariance is
	\begin{displaymath}
	  \tilde P_k  =  \begin{cases}
	    A\tilde P_{k-1}A' + Q = h(\tilde P_{k-1}),& \textrm{when }\gamma_k\eta_k = 0,\\
	    P_k, & \textrm{when }\gamma_k\eta_k = 1.
	  \end{cases}, \tilde P_0 = \Sigma.
	\end{displaymath}
      \item We define the average communication rate and estimation error as
	\begin{align*}
	  r(\gamma) &\triangleq  \limsup_{N\rightarrow\infty} \frac{1}{N}\sum_{k=1}^{N}\gamma_k.\\
	  J(\gamma,\Sigma) &\triangleq \limsup_{N\rightarrow \infty}\frac{1}{N}\sum_{k=1}^N trace(E\tilde P_k).
	\end{align*}
    \end{itemize}
  \end{frame}

  \begin{frame}{Optimal Sensor Scheduling}
    The goal of the smart sensor is to find an infinite schedule $(\gamma_1,\gamma_2,\ldots)$ which solves the following optimization problem:
    \begin{align*}
      \min_{\gamma}&&
      & J(\gamma,\Sigma)\nonumber\\
      s.t.&&
      &r(\gamma)\leq r_*,\; J(\gamma,\Sigma)<\infty.
    \end{align*}
    We will call a schedule $\gamma$ to be feasible if $r(\gamma)\leq r_*$ and $J(\gamma,\Sigma)$ is finite.
  \end{frame}

  \section{NO-ACK Protocol}
  \begin{frame}{Feasibility of NO-ACK Protocol}
    We first consider that no acknowledgement is sent from the fusion center to the smart sensor. As a result, the smart sensor does not know whether the fusion center receives $\hat x_k$ or not.
    \begin{theorem}
      There exists a feasible schedule for the No-ACK protocols if
      \begin{equation}
	r_* > -\frac{2\log(\rho(A))}{\log(1-p)}.
      \end{equation}
      where $\rho(A)$ is the spectral radius of $A$. On the other hand, if 
      \begin{displaymath}
	r_* < -\frac{2\log(\rho(A))}{\log(1-p)},
      \end{displaymath}
      then there does not exist a feasible schedule for the No-ACK protocols. 
    \end{theorem}
  \end{frame}

  \begin{frame}{Feasibility of NO-ACK Protocol}
    \begin{block}{Sketch of the Proof}
      \begin{itemize}
	\item For the smart sensor, $E\tilde P_k$ follows the following recursive equation:
      \begin{displaymath}
	E\tilde P_k  =  \begin{cases}
	  AE\tilde P_{k-1}A' + Q ,& \textrm{when }\gamma_k = 0,\\
	  pP_k + (1-p)\left(AE\tilde P_{k-1}A'+Q\right), & \textrm{when }\gamma_k = 1.
	\end{cases}.
      \end{displaymath}
    \item Roughly speaking, when $\gamma_k = 0$, then $E\tilde P_k \approx \rho(A)^2 E\tilde P_{k-1}$.
    \item  When $\gamma_k = 1$, $E\tilde P_k \approx (1-p)\rho(A)^2 E\tilde P_{k-1}$.
      \end{itemize}
    \end{block}
  \end{frame}

  \begin{frame}{Approximation of Feasible Schedule by Periodic Schedules}
    \begin{itemize}
      \item It is difficult to optimize over all feasible schedules as they form a infinite dimensional space.  
      \item In the next theorem, we prove that any feasible schedule can be arbitrarily approximated by periodic schedules.
      \item As a result, we only need to focus on periodic schedules (exhaustive search for small period, convex relaxation).
    \end{itemize}
    \begin{theorem}
      For any schedule $\gamma$ with finite cost function and any positive number $\delta_1,\delta_2 >0$, there exists a periodic schedule $\gamma'$, such that
      \begin{displaymath}
	r(\gamma') \leq r(\gamma)+\delta_1,\;J(\gamma',\Sigma)\leq J(\gamma,\Sigma)+\delta_2.
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \section{ACK Protocol}
  \begin{frame}{ACK Protocol}
    \begin{itemize}
      \item For ACK protocols, we assume that the fusion center sends an acknowledgement whenever it receives a packet.
      \item We further assume that the acknowledgement can be reliably received by the sensor.
      \item Therefore, the smart sensor knows $\gamma_1\eta_1,\ldots,\gamma_{k-1}\eta_{k-1}$ at time $k$. 
      \item Let us define the following random variable $\tau_k$ as
	\begin{displaymath}
	  \tau_k = k - \max\left(\{i\leq k:\gamma_i\eta_i=1\}\bigcup \{0\}\right).
	\end{displaymath}
	Therefore, $\tau_k$ indicates the interval between the last successful communication and the current time and is known by the smart sensor.
    \end{itemize}
  \end{frame}

  \begin{frame}{ACK Protocols}
    \begin{itemize}
      \item $\tau_k$ depends only on the previous $\tau_{k-1}$ and the choice of the sensor $\gamma_k$. To be specific, if $\gamma_k = 0$, then 
	\begin{displaymath}
	  \tau_k = \tau_{k-1} + 1.
	\end{displaymath}
	If $\gamma_k = 1$, then
	\begin{displaymath}
	  \tau_k =\begin{cases}
	    \tau_{k-1} + 1&\textrm{with probability } 1-p\\
	    0&\textrm{with probability }p
	  \end{cases}
	\end{displaymath}
    \end{itemize}
  \end{frame}

  \begin{frame}{ACK Protocol}
    \begin{itemize}
      \item  The estimation error covariance
	\begin{displaymath}
	  \tilde P_k  =  \begin{cases}
	    A\tilde P_{k-1}A' + Q = h(\tilde P_{k-1}),& \textrm{when }\gamma_k\eta_k = 0,\\
	    P_k, & \textrm{when }\gamma_k\eta_k = 1.
	  \end{cases}.
	\end{displaymath}
      \item As a result, 
	\begin{displaymath}
	  \tilde P_k = h^{\tau_k}(P_{k-\tau_k}), 
	\end{displaymath}
	where $P_{k-\tau_k}$ converges to a fixed matrix $P$.
      \item Define
	\begin{displaymath}
	  f(k) \triangleq  trace[h^k(P)].
	\end{displaymath}
      \item $trace(P_k)$ converges to $f(\tau_k)$.
    \end{itemize}
  \end{frame}

  \begin{frame}{MDP Formulation}
    \begin{itemize}
      \item We model the strategy of the smart sensor as a Markov Decision Process. At time $k$, the sensor randomly decides to send $\hat x_k$ with probability $\alpha(\tau_{k-1})\in [0,1]$.
      \item Define the limit distribution of $\tau_k$ as: $ \pi(n)\triangleq \lim_{k\rightarrow\infty} P(\tau_k = n)  $.
      \item The average communication rate and cost function are given by
	\begin{displaymath}
	  r(\gamma) = \sum_{n = 0}^\infty \alpha(n)\pi(n),\; J(\gamma,\Sigma) = \sum_{n=0}^\infty f(n)\pi(n). 
	\end{displaymath}
      \item The sensor scheduling problem can be written as
	\begin{align*}
	  \min_{\alpha(n)}&&
	  &\sum_{n=0}^\infty f(n)\pi(n) \nonumber\\
	  s.t.&&
	  &\sum_{n = 0}^\infty \alpha(n)\pi(n)\leq r_*,\;\sum_{n=0}^\infty f(n) \pi(n)<\infty.
	\end{align*}
    \end{itemize}
  \end{frame}

  \begin{frame}{The Optimal Solution of MDP}
    \begin{theorem}
      Let $n_0$ to be the largest integer which satisfies the following inequality
      \begin{displaymath}
	n\leq \frac{1-r_*}{r_*p} + 1,
      \end{displaymath}
      and let $q=(1-n_0r_*p)p$. The optimal $\alpha(n)$ is given by
      \begin{displaymath}
	\alpha(n) = \begin{cases}
	  0&n<n_0\\
	  \frac{r_*p-q}{r_*p^2}&n=n_0\\
	  1&n>n_0+1.
	\end{cases}
      \end{displaymath}
      The corresponding stationary distribution of the optimal schedule is given by
      \begin{displaymath}
	\pi(n) = \begin{cases}
	  r_*p&n\leq n_0\\
	  q(1-p)^{n-n_0-1}&n>n_o.
	\end{cases}
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \begin{frame}{Feasibility of the ACK Protocol}
    \begin{theorem}
      There exists a feasible schedule for the ACK case if 
      \begin{displaymath}
	1 > -\frac{2\log(\rho(A))}{\log(1-p)},
      \end{displaymath}
      On the other hand, if
      \begin{displaymath}
	1 < -\frac{2\log(\rho(A))}{\log(1-p)},
      \end{displaymath}
      then there does not exist a feasible schedule 
    \end{theorem}
    It can be seen that in the ACK case, the feasibility of the optimization problem does not depend on communication rate $r_*$.   
  \end{frame}

  \section{Simulation}
  \begin{frame}{Simulation}
    \begin{itemize}
      \item We choose the following parameter
	\begin{displaymath}
	  A = \sqrt{2},\,C = Q = R =1,\, \Sigma = 1/\sqrt{2},\, p=0.75.
	\end{displaymath}
      \item To make the No-ACK protocol feasible, $r_*$ should satisfy
	\begin{displaymath}
	  r_* \geq 2\log(\sqrt{2})/\log(1-0.75) = 0.5 
	\end{displaymath}
      \item On the other hand, the ACK protocol is always feasible regardless of $r_*$.
    \end{itemize}
  \end{frame}

  \begin{frame}{Simulation}
    \begin{figure}[htpb]
      \setlength\figureheight{5cm}
      \setlength\figurewidth{10cm}
      \begin{center}
	\inputtikz{tcpudp}
      \end{center}
      \caption{$J(\gamma,\Sigma)$ v.s. $r_*$ for ACK and No-ACK cases}
    \end{figure}
  \end{frame}

  \section{Conclusion}
  \begin{frame}{Conclusion}
    \begin{itemize}
      \item We consider the problem of infinite-horizon sensor scheduling over of a discrete-time linear Gaussian system over a lossy network.
      \item We prove necessary and sufficient conditions (with a trivial gap) under which there exists a schedule with finite average estimation error for both No-ACK protocols and ACK protocols. 
      \item For No-ACK case, we prove that the optimal schedule can be arbitrarily approximated by periodic schedules.
      \item For the ACK case we were able to provide the optimal schedule explicitly. 
    \end{itemize}
  \end{frame}

  \end{document}


