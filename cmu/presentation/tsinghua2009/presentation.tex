\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Estm. and Cont. over WSNs]{Estimation and Control over Wireless Sensor Networks}
\author[Yilin Mo]{Yilin $\mathrm{Mo}^*$, Bruno $\mathrm{Sinopoli}^*$, Roberto $\mathrm{Ambrosino}^{\dag}$ and Ling $\mathrm{Shi}^{\ddag}$}
\institute[Carnegie Mellon University]{$*$: Department of Electrical and Computer Engineering,\\ Carnegie Mellon University\\
$\dag$: Dipartimento per le Tecnologie,\\ Universit\`{a} degli Studi di Napoli Parthenope\\
$\ddag$: Electronic and Computer Engineering,\\ Hong Kong University of Science and Technology\\}
\date[Sept 10th, 2009]{September 10th, 2009}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\setbeamercovered{transparent}
\begin{frame}[label=outline]
  \frametitle{Outline}
  \tableofcontents[pausesections]
\end{frame}
\setbeamercovered{invisible}

\section{Introduction}
\begin{frame}{Wireless Sensor Networks}
  \begin{block}{Definition}
A wireless sensor network (WSN) is a wireless network consisting of spatially distributed autonomous devices using sensors to cooperatively monitor physical or environmental conditions. (From Wikipedia)
  \end{block}
      \begin{figure}
	\begin{center}
	  \includegraphics[width = 0.7 \textwidth]{wsn.jpg}
	\end{center}
	\caption{Wireless Sensor Network}
	\begin{center}
		\tiny{\url{http://agent.cs.dartmouth.edu/scalable/DSCN0022.JPG}}
	\end{center}
      \end{figure}
\end{frame}


\begin{frame}{Wireless Sensor Networks}
  \begin{enumerate}
    \item Nodes consist of sensor, micro-controller, transceiver, battery. 
    \item Wireless
    \item Low cost
    \item Easy to setup
    \item \dots
  \end{enumerate}
\end{frame}

\begin{frame}{Applications}
  \begin{enumerate}
    \item Traffic control
    \item Health care
    \item Habitat monitoring
    \item Battlefield monitoring
    \item \dots
  \end{enumerate}
\end{frame}

\begin{frame}{New Challenges}
  \begin{enumerate}
    \item Low reliability: sensors and communication between sensors may fail.
    \item Large scale: algorithms need to be scalable. (Better to be distributed)
    \item Energy constrained: communication costs too much energy. (Network protocols need to be redesigned.)
    \item Exposed: sensors are easily accessible by malicious third party.
    \item Heterogeneous: different sensors in the same network.
    \item \dots\dots
  \end{enumerate}
  \pause
  How to analyze the efficiency of classical algorithms in WSNs? How to design new algorithms which are robust, scalable, energy efficient, secure, \dots?
\end{frame}

\section{Preliminary: Kalman Filter}

\againframe<2>{outline}

\begin{frame}{How to Combine Measurements}
  \begin{itemize}
    \item Alice and Bob measure temperature $x$ in the room.
    \item Alice: measurement $y_1$ and error covariance $\sigma_{y_1}^2$.
    \item Bob: measurement $y_2$ and error covariance $\sigma_{y_2}^2$.
    \item What is the best estimate of $x$?
  \end{itemize}
  \begin{block}{Maximum Likelihood Estimator}
    \begin{align*}
      \left(\frac{1}{\sigma_{y_1}^2}+\frac{1}{\sigma_{y_2}^2}\right)\hat x &= \frac{y_1}{\sigma_{y_1}^2} + \frac{y_2}{\sigma_{y_2}^2}\\
      \frac{1}{\sigma^2}& = \frac{1}{\sigma_{y_1}^2}+ \frac{1}{\sigma_{y_2}^2}
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}{How to Combine Measurements}
  \[y_1 = -0.5,\,\sigma_{y_1}^2 = 0.5,\,y_2 = 0, \sigma_{y_2}^2 = 2,\,\hat x = -0.4, \sigma^2 = 0.4.\]
  \begin{figure}[htpb]
    \begin{center}
      \only<1>{ \includegraphics[width = 0.7 \textwidth]{measure2.jpg}}
      \only<2>{ \includegraphics[width = 0.7 \textwidth]{measure1.jpg}}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Multiple Measurements}
  Consider $M$ measurements $y_i$, with covariances $\sigma_{y_i}^2$.
  \begin{block}{Maximum Likelihood Estimator}
    \begin{align*}
      \left(\sum_{i=1}^M\frac{1}{\sigma_{y_i}^2}\right)\hat x &=\sum_{i=1}^M \frac{y_i}{\sigma_{y_i}^2} \\
      \frac{1}{\sigma^2}& =\sum_{i=1}^M \frac{1}{\sigma_{y_i}^2}
    \end{align*}
  \end{block}
  \pause
  What if the measurements do not arrive at the same time?
\end{frame}

\begin{frame}{Sequential Measurements}
Define $\hat x_k$ to be the estimation of $x$ given $y_1,\ldots, y_k$, $\sigma_k^2$ to be the error covariance.
\begin{block}{Recursive Update}
    \begin{align*}
      \hat x_k &= \hat x_{k-1} + K_k(y_k - \hat x_{k-1}) \\
      K_k & = \frac{\sigma_{k-1}^2}{\sigma_{k-1}^2 + \sigma_{y_k}^2}\\
      \frac{1}{\sigma_k^2}& =\frac{1}{\sigma_{k-1}^2}+ \frac{1}{\sigma_{y_k}^2}
    \end{align*}
\end{block}
\end{frame}

\begin{frame}{System Model}
  We consider the WSN is monitoring the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{equation}
	\begin{split}
	  x_{k+1} &= Ax_k + Bu_k + w_k,\\
	  y_{k} &= C x_k + v_k.
	\end{split}
	\label{eq:systemdiscription}
      \end{equation}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors.
      \item $u_k \in \mathbb R^p$ is the control inputs.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
    \end{itemize}
  \end{frame}


\begin{frame}{ Kalman filter}
  Solution: Kalman filter!!
  \pause
  \begin{block}{Kalman Filter}
    \begin{align*}
      Initialization :&\\
    &\hat x_{0|-1}  = \bar x_0 ,\quad P_{0|-1}  = \Sigma\\
    Prediction:&&\\
    &\hat x _{k + 1|k}  = A \hat x_{k|k} + Bu_k  , \quad P_{k + 1|k}  = AP_{k|k} A^H  + Q \\
    Correction:&&\\
    &\hat x_{k|k} = \hat x_{k|k - 1}  + K_k (y_k  - C \hat x _{k|k - 1} )  \\
    &K_k = P_{k|k - 1}  C^H ( CP_{k|k - 1}  C^H  +  R)^{ - 1}  \\
    &P_{k|k}^{-1} = P_{k|k - 1}^{-1} + C^H R^{-1} C  ,
  \end{align*}
\end{block}
\end{frame}

\begin{frame}{Important Facts about Kalman Filter}
  \begin{enumerate}
    \item Kalman filter is a linear filter ($K_k$ does not depend on $y_k$).
    \item You need to keep track of both $\hat x$ and $P$.
    \item Estimation gain $K_k$ is not fixed in general.
      \pause
    \item However\dots
      \pause
    \item If the system is observable, then $K_k$ and $P_{k|k}$ converges!!
    \item You can use a linear filter with gain $K_\infty$ to achieve the same performance as Kalman filter!!(No need to keep track of $P$)
  \end{enumerate}
\end{frame}

\section{Secure Control}

\againframe<3>{outline}

\begin{frame}{Information Security}
  \begin{itemize}
    \item Key properties of information security:
      \begin{enumerate}
	\item Secrecy: unauthorized individuals cannot read sensors' data.
	\item Integrity: unauthorized individuals cannot modify sensors' data.
	\item Authenticity, non-repudiation,\dots
      \end{enumerate}
      \pause
    \item Is information security enough?
      \pause
    \item Secrecy: attacker can place a sensor besides your sensor
    \item Integrity: attacker can place an actuator besides your sensor
  \end{itemize}
\end{frame}

\begin{frame}{Attack Model}
  The attacker can
  \begin{enumerate}
    \item record and modify the sensors' readings $y_k$
    \item inject malicious control input
  \end{enumerate}
  \pause
  \begin{block}{Replay Attack}
    \begin{enumerate}
      \item Record sufficient number of $y_k$s
      \item Inject malicious control input to the system and replay the previous $y_k$s.
    \end{enumerate}
  \end{block}
  \pause
  We want to see if the classical estimation, control and failure detection scheme can detect replay attack.
\end{frame}

\begin{frame}{Kalman Filter and LQG Controller}
  \begin{itemize}
    \item Kalman filter (Assume already in steady state)
      \begin{displaymath}
	\hat x_{0|-1} = \bar x_0, \hat x_{k+1|k} = A\hat x_{k|k} + Bu_k,\hat x_{k+1|k+1} = \hat x_{k+1|k}+ K(y_{k+1} - C \hat x_{k+1|k}).
      \end{displaymath}
\item The LQG controller tries to minimize
  \begin{displaymath}
    J =\min \lim_{T\rightarrow \infty}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right].
  \end{displaymath}
\item The solution is a fixed gain controller
  \begin{displaymath}
u_k^* = -(B^TSB+U)^{-1}B^TSA\hat x_{k|k} = L \hat x_{k|k},
  \end{displaymath}
  where
  \begin{displaymath}
    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
  \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{$\chi^2$ Failure Detector}
\begin{itemize}
  \item The innovation of Kalman filter $y_k - C\hat x_{k|k-1}$ are i.i.d. Gaussian distributed with zero mean.
    \begin{block}{$\chi^2$ Detector}
      \begin{displaymath}
  g_k=\sum_{i = k-\mathcal T+1}^k (y_i - C\hat x_{i|i-1})^T\mathcal P^{-1}(y_i - C\hat x_{i|i-1})\lessgtr threshold,
      \end{displaymath}
      where $\mathcal P$ is the covariance of the innovation.
    \end{block}
\end{itemize}
\end{frame}

\begin{frame}{System Diagram}
      \begin{figure}
	\begin{center}
	  \includegraphics[width = 0.8 \textwidth]{diagram.1}
	\end{center}
	\caption{System Diagram}
      \end{figure}
\end{frame}

\begin{frame}{Time Shifted System}
  \begin{enumerate}
    \item To simplify notation, consider the changed readings $y_k'$ comes from the following system
\begin{align*}
x'_{k+1} &= Ax'_{k} + Bu'_k + w'_k,\; y'_k  = Cx'_{k} + v'_k,\\
\hat x'_{k+1|k}&= A\hat x'_{k|k} + Bu'_k,\; \hat x'_{k+1|k+1}  = \hat x'_{k+1|k} + K(y'_k-C\hat x'_{k+1|k}),\\
u'_k &= L\hat x'_{k|k}.
\end{align*}
\item The above system is a time shifted version of original system.
  \end{enumerate}
\end{frame}

\begin{frame}{Detection of Replay Attack}
  \begin{itemize}
\item Manipulating the update equation of Kalman filter
\begin{align*}
\hat x_{k+1|k} &=(A+BL)(I-KC)\hat x_{k|k-1} +(A+BL) Ky'_k \\
&= \mathcal A \hat x_{k|k-1} + (A+BL)Ky'_k\\
\hat x_{k+1|k}' &=\mathcal A \hat x_{k|k-1}' + (A+BL)Ky'_k . \\
\end{align*}
\item Hence, 
\begin{align*}
  \hat x_{k|k-1} - \hat x'_{k|k-1}& = \mathcal A^k( \hat x_{0|-1} - \hat x'_{0|-1}).\\
  y_k' - C\hat x_{k|k-1}& =( y_k' - C\hat x'_{k|k-1}) + C\mathcal A^k( \hat x_{0|-1} - \hat x'_{0|-1}),
\end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{Counter Measures}
     Replay is feasible because the optimal estimator and controller use fixed gains:
      \begin{align*}
	\hat x_{k+1|k} &= \mathcal A \hat x_{k|k-1} + (A+BL)Ky'_k\\
	\hat x_{k+1|k}' &=\mathcal A \hat x_{k|k-1}' + (A+BL)Ky'_k . \\
      \end{align*}
 If we add a random control input to the system:
  \begin{enumerate}
    \item If the system respond to this input, then there is no replay attack
    \item If the system does not respond, then there is a replay attack
    \item Random control inputs act like time stamps. 
    \item Cost: The controller is not optimal any more.
  \end{enumerate}	
\end{frame}

\begin{frame}{Counter Measures}
Let control input to be
\begin{align*}
u_k = u_k^* + \Delta u_k,
\end{align*}
where $u_k^*$ is the LQG control input,  $\Delta u_k$ is a Gaussian random control input with 0 mean and covariance of $\mathcal Q$.
\pause

Increasing in LQG cost: \[trace[(U+B^TSB)\mathcal Q].\]

\pause
Kalman filtering equation:
\begin{align*}
\hat x_{k+1|k} & = \mathcal A \hat x_{k|k-1}' + (A+BL)Ky'_k + B\Delta u_k\\
\hat x_{k+1|k}' &=\mathcal A \hat x_{k|k-1}' + (A+BL)Ky'_k + B\Delta u_k',
\end{align*}
and
\begin{align*}
  \hat x_{k|k-1} - \hat x'_{k|k-1}& = \mathcal A^k( \hat x_{0|-1} - \hat x'_{0|-1}) +  \sum_{i=0}^{k-1}\mathcal A^{k-i-1}B(\Delta u_i - \Delta u'_i) .\\
\end{align*}
\end{frame}

\begin{frame}{New System Diagram}
  \begin{figure}
    \begin{center}
      \includegraphics[width = 0.8 \textwidth]{diagram.2}
    \end{center}
    \caption{System Diagram}
  \end{figure}
\end{frame}

\begin{frame}{Simulation Result}
  \begin{enumerate}
    \item One dimensional system, single sensor
\begin{align*}
x_{k+1}&=x_{k} + u_k + w_k\\
y_k &=x_{k} + v_k
\end{align*}
\item Parameters:
  \begin{itemize}
    \item $R = 0.1$, $Q=1$.
    \item $W=U=1$.
    \item Detector window size $\mathcal T = 5$, false alarm rate $5\%$.
    \item $K = 0.9161,\, L = -0.6180,\, \mathcal A = 0.0321.$
  \end{itemize}
  \end{enumerate}
\end{frame}

\begin{frame}{Simulation Result}
\begin{figure}[htb]
\begin{minipage}{0.6\textwidth}
\centering
      \includegraphics[width = 0.95 \textwidth]{security.jpg}
\caption{Detection Rate}
\end{minipage}%
\begin{minipage}{0.5\textwidth}
  \begin{itemize}
    \item Blue: $\mathcal Q = 0.6$
    \item Brown: $\mathcal Q = 0.4$
    \item Red: $\mathcal Q = 0.2$
    \item Dark Blue: $\mathcal Q = 0$
  \end{itemize}
\end{minipage}
\end{figure}
\end{frame}

\begin{frame}{Extensions}
  \begin{enumerate}
    \item Even smarter attacker?
    \item DoS attack?
    \item Replay mixed with DoS attack?
    \item Better counter measures?
    \item \dots
  \end{enumerate}
\end{frame}

\section{Sensor Selection}
\againframe<4>{outline}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Suppose $m$ unique sensors measuring the same quantity,
      \begin{displaymath}
	\sigma^2 = \sigma_y^2 / m.
      \end{displaymath}
    \item The energy cost is at least proportional to $m$.
      \pause
    \item Depends on application, not all sensors are needed!!
    \item Put sensors to sleep to save energy.
  \end{itemize}
      \begin{figure}[H]
	\begin{center}
	  \includegraphics{pic.1}
	  \caption{Sensors Selected at time $k$} \label{fig:sensor_topology_example}
	\end{center}
      \end{figure}
\end{frame}

\begin{frame}{Problem Formulation}
  \begin{itemize}
      \item Define $\gamma_{k,i}$ as the binary variable such that $\gamma_{k,i}=1$ if sensor $i$ transmits during the time step $k$.
      \item Define $\gamma_k \triangleq diag(\gamma_{k,1},\ldots,\gamma_{k,m})$. 
      \item Define $\vec \gamma \in \mathbb R^{mT}$ to be a vector of $\gamma_{k,i},\;k=1,\ldots,T,\,i=1,\ldots,m$.
  \end{itemize}
\pause
  \begin{block}{Multi-step Sensor Selection Problem}
\begin{align*}\label{opt_prob}
 \, &\min_{\vec\gamma}\quad \quad \sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T),\\
  s.t.&\quad \quad \quad  H \vec \gamma \leq b, \nonumber\\
  & \quad \quad\quad \gamma_{k,i} = 0\;or\;1,\;k=1,\ldots,T,\;i=1,\ldots,m \nonumber
\end{align*}
where $\mathcal Q_k \in \mathbb R^{n' \times n}$ and is of full row rank,  $H \in R^{h \times mT}$ and $b\in R^h$.
  \end{block}
\end{frame}

\begin{frame}{Possible Objective Functions}
  \begin{enumerate}
    \item Minimization of the final estimation error:
\[\mathcal Q_1,\ldots,\mathcal Q_{T-1} = 0,\;\mathcal Q_T = I \]
\[\Rightarrow 
 \sum_{k=1}^T trace(\mathcal Q_kP_{k}\mathcal Q_k^T)=trace(P_{T})
\]
    \item Minimization of the average estimation error:
\[\mathcal Q_k = I, k = 1,\ldots,T\]
      \[\Rightarrow 
 \sum_{k=1}^T trace(\mathcal Q_kP_{k}\mathcal Q_k^T)=\sum_{k=1}^T  trace(P_{k})
\]      
    \item Minimization of objective function of LQG control
  \end{enumerate}
\end{frame}

\begin{frame}{Possible Constraints}
  \begin{enumerate}
    \item Fixed number of sensors to be used at each time step
\[\sum_{i=1}^m\gamma_{k,i} \leq p, \;k=1,\ldots,T\]
where $p$ is the number of sensor selected in each step.
\item Sensor energy constraints
\[e_i\sum_{k=1}^T \gamma_{k,i} \leq \mathcal E_i,\;i =1,\ldots,m\]
where $E_i$ and $e_i$ are respectively the initial energy and the transmission energy consumption of the sensor $i$. 
\item Topology Constraints
\[\gamma_{k,i} \geq \gamma_{k,j},\;k =1,\ldots,T,\;j \in \mathcal C_i\]
where $\mathcal C_i$ is the set of child nodes of sensor $i$.  
  \end{enumerate}
\end{frame}

\begin{frame}{Reformulation of Estimation Error Covariance}
  \begin{enumerate}
    \item Let us first only consider estimation error covariance $P_{1|1}$.
      \begin{block}{}
	\begin{align}
	  K_1& = P_{1|0}  C^H\gamma_1 (\gamma_1 CP_{1|0}  C^H \gamma_1 + \gamma_1 R\gamma_1)^{ - 1}  \nonumber\\
	  P_{1|1}& = P_{1|0}  -  K_1 \gamma_1 CP_{1|0}  \nonumber,
	\end{align}
      \end{block}
    \item The relation between $P_{1|1}$ and $\gamma_1$ is very complicated
    \item However, we find that
      \begin{equation}
	P_{1|1} = P_{1|1}^* + (K_1\gamma_1 - K_1^*)Cov(y_1)(K_1\gamma_1 - K_1^*)^T,
      \end{equation}
      where $K_1^*,\,P_{1|1}^*$ are the Kalman gain and error covariance matrix when all the sensor data are used, i.e., $\gamma_1 = I$.
  \end{enumerate}
\end{frame}

\begin{frame}{Reformulation of Estimation Error Covariance}
  \begin{theorem}
    Consider the LTI system. The estimation error covariance matrix $P_{k|k}$ satisfies the following equality
    \begin{equation}
      P_{k|k} = P_{k|k}^* + (G_k\Gamma_k-G_k^*)Cov(Y_k)(G_k\Gamma_k-G_k^*)^T,
    \end{equation}
    where $P_{k|k}^*$ is the estimation error covariance when all the sensor data are used, $Y_k = [y_1^T,\ldots,y_k^T]^T$, $\Gamma_k = diag(\gamma_1,\ldots,\gamma_k)$. $G_k,\,G_k^*$ satisfy the following recursive equations
    \begin{align*}
      G_{1}^* &= K^*_1,\,&G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*],\\
      G_{1} &= K_1,\,&G_{k+1} = [(A-K_{k+1}CA)G_k,\;K_{k+1}].
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{Minimization Problem}
  We can relax the original problem into
  \begin{block}{}
    \begin{align*}
      &\;\min_{\mathcal G_k,\gamma_{k,i}} \quad \quad \sum_{k=1}^T trace\left[ (\mathcal G_k - \mathcal Q_kG_k^*)Cov(Y_k)(\mathcal G_k  - \mathcal Q_kG_k^*)^T\right],\\
  &s.t. \quad \quad  H \vec \gamma \leq b \\ \nonumber
  & \quad\quad \quad \gamma_{k,i} \geq \left\|\sum_{j = k}^T \left\|{\vec {\mathcal G}}_{j,(k-1)m+i}\right\|_1\right\|_0,
    \end{align*}
  \end{block}
  where $\mathcal G_k =\mathcal Q_k G_k\Gamma_k$.
\end{frame}

\begin{frame}{Illustrative Example}
Consider a system composed of
  \begin{itemize}
    \item  $m=30$ sensors 
    \item $n=20$ states 
    \item $A,\,C$ randomly chosen matrices 
    \item $Q,\,R$ randomly chosen symmetric positive defined matrices 
    \item  $\mathcal Q_k=I ,\, \forall k$
  \end{itemize}
\end{frame}

\begin{frame}{Illustrative Example}
We consider the estimation performance both of our strategy and a random strategy and we normalize them respect to the optimal one.
  \begin{figure}[H]
    \begin{center}
      \includegraphics[width=0.6\textwidth]{performancegap.jpg}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Extension}
  \begin{enumerate}
    \item Dual problem?
    \item Asynchronized measurements?
    \item Distributed strategy?
    \item Local fusion?
    \item \dots
  \end{enumerate}
\end{frame}

\section{Kalman Filtering with Intermittent Observation}
\againframe<5>{outline}

\begin{frame}{Classical Kalman Filter in Unreliable Network}
  \begin{overlayarea}{\textwidth}{2cm}
    Kalman filter assumes that all the observations $y_0,y_1,\ldots,y_k$ are available at time $k$.

    However in an unreliable network, observation packets can be lost, corrupted or significantly delayed.
  \end{overlayarea}
  \begin{overlayarea}{\textwidth}{2cm}
    \only<1|handout:0>{\includegraphics{slides.1}}
    \only<2-|handout:1>{\includegraphics{slides.2}}
  \end{overlayarea}
  What is the form of optimal Filter? 
\end{frame}

\begin{frame}{Channel Models}  
  \begin{itemize}
    \item Erasure channel: The estimator does not use corrupted or significantly delayed packets (treated as lost). 
      \pause
    \item Single packet: All the observations made at time $k$ are either received in full or lost completely.
   \[
  \widetilde y_k = \gamma_k y_k,\;\gamma_k=0,1
  \]
  \pause
  \item Memoryless channel: $\gamma_i$s are i.i.d. distributed and $P(\gamma = 1) = p$.
  \end{itemize}
\end{frame}

\begin{frame}{Kalman Filter with Intermittent Observation}
  [Sinopoli, 04] This problem can still be solved recursively in a similar way as classical Kalman filter. 
  \begin{align}
    Initialization:&&\hat x_{0| - 1} & = \overline x_0 ,\quad P_{0| - 1}  = \Sigma \\
    Prediction:&&\hat x _{k + 1|k} & = A \hat x _{k|k}  , \quad P_{k + 1|k}  = AP_{k|k} A^H  + Q \\
    Correction:&&K_k& = P_{k|k - 1} C^H (CP_{k|k - 1} C^H  + R)^{ - 1}\\
    &&\hat x _{k|k}& = \hat x _{k|k - 1}  + \alert{\gamma_k} K_k (y_k  - C \hat x _{k|k - 1} ) \\
    &&P_{k|k}^{-1}& = P_{k|k - 1}^{-1}  +  \alert{\gamma_k} C^H R^{-1} C. 
  \end{align}
  \pause
  What about performance?
\end{frame}


\begin{frame}{Kalman Filter with Intermittent Observation}
  Rewrite the error covariance matrix as
  \begin{equation}
    P_{k+1}=AP_k A^H+Q-\gamma_k A P_k C^H(C P_kC^H+R)^{-1}C P_k A^H,
    \label{eq:basicricattieqn}
  \end{equation}
  where $P_k \triangleq P_{k|k-1}$.  \pause $P_k$ is now stochastic. $EP_k$ ?
  \pause
  \begin{displaymath}
    \begin{split}
      E P_{k+1}&= E [A P_k A^H]+ EQ-E[\gamma_k A P_k C^H(C P_kC^H+R)^{-1}C P_k A^H]\\
      &=A (EP_k) A^H + Q -E\gamma_k \times A E[\alert{P_kC^H(C P_kC^H+R)^{-1}CP_k}] A^H\\
      & = ???
    \end{split}
  \end{displaymath}
  In general it is impossible to get a recursive equation of $EP_k$ due to the non-linear nature of Riccati Equation.
\end{frame}

\begin{frame}{Upper and Lower Bound for $EP_k$}
  \begin{theorem}
    [Sinopoli,04] Assume that $(A, Q^{1/2})$ is controllable, $(C,A)$ is detectable. Then
    \begin{equation}
      0 < S_k \leq EP_k \leq V_k,
      \label{eq:errorbound}
    \end{equation}
    where $S_0 = P_0 = V_0$ and 
    \begin{equation}
      \begin{split}
	S_{k+1} &= [1-P(\gamma=1)] A S_k A^T + Q\\
	V_{k+1} &= AV_kA^T + Q - P(\gamma=1) AV_kC^T(CV_kC^T+R)^{-1}CV_kA
      \end{split}
    \end{equation}
  \end{theorem}
  \pause
  Asymptotic of $P_k,S_k,V_k$?
\end{frame}

\begin{frame}{Asymptotic of $S_k, V_k$}
  \begin{displaymath}
    A = diag(2,-2), C = [1,1], Q = I, R = 1, \Sigma_0 = I
  \end{displaymath}
  \begin{figure}
    \begin{center}
      \includegraphics[height=5cm]{slides.15}
    \end{center}
  \end{figure}
  \pause
  $\lim_{k\rightarrow\infty}EP_k$ will be unbounded under certain circumstances.
\end{frame}

\begin{frame}{Existence of Critical Value under i.i.d. packets loss}
  \begin{theorem}
    [Sinopoli,04] Under i.i.d. packets loss, if $(A,Q^{\frac{1}{2}})$ is controllable, $(C,A)$ is detectable, and $A$ is unstable, then there exists a $p_c\in[0,1)$ such that
    \begin{eqnarray}\label{eqn:lambdaCrit}
      \lim_{k\rightarrow\infty}E[P_k]=+\infty & \mbox{for } 0\leq
      p \leq p_c \; \;
      \mbox{and  \hspace{3mm}$\exists P_0 \geq 0$}&\\
      E[P_k] \leq M_{P_0} \;\; \forall k & \mbox{for } p_c <
      p \leq 1 \; \; \mbox{and \hspace{3mm}$\forall P_0 \geq 0$}&
    \end{eqnarray}
    where $M_{P_0} > 0 $  depends on the initial condition $P_0 \geq 0$
  \end{theorem}
\end{frame}

\begin{frame}{Bounds for Critical Value under i.i.d. packets loss}
  \begin{figure}
    \begin{center}
      \includegraphics{slides.16}
    \end{center}
  \end{figure}
  \pause
  If $C$ is invertible, then $\underline p_c = \overline p_c = p_c$.
\end{frame}

\begin{frame}{New results}
  \begin{enumerate}
    \item (Plarre, 09) Upper and lower bounds meets each other when $C$ is invertible in observable space 
    \item (Mo, 08) Upper and lower bounds meets each other when system is non-degenerate 
    \item (Huang, 07) Critical value of Kalman filter under Markovian packet drop 
    \item (Xie, 09) Critical value of Kalman filter under packet drop and quantization 
    \item (Censi, 09) (Vakili, 09) Probability density function of error covariance matrix 
  \end{enumerate}
\end{frame}

\section{Conclusion}


\begin{frame}{Conclusion}
In this presentation, we considered the following three problems in WSNs
\begin{enumerate}
  \item Secure control,
  \item Sensor selection,
  \item Kalman filtering with intermittent observations.
\end{enumerate}
\pause
There are a lot of exciting problems:

cooperative control, quantization, asynchronism, delays, distributed estimation, data fusion, target tracking, consensus...
\end{frame}


\begin{frame}
  \begin{center}
    \Huge{ Thank you for your time!!\\
    Questions?}
  \end{center}
\end{frame}
\end{document}
