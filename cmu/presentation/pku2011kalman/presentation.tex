\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{subfigure}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Kalman Filtering]{Kalman Filtering with Intermittent Observations}
\author[Yilin Mo]{Yilin Mo and Bruno Sinopoli}
\institute[Carnegie Mellon University]{Department of Electrical and Computer Engineering\\ Carnegie Mellon University}
\date[Aug 31, 2011]{August 31, 2011}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Motivation}
  \begin{overlayarea}{\textwidth}{2cm}
    Kalman filter assumes that all the observations $y_0,y_1,\ldots,y_k$ are available at time $k$.

    However in an unreliable network, observation packets can be lost, corrupted or significantly delayed.
  \end{overlayarea}
  \begin{overlayarea}{\textwidth}{2cm}
    %\only<1|handout:0>{\includegraphics{slides.1}}
    %\only<2-|handout:1>{\includegraphics{slides.2}}
  \end{overlayarea}
  What is the optimal filter? What is the performance of the optimal filter?
\end{frame}

\begin{frame}{System Model}
  We consider the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{equation}
	\begin{split}
	  x_{k+1} &= Ax_k  + w_k,\\
	  y_{k} &= C x_k + v_k.
	\end{split}
	\label{eq:systemdiscription}
      \end{equation}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
      \item We assume that $A$ is diagonalizable and unstable, $(A,C)$ is observable, $(A,Q^{1/2})$ is controllable. 
    \end{itemize}
  \end{frame}

  \begin{frame}{Channel Models}  
    \begin{itemize}
      \item Erasure channel: The estimator does not use corrupted or significantly delayed packets (treated as lost). 
      \item Single packet: All the observations made at time $k$ are either received in full or lost completely.
	\[
	\widetilde y_k = \gamma_k y_k,\;\gamma_k=0,1
	\]
      \item Memoryless channel: $\gamma_i$s are i.i.d. distributed and $P(\gamma = 1) = p > 0$.
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman Filter}
    (Sinopoli, 04) The estimation problem can still be solved recursively in a similar way as classical Kalman filter. 
    \begin{align}
      Initialization:&&\hat x_{0| - 1} & = \overline x_0 ,\quad P_{0| - 1}  = \Sigma \\
      Prediction:&&\hat x _{k + 1|k} & = A \hat x _{k|k}  , \quad P_{k + 1|k}  = AP_{k|k} A'  + Q \\
      Correction:&&K_k& = P_{k|k - 1} C' (CP_{k|k - 1} C'  + R)^{ - 1}\\
      &&\hat x _{k|k}& = \hat x _{k|k - 1}  + \alert{\gamma_k} K_k (y_k  - C \hat x _{k|k - 1} ) \\
      &&P_{k|k}^{-1}& = P_{k|k - 1}^{-1}  +  \alert{\gamma_k} C' R^{-1} C. 
    \end{align}
    What about performance?
  \end{frame}

  \begin{frame}{Evolution of Mean Square Estimation Error}
    Rewrite the error covariance matrix as
    \begin{equation}
      P_{k+1}=AP_k A'+Q-\gamma_{k+1} A P_k C'(C P_kC'+R)^{-1}C P_k A',
      \label{eq:basicricattieqn}
    \end{equation}
    where $P_k \triangleq P_{k+1|k}$. $P_k$ is now stochastic. 
    \begin{displaymath}
      \begin{split}
	E P_{k+1}&= E [A P_k A']+ EQ-E[\gamma_{k+1} A P_k C'(C P_kC'+R)^{-1}C P_k A']\\
	&=A (EP_k) A' + Q -E\gamma_{k+1} \times A E[\alert{P_kC'(C P_kC'+R)^{-1}CP_k}] A'\\
	& = ???
      \end{split}
    \end{displaymath}
    In general it is impossible to get a recursive equation of $EP_k$ due to the non-linear nature of Riccati Equation.
  \end{frame}

  \section{Previous Work}

  \begin{frame}{Boundedness of $EP_k$}
    \begin{theorem}
      [Sinopoli, 04] Under i.i.d. packets loss, if $(A,Q^{\frac{1}{2}})$ is controllable, $(A,C)$ is observable, and $A$ is unstable, then there exists a $p_c\in[0,1)$ such that
      \begin{eqnarray}\label{eqn:lambdaCrit}
	\lim_{k\rightarrow\infty}E[P_k]=+\infty & \mbox{for } 0\leq
	p \leq p_c \; \;
	\mbox{and  \hspace{3mm}$\exists P_0 \geq 0$}&\\
	E[P_k] \leq M_{P_0} \;\; \forall k & \mbox{for } p_c <
	p \leq 1 \; \; \mbox{and \hspace{3mm}$\forall P_0 \geq 0$}&
      \end{eqnarray}
      where $M_{P_0} > 0 $  depends on the initial condition $P_0 \geq 0$
    \end{theorem}
    \begin{theorem}
      [Sinopoli, 04]  The critical value $p_c$ of the system is lower bounded by
      \begin{displaymath}
	p_c \geq 1 - |\lambda_1|^{-2}.
      \end{displaymath}
      The lower bound is tight if the system is one-step observable. 
    \end{theorem}
  \end{frame}

  \section{Main Results}
  \begin{frame}{Some Notations}
    \begin{itemize}
      \item Define
	\begin{align*}
	  h(X) \triangleq AXA' + Q,\; g(X) \triangleq h(X) - AXC'(CXC'+R)^{-1}CXA'.
	\end{align*}
	Therefore
	\begin {displaymath}
	P_{k+1} = \left\{ {\begin{array}{*{20}c}
	  {h(P_k)}, &\textrm{if } {\gamma_{k+1}  = 0}  \\
	  {g(P_k)}, &\textrm{if } {\gamma_{k+1}  = 1}  \\
	\end{array}} \right.
      \end {displaymath}
    \item Define \[k_1(k) \triangleq \sup\{j \leq k|\gamma_j = 1\}, k_i(k) \triangleq \sup\{j < k_{i-1}(k)|\gamma_j = 1\},i> 1.\]
  \end{itemize}
  \begin{center}
  \includegraphics{pic.3}
  \end{center}
\end{frame}

\begin{frame}{A Lower Bound of $P_k$}
  \begin{theorem}
    If $(A,Q^{1/2})$ is controllable and $k \geq k_1 + n$, then $trace(P_k) \geq \alpha |\lambda_1|^{2(k-k_1)}$, where $\alpha > 0$ is a constant independent of $\gamma_i$s.
  \end{theorem}
  \begin{proof}
    \[P_k = h^{k-k_1}(P_{k_1})\geq h^{k-k_1}(0).\]
  \end{proof}
  \begin{center}
  \includegraphics{pic.2}
  \end{center}
\end{frame}

\begin{frame}{An Upper Bound of $P_k$ for One-Step Observable Systems}
  \begin{theorem}
    If the system is one-step observable, then $trace(P_k) \leq \beta |\lambda_1|^{2(k-k_1)}$, where $\beta > 0$ is a constant independent of $\gamma_i$s.
  \end{theorem}
  \begin{proof}
       If the system is one-step observable, then $g(X)$ is bounded by $M_0$. Hence
	\[P_k = h^{k-k_1}(P_{k_1}) =  h^{k-k_1}(g(P_{k_1-1}))\leq h^{k-k_1}(M_0).\]
  \end{proof}
  \begin{center}
  \includegraphics{pic.2}
  \end{center}
\end{frame}

\begin{frame}{Observable System: Counter Example}
  \begin{itemize}
    \item For one-step observable systems, $trace(P_k) \sim |\lambda_1|^{2(k-k_1)}$, $p_c = 1-|\lambda_1|^{-2}$.
    \item Observability means that $P_k$ is bounded given $n$ sequential observations, i.e. $g^n(X) < M_0$ for some constant $M_0$.
    \item Can we bound $P_k$ by any $n$ measurements? 
    \item Consider the following system:  
      \[A = \left[ {\begin{array}{*{20}c}
	0 & 2  \\
	2 & 0 
      \end{array}} \right],\,C = \left[ {\begin{array}{*{20}c}
	1 & 0 
      \end{array}} \right],\,Q=\Sigma = I,\,R = 1.\]
  \end{itemize}
  \begin{center}
  \includegraphics{pic.1}
  \end{center}
\end{frame}

\begin{frame}{Observable System: Counter Example}
  \begin{itemize} 
    \item It can be proved that $(A,C)$ is observable. However, $A^2 =4I$ and $(A^2,C)$ is not observable.
    \item Half of the observations contains only contain ``redundant'' information.
      \begin{figure}[h]
	  \begin{center}
	  %\includegraphics{pic.4}
	  \end{center}
      \end{figure}
    \item If the angle between $\lambda_1$ and $\lambda_2$ is $2\pi r/q$, then $A^q = const\times I$. Hence $(A^q,C)$ is not observable if $rank(C) = 1$.
    \item $1/q$ of the observations contains only contain ``redundant'' information.
  \end{itemize}
\end{frame}

\begin{frame}{Observable System: Counter Example}
\begin{theorem}
  For unstable observable systems satisfying:
  \begin{itemize}
    \item  $A = diag(\lambda_1,\lambda_2) $;
    \item $|\lambda_1| = |\lambda_2| > 1$;
    \item  $rank(C) = 1$;
  \end{itemize}
   the critical value of the system is given by
  \begin{equation}
    p_c = 1-|\lambda_1|^{-\dfrac{2}{1-D_M(\varphi/2\pi)}},
  \end{equation}
  where $\varphi$ is the angle between $\lambda_1$ and $\lambda_2$, and $D_M(x)$ is the modified Dirichlet function defined as
  \begin{equation}
    D_M(x) = \left\{ \begin{array}{*{20}l}
      0 & \textrm{for $x$ irrational}\\
      1/q & \textrm{for $x = r/q$, $r,q \in \mathbb Z$ and irreducible.}\\
    \end{array}\right..
  \end{equation}
\end{theorem}
\end{frame}

\section{Non-Degeneracy}
\begin{frame}{Non-Degeneracy}
  \begin{itemize}
    \item One-step observability is usually too strong for real applications.
    \item Observability is too weak since observability may be lost due to random sampling.      
    \item Let $A = diag(\lambda_1,\ldots,\lambda_n)$, $C = [C_1,\ldots, C_n]$.
    \item Consider index set $\mathcal I = \{i_1,\ldots,i_l\}\subseteq \{1,\ldots,n\}$.
    \item Define $A_{\mathcal I} = diag(\lambda_{i_1},\ldots,\lambda_{i_l})$, $C_{\mathcal I} = [C_{i_1},\ldots,C_{i_l}]$.
    \item We call the subsystem $(A_{\mathcal I}, C_{\mathcal I})$ an equiblock if $\lambda_{i_1}=\ldots=\lambda_{i_l}$. 
    \item We call the subsystem $(A_{\mathcal I}, C_{\mathcal I})$ a quasi-equiblock if $|\lambda_{i_1}|=\ldots=|\lambda_{i_l}|$. 
      \begin{definition}
	A system is non-degenerate if every quasi-equiblock $(A_{\mathcal I},C_{\mathcal I})$ is one-step observable.
      \end{definition}
  \end{itemize}
\end{frame}

\begin{frame}{Non-Degeneracy}
  \begin{itemize}
    \item Degenerate:
      \[A = diag (2,-2), C =[1,1]\] 
    \item Non-degenerate:
     \[ A = diag(2,-2,3,-3), C = \left[ {\begin{array}{*{20}c}
  1 & 0 & 1 & 0  \\
  0 & 1 & 0 & 1  \\
\end{array}} \right],\]
    \item A system is observable if every equiblock is one-step observable.
    \item A system is non-degenerate if every quasi-equiblock is one-step observable.
    \item Non-degeneracy is stronger than observability, but weaker than one-step observability. 
  \end{itemize}
  \begin{theorem}
    If the system is non-degenerate, then $(A^k,C)$ is observable for any $k\in \mathbb N$. 
  \end{theorem}
\end{frame}

\begin{frame}{An upper bound of $P_k$ for non-degenerate systems}
  \begin{theorem}
    If the system is non-degenerate, then
    \[trace(P_k) \leq \beta (|\lambda_1|+\varepsilon)^{2(k-k_l)},\] 
    where $\beta > 0$ is a constant independent of $\gamma_i$s, $\varepsilon > 0$ can be arbitrarily small and $l$ is the number of unstable and critically stable eigenvalues.
  \end{theorem}
  $A = diag(4,3,2)$, $C_1 = I_3$, $C_2 = [1,1,1]$.
  \begin{center}
    \includegraphics{pic.3}
  \end{center}
\end{frame}

\begin{frame}{Comparison between One-Step Observable and Non-Degenerate Systems}
\begin{tabular}{ccc}
\hline
 & One-Step Observable & Non-Degenerate \\ \hline 
 \vspace{0.5cm}Lower Bound & $P_k \geq \alpha |\lambda_1|^{2(k-k_1)}$ & $P_k \geq \alpha |\lambda_1|^{2(k-k_1)}$  \\ 
 \vspace{0.5cm}Upper Bound & $P_k \leq \beta |\lambda_1|^{2(k-k_1)}$& $P_k \leq \beta (|\lambda_1|+\varepsilon)^{2(k-k_l)}$    \\ 
 \vspace{0.5cm}Critical Value & $p_c = 1-|\lambda_1|^{-2}$ & $p_c = 1-|\lambda_1|^{-2}$    \\ \hline
\end{tabular}
\end{frame}


\section{Conclusion}

\begin{frame}{Observable System: Counter Examples}
\begin{theorem}
  For unstable observable systems satisfying:
  \begin{itemize}
    \item  $A = diag(\lambda_1,\lambda_2) $;
    \item $|\lambda_1| = |\lambda_2| > 1$;
    \item  $rank(C) = 1$;
  \end{itemize}
   the critical value of the system is given by
  \begin{equation}
    p_c = 1-|\lambda_1|^{-\frac{2}{1-D_M(\varphi/2\pi)}},
  \end{equation}
  where $\varphi$ is the angle between $\lambda_1$ and $\lambda_2$. Otherwise the critical value is
  \begin{equation}
    p_c = 1-|\lambda_1|^{-2}.
  \end{equation}
\end{theorem}
\end{frame}

\begin{frame}{Conclusion}
  \begin{itemize}
    \item We considered the performance of Kalman filtering with intermittent observations.
    \item We provided a new definition of non-degeneracy.
    \item We proved a complete characterization of critical value for second order diagonalizable systems.
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \Huge{ Thank you!\\
    Questions?}
  \end{center}
\end{frame}
\end{document}
