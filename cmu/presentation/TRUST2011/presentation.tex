\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{subfigure}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Secure Detection]{Secure Detection in the Presence of Integrity Attacks}
\author[Jo\~{a}o Hespanha]{Yilin Mo$^*$, Jo\~{a}o Hespanha$^\dag$ and Bruno Sinopoli$^*$}
\institute[UCSB]{
$*$: Department of Electrical and Computer Engineering\\ Carnegie Mellon University\\
$\dag$: Department of Electrical and Computer Engineering\\ University of California, Santa Barbara 
}
\date[ACC 2012]{American Control Conference, 2012}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Cyber Physical Systems (CPS) refer to the embedding of widespread sensing, computation, communication and control into physical spaces.
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. \alert{Safety Critical}
    \item The next generation CPS, such as smart grids, will make extensive use of information technology. 
    \item Stuxnet raised significant concerns on CPS security. 
    \item How to design secure CPS?
  \end{itemize}
\end{frame}

\begin{frame}{A Classical Detection Problem}
  \begin{itemize}
    \item We want to decide whether the state $\theta$ is $1$ or $-1$.
      \begin{align*}
	\theta=\begin{cases}
	  -1 &\text{w.p.~}p^-\\
	  +1 &\text{w.p.~}p^+\\
	\end{cases}
      \end{align*}
    \item $m$ sensors are measuring the system:
      \begin{displaymath}
	y_i = a\theta + v_i.
      \end{displaymath}
    where $a > 0$ is a constant. $v_i$ are i.i.d. Gaussian noise with mean $0$ and variance $1$.
    \item A detector is a function $f:\mathbb R^m\rightarrow \{-1,1\}$.
    \item The optimal detector with minimum detection error (when there is no attacker) is a Naive Bayesian Detector:
      \begin{align*}
	\hat \theta=f(y)=\begin{cases}
	  -1 &\text{if }\sum_{i} y_i < \eta/2a\\
	  +1 &\text{if }\sum_{i} y_i \geq \eta/2a\\
	\end{cases},
      \end{align*}
      where $\eta = \log(p^-/p^+)$.
  \end{itemize}
\end{frame}

\begin{frame}{System Diagram}
  \begin{figure}[<+htpb+>]
    \begin{center}
      \includegraphics[width=0.9\textwidth]{pic.1}
    \end{center}
    \caption{System Diagram}
  \end{figure}
  Suppose the attacker compromise one sensor. Thus, it can manipulate $\sum_i y_i$ arbitrarily. Hence, it has full control over $\hat \theta$. 
\end{frame}

\begin{frame}{Countermeasures}
  \begin{block}{Information Security}
    \begin{itemize}
      \item Tamper-resistant microprocessor, Software attestation, Secure Communication Protocol,\dots
      \item It is hard to guarantee security for every single sensor. (A single compromised sensor can totally ruin the Naive Bayes detector.)
      \item Physical attacks?
    \end{itemize}
  \end{block}
  \begin{block}{System Theory}
    \begin{itemize}
      \item Bad data detection, Robust detection ($\varepsilon-$contamination, bounded total variation difference, \dots)  
      \item The uncertainty models of system theoretic approaches are usually not quite different from the cyber attacks.
    \end{itemize}
  \end{block}
  Our goal: To design the optimal detector that can withstand Byzantine attacks from at most $n$ sensors.
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the detection algorithm $f$ (Kerckhoffs' Principle);
    \item the true state $\theta$;
    \item all measurements $y$.
  \end{itemize}
  The attacker can manipulate up to $n$ measurements arbitrarily.
  \begin{displaymath}
   y' = y + u,\,\|u\|_0\leq n. 
  \end{displaymath}
  The attacker wants to minimize the probability of detection (or maximize probability of error):
  \begin{displaymath}
   P_d(f) = P(f(y')=\theta)
  \end{displaymath}
\end{frame}

\begin{frame}{Attacker's Strategy}
  The optimal strategy for the attacker:
\begin{align*}
	u= \begin{cases}
	  \displaystyle argmin_{\|u\|_0\leq n} f(y+ u) & (\theta =1)\\
	  \displaystyle argmax_{\|u\|_0\leq n} f(y+ u) & (\theta =-1)
	\end{cases}
\end{align*}
\begin{figure}[<+htpb+>]
  \centering
  \subfigure[$\theta = -1$]{
  \includegraphics{pic.2}}
  \subfigure[$\theta = 1$]{
  \includegraphics{pic.3}}
\end{figure}
\end{frame}

\begin{frame}{``Good'' Point}
\begin{figure}[<+htpb+>]
  \centering
  \subfigure[First Detector $f_1$]{
  \includegraphics{pic.4}}
  \subfigure[Second Detector $f_2$]{
  \includegraphics{pic.5}}
\end{figure}
There are three categories of point in $R^m$:
\begin{enumerate}
  \item for all $\|u\|_0\leq n$, $f(y+u)=1$. (\alert{Good})
  \item for all $\|u\|_0\leq n$, $f(y+u)=-1$. (\alert{Good}) 
  \item for some $\|u\|_0\leq n$, $f(y+u)=1$ and for some $\|u\|_0\leq n$, $f(y+u)=-1$. (Bad)
\end{enumerate}
\end{frame}

\begin{frame}{``Good'' sets}
Define
      \begin{align*}
	Y^+(f) &\triangleq \big\{y:f(y+ u)=1, \; \forall \|u\|_0\leq n\big\},\\
	Y^-(f) &\triangleq \big\{y:f(y+ u)=-1, \; \forall \|u\|_0\leq n\big\},
      \end{align*}
The detector can correctly detect $\theta$ if and only if 
\begin{displaymath}
	(\theta,y)\in\Big\{ (-1,y): y\in Y^-(f) \Big\} \cup \Big\{ (+1,y): y\in Y^+(f) \Big\}
\end{displaymath}
As a result
\begin{displaymath}
 P_d(f) = p^-P(y\in Y^-(f)|\theta = -1)+p^+P(y\in Y^+(f)|\theta = 1). 
\end{displaymath}
  If $Y^-(f)\subseteq Y^-(g)$ and $Y^+(f)\subseteq Y^+(g)$, then $P_d(f)\leq P_d(g)$.
\end{frame}

\begin{frame}{$Y^-$ and $Y^+$ for different detectors}
\begin{figure}[<+htpb+>]
  \centering
  \subfigure[First Detector $f_1$]{
  \includegraphics[height=2.7cm, width = 4cm]{pic.8}}
  \subfigure[$Y^-$ and $Y^+$ of $f_1$]{
  \includegraphics[height=2.7cm, width = 4cm]{pic.9}}\\
  \subfigure[Second Detector $f_2$]{
  \includegraphics[height=2.7cm, width = 4cm]{pic.6}}
  \subfigure[$Y^-$ and $Y^+$ of $f_2$]{
  \includegraphics[height=2.7cm, width = 4cm]{pic.7}}
\end{figure}

\end{frame}

\section{Main Results}
\begin{frame}{A Hamming-like distance}
Define metric $d:\mathbb R^m\times \mathbb R^m\rightarrow \mathbb N_0$ as
\begin{displaymath}
 d(x,y)=\|x-y\|_0. 
\end{displaymath}
Let $X,Y$ be two subsets of $\mathbb R^m$, define
\begin{displaymath}
  d(X,Y) = min_{x\in X,y\in Y}d(x,y),\,d(x,Y) = d(\{x\},Y). 
\end{displaymath}
\end{frame}

\begin{frame}{Lemmas}
  \begin{lemma}
    For any detector $f$, $d(Y^-(f),Y^+(f))\geq 2n+1$.
  \end{lemma}
  \begin{lemma}
    Given $X^-,\,X^+$ two subsets of $\mathbb R^m$, and $d(X^-,X^+)\geq 2n+1$, there exists a detector $f$, such that $X^-\subseteq Y^-(f)$ and $X^+\subseteq Y^+(f)$.
  \end{lemma}
\end{frame}

\begin{frame}{Main Theorem}
  \begin{theorem}
    The optimal $f^*$ is of the following form
    \begin{align*}
      f^*(y) = \begin{cases}
	1 & d(y,X^-)\ge d(y,X^+)\\
	-1 & d(y,X^-)< d(y,X^+),
      \end{cases}
    \end{align*}
    where $X^{+}$ and $X^{-}$  are the solutions of the following optimization problem:
    \begin{align*}
      &\mathop{\textrm{maximize}}\limits_{X^+,X^-}&
      & P(y\in X^-|\theta = -1)p^-+P(y\in X^+|\theta = 1)p^+\\
      &\textrm{subject to}&
      & d(X^-,\,X^+)\geq 2n+1
    \end{align*}
  \end{theorem}
      
\end{frame}
\begin{frame}{The structure of the optimal detector}
\begin{figure}[<+htpb+>]
    \begin{center}
  \includegraphics{pic.12}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Main results}
  \begin{itemize}
    \item If $n \geq m/2$, then the optimal detector is $f=1$ or $f = -1$.
    \item The true optimal detector is difficult to compute when $n < m/2$.
    \item As a result, we propose a heuristic detector based on trimmed mean:
      \begin{enumerate}
	\item The detector sorts all the $y_i$s in descending order.
	\item The detector throws away $n$ measurements with the largest $y_i$s and $n$ measurements with the least $y_i$s.
	\item The detector sums the remaining $m-2n$ $y_i$s and compares it to $\eta/2a$. The detector chooses $\hat \theta = -1$ is the truncated sum is less than $\eta/2a$, otherwise the detector chooses $\hat \theta = 1$.
      \end{enumerate}
      It can be proved that the corresponding $X^-$ and $X^+$ are
		\begin{displaymath}
		  \begin{split}
		    X^- &= \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i< \eta/2a,\forall |\mathcal I|=m-2n,\,\mathcal I\subset \{1,\ldots,m\}\},\\
		  X^+& = \{y\in \mathbb R^m:\sum_{i\in\mathcal I}y_i\geq \eta/2a,\forall |\mathcal I|=m-2n,\,\mathcal I\subset \{1,\ldots,m\}\} .
		  \end{split}
		\end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{A Heuristic Detector}
  \begin{itemize}
    \item Suppose we received the following measurements:
      \begin{displaymath}
y_1 = -2,\,y_2 = 5,\,y_3 = 1,\,y_4 = -3,\,y_5 = 0.5.	
      \end{displaymath}
    \item $p^- = p^+ = 0.5$, $a = 1$.
    \item After sorting we have:
      \begin{displaymath}
5,\,1,\,0.5,\,-2,\,-3.	
      \end{displaymath}
    \item If $n = 0$, then we have $5+1+0.5-2-3 =1.5> 0$. Hence $\hat \theta = 1$.
    \item If $n = 1$, then we have $1+0.5-2=-0.5 < 0$. Hence $\hat \theta = -1$.
    \item If $n=2$, then we have $0.5 > 0$. Hence $\hat \theta  = 1$.
  \end{itemize}
\end{frame}

\begin{frame}{Heuristic Detector for $n<m/2$}
  \begin{figure}[<+htpb+>]
    \begin{center}
      \includegraphics[width=0.5\textwidth]{figure4.eps}
    \end{center}
    \caption{Probability of Error v.s. Number of Sensors($m$), where $p^- = p^+ = 0.5$, $a = 1$. The green line: $n = 0$. The red line: $n = 1$. The black line: $n = 2$. The blue line: $n =3$.}
  \end{figure}
  We can prove that the heuristic detector is asymptotically optimal. 
\end{frame}

\begin{frame}{Special Case: $m=2n+1$}
  Although it is very difficult to compute the true optimal detector for general $m$ and $n$, we can prove that the true optimal detector is based on a voting strategy when $m = 2n+1$:
  \begin{enumerate}
    \item The detector compares each measurement $y_i$ with a threshold $\eta_i$ and get a ``local'' estimate $\hat \theta_i$. ($\eta_i$s are chosen to maximize the probability of detection.)
    \item The detector decides $\hat \theta = 1$ if there are more than half of the $\hat \theta_i = 1$. Otherwise it decides $\hat \theta = -1$.
  \end{enumerate}
\end{frame}
\section{Conclusion}
\begin{frame}{Conclusion}
  \begin{itemize}
    \item We present a robust detector design which can withstand integrity attacks on up to $n$ sensors.
    \item In particular, if $n \geq m/2$, then the optimal detector is constant filter $f = 1$ or $f = -1$.
    \item For general $n < m/2$, we propose a heuristic detector design.
    \item For special case of $m = 2n+1$, we prove that the optimal detector is based on a voting scheme.
  \end{itemize}
\end{frame}
\end{document}

\begin{frame}{$m=2n+1$}
  We can enlarge $X^-$ and $X^+$ by the following process:
      \begin{displaymath}
	X^+_{new} = \{y:d(y,X^-)\geq 2n+1\},\,  X^-_{new} = \{y:d(y,X^+_{new})\geq 2n+1\}.
      \end{displaymath}
      When $m = 2n+1$, define 
      \begin{displaymath}
	X_i = \{y_i\in \mathbb R:\textrm{there exists $y\in X^-$ such that the $i$th element of $y$ is $y_i$}\}.
      \end{displaymath}
      Since $X^+_{new} = \{y:d(y,X^-)\geq 2n+1 = m\}$, any $y^+$ from $X^+_{new}$ must satisfies $y_i^+\notin X_i$ for all $i$, which implies 
      \begin{displaymath}
	X^+_{new} = \prod_{i=1}^m \mathbb R\backslash X_i.	
      \end{displaymath}
      Similarly,
      \begin{displaymath}
	X^-_{new} = \prod_{i=1}^m  X_i.	
      \end{displaymath}
\end{frame}

\begin{frame}{$m=2n+1$}
  We only consider $X^-,\,X^+$ of the following form
  \begin{displaymath}
    X^- = \prod_{i=1}^m X_i,\,X^+ = \prod_{i=1}^m\mathbb R\backslash X_i. 
  \end{displaymath}
  The corresponding probability of error can be expressed as
  \begin{displaymath}
    \begin{split}
    P_d(f) &= p^-P(y\in \prod_{i=1}^m X_i|\theta = -1)+p^+P(y\in \prod_{i=1}^m \mathbb R\backslash X_i|\theta = 1) \\
     &= p^-P(y\in \prod_{i=1}^m X_i|\theta = -1)+p^+P(y\in \prod_{i=1}^m \mathbb R\backslash X_i|\theta = 1) \\
    &=p^-\prod_{i=1}^m P(y_i\in X_i|\theta = -1) +p^+  \prod_{i=1}^m P(y_i\notin X_i|\theta = 1) 
    \end{split}
  \end{displaymath}
  The optimal $X_i$ must be of the following form:
  \begin{displaymath}
    X_i = \{y_i\in \mathbb R:y_i\leq \eta_i\} .
  \end{displaymath}
\end{frame}


\begin{frame}{$m=2n+1$}
  It is easy to see that
  \begin{displaymath}
    d(y,X^-) = \sum_{i=1}^m \mathbf 1_{X_i}(y_i),\,d(y,X^+) = \sum_{i=1}^m \mathbf 1_{\mathbb R\backslash X_i}(y_i)
  \end{displaymath}
  The optimal $f^*$ can be computed by a voting scheme:
  \begin{itemize}
    \item The detector computes $m$ estimation $\hat \theta_i$ by a Neymann-Pearson detector based on $y_i$.
    \item The detector decides $\hat \theta = 1$ if no less than $n+1$ $\hat \theta_i = 1$. Otherwise the detector decides $\hat \theta = -1$.
  \end{itemize}
\end{frame}

