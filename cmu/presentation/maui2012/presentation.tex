\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,mindmap,plotmarks}

\usepackage{pgfplots}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}


\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Secure CPS]{Secure Estimation and Control in Cyber-Physical Systems}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{
Control and Dynamical Systems\\ California Institute of Technology\\
}
\date[Sept 3, 2014]{Sept 3, 2014 at Nanyang Technological University\\ \vspace{1cm}
\small Joint Work with Bruno Sinopoli, Richard M. Murray, Sean Weerakkody, Rohan Chabukswar}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette secondary}{fg=caltechcolor!70!black,bg=gray!15!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Cyber-Physical System}
  \begin{itemize}
    \item Cyber-Physical Systems (CPSs) refer to the embedding of computation, communication and control into physical spaces.
      \begin{center}
	\begin{tikzpicture}[scale=0.45,transform shape,level distance=0cm,
	  level 1 concept/.append style={sibling angle=120,minimum size = 3cm},
	  ]
	  \path [draw=caltechcolor!50,fill=caltechcolor!20,thick,rounded corners] (-10,-4.5) rectangle (10,7);
	  \node at (-9,6) [anchor=north west] {\Huge Physical Space};
	  \path[mindmap,concept color=black,text=white]
	  node[concept] {\Huge CPS}
	  [clockwise from=330]
	  child[concept color=green!50!black] { node[concept](communication) {\huge Comm} }
	  child[concept color=red] { node[concept](control) {\huge Control} }
	  child[concept color=blue] { node[concept](computation) {\huge Comp} };
	\end{tikzpicture}
      \end{center}
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. 
  \end{itemize}
\end{frame}

\begin{frame}{Security Threats for the CPS}
  \begin{itemize}
    \item Currently, CPSs are usually running in dedicated networks.
    \item However, the next generation CPS, such as Smart Grids, VANet, Smart Buildings, will make extensive use of widespread networking.
    \item As the CPSs are more interconnected and ``smarter'', they also become more vulnerable to malicious attacks. 
    \item Reasons to attack CPS: financial gain, cyber warfare, terrorism\dots 
    \item Stuxnet, the first malware that spies on and subverts CPS, was discoverd in June 2010, which provides a clear sample for the future to come. 
  \end{itemize}
\end{frame}

\begin{frame}{Cyber-Physical Security v.s. Cyber Security}
  \begin{itemize}
    \item Cyber security protects the confidentiality, integrity and availability of information.
    \item Tools: Encryption, Digital Signature, Tamper Resistant Device, Software Authentication, etc.
    \item Cyber security is a tool for CPS security, not necessary a goal.
    \item Limitations of cyber security based methods:
      \begin{itemize}
	\item Lack of the model of the physical system
	\item Difficult to guarantee security for every single device
	\item Zero-day attacks: Stuxnet contains 4 zero-day vulnerabilities of Windows. 
	\item Physical attacks
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Cyber-Physical Security v.s. System Theory}
  \begin{itemize}
    \item System theory based approaches:
      \begin{itemize}
	\item Fault detection and identification
	\item Robust methods
	\item Game theory 
	\item \dots
      \end{itemize}
    \item Limitations of system theory based methods:
      \begin{itemize}
	\item Lack of the model of the cyber system
	\item The failure and uncertainty models are usually quite different from attack models: An intelligent adversary could design its attack to optimally exploit some vulnerability of the system.
      \end{itemize}
    \item System theory based approaches need to be reexamined in the CPS security context. 
  \end{itemize}
\end{frame}

\begin{frame}{Cyber-Physical Security}
  \begin{center}
    \begin{tikzpicture}[scale=0.65,transform shape,level distance=0cm,level 1 concept/.append style={sibling angle=120}]
      \path[mindmap,concept color=black,text=white]
      node[concept] {\Large CPS Security}
      [clockwise from=330]
      child[concept color=green!50!black] {
      node[concept] {System Theory}
      }
      child[concept color=red] { node[concept] {Cyber Security} }
      child[concept color=blue] { node[concept] {Modeling} };
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Cyber-Physical Security: Modeling}
  \begin{center}
    \begin{tikzpicture}[>=latex]
      \draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
      \node at (0,0) {Physical System};
      \draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
      \node at (0,2) {Cyber Infrastructure};
      \draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
      \node at (0,4) {Estimation/Control Algorithm};
      \draw [thick,->] (-2,3.5)--(-2,2.5);
      \draw [thick,->] (-2,1.5)--(-2,0.5);
      \draw [thick,<-] (2,3.5)--(2,2.5);
      \draw [thick,<-] (2,1.5)--(2,0.5);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Cyber-Physical Security: Modeling}
  \begin{center}
    \begin{tikzpicture}[>=latex]
      \draw [draw=green!50,fill=green!20,thick] (-4.5,1) rectangle (4.5,5.5);
      \node at (0,5) {Cyber Security};
      \draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
      \node at (0,0) {Physical System};
      \draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
      \node at (0,2) {Cyber Infrastructure};
      \draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
      \node at (0,4) {Estimation/Control Algorithm};
      \draw [thick,->] (-2,3.5)--(-2,2.5);
      \draw [thick,->] (-2,1.5)--(-2,0.5);
      \draw [thick,<-] (2,3.5)--(2,2.5);
      \draw [thick,<-] (2,1.5)--(2,0.5);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Cyber-Physical Security: Modeling}
  \begin{center}
    \begin{tikzpicture}[>=latex]
      \draw [draw=green!50,fill=green!20,thick] (-4.5,3) rectangle (4.5,-1.5);
      \node at (0,-1) {$x(k+1) =Ax(k)+Bu(k),\,y(k) = Cx(k)$};
      \draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
      \node at (0,0) {Physical System};
      \draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
      \node at (0,2) {Cyber Infrastructure};
      \draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
      \node at (0,4) {Estimation/Control Algorithm};
      \draw [thick,->] (-2,3.5)--(-2,2.5);
      \draw [thick,->] (-2,1.5)--(-2,0.5);
      \draw [thick,<-] (2,3.5)--(2,2.5);
      \draw [thick,<-] (2,1.5)--(2,0.5);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Cyber-Physical Security: Modeling}
  \begin{center}
    \begin{tikzpicture}[>=latex,
box/.style={rectangle, draw,rounded corners, thick}
      ]
      \draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-5,-0.5) rectangle (5,0.5);
      \draw [draw=blue!50,fill=blue!20,thick] (-5,1.5) rectangle (5,2.5);
      \draw [draw=red!50,fill=red!20,thick] (-5,3.5) rectangle (5,4.5);
      \node (agc1) [box] at (-3,4) {AGC};
      \node (agc2) [box] at (3,4) {AGC};
      \node (c1) [box] at (-3,2) {Computer};
      \node (c2) [box] at (3,2) {Computer};
      \node (r) [box] at (0,2) {Router};
      \node (g1) [box] at (-3,0) {Generator};
      \node (g2) [box] at (3,0) {Generator};
      \node (l1) [box] at (-1,0) {Load};
      \node (l2) [box] at (1,0) {Load};
      \draw [thick,<->] (agc1) to (c1);
      \draw [thick,<->] (agc2) to (c2);
      \draw [thick,<->] (c1) to (r);
      \draw [thick,<->] (c2) to (r);
      \draw [thick,<->] (c1) to (g1);
      \draw [thick,<->] (c2) to (g2);
      \draw [thick,<->] (g1) to (l1);
      \draw [thick,<->] (g2) to (l2);
      \draw [thick,<->] (l1) to (l2);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Cyber-Physical Security: System Theory}
     We cannot take the security of the system as granted.
  \begin{itemize}
    \item How to model the attack?
    \item How to quantify the security level of the CPS?
    \item How to analyze if the existing algorithms are secure?
    \item How to design new secure algorithms?
    \item How to detect and identify malicious nodes in the system?
    \item How to enable graceful degradation when the system is compromised? 
    \item \dots
  \end{itemize}
\end{frame}

\section{Physical Authentication and Watermarking}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Stuxnet}
  \begin{itemize}
    \item NY times:``The worm itself now appears to have included two major components. One was designed to send Iran's nuclear centrifuges spinning wildly out of control. Another seems right out of the movies: The computer program also \alert{secretly recorded what normal operations at the nuclear plant looked like, then played those readings back to plant operators}, like a pre-recorded security tape in a bank heist, so that it would appear that everything was operating normally while the centrifuges were actually tearing themselves apart.''
    \item We published the following paper: ``\emph{Secure control against replay attacks}'' in Sept, 2009, while the worm was first discovered in June, 2010. 
  \end{itemize}
\end{frame}

\begin{frame}{System Model}
  \begin{block}{System Description}
      \begin{displaymath}
	\begin{split}
	  x(k+1) &= Ax(k)  + Bu(k)+w(k),\\
	  y(k) &= C x(k) + v(k).
	\end{split}
      \end{displaymath}
    \end{block}
    \begin{itemize}
      \item $x(k) \in \mathbb R^n$ is the state vector.
      \item  $y(k) \in \mathbb R^m$ is the measurements from the sensors.
      \item  $u(k) \in \mathbb R^p$ is the control input.
      \item $w(k),v(k),x(0)$ are independent Gaussian random vectors, and $x(0) \sim \mathcal N(0,\;\Sigma)$, $w(k) \sim \mathcal N(0,\;Q)$ and $v(k) \sim \mathcal N(0,\;R)$.
      \item The system is assumed to be controllable and observable.
    \end{itemize}
  \end{frame}

  \begin{frame}{LQG Controller and Kalman filter}
    \begin{itemize}
      \item We assume that the system operator wants to minimize the following cost function:
	\begin{displaymath}
	  J = \lim_{T\rightarrow \infty}\min_{u(0),\ldots,u(T)}E\frac{1}{T}\left[\sum_{k=0}^{T-1} x(k)'Wx(k)+u(k)'Uu(k)\right],
	\end{displaymath}
	where $W, U$ are positive semidefinite matrices. 
      \item The optimal controller is a fixed gain controller, which takes the following form:
	\begin{displaymath}
	  u(k) =  L^*\hat x(k),
	\end{displaymath}
      \item The optimal estimator (Kalman filter) follows the following update equations:
	\begin{align*}
	  \hat x(k+1|k) &= A\hat x(k)+Bu(k),\\
	  \hat x(k+1) &= \hat x(k+1|k) + K^*\left[y(k+1) - C\hat x(k+1|k)\right],
	\end{align*}
    \end{itemize}
  \end{frame}


  \begin{frame}{$\chi^2$ Failure Detector}
    \begin{itemize}
      \item The residue $z(k)$ of the Kalman filter is defined as
	\begin{displaymath}
	  z(k) \triangleq y(k) - C\hat x(k|k-1),
	\end{displaymath} 
	which is i.i.d. Gaussian distributed with zero mean. 
      \item $\chi^2$ detector triggers an alarm based on the following event:
	\begin{displaymath}
	  g(k)=  z(k)^T\mathcal P^{-1}z(k)> threshold,
	\end{displaymath}
	where $\mathcal P$ is the covariance matrix of $z(j)$ and $\mathcal T$ is the window size.
      \item The alarm rate at time $k$ is 
	\begin{displaymath}
	  \beta(k) = P(g(k) > threshold).
	\end{displaymath}
	If the system is operating normally, then the alarm rate is a constant, which we denote as the false alarm rate $\alpha$.
    \end{itemize}
  \end{frame}

  \begin{frame}{System Diagram}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{systemdiagram}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Replay Attack Model}
    \begin{enumerate}
      \item The attacker can read and modify all the measurements $y(k)$ arbitrarily.
      \item It can inject an external control input $u^a(k)$ into the system. 
    \end{enumerate}
    The strategy of the attacker can be divided into two stages:
    \begin{enumerate}
      \item The attacker records a sufficient number of $y(k)$s without injecting any control input $u^a(k)$. 
      \item The attacker injects a sequence of desired control input $u^a(k)$ while replaying the previous recorded $y(k)$s starting from time $0$.
    \end{enumerate}
    When the system is under attack, the controller will be unable to perform closed-loop control. Hence the only way to counter this attack is to detect its presence. 
  \end{frame}

  \begin{frame}{Replay Attack: First Stage}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramone}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Replay Attack: Second Stage}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramtwo}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Feasibility of Replay Attacks}
    \begin{figure}[htpb]
      \setlength\figureheight{4.5cm}
      \setlength\figurewidth{10cm}
      \begin{center}
	\inputtikz{replayunstableA2}
      \end{center}
    \end{figure}
    Replay attack is not always feasible!
  \end{frame}

  \begin{frame}{Feasibility of Replay Attacks}
    \begin{figure}[htpb]
      \begin{center}
	\inputtikz{replaydiagramthree}
      \end{center}
    \end{figure}
  \end{frame}

  \begin{frame}{Feasibility of Replay Attacks}
    The update equation of Kalman filter follows:
    \begin{align*}
      \hat{x}(k+1|k)&=A\hat{x}(k)+Bu(k)=\left(A+BL^*\right)\hat{x}(k)\\
      &=\left(A+BL^*\right)\left(I-K^*C\right)\hat{x}(k|k-1)+\left(A+BL^*\right)K^*y(k).
    \end{align*}

    \begin{theorem}
      If $\mathcal A = (A+BL^*)(I-K^*C)$ is stable, then the detection rate of $\chi^2$ detector $\beta^c(k)$ converges to the false alarm rate $\alpha$ during the attack, i.e.,
      \begin{displaymath}
	\lim_{k\rightarrow\infty}\beta^c(k) = \alpha.  
      \end{displaymath}
      On the other hand, if $\mathcal A$ is strictly unstable, then the detection rate $\beta^c(k)$ converges to $1$, i.e.,
      \begin{displaymath}
	\lim_{k\rightarrow\infty}\beta^c(k) = 1.  
      \end{displaymath}
    \end{theorem}
  \end{frame}

  \begin{frame}{Countermeasures: Unstable $\mathcal A$}
    \begin{itemize}
      \item We could choose estimation and control gain $K$ and $L$, such that $A+BL$ and $A-KCA$ are stable, while rendering $\mathcal A$ strictly unstable.
      \item The LQG cost of using $K$ and $L$ during normal operation is 
	\begin{displaymath}
	  J=\tr\left(\left[ {\begin{array}{*{20}c}
	    W+L^TUL & -L^TUL  \\
	    -L^TUL & L^TUL  \\
	  \end{array}} \right] \tilde{X}\right),
	\end{displaymath}
	where $\tilde X$ is the solution of the following Lyapunov equation:
	\begin{displaymath}
	  \tilde{X}=\tilde{A}\tilde{X}\tilde{A}^T+\tilde{Q},
	\end{displaymath}
	where
	\begin{displaymath}
	  \tilde A \triangleq  \left[{\begin{array}{*{20}c}
	    A+BL&-BL\\
	    0&A-KCA 
	  \end{array}}\right]
	\end{displaymath}
	\begin{displaymath}
	  \tilde Q \triangleq \left[{\begin{array}{*{20}c}
	    I&0\\
	    I-KC&-K
	  \end{array}}\right] \left[{\begin{array}{*{20}c}
	    Q&0\\
	    0&R
	  \end{array}}\right]\left[{\begin{array}{*{20}c}
	    I&0\\
	    I-KC&-K
	  \end{array}}\right]^T. 
	\end{displaymath}
    \end{itemize}
  \end{frame}

  \begin{frame}{Countermeasures: Authentication and Watermarking}
    \begin{itemize}
      \item Challenge-response authentication is widely in cyber security, where one party presents a question ("challenge") and another party must provide a valid answer ("response") to be authenticated.
      \item Similarly, we could change the control law by adding a watermarking signal:
	\begin{displaymath}
	  u(k) = L^*\hat x(k) + \xi(k).
	\end{displaymath}
      \item The watermarking signal $\xi(k)$ acts as a ``challenge'' and the sensor measurement is the ``response''. 
      \item During normal operation, the ``challenge'' and ``response'' are correlated through the system dynamics, while the correlation cease to exist when the replay begins.
    \item The CPS will remain stable. However, we sacrifice LQG performance since the control is not optimal.
      \item How to design the ``optimal'' watermarking signal?
    \end{itemize}
  \end{frame}

\begin{frame}{Authentication and Watermarking}
  \begin{center}
    \begin{tikzpicture}[>=latex]
      \draw [draw=caltechcolor!50,fill=caltechcolor!20,thick] (-4,-0.5) rectangle (4,0.5);
      \node at (0,0) {Physical System};
      \draw [draw=blue!50,fill=blue!20,thick] (-4,1.5) rectangle (4,2.5);
      \node at (0,2) {Cyber Infrastructure};
      \draw [draw=red!50,fill=red!20,thick] (-4,3.5) rectangle (4,4.5);
      \node at (0,4) {Estimation/Control Algorithm};
      \node [anchor=east] at (-2,3) {Challenge};
      \node [anchor=west] at (2,3) {Response};
      \draw [thick,->] (-2,3.5)--(-2,2.5);
      \draw [thick,->] (-2,1.5)--(-2,0.5);
      \draw [thick,<-] (2,3.5)--(2,2.5);
      \draw [thick,<-] (2,1.5)--(2,0.5);
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Countermeasures: Authentication and Watermarking}
  \begin{itemize}
    \item We restrict the watermarking signal $\xi(k)$ to be a zero-mean stationary Gaussian process, with autocovariance function $\Gamma(d)$:
      \begin{displaymath}
	\Gamma(d) \triangleq \mathbb E \xi(k)\xi^T(k+d).
      \end{displaymath}
    \item The additional LQG cost incurred by $\xi(k)$ is a linear functional of $\Gamma$.
      \begin{displaymath}
	\text{Additional LQG cost} = \mathcal L_1(\Gamma). 
      \end{displaymath}
    \item The KL divergence between ``compromised'' residue $z^c(k)$ and ``normal'' residue $z(k)$ is a convex functional of $\Gamma$, which satisfies
      \begin{displaymath}
	\frac{1}{2}\mathcal L_2(\Gamma) \leq D_{KL}(z^c(k)||z(k))\leq \mathcal L_2(\Gamma)-\frac{1}{2}\log(1+\mathcal L_2(\Gamma)),
      \end{displaymath}
      where $\mathcal L_2$ is a linear functional of $\Gamma$. 
  \end{itemize}
\end{frame}

\begin{frame}{Countermeasures: Authentication and Watermarking}
  We consider maximizing the ``relaxed'' KL divergence, while satisfying LQG performance constraint.
  \begin{align*}
    &\mathop{\textrm{maximize}}\limits_{\Gamma}&
    & \mathcal L_2(\Gamma)\\
    &\textrm{subject to}&
    & \mathcal L_1(\Gamma) \leq \Delta J\\
    &&& \Gamma \text{ is an autocovariance function}
  \end{align*}
\end{frame}

\begin{frame}{Countermeasures: Authentication and Watermarking}
  \begin{theorem}
    The optimal autocovariance function $\Gamma_*$ is given by:
    \begin{equation}
      \Gamma_*(d) = \exp(2\pi j d \omega_*) H_* +\overline{ \exp(2\pi j d \omega_*) H_*}.
      \label{eq:optimalcovariance}
    \end{equation}
    where $\omega_*\in [0,0.5]$ and $H_* = hh^H$ is a PSD matrix of rank one. The corresponding watermarking signal $\xi(k)$ is a hidden Markov process 
    \begin{displaymath}
      \begin{split}
	\theta(k+1) &= A_\omega \theta(k),\,\theta(0)\sim \mathcal N(0,I_2)\\
	\xi(k) &= C_h \theta(k),
      \end{split}
    \end{displaymath}
    where
    \begin{displaymath}
      A_\omega\triangleq \left[ \begin{array}{cc}
	\cos 2\pi\omega_* & -\sin2\pi\omega_* \\
	\sin2\pi\omega_* & \cos2\pi\omega_* \\
      \end{array} \right], C_h \triangleq  \left[ \begin{array}{cc}
	\sqrt{2}h_r&\sqrt{2}h_i
      \end{array} \right],
    \end{displaymath}
    where $h_r$ and $h_i$ are the real and imaginary part of $h$ respectively.
  \end{theorem}
\end{frame}

\begin{frame}{Countermeasures: Authentication and Watermarking}
  \begin{itemize}
    \item $\omega_*$ and $H_*$ are the solutions of the following problem:
      \begin{align*}
	&\mathop{\textrm{maximize}}\limits_{\omega,H}&
	& \mathcal F_2(\omega,H) \\
	&\textrm{subject to}&
	& \mathcal F_1(\omega,H)\leq \Delta J,\\
	&&&0 \leq \omega\leq 0.5,\\
	&&& H \text{ Hermitian and PSD.}
      \end{align*}
      where $\mathcal F_1,\,\mathcal F_2$ are linear with respect to $H$. 
    \item For a fixed $\omega$, the optimal $H$ can be derived using semidefinite programming.
    \item The optimal $\omega$ can then be found by searching over all $\omega$ in $[0,0.5]$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Unstable $\mathcal A$}
  \begin{itemize}
    \item We consider the following system
      \begin{align*}
	x(k+1) &=\left[{\begin{array}{*{20}c}
	  1&1\\
	  0&1
	\end{array}}\right]  x(k) + \left[{\begin{array}{*{20}c}
	  0\\
	  1
	\end{array}}\right] u(k) + w(k),\\
	y(k) &= \left[{\begin{array}{*{20}c}
	  1& 0
	\end{array}}\right] x(k) + v(k),
      \end{align*}
      where $Q = W = I$ and $R = U = 1$, $\alpha = 1\%$
    \item The optimal estimation, control gain matrices and LQG cost are
      \begin{displaymath}
	K^* =  \left[{\begin{array}{*{20}c}
	  0.8218\\
	  0.4221
	\end{array}}\right] ,\,L^* =  \left[{\begin{array}{*{20}c}
	  -0.4221&-1.2439
	\end{array}}\right] ,\, J^* = 27.7818.
      \end{displaymath}
      The eigenvalues of the corresponding $\mathcal A^*$ matrix are $-0.0773,-0.4105$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Unstable $\mathcal A$}
  \begin{itemize}
    \item To render $\mathcal A$ unstable while maintaining stability of the CPS, we choose the following $K$ and $L$:
      \begin{displaymath}
	K =  \left[{\begin{array}{*{20}c}
	  0.9822\\
	  0.5973
	\end{array}}\right] ,\,L =  \left[{\begin{array}{*{20}c}
	  -0.5973&-1.5794
	\end{array}}\right].
      \end{displaymath}
    \item The LQG cost is $J = 42.5746$  
    \item The eigenvalues of the corresponding $\mathcal A$ matrix are $-1.1586,\,-0.0003$.
    \item The eigenvalues of $A+BL$ are $0.3727,0.0479$ and the eigenvalues of $A-KCA$ are $0.3727,0.0479$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Unstable $\mathcal A$}
  \begin{figure}[htpb]
    \setlength\figureheight{4.5cm}
    \setlength\figurewidth{10cm}
    \begin{center}
      \inputtikz{replayunstableA}
    \end{center}
    \caption{Detection rate at each time step for stable $\mathcal A$ (blue) and unstable $\mathcal A$ (red)}
    \label{fig:betavsa}
  \end{figure}
\end{frame}

\begin{frame}{Numerical Example: Physical Authentication}
  \begin{figure}[htpb]
    \setlength\figureheight{4.5cm}
    \setlength\figurewidth{10cm}
    \begin{center}
      \inputtikz{betavscost}
    \end{center}
    \caption{Asymptotic Single Step Detection rate v.s. Incremental LQG cost}
    \label{fig:betavscost}
  \end{figure}

\end{frame}
\section{Secure Estimation}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Canonical Estimation Problem}
  \begin{itemize}
    \item The goal is to estimate the state $x\in \mathbb R$ with $m$ sensor measurements $y = [y_1,\dots,y_m]'$.
      \begin{center}
	\begin{tikzpicture}
	  [place/.style={circle,draw=caltechcolor!50,fill=caltechcolor!20,thick}]
	  \node (s1) at (0,1.8) [place] {$S_1$};
	  \node (s2) at (0,0.2) [place] {$S_2$};
	  \node (sm) at (0,-1.8) [place] {$S_m$};
	  \node (fc)  at (3,0) [place] {$FC$};
	  \node (result)  at (6,0) {$\hat x = f(y)$};
	  \draw [semithick,->] (s1)-- node[midway,above]{$y_1$}(fc);
	  \draw [semithick,->] (s2)-- node[midway,above]{$y_2$}(fc);
	  \draw [semithick,->] (sm)-- node[midway,above]{$y_m$}(fc);
	  \draw [semithick,->] (fc)--(result);
	  \node  at (0,-0.8) {$\vdots$};
	\end{tikzpicture}
      \end{center}
    \item The optimal estimator with minimum Mean Squared Error (MSE) is:
      \begin{align*}
	\hat x=f(y)=\mathbb E(x|y).
      \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{Estimation Problem: the Gaussian Case}
  \begin{itemize}
    \item Assume that 
      \begin{displaymath}
	y_i = x + v_i,
      \end{displaymath}
      where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$.
    \item The optimal estimator (in the MSE sense)
      \begin{displaymath}
	\hat x=	f(y) = \frac{1}{m+1}\sum_{i=1}^m y_i.
      \end{displaymath}
    \item Suppose one sensor measurement is compromised by an adversary. Then the adversary can change $\hat x$ to an arbitrary value.
    \item On the other hand, the median is a more secure estimate, since it will remain bounded when less than half of the sensors are malicious:
      \begin{displaymath}
	\hat x = \Med(y).	
      \end{displaymath}
    \item Given a desired level of ``security'', how to find the ``optimal'' estimator? 
  \end{itemize}
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the estimation algorithm $f$ (Kerckhoffs' Principle);
    \item the true state $x$;
    \item all measurements $y$.
  \end{itemize}
  The attacker can manipulate up to $l$ measurements arbitrarily.
  \begin{displaymath}
    y^c = y + \gamma\circ y^a,
  \end{displaymath}
  where the \emph{sensor-selection} vector $\gamma$ takes value in 
  \begin{displaymath}
    S_\gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i = l\}
  \end{displaymath}

  The attacker wants to maximize the MSE while the estimator wants to minimize the MSE. 
\end{frame}

\begin{frame}{The Optimal Attack Policy}
  \begin{itemize}
    \item The attacker would either make $\hat x$ as large as possible or as small as possible (depending on $x$).
    \item To compute the worst-case MSE, let us define
      \begin{align*}
	f^+(y) &\triangleq \sup_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a),\\
	f^-(y) &\triangleq \inf_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a).
      \end{align*}
    \item Consider $f = (y_1+y_2+y_3)/3$ and $l = 1$, then 
      \begin{displaymath}
	f^+(y) = + \infty,\;f^-(y) = -\infty.
      \end{displaymath}
    \item Consider $f = \Med(y_1,y_2,y_3)$ and $l = 1$, $y_1=1,y_2=2,y_3=3$ then 
      \begin{displaymath}
	\begin{split}
	  f^+(y) &= \Med(\alert{4},2,3) = 3 = \Max(y_1,y_2,y_3),\\
	  f^-(y) &=  \Med(1,2,\alert{0}) = 1 =\Min(y_1,y_2,y_3).
	\end{split}
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{The Optimal Attack Policy}
  \begin{itemize}
    \item Suppose that the true measurement is $y$, then the attack can only change the state estimation $\hat x = f(y^c)$ in the interval $[f^-(y),f^+(y)]$. 
    \item The optimal attack policy is to manipulate the state estimate $\hat x$ to either $f^+(y)$ or $f^-(y)$, depending on which one is further away from the true state $x$. 
      \vspace{0.5cm}
      \begin{center}
	\begin{tikzpicture}
	  \draw [stealth-stealth,thick] (-3,0.4)--(3,0.4);
	  \draw[-stealth,thick] (-5,0)--(5,0);
	  \draw (-3,0.5)--(-3,-2pt) node [anchor=north]{\alert{$f^-(y)$}};
	  \draw (3,0.5)--(3,-2pt) node [anchor=north]{$f^+(y)$};
	  \draw (1,2pt)--(1,-2pt) node [anchor=north]{$x$};
	  \draw (0,2pt)--(0,-2pt) node [anchor=north]{$f(y)$};
	\end{tikzpicture}
      \end{center}
    \item Not ``energy'' constrained: 0-norm constrained
    \item Not bounded: the compromised measurements can be arbitrarily large
    \item Not independent: the compromised measurements are jointly optimized
    \item Not random: the compromised measurements are chosen to cause maximal damage 
  \end{itemize}
\end{frame}

\begin{frame}{The Worst-Case MSE}
  Define $e(x,y) \triangleq \max \left( (f^+(y)-x)^2,(f^-(y)-x)^2 \right)$.
  The worst-case MSE is define as
  \begin{displaymath}
    \MSE(f) \triangleq \int_{\mathbb R^{m+1}}e(x,y)d\mu = \mathbb Ee(x,y). 
  \end{displaymath}
  \begin{theorem}
    For two estimators $f_1$ and $f_2$, if the following inequalities hold
    \begin{displaymath}
      f^+_1(y) \geq f^+_2(y)\geq f^-_2(y)\geq f^-_1(y), \,\forall y.
    \end{displaymath}
    then
    \begin{displaymath}
      \MSE(f_1)\geq \MSE(f_2). 
    \end{displaymath}
  \end{theorem}
  \begin{center}
    \begin{tikzpicture}
      \draw [stealth-stealth,thick] (-1,0.2)--(1,0.2);
      \draw [stealth-stealth,thick] (-3,0.4)--(3,0.4);
      \draw[-stealth,thick] (-5,0)--(5,0);
      \draw (-3,0.5)--(-3,-2pt) node [anchor=north]{$f_1^-$};
      \draw (-1,0.3)--(-1,-2pt) node [anchor=north]{$f_2^-$};
      \draw (1,0.3)--(1,-2pt) node [anchor=north]{$f_2^+$};
      \draw (3,0.5)--(3,-2pt) node [anchor=north]{$f_1^+$};
    \end{tikzpicture}
  \end{center}
\end{frame}

\begin{frame}{Optimal Estimator: $l\geq m/2$}
  \begin{theorem}
    If $l\geq m/2$, then the optimal estimator $f^*$ is given as $f^* = \mathbb Ex$.
  \end{theorem}
  If the attacker compromises half of the sensors, then the optimal estimator will depend only on the a-prior information.
\end{frame}


\begin{frame}{Optimal Symmetric and Monotone Estimator: $l< m/2$}
  We consider the case where less than half of the sensors are compromised.

  \begin{definition}[Symmetry]
    An estimator $f$ is symmetric if for any permutation $({i_1},\dots,{i_m})$ of $(1,\dots,m)$ and any $y = [y_1,\dots,y_m]^T$, the following equality holds:
    \begin{displaymath}
      f(y_1,\dots,y_m) = f(y_{i_1},\dots,y_{i_m}).
    \end{displaymath}
  \end{definition}
  \begin{definition}[Monotone]
    An estimator $f$ is monotonically increasing if for any $y = [y_1,\dots,y_m]^T$ and $y'= [y_1',\dots,y_m']^T$ such that $y_i\geq y_i'$, for $i = 1,\dots,m$, the following inequality holds:
    \begin{displaymath}
      f(y_1,\dots,y_m) \geq f(y_{1}',\dots,y_{m}').
    \end{displaymath}
    An estimator $f$ is monotone if either $f$ is monotonically increasing or $-f$ is monotonically increasing.
  \end{definition}
\end{frame}


\begin{frame}{Optimal Symmetric and Monotone Estimator: $l< m/2$}
  \begin{definition}
    Define the function $\Med_{l}:\mathbb R^{m}\rightarrow \mathbb R^{m-2l}$ as a symmetric function, which satisfies
    \begin{align}
      \Med_l(y_1,\ldots,y_m) &\triangleq (y_{l+1},\ldots,y_{m-l}),
    \end{align}
    when $y_1\leq \dots\leq y_m$.
  \end{definition}
  \begin{itemize}
    \item Since we assume that $\Med_l$ is symmetric, we only need to define it for $y_1\leq \dots\leq y_m$.
    \item The $\Med_l$ function can be seen as removing the largest $l$ measurements and the smallest $l$ measurements. In particular, if $m =2l+1$, then $\Med_l(y)$ is just the median of $y$. 
  \end{itemize}
\end{frame}

\begin{frame}{Optimal Symmetric and Monotone Estimator: $l< m/2$}
  \begin{theorem}
    The optimal symmetric and monotone estimator $f$ is of the following form
    \begin{displaymath}
      f(y) =  \varphi(\Med_l(y)),
    \end{displaymath}
    where $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ is a symmetric and monotone function.
  \end{theorem}
\end{frame}

\begin{frame}{Optimal Estimator: $l< m/2$}
  \begin{itemize}
    \item The optimal symmetric and monotone estimator would throw away $2l$ measurements.
    \item Without symmetry and monotonicity, it is unclear which $2l$ measurements should be discarded by the estimator.
    \item Consider a subset $\mathcal K \subset \{1,\dots,m\}$ with cardinality $|\mathcal K|=2l$. 
    \item Denote $\varphi_{\mathcal K}(y)$ as a local estimator based on $m-2l$ measurements by removing the measurements in $\mathcal K$. For example, 
      \begin{displaymath}
	\varphi_{\{1,2\}}(y_1,y_2,y_3,y_4) = \frac{y_3+y_4}{2}.
      \end{displaymath}
    \item There are in total $m\choose{2l}$ local estimator $\varphi_{\mathcal K}$s.
  \end{itemize}
\end{frame}

\begin{frame}{Optimal Estimator: $l< m/2$}
  \begin{itemize}
    \item Let $m=4,\,l=1$.
    \item $y = [0.5,\, 1.8,\,-2.3,\,0.9]'$.
    \item Suppose that we design the local estimator $\varphi_{\mathcal K}$ to be the algebraic average, i.e.,
      \begin{displaymath}
	\varphi_{\mathcal K}(y) =\frac{1}{m-2l} \sum_{i\notin \mathcal K}y_i.
      \end{displaymath}
    \item We get ${4\choose{2}}=6$ estimates :
      \begin{align*}
	\varphi_{\{1,2\}} &= -0.7,\, \varphi_{\{1,3\}} = 1.35,\, \varphi_{\{1,4\}} = -0.25,\\
	\varphi_{\{2,3\}} &= 0.7,\, \varphi_{\{2,4\}} = -0.9,\, \varphi_{\{3,4\}} = 1.15.
      \end{align*}
    \item Which one to pick from $(-0.9,-0.7,-0.25,0.7,1.15,1.35)$?
  \end{itemize}
\end{frame}

\begin{frame}{Optimal Estimator: $l<m/2$}
  Consider two index sets $\mathcal I,\mathcal J$ with size $l=1$. We list all 
  \begin{displaymath}
    \varphi_{\mathcal K}(y) = \varphi_{\mathcal I\bigcup \mathcal J}(y)
  \end{displaymath}
  in the following table: 
  \begin{table}[h]
    \centering
    \begin{tabular}{c|cccc} \hline
      & $\mathcal J = \{1\}$  & $\mathcal J = \{2\}$ & $\mathcal J = \{3\}$ & $\mathcal J = \{4\}$  \\ \hline
      $ \mathcal I = \{1\}$&    &-0.7 &\alert{1.35 }  &-0.25\\ 
      $ \mathcal I = \{2\}$&-0.7 &    &\alert{0.7 }&-0.9\\ 
      $ \mathcal I = \{3\}$&\alert{1.35 }  &0.7 &    &1.15\\ 
      $ \mathcal I = \{4\}$&-0.25 & -0.9  &\alert{1.15}& \\\hline 
    \end{tabular}
  \end{table}
  The estimator $f$ should be of the following form:
  \begin{displaymath}
    f = \min_{\mathcal I}\left[ \max_{\mathcal J,\mathcal J\bigcap \mathcal I =\emptyset} \varphi_{\mathcal I\bigcup\mathcal J}(y) \right] = \min(1.35,0.7,1.35,1.15) = 0.7.
  \end{displaymath}
\end{frame}

\begin{frame}{Optimal Estimator: $l< m/2$}
  \begin{theorem}
    The optimal estimator $f$ is of the following form
    \begin{displaymath}
      f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],
    \end{displaymath}
    where $\varphi_{\mathcal K},\,|\mathcal K|=2l$ are the solutions of the following optimization problem:
    \begin{displaymath}
      \mathop{\text{minimize}}\limits_{\varphi_{\mathcal K}}\;\; \mathbb E \left\{\max_{|\mathcal K| = 2l} \left[x - \varphi_{\mathcal K} (y)\right]^2\right\}
    \end{displaymath}
  \end{theorem}
\end{frame}

\begin{frame}{Optimal Estimator: $l< m/2$}
  \begin{itemize}
    \item The objective function:
      \begin{displaymath}
	\mathop{\text{minimize}}\limits_{\varphi_{\mathcal K}}\;\; \mathbb E \left\{\max_{|\mathcal K| = 2l} \left[x - \varphi_{\mathcal K} (y)\right]^2\right\}
      \end{displaymath}
      is a convex functional of $\varphi_\mathcal K$.
    \item Suppose we restrict each $\varphi_{\mathcal K}$ to be in a finite dimensional space (e.g., all polynomials of degree less than $n$):
      \begin{displaymath}
	\varphi_{\mathcal K} = a_{\mathcal K,1}g_1+\ldots+ a_{\mathcal K,n}g_n
      \end{displaymath}
    \item Since the objective function is convex with respect to the coefficient $a_{\mathcal K,i}$. The optimal coefficient $a_{\mathcal K,i}$ can be derived via convex optimization.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: the Gaussian Case}
  \begin{itemize}
    \item We specialize our results for the Gaussian case, with $m = 3$ and $l = 1$.
    \item We assume
      \begin{displaymath}
	y_i = x+ v_i,\,i=1,2,3, 
      \end{displaymath}
      where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$.
    \item The optimal $\varphi$ in the space of polynomials of degree less than $5$ is
      \begin{displaymath}
	\varphi_{\{1,2\}}(y) = 0.29 y_3 ,\;  \varphi_{\{2,3\}}(y) = 0.29 y_1 ,\;  \varphi_{\{3,1\}}(y) = 0.29 y_2 .
      \end{displaymath}
      The optimal estimator and MSE are 
      \begin{displaymath}
	f(y) = \Med_1(0.29y_1,0.29y_2,0.29y_3),\,\MSE(f) = 0.9. 
      \end{displaymath}
    \item When there is no attack, the optimal estimator and MSE are
      \begin{displaymath}
	f(y) = (y_1+y_2+y_3)/4,\,\MSE(f) = 0.25. 
      \end{displaymath}
    \item The MSE of a constant estimator $f =\mathbb Ex= 0$ is $\MSE(f) = 1$.
    \item There is a large degradation of estimation performance when $1$ sensor is compromised.
  \end{itemize}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion: Towards a Science of Cyber-Physical Security}
  \begin{itemize}
    \item Security of cyber-physical systems is of paramount importance
    \item A science of CPS security needs to be developed
    \item System theory is a powerful tool and will play an important role in CPS security.
    \item New challenges:
      \begin{itemize}
	\item Include security metrics within design goals
	\item Introduce realistic attack models
	\item Develop new risk models, analysis and design tools
	\item \dots
      \end{itemize}
    \item New Opportunities: System theory can be used to solve problems in cyber security, e.g., physical authentication. 
  \end{itemize}
\end{frame}

\begin{frame}
  \begin{center}
    \Huge{Thank You!}
  \end{center}
\end{frame}

\end{document}
