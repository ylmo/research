In this section we will illustrate the efficiency of the sensor selection strategy with some numerical examples.\\
Throughout this section we will consider a system composed of $m$ sensors from which only $p$ of them can be selected at each time. The sensors measurements are used to estimate a dynamical system consisting of $n=20$ states. We want to minimize the average estimation error, i.e. we assume $\mathcal Q_k = I, \, k = 1,\ldots, T$. The matrices $A,\,C$ are chosen randomly with each entry independently and uniformly distributed on $[0,1]$. The matrix $Q$ is computed by multiplying a randomly generated matrix with its transpose (to make them positive definite), where each entry of the random matrix is also independently and uniformly distributed on $[0,1]$. We choose $\Sigma = I_n$.

\subsection{One step sensor selection strategy with uncorrelated measurements}

In this subsection we will show that our algorithm, in addition to allowing multi-step sensor selection and being able to account for correlated sensor measurements, outperforms existing methods for single step sensor selection with uncorrelated sensor measurements.  In order to compare our sensor selection strategy with other approaches proposed in the literature, we consider the problem of minimizing next step estimation error, i.e. $T = 1$, and we also assume uncorrelated noise, i.e. $R$ is a random generated diagonal matrix. To show the quality of our sensor selection strategy, we first compare it with the real optimal sensor selection scheme. The comparison is evaluated in terms of $trace(P_{k|k})$.\\
The optimal sensor selection strategy is evaluated with an exhaustive search of the solution set, which is composed by $\binom mp$ elements. It is straightforward to recognize that it can be computed just for small networks. In the first example we will assume to have a WSN of $m=30$ nodes and $1 \leq p\leq 10$. For each value of $p$ we consider the estimation performance both of our strategy and a random strategy and we normalize them respect to the optimal one. The relative difference is shown in Figure~\ref{fig:performance_gap}.
\begin{figure}
   \begin{center}
   \includegraphics[width=2.7in]{performance_gap.eps}
    \caption{Relative estimation performance gap of the proposed sensor selection strategy (red solid line) and the random strategy (blue dashed line) with respect to the optimal strategy.} \label{fig:performance_gap}
   \end{center}
\end{figure}
Figure \ref{fig:performance_gap} clearly illustrates that the proposed sensor selection strategy assures very good performance, within $1\%$ of the true optimal, for all values of $p$ while the performance of the random method tends to make worse when the number of selected sensor increases.
The comparative analysis is limited by the complexity of computing the optimal solution. For the case of $p=10$ and $m=30$ the space of the possible solution is in the order of $10^7$ and it takes hours to find the real optimal solution.\\
We next compare our algorithm to the one proposed in \cite{Joshi} for a WSN of $m=50$ nodes\footnote{In this comparison we do not implement the local optimization algorithm proposed in \cite{Joshi} to improve the sensor selection performance, since it is a general technique that can be applied to any sensor selection schemes, including ours.}
In Figure \ref{fig:boyd_comp} we plot the performance gap between the proposed algorithm and the one in \cite{Joshi}. In particular, for each value of $p$, we evaluate the estimation performance of both the approaches over a time horizon of $T_h$ steps by applying the one step sensor selection method step by step. To achieve a statistic performance analysis, we average the simulation results over $50$ random generated systems for each $p$. In Figure \ref{fig:boyd_comp} we show the performance gap of the strategy in \cite{Joshi} with respect to our sensor selection strategy for $T_h=1,4,7,10$.\\
\begin{figure}
   \begin{center}
   \includegraphics[width=2.8in]{boyd_comp3.eps}
    \caption{Relative estimation performance gap of the sensor selection strategy in \cite{Joshi} with respect to the proposed strategy.} \label{fig:boyd_comp}
   \end{center}
\end{figure}
Figure \ref{fig:boyd_comp} shows that our sensor selection strategy performs better than the one in \cite{Joshi} for all the values of $p$. Moreover the gap increases with the time horizon $T_h$ and it is approximately $33\%$ in case of $T_h=10$ and $p=1$. However, it is important to recognize that our sensor selection method is computationally more intensive than the one in \cite{Joshi}. In particular, the number of arithmetic operations for the algorithm in \cite{Joshi} is $O(m^3)$ while for our one step sensor selection, is $O(n^{2.5}m^{2.5})$.\\
Finally, we want to remark that the one step sensor selection strategy with uncorrelated measurements represents just a possible application of the proposed approach.
We do not perform any comparison for either the multi-step case or the one with correlated noises as the optimal one is practically computationally infeasible and we are unaware of any other method in the literature. 
%In the mean time, differently from the other approaches proposed in the literature, it allows us to achieve the sensor selection problem also in case of correlated noise or in case of multi-step analysis.

\subsection{Multi-step sensor selection: a ``divide and conquer'' approach}
In theorem \ref{theorem:multigain} we bounded the performance loss due to dividing the horizon $T$ into smaller intervals and performing sensor scheduling over each of them. We consider the problem of minimizing the estimation error over a window size of $4$ steps, i.e. we assume to minimize $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$. We wish to compare the solution of problem $(P_0)$ for $T=4$ with a greedy algorithm that solves $(P_0)$ over subintervals $T'=1$ and $2$ until the time window $T=4$ is reached.
%   On one hand, traditional way to solve this problem is using a greedy approach. A greedy algorithm first finds the sensor schedule at time $1$ which only minimize $trace(P_{1|1})$. Then, based on this schedule, it computes all the initial conditions at time $2$ and then finds the schedule for time $2$ which minimize $trace(P_{2|2})$. On the other hand, our algorithm can solve the multi-step sensor selection in one shot considering the coupling among measurements made at different time.
We assume a WSN of $m=10$ nodes is used to monitor a system of dimension $n=10$ and we select $p=2$ sensor each step.
In figure \ref{fig:multistep} we show the value of the objective function $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$ solving the optimization problem in subintervals of $T=1,2,4$.
\begin{figure}
   \begin{center}
   \includegraphics[width=2.8in]{multistep.eps}
    \caption{Value of the objective function $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$ solving the optimization problem in subintervals of $T=1$, $T=2$ or $T=4$.} \label{fig:multistep}
   \end{center}
\end{figure}
The simulation results show that the performance loss is not significant, confirming that the ``divide and conquer'' approach can be a suitable approximation to the multi-step approach for large time horizons.

% It can be seen that increasing the window size $T$ the estimation performance tends to be better. However, as stated in Theorem \ref{theorem:multigain}, the increment is not linear in $T$.
