
In this section we will illustrate the efficiency of the sensor selection strategy with some numerical examples.\\
Throughout this section we will consider a system composed of $m$ sensors from which only $p$ of them can be selected at each time. The sensors measurements are used to estimate a dynamical system consisting of $n$ states. We assume to minimize the average estimation error, hence we impose that $\mathcal Q_k = I, \, k = 1,\ldots, T$.

\subsection{Examples: Temperature Monitoring}

Let us consider a numerical example in which a sensor network is deployed to monitor the temperature in a planar closed region.
We model the heat process in the region of side $l$ as
\begin{equation}
	\frac{\partial u}{\partial t} = \alpha \left(\frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2}\right),
	\label{eq:diffussionpdfcont}
\end{equation}
with boundary conditions
\begin{equation}
  \left.\frac{\partial u}{\partial x_1}\right|_{t,0,x_2}= \left.\frac{\partial u}{\partial x_1}\right|_{t,l,x_2}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,0}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,l}= 0,
\end{equation}
where  $x_1,x_2$ indicate the coordinates of the region; $u(t,x_1,x_2)$ denotes the temperature at time $t$ at location $(x_1,x_2)$ and $\alpha$ indicates the speed of the diffusion process.
In \cite{NecSys09_Mo} the model has been discretized in space with a $N \times N$ grid and in time with frequency of $1$ Hz yielding the following linear discrete time model
\[
U_{k+1}=AU_k+w_k\,,
\]
where the vector $U_k \in \mathbb R^{N^2}$ indicates the temperature at each point of the grid, $A\in \mathbb R^{N^2 \times N^2}$ takes into account the diffusion process and $w_k$ is the process noise.

Let us assume that $m$ sensors are randomly distributed in the region and each sensor measures a linear combination of the temperature of the grid around it (see \cite{NecSys09_Mo}) so that the measurement equation becomes
\[
Y_{k}=C U_{k} + v_k\,,
\]
where $Y_k \in \mathbb R^{m}$ is the measurement vector, $v_k$ is the measurement noise and $C \in \mathbb R^{m\times N^2}$ is defined in  \cite{NecSys09_Mo}.

For the simulations, we impose $\alpha = 0.1 \; m^2/s$, $l=4\; m$ and $N=3$ (hence the grid size is $h=2 \;m$). Moreover, we assume the WSN is composed by $m=20$ nodes. In this section we consider the problem of minimizing the next step estimation error, i.e. $T = 1$, and we also assume uncorrelated noise with identity covariance matrix both for the process and the measurement noise. To show the quality of our sensor selection strategy, we compare it in terms of $trace(P_{k|k})$ with the real optimal sensor selection scheme.
The optimal sensor selection strategy is evaluated with an exhaustive search of the solution set, which is composed by $\binom mp$ elements. It is straightforward to recognize that it can be computed just for small networks. We assume that  $1 \leq p\leq 20$ and, for each value of $p$, we consider the estimation performance both of our strategy and a random strategy and we normalize them with respect to the optimal one. We average the simulation results for each $p$ over $50$ random placements of the sensors in the planar region. In Figure~\ref{fig:performance_gap}, we show the performance gap for $1 \leq p\leq 20$.
\begin{figure}
   \begin{center}
   \includegraphics[width=3.2in]{performance_gap.eps}
    \caption{Average relative estimation performance gap of the proposed sensor selection strategy (red solid line) and the random strategy (blue dashed line) with respect to the optimal strategy.} \label{fig:performance_gap}
   \end{center}
\end{figure}
The figure clearly illustrates that the proposed sensor selection strategy assures near optimal performance for all values of $p$ with a gap that rapidly decreases to zero as $p$ increases. On the other hand, the performance of the random method reaches a maximum gap of $20\%$ compared to the optimal strategy and it is always sensibly lower than the performance of proposed strategy.

\subsection{Comparison to state of the art}

In this section we consider again the minimization problem of the next step estimation error with uncorrelated noise and we compare our algorithm to the one proposed in \cite{Joshi} for a WSN of $m=50$ nodes\footnote{In this comparison we do not implement the local optimization algorithm proposed in \cite{Joshi} to improve the sensor selection performance, since it is a general technique that can be applied to any sensor selection schemes, including ours.}. To perform the comparison we consider random generated systems with $n=20$ states where $A,\,C$ are chosen randomly with each entry independently and identically distributed (iid) on $[0,1]$. The matrix $Q$ is computed by multiplying a randomly generated matrix with its transpose (to make them positive definite), where each entry is also iid on $[0,1]$. We choose $\Sigma = I_n$.
In Figure \ref{fig:boyd_comp} we plot the performance gap between the proposed algorithm and the one in \cite{Joshi}. In particular, for each value of $p$, we evaluate the estimation performance of both approaches over a time horizon of $T_h$ steps by applying the one step sensor selection method step by step. To achieve a statistic performance analysis, we average the simulation results over $50$ random generated systems for each $p$. In Figure \ref{fig:boyd_comp} we show the performance gap of the strategy in \cite{Joshi} with respect to our sensor selection strategy for $T_h=1,4,7,10$.\\
\begin{figure}
   \begin{center}
   \includegraphics[width=3.2in]{boyd_comp3.eps}
    \caption{Relative estimation performance gap of the sensor selection strategy in \cite{Joshi} with respect to the proposed strategy.} \label{fig:boyd_comp}
   \end{center}
\end{figure}
Figure \ref{fig:boyd_comp} shows that our sensor selection strategy performs better than the one in \cite{Joshi} for all the values of $p$. Moreover the gap increases with the time horizon $T_h$ and it is approximately $33\%$ in case of $T_h=10$ and $p=1$. However, it is important to recognize that our sensor selection method is computationally more intensive than the one in \cite{Joshi}. In particular, the number of arithmetic operations for the algorithm in \cite{Joshi} is $O(m^3)$ while for our one step sensor selection, is $O(n^{2.5}m^{2.5})$.\\
Finally, we want to remark that the one step sensor selection strategy with uncorrelated measurements represents just a possible application of the proposed approach.
We do not perform any comparison for either the multi-step case or the one with correlated noises as the optimal one is practically computationally infeasible and we are unaware of any other method in the literature.
%In the mean time, differently from the other approaches proposed in the literature, it allows us to achieve the sensor selection problem also in case of correlated noise or in case of multi-step analysis.

\subsection{Multi-step sensor selection: a ``divide and conquer'' approach}
In theorem \ref{theorem:multigain} we bounded the performance loss due to dividing the horizon $T$ into smaller intervals and performing sensor scheduling over each of them. We consider the problem of minimizing the estimation error over a window size of $4$ steps, i.e. we assume to minimize $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$. We wish to compare the solution of problem $(P_0)$ for $T=4$ with a greedy algorithm that solves $(P_0)$ over subintervals $T'=1$ and $2$ until the time window $T=4$ is reached.
%   On one hand, traditional way to solve this problem is using a greedy approach. A greedy algorithm first finds the sensor schedule at time $1$ which only minimize $trace(P_{1|1})$. Then, based on this schedule, it computes all the initial conditions at time $2$ and then finds the schedule for time $2$ which minimize $trace(P_{2|2})$. On the other hand, our algorithm can solve the multi-step sensor selection in one shot considering the coupling among measurements made at different time.
We assume a WSN of $m=10$ nodes is used to monitor a random generated system of dimension $n=10$ and we select $p=2$ sensor each step.
In figure \ref{fig:multistep} we indicate the performance gap, in terms of $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$, solving the optimization problem in subintervals of $T=1,2,4$, normalized to the case of $T=4$.
\begin{figure}
   \begin{center}
   \includegraphics[width=3.2in]{multistep.eps}
    \caption{Relative gap of $trace(P_{1|1} + P_{2|2}+P_{3|3}+P_{4|4})$ of solving the optimization problem in subintervals of $T=1$, $T=2$ or $T=4$, normalized to the case of $T=4$.} \label{fig:multistep}
   \end{center}
\end{figure}
The simulation results show that the performance loss is quasi linear and not significant, confirming that the ``divide and conquer'' approach can be a suitable approximation to the multi-step approach for large time horizons.

% It can be seen that increasing the window size $T$ the estimation performance tends to be better. However, as stated in Theorem \ref{theorem:multigain}, the increment is not linear in $T$.
