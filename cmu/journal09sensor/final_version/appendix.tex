In order to prove Theorem \ref{theorem:multigain}, we need some preliminary lemmas.\\
Let us define the following functions:
\begin{align*}
g(X,S) = X+S  \quad,\quad  h(X,S) = (X^{-1}+S)^{-1}
\end{align*}
where $X,S \in \mathbb R^{n\times n}$.
\begin{lem} \label{Lemma2}
  Suppose that $X,Y \in \mathbb R^{n\times n}$ are positive semidefinite and $X \geq Y$, then \footnote{All the comparisons between matrices in this section are in the positive semidefinite sense}
  \begin{align*}
  g(X,S) \geq g(Y,S) \quad ,\quad h(X,S) \geq h(Y,S).
  \label{eq:ghincreas}
  \end{align*}
\end{lem}
\begin{pf}
  The proof easily follows from the definition of positive semidefinite matrix.
\end{pf}

\begin{lem} \label{Lemma3}
  Assume $X,S \in \mathbb R^{n\times n}$ are positive semidefinite and let $\alpha>0$ be a scalar such that $S \geq \alpha X$. Then
  \begin{equation}
    g( (1+\rho) X,S) \leq (1+\frac{\rho}{1+\alpha})g(X,S) \quad \forall \rho>0 \,.
  \label{eq:ginequality}
  \end{equation}
\end{lem}
\begin{pf}
  \begin{align*}
    &g( (1+\rho) X,S) - (1+\frac{\rho}{1+\alpha})g(X,S)=\\
    &(1+\rho)X+S - (1+\frac{\rho}{1+\alpha})(X+S)=\frac{\rho(\alpha X - S)}{1+\alpha} \leq 0 \,.
  \end{align*}
\end{pf}

\begin{lem}\label{Lemma4}
  Assume $X,S \in \mathbb R^{n\times n}$ are positive semidefinite. Then
  \begin{equation}
  h((1+\rho) X,S) \leq (1+\rho) h(X,S), \forall \rho > 0\,.
  \label{eq:hinequality}
  \end{equation}
\end{lem}
\begin{pf}
  Since $X,S$ are both positive semidefinite, we know that there exists a matrix $U \in \mathbb R^{n\times n}$ that can diagonalize $X^{-1}$ and $S$ simultaneously, i.e.
  \begin{equation}
    X^{-1} = U^{-1} (U^{-1})^T,\;S = U^{-1}\Lambda (U^{-1})^T ,
  \end{equation}
  where $\Lambda = diag(\lambda_1,\ldots,\lambda_n)$ and $\lambda_i \geq 0$. Hence
  \begin{align*}
&  h((1+\rho) X,S) - (1+\rho) h(X,S)\\
& = U^T\left\{\left[(1+\rho)^{-1}I + \Lambda\right]^{-1} - (1+\rho)(I+\Lambda)^{-1}   \right\}U \\
& = U^T diag(\ldots,\frac{1+\rho}{1+(1+\rho)\lambda_i}-\frac{1+\rho}{1+\lambda_i},\ldots) U\,.
\end{align*}
It is easy to see that the diagonal matrix in the equation above is negative semidefinite. Thus,
  \begin{align*}
  h((1+\rho) X,S) \leq (1+\rho) h(X,S), \forall \rho > 0\,.
  \end{align*}
\end{pf}
We are now ready to prove Theorem~\ref{theorem:multigain}.
\begin{pf}
 Due to space limits, we will only prove the theorem for $N = 2$ as the proof for arbitrary $N$ follows the same reasoning. Let us define a third sensor scheduling $S_3$ as:
  \begin{align*}
  & S_3 [1 \;,\; T]= S_2 [1 \;,\; T] \\
  & S_3 [T+1 \;,\; 2T]= S_1 [T+1 \;,\; 2T]
  \end{align*}
  We denote the estimation error covariance of schedule $S_3$ at time $k$ to be $P_{k|k}(S_3)$.\\
By definition of the sensor scheduling $S_2$, we know that from time $1$ to $T$ it represents the optimal scheduling, which means that
\begin{align}
  \sum_{k=1}^T trace( P_{k|k}(S_3)) &= \sum_{k=1}^T trace(P_{k|k}(S_2)) \nonumber \\ &\leq \sum_{k=1}^T trace(P_{k|k}(S_1)).
  \label{eq:windowone}
\end{align}
Moreover, from time $T+1$ to time $2T$, the schedule $S_2$ is the optimal for initial condition $P_{T|T}(S_2)$. Since the schedule $S_3$ has the same initial condition $P_{T|T}(S_3) = P_{T|T}(S_2)$, we have
\begin{equation}
  \sum_{k=T+1}^{2T} trace(P_{k|k}(S_3)) \geq \sum_{k=T+1}^{2T} trace(P_{k|k}(S_2)).
  \label{eq:windowtwo}
\end{equation}
Combining \eqref{eq:windowone} and \eqref{eq:windowtwo}, we get
\begin{align*}
   &\sum_{k=1}^{2T}trace( P_{k|k}(S_2)) - \sum_{k=1}^{2T} trace(P_{k|k}(S_1))\\
   &\leq  \sum_{k=T+1}^{2T} trace(P_{k|k}(S_3))-\sum_{k=T+1}^{2T} trace(P_{k|k}(S_1)).
\end{align*}
Starting at time $T$, the prediction at time $T+1$ is
\begin{align*}
  P_{T+1|T}(S_3) &= AP_{T|T}(S_3)A^T + Q = g(AP_{T|T}(S_3)A^T, Q)\\
  P_{T+1|T}(S_1) &= AP_{T|T}(S_1)A^T + Q = g(AP_{T|T}(S_1)A^T, Q).
\end{align*}
Hence, by Lemmas \ref{Lemma2} and \ref{Lemma3},
\begin{align*}
  P_{T+1|T}(S_3) &= g(AP_{T|T}(S_3)A^T, Q)\\
  &  \leq g( (1+\rho) AP_{T|T}(S_1)A^T, Q)  \\
  & \leq(1+\frac{\rho}{1+\alpha})g(AP_{T|T}(S_1)A^T,Q)\\
  &=(1+\frac{\rho}{1+\alpha})P_{T+1|T}(S_1),
\end{align*}
where $\rho$ and $\alpha$ are defined in \eqref{rho} and \eqref{alpha}.
At time $T+1$, $S_1$ and $S_3$ choose the same sensors. Suppose the corresponding measurement is
\begin{equation}
  y_{T+1} = C_{T+1}x_{T+1} + v_{T+1},
\end{equation}
where $v_{T+1} \sim \mathcal N(0,R_{T+1})$. Using information filter, we can write the measurement update at time $T+1$ as
\begin{align*}
  P_{T+1|T+1}(S_3) &= \left[(P_{T|T}(S_3))^{-1} + C_{T+1}^TR_{T+1}^{-1}C_{T+1}\right]^{-1}\\& = h( P_{T+1|T}(S_3), C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  P_{T+1|T+1}(S_1) &= h(P_{T+1|T}(S_1), C_{T+1}^TR_{T+1}^{-1}C_{T+1})
\end{align*}
Hence, from Lemmas \ref{Lemma2} and \ref{Lemma4},
\begin{align*}
  &P_{T+1|T+1}(S_3)  = h(P_{T+1|T}(S_3), C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
 % &\leq h( (1+\frac{\rho}{1+\alpha})P_{T+1|T}(S_1), C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  &\leq (1+\frac{\rho}{1+\alpha})h( P_{T+1|T}(S_1), C_{T+1}^TR_{T+1}^{-1}C_{T+1})\\
  &= (1+\frac{\rho}{1+\alpha}) P_{T+1|T+1}(S_1)\,.
\end{align*}
By induction, we know that
\begin{align}
  P_{T+k|T+k}(S_3)\leq \left[1+\frac{\rho}{(1+\alpha)^k}\right]&P_{T+k|T+k}(S_1)\,, \nonumber \\
  & k=1,\ldots,T .
\end{align}
Hence,
\begin{align*}
  &\sum_{k=T+1}^{2T} trace(P_{k|k}(S_3))-\sum_{k=T+1}^{2T} trace(P_{k|k}(S_1))\leq \\
  & \sum_{k=1}^{T} \frac{\rho}{(1+\alpha)^k}trace(P_{T+k|T+k}(S_1))\leq \sum_{k=1}^{T} \frac{\rho \beta}{(1+\alpha)^k} \leq \frac{\rho\beta}{\alpha},
\end{align*}
which concludes the proof.
\end{pf}
