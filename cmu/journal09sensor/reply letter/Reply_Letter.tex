\documentclass[10pt,oneside,a4paper]{letter}

\usepackage{amsfonts}

%% ----------------------------------------------------------------
%\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
%\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
%% THEOREMS -------------------------------------------------------
%\newtheorem{thm}{Theorem}[section]
%\newtheorem{cor}[thm]{Corollary}
%\newtheorem{lem}[thm]{Lemma}
%\newtheorem{prop}[thm]{Proposition}
%\theoremstyle{definition}
%\newtheorem{defn}[thm]{Definition}
%\theoremstyle{remark}
%\newtheorem{rem}[thm]{Remark}
%\numberwithin{equation}{section}
%% MATH -----------------------------------------------------------
%\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
%\newcommand{\abs}[1]{\left\vert#1\right\vert}
%\newcommand{\set}[1]{\left\{#1\right\}}
%\newcommand{\Real}{\mathbb R}
%\newcommand{\eps}{\varepsilon}
%\newcommand{\To}{\longrightarrow}
%\newcommand{\BX}{\mathbf{B}(X)}
%\newcommand{\A}{\mathcal{A}}
% ----------------------------------------------------------------
\begin{document}

\signature{Yilin Mo, Roberto Ambrosino, Bruno Sinopoli}

\Large
%
\begin{center} Answers to AE's and reviewers' comments\\
\bigskip
\large Paper Number: 09-0502\\
\bigskip
\today
\end{center}
\normalsize
\bigskip
\textbf{Title:} Sensor Selection Strategies for State Estimation in Energy Constrained
Wireless Sensor Networks

\textbf{Authors:} Yilin Mo, Roberto Ambrosino, Bruno Sinopoli\\
\medskip



We are grateful to the AE and to the Reviewers for their constructive critics. We have thoroughly
revised the paper taking into account all their remarks.

In the following we summarize the major changes.

\begin{itemize}
\item In order to analyze the tightness of the bounds in Theorem 10, a detailed discussion has been added after Theorem 10, as requested by Reviewer $\#2$.
\item A real world example concerning the monitoring problem of the temperature in a
planar closed region has been added, as requested by Reviewer $\#2$.
\item The reservations about novelty of the paper made by Reviewer $\#5$ has been addressed.
\end{itemize}

In the following we discuss in details the issues raised by the
Reviewers.

\newpage
\noindent {\bf Reviewer \# 2}

\begin{itemize}
\item {\em{Point 1: LQG problem}} \\The system equation has been added in the revisited version of the paper.

    \medskip

    \item {\em Point 2: Theorem 10}\\
    Assuming that the system under analysis is stable, which is reasonable for most of the real monitoring problems, we could find bounds for the parameter $\alpha,\,\beta,\,\rho$, which are independent on $T$ and hence will not diverge as $T$ goes to infinity. This concept was already described in the previous version of the paper, after Theorem $10$. However, to clarify the result, it has been modified as follows

  \textit{" Let $S_*$ be the schedule that selects all the sensors and $S_{\phi}$  be the schedule that selects no sensors. For any schedule $S$, we know that
 \begin{equation}
	P_{k|k}(S_*) \leq P_{k|k}(S) \leq P_{k|k}(S_{\phi})	\,.\nonumber
 \end{equation}
 If the system is detectable, then $P_{k|k}(S_*)$ converges. Moreover if we assume that the system is stable also $P_{k|k}(S_\phi)$ converges.
 It means that $P_{k|k}(S_*)$ and $P_{k|k}(S_\phi)$ are uniformly bounded and hence there exists two positive semidefinite matrices $\overline P$ and $\underline P$, such that
      \begin{displaymath}
	\underline P\leq P_{k|k}(S_*)\leq P_{k|k}(S) \leq P_{k|k}(S_\phi)\leq \overline P.
      \end{displaymath}
Since the above inequality is independent of both schedule $S$ and time index $k$, we can conclude that $\alpha,\,\beta,\,\rho$ are independent of the real window size $T$. Moreover, the loss incurred when dividing the whole window to $N$ parts will always be sublinear with respect to $N$."}
    \medskip

    \item {\em Point 3.1: Real world example }\\ A real world example concerning the monitoring problem of the temperature in a planar region has been added. In particular, the example section has been divided in three parts. The first is devoted to the analysis of real word example with a comparison among our sensor selection method, a random selection method and the real optimal sensor selection scheme. The second part is now devoted to the comparison with the literature. For this second part,we decided to leave the statistical analysis with random generated systems to make the comparison results more general. The third part, as in the previous version of the paper, is dedicated to the multi-step analysis.

    \item {\em Point 3.2: Figure 1}\\ We agree with the reviewer that Figure 1 could appear little bit confusing. In particular, the performance gap does not always increase with the number of selected sensors but the gap reaches a maximum value when we pick more or less half of the sensors and then it starts to decrease (obviously it is zero when we choose all the sensors). However, as we already wrote in the paper, the computational effort needed to find the optimal solution is very high and it was numerically hard to conclude the analysis with a network of $m=30$ sensors. For this reason, in the revisited version of the paper we have repeated the same analysis for the real word example assuming a wireless sensor network of m=20 nodes. It this way the meaning of the figure appears clear.

    \item {\em Point 4: Minor typos} \\
        \begin{enumerate}
        \item The sentence \textit{"As a result..."} has been changed in \\
        \textit{"In this paper, the formulation $(P_0')$ is also a combinatorial problem since the last constraint of $(P_0')$ contains an $L_0$ norm."}
        \item[2. 3. 4.]  Done
        \end{enumerate}

\end{itemize}


\newpage
\noindent {\bf Reviewer \# 5}


\begin{itemize}
\item {\em{Point 1: Novelty of the paper}} \\
    Sensor selection strategies are one of the typical method used to reduce the energy consumption in a WSN while guaranteeing certain estimation performance. To the best of our knowledge, the deterministic sensor selection strategies proposed until now choose the transmitting sensors in each step of time according to an objective function related to the one step estimation performance.

    The main novelty of the paper is, first of all, to find the solution of a multistep sensor selection problem with correlated measurement noise. On one hand, given a fixed window of time, the multistep approach is able to improve the estimation performance respect to the step-by-step strategy (as clearly illustrated both theoretically in the main section and by simulation in the example section); on the other hand, the correlated measurement noise assumption allows us to consider sensor selection problems that were unsolvable until now.\\

    To tackle this problem, the innovative concepts introduced in the paper have been:
    \begin{itemize}
    \item a general formulation ($P_0$) of the sensor selection problem which can be applied to several scenarios both in terms of sensor selection problems and network constraints.  This formulation has been particularly appreciated by Reviewer $5$.
    \item a closed form for the multistep Kalman filter equations that directly relates the estimation of the state of the system at time $k$ to the measurements from $1$ to $k$.
    \item manipulations of the objective function which lead to an explicit expression of sensor selection variables. This step is fundamental to apply the relaxation procedure presented in Section $3.2$.
    \item a condition to quantify the benefits of the multistep approach and the loss of performance of the "divide and conquer" approach
    \end{itemize}

    Finally, the importance of the example section must be underlined. In fact, in this section we not only show the performance of the multistep approach, but we also focus our attention on the performance of the proposed strategy in case of one step sensor selection with uncorrelated measurements. This is the only case already considered in the literature and we show the our method performs better than the other presented in the literature and our solution is near to the optimal one. \\
    Moreover, according to Reviewer $2$ and the AE suggestions, we have introduced a real world example to analyze the effectiveness of our results (see the answer to Reviewer $2$). Due to this new content, the Introduction has been reduced to stay within the limits of 10 pages and an additional reduction of the length of the paper could in our opinion effect the meaning of the paper.

\item {\em Point 2: Minor points} \\
        \begin{enumerate}
        \item Given a matrix $A$, we indicate with $A^H$ the conjugate transpose (also called Hermitian transpose) of $A$. However, taking into account that all the matrices are assumed real,  the conjugate transpose is equal to the transpose. Hence, as suggested by the reviewer, we substitute the symbol $^H$ with $^T$.
        \item The comma in equation (16) has been removed
        \item Done
        \item The figure has been enlarged
        \item The work by Vijay Gupta et al. has been added to the references. Moreover the following sentence has been added to the Introduction:\\
            \textit{"In [6], Gupta et al. propose a stochastic sensor selection algorithm to minimize the expected steady state estimation error covariance."}
        \end{enumerate}

\end{itemize}

\end{document}
% ----------------------------------------------------------------
