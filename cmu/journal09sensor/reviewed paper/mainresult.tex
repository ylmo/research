This section is devoted to manipulating and relaxing the optimization problem $(P_0)$ to make it explicit and convex with respect to the optimization variables.
\subsection{Reformulation}

In this subsection, we will derive the relation between $P_{k|k}$ and $\vec \gamma$ explicitly. First let us consider the estimation problem for a linear system without sensor selection, i.e. all the readings from sensors are used. Kalman filter provides the optimal solution which takes the following form:
\begin{align}\label{eq_Kalman_filter}
    \hat x_{1| 0}^* & = \bar x_1 ,\quad P_{1|0}^*  = \Sigma\\
    \hat x _{k + 1|k}^* & = A \hat x^*_{k|k}  , \quad P_{k + 1|k}^*  = AP^*_{k|k} A^T  + Q \nonumber\\
    K_k^*& = P_{k|k - 1}^* C^T (CP_{k|k - 1}^* C^T  + R)^{ - 1}  \nonumber\\
    \hat x^*_{k|k}& = \hat x^*_{k|k - 1}  + K_k^* (y_k  - C \hat x _{k|k - 1}^* )  \nonumber\\
    P^*_{k|k}& = P^*_{k|k - 1}  -  K^*_k CP^*_{k|k - 1}.  \nonumber
  \end{align}
  This formulation of the Kalman filter explicitly shows the dependence of the estimated state $\hat x_{k|k}^*$ from $y_k$. However the dependence from the 'old' measurements is recursive. In the following Lemma we express the estimated state $\hat x_{k|k}^*$ given by the Kalman filter in terms of all the observations, represented by $Y_k$.
\begin{lem}\label{def_G}
Consider a linear system as in \eqref{eq:systemdiscription}. The state estimation $\hat x_{k|k}^*$ given by the Kalman filter is
\begin{equation}
  \begin{split}
  \hat x_{k|k}^* = G^*_k \left[ {\begin{array}{*{20}c}
    {y_1 }  \\
    \vdots   \\
    {y_{k-1} }  \\
    {y_k }  \\
  \end{array}} \right] + H_k^*\bar x_1
  = G^*_k Y_k+H_k^*\bar x_1 \,,
  \label{eq:kalmanestimation}
  \end{split}
\end{equation}
where $G_1^* = K_1^* \,\,H_1^* = I-K_1^*C$ and $G_k^*,\,H_k^*$ can be evaluated recursively as
\begin{equation}
  \begin{split}
  G_{k+1}^* &= [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*]\,,\\
  H_{k+1}^* &= (A-K_{k+1}^*CA)H_k^*\,,
  \label{eq:kalmangain}
  \end{split}
\end{equation}
where $K_k^*$ is the optimal Kalman gain given by \eqref{eq_Kalman_filter}.
\end{lem}
\begin{pf}
  We will prove the lemma by induction. When $k = 1$, by \eqref{eq_Kalman_filter}, we know that
  \begin{equation}
    \hat x_{1|1} = \bar x_1 + K_1^* (y_1 - C\bar x_1) = K_1^* Y_1 + (I-K_1^*C)\bar x_1.
  \end{equation}
  Hence $G_1^* = K_1^*$ and $H_1^* = I-K_1^*C$.
  Now suppose that \eqref{eq:kalmangain} holds for $k = 1,\ldots,N$. When $k = N+1$, we have
  \begin{equation}
    \begin{split}
    \hat x_{N + 1|N}^* & = A \hat x^*_{N|N} = AG^*_N Y_N + AH^*_N \bar x_1\\
    \hat x^*_{N+1|N+1}& = \hat x^*_{N+1|N}  + K_{N+1}^* (y_{N+1}  - C \hat x_{N+1|N}^* )\\
    %&= K_{N+1}^* y_{N+1} + (I-K^*_{N+1}C)\hat x^*_{N+1|N} \\
    &= K_{N+1}^* y_{N+1} + (A-K^*_{N+1}CA)G^*_N Y_k \\
    &+ (A-K^*_{N+1}CA)H_N^* \bar x_1  \,.
    \end{split}
  \end{equation}
  Hence, \eqref{eq:kalmangain} holds for $k = N+1$ and, by induction, we conclude the proof.
\end{pf}
Now, let us consider the sensor selection problem. In the case of sensor selection, the estimated state $\hat x_{k|k} = E(x_k|\Gamma_kY_k)$ can still be written as
\begin{align}\label{eq:kalmanestimationforselection}
  \hat x_{k|k} &= G_k\Gamma_k Y_k + H_k \bar x_1 ,  \nonumber\\
    P_{k|k} &=Cov(\hat x_{k|k}  - x_k) = Cov(G_k\Gamma_k Y_k + H_k\bar x_1 - x_k) \nonumber\\
    &=Cov(G_k\Gamma_k Y_k - x_k),
\end{align}
where $G_k,\,H_k$ are the optimal gains under the sensor selection scheme $\Gamma_k$. The last equality is true since $H_k\bar x_1$ is a deterministic vector. In general, $G_k$ is different from $G^*_k$ and depends on the sensor selection strategy $\Gamma_k$ we choose. However we will not write $G_k$ explicitly in terms of $\Gamma_k$, because that will lead to a Riccati equation, which is hard to analyze in general. In contrast, by exploiting the optimality of Kalman filter, we will optimize over the matrix $G_k$. In the next Theorem, we propose a new formulation for the minimization problem of $trace(P_{k|k})$ over both $G_k$ and $\Gamma_k$.
\begin{thm}\label{main_theor}
Consider the linear system \eqref{eq:systemdiscription} and let $\Gamma_k$ be the sensor selection matrix.  The minimization problem over $\Gamma_k$ of the $trace(P_{k|k})$, given by the Kalman filter, can be formulated as
\begin{align}
 \min_{G_k,\Gamma_k}\;  \left\|G_k\Gamma_k\mathcal S_k -  G^*_k \mathcal S_k \right\|_F^2,
\end{align}
where $G_k^*$ is given in \eqref{eq:kalmangain}, $\left\|\cdot\right\|_F$ represents the Frobenius norm and
\begin{align}
&\mathcal S_k \in \mathbb R^{mk\times mk}\;:\;Cov(Y_k) = \mathcal S_k \mathcal S_k\,.
\end{align}
\end{thm}
\begin{pf}
In the case of sensor selection, the covariance matrix can be written as
\begin{equation}\label{cov_matrix1}
  \begin{split}
    P_{k|k} &=Cov(\hat x_{k|k}  - x_k) = Cov(G_k\Gamma_k Y_k - x_k) \\
    &= Cov [(G_k\Gamma_k - G_k^*) Y_k  + G_k^* Y_k +H_k^*\bar x_1- x_k ]\\
    & = Cov [(G_k\Gamma_k - G_k^*) Y_k]  + Cov(G_k^* Y_k +H_k^*\bar x_1- x_k )\\
    & = (G_k\Gamma_k - G_k^*) Cov(Y_k)(G_k\Gamma_k - G_k^*)^T  + P_{k|k}^* \,.
  \end{split}
\end{equation}
The second equality holds because $H_k^*\bar x_1$ is deterministic and furthermore it does not change the covariance. The decomposition of the covariance matrix in the third line of equation \eqref{cov_matrix1} is possible since, by the optimality of Kalman filter,  $x_k - \hat x_{k|k}^*=x_k-G_k^*Y_k-H_k^*\bar x_1 $ is orthogonal to $Y_k$. Therefore the cost function can be written as
\begin{equation}
  \begin{split}
&\min_{\Gamma_k} \;trace( P_{k|k})=\min_{\Gamma_k} trace(P_{k|k}^*)+\\
&+ \min_{\Gamma_k,G_k} \; trace\left[(G_k \Gamma_k - G_k^*) Cov(Y_k) (G_k \Gamma_k - G_k^*)^T\right].
  \end{split}
  \label{eq:optimazition1}
\end{equation}
Since the first term does not depend on $\Gamma_k$, we will focus only on the second term.  Let us decompose $Cov(Y_k)$ as $\mathcal S_k \mathcal S_k$, where $\mathcal S_k$ is symmetric. Since $Cov(Y_k)$ is positive semidefinite, we can always find $\mathcal S_k$. Thus
\begin{equation}
  \begin{split}
  &(G_k \Gamma_k - G_k^*) Cov(Y_k) (G_k \Gamma_k - G_k^*)^T \\
  &= (G_k\Gamma_k \mathcal S_k -  G_k^*\mathcal S_k)(G_k\Gamma_k \mathcal S_k -  G_k^*\mathcal S_k)^T.
  \end{split}
\end{equation}
As a result, the minimization problem $\min_{\Gamma_k}\; trace(P_{k|k})$ is equivalent to
\begin{align*}
\min_{\Gamma_k,G_k}& \; trace\left[(G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k)  (G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k)^T\right] = \\
\min_{\Gamma_k,G_k}& \;\left\|G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k\right\|_F^2.
\end{align*}
\end{pf}
From Theorem \ref{main_theor} we can derive the following corollary.
\begin{cor}\label{cor1}
Consider the linear system \eqref{eq:systemdiscription} and let $\Gamma_k$ be the sensor selection matrix. The minimization problem over $\Gamma_k$ of the $trace(\mathcal Q_k P_{k|k} \mathcal Q_k^T)$ given by the Kalman filter can be formulated as
\begin{align}
 \min_{\Gamma_k,G_k} \;  \left\| \mathcal Q_k G_k\Gamma_k \mathcal S_k -  \mathcal Q_k G^*_k\mathcal S_k \right\|_F^2,
\end{align}
where where $G_k^*$ is given in \eqref{eq:kalmangain} and
\begin{align*}
&\mathcal S_k \in \mathbb R^{mk\times mk}\;:\; Cov(Y_k) = \mathcal S_k \mathcal S_k \nonumber\,.
\end{align*}
\end{cor}
\begin{pf}
It follows from the proof of Theorem \ref{main_theor}.
\end{pf}
Now let us define the matrix
\begin{equation}
  \mathcal G_k \triangleq \mathcal Q_k G_k \Gamma_k \in \mathbb R^{n' \times mk}\,,
\end{equation}
the following theorem relates $\gamma_i$ and $\mathcal G_k$.
\begin{thm}
  Denote the $i$th column of the matrix $\mathcal G_k$ as ${\vec {\mathcal G}}_{k,i}$. The following inequality
  %of $\gamma_i$ and ${\vec {\mathcal G}}_{k,i}$
  holds:
 \begin{equation}
   \gamma_i \geq \left\|\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1\right\|_0,
 \end{equation}
 where $\left\|\cdot \right\|_0$ (called the $L_0$ norm\footnote{$L_0$ is in fact not a norm.}) of a scalar is $0$ if the scalar is $0$ (it is $1$ otherwise) and $k'$ is the smallest number which satisfies $mk' > i$.
\end{thm}
\begin{pf}
  It follows directly from the definition of $\mathcal G_k$.
\end{pf}

Using all the previous arguments, we can conclude that the optimization problem $(P_0)$ is equivalent to
\begin{align} \label{non_conv_opt}
(P_0'):\, &\min_{\mathcal G_k, \vec \gamma} && \sum_{k=1}^T  \left\| \mathcal G_k \mathcal S_k - \mathcal Q_kG_k^*\mathcal S_k \right\|_F^2,\\
  &s.t.&&  H \vec \gamma \leq b \nonumber \\
       &&&\gamma_i \geq \left\|\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1\right\|_0  \quad i=1\dots mT.
\end{align}
\begin{rem}
The main improvement of formulation $(P_0')$ over $(P_0)$ is that instead of $trace(\mathcal Q_k P_{k|k}\mathcal Q_k)$, we now have an explicit quadratic objective function as well an explicit relation between the said objective function and the $\gamma_i$s.
\end{rem}

In general, sensor selections are hard combinatorial problems as they involve binary constraints. For large systems the problem becomes computationally infeasible. Solutions in this case are based on heuristic methods. In this paper, the formulation $(P_0')$ is also a combinatorial problem since the last constraint of $(P_0')$ contains an $L_0$ norm. However, formulation $(P_0')$ allows us to easily relax it to a convex problem, which will be discussed in the next subsection.

\subsection{Reweighted $L_1$ approximation}
This subsection describes a reweighted $L_1$ approximation of problem $(P_0')$.\\
As discussed in the previous subsection, $(P_0')$ is a combinatorial problem. In principle, to find the optimal solution, we should check all possible sensor selection schedules verifying the constraints. However, if the number of sensors is large, this evaluation becomes infeasible. Hence we have to consider solutions which are suboptimal, but computationally feasible. We will derive suboptimal solution based on a relaxed convex version of the original optimization problem \cite{BOYD:Convex}.\\
One standard technique to solve the $L_0$ optimization problem $(P_0')$ is to replace the $L_0$ norm with $L_1$ norm. However, the main drawback of this approach is that in $L_1$ larger numbers are penalized much more than in $L_0$, where all the non-zero numbers are treated equally. In \cite{boydreweight}, the authors provide a reweighted $L_1$ minimization to address this issue. According to this method, the $L_0$ norm is substituted with a weighted $L_1$ norm, where the weights are chosen to avoid the penalization of the bigger coefficients. The authors further propose an iterative algorithm that alternates between a minimization phase and a of redefinition the weights. The authors find that this reweighted $L_1$ minimization outperforms traditional $L_1$ minimization in many situations. In the following we use this algorithm to relax the optimization problem $(P_0')$. The algorithm is composed of $4$ steps:
\begin{enumerate}
\item Set the iteration count $l$ to zero and set the weights vector to $w_i^{0}=1$ for $i=1,...., mT$
\item Solve the weighted $L_1$ minimization problem
\begin{align} \label{conv_opt}
(P_1):\, &\min_{\mathcal G_k, \vec \gamma} && \sum_{k=1}^T  \left\| \mathcal G_k \mathcal S_k - \mathcal Q_kG_k^*\mathcal S_k \right\|_F^2,\\
  &s.t.&&  H \vec \gamma \leq b \nonumber\\
    &&&\gamma_i \geq w_i^{l}\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1 \quad i=1\dots mT. \nonumber
\end{align}
Let the solution be $\gamma_1^l,\ldots,\gamma_{mT}^l$
\item Update the weights
\[
w_i^{l+1}=\frac{1}{\gamma_{i}^l+\epsilon}\quad i=1, \dots ,mT.
\]
\item Terminate if either $l$ reaches a specified maximum number of iterations $l_{max}$ or the solution has converged. Otherwise, increase $l$ and return to step 2.
\end{enumerate}
\begin{rem}
  As in \cite{boydreweight}, we introduce the parameter $\epsilon >0$ in step 3 in order to avoid inversion of zero-valued components in $\vec \gamma$.
\end{rem}
\begin{rem}
We do not make any assumption on the covariance matrices $Q,\,R,\,\Sigma$ during the reformulation and relaxation steps. Hence, the relaxed problem $(P_1)$ can handle correlated measurement noise.
\end{rem}
\begin{rem}
  Usually the problem takes less than $10$ reweightings for the solution to converge\footnote{We do not need to have a very accurate solution because we will threshold $\vec \gamma$ and make it binary.}. For each weighted minimization, it is easy to see that the problem is a quadratic programming(QP) problem. In \cite{Quadcomplexity}, the authors proposed an interior point algorithm for solving QP with $O(N^{2.5})$ arithmetic operations per iteration, where $N$ is the total number of optimization variables. In our problem, each $G_k$ is a $n\times mk$ matrix and $\vec\gamma \in \mathbb R^{mT}$. Hence, the total number of optimization variables is
  \begin{equation*}
    N = nm\sum_{k=1}^T k + mT = nmT(T+1)/2+mT.
  \end{equation*}
  Since we do not need an accurate solution of QP, we can assume that the number of iterations needed for solving QP is fixed. Hence, the complexity of our algorithm is $O(n^{2.5}m^{2.5}T^5)$.
\end{rem}


\subsection{Multi-step Analysis}

In the previous section we have shown how the multi-step sensor selection problem in case of correlated noise can be relaxed into a convex minimization problem. Although the relaxation technique significantly contributes to reducing the computational complexity, the problem is still intractable for large $T$s. In fact, as the window size $T$ increases, the number of optimization variables rapidly grows ($O(T^2)$) as shown in the previous subsection.
A natural solution to this problem is to divide the scheduling period $T$ into smaller intervals and solve the optimization problem sequentially for each of these smaller intervals. This is clearly an approximation which will yield suboptimal results.
In this section, we will provide a complete analysis of the approximation and we will prove that the gap between the greedy approach and optimal is at most linear with respect to the ratio of window size.\\
Let us indicate with $S_1$ the optimal sensor selection strategy given by Problem $(P_0)$ in  $[1 \,, NT]$ with $\mathcal Q_k = I_n$. Let $P_{k|k}(S_1)$ be the estimation error covariance at time $k$ given by the schedule $S_1$.  Consider now the greedy algorithm that first solves problem $(P_0)$ from time $1$ to time $T$ and then solves it again for time $T+1$ to $2T$ and so on. Let us indicate with $S_2$ the sensor selection strategy in $[1 \,, NT]$ given by the greedy method and with $P_{k|k}(S_2)$ the corresponding error covariance matrix\footnote{For simplicity we just choose $\mathcal Q_k= I$}.

\begin{rem}
  To compare the optimal sensor selection strategy in $[1 \,, NT]$ with the solution of the greedy algorithm we assume the constraints of Problem $(P_0)$ on $\gamma_i$s in each small window $[KT+1,\,(K+1)T]$ are decoupled from each other, which means that the constraints take the following form
\begin{align*}
 \left[ {\begin{array}{*{20}c}
	H_1&&\\
	&\ddots&\\
	&&H_N
  \end{array}} \right]  \vec \gamma \leq \left[ {\begin{array}{*{20}c}
	b_1\\
	\vdots\\
	b_N	
  \end{array}} \right] , \gamma_i = 0\;or\;1,\;i=1,\ldots,NmT, \nonumber
\end{align*}
where each $H_i$ has $mT$ columns.
\end{rem}

%\begin{figure}
%   \begin{center}
%   \includegraphics[width=2in]{multistep_analysis.eps}
%    \caption{For $N=2$, the sensor selection strategy $S_1$ is the optimal strategy in the interval $[0 \;,\;2T]$ while $S_2$ is the union of the optimal strategies in $[0 \;,\;T]$ and $[T+1 \;,\;2T]$} \label{fig:multistep_analysis}
%   \end{center}
% \end{figure}

In this following theorem we characterize the loss of performance of the sensor selection strategy $S_2$ compared to $S_1$ in terms of objective function of Problem $(P_0)$ in $[1 \,, NT]$.
\begin{thm}
  \label{theorem:multigain}
 The loss of performance of schedule $S_2$ with respect to $S_1$ satisfies the following inequality:
 \begin{equation*}
   \sum_{k=1}^{NT}trace(P_{k|k}(S_2)) - \sum_{k=1}^{NT} trace(P_{k|k}(S_1)) \leq (N-1)\frac{\rho\beta}{\alpha}
 \end{equation*}
 where
 \begin{align}
  \label{rho} \rho    &= \inf\{r >0 |P_{KT|KT}(S_2)\leq (r+1)P_{KT|KT}(S_1),\nonumber\\
  &K = 1,\ldots,N-1\} \\
  \label{alpha}\alpha  &= \sup\{r >0 |Q \geq r (AP_{k|k}(S_1)A^T)  \nonumber \\
  & \hspace{3cm},\,k=T+1,\ldots,NT\} \\
  \label{beta}\beta   &= \sup\{trace(P_{k|k}(S_1))|k=T+1,\ldots,NT\}.
\end{align}
\end{thm}
\begin{pf}
See Appendix.
\end{pf}
 Let $S_*$ be the schedule that selects all the sensors and $S_{\phi}$  be the schedule that selects no sensors. For any schedule $S$, we know that
 \begin{equation}
	P_{k|k}(S_*) \leq P_{k|k}(S) \leq P_{k|k}(S_{\phi})	\,.
	\label{eq:schedulebound}
 \end{equation}
 If the system is detectable, then $P_{k|k}(S_*)$ converges. Moreover if we assume that the system is stable also $P_{k|k}(S_\phi)$ converges. 
 It means that $P_{k|k}(S_*)$ and $P_{k|k}(S_\phi)$ are uniformly bounded and hence there exists two positive semidefinite matrices $\overline P$ and $\underline P$, such that
      \begin{displaymath}
	\underline P\leq P_{k|k}(S_*)\leq P_{k|k}(S) \leq P_{k|k}(S_\phi)\leq \overline P.
      \end{displaymath}
Since the above inequality is independent of both schedule $S$ and time index $k$, we can conclude that there exist uniform bounds for $\alpha,\,\beta,\,\rho$ which are independent of the real window size $T$. Moreover, the loss incurred when dividing the whole window to $N$ parts will always be sublinear with respect to $N$.\\
 In the meanwhile, solving $(P_0)$ on $[0,\,NT]$ needs $O(n^{2.5}m^{2.5}(NT)^5)$ arithmetic operations while to compute schedule $S_2$, we only need $O(n^{2.5}m^{2.5}T^5)$ arithmetic operations for $N$ smaller window of size $T$. Hence, reducing the window size by $N$ will reduce the computation complexity to $N^{-4}$ of the original problem. We can conclude that when the system is large, the greedy algorithm is a reasonable approach to reduce computational complexity.
