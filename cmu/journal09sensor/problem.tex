In this section we will derive a general framework which can capture a large number of sensor selection problems.\\
Consider the linear system
\begin{equation}
  \begin{split}
    x_{k+1} &= Ax_k + w_k,\\
    y_{k} &= C x_k + v_k,
  \end{split}
  \label{eq:systemdiscription}
\end{equation}
where $w_k,v_k,x_1$ are independent Gaussian random variables, $x_1 \sim \mathcal N(\bar x_1,\,\Sigma)$, $w_k \sim \mathcal N(0,\,Q)$ and $v_k \sim \mathcal N(0,\,R)$\footnote{Note that we start at time $1$ instead of time $0$.}. We assume $x_k \in \mathbb R^n$ and $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}  ]^T \in \mathbb R^m$ is the vector of the sensors' measurements with $y_{k,i}$ the measurement of sensor $i$ at time $k$. Let us indicate with $\gamma_{j}, j=1, \ldots, mT$, the binary variables such that $\gamma_{m(k-1)+i}=1$ if sensor $i$ transmits at step time $k$, for $i=1 \dots m$ and $k=1 \dots T$, and it is $0$ otherwise. Thus, at time $k$, the estimator receives readings $[\gamma_{m(k-1)+1}y_{k,1},\ldots,\gamma_{m(k-1)+m}y_{k,m}]^T$.\\
The aim of this paper is to compute a sensor selection sequence $\gamma_1,\ldots,\gamma_{mT}$ from time $1$ to time $T$, which provides the optimal estimation performance while satisfying specific energy and topology constraints.\\
In order to define a multi-step Kalman filter with sensor selection, let us introduce the following quantities
\begin{align}
  Y_k &\triangleq  [y_1^T,y_2^T,\ldots,y_k^T]^T &\in& \mathbb R^{mk},\\
  \Gamma_k &\triangleq diag(\gamma_1,\ldots,\gamma_{mk}) &\in& \mathbb R^{mk\times mk},\\
  \vec \gamma &\triangleq [\gamma_1,\ldots,\gamma_{mT}]^T &\in& \mathbb R^{mT},\\
  \hat x^*_{k|k} &\triangleq E\left(x_k|Y_k\right)&\in&\mathbb R^{n},\\
  P^*_{k|k} &\triangleq Cov\left(x_k|Y_k\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k|k} &\triangleq E\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n},\\
  P_{k|k} &\triangleq Cov\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}
The multi-step sensor selection problem can be formulated as the following optimization problem
\begin{align}\label{opt_prob}
 (P_0):\, &\min_{\vec\gamma} && \sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T),\\
  &s.t.&&  H \vec \gamma \leq b, \nonumber\\
  &&& \gamma_i = 0\;or\;1,\;i=1,\ldots,mT, \nonumber
\end{align}
where $\mathcal Q_k \in \mathbb R^{n' \times n}$ is of full row rank\footnote{In order to include some objective functions into this framework, we assume $n'\leq n$.},  $H \in R^{h \times mT}$ and $b\in R^h$.

This formulation can address several classes of sensor selection problems, despite to the fact that it may look restrictive at first. In this framework, the matrices $\mathcal Q_k$ can be used to define the minimization problem we want to tackle, while the matrix $H$ and the vector $b$ define the constraints we impose on the wireless sensors. In the following paragraphs we outline some of the problems that can be tackled in the proposed framework, by appropriate choice of the matrices $\mathcal Q_ks, H$ and the vector $b$. Let us start with the objective function.

\begin{itemize}
  \item \textit{Minimization of the final estimation error}

    Assume we want to minimize the estimation error at time $T$. Thus, we can choose \[\mathcal Q_1,\ldots,\mathcal Q_{T-1} = 0,\;\mathcal Q_T = I.\]
  \item \textit{Minimization of the average estimation error}

    Assume we want to minimize the average estimation error from time $1$ to time $T$. Thus, we can choose \[\mathcal Q_k = I, k = 1,\ldots,T.\]

  \item \textit{Minimization of the estimation error of a single state}

    Assume at time $k$ we are interested in the $i$th state $x_{k,i}$ of the system. Thus, we can choose \[\mathcal Q_k = [\delta_{1,i},\ldots,\delta_{n,i}]\in \mathbb R^{1\times n},\] where $\delta_{i,j} = 1$ if $i = j$, it is $0$ otherwise. Notice that $n' = 1 \neq n$ in general, which explains the reason why we do not require $\mathcal Q_k$ to be a square matrix.

  \item \textit{Minimization of the objective function of a finite horizon Linear Quadratic Gaussian (LQG) regulation problem}

    Assume we have the following LQG cost function
    \begin{displaymath}
      J_T = E\left[x_T^T W_T x_T + \sum_{k=1}^{T-1} (x_k^TW_kx_k+u_k^TU_ku_k)\right],
    \end{displaymath}
    where $W_k, U_k$ are positive semidefinite matrices. Given a sensor selection schedule, it is well known that the optimal controller and estimator, which minimize the LQG cost function, can be designed separately. The optimal control law is
    \begin{displaymath}
      \begin{split}
	u_k &= -(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A\hat x_{k|k},
      \end{split}
    \end{displaymath}
where $S_k$ satisfies the backward recursive equations
    \begin{displaymath}
      \begin{split}
	S_k &= A^TS_{k+1}A+W_k\\
	&- A^TS_{k+1}B(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A,\\
      \end{split}
    \end{displaymath}
with $S_T = W_T$. The optimal estimator is still the Kalman filter. The optimal value of cost function is
\begin{equation}
  \begin{split}
  J_T^* &= trace(S_1\Sigma)+ \sum_{k=1}^{T-1}trace(S_{k+1}Q)
  \\&+ \sum_{k=1}^{T-1}trace[(A^TS_{k+1}A+W_k-S_k)P_{k|k}].
\end{split}
\end{equation}
Thus, in order to find the best sensor selection schedule which minimizes $J_T^*$, we can let \[\mathcal Q_k^T\mathcal Q_k = A^TS_{k+1}A + W_k - S_k , \mathcal Q_T = 0.\]
\end{itemize}
Next we will show how several network constraints can be formulated within the proposed framework. However we will not write the constraints explicitly in the form of $H\vec \gamma \leq b$. Instead we will only show that the constraints are linear on $\vec \gamma$.
\begin{itemize}
  \item \textit{Fixed number of sensors to be used at each time step}

    Assume at each time step we want to select no more than $p < m $ sensors. Thus, the constraints can be written as
    \begin{align}\label{fix_number}
    \sum_{i=1}^m\gamma_{m(k-1)+i} \leq p, \;k=1,\ldots,T
    \end{align}
  \item \textit{Sensor energy constraints}

     Assume each sensor has initial energy $\mathcal E_1,\ldots,\mathcal E_m$ and each observation consumes $e_1,\ldots,e_m$ energy units respectively. As a result, sensor $i$ can do at most $\mathcal E_i/e_i$ observations until its battery runs out. Since we cannot use the sensor after the battery dies, the energy constraints can be written as
\begin{align}\label{lifetime}
\sum_{k=1}^T \gamma_{m(k-1)+i} \leq \mathcal E_i/e_i,\;i =1,\ldots,m
\end{align}
  \item \textit{Multi-hop sensor networks}

    Consider that the sensor network has a tree structure and the estimator is the root node. The network uses a data aggregation protocol, where each node forwards the observation packets to its parent until the packet reaches the root. Moreover, the nodes on the route of the packet will add their own readings into the packet. As a result, a node needs to be selected to send observations if a child node is selected. Define the set of child nodes of sensor $i$ to be $\mathcal C_i$. We can write the topology constraints of the network as
    \begin{align}\label{multi-hop}
    \gamma_{m(k-1)+i} \geq \gamma_{m(k-1)+j},\;k =1,\ldots,T,\;j \in \mathcal C_i\,.
    \end{align}
\end{itemize}

