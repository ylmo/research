maxstep = 6; 
epsilon = 0.005;
gammaold =sum((abs(Gstar(:,1:m,1)/S(1:m,1:m,1))));
gammaold = 1./(gammaold+epsilon);
%gammaold = ones(1,m);
for i = 1:maxstep
    cvx_begin
    if i ~= maxstep
        cvx_precision low
    else
        cvx_precision default
    end
        cvx_quiet(true);
        variable G(n,m)
        variable gamma(1,m)
        variable totalgamma(1,m);
        minimize (norm(G(1:n,1:m)* S(1:m,1:m,1)-Gstar(:,1:m,1),'fro'))
        subject to
        gamma >= sum(abs(G))
        totalgamma == gammaold(1:m).*gamma(1:m)
        sum(totalgamma) <= p + (m-p)*0.5^i
    cvx_end
    if (~strcmp(cvx_status,'Solved'))
        break
    end
    totalgamma;
    gammaold = 1./(gamma+epsilon);
end
gamma = gammaold.*gamma;
[tmp Index] = sort(gamma,'descend');
gamma(Index(1:p)) = 1;
gamma(Index(p+1:m)) = 0;