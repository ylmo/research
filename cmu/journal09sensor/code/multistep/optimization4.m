maxstep = 15; 
epsilon = 0.005;
gammaold = ones(1,4*m);
for i = 1:maxstep
    cvx_begin
        if i ~= maxstep
        cvx_precision low
    else
        cvx_precision default
    end
        cvx_quiet(true);
        variable G(4*n,4*m)
        variable gamma(1,4*m)
        variable totalgamma(1,4*m);
        minimize (sum(sum((G(1:n,1:m)*S(1:m,1:m,1)-Gstar(:,1:m,1)).^2))+sum(sum((G(n+1:2*n,1:2*m)*S(1:2*m,1:2*m,2)-Gstar(:,1:2*m,2)).^2))+sum(sum((G(2*n+1:3*n,1:3*m)*S(1:3*m,1:3*m,3)-Gstar(:,1:3*m,3)).^2))+sum(sum((G(3*n+1:4*n,1:4*m)*S(1:4*m,1:4*m,4)-Gstar(:,1:4*m,4)).^2)))
        subject to  
        G(1:n,(m+1):2*m) == zeros(n,m)
        gamma >= sum(abs(G))
        totalgamma == gammaold.*gamma;
        sum(totalgamma) <= 4*p + (4*m-4*p) * 0.5^i  ;
    cvx_end
    if (~strcmp(cvx_status,'Solved'))
        break
    end
    totalgamma;
    gammaold = 1./(gamma+epsilon);
end
totalgamma;
[tmp Index] = sort(totalgamma,'descend');
gamma(Index(1:4*p)) = 1;
gamma(Index(4*p+1:4*m)) = 0;
