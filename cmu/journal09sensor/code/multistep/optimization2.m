maxstep = 15; 
epsilon = 0.005;
gammaold = zeros(1,2*m);
gammaold(1,1:m) =sum((abs(Gstar(:,1:m,1)/S(1:m,1:m,1))));
gammaold = gammaold + sum((abs(Gstar(:,1:2*m,2)/S(1:2*m,1:2*m,2))));
gammaold = 1./(gammaold+epsilon);
%gammaold = ones(1,2*m);
for i = 1:maxstep
    cvx_begin
        if i ~= maxstep
        cvx_precision low
    else
        cvx_precision default
    end
        cvx_quiet(true);
        variable G(2*n,2*m)
        variable gamma(1,2*m)
        variable totalgamma(1,2*m);
        minimize (sum(sum((G(1:n,1:m)*S(1:m,1:m,1)-Gstar(:,1:m,1)).^2))+sum(sum((G(n+1:2*n,1:2*m)*S(1:2*m,1:2*m,2)-Gstar(:,1:2*m,2)).^2)))
        subject to  
        G(1:n,(m+1):2*m) == zeros(n,m)
        gamma >= sum(abs(G))
        totalgamma == gammaold.*gamma;
        sum(totalgamma) <= 2*p + (2*m-2*p) * 0.5^i  ;
    cvx_end
    if (~strcmp(cvx_status,'Solved'))
        break
    end
    totalgamma;
    gammaold = 1./(gamma+epsilon);
  % gammaold
end
%gamma
totalgamma;
%gamma = gammaold.*gamma;
[tmp Index] = sort(totalgamma,'descend');
gamma(Index(1:2*p)) = 1;
gamma(Index(2*p+1:2*m)) = 0;
sum(sum((G(1:n,1:m)*S(1:m,1:m,1)-Gstar(:,1:m,1)).^2)) + trace(Pstar(:,:,1));
sum(sum((G(n+1:2*n,1:2*m)*S(1:2*m,1:2*m,2)-Gstar(:,1:2*m,2)).^2))+ trace(Pstar(:,:,2));
