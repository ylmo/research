P = Sigma;
for i = 1:(size(gamma,2)/m)
    Index = find((gamma(((i-1)*m+1):i*m)) >= 0.5);
    P = A* P *A' + Q;
    Ctmp = C(Index,:);
    Rtmp = R(Index,Index);
    K = P * Ctmp' *inv(Ctmp*P*Ctmp'+Rtmp);
    P = P - K*Ctmp*P;
    step;
    tr(step) = trace(P);
    schedule(step,:) = gamma(((i-1)*m+1):i*m);
    step = step + 1;
end
Sigma = P;
save(name)
