clc
clear
rand('twister',sum(clock)*100)
n = 10;
m = 10;
p = 4;
r = 5;
T = 1;
A = rand(n);
C = rand(m,n);
B = rand(n,r);
Qhalf = rand(n)/sqrt(n);
Q = Qhalf*Qhalf';
R = eye(m);
Sigmahalf = rand(n)/sqrt(n);
Sigma = Sigmahalf * Sigmahalf';
step = 1;
Slqg = eye(n);

for i = 1:100
    Slqg = A'*Slqg*A+eye(n) - A'*Slqg*B*inv(B'*Slqg*B+eye(r))*B'*Slqg*A;
end
Glqg = -inv(B'*Slqg*B+eye(r))*B'*Slqg*A;
Qlqg = sqrtm(A'*Slqg*A+eye(n)-Slqg);
Qlqg = real(Qlqg);