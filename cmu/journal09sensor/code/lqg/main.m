init
name = sprintf('%04d_%02d_%02d_%02d_%02d_%02d',round(clock));
%------------------------1 step----------------------%
w = randn(n,100);
v = randn(m,100);
x0 = Sigmahalf*randn(n,1);
x = x0;
xhat = zeros(n,1);
u = Glqg*xhat;
P = Sigma;
for iter = 1:100
    %prediction update
    x = A*x + Qhalf*w(:,iter) + B*u;
    xhat = A*xhat + B*u;
    P = A* P *A' + Q;    
    %sensor selection
    vars
    optimization1
    Index = find(gamma >= 0.5);
    %measurement    
    Ctmp = C(Index,:);
    Rtmp = R(Index,Index);
    y = Ctmp * x + v(Index,iter);
    K = P * Ctmp' *inv(Ctmp*P*Ctmp'+Rtmp);
    P = P - K*Ctmp*P;
    
    xhat = xhat + K*(y - Ctmp*xhat);
    u = Glqg * xhat;
    
    performance1(iter) = x'*x + u'*u
    schedule1(iter,:) = gamma
    save(name)
end
%-------------------------boyd-----------------------%
x = x0;
xhat = zeros(n,1);
u = Glqg*xhat;
P = Sigma;
for iter = 1:100
    %prediction update
    x = A*x + Qhalf*w(:,iter) + B*u;
    xhat = A*xhat + B*u;
    P = A* P *A' + Q;    
    %sensor selection
    boyd;
    %measurement
    Ctmp = C(Index,:);
    Rtmp = R(Index,Index);
    y = Ctmp * x + v(Index,iter);
    K = P * Ctmp' *inv(Ctmp*P*Ctmp'+Rtmp);
    P = P - K*Ctmp*P;
    
    xhat = xhat + K*(y - Ctmp*xhat);
    u = Glqg * xhat;
    
    performanceb(iter) = x'*x + u'*u;
    scheduleb(iter,:) = gamma;
    save(name)
end

%------------------------random----------------------%
x = x0;
xhat = zeros(n,1);
u = Glqg*xhat;
P = Sigma;
for iter = 1:100
    %prediction update
    x = A*x + Qhalf*w(:,iter) + B*u;
    xhat = A*xhat + B*u;
    P = A* P *A' + Q;    
    %sensor selection
    Index = randperm(m);
    Index = Index(1:p);
    gamma = zeros(1,m);
    gamma(Index) = 1;
    %measurement
    Ctmp = C(Index,:);
    Rtmp = R(Index,Index);
    y = Ctmp * x + v(Index,iter);
    K = P * Ctmp' *inv(Ctmp*P*Ctmp'+Rtmp);
    P = P - K*Ctmp*P;
    
    xhat = xhat + K*(y - Ctmp*xhat);
    u = Glqg * xhat;
    
    performancer(iter) = x'*x + u'*u;
    scheduler(iter,:) = gamma;
    save(name)
end