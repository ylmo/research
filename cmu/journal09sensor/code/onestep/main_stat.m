clear all
close all
clc
p0=1;
p_end=25;

for  N=1:50
    init
    Sigma_init=Sigma;
    for p=p0:p_end
        
        name = sprintf('%04d_%02d_%02d_%02d_%02d_%02d',round(clock));
        %------------------------1 step----------------------%
        Sigma=Sigma_init;
        step=1;
        vars
        optimization1
        display2
        tr1(N,p-p0+1) = tr;
        clear tr 
        %------------------------ boyd ----------------------%
        Sigma=Sigma_init;
        step = 1;
        boyd
        display2
        trb(N,p-p0+1) = tr;
        clear tr 
        %------------------------random----------------------%
        step = 1;
        Sigma=Sigma_init;
        index=randperm(m);
        gamma = zeros(1,m);
        gamma(index(1:p)) = 1;
        display2
        trr(N,p-p0+1) = tr;
        clear tr 
%         %------------------------all_sensors----------------------%       
%         Sigma=Sigma_init;
%         step = 1;
%         gamma=ones(1,m);
%         display2
%         tro(N,p-p0+1) = tr;
%         N
%         clear tr
        save dati tr1 trb trr p0 p_end
    end
    N
end