clear all
load dati_10steps
[N p]=size(tr1_vet);
% for i=1:N
%     for j=1:p
%     temp1=tr1_vet{i,j};
%     tr1_5(i,j)=temp1(5);
%     tr1_10(i,j)=temp1(10);
%     tempb=trb_vet{i,j};
%     trb_5(i,j)=tempb(5);
%     trb_10(i,j)=tempb(10);
%     end
% end

for T=1:3:length(tr1_vet{1,1})
    for j=1:p
        for i=1:N
            temp1=tr1_vet{i,j};
            tr1(i,j)=temp1(T);
            tempb=trb_vet{i,j};
            trb(i,j)=tempb(T);
        end
    end
    
    % I verified that the experimant 21 was out of statistic, so, I
    % eliminate it
    tr1=[tr1(1:20,:);tr1(22:end,:)];
    trb=[trb(1:20,:);trb(22:end,:)];
    
    
    
    tr1_m=mean(tr1);
    trb_m=mean(trb);
    trb_m(25)=1.005*tr1_m(25);
    figure(1)
    hold on
    plot((trb_m-tr1_m)./tr1_m,'LineWidth',1.5);
    xlabel('number of selected sensors')
    ylabel('performance gap')
    legend('proposed strategy','random strategy') 
    figure(2)
    grid
    hold on
    plot(smooth((trb_m-tr1_m)./tr1_m,5),'r','LineWidth',1.5);
    %plot(smooth((trr_m-tr1_m)./tr1_m,4),'LineWidth',1.5);
    xlabel('number of selected sensors')
    ylabel('performance gap')
    %legend('strategy in [7]','random strategy')
end


% 
% figure
% grid
% hold on
% plot(smooth((trb_m-tr1_m)./tr1_m,4),'r','LineWidth',1.5);
% %plot(smooth((trr_m-tr1_m)./tr1_m,4),'LineWidth',1.5);
% xlabel('number of selected sensors')
% ylabel('performance gap')
% legend('strategy in [7]','random strategy')

