clear all
close all
clc
p0=1;
p_end=25;

T_end=10;
for  N=1:20
    init
    Sigma_init=Sigma;
    for p=p0:p_end
        
        name = sprintf('%04d_%02d_%02d_%02d_%02d_%02d',round(clock));
        %------------------------1 step----------------------%
        Sigma=Sigma_init;
        step=1;
        for time=1:T_end
        vars
        optimization1
        display2
        end
        tr1_vet{N,p-p0+1}=tr;
        tr
%         tr1_5(N,p-p0+1) = tr(5);
%         tr1_10(N,p-p0+1) = tr(end);
        
        
        clear tr 
        %------------------------ boyd ----------------------%
        step = 1;
        Sigma=Sigma_init;
        for time=1:T_end
        vars
        boyd
        display2
        end
        trb_vet{N,p-p0+1}=tr;
        tr
%         trb_5(N,p-p0+1) = tr(5);
%         trb_10(N,p-p0+1) = tr(end);

        clear tr 
        N
        
        save dati tr1_vet trb_vet p0 p_end
    end
end