%clc
%clear
rand('twister',sum(clock)*100)
n = 20;
m = 50;

T = 2;
A = rand(n);

C = rand(m,n);

Q = rand(n);
Q = Q*Q';

R = eye(m);
Sigma = rand(n);
Sigma = Sigma * Sigma';
step = 1;