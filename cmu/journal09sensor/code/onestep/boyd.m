alpha = 0.8;
beta = 0.5;
k = 0.1;
t = 1;
z = ones(m,1)/m*p;
grad = zeros(m,1);
I = inv(A*Sigma*A' + Q);


tmp = inv(I + C'*diag(z)*C);

for iteration2 = 1:5
    value = trace(tmp) - k * sum(log(z)) - k * sum(log(1-z));

    for iteration = 1:10
        for i = 1:m
            grad(i) = - norm(tmp*C(i,:)',2)^2 - k/z(i) + k/(1-z(i));
        end

        vec = (eye(m) - ones(m)/m)*grad;

        t = 1;
        while true
            newz = z - t*vec;
            if max(newz)>=1 | min(newz) <= 0
                t = alpha * t;
                continue
            end
            newtmp =  inv(I + C'*diag(newz)*C);

            newvalue = trace(newtmp) - k * sum(log(newz)) - k * sum(log(1-newz));

            if newvalue <= value - t * beta * vec' * grad
                z = newz;
                tmp = newtmp;
                value = newvalue;
                break;
            end

            t = alpha * t;

        end
        if norm(grad,2) <= 0.001
            break
        end
    end
    k = 0.1*k;
end

[z index]=sort(z,'descend');
gamma = zeros(1,m);
gamma(index(1:p)) = 1;