clear all
close all 

load DATA_REAL

trr_m=mean(trr);
tr1_m=mean(tr1);
tr_real_opt_m=mean(tr_real_opt);
 plot(smooth((trr_m-tr_real_opt_m)./tr_real_opt_m),'--','LineWidth',1.5);
 hold on
 plot(smooth((tr1_m-tr_real_opt_m)./tr_real_opt_m),'r','LineWidth',1.5);
 xlabel('number of selected sensors')
 ylabel('performance gap')
 legend('random strategy','proposed strategy') 