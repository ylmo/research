clear all
close all
clc
p0=1;
p_end=20;

for  N=1:50
    init
    Sigma_init=Sigma;
    for p=p0:p_end
        
        name = sprintf('%04d_%02d_%02d_%02d_%02d_%02d',round(clock));
        %------------------------1 step----------------------%
        Sigma=Sigma_init;
        step=1;
        vars
        optimization1
        display2
        tr1(N,p-p0+1) = tr;
        clear tr 
        %------------------------ real_opt ----------------------%
        
        
        Sigma=Sigma_init;
        step = 1;
        index=1:p;
        gamma = zeros(1,m);
        gamma(index) = 1;
        display2
        Opt_temp=tr;
        cont=m-p+1:m;
        for i=1:binomial(m,p)-1
            temp=mod(index(end)+1,m+1);
            ind=max(find(index-cont<0));
            for j=length(index):-1:1
                if temp==0                    
                    index(j)=index(ind)+1+(j-ind);   
                    temp=mod(index(j-1)+1,m+1-p+j-1);
                else
                    index(j)=temp;
                    if j>1
                    temp=index(j-1);
                    end
                end
            end
        Sigma=Sigma_init;
        step = 1;
        gamma = zeros(1,m);
        gamma(index) = 1;
        display2
        Opt_temp=min(Opt_temp,tr);    
        end
        tr_real_opt(N,p-p0+1) = Opt_temp;
        clear tr 
        %------------------------random----------------------%
        step = 1;
        Sigma=Sigma_init;
        index=randperm(m);
        gamma = zeros(1,m);
        gamma(index(1:p)) = 1;
        display2
        trr(N,p-p0+1) = tr;
        clear tr 
    end
    N
end
save dati_real_2