%clc
%clear
rand('twister',sum(clock)*100)

%generate network graph
gridlength = 3;
n = gridlength^2 ;
m = 20;
xaxis = (gridlength-1) * rand(1,m) + 1;
yaxis = (gridlength-1) * rand(1,m) + 1;
xaxis(1) = gridlength/2 + 0.5;
yaxis(1) = gridlength/2 + 0.5;
graph = zeros(m);
for i = 1:m
    for j = 1:m
        graph(i,j) = (xaxis(i)-xaxis(j))^2 + (yaxis(i)-yaxis(j))^2;
    end
end
baseenergy = 1;
graph = graph + baseenergy;



%generate A matrix for heat transfer system
coef = 0.1;
A = eye(n);
for i = 1:gridlength
    for j = 1:gridlength
        k = (i-1)*gridlength + j;
        if i >= 2
            l = (i-2)*gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if i <= gridlength - 1
            l = i * gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j >= 2
            l = (i-1)*gridlength + j - 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j <= gridlength - 1
            l = (i-1)*gridlength + j + 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
    end
end

%generate C matrix
C = zeros(m,n);
for i = 1:m
    x = xaxis(i);
    y = yaxis(i);
    xgrid = floor(x);
    ygrid = floor(y);
    for j = 0:1
        for k = 0:1
            l = (xgrid+j-1) * gridlength + ygrid + k;
            C(i,l) = (1-abs(xgrid+j-x))*(1-abs(ygrid+k-y));
        end
    end
end



[m n]=size(C);


T = 2;

Q = rand(n);
Q = Q*Q';

R = eye(m);
Sigma = rand(n);
Sigma = Sigma * Sigma';
step = 1;