Gstar = zeros(n,m*T,T);
H = zeros(n,n,T);
P = A*Sigma*A' + Q;
K = P * C' *inv(C*P*C'+R);
P = P - K*C*P;
Pstar(1:n,1:n,1) = P;
Gstar(:,1:m,1) = K;
for i = 2:T
    P = A*P*A' + Q;
    K = P * C' *inv(C*P*C'+R);
    P = P - K*C*P;
    Pstar(:,:,i) = P;
    Gstar(:,1:i*m,i) = [(A-K*C*A)*Gstar(:,(1:(i-1)*m),i-1) K];
end
tmpa = zeros(n*T,n*(T+1));
tmpc = zeros(m*T,n*T);
tmpw = zeros(n*(T+1),n*(T+1));
tmpw(1:n,1:n) = Sigma;
tmpr = zeros(m*T,m*T);
for i = 1:T
    for j = 1:i+1
        tmpa (((i-1)*n+1):i*n,((j-1)*n+1):j*n) = A^(i+1-j);
    end
    tmpc (((i-1)*m+1):i*m,((i-1)*n+1):i*n) = C;
    tmpw ((i*n+1):(i+1)*n,(i*n+1):(i+1)*n) = Q;
    tmpr (((i-1)*m+1):i*m,((i-1)*m+1):i*m) = R;
end
COV = tmpc*tmpa*tmpw*tmpa'*tmpc'+tmpr;
S = zeros(m*T,m*T,T);
for i = 1:T
    S(1:i*m,1:i*m,i) = sqrtm(COV(1:i*m,1:i*m));
    Gstar(:,:,i) = Gstar(:,:,i)*S(:,:,i);
end