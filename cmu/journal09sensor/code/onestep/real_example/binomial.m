function x=binomial(a,b)

num=1;

for i=1:a
    num=num*i;
end

den1=1;

for i=1:b
    den1=den1*i;
end

den2=1;

for i=1:a-b
    den2=den2*i;
end

x=num/(den1*den2);
end

