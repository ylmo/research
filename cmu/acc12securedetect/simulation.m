pplus = 0.51;
pminus = 0.49;

a = 2;

zeta = 0;
for j = 1:50
    ratio = pminus * Qfunc(-zeta - a)^34/(pplus * Qfunc(zeta-a)^34);
    zeta = log(ratio)/2/a
end