This paper studies the problem of estimating a scalar quantity based on
noisy measurements, where some of the measurements can be corrupted by
an attacker.  The authors show that when the attacker corrupts more
than half of the measurements, the best estimator is a constant
estimator based on the a priori distribution of the random variable. 
When the attacker corrupts fewer than half of the measurements, the
authors provide an optimal estimator that is a selection from a set of
"local estimators", each of which is an estimator that uses only a
subset of the measurements.  

The results in this paper are generally interesting, and are
potentially appropriate for a technical note.  A few aspects of the
paper should be clarified.  

-- The cost function is assumed to be convex on page 5.  It seems that
the authors also assume nonnegative and increasing?  In fact, it seems
that for most of the results in this paper, assuming the cost function
is increasing in |e| is sufficient.  The convexity of c only seems to
come into play in the proof of Theorem 4.  Since an increasing cost in
|e| is arguably more relevant to this application, it might be worth
framing the results for this assumption, rather than the convex
assumption.

-- In the proof of Theorem 2, the authors state that the optimal
estimator "must be" a constant estimator.  While the authors have
argued that a constant estimator achieves the optimal, can other
estimators achieve the same cost?  A comment on this would be useful.

-- The definition of the set of estimators \varphi_K^* in equation 25
is unclear.  Each \varphi_K^* is defined for a fixed set K of 2l
sensors.  However, equation 25 is optimizing over different sets K of
2l sensors.  Each estimator \varphi_K^* in the set should be explicitly
defined.

-- In the proof of Lemma 3, doesn't step 1 follow from the standard
fact that min_x max_y f(x,y) >= max_y min_x f(x,y)?  
