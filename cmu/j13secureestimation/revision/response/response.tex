%\documentclass[9pt,technote,letterpaper]{IEEEtran}

%\documentclass[10pt,letterpaper]{IEEEtran}
\documentclass{article}
\usepackage{amsmath}
\usepackage{amsfonts,amssymb}
\usepackage{tikz} %add back

\newcommand{\alert}[1]{\textcolor{red}{#1}}
\author{Yilin Mo Bruno Sinopoli}

\title{Answers to the reviewer's comments}

\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their comments. The detailed responses are listed as below:


\section{Response to Reviewer \#1}
\emph{ This paper studies the problem of estimating a scalar quantity based on noisy measurements, where some of the measurements can be corrupted by an attacker.  The authors show that when the attacker corrupts more than half of the measurements, the best estimator is a constant estimator based on the a priori distribution of the random variable. When the attacker corrupts fewer than half of the measurements, the authors provide an optimal estimator that is a selection from a set of ``local estimators'', each of which is an estimator that uses only a subset of the measurements.  }

\emph{ The results in this paper are generally interesting, and are potentially appropriate for a technical note.  A few aspects of the paper should be clarified. }
\begin{enumerate}
  \item \emph{The cost function is assumed to be convex on page 5. It seems that the authors also assume nonnegative and increasing?}

    We thank the reviewer for the insightful comments. We do not assume that $c$ is non-negative and increasing in the paper. We want to provide the most general results, even though for most practical applications, the cost function is non-negative.
    
    \emph{In fact, it seems that for most of the results in this paper, assuming the cost function is increasing in $|e|$ is sufficient.  The convexity of $c$ only seems to come into play in the proof of Theorem 4.  Since an increasing cost in $|e|$ is arguably more relevant to this application, it might be worth framing the results for this assumption, rather than the convex assumption.}

    Indeed, the convexity of the cost function $c$ is only used to prove Theorem~4. The rest of the results only require that $c$ is quasi-convex with respect to $e$, which is even weaker than monotonically increasing in $|e|$. We have changed the paper accordingly to make our assumption more general.


  \item \emph{In the proof of Theorem 2, the authors state that the optimal estimator ``must be'' a constant estimator. While the authors have argued that a constant estimator achieves the optimal, can other estimators achieve the same cost?  A comment on this would be useful.}

    The reviewer is correct. The optimal estimator may not necessarily be unique. Hence, it is possible that a non-constant estimator also achieve the optimal performance. We have changed the statement of the theorem accordingly.

  \item \emph{The definition of the set of estimators $\varphi_K^*$ in equation 25 is unclear.  Each $\varphi_K^*$ is defined for a fixed set $K$ of $2l$ sensors. However, equation 25 is optimizing over different sets $K$ of $2l$ sensors. Each estimator $\varphi_K^*$ in the set should be explicitly defined.}

    We have changed $\varphi_{\mathcal K}$ to $\{\varphi_{\mathcal K}\}$ to indicate that the minimization is over a set of estimators instead of one single estimator.

  \item \emph{In the proof of Lemma 3, doesn't step 1 follow from the standard fact that $min_x max_y f(x,y) \geq max_y min_x f(x,y)$?  }

    The first step does not follow from the fact that 
    \begin{displaymath}
      \min_{x\in X}\max_{y\in Y} f(x,y) \geq \max_{y\in Y} \min_{x\in X} f(x,y) .
    \end{displaymath}
    The reason is that we require $\mathcal I \bigcap \mathcal J = \emptyset$. It is not necessarily true that
    \begin{equation}
      \min_{x\in X}\max_{y\neq x,y\in Y} f(x,y) \geq \max_{y\in Y} \min_{x\neq y, x\in X} f(x,y) .
      \label{eq:minmax}
    \end{equation}
    For example, suppose that $X = \{0,1\}$ and $Y = \{0,1\}$, then 
    \begin{displaymath}
      \min_{x\in X}\max_{y\neq x,y\in Y} f(x,y) = \min( f(0,1),f(1,0)),
    \end{displaymath}
    and
    \begin{displaymath}
      \max_{y\in Y} \min_{x\neq y,x\in X}f(x,y) = \max( f(0,1),f(1,0)),
    \end{displaymath}
    which implies that 
    \begin{displaymath}
      \min_{x\in X}\max_{y\neq x, y\in Y} f(x,y) \leq \max_{y\in Y} \min_{x\neq y,x\in X} f(x,y),
    \end{displaymath}
    which is the opposite of \eqref{eq:minmax}.
\end{enumerate}
\newpage
\section{Response to Reviewer \#2}
\emph{The subject of the paper under revision is the problem of estimating a real variable from measurements corrupted by an attacker. The attacker has knowledge of the variable to be estimated, of the estimation algorithm, and of the whole set of measurements. The attacker can arbitrarily compromise only a subset of measurements of fixed cardinality. It is shown that, if the attacker compromises more than half measurements, then an optimal estimator only relies on prior knowledge of the variable to be estimated, without using the sensor measurements. If the attacker corrupts less than half measurements, then conditions to derive an optimal estimator are discussed. Finally, a numerical example is presented.}

\emph{The considered topic is of timely interest, and the proposed analysis is novel. On the negative side, (i) the presentation is at times confusing and unclear, and (ii) the contribution and the applicability of the proposed method need to be clarified. Additionally, (iii) a more detailed comparison with the existing literature on the topic should be included.}

We thank the reviewer for the insightful comments. We changed some notations to make the paper more consistent. Furthermore, the introduction has been rewritten to clarify the contributions of the paper and the applicability of the proposed methodology.

\emph{Below are some specific comments:}

\begin{enumerate}
  \item \emph {Several symbols and functions are introduced in the paper. Often, the adopted notation does not reflect the meaning of the variables ($f$ for estimation function, $\gamma$ for sensor selection vector, $\varphi$ and $\psi$ for estimators...). Additionally, variables are not properly recalled or defined in the statements of Lemmas and Theorems. As a result, the paper does not read well, and statements requires extra effort to be understood. Sometimes, the notation might also be simplified for clarity: at the beginning of Section II, the notation $\gamma \circ y^a$ to identify the vector introduced by the attacker could be simply written as $y^a$, where $y^a$ is a sparse vector with certain support.}

    We have changed some notations to make the paper more consistent. To summarize, $\mathcal I,\mathcal J,\mathcal K$ represent a subset of sensors. $f,g,h$ represent estimators. $\varphi,\psi,\tau,\rho$ represent local estimators. The use of $\gamma$ to represent sensor selection vector is fairly standard in the literature. For example, it is used in the following paper:
   
L. Shi and P. Cheng and J. Chen, "Optimal Periodic Sensor Scheduling With Limited Resources," Automatic Control, IEEE Transactions on , vol.56, no.9, pp.2190,2195, Sept. 2011

    For the $\circ$ notation, we feel that it greatly simplifies the proofs of Lemma~1 and 3. Using the sparsity notation would instead makes the proofs more complicated.

  \item \emph{Theorem 2 is confusing. First, is $\delta^*$ the minimizer of (19) or the solution (i.e., minimum value) of (19)?}
  
We adopt the notations in the following book:

S. Boyd and L. Vandenberghe, {\it Convex Optimization}. Cambridge University Press, 2004.

A \emph{solution} of a minimization (maximization) problem is the minimizer (maximizer) of the problem.

\emph{Second, what is the domain of the minimization problem?}

The statement of the theorem has been changed to clearly define the domain of the optimization problem.

\emph{Third, it is confusing that an estimator is equal to a real value. It is probably meant that the estimation function is constant with respect of the sensor measurements; yet, statements should be more rigorous. It may help to state a formal definition of estimator, and estimation function.} 

The estimation function is a mapping from the space of compromised measurements $\mathbb R^m$ to the space of state estimate $\mathbb R$. We added definition 1 to formally define the notion of estimator.

\emph{Similar comments apply to Theorem 3, and Theorem 6.}

The statement of Theorem 3 and 6 has been changed accordingly.

\item \emph{The paper is written in very general terms. As a drawback, the applicability of the proposed analysis is unclear. For instance in Section IV, it seems that the original problem is only restated as different optimization problems, whose solution is not analyzed. Then, what is the contribution of the paper for the design of optimal estimators in the case of measurements corrupted by an attacker? The numerical example in Section VI does not clarify this point. In fact, although a simple scenario is considered, the proposed solution is only an approximated one, and it is stated that it remains an open problem to find optimal parameters $t_i$.}

We would like to point out that it is unclear how (12) can be solved using direct method. Hence, the main contribution of the paper is to prove that the optimal estimator for (12) has a special form and it can be derived as the solution of a convex optimization problem. Furthermore, we could approximately solve the convex optimization problem by using finite-degree polynomials. 

The true optimal function is an interesting theoretical question, which we would like to investigate in the future. However, in practice, a sufficiently high-order polynomial may be an accurate enough approximation, which can be solved using the methodology proposed in this paper.

\item \emph{The main result in Section IV (Theorem 3) states that an optimal estimator can be designed by solving the optimization problems in eq (24) and (25). It is not clear why the optimization problems (24) and (25) are easier than the original problem of finding an optimal estimator.}

  In the original problem, the optimal estimator may be an arbitrary function. Furthermore, the expected cost $\mathcal C(f)$ may not be necessarily convex with respect to $f$. As a result, it is unclear how equation (12) can be solved directly. In this paper, we prove that the optimal estimator has a specific form and can be derived as the optimal solution of a convex optimization problem (25).

\item \emph{ In Definition 6, the statement before eq (22) is incomplete (``...  holds for all...'').}

Fixed
\item \emph{To improve readability and to better motivate the analysis in the paper, it may be beneficial to present a detailed example early in the manuscript.}

  We thank the review for this suggestion to improve the quality of the paper.. Unfortunately, due to the space limit, we could not add an additional example. We have improved the Numerical Examples Section to better demonstrate the performance of our estimator.

\item \emph{The paper shows that, if the attacker compromises more than half measurements, then no information can be extracted from the measurements. This finding recalls some classic results on Byzantine generals. Is there any connection with the literature of Byzantine generals or a different explanation of the proposed results?}

It is possible that there exists a connection between our results and the Byzantine general problem. However, we feel that the connection will be non-trivial for the following reasons:
\begin{enumerate}
  \item In Byzantine general problem, each general needs to make an individual decision. In our problem, only the estimator needs to compute the state estimate. 
  \item In Byzantine general problem, all loyal generals need to reach the same decision. However, in our problem, the estimator is allowed to have estimation error.
  \item In Byzantine general problem, the decision is a binary variable. In our case, the state estimate is a real number. Furthermore, in our case, there is a-priori knowledge on the distribution of the state.
  \item The classical Byzantine general problem not solvable when more than one third of the general is malicious. In our case, the optimal estimator becomes trivial when more than half of the sensor is malicious.
\end{enumerate}
As a result, we feel that the connection cannot be explained clearly in the paper within the page limit. Furthermore, we think the main focus of the paper is on the case where $l < m/2$ rather than the case where $l \geq m/2$. Therefore, the discussion on the Byzantine general problem is not included in the revised paper.

\item \emph{Is an optimal estimator unique? The wording ``the optimal estimator'' appears in the paper, yet no uniqueness proof is present.}

  The optimal estimator may not necessarily be unique. We have added a footnote to clarify this issue.

\item \emph{In Definition 7, what is $\mathcal S$?}

  $\mathcal S$ is defined as the set of all sensors at the beginning of the Problem Formulation Section.
\end{enumerate}
\newpage
\section{Response to Reviewer \#4}
\emph{This paper concerns estimation under possible integrity attacks on a subset of the available measurements. This is a relevant and timely research problem. The paper takes a rather theoretical, probabilistic, approach to study the problem, but nevertheless obtains some simple conclusions (such as the criticality of securing at least half the sensors). The proposed estimation policy seems rather prohibitive from a computational point of view, however.}

\emph{The paper is technically correct as far as I can judge. In general, the paper is well written and clearly presented (see comments below, however).  }

\emph{Other comments:}
\begin{enumerate}
  \item \emph{ I happened to also review the paper ``Resilient Detection in the Presence of Integrity Attacks'', which the authors submitted to IEEE Transactions on Signal Processing in 2013. I would find it useful if the authors could comment on similarity and differences between that paper and this.}

    We revised the introduction section to mention our TSP paper. On the high level, this paper can be seen as an extension to the TSP paper since we consider real instead of binary state variables and some results do resemble the results in TSP paper, especially for the case where $l \geq m/2$. However, the techniques used in this paper and the TSP paper are quite different and the optimal estimator/detector are different for the case where $l < m/2$. Unfortunately, a very detailed comparison between the two paper cannot be included due to the space limit. 

  \item \emph{ Page 3: I find the discussion under ``related work'' concerning the difference between robust and secure estimators confusing. Robust H-infinity-type estimators are also of worst-case type, and has a game-theoretic interpretation. Please clarify.}

    We have added the discussion on $H_\infty$ estimators. The main differences between the $H_\infty$ approach and our approach are as follows:
    \begin{enumerate}
      \item The $H_\infty$ estimator is a dynamic estimator, while our estimator is static. 
      \item The $H_\infty$ estimator can be considered as the ``worst-case'' estimator when the disturbance is an $L_2$ function or has a bounded power spectra density. On the other hand, our estimator is the ``worst-case'' estimator when the disturbance is sparse, which may be reasonable for security settings, since the attacker could arbitrarily change the compromised sensor readings.  
    \end{enumerate}

  \item \emph{I find Lemma 1 a bit unclear. Does $\delta$ not depend on $\cal I$ and $\cal J$? Should it not say "for all $y$" after (20)?}

    $\delta$ may depends on $\mathcal I$ and $\mathcal J$. We have changed the statement to clarify the issue.

  \item \emph{Theorem 3: Please be more specific in theorem statement. Compare with Theorem 2. The estimator is only optimal under a certain assumption on the attacker.}

    We have changed the statement of Theorem 3 and 5 to clearly state that they only apply to the case where $l < m/2$.

  \item \emph{Theorem 3: Please specify which function class you optimize over in (25).}

    We have changed the statement to indicate that $\varphi_k$ needs to be a local estimator. 

  \item \emph{ Remark 5: What do you mean by homogeneous sensors? That they are identical?}

    We have changed the word ``homogeneous'' to ``statistically identical'' to avoid confusions.

  \item \emph{ Remark 7: Does the computational cost $O(m \log m)$ include solving (42)?}

    No, the computational cost does not include the cost of solving the optimization problem, since the optimization problem can be solved off-line. We have changed Remark 7 to address this issue.

  \item \emph{ It is shown that the performance of the secure estimator in the case $m=2l+1$ is necessarily bad. Can the authors say anything about how the performance improves as $l$ decreases? If the rate of improvement is very low, perhaps the taken worst-case scenario is too restrictive to be practically useful.}

    Another example has been added in the simulation section to illustrate the performance of the ``worst-case'' estimator when $l$ is fixed and $m$ goes to infinity.

\end{enumerate}
\emph{Minor comments/typos:}
\begin{enumerate}
  \item \emph{Page 4, line 4: $x->x \in R$?}

    Fixed.
  \item \emph{Page 8, "several intermediate steps" Only seem to be one Lemma.}

    Fixed.
  \item \emph{Page 8, There is a minor notational overload since $y_i$ is used both for element $i$ of $y$, and for signal $i$ ($y_1$ and $y_2$).}

    $y_1$ and $y_2$ have been changed to $y$ and $y'$ respectively.
  \item \emph{ Page 9, Def. 6: for all $->$ for all $y$?}

    Fixed.
  \item \emph{ Page 17: line -3: $\hat x = \dots/3$}

    It is indeed $\hat x = (y_1+y_2+y_3)/4$ since we have the prior knowledge that $x$ is a standard normal random variable. On the other hand, $\hat x = (y_1+y_2+y_3)/3$ is the maximum likelihood estimator when there is no prior knowledge on $x$.

\end{enumerate}
\end{document}
