Dear authors,

We have received three reviews for your submitted manuscript. The
reviewers are generally appreciative of the results but point out
several ways in which the manuscript can be improved.

Apart from addressing all comments made by the reviewers, the revised
version of the manuscript needs to pay particular attention to the
following issues (some of which were raised by one or more reviewers):

1) The discussion on the motivation and applicability of the results in
the paper needs to be improved (see the comments by Reviewer 2 in
Review 60388).

2) Consider the suggestion of Reviewer 1 (in Review 48853) regarding
the properties that the cost function needs to satisfy. This can
potentially generalize the results and also clarify exactly what is
needed.

3) There are several instances where notation is confusing. The
reviewers have a lot of complaints (and suggestions) about how notation
can be simplified and precision can be improved (see, for example, the
comments by Reviewer 2 in Review 60388). Both Reviewers 1 and 2 point
out that the optimal estimator may not be unique and this issue needs
to be clarified.

4) Connections to earlier work (e.g., Byzantine generals type problems,
robust estimation) as well as your own work should be better
articulated. In particular, see the comments of Reviewer 4 in Review
36027.
