The subject of the paper under revision is the problem of estimating a
real variable from measurements corrupted by an attacker. The attacker
has knowledge of the variable to be estimated, of the estimation
algorithm, and of the whole set of measurements. The attacker can
arbitrarily compromise only a subset of measurements of fixed
cardinality. It is shown that, if the attacker compromises more than
half measurements, then an optimal estimator only relies on prior
knowledge of the variable to be estimated, without using the sensor
measurements. If the attacker corrupts less than half measurements,
then conditions to derive an optimal estimator are discussed. Finally,
a numerical example is presented.

The considered topic is of timely interest, and the proposed analysis
is novel. On the negative side, (i) the presentation is at times
confusing and unclear, and (ii) the contribution and the applicability
of the proposed method need to be clarified. Additionally, (iii) a more
detailed comparison with the existing literature on the topic should be
included. Below are some specific comments:

1) Several symbols and functions are introduced in the paper. Often,
the adopted notation does not reflect the meaning of the variables (f
for estimation function, \gamma for sensor selection vector, \phi and
\psi for estimators...). Additionally, variables are not properly
recalled or defined in the statements of Lemmas and Theorems. As a
result, the paper does not read well, and statements requires extra
effort to be understood. Sometimes, the notation might also be
simplified for clarity: at the beginning of Section II, the notation
\gamma \circle y^a to identify the vector introduced by the attacker
could be simply written as y^a, where y^a is a sparse vector with
certain support.

2) Theorem 2 is confusing. First, is \delta^* the minimizer of (19) or
the solution (i.e., minimum value) of (19)? Second, what is the domain
of the minimization problem? Third, it is confusing that an estimator
is equal to a real value. It is probably meant that the estimation
function is constant with respect of the sensor measurements; yet,
statements should be more rigorous. It may help to state a formal
definition of estimator, and estimation function.

Similar comments apply to Theorem 3, and Theorem 6.

3) The paper is written in very general terms. As a drawback, the
applicability of the proposed analysis is unclear. 

For instance in Section IV, it seems that the original problem is only
restated as different optimization problems, whose solution is not
analyzed. Then, what is the contribution of the paper for the design of
optimal estimators in the case of measurements corrupted by an
attacker? The numerical example in Section VI does not clarify this
point. In fact, although a simple scenario is considered, the proposed
solution is only an approximated one, and it is stated that it remains
an open problem to find optimal parameters t_i.

4) The main result in Section IV (Theorem 3) states that an optimal
estimator can be designed by solving the optimization problems in eq
(24) and (25). It is not clear why the the optimization problems (24)
and (25) are easier than the original problem of finding an optimal
estimator.

Minor comments:
5) In Definition 6, the statement before eq (22) is incomplete ("...
gold for all...").

6) To improve readability and to better motivate the analysis in the
paper, it may be beneficial to present a detailed example early in the
manuscript.

7) The paper shows that, if the attacker compromises more than half
measurements, then no information can be extracted from the
measurements. This finding recalls some classic results on Byzantine
generals. Is there any connection with the literature of Byzantine
generals or a different explanation of the proposed results?

8) Is an optimal estimator unique? The wording "the optimal estimator"
appears in the paper, yet no uniqueness proof is present.

9) In Definition 7, what is S?