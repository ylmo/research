for  m = 3
    exp = 0;
    while exp < 10
        x = randn(1000,1);
        y = zeros(1000,m);
        for i = 1:m
            y(:,i) = randn(1000,1) + x;
        end
        y = sort(y,2);
        z = zeros(1000,2);
        z(:,1) = sum(y(:,1:(m-2)),2);
        z(:,2) = sum(y(:,3:m),2);
        
        cvx_begin
        variable e(1000,1)
        variable a
        minimize(e'*e/1000)
        subject to
        e >= abs(a*z(:,1) - x)
        e >= abs(a*z(:,2) - x)
        cvx_end
        if strcmp(cvx_status,'Solved')
            opt(m) = opt(m) + cvx_optval;
            exp = exp + 1;
        end
    end
end
opt = opt/10;
