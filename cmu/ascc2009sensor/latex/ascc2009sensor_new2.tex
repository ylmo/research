\documentclass[letterpaper,10 pt,conference]{IEEEconf}
% use above line letter sized paper
\overrideIEEEmargins
% Needed to meet printer requirements.

%DEBUG:
\IEEEoverridecommandlockouts

\usepackage{color}
\usepackage{float}
\usepackage{ifpdf}
\usepackage[dvips]{graphicx}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}
\usepackage[dvips]{epsfig}

\newcommand{\MAEF}{\mbox{\sc Maximum Available Energy First}}
\newcommand{\MEDF}{\mbox{\sc Mimimum Energy Deduction First}}
\newcommand{\Hybrid}{\mbox{\sc Hybrid Scheme}}

\newtheorem{theorem}{Theorem}[section]
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{conjecture}[theorem]{Conjecture}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{remark}[theorem]{Remark}
\newtheorem{comments}[theorem]{Comments}
\newtheorem{fact}[theorem]{Fact}
\newtheorem{example}[theorem]{Example}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{assumption}[theorem]{Assumption}
\newtheorem{problem}[theorem]{Problem}
% \newtheorem{algorithm}[theorem]{Algorithm}
\newtheorem{acknowledgment}[theorem]{acknowledgment}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{\LARGE \bf
Network Lifetime Maximization via Sensor Selection}
\author{Yilin $\mathrm{Mo}^*$, Ling $\mathrm{Shi}^{\dag}$, Roberto $\mathrm{Ambrosino}^{\ddag}$ and Bruno $\mathrm{Sinopoli}^*$
\thanks{$*$: Department of Electrical and Computer Engineering, Carnegie Mellon University. Email: ymo@andrew.cmu.edu, brunos@ece.cmu.edu.}
\thanks{$\dag$: Electronic and Computer Engineering, Hong Kong University of Science and Technology. Email: eesling@ust.hk}
\thanks{$\ddag$: Dipartimento per le Tecnologie, Universit\`{a} degli Studi di Napoli Parthenope. Email: ambrosino@uniparthenope.it}
\thanks{The work by Y.~Mo and B.~Sinopoli is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}
\maketitle \thispagestyle{empty} \pagestyle{empty}

%\tableofcontents

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{abstract}
%In the paper we consider the state estimation carried over a sensor network. At each time step, only a subset of all sensors are selected to send their observations to the fusion center, where a Kalman filter is implemented to perform the state estimation. The sensors are selected to maximize the lifetime of the network while maintaining a desired quality of state estimation accuracy. We discuss two selection algorithm, one is of less computation complexity and the other gives better performance in term of lifetime. A real world example is provided to demonstrate both algorithms.

In this paper we consider the state estimation carried over a sensor network. At each time step, only a subset of all sensors are selected to send their observations to the fusion center, where a Kalman filter is implemented to perform the state estimation. The sensors are selected to maximize the lifetime of the network while maintaining a desired quality of state estimation accuracy. We propose two selection algorithm: one is maximum energy available first algorithm, and the other is a heuristic algorithm, based on convex optimization, for approximately solve the problem . A real world example is presented to further illustrate the efficiency of both algorithm.
\end{abstract}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\section{Introduction}\label{section:introduction}

Advances in fabrication, modern sensor and communication technologies, and computer architecture have boosted the development of sensor networks which have a wide range of applications, including environment and habitat monitoring, health care, home and office automation, and traffic control~\cite{wireless_sensor_network}.  In many of these applications, there is an economic incentive towards using off-the-shelf sensors which are typically battery powered. A consequence of this is that the individual hardware components might be of relatively low quality and that communication resources are quite limited. In many cases, a set of sensors are needed in order to provide a guaranteed level of estimation quality. One of the key design problems here is to determine which set of sensors to be used at each time to maximize the lifetime (to be defined in Section~\ref{section:problem_setup}) of such a network which always provides a guaranteed level of estimation quality.

The problem of state estimation of a linear time-invariant system over a digital communication channel that has a finite bandwidth capacity was introduced by Wong and Brockett \cite{Wong1,Wong2} and further pursued by others (e.g., \cite{Brockett1,Nair1,Petersen}).  Sinopoli et al. \cite{sinopoli} discussed how packet loss can affect state estimation. They showed there exists a certain threshold of the packet loss rate above which the state estimation error diverges in the expected sense, i.e., the expected value of the error covariance matrix becomes unbounded as time goes to infinity.  They also provided lower and upper bounds of the threshold value. Following the spirit of \cite{sinopoli}, Liu and Goldsmith \cite{xiangheng} extended the idea to the case where there are multiple sensors and the packets arriving from different sensors are dropped independently. They provided similar bounds on the packet loss rate for a stable estimate, again in the expected sense. Different from the previous work, the stability of the Kalman filter was investigated via a probabilistic approach in~\cite{shi-epstein-tiwari-murray}.

Sensor network energy minimization and lifetime maximization problems have been hot areas of research over the past few years, as one of the critical constraints of such networks is limited energy resources available. Sensor network energy minimization is typically done via efficient MAC protocol design~\cite{mac_sensor_network}, or via efficient scheduling of the sensor states~\cite{wake_up_scheme, sleep_schedule}. Yu et al.~\cite{energy_management} proposed a scalable topology and energy management scheme in wireless sensor networks. Xue and Ganz~\cite{lifetime_analysis} showed that  the lifetime of the sensor networks is influenced by transmission schemes, network density and transceiver parameters with different constraints on network mobility, position awareness and maximum transmission range.  Chamam and Pierre~\cite{optimal_state_scheduling} proposed a sensor scheduling scheme to optimally put sensors in active or inactive modes. A sensor transmitting scheduling was suggested by Chen et al.~\cite{transmission_scheduling}. Lai et al.~\cite{improve_sensor_lifetime} proposed a scheme to divide the deployed sensors into disjoint subsets of sensors such that each subset can complete the mission, and then maximized the number of such disjoint subsets. Similar approaches can be found in~\cite{energy_efficient_organization} where the sensors are partitioned into groups which are successively scheduled to be active for sensing and delivering data. Shi et. al~\cite{Ling_cdc07} considered sensor energy minimization as a means to maximize network lifetime while guaranteeing a desired quality of estimation accuracy. They~\cite{shi-febid08} further propose a sensor tree scheduling algorithm which leads to longer lifetime of the network.

Sensor network lifetime maximization under estimation quality constraint is studied in this paper. We define the lifetime of such a network to be the first time that the network stops to provide a desired level of estimation quality. To the best of our knowledge, this definition of the lifetime of a sensor network is first introduced in this paper. Under this notion, different scheduling policies (i.e., choosing a set of sensors at each time) may lead to different lifetime of the network, we are thus interested in finding the optimal scheduling policy such that the lifetime of the network is maximized.

The rest of the paper is organized as follows. In Section~\ref{section:problem_setup}, we first formally define the lifetime of a sensor network and introduce the mathematical setup of the problem. We then present the main results in Section~\ref{section:main}. Simulation results are provided in Section~\ref{section:simulation} to demonstrate the theory and algorithms. In Section~\ref{section:extension}, we describe several variations and extensions on the network lifetime maximization problem, which can be incorporated in the convex optimization framework. Concluding remarks and future work are provided in Section~\ref{section:conclusion}.

\section{Problem Statement}\label{section:problem_setup}

Consider a linear system
\begin{equation}
  \begin{split}
    x_{k+1} &= Ax_k + w_k,\\
    y_{k} &= C x_k + v_k.
  \end{split}
  \label{eq:systemdiscription}
\end{equation}
where $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. We suppose that $x_k \in \mathbb R^n$ and $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}  ]^T \in \mathbb R^m$ is the vector of the measurements from the sensors. Each element $y_{k,i}$ represents the measure of the sensor $i$ at time $k$.

Assume that the sensor nodes are battery powered. Let $E_{k, i}$ denote the remaining energy of sensor $S_i$ after time $k$ and define $E_k \triangleq [E_{k,1},\ldots,E_{k,m}]^T$. Without introducing conservatism, we also assume that the energy cost for $S_i$ sending a measurement packet to the fusion center is $1$.

 \begin{figure}[H]
   \begin{center}
   \includegraphics{pic.1}
    \caption{Sensors Selected at time $k$} \label{fig:sensor_topology_example}
   \end{center}
\end{figure}
Assume that the sensors start sending measurement from time $1$. Let $\mathcal{S}_k,\;k=1,2,\ldots$, be the set of sensors that are selected to transmit their measurements to the fusion center at time $k$. For example, in Figure~\ref{fig:sensor_topology_example}, $\mathcal{S}_k = \{S_1, S_2, S_6\}$. A sensor selection schedule is defined as an infinite series of sensor selection strategy $\mathcal S = (\mathcal S_1,\mathcal S_2,\ldots)$ and it is defined feasible if $E_{k,i} \geq 0,\;\forall k,i$, which means we are not using the sensors that have no power left.

Let $P_k$ denote the error covariance at the estimator at time $k$, which clearly depends on the set of data measurement received from time $1$ to time $k$, and we indicate $P_k$ as $P_k(\mathcal{S})$ to underline the dependence on the sensor selection strategies.

Suppose it is required that $P_k \leq P_d$ for all $k$, where $P_d$ is a given positive definite matrix, which can be interpreted as a desired estimation accuracy level. The lifetime $L$ of the network under schedule $\mathcal S$ is defined to be
\begin{equation}\label{eqn:lifetime-definition}
L(\mathcal S) \triangleq \min_k \{k: P_k(\mathcal S) \nleq P_d\} - 1.
\end{equation}

The maximal lifetime of the network is defined as
\begin{equation}\label{eqn:maxlifetime-definition}
  \mathcal L \triangleq \sup_{\mathcal S\;feasible} L(\mathcal S).
\end{equation}

The main goal of this paper is to find the optimal or suboptimal scheduling policy, i.e., determining $\mathcal{S}_k$ at each time $k$ such that the $L$ is maximized.

\section{Main Results}\label{section:main}

\subsection{Reformulation}

By the definition of maximal lifetime $\mathcal L$, we know that at time $\mathcal L+1$, there does not exist a sensor selection schedule $\mathcal S$, such at
\begin{enumerate}
  \item $P_k(\mathcal S) \leq P_d$, $k = 1,\ldots,\mathcal L+1$
  \item $\mathcal S$ is feasible, which is equivalent to $E_{\mathcal L+1,i} \geq 0,\;i=1,\ldots,m$
\end{enumerate}
On the other hand, at time $\mathcal L$, there exists at least one schedule $\mathcal S^*$ such that 1) and 2) both hold.

Taking into account these considerations, let us consider the following optimization problem $(P_0)$:
\begin{align}
  \label{eq:optimization}
  (P_0):\;&\min_{\mathcal S} && f_p(E_T)\nonumber\\
	& s.t.&&  P_k \leq P_d, \;\forall k = 1,\ldots,T,\nonumber\\
	&&&\mathcal S\;feasible,\nonumber
\end{align}
where the function $f_p$ is defined as
\begin{equation}
    f_p = \sum_{i = 1}^m E_{T,i}^{-p}, \,p > 0.
\end{equation}
We also define the following function to be $f_0$
\begin{equation}
    f_0 = - \sum_{i=1}^m \log(E_{T,i}).
\end{equation}

Finding the lifetime of the network $\mathcal L$ is equivalent to finding the maximum value of $T$ such that problem $(P_0)$ is feasible. Hence, if we can solve $(P_0)$ for every $T$, in order to find the maximum lifetime of the network, we could first find a $T'$ such that $(P_0)$ is infeasible and then use bisection method to find the real $\mathcal L$.

However, in general, the lifetime of a network can be very long and as consequence,
 \begin{itemize}
 \item solving $(P_0)$ could be intractable;
 \item due to the low reliability of the sensors, some of them may die before batteries running out. As a result of such failure, the optimal sensor schedule will change.
 \end{itemize}
For the above reasons, we propose a greedy method to tackle the maximization of network lifetime. We will try to solve $(P_0)$ for a fixed window size $T$ and, if it is feasible, we will use the optimal schedule for time $1$ to time $T$. After that, we try to solve the problem again for time $T+1$ to time $2T$ using the initial conditions $E_{T,i}, P_{T|T}$. This procedure is repeated until the problem is infeasible, at which time it is replaced by the bisection method inside the window $[0,\;T]$.

In the following subsection we will manipulate and relax problem $(P_0)$ to make it an explicit convex programming problem. However, before doing that, we would like to propose a maximum energy available scheduling policy which is of less computational complexity. At each time step, the algorithm simply choose the sensors with most energy left until $P_k \leq P_d$. This approach may be desirable if the computation capability of the fusion center is limited.

%Let us consider the following optimization problem $(P_0)$:
%\begin{align}
%  \label{eq:optimization}
%  (P_0):\;&\min_{\mathcal S} && f(E_T)\nonumber\\
%	& s.t.&&  P_k \leq P_d, \;\forall k = 1,\ldots,T,\nonumber\\
%	&&&\mathcal S\;feasible.\nonumber
%\end{align}
%
%The function $f$ is chosen to satisfies the following properties:
%\begin{enumerate}
%  \item $f$ is a decreasing function w.r.t. each $E_{T,i}$,
%  \item if any $E_{T,i} \rightarrow 0$, $f \rightarrow +\infty$.
%  \item $f$ is convex,
%\end{enumerate}
%The first property ensures that we are maximizing the energy left. The second property will prevent the optimization algorithm from exhausting some sensors while the others still have plenty of battery left. The last property will ensure that the problem is tractable after relaxation, which will be discussed in the next subsection. In this paper we are particularly interested in the following $f$s:
%    \[
%    f_p = \sum_{i = 1}^m E_{T,i}^{-p}, \,p > 0.
%    \]
%    For simplicity we denote the following function as $f_0$:
%    \[
%    f_0 = - \sum_{i=1}^m \log(E_{T,i}).
%    \]
%It is not hard to show that these functions all satisfy the condition we imposed on $f$. Also it can be seen that the objective function $f_p$ requires more uniform decrease in energy when $p$ is large.
%
%By the definition of maximal lifetime $\mathcal L$, we know that at time $\mathcal L+1$, there does not exist a sensor selection schedule $\mathcal S$, such at
%\begin{enumerate}
%  \item $P_k(\mathcal S) \leq P_d$, $k = 1,\ldots,\mathcal L+1$
%  \item $\mathcal S$ is feasible, which is equivalent to $E_{\mathcal L+1,i} \geq 0,\;i=1,\ldots,m$
%\end{enumerate}
%As a result, we know that $(P_0)$ is infeasible.
%
%On the other hand, at time $\mathcal L$, there exists at lease one schedule $\mathcal S^*$ such that 1) and 2) both hold. Thus, the $(P_0)$ is feasible and schedule $\mathcal S^*$ is the optimal schedule that maximize the life time ,i.e.
%\begin{equation}
%	\mathcal S^* = argmax_{\mathcal S \; feasible} L(\mathcal S).
%\end{equation}
%
%Suppose we could solve $(P_0)$ for any $T$. Then in order to find the life time, we could first find an infeasible $L$ and then use bisection method to find the real $\mathcal L$.
%
%However, in practice, solving $(P_0)$ may be intractable since the lifetime is usually long. Another reason why solving $(P_0)$ for a large $\mathcal L$ is of little interest is that due to the low reliability of the sensors, some of them may die before batteries running out. As a result of such failure, the optimal sensor schedule will change.
%
%For the above reasons, we propose a greedy method to solve the problem. We will have a fixed window size of $T$. Every time we solve the optimization problem $(P_0)$, if the solution is less than $0$, we will use bisection method to find the lifetime. If it is feasible, we will use the optimal schedule for time $1$ to time $T$. After that, we try to solve the problem again for time $T+1$ to time $2T$ using the initial conditions $E_{T,i}, P_{T|T}$. This procedure is repeated until the problem is infeasible, at which time it is replaced by the bisection method. However the optimization problem $(P_0)$ is binary programming and currently there is no way to solve this type of problem efficiently. As a result, we have to use some relaxation techniques to convert the problem to a convex one.
%
%Alternatively, we propose a maximum available energy first algorithm: at every time step, we pick up the sensors with most power left until we meet the constraints. It is not hard to see that this strategy can be seen as an extreme case of $(P_0)$, where $T = 1$ and $f(E_T) = \lim_{p\rightarrow\infty}f_p(E_T)$. This solution may be desirable if the computational power is limited.

\subsection{Convex Relaxation}
This section is devoted to find a convex relaxation for optimization problem $(P_0)$.  In order to do this we first need to find the relation between $E_{T,i},\, P_k,\, \mathcal S$. Let us indicate with $\gamma_{j},\;j=1,\ldots,mT$ the binary variable such that $\gamma_{m(k-1)+i}=1$ if sensor $i$ transmits during the step time $k$, i.e. $S_i \in \mathcal S_k$; otherwise it is $0$. Thus, it is simple to see that
\begin{equation}
	E_{T,i} = E_{0,i} - \sum_{k=1}^T \gamma_{m(k-1)+i},
\end{equation}
and also the feasible condition on $\mathcal S$ is equivalent to
\begin{equation}
  E_{T,i} \geq 0,\,i = 1,\ldots,m
\end{equation}

By the definition of $\gamma_{j}$ the estimator receives at time $k$ the readings $[\gamma_{m(k-1)+1}y_{k,1},\ldots,\gamma_{m(k-1)+m}y_{k,m}]^T$. In order to find the relation between $P_k$ and $\gamma_{k,i}$s, we need to first define the following quantities:
\begin{align}
  Y_k &\triangleq  [y_1^T,y_2^T,\ldots,y_k^T]^T &\in& \mathbb R^{mk},\\
  \Gamma_k &\triangleq diag(\gamma_1,\ldots,\gamma_{mk}) &\in& \mathbb R^{mk\times mk},\\
  \vec \gamma &\triangleq [\gamma_1,\ldots,\gamma_{mk}]^T &\in& \mathbb R^{mk},\\
  \hat x^*_{k|k} &\triangleq E\left(x_k|Y_k\right)&\in&\mathbb R^{n},\\
  P^*_{k|k} &\triangleq Cov\left(x_k|Y_k\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k|k} &\triangleq E\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n},\\
  P_{k|k} &\triangleq Cov\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}

In the following we characterize the relation between $\gamma_j$s and $P_k$s. First consider that all the sensors are used at each time, then the Kalman filter for the system takes the following form
\begin{align}\label{eq:Kalman_filter}
    \hat x_{0| 0}^* & = 0 ,\quad P_{0|0}^*  = \Sigma\\
    \hat x _{k + 1|k}^* & = A \hat x^*_{k|k}  , \quad P_{k + 1|k}^*  = AP^*_{k|k} A^H  + Q \nonumber\\
    K_k^*& = P_{k|k - 1}^* C^H (CP_{k|k - 1}^* C^H  + R)^{ - 1}  \nonumber\\
    \hat x^*_{k|k}& = \hat x^*_{k|k - 1}  + K_k^* (y_k  - C \hat x _{k|k - 1}^* )  \nonumber\\
    P^*_{k|k}& = P^*_{k|k - 1}  -  K^*_k CP^*_{k|k - 1}  \nonumber,
  \end{align}
  where $\hat x_{k|k}^*$ is the state estimation at time $k$, $P_{k|k}^*$ is the estimation error covariance matrix.

\begin{lemma}
  The state estimation $\hat x_{k|k}^*$ is linear in $Y_k$, which implies that
\begin{equation}
  \hat x_{k}^* = G^*_k \left[ {\begin{array}{*{20}c}
    {y_1 }  \\
    \vdots   \\
    {y_{k-1} }  \\
    {y_k }  \\
  \end{array}} \right] = G^*_k Y_k ,
\end{equation}
where $G_1^* = K_1^*$ and $G_k^*$ can be evaluated recursively as
\begin{equation}
  G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*]\,.
\end{equation}
\end{lemma}
\begin{proof}
We know that Kalman filter is the optimal linear estimator, thus $\hat x_{k|k}^*$ is linear in $Y_k$ and can be express as
\begin{equation}
  \hat x_{k|k}^* = G^*_k \left[ {\begin{array}{*{20}c}
    {y_1 }  \\
    \vdots   \\
    {y_{k-1} }  \\
    {y_k }  \\
  \end{array}} \right] = G^*_k Y_k ,
\end{equation}
where $G^*_k$ is the optimal gain which can be computed from Kalman filter Equations~\ref{eq:Kalman_filter}. In particular, $\hat x^*_{1|1} = G_1^* Y_1 = K_1^* y_1$, $G_1^* = K_1^*$ and
\[
\begin{split}
\hat x_{k+1|k+1}^* &= G_{k+1}^* Y_{k+1} = K_{k+1}^* y_{k+1} + (A-K_{k+1}^*CA)\hat x_{k|k}^*\\
& = K_{k+1}^* y_{k+1} + (A-K_{k+1}^*CA)G_k^* Y_k\,.
\end{split}
\]
Thus, $G_k^*$ follows the following recursive equation
\begin{equation}\label{def:G*}
  G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*]\,.
\end{equation}
\end{proof}
\begin{remark}
Note that, by the optimality of Kalman filter, $x_k - \hat x_{k|k}^* =  x_k - E(x_k|Y_k)$ is orthogonal to $Y_k$.
\end{remark}

Now, let us consider the sensor scheduling scheme. We first consider a linear filter with gain $G_k$, which implies that
\begin{equation}
  \hat x_{k|k} = G_k\Gamma_k Y_k \,.
  \label{eq:kalmanestimationforselection}
\end{equation}
By exploiting the optimality and linearity of Kalman filter, the following Theorem holds.

\begin{theorem}
  \label{theorem:covarianceafterscheduling}
  Consider the linear system \eqref{eq:systemdiscription}. The estimation error covariance matrix $P_{k|k}$ verifies the following inequality
  \begin{equation}
    P_{k|k} \leq P_{k|k}^* + (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T,
  \end{equation}
  where $P_{k|k}^*,G_k^*$ are given in \eqref{eq:Kalman_filter} and \eqref{def:G*} , $P_{k|k}$ is the estimation error covariance for Kalman filter with sensor scheduling, and $\mathcal G_k = G_k\Gamma_k$, where $G_k$ is the gain for a linear filter. Furthermore, there exists a $\mathcal G_k$ such that the equation holds.
\end{theorem}

\begin{proof}
The error covariance matrix of a linear filter under sensor scheduling scheme is given by
\begin{displaymath}
  \begin{split}
    & Cov(G_k\Gamma_k Y_k - x_k) = Cov [(G_k\Gamma_k - G_k^*) Y_k  + G_k^* Y_k - x_k ]\\
    & = Cov [(G_k\Gamma_k - G_k^*) Y_k]  + Cov(G_k^* Y_k - x_k )\\
    & = (G_k\Gamma_k - G_k^*) Cov(Y_k)(G_k\Gamma_k - G_k^*)^T  + P^*_{k|k}\\
    & = P_{k|k}^* + (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T.
  \end{split}
\end{displaymath}
The second equation is true because $x_k - \hat x_{k|k}^*$ is orthogonal to $Y_k$.

Since Kalman filter is the optimal linear filter, we know that
  \begin{displaymath}
    P_{k|k} \leq P_{k|k}^* + (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T,
  \end{displaymath}
  and there exists a $\mathcal G_k$ such that the equality holds.
\end{proof}

Applying the result of Theorem~\ref{theorem:covarianceafterscheduling}, it is straightforward to notice that the constraint $P_k \leq P_d$ can be rewritten as
\begin{equation}
    \exists \mathcal G_k, \; s.t.\;(\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T \leq P_d - P_{k|k}^*\,.
\end{equation}

\begin{remark}
Considering the definition of $\mathcal G_k = G_k \Gamma_k$, if we denote
 the $j$th column of $\mathcal G_k$ as $\vec{\mathcal G}_{k,j}$, the following inequality holds
  \begin{equation}
    \gamma_{j} \geq \left\|\sum_{k=k'}^T\left\| \vec{\mathcal G}_{k,j}\right\|_1 \right\|_0,
  \end{equation}
  where $\left\|\vec v\right\|_0$ is simply the number of non-zero elements of $\vec v$. $k'$ is the smallest number satisfies $mk' > j$.
\end{remark}


Taking into account all the results described above, the optimization problem $(P_0)$ can be written as
\begin{align*}
  (P_0):\;&\min &&\;f_p(E_T)\\
	& s.t.&&  (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T \leq P_d - P_{k|k}^*,\nonumber \\
	&&& \gamma_{j} \geq  \left\|\sum_{k=k'}^T\left\| \vec{\mathcal G}_{k,j}\right\|_1 \right\|_0,\nonumber\\
	&&&  E_{T,i} = E_{0,i} - \sum_{k=1}^T \gamma_{m(k-1)+i}.\nonumber\\
	&&& E_{T,i} \geq 0.\nonumber
\end{align*}

Furthermore, we can approximate $f_p$ using Taylor expansion
\begin{equation}
  f_p(E_{T}) = f_p(E_{0}) + p \sum_{i = 1}^m\left[ E_{0,i}^{-p-1}\left(\sum_{k=1}^T\gamma_{m(k-1)+i}\right)\right],
\end{equation}
and the optimization problem $(P_0)$ can be further simplified to
\begin{align*}
  (P_0'):\;&\min &&\;\sum_{i = 1}^m\left[ E_{0,i}^{-p-1}\left(\sum_{k=1}^T\gamma_{m(k-1)+i}\right)\right],\\
	& s.t.&&  (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T \leq P_d - P_{k|k}^*,\nonumber \\
	&&& \gamma_{j} \geq  \left\|\sum_{k=k'}^T\left\| \vec{\mathcal G}_{k,j}\right\|_1 \right\|_0 \quad j=1\dots mT,\nonumber\\
	&&&   \sum_{k=1}^T \gamma_{m(k-1)+i}\leq E_{0,i} \quad i=1\dots m\,.  \nonumber
\end{align*}

Although this formulation completely describes the original problem $(P_0)$, the second constraint, which contains the zero norm, is not convex. As a result, we will propose a reweighted $L_1$ approach to solve this problem\cite{boydreweight}. The algorithm is divided into 4 steps:
\begin{enumerate}
\item Set the iteration count $l$ to zero and set the weights vector to $w_j^{1}=1$ for $j=1.... mT$.
\item Solve the weighted $\textit{L}_1$ problem $(P_1)$
\begin{align*} \label{conv_opt}
  &(P_1):&&\;\min \;\sum_{i = 1}^m\left[ E_{0,i}^{-p-1}\left(\sum_{k=1}^T\gamma_{m(k-1)+i}\right)\right],\\
	& s.t.&&  (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^T \leq P_d - P_{k|k}^*,\nonumber \\
	&&&   \sum_{k=1}^T \gamma_{m(k-1)+i}\leq E_{0,i}.  \nonumber
\end{align*}
with
\[
\gamma_{j}  \geq w_j^{l}  \sum_{k=k'}^T\|\vec {\mathcal G}_{k,j}\|_1.
\]
Suppose the solution is $\gamma_1^l,\ldots,\gamma_{mT}^l$
\item Update the weights
\[
w_j^{l+1}=\frac{1}{\gamma_{j}^l+\epsilon}\quad,j=1 \dots mT,
\]
where $\epsilon$ is a small constant.
\item Terminate on convergence or when $l$ attains a specified maximum number of iterations $l_{max}$. Otherwise, increment $l$ and go to step 2.
\end{enumerate}

\section{Illustrative Example: Monitoring} \label{section:simulation}

In this section, a specific estimation example, monitoring over wireless sensor network, is presented to further illustrate the impact of a sensor selection strategy on the lifetime of the network.

\subsection{Model Definition}
 Let's consider the problem of carbon dioxide $(CO_2)$ sequestration to reduce emissions of this gas produced from burning fossil fuels in power generation plants \cite{tec_rep_CMU}.  WSNs can be employed to monitor sequestration sites for leaks \cite{tec_rep_PITT}. Due to the nature of the problem, acquiring data from every sensors at each sampling period is impractical and expensive for the energy viewpoint. Thus, an optimal sensor scheduling strategy become crucial in order to accurately estimate the $CO_2$ concentration level with limited sensing and, at the same time, increasing the lifetime of the network.

The continuous-time partial differential equation (PDE) model describing a convection-dispersion process is ,
\[
\frac{\partial c(p,t)}{\partial t}+\phi(p,t)\frac{\partial c(p,t)}{\partial p}=\alpha(p,t)\frac{\partial^2 c(p,t)}{\partial^2 p}
 \]
with the surface $z=0$ boundary condition
\[
-\alpha(z,t)\frac{\partial c(p,t)}{\partial z}|_{p=\{x,y,0\}}=\lambda(x,y,t)
\]
where
\begin{itemize}
\item $c(p,t)$ is concentrations of $CO_2$ at time $t$ at point $p$
\item $\phi(p,t)$ and $\alpha(p,t)$ are respectively the advection and dispersion coefficients at time $t$ at point $p$
\item $\lambda(x,y,t)$ is the source inputs of $CO_2$ at time $t$ at the point $(x,y)$ of the surface
\end{itemize}


Following the approach proposed \cite{Jim}, we can derive the discrete-time finite-dimensional model for surface $CO_2$ leakage
\begin{eqnarray}\label{eqn.dyn}
% \nonumber to remove numbering (before each equation)
 x_{k+1} &=& A_k x_{k}+ B_k u_{k}+ w_{k}\,,
\end{eqnarray}
where we divide the bidimensional observed region in $N\times M$ points placed $\Delta x$ apart and we discretize in time with a step $\Delta k$. Hence, $x_{i,k}$ and $u_{i,k}$ indicate respectively the concentration and the source input of $CO_2$ at the point $i$ of the region at time $k$.



%
%\begin{eqnarray}\label{eqn.dyn2}
%% \nonumber to remove numbering (before each equation)
% \nonumber \tilde x_{t+1} &=& \tilde A_t \tilde x_{t}+ \tilde w_{t}
%\end{eqnarray}
%where $\tilde x_t=[x_t \,\, u_t]^{'}$ and
%\begin{equation}\label{eqn.ABC}
%    \tilde A_t =
%    \left(
%      \begin{array}{cc}
%        A_t & B_t \\
%        0 & I \\
%      \end{array}
%    \right)\,.
%\end{equation}


\subsection{Sensor Selection Problem}

In this application, we will focus on the monitoring problem of the sequestration sites to show how the proposed sensor selection strategy has a great impact on the lifetime of the network, guarantying, at the same time, the desired estimation performance in each step time. Hence, we suppose to have a priori knowledge of location and magnitude of the sources. As consequence the system we consider for the estimation problem can be reduced to
\begin{eqnarray}\label{eqn.dyn2}
% \nonumber to remove numbering (before each equation)
 x_{k+1} &=& A_k x_{k}+ w_{k}\,,
\end{eqnarray}
where $x\in \mathbb R^n$, with $n=NM$.
Let's consider a network of $n$ sensors uniformly distributed over the monitored
 region and suppose that the sensors measure the concentration of $CO_2$ at the points where they are placed.
According to the definition of $x(t)$, let us model the measurement of each sensor as
\begin{eqnarray}\label{sensors}
  y_{k} &=& x_{k} + v_{k} \,.
\end{eqnarray}

\normalsize
For the simulations, we impose the following value of the parameters:
\begin{itemize}
\item $\Delta x= 6 m$
\item $\Delta k= 3 sec$
\item $N=M=3$
\item $\phi_x=0.5 \frac{m}{sec} $
\item $\phi_y=2 \frac{m}{sec}$
\item $\alpha_x=\alpha_y=\alpha_z=2.5 \frac{m^2}{sec}$
\item $Q \in \mathbb R^{n\times n}$ and $R \in \mathbb R^{m\times m}$ are symmetric random matrices with entries in $[0\; 1]$\,.

\end{itemize}
Accordingly with these parameters, the matrix $A \in \mathbb R^{9\times 9} $ is constant and we have $9$ sensors, one in each state of the system.

To evaluate the performance of the proposed sensor selection algorithm, we choose $Pd=2.5I_n + 2.5\cdot \mathbf{1}_n$, where $I_n$ and $\mathbf{1}_n$ indicate respectively the identity matrix of dimension $n$ and the matrix with all elements equal to $1$ of dimension $n$.
In Figure~\ref{fig:lifetime} we compare the lifetime of the network applying the proposed algorithm for $T=1,2,3$ with the lifetime using the maximum energy available (MEA) scheduling policy described in section \ref{section:main}.
\begin{figure}[H]
   \begin{center}
   \includegraphics[width=3in]{fig1.eps}
    \caption{Lifetime vs dimension of the window size $T$} \label{fig:lifetime}
   \end{center}
\end{figure}

Figure~\ref{fig:lifetime} clearly shows that using the proposed algorithm we have an improvement in terms of lifetime of $20\%$ with respect the MEA algorithm, independently on the window size we choose. However, we see that such performances are not an increasing function
of the window size $T$. This is essentially due to the relaxation procedure we use to make problem $(P_0)$ convex. In particular,
it is based on the relaxation of the non convex zero norm into the weighted one norm and this procedure is precise only if the matrix under norm is sparse. So, increasing the window size we tend to loose sparsity and the result is less accurate. The analysis of this behavior
will be one the the major point of future research.


\section{Extensions}\label{section:extension}
This section is devoted to discuss several extensions in the convex relaxation framework proposed in Section~\ref{section:main}.
\subsection{Alternative Objective Function}
So far we only used the following functions as the objective function
\begin{align*}
  f_p(E_T) &= \sum_{i = 1}^m \left(E_{T,i}\right)^{-p},\,p > 0,\\
  f_0(E_T) &= \sum_{i = 1}^m -\log(E_{T,i}).
\end{align*}
In fact, any function $f(E_T)$ which satisfies the following conditions can be used as objective function:
\begin{enumerate}
  \item $f(E_T)$ is a decreasing function w.r.t. each $E_{T,i}$,
  \item if any $E_{T,i} \rightarrow 0$, $f(E_T) \rightarrow +\infty$,
  \item $f(E_T)$ is convex.
\end{enumerate}
The first property ensures that we are maximizing the energy left. The second property will prevent the optimization algorithm from exhausting some sensors while the others still have plenty of battery left. The last property will ensure that the problem is tractable by convex optimization. Some possible choices are
\begin{itemize}
  \item \begin{displaymath}
      f(E_T) = \left(\min_i E_{T,i}\right)^{-1}
    \end{displaymath}
  \item \begin{displaymath}
      f(E_T) = \sum_{i= 1}^m \alpha_i E_{T,i}^{-p_i}, \, \alpha_i,p_i > 0
    \end{displaymath}
\end{itemize}
\subsection{Alternative Estimation Accuracy Constraints}
Sometimes we do not care about the accuracy of all the states. Instead, we only want the estimation of some of the states, or more general, linear combinations of states, to be accurate. Thus, the constraints on the $P_{k|k}$ can be written as
\begin{equation}
  S_k P_{k|k}S_k^T \leq P_d.
\end{equation}
For example, if we only care about the first state, then we can choose $S_k = [1, 0,\ldots,0]$. As a result, we need to change the corresponding constraint in optimization problem $(P_1)$ to
\begin{equation}
  S_k (\mathcal G_k-G_k^*)Cov(Y_k)(\mathcal G_k-G_k^*)^TS_k^T  \leq P_d - S_kP_{k|k}S_k^T
\end{equation}

\subsection{Topology Constraints}
The topology of the network discussed in Section~\ref{section:main} can be seen as a star topology, where every sensor can talk to the fusion center directly. However, different topology may be used in sensor network, where an observation packet may be forwarded by many sensors until it reaches the fusion center. As a result, we need to consider the energy cost of those sensors acting as a router.

Due to the space limit, we will only discuss a simple case where the network incorporate a tree topology and the fusion center is the root node. We also assume that the energy for sending a packet is $1$ no matter how many observations are contained in the packet. Define the $\mathcal C(i)$ as the set of children node of sensor $i$. Thus, the topology constraints can be expressed as
\begin{equation}
  \gamma_{m(k-1)+i} \geq \gamma_{m(k-1)+j}, \,k=1,\ldots,T,\,j\in\mathcal C(i)
\end{equation}
Such constraints are linear and tractable by convex optimization.
\section{Conclusion} \label{section:conclusion}

In this paper, we considered the network lifetime maximization problem for state estimation over sensor networks. A reweighted $L_1$ approach is used to approximately solve the problem. A numerical example illustrates that it can significantly increase the lifetime comparing to maximum energy available first algorithm.

There are a few interesting extensions we would like to pursue in the future which include considering packets drop in the communication link; finding the gap between the true optimal lifetime and the lifetime given by our method; evaluating the algorithm in real applications.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\bibliographystyle{IEEEtran}
\bibliography{ascc09-reference}

\end{document}

%% 