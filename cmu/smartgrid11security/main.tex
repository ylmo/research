\documentclass{article}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}

\usepackage{amsthm}


\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{prob}{Problem}

\begin{document}
Suppose $Z_k$s are i.i.d. random variables with finite second moment. Let us define 
\begin{displaymath}
  \mathbb EZ_k = Z,\, Cov(Z_k) = D. 
\end{displaymath}

Suppose $\gamma_k$ are i.i.d. Bernoulli random variables, such that $P(\gamma_k = 1) = p$.

Now consider a switching system with state variable $x_k$, which follows the following update equation:
\begin{equation}
  x_{k+1} = \gamma_{k+1} x_k + (1-\gamma_{k+1}) Z_{k+1},\,x_0 = Z_0. 
  \label{eq:systemequation}
\end{equation}
In other words, $x_{k+1}$ remains the same as $x_k$ with probability $p$ and jumps to a different state with probability $1-p$. It can be easily proved that $\{x_k\}$ is a well defined random process. The following lemma characterize the statistics of $x_k$.

\begin{lemma}
  \begin{displaymath}
    \mathbb Ex_k = Z,\,Cov(x_k) = D,\,\mathbb E(x_kx_{k-i}')=p^iD.
  \end{displaymath}
\end{lemma}
\begin{proof}
  Trivial. 
\end{proof}

Let 
\begin{displaymath}
  y_k = C_kx_k+v_k 
\end{displaymath}
be the observation of the system at time $k$, where $v_k$ are i.i.d. Gaussian Noise with mean $0$ and covariance $R_k$.

The form of the optimal estimator of state $x_k$ based on observations $y_0,\ldots,y_k$ is quite complicated. As a result, we will constraint ourselves to only consider linear recursive state estimator, which takes the following form:
\begin{equation}
  \hat x_{k+1} = \hat x_k + K_{k+1}(y_{k+1} - C_{k+1}\hat x_k), \hat x_{-1} = Z,
  \label{eq:filterequation}
\end{equation}
where $K_k$ is the observation gain. 

Let us define
\begin{displaymath}
  P_k \triangleq Cov(\hat x_k - x_k), \,Q_k \triangleq Cov(\hat x_k).  
\end{displaymath}
To evaluate the performance of the estimator, we have the following theorem:
\begin{theorem}
  The following statements on $\hat x_k$ are true:
  \begin{enumerate}
    \item The estimator is unbiased, which means:
      \begin{equation}
	\mathbb E(\hat x_k - x_k) = 0. 
	\label{eq:bias}
      \end{equation}
    \item The covariance of $\hat x_k$ follows:
      \begin{equation}
	\begin{split}
	Q_{k+1} &=  (I-K_{k+1}C_{k+1})Q_k(I-K_{k+1}C_{k+1})' + K_{k+1}C_{k+1}DC_{k+1}'K_{k+1}'\\
	&+K_{k+1}R_{k+1}K_{k+1}'+(I-K_{k+1}C_{k+1})G_kC_{k+1}'K_{k+1}',Q_{-1}=0,
	\end{split}
	\label{eq:covariance}
      \end{equation}
      where $G_k$ follows
      \begin{equation}
	G_{k+1} =p(I-K_{k+1}C_{k+1})G_k + 2pK_{k+1}C_{k+1}D,\,G_{-1} = 0. 
	\label{eq:Gk}
      \end{equation}
    \item The covariance of estimation error $\hat x_k - x_k$ follows:
      \begin{equation}
	\begin{split}
	  P_{k+1} &= (I-K_{k+1}C_{k+1})\left[pP_k + (1-p)Q_k+(1-p)D\right](I-K_{k+1}C_{k+1})'\\
	  &+K_{k+1}R_{k+1}K_{k+1}',
	\end{split}
	\label{eq:estimationerror}
      \end{equation}
      with initial condition $P_{-1} = D$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  \begin{enumerate}
    \item We will prove that by induction. It is easy to see that 
      \begin{displaymath}
	\mathbb E(\hat x_{-1} - x_{-1}) = Z - Z =0.	
      \end{displaymath}
      Now suppose that \eqref{eq:bias} holds for $k=i-1$. For $k = i$, 
      \begin{displaymath}
	\begin{split}
	  \mathbb E(\hat x_{i}-x_{i}) &= \mathbb E(\hat x_{i-1}+K_{i}C_{i} x_{i}	+ K_{i}v_{i}-K_iC_i\hat x_{i-1}-x_i)\\
	  &=Z+K_iC_iZ+0-K_iC_iZ-Z = 0,
	\end{split}
      \end{displaymath}
      which finishes the proof.
    \item It is trivial to see that $Q_{-1} = 0$. By \eqref{eq:filterequation}, we know that
      \begin{displaymath}
	\hat x_{k+1} = (I-K_{k+1}C_{k+1})\hat x_k + (K_{k+1}C_{k+1}x_{k+1} + v_{k+1}),
      \end{displaymath}
      which implies that
      \begin{displaymath}
	\begin{split}
	Q_{k+1} &=  (I-K_{k+1}C_{k+1})Q_k(I-K_{k+1}C_{k+1})' + K_{k+1}C_{k+1}DC_{k+1}'K_{k+1}'\\
	&+K_{k+1}R_{k+1}K_{k+1}'+2(I-K_{k+1}C_{k+1})\mathbb E(\hat x_kx_{k+1}')C_{k+1}'K_{k+1}'.
	\end{split}
      \end{displaymath}
      As a result, we only need to prove that
      \begin{equation}
	G_k = 2\mathbb E(\hat x_kx_{k+1}').
	\label{eq:gkdefinition}
      \end{equation}
      Rewrite $\hat x_k$ as
      \begin{displaymath}
	\hat x_k = \sum_{i=0}^k\left[\left(\prod_{j=i+1}^kI-K_{j}C_j\right)K_i(C_ix_i+v_i)\right].
      \end{displaymath}
      As a result
      \begin{displaymath}
	\begin{split}
	G_k &= 2\mathbb E(\hat x_k x_{k+1}')=2 \sum_{i=0}^k\left[\left(\prod_{j=i+1}^kI-K_{j}C_j\right)K_iC_i\mathbb E(x_ix_{k+1}')\right]\\
	& = 2\sum_{i=0}^k\left[\left(\prod_{j=i+1}^kI-K_{j}C_j\right)K_iC_i p^{k+1-i}D\right].
	\end{split}
      \end{displaymath}
      Hence, $G_{k+1}$ can be written as
  \begin{displaymath}
	\begin{split}
	  G_{k+1} & = p(I-K_{k+1}C_{k+1})\times 2\sum_{i=0}^k\left[\left(\prod_{j=i+1}^kI-K_{j}C_j\right)K_iC_i p^{k+1-i}D\right]\\
	  &+ 2pK_{k+1}C_{k+1}D=p(I-K_{k+1}C_{k+1})G_k + 2pK_{k+1}C_{k+1}D,
	\end{split}
      \end{displaymath}
   which finishes the proof. 
 \item It is easy to see that $P_{-1} = D$. By \eqref{eq:filterequation}, 
       \begin{displaymath}
	 \hat x_{k+1} - x_{k+1} =  \hat x_k + K_{k+1}(y_{k+1} - C_{k+1}\hat x_k) -  x_{k+1} = (I-K_{k+1}C_{k+1})(\hat x_k - x_{k+1})+K_{k+1}v_{k+1}.
       \end{displaymath}
       Suppose that $\gamma_{k+1} = 1$, which means that $x_{k+1} = x_k$. In that case,
       \begin{displaymath}
	 Cov(\hat x_{k+1} - x_{k+1}|\gamma_{k+1} = 1) = (I-K_{k+1}C_{k+1}) P_{k}(I-K_{k+1}C_{k+1})' +K_{k+1}R_{k+1}K_{k+1}'.
       \end{displaymath}
       If $\gamma_{k+1} = 0$, then $x_{k+1}$ is independent of $x_k,\,x_{k-1},\ldots,$. As a result, it is independent of $\hat x_k$. Hence
       \begin{displaymath}
	 Cov(\hat x_{k+1} - x_{k+1}|\gamma_{k+1} = 0) = (I-K_{k+1}C_{k+1}) (Q_{k}+D)(I-K_{k+1}C_{k+1})' +K_{k+1}R_{k+1}K_{k+1}'.
       \end{displaymath}
       Combining the two results, we have
       \begin{displaymath}
	 \begin{split}
	 P_{k+1} &=  pCov(\hat x_{k+1} - x_{k+1}|\gamma_{k+1} = 1)+(1-p)Cov(\hat x_{k+1} - x_{k+1}|\gamma_{k+1} = 0)\\
	 & =  (I-K_{k+1}C_{k+1})\left[ pP_{k}+(1-p)Q_k + (1-p)D\right](I-K_{k+1}C_{k+1})' +K_{k+1}R_{k+1}K_{k+1}',
	 \end{split}
       \end{displaymath}
       which finishes the proof.
  \end{enumerate}
\end{proof}

\begin{theorem}
  Let $K_{k+1}^*$ be the solution of the following optimization problem:
  \begin{displaymath}
    K_{k+1}^* = argmin_{K_{k+1}}trace(P_{k+1}).
  \end{displaymath}
  Then $K_{k+1}^*$ is given by
  \begin{displaymath}
    K_{k+1}^* =  \left[ pP_{k}+(1-p)Q_k + (1-p)D\right]C_{k+1}'\left\{C_{k+1}\left[ pP_{k}+(1-p)Q_k + (1-p)D\right]C_{k+1}'+R_{k+1}\right\}^{-1}
  \end{displaymath}
\end{theorem}
\end{document}
