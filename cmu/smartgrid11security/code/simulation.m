clc
clear
p = 0.99;
Z = 0.5;
D = 1/12*eye(2);
I = eye(2);
r = 0.01;

T = 1000;

x = rand(2,T);

hatx = zeros(2,T) + Z;
Q = zeros(2);
G = zeros(2);
P = D;
for i = 2:T
    if rand() <= p
        x(:,i) = x(:,i-1);
    end
    C = rand(1,2);
    y = C*x(:,i) + sqrt(r)*randn();
    Ptmp = p*P + (1-p)*Q + (1-p)*D;
    K = Ptmp*C'/(C*Ptmp*C'+r);
    hatx(:,i) = hatx(:,i-1)+K*(y-C*hatx(:,i-1));
    P = (I-K*C)*Ptmp*(I-K*C)' + K*r*K';
    Q = (I-K*C)*Q*(I-K*C)' + K*r*K'+ K*C*D*C'*K' + (I-K*C)*G*C'*K'...
        + K*C*G'*(I-K*C)';
    G = p*(I-K*C)*G+p*K*C*D;
end

plot(x(1,:))
hold on
plot(hatx(1,:),'r')