\section{Appendix}
\label{sec:appendix}
This section is devoted to proving Theorem~\ref{theorem:upperbound}. Before continue on, we would like to state the following properties of function $g$ and $h$, which are used in the proof:
\begin{proposition}
  \label{proposition:two}
	{\em	Let 
  \begin{displaymath}
  h(X,A,Q) = AXA^H + Q,\,\tilde g(X,R) = X - XC^H(CXC^H+R)^{-1}CX,\,g(X,A,Q,R) = \tilde g(h(X,A,Q),R),
  \end{displaymath}
  where X, Q and R are defined in Section~\ref{sec:formulation}. Then the following propositions hold:
  \begin{enumerate}
    \item $h$ is an increasing function of $X$ and $Q$\footnote{By increasing we mean if $X_1 \geq X_2$, i.e. $X_1-X_2$ is positive semidefinite, then $h(X_1)-h(X_2)$ is also positive semidefinite.}.
    \item $g$ is an increasing function of $X$, $Q$, $R$.
    \item $h(X) \geq g(X)$.
    \item $h(X,\alpha A,Q) \geq h(X,A,Q)$ when $\alpha > 1$.
    \item $g(X,\alpha A,Q,R) \geq g(X,A,Q,R)$ when $\alpha > 1$.
  \end{enumerate}}
\end{proposition}
\begin{proof}
  \begin{enumerate}
    \item The first proposition is trivial.
    \item It is easy to see that $\tilde g$ is an increasing function of $R$ and hence that $g$ is also increasing of $R$. To prove that $g$ is an increasing function of $X$, let us first use the matrix inversion lemma to write $\tilde g$ as
      \begin{displaymath}
	\tilde g(X,R) = (X^{-1} + C^HR^{-1}C)^{-1}.
      \end{displaymath}
      It is easy to see that $\tilde g$ is increasing in $X$. Combining this with the fact that $h$ is also an increasing function of $X$, we can conclude that $g$ is increasing in $X$.
      \item
	      \begin{displaymath}
		     h(X) - g(X) =  h(X)C^H(Ch(X)C^H+R)^{-1}Ch(X)\geq 0.
	      \end{displaymath}
    \item It is easy to see that 
      \begin{displaymath}
	h(X,\alpha A,Q) - h(X,A,Q) = (\alpha^2-1)AXA^H \geq 0.
      \end{displaymath}
    \item The proposition is true because of the previous proposition and the fact that $g$ is increasing in $X$.
  \end{enumerate}
\end{proof}

Now we want to use the above propositions to simplify the problem. First since $g,h$ are increasing in $Q, R$ and $X$, we could find $\alpha_1 > 0$ such that $Q \leq \alpha_1 I_n$, $\Sigma \leq \alpha_1 I_n$ and $R \leq \alpha_1 I_m$. Since we are only concerned with the upper bound, without loss of generality we can replace $Q,\Sigma$ and $R$ by $\alpha_1 I_n,\,\alpha_1 I_n$ and $\alpha_1 I_m$.   

Furthermore without loss of generality we can assume that there is no eigenvalue of $A$ that is on the unit circle by replacing $A$ by $(1+\delta)A$, where $\delta >0$ can be arbitrary small due to the fact that
\[
h(X,\alpha A,Q) \geq h(X,A,Q),\; g(X,\alpha A,Q,R) \geq g(X,A,Q,R),\;\textrm{ if }\alpha > 1.
\]

Combining with the assumption that $A$ can be diagonalized, we assume that $A = diag(A_1,A_2)$, where $A_i$ is diagonal and $A_1$ and $A_2$ contain the strictly unstable and stable eigenvalues of $A$ respectively. We also assume $C = [C_1,\,C_2]$, where $C_i$s are of proper dimensions.

From definition of $h$ and $g$, we know that 
\begin{displaymath}
	P_{k} =  h^{i_1}gh^{i_2-1}g\cdots h^{i_l-1}gh^{i_{l}-1}(\Sigma),
\end{displaymath}
provided that $l$ packets arrive at time $k_1>\ldots>k_l$. Also define $k_0 = k$, $k_{l+1} = 0$ and $i_j = k_{j-1} - k_j$ for $1\leq j\leq l+1$. As a result, proving Theorem~\ref{theorem:upperbound} is equivalent to showing that
\begin{displaymath}
   trace( P_{k}) \leq \overline \alpha \prod_{j=1}^l (|\lambda_j|+\varepsilon)^{2i_j}.
\end{displaymath}

To prove the above inequality, we exploit the optimality of Kalman filtering. We construct a linear filter whose error covariance satisfies \eqref{eq:upperbound}. Therefore $P_k$ must also satisfy \eqref{eq:upperbound} due to the optimality of Kalman filtering. To construct such an estimator, let us rewrite the system equations as
\begin{displaymath}
  \begin{split}
    x_{k+1,1}&=A_1 x_{k,1} + w_{k,1}\\
    x_{k+1,2} &= A_2x_{k,2} + w_{k,2}, \\ 
    y_k &= C_1 x_{k,1} + v_k + C_2 x_{k,2}
  \end{split}
\end{displaymath}

Since $A_2$ is stable, we can just use $\hat x_{k,2} = A_2^k \overline x_{0,2}$ as an unbiased estimate of $x_{k,2}$. The new system equations become
\[
  \begin{split}
    x_{k+1,1}&=A_1 x_{k,1} + w_{k,1}\\
    y'_k&= (y_k -C_2 A^k_2\overline x_{0,2})= C_1 x_{k,1} + v_k + C_2 (x_{k,2} - A_2^k \overline x_{0,2})  
  \end{split}
  \]
  To obtain an estimator for $x_{k,1}$, let us first write down the relation between $y_k'$ and $x_{k,1}$ as: 
\begin{align}
  \label{eq:relationloss}
  \left[ {\begin{array}{*{20}c}
    {\gamma_k y_k' } \\
    \vdots \\
    {\gamma_1 y_1'} \\
    \bar x_{0,1}\\
  \end{array}} \right] &= \left[ {\begin{array}{*{20}c}
    { \gamma_k C_1 } \\
    \vdots \\
    {\gamma_1 C_1A_1^{ - k+1} } \\
    { A_1^{ - k} } \\
  \end{array}} \right]x_{k,1} + \left[ {\begin{array}{*{20}c}
    {\gamma_k v_k } \\
    \vdots \\
    {\gamma_1 v_1 } \\
    {\bar x_{0,1} - x_{0,1}}
  \end{array}} \right] +  \left[ {\begin{array}{*{20}c}
    {\gamma_k C_2(x_{k,2} - A_2^k\overline x_{0,2}) } \\
    \vdots \\
    {\gamma_1 C_2(x_{1,2} -A_2 \overline x_{0,2}) } \\
    {0}
  \end{array}} \right] \\
  &- \left[ {\begin{array}{*{20}c}
    {0 } & \cdots &0& 0  \\
    {\gamma_{k-1} C_1A_1^{ - 1} } & \cdots &0& 0  \\
    \vdots&\ddots & \vdots & \vdots   \\
    {\gamma_1 C_1A_1^{ - k+1} } & \cdots& {\gamma_1 C_1A_1^{ - 1} } &0 \\
    {A_1^{ - k} } & \cdots &A_1^{-2}& {A_1^{ - 1} }  \\
  \end{array}} \right] \left[ {\begin{array}{*{20}c}
    {w_{k-1,1}}\\
    \vdots \\
    {w_{0,1}} \\
  \end{array}} \right].\nonumber
\end{align}
where $\gamma_i = 1$ if and only if $i  = k_j$, $j=1,\ldots,l$. To write \eqref{eq:relationloss} in a more compact form, let us define the following quantities:
\begin{equation}
  F_k \triangleq\left[ {\begin{array}{*{20}c}
    {A_1^{-1} } & \cdots & 0 & 0 \\
    \vdots & \ddots & \vdots & \vdots \\
    {A_1^{ - k+1} } & \cdots & {A_1^{-1} } & 0 \\
    {A_1^{ - k} } & \cdots & {A_1^{ - 2} } & {A_1^{-1} } \\
  \end{array}} \right] \in \mathbb{C}^{lk\times lk}.\\
  \label{eq:matrixdefF}
\end{equation}
\begin{equation}
  G_k\triangleq \left[ {\begin{array}{*{20}c}
    {0} & \cdots & 0&0 \\
    {C_1} & \cdots & 0&0 \\
    \vdots & \ddots & \vdots&0 \\
    {0 } & \cdots & C_1&0 \\
    {0 } & \cdots &0& I_l \\
  \end{array}} \right]\in \mathbb{C}^{(mk+l)\times lk}.
  \label{eq:matrixdefG}
\end{equation}
\begin{equation}
  e_k\triangleq -G_k F_k\left[ {\begin{array}{*{20}c}
    {w_{k-1,1} } \\
    \vdots \\
    {w_0,1} \\
  \end{array}} \right] + \left[ {\begin{array}{*{20}c}
    {v_k } \\
    \vdots \\
    {v_1 } \\
    \bar x_{0,1} - x_{0,1}
  \end{array}} \right] +   \left[ {\begin{array}{*{20}c}
    { C_2(x_{k,2} - A_2^k\overline x_{0,2}) } \\
    \vdots \\
    { C_2(x_{1,2} - \overline x_{1,2}) } \\
    {0}
  \end{array}} \right]\in \mathbb{C}^{mk+l}.
  \label{eq:matrixdefe}
\end{equation}
\begin{equation}
  T_k \triangleq \left[ {\begin{array}{*{20}c}
    {C_1 } \\
    \vdots \\
    {C_1A_1^{ - k+1 } } \\
    A_1^{-k}
  \end{array}} \right] \in \mathbb{C}^{(mk+l)\times l},
  Y_k' = \left[ {\begin{array}{*{20}c}
    {y_k' } \\
    \vdots \\
    {y_1' } \\
    \bar x_{0,1}
  \end{array}} \right] \in \mathbb{C}^{mk+l}.
\end{equation}

Define $\Gamma_k$ as the matrix obtained by removing the zero rows from $diag(\gamma_kI_m,\gamma_{k-1}I_m,\ldots,\gamma_1I_m, I_l)$, where $\gamma_{i} = 1$ if and only if $i = k_j$, $j = 1,\ldots,l$.  Thus $\Gamma_k$ is a $ml+l$ by $mk+l$ matrix. Also define
\[
\widetilde Y_k'\triangleq \Gamma_k Y_k,\,\widetilde T_k\triangleq \Gamma_kT_k,\,\widetilde e_k\triangleq \Gamma_ke_k.
\]

Now we can rewrite \eqref{eq:relationloss} in a more compact form as
\begin{equation}
  \label{eq:linearform}
  \widetilde Y_k'=\widetilde T_k x_{k,1}+\widetilde e_k.
\end{equation}

  Since we assumed, without loss of generality, that $R,\,Q,\,\Sigma_0$ are all diagonal matrices, it is easy to see that $x_{k,2}$ and $x_{k,1}$ are mutually independent. As a result, one can easily prove that the following estimator of $x_k$ is unbiased
  \begin{equation}
    \hat x_{k,1}=(\widetilde T_{k}^H Cov (\widetilde e_{k})^{-1}\widetilde T_{k})^{-1}T_{k}^H Cov(\widetilde e_k)^{-1}\widetilde Y_k',\,\hat x_{k,2} = A_2^k \overline x_{0,2},
  \end{equation}
with covariance 
\begin{equation}
 P'_k = \left[ {\begin{array}{*{20}c}
 P'_{k,1} & P'_{k,off}  \\
  (P'_{k,off})^H & P'_{k,2}  \\
\end{array}} \right],
\end{equation}
where
\begin{equation}
	P'_{k,1} = (\widetilde T_{k}^H Cov (\widetilde e_{k})^{-1}\widetilde T_{k})^{-1}, \;P'_{k,2} = Cov(x_{k,2}) = A_2^k\Sigma_0 A_2^{kH} + \sum_{i=0}^{k-1} A_2^i Q A_2^{iH}.
\end{equation}

Since we are only concerned with the trace of $P'_k$, it is easy to see that 
\begin{displaymath}
  trace(P'_k) = trace (P'_{k,1}) + trace(P'_{k,2}), 
\end{displaymath}
where $trace(P'_{k,2})$ is uniformly bounded regardless of $k$. Therefore, in order to prove \eqref{eq:upperbound} we focus exclusively on $trace (P'_{k,1})$. Combining this argument with the optimality argument of Kalman filtering, we only need to prove that
\begin{displaymath}
   trace( P'_{k,1})=trace[(\widetilde T_{k}^H Cov (\widetilde e_{k})^{-1}\widetilde T_{k})^{-1}]\leq \overline \alpha \prod_{j=1}^n (|\lambda_j|+\varepsilon)^{2i_j}.
\end{displaymath}

%\begin{lemma}
%  \label{lemma:bound}
%  If $A = diag (\lambda_1,\lambda_2,\cdots,\lambda_n)$, where $|\lambda_1| \geq |\lambda_2| \geq \cdots \geq |\lambda_n| > 1$, then $F_k F_k^H$ is bounded by
%  \begin {equation}
%  \frac{1}{(|\lambda_1|+1)^2} I_{n(k+1)} \leq F_k F_k^H \leq \frac{1}{(|\lambda_n|-1)^2} I_{n(k+1)},
%\end {equation}
%where $F_k$ is defined in \eqref{eq:matrixdefF}.
%\end{lemma}
Now we claim that the following lemma is true:
\begin{lemma}
	{\em \label{lemma:pseudoinv}
  If a system satisfies assumptions $(H1)-(H3)$, then $P'_{k,1}$ is bounded by
  \begin {equation}
  \label{eq:mlebound}
  P'_{k,1} \leq \alpha_2(\sum_{j=1}^l (A^{k_j-k})^HC^HCA^{k_j-k} + A^{-kH} (A^{-k}))^{-1},
\end {equation}
where $ \alpha_2  \in \mathbb {R}$ is constant independent of $i_j$. }
\end{lemma}
\begin{proof}
We bound $Cov(\widetilde e_k)$ by a diagonal matrix. Since we assume that $w_k,\,v_k,\,x_0$ are mutually independent, it is easy to prove that
	\begin{displaymath}
 Cov( e_k) =  G_k F_kCov\left(\left[ {\begin{array}{*{20}c}
    {w_{k-1,1} } \\
    \vdots \\
    {w_{0,1}} \\
  \end{array}} \right]\right)F_k^HG_k^H + Cov\left( \left[ {\begin{array}{*{20}c}
    {v_k } \\
    \vdots \\
    {v_1 } \\
    \bar x_{0,1} - x_{0,1}
  \end{array}} \right]\right) + Cov\left(  \left[ {\begin{array}{*{20}c}
    { C_2x_{k,2} } \\
    \vdots \\
    { C_2x_{1,2}  } \\
    {0}
  \end{array}} \right]\right).
	\end{displaymath}

Let us consider the first term. Notice that
  \[
  F_k^{-1}=
  \left[ {\begin{array}{*{20}c}
    A_1 & {} & {} & {} \\
    { - I_l} & \ddots & {} & {} \\
    {} & \ddots & A_1 & {} \\
    {} & {} & { - I_l} & A_1 \\
  \end{array}} \right].
  \]
  Therefore,
  \[
  (F_k F_k^H)^{-1}= \left[ {\begin{array}{*{20}c}
    {A_1^HA_1 + I} & { - A_1} & {} & {} \\
    { - A_1^H} & \ddots & \ddots & {} \\
    {} & \ddots & {A_1^HA_1 + I} & { - A_1} \\
    {} & {} & { - A_1^H} & {A_1^HA_1 } \\
  \end{array}} \right].
  \]
  By Gershgorin's Circle Theorem\cite{gerschgorincircle}, we know that all the eigenvalues of $(F_kF_k^H)^{-1}$ are located inside the union of the following circles:$|\zeta - |\lambda_i|^2-1| = |\lambda_i|,|\zeta - |\lambda_i|^2-1| = 2|\lambda_i|,|\zeta - |\lambda_i|^2| = |\lambda_i|$.
  % where $\zeta$s are the eigenvalues of $(F_k F_k^H)^{-1}$.

  Since $|\lambda_1| \geq |\lambda_2| \geq \cdots \geq |\lambda_l| > 1$, for each eigenvalue $\zeta_i$ of $(F_kF_k^H)^{-1}$, the following holds\footnote{Note that all the eigenvalues of $(F_kF_k^H)^{-1}$ are real.}:
  \begin{equation}
    \zeta_i \geq min \{|\lambda_i|^2+1-|\lambda_i|,|\lambda_i|^2+1-2|\lambda_i|,|\lambda_i|^2-|\lambda_i|\},
  \end {equation}

  Thus, $0 < (|\lambda_l|-1)^2 \leq \zeta_i $, which in turn gives
  \[
   F_k F_k^H \leq \frac{1}{(|\lambda_l|-1)^2}I_{lk}.
  \]

Since we assume that $Q = \alpha_1 I_n$, we can prove that
\begin{displaymath}
G_k F_kCov\left(\left[ {\begin{array}{*{20}c}
    {w_{k-1,1} } \\
    \vdots \\
    {w_0,1} \\
  \end{array}} \right]\right)F_k^HG_k^H = \alpha_1 G_kF_kF_k^HG_k^H \leq \frac{\alpha_1}{(|\lambda_l|-1)^2} G_kG_k^H.
\end{displaymath}
From the definition, 
\[
  \frac{\alpha_1}{(|\lambda_l|-1)^2} G_kG_k^H =  \frac{\alpha_1}{(|\lambda_l|-1)^2} diag(0,C_1C_1^H,\ldots,C_1C_1^H,I_l),
\]
which is uniformly bounded by $\alpha_3 I_{mk+l}$. 

Now let us consider the second term, since $R = \alpha_1 I_m$, it is trivial to see that
\begin{displaymath}
	Cov\left( \left[ {\begin{array}{*{20}c}
    {v_k } \\
    \vdots \\
    {v_1 } \\
    \bar x_{0,1} - x_{0,1}
  \end{array}} \right]\right)=\alpha_1 I_{mk+l}.
\end{displaymath}
Now consider the last term, let us write $x_{k,2}$ as
\begin{displaymath}
	\left[ {\begin{array}{*{20}c}
		{ x_{k,2} } \\
		\vdots \\
		{ x_{1,2}  }\\ 
		x_{0,2}
	\end{array}} \right] =  \left[ {\begin{array}{*{20}c}
		{I_{n-l}} & \cdots & {A_2^{k-1} } & {A_2^k } \\
		\vdots & \ddots & \vdots & \vdots \\
		{0 } & \cdots & {I_{n-l} } & A_2 \\
		{0 } & \cdots & 0 & I_{n-l} \\
  \end{array}} \right]	\left[ {\begin{array}{*{20}c}
		{ w_{k-1,2} } \\
		\vdots \\
		{ w_{0,2}  } \\
		x_{0,2}
	\end{array}} \right]. 
\end{displaymath}
As a result, 
\begin{displaymath}
	Cov\left(	\left[ {\begin{array}{*{20}c}
		{ x_{k,2} } \\
		\vdots \\
		{ x_{1,2}  } \\
		x_{0,2}
	\end{array}} \right]\right)=\alpha_1 \left[ {\begin{array}{*{20}c}
		{I_{n-l}} & \cdots & {A_2^{k-1} } & {A_2^k } \\
		\vdots & \ddots & \vdots & \vdots \\
		{0 } & \cdots & {I_{n-l} } & A_2 \\
		{0 } & \cdots & 0 & I_{n-l} \\
  \end{array}} \right] \left[ {\begin{array}{*{20}c}
		{I_{n-l}} & \cdots & {A_2^{k-1} } & {A_2^k } \\
		\vdots & \ddots & \vdots & \vdots \\
		{0 } & \cdots & {I_{n-l} } & A_2 \\
		{0 } & \cdots & 0 & I_{n-l} \\
	\end{array}} \right]^H \leq\frac{ \alpha_1}{(1-|\lambda_{l+1}|)^2}I_{(n-l)(k+1)},
\end{displaymath}
where the proof of the last inequality is similar to the proof of $F_kF_k^H \leq (|\lambda_l|-1)^{-2}I_{lk}$ and is omitted. Therefore it is easy to see that
\begin{displaymath}
	Cov\left(  \left[ {\begin{array}{*{20}c}
    { C_2x_{k,2} } \\
    \vdots \\
    { C_2x_{1,2}  } \\
    {0}
  \end{array}} \right]\right)\leq \alpha_4 I_{mk+l},
\end{displaymath}
where $\alpha_4$ is a constant. As a result, we have proved that
\begin{displaymath}
	Cov(e_k) \leq \alpha_2 I_{mk+l},	
\end{displaymath}
where $\alpha_2 = \alpha_3+\alpha_1+\alpha_4$. Moreover 
\[
Cov(\widetilde e_k)  = \Gamma_kCov(e_k)\Gamma_k^H\leq \alpha_2 I_{ml+l}.
\]
The above bound is independent of $i_1,\ldots,i_{l+1}$, which proves
\[
P'_{k,1} = (\widetilde T_k^HCov(\widetilde e_k)^{-1}\widetilde T_k)^{-1} \leq \alpha_2(\sum_{j = 1}^l (A^{k_j-k})^HC^HCA^{k_j-k} + A^{-kH} A^{-k})^{-1}.
\]
\end{proof}

We manipulate $ \sum_{j=1}^n (A^{-i_j})^HC^HCA^{-i_j}$ to prove the upper bound by using cofactors for matrix inversion. Before continue, we need the following lemmas.
\begin{lemma}
  \label{lemma:gaussianelimination}
  {\em For a non-degenerate system, it is possible to find a set of row vectors $L_1,L_2,\ldots,L_l$, such that $L_i C = [l_{i,0},\ldots,l_{i,l}]$, where $l_{i,i} = 1$ and $l_{i,a}=0$ if $|\lambda_i|=|\lambda_a|$ and $i\neq a$.}
\end{lemma}
\begin{IEEEproof}
  It is simple to show that the lemma holds by using Gaussian Elimination for every quasi-equiblock.
\end{IEEEproof}
\begin{lemma}
  \label{lemma:determinant}
  {\em Consider that $|\lambda_{1}| \geq |\lambda_{2}|\cdots \geq |\lambda_l|$, $l_{i,i} = 1$ and $l_{i,a} = 0$ if $i\neq a$ and $|\lambda_i|=|\lambda_a|$. Let $i_1 = k - k_1$ and $i_j = k_{j-1} - k_j$ for $2\leq j\leq l$. Define
  \[
  D=\left| {\begin{array}{*{20}c}
    {l_{1,1}\lambda _1^{ k_1 -k  } } & {l_{1,2}\lambda _2^{ k_1 - k } } & \cdots & {l_{1,l}\lambda _l^{k_1 - k} } \\
    {l_{2,1}\lambda _1^{ k_2- k } } & {l_{2,2}\lambda _2^{  k_2 - k } } & \cdots & {l_{2,l}\lambda _l^{ k_2 - k } } \\
    \vdots & \vdots & \ddots & \vdots \\
    {l_{l,1}\lambda _1^{ k_l - k } } & {l_{l,2}\lambda _2^{ k_l - k } } & \cdots & {l_{l,l}\lambda _l^{ k_l- k } } \\
  \end{array}} \right|.
  \]
 Then $D$ is asymptotic to $\prod_{j=1}^l\lambda_j^{k_j-k}$, i.e.
  \begin{equation}
    \label{eq:asymptotic}
    \lim_{ i_1, i_2,\ldots, i_l \rightarrow + \infty} \frac{D}{\prod_{j=1}^l\lambda_j^{k_j-k}}=1.
  \end{equation}}
\end{lemma}

\begin{IEEEproof}[Proof of Lemma~\ref{lemma:determinant}]
  The determinant $D$ has $l!$ terms, which have the form $sgn(\sigma) \prod_{j=1}^l l_{j,a_j} \lambda_{a_j}^{k_j-k}$. $\sigma = (a_1,a_2,\ldots,a_l)$ is a permutation of the set $\{1,2,\ldots,l\}$ and $sgn(\sigma) = \pm 1$ is the sign of permutation. Rewrite \eqref{eq:asymptotic} as
  \[
  \begin{split}
    \frac{D}{\prod_{j=1}^l\lambda_j^{k_j-k}}&=\sum_{\sigma} sgn(\sigma) {\frac {\prod_{j=1}^l l_{j,a_j}
    \lambda_{a_j}^{k_j-k}}{\prod_{j=1}^l\lambda_j^{k_j-k}}} =\sum_{\sigma} sgn(\sigma){\prod_{j=1}^l l_{j,a_j}\frac{\left(\prod_{j=1}^l \lambda_{a_j}\right)^{- i_1} \cdots\left(\prod_{j=l}^l \lambda_{a_j}\right)^{- i_l}}{\left(\prod_{j=1}^l \lambda_j\right)^{- i_1}\cdots\left(\prod_{j=l}^l \lambda_j\right)^{- i_l}}}\\
    &=\sum_{\sigma} sgn(\sigma){\prod_{j=1}^l l_{j,a_j}\prod_{b=1}^l\left(\frac{\prod_{j=b}^l \lambda_{a_j}}{\prod_{j=b}^l \lambda_j}\right)^{-i_j}}.
  \end {split}
  \]

  Now we can just analyze each term of the summation. Since $|\lambda_1| \geq \cdots \geq |\lambda_l|$, $|\prod_{j=b}^l \lambda_{a_j}| \geq |\prod_{j=b}^l \lambda_{j}|$. First consider that there exist some $j$s such that $|\lambda_{a_j}| \neq |\lambda_{j}|$ and define $j^*$ to be the largest, which means $|\lambda_{a_{j^*}}| \neq |\lambda_{j^*}|$ and $|\lambda_{a_j}| = |\lambda_{j}|$ for all $j$ greater than $j^*$. Since $|\lambda_{j^*}|$ is the smallest among $|\lambda_1|,\ldots,|\lambda_j|$, we know that $|\lambda_{a_{j^*}}| > |\lambda_{j^*}|$. Thus,
  \begin{displaymath}
    \left| \frac{\prod_{j=j^*}^l \lambda_{a_j}}{\prod_{j=j^*}^l \lambda_j} \right| > 1,
  \end{displaymath}

  \begin{displaymath}
    \lim_{ i_1, i_2,\ldots, i_l \rightarrow \infty} \left|\prod_{j=1}^l l_{j,a_j}\prod_{b=1}^l\left(\frac{\prod_{j=b}^l \lambda_{a_j}}{\prod_{j=b}^l \lambda_j}\right)^{-i_j}\right| \leq |\prod_{j=1}^l l_{a,a_j}| \lim_{ i_{j^*} \rightarrow \infty} \left|\frac{\prod_{j=j^*}^l \lambda_{a_j}}{\prod_{j=j^*}^l \lambda_j}\right|^{- i_{j^*}} = 0.
  \end{displaymath}

  Then consider that if for all $j$, $|\lambda_{a_j}|=|\lambda_j|$, but $(a_1,\ldots,a_l) \neq (1,2,\ldots,l)$. Thus, there exists $j^*$ such that $a_{j^*} \neq j^*$. Hence $l_{j^*,a_{j^*}} = 0$. Therefore, these terms are always 0.

  The only term left is 
  \begin{displaymath}
    sgn(\sigma)\prod_{j=1}^l l_{j,j}\prod_{b=1}^l\left(\frac{\prod_{j=b}^l \lambda_{j}}{\prod_{j=b}^l \lambda_j}\right)^{- i_j} = 1.
  \end{displaymath}

  Thus, we can conclude that
  \[
  \lim_{ i_1, i_2,\ldots, i_l \rightarrow \infty} \frac{D}{\prod_{j=1}^l\lambda_j^{k_j-k}}=1.
  \]
\end{IEEEproof}

  Because the system is non-degenerate, by Lemma~\ref{lemma:gaussianelimination}, we know that there exist $L_1,L_2,\cdots,L_l$, such that $L_i C = [l_{i,1},\ldots,l_{i,l}]$ is a row vector, $l_{i,i}=1$ and $l_{i,a} = 0$ if $i \neq a$ and $|\lambda_i| =|\lambda_a|$. 

  Define matrices
  \begin{equation}
    U \triangleq  \left[ {\begin{array}{*{20}c}
      {l_{1,1}\lambda _1^{ - i_1 } } & {l_{1,2}\lambda _2^{ - i_1 } } & \cdots & {l_{1,l}\lambda _l^{ - i_1 } } \\
      {l_{2,1}\lambda _1^{ - i_2 } } & {l_{2,2}\lambda _2^{ - i_2 } } & \cdots & {l_{2,l}\lambda _l^{ - i_2 } } \\
      \vdots & \vdots & \ddots & \vdots \\
      {l_{l,1}\lambda _1^{ - i_l } } & {l_{l,2}\lambda _2^{ - i_l } } & \cdots & {l_{l,l}\lambda _l^{ - i_l } } \\
    \end{array}} \right], O \triangleq U^{-1}.
  \end{equation}

  Define $\alpha_5 = \max (\lambda_{max}(L_1^H L_1),\ldots,\lambda_{max}(L_l^H L_l))$. Thus, $L_i^H L_i \leq \alpha_5 I_m$, and
  \begin{equation}
    \begin{split}
      &\sum_{j =1}^{l} A^{-i_j H}C^H C A^{-i_j} \geq \sum_{j =1}^{l} \frac{1}{\alpha_5}A^{-i_j H}C^HL_j^HL_j C A^{-i_j}\\
      &=\frac{1}{\alpha_5} \left[ {\begin{array}{*{20}c}
	{A^{-i_1H}C^HL_1^H} & {\cdots} & {A^{-i_lH}C^HL_1^H}  \\
      \end{array}} \right]\left[ {\begin{array}{*{20}c}
	{L_1CA^{-i_1}}  \\
	{\vdots}  \\
	{L_lCA^{-i_l}}  \\
      \end{array}} \right]=\frac{1}{\alpha_5} U^H U,
    \end{split}
  \end{equation}
  and
  \begin{equation}
    \begin{split}
      \label{eq:traceeqsum}
      &\left(\sum_{j =1}^{l} A^{(k_j-k) H}C^H C A^{k_j-k}\right)^{-1} \leq \alpha_5 \left(U^HU\right)^{-1} = \alpha_5 OO^H \leq \alpha_5trace(OO^H) I_l \\&= \alpha_5\sum_{a,b} O_{a,b}(O^H)_{b,a}I_l = \alpha_5\sum _{a,b} O_{a,b} \times conj(O_{a,b})I_l = \alpha_5\sum_{a,b} |O_{a,b}|^2 I_l,
    \end{split}
  \end{equation}
  where $conj()$ means complex conjugate. 

  Now by Lemma~\ref{lemma:determinant}, we can compute the cofactor matrix of $U$ and hence $O=U^{-1}$. Define the minor $M_{a,b}$ of $U$ as the $(l-1)\times (l-1)$ matrix that results from deleting row $a$ and column $b$. Thus
  \begin{equation}
    O_{a,b}=\frac{(-1)^{a+b}\det(M_{b,a})}{\det (U)}.
    \label{eq:cofactor}
  \end{equation}

  By Lemma~\ref{lemma:determinant}, we know that
  \begin{displaymath}
    \lim_{ i_1, i_2,\ldots, i_l \rightarrow \infty} \frac{\det(U)}{\prod_{j=1}^l\lambda_j^{k_j-k}}=1.
  \end{displaymath}
  Since $M_{a,b}$ has the same structure as $U$, it is easy to show that 
  \[\det(M_{a,b}) \leq \rho_{a,b}\prod_{j=2}^l |\lambda_j^{k_{j-1}-k}|,\] 
  where $\rho_{a,b}$ is a constant. Thus, 
  \begin{equation}
    \begin{split}
      &\limsup_{ i_1,\ldots, i_l \rightarrow \infty}\frac{\left(\sum_{j =1}^{l} A^{k_j-k H}C^H C A^{k_j-k}\right)^{-1}}{\prod_{j=1}^l |\lambda_j|^{2 i_j}} \leq \limsup_{ i_1,\ldots, i_l \rightarrow \infty}\frac{\alpha_5\sum_{a,b}|O_{a,b}|^2}{\prod_{j=1}^l |\lambda_j|^{2 i_j}}I_l\\ 
      &= \limsup_{ i_1,\ldots, i_l \rightarrow \infty} \alpha_5 \left(\sum_{a,b} \left|\frac{\det(M_{a,b})}{\det(U)}\right|^2/\prod_{j=1}^l |\lambda_j|^{2i_j}\right) I_l\\
      &\leq \alpha_5 \left(\sum_{a,b} \rho_{a,b}^2 \left|\frac{\prod_{j=2}^l |\lambda_j^{k_{j-1}-k}|}{\prod_{j=1}^l\lambda_j^{k_j-k} }\right|^2/\prod_{j=1}^l |\lambda_j|^{2 i_l}\right) I_l = \alpha_5 \sum_{i,j}\rho_{i,j}^2 I_l. 
    \end{split}
  \end{equation}

  Hence, there exists $\alpha_6 > 0$ such that
  \begin{displaymath}
	  \left(\sum_{j =1}^{l} A^{(k_j-k) H}C^H C A^{k_j-k}\right)^{-1}\leq \alpha_6 \prod_{j=1}^l |\lambda_j|^{2 i_j}I_l,
  \end{displaymath}
  Along with Lemma~\ref{lemma:pseudoinv} we can finish the proof.
