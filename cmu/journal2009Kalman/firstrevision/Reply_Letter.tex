\documentclass[10pt,oneside,a4paper]{letter}

\usepackage{amsfonts}
\usepackage[T1]{fontenc} 
%% ----------------------------------------------------------------
%\vfuzz2pt % Don't report over-full v-boxes if over-edge is small
%\hfuzz2pt % Don't report over-full h-boxes if over-edge is small
%% THEOREMS -------------------------------------------------------
%\newtheorem{thm}{Theorem}[section]
%\newtheorem{cor}[thm]{Corollary}
%\newtheorem{lem}[thm]{Lemma}
%\newtheorem{prop}[thm]{Proposition}
%\theoremstyle{definition}
%\newtheorem{defn}[thm]{Definition}
%\theoremstyle{remark}
%\newtheorem{rem}[thm]{Remark}
%\numberwithin{equation}{section}
%% MATH -----------------------------------------------------------
%\newcommand{\norm}[1]{\left\Vert#1\right\Vert}
%\newcommand{\abs}[1]{\left\vert#1\right\vert}
%\newcommand{\set}[1]{\left\{#1\right\}}
%\newcommand{\Real}{\mathbb R}
%\newcommand{\eps}{\varepsilon}
%\newcommand{\To}{\longrightarrow}
%\newcommand{\BX}{\mathbf{B}(X)}
%\newcommand{\A}{\mathcal{A}}
% ----------------------------------------------------------------
\begin{document}

\signature{Yilin Mo and Bruno Sinopoli}

\Large
%
\begin{center} Answers to AE's and reviewers' comments\\
\bigskip
\large Submission Number: 09-647\\
\bigskip
\today
\end{center}
\normalsize
\bigskip
\textbf{Title:} Towards Finding the Critical Value for Kalman Filtering with Intermittent Observations

\textbf{Authors:} Yilin Mo and Bruno Sinopoli\\
\medskip

We are grateful to the editor and to the reviewers for their constructive reviews. We have thoroughly revised the paper taking into account all their remarks.
The main focus of this paper has been significantly broadened to address the concerns of several reviewers about the novelty of the paper.
This work breaks away from existing body of literature and uses a novel approach to characterize the stability of Kalman filtering under packet losses. There are several important new contributions. First of all the problem formulation is general and can address any packet drop model. While existing results partially address the stability of the first moment of the error covariance under i.i.d packet losses, we characterize the stability of all moments under Markovian packet loss models. We also show that the proposed methodology applies to general packet loss models, provided that certain probabilities associated with the specific loss process can be computed. This not only advances the current state of the art, as the computation of the critical value becomes a special case, but generalizes to a much larger class of erasure channel models. This goal is accomplished by characterizing the tail distribution of $trace(P_k)$, i.e. study how $P(trace(P_k) > M)$ converges to $0$ as $M$ goes to infinity. We will show that, under minimal assumptions on the system, $trace(P_k)$ follows a power decay law. This result has tremendous value of its own, as it provides the designer with a quantitative bound on the quality of the estimator. We are able compute the exact decay rate for a large class of linear Gaussian systems, defined as non-degenerate. We illustrate the relationship between non-degeneracy and observability and argue that such condition, rather than the weaker notion of observability, is the appropriate one to check when the observation process is random. Under this assumption we derive useful bounds for the Riccati equation, that can be used independently of any loss process.
We compute the critical value for non-degenerate system as a consequence of the tail distribution and prove that it attains the lower bound derived in [1]. Degenerate systems will in general have a higher critical probability, as shown in [7].


% First, instead of focusing on the critical value, we focus our effort on the distribution of $P(trace(P_k) > M)$, i.e., the probability that the Kalman filter gets a large estimation error. We prove that $sup_k P(trace(P_k)>M)$ follows a power decay law as $M$ goes to infinity and find the exact decay rate for non-degenerate systems. The critical value for the non-degenerate system is then derived as a consequence of the tail distribution of $trace(P_k)$. Second we have change the i.i.d. packet model to the more general Markov model. Finally we give more comments on the concept of non-degeneracy in Section III and Section IV and show why we believe it is a proper condition to study Kalman filtering with intermittent observations.
\newpage
Summary of Changes

\emph{Note that the numbering of sections below refers to the new version of the manuscript.}

\emph{Section I: Introduction}

\begin{enumerate}
  \item Several citations have been added as suggested by Reviewer 13.
  \item The last two paragraphs have been changed according to the new structure of the paper.
\end{enumerate}

\emph{Section II: Problem Formulation}

 We shifted our focus from direct analysis of the critical value to the decay rate of $sup_k P(trace(P_k)>M)$ as $M$ goes to infinity. In particular, the following changes have been made:
\begin{enumerate}
  \item $P_k$ now refers to $P_{k|k}$ instead of $P_{k|k-1}$.
  \item We deal with general Markov packet drop model instead of i.i.d. packet drop model.
  \item Definitions of functions $h$ and $g$ have been added as they are used in Section III.
  \item The definitions of $\varphi(M)$ and $\overline \phi$, $\underline \phi$, $\phi$ have been added to characterize the tail distribution of $trace(P_k)$.
\end{enumerate}

\emph{Section III: Non-degenerate Systems}

A comment on the hypothesis $(H1)-(H3)$ has been added to address the issue of generality. Also we introduce the concept of non-degeneracy in this section. In particular the following changes have been made to clarify such concept and its importance:
    \begin{enumerate}
      \item In order to better compare with the concept of observability, the definition of equi-block has been changed. Also a citation has been added as requested by Reviewer 2.
      \item More details have been added in order to relate observability, non-degeneracy and one-step observability.
      \item Insights on the concept of non-degeneracy have been added at the end of this section. 
    \end{enumerate}
    
    \emph{Section IV: General Inequalities on Iterative Lyapunov and Riccati Equations}


   In this section, we establish several important inequalities on iterative Riccati and Lyapunov equations, which are used to derive the exact decay rate of $\varphi(M)$. Moreover these inequalities are very general as no assumption on the packets drop model is made to derive them. In particular, in (9) we establish an upper bound for the estimation error of non-degenerate systems when any $l$ packets reach the estimator, where $l$ is the number of unstable eigenvalues. Since no packet drop model is assumed in the proof of (9), (9) is useful to analyze the performance of Kalman filtering with any loss model. At the end of this section, we show that for degenerate systems (9) does not hold in general, which is the key difference between non-degenerate and degenerate systems. 

   \emph{Section V: Decay Rate for Non-degenerate systems}

 In this section, we derive the exact decay rate for $\varphi(M)$ for non-degenerate systems.

 \emph{Section VI: Critical Value and Boundedness of Higher Moments}

In this section we compute the exact critical value for non-degenerate systems. We also give a detailed comparison between the our result and the results derived in [1] and [2]. Furthermore we derived necessary and sufficient conditions for the boundedness of higher moments.

\emph{Section VII: Conclusion}

The conclusion has been rewritten to reflect the changes in the paper.

\emph{Appendix}

The original Section V has been moved to the appendix as suggested by Reviewer 2. The original Section IV has been removed since our main focus has been changed. The original Section VI on the critical value for degenerate second order systems has also been removed to make the paper more compact.
\newpage
\emph{Theoretical Contribution and Why We Believe It is Appropriate for Full Paper}

We believe that the new version of the paper has provided more insight on estimation in lossy networks. In particular:
\begin{itemize}
	\item We provide a new technical condition of non-degeneracy. In the literature on the Kalman filtering with intermittent observations, usually one can only get a tight bound on critical value under the condition that the system is one-step observable (or other variations of one-step observability). The definition of non-degeneracy is weaker than one-step observability and usually holds for most of the systems. Hence, the result proved in this paper can be used for much wider class of linear systems.
	\item We provide lower and upper bounds for iterative Riccati and Lyapunov equations. Since these bounds do not depend on a particular packet drop model, we believe that they can be useful to other researchers to study the impact of  different packet drop models on state estimation.
	\item We characterize the tail behavior of $\varphi(M)$. We show that it follows a power decay law and are able to compute the exact decay rate for non-degenerate systems. To the best of our knowledge the tail distribution has not been studied in the literature.
	\item The critical value of non-degenerate system is also derived based on the tail distribution, which extends the result from [1] and [2].
\end{itemize}


\newpage
\noindent {\bf Reviewer \# 2}
\emph{ The paper is concerned with the problem of state estimation of discrete-time linear Gaussian systems where the observations are communicated via a memoryless packet erasure channel. A Bernoulli independent process with given arrival probability is chosen to model whether or not the observations make it through the channel. The paper derives the critical value of the arrival probability below which the optimal estimator will diverge, under minimum assumptions on the system and observation matrices. These results are an improvement over previous studies, where only upper and lower bounds on the critical arrival probabilities were proposed.\\  
With two exceptions (see i. and ii. below), the various developments seem technically sound to me, although their presentation and readability could be sensibly improved. The paper contains about twenty theorems and lemmas with their proofs, some of them being particularly tricky. A personal suggestion to improve the presentation of the paper would be to place the proofs in an appendix.}

Many proofs have been moved to the appendix in hope to improve legibility.  Additional comments on the theorems have been added to make them more easily understandable.

\emph{ My main concerns with the paper are \\ 
i. the validity of Eqs. (11) on p.8, which I was unable to derive using the Matrix Inversion lemma, as suggested by the authors. It seems, moreover, that substituting the matrices in Eqs. (11) and (2) with simple test values (for instance $A=P_k=C=Q=R=1$) leads to different results for $P_{k+1}$. Could the author provide more information on the derivation of Eqs. (11)?\\ }

(11) has been manipulated and moved to Proposition 2 in the appendix. As a result, it should be easy to see the matrix inversion lemma applies now.

\emph{ii. the expression given for $G_k G_k^H$ on p.14, which looks suspicious to me, based on the definition of $G_k$ given in Eq.(18). The fact that $C C^H$ is a matrix of dimension m x m which can not be added to $I_n$ is alarming.  \\ }

The definition of $G_k$ has been slightly changed to reflect the change of focus. In the new version $G_kG_k^H = diag(0,C_1C_1^H,\ldots,C_1C_1^H,I_l)$, the dimension of which should be correct.

\emph{Minor comments:\\ 
On p.6, the authors write 'It can be seen that non-degeneracy is a stronger property than observability...', which does not seem trivial to me and might require an explanation.\\ }

It turns out to be this result is related to Theorem~1 in [15]. The appropriate citation has been added.

\emph{In the proof of Theorem 3 on p.8, the first line assumes that blocks are smaller than m in size, while Theorem 3 on p.6 applies to blocks of size n or less. Could the authors clarify this issue?\\ }

Theorem~3 has been removed. For the sake of clarity, we want to acknowledge that in the proof of the original submission there was a typo. It should have been $l < n$, not $m$.

\emph{Typos:\\
- On p.2, In [1], the author -> In [1], the authors.\\}
Changed.

\emph{- On p.3, In [13], the authors shows -> In [13], the authors show.\\}
Changed.

\emph{- At the bottom of p.8, the notations are not coherent with p.9. In the matrix, $C_I$ and $C_J$ stand for $C_1$ and $C_2$, or conversely.\\}
Removed from the new version.

\emph{- On p.11, the 'in' symbol is not in the right place.\\}
Changed. Also the definition of $G_k$ is in the appendix now.

\emph{- On p.18, 'same structure with'-> 'same structure as'.\\}
Changed.

\emph{- On p.19, it seems to me that the symbols '>=' stand for '<='.\\}
Changed. Now it is the last equation in the appendix.

\emph{- On p.20, does one use the same method 'as' in Lemma 2 and Theorem 3? \\}
\emph{- On p.21, 'follows recursive equation' -> 'follows the recursive equation'.\\}
These sentences have been removed from the new version.

\emph{- In Theorem 9 on p.22, the statement that the phase is a fraction of 2pi should appear in the hypotheses of the theorem.\\}
\emph{- On p.24, in Eq. (52), a factor is missing in the expression for the determinant. \\}
\emph{- On p.25, 'convergences' -> 'converges'.\\
- On p.26, 'proof' -> 'prove'.}

This section has been completely removed to make the paper more compact.


\newpage
\noindent {\bf Reviewer \# 7}
                                             
\emph{The authors analyse the problem of optimal estimation for linear Gaussian systems where packets containing observations are dropped according to an i.i.d. Bernoulli process, modelling a memoryless erasure channel.  The authors extend results in [1] by computing the value of the critical arrival probability that ensures boundedness of the estimation error, under minimally restrictive conditions on the matrices A and C.  \\
Since the paper is really full of maths, I would be useful for the reader to highlight the most important results. I suggest to insert a table where the critical value is reported for the various cases analysed by the authors. It would be also useful to insert some further examples for A and C, that show the findings. For instance, an example about the comment on the first paragraph on page 22. }

Since we shifted the main focus, the new version deals extensively with the tail behavior and critical value for non-degenerate systems. As a consequence, the results should be much clearer. Also we have added a detailed comparison between our result and the results from [1] and [2].

\emph{Minor changes:\\
-please, insert a marker to identify the end of theorems and definitions\\}
The definitions and theorems have been changed to italic font.

\emph{-Equation (9), there are two dots at the end of the equation.\\}
Equation (9) has been removed in the new version.

\emph{-Equation (18), remove the symbol $\in$ after the equal.\\}
Changed. Also the definition of $G_k$ is in the appendix now.

\emph{-please, clarify that the expected value is w.r.t the noises.\\}
It is not so clear which expectation the reviewer is refering to. However we would like to comment that $EP_k$ is w.r.t. $\gamma_k$, not to the noise.

\emph{-End of equation (28), a dot is missing.\\}
Equation (28) has been removed in the new version.

\emph{-Two equations after (51), remove the double || by using another symbol for "such that".\\
-Equation (63), a comma is missing at the end.\\}
This section has been completely removed.

\newpage
\noindent{\bf Reviewer \# 9}

\emph{This paper considers the convergence problem for estimation of lossy network systems.  The main contribution of the paper is to find the critical value for convergence under several assumptions. In my opinion, the contribution is not significant.}

We shifted the main focus from critical value to the distribution of $trace(P_k)$, which is discussed in Subsection III-C. Also some inequalities on iterative Riccati and Lyapunov equations have been proved, which we believe have theoretical values in their own right.

\emph{ In particular, the following two are assumed throughout the paper as described following Theorem 2.\\
 (1) $R=I_m, Q=I_m, \Sigma_0=I_m$.\\
 (2) A is diagonal.\\
Either one of the two may be assumed without loss of generality under (H2) and (H3). However, the pair of (1) and (2) is very strong in practice.  In fact, if A is diagonalized, then the assumption (1) is  not satisfied in general.  Thus Theorem 3 and later theorems are restricted.  Moreover, if A with complex eigenvalues is diagonalized, then the transformation matrix is complex and hence the covariance 
matrices for the transformed system are also complex. Such complex matrices are not discussed in the paper.  }

Theorem 2 have been merged to the appendix. To clarify, we do not assume (1) and (2) hold at the same time. Instead we assume that:\\
 (1) $ R\leq \alpha_1 I_m, Q\leq \alpha_1 I_n, \Sigma\leq \alpha_1 I_n$, for some $\alpha_1$\\
 (2) A is diagonalizable and hence is assumed to be in the diagonal standard form.\\
In other word, we assume that $R,Q,\Sigma_0$ are bounded from above. Since we are interested in the upper bound we can always replace $Q,R,\Sigma$ by larger matrices. Hence, we can assume they are diagonal without loss of generality.

\emph{In addition, the results of the paper are mere rehashes of just one of the previous results shown in [1].  No new concepts to analyze lossy network systems are proposed in the paper.}

The main focus has been significantly changed and provides new methodology that introduces new results that broadly generalize [1] and provides insight to the problem of estimation with random observations. To the best of our knowledge, there is no research that addresses the problem using our methodology and provides results of such generality. Hence, we believe that our charaterization is a new way to address the problem of estimation over lossy networks.

\newpage
\noindent{\bf Reviewer \# 13}
\emph{The paper considers the asymptotic behavior of the Kalman filter for a linear system for which measurements are lost randomly with probability p at each period, independently of the other periods. This problem was considered in Sinopoli et al. [1], where it was found that there is a threshold $p_c$ such that for $p < p_c$ the error covariance matrix diverges if the system is unstable. A lower bound on $p_c$ was also computed and was shown to be exactly equal to $p_c$ when C is invertible.  The goal of this paper is to compute $p_c$ is other situations, where C is not necessarily invertible.\\}
\emph{In my opinion, the paper is too narrow in scope and lacking a compelling story to be acceptable as a full paper. Since it essentially adds some technical results complementing the result of [1], my suggestion would be to resubmit it as a technical note. The authors motivate their problem by a networked controlled problem where the measurements are sent over a packet dropping link. However, I do not find this issue of the boundedness of the Kalman filter particularly interesting, because it arises only when one tries to estimate an observable but unstable system. It is not clear when in a practical networked control scenario one would want to leave the plant unstable and be concerned about any convergence issue of the filters in that case. Following Anderson and Moore, whether or not the error variance is bounded when the system is not stable ``is perhaps an academic point'' [Optimal Filtering, p.78]. }

As discussed above the paper has been considerably modified with a new approach.
The main focus of this paper has been significantly broadened to address the concerns of several reviewers about the novelty of the paper.
This work breaks away from existing body of literature and uses a novel approach to characterize the stability of Kalman filtering under packet losses. There are several important new contributions. First of all the problem formulation is general and can address any packet drop model. While existing results partially address the stability of the first moment of the error covariance under i.i.d packet losses, we characterize the stability of all moments under Markovian packet loss models. We also show that the proposed methodology applies to general packet loss models, provided that certain probabilities associated with the specific loss process can be computed. This not only advances the current state of the art, as the computation of the critical value becomes a special case, but generalizes to a much larger class of erasure channel models. This goal is accomplished by characterizing the tail distribution of $trace(P_k)$, i.e. study how $P(trace(P_k) > M)$ converges to $0$ as $M$ goes to infinity. We will show that, under minimal assumptions on the system, $trace(P_k)$ follows a power decay law. This result has tremendous value of its own, as it provides the designer with a quantitative bound on the quality of the estimator. We are able compute the exact decay rate for a large class of linear Gaussian systems, defined as non-degenerate. We illustrate the relationship between non-degeneracy and observability and argue that such condition, rather than the weaker notion of observability, is the appropriate one to check when the observation process is random. Under this assumption we derive useful bounds for the Riccati equation, that can be used independently of any loss process.
We compute the critical value for non-degenerate system as a consequence of the tail distribution and prove that it attains the lower bound derived in [1]. Degenerate systems will in general have a higher critical probability, as shown in [7].

We do agree that studying the estimation problem alone on an unstable system is not so meaningful. However, the problem becomes practical when we close the loop by adding feedback control law. In the classical settings, the separation principle states that in order to control an unstable system, one need both a stable estimator and controller. Moreover, the optimal estimator can be designed regardless of the control law. Schenato et al. also proved similar result for networked control system in their paper ``Foundations of Control and Estimation over Lossy Networks'' published in January 2007 on Proceedings of IEEE. Hence, we believe our effort has meanings as it is the first step to stabilize an unstable system in a lossy network.

\emph{The condition of [1] that C is invertible is replaced here by a technical condition, the non-degeneracy condition of def. 5 in the main theorem (thm. 4). The authors however do not spend much time motivating this condition or providing an example to show that it is potentially interesting and worth the trouble of following a much longer proof than for the cases considered in [1] or [3] for example. An attempt at motivating the assumptions of thm. 4 is made on p.6 in the 2 paragraphs preceding the statement of the thm. I agree that one-step observability for C is too strong. However, the hypothesis should be compared to the available one of Plarre and Bullo instead [3], which is weaker than one-step observability and simpler to state than the assumption of thm. 4.  Given the absence of a proper motivation for the results and assumptions of thm 4, I found it difficult to follow the 20 pages of computations proving it.}

In the new version we add several comments on the condition of non-degeneracy and the reason why we believe it is the proper assumption to study the Kalman filtering in lossy network. The main reason is the loss of observability when randomly sampling the system, which is incurred by the packet drops. It is well known that observability may be lost when discretizing a continuous time system since the distinct eigenvalues may rotate to the same position. The same problem appears when some packets are dropped, which is equivalent to sampling the system at a random rate. The condition of non-degeneracy will ensure that no matter how the system is sampled, it will always be ``observable''(Observable in the sense of equation (9)). The detailed discussion can be found in Subsections III-A and III-B.

Also a detailed comparison has been add to compare the result of Plarre and Bullo with ours in Subsection III-C.

\emph{In addition, it is assumed that A is diagonalizable, which was not assumed in [1],[3]. Note that this is not satisfied for some interesting discrete-time examples, such that the discretization of a contintuous-time double integrator (i.e., where A=[1,h;0,1] - or more generally the discretization of a continuous-time system that has at least one Jordan block of size >= 2 with 1's on the superdiagonal. These systems do arise in engineering applications).}

We do agree that the diagonalizabiliby condition excludes some interesting systems and it is our goal to address such cases. However we think the case presented addresses most interest systems. We believe that  extensions to the systems mentioned above will be lengthy and will most certainly require another paper.

\emph{If the paper is submitted as a full paper, then the bibliography looks rather thin to me. In particular, the oldest papers referenced are from 2004, whereas it seems to me that these issues have been considered at least since Athan et al.'s work on the uncertainty threshold principle in the 70's. It seems intuitively that by duality between the control and estimation problems, these results should be connected, although in their case the Riccati recurrence is deterministic (probably because they integrate wrt the disturbances -here gammak- earlier).}

The papers of Athans et al. as well as several other citations have been added to the bibliography. However we would like to point out altough philosophically our works and the works of Athans et al. are related, technically it is non-trivial to derive our results from their work. In fact, as the reviewer pointed out, they deal with deterministic Riccati equations. Meanwhile in our case the Riccati equation is stochastic. Also it is worth point out that in the case of packets loss, the duality of estimation and control does not always hold, as is shown in the paper of Schenato et al., where the authors could compute the exact critical value for the controller but not for the estimator.

\emph{Part VI (2x2 case): the authors seem to include the case of A a complex matrix, since lambda1 and lambda2 are not necessarily complex conjugates. In that case, the iterates $x_k$ can become complex valued as well, and intuitively I start having doubts about some previous results, e.g. the meaning of the Kalman filter for complex valued signals, the use of inequalities, etc. It might be the case that all this still makes sense. But again A complex valued seems to be just a mathematical curiosity without propor motivational discussion. If the eigenvalues are complex conjugates then maybe you can find a simpler proof. }

This section has been completely removed. However we still want to make some comments on this issue. Since we usually assume $A$ matrix is in its diagonal standard form, we have to assume all the vectors and matrices are in the complex domain. Otherwise the discussion in this paper will be too restricted. A remark on the complex domain has been added in the problem formulation paper. As a result it should be clear to the reader why complex domain is proper for this problem.

\emph{I've recorded a few typos:\\
- review the sentence "In particular, ..." in the abstract.\\}
The sentence has been removed.

\emph{- p.3 last paragraph before II: "the paper **are** organized"\\}
Changed.

\emph{- p.3 last sentence: mention that wk,vk are iid - or say simply that they are uncorrelated white Gaussian noise\\}
The i.i.d. assumption is claimed later in this paragraph.

\emph{- p.4: displays (3), (4) replace quantifiers by for some, for all (especially the first one)\\}
Changed.

\emph{- p.4: footnote 3, review that sentence\\}
Changed.

\emph{- p.6: "It can be seen... **more weaker**..."\\}
Changed.

\emph{- proof of thm. 3: at some places, m should be n. Last line of display (12): change the order of the matrix multiplication before sign <=\\} 
Removed from the new version.

\emph{- display (36): it seems that O is used before defining it 3 lines below\\}
Changed. Now $O$ is defined right after $U$

\emph{- p.18: since Mij has the same structure **with** U}
Changed.

\newpage
\noindent{\bf Reviewer \# 18}

\emph{The paper deals with the problem of finding the critical probability for the Kalman filter with intermittent observations, which was analyzed in the earlier work, mostly in [1].\\
The paper gives a complete characterization of the critical value for the, so called, non-degenerate systems (Theorem 4).  Even though, for the degenerate systems, the exact critical value has been found only for the second order systems (Theorem 5), to my opinion, the presented results are still publishable.}

\emph{A few comments and typos:}

\emph{1. Some remark should be added which clarifies why the non-degeneracy is important. It seems to me that it is being used only in Lemmas 4 and 6, due to some technical reasons. A broader picture regarding the importance of this property should be given (or just state that it is due to technical reasons).}

In the new version we add several comments on the condition of non-degeneracy and the reason why we believe it is the proper assumption to study the Kalman filtering in lossy network. The main reason is the loss of observability when randomly sampling the system, which is incurred by the packet drops. It is well known that observability may be lost when discretizing a continuous time system since the distinct eigenvalues may rotate to the same position. The same problem appears when some packets are dropped, which is equivalent to sampling the system at a random rate. The condition of non-degeneracy will ensure that no matter how the system is sampled, it will always be ``observable''(Observable in the sense of equation (9)). The detailed discussion can be found in Subsections III-A and III-B.

\emph{2. Some example, where the critical value is calculated numerically, should be given. It could illustrate the property that for degenerate systems the critical value differs from (6). It could include both second order (for which the critical value is given by (8)) and higher order systems. This could also help in clarifying the importance of non-degeneracy.}

Since the focus of the paper has been changed, we now only deal with non-degenerate system, whose critical value can be computed via Thereom~7. Hence, we think it may not be necessary to add an example in the new version since Theorem~7 is quite self-explanatory. The importance of non-degeneracy is discussed in Subsection III-A,B and we also refer to one of our early papers where we proved that the critical value for a degenerate system is in general different from the one of Theorem~7.

\emph{3. In the assumption (H2) it should be noted that the matrix A is diagonalizable with respect to the set C (set of complex number), and not R (which would be too restrictive).}

This issue is clarified in the new version. Now all matrices and vectors are defined in the complex domain.

\emph{4. On page 7, in the paragraph above Theorem 5, it should be $C^{2x2}$ (instead of $R^{2x2}$).}

The discussion on the second order system has been removed to make the paper more compact. Also every matrix is now defined in the complex domain in the new version.

\emph{5. Page 16, in the formulation of Lemma 5, $i_j-s$ are not defined.}

Changed.

\emph{6. In the equation (40), both "greater than or equal" should be "less than or equal".}

Changed. Now it is the last equation in the appendix.

\emph{7. In the last conclusion of the proof of Theorem 7 mention that you are using Theorem 6.}

Theorem~7 has been merged to the appendix and we think it should be clear now.

\emph{8. The purpose of the Corollary 1 is not clear. Since $\delta i_j$ needs to be large enough, then k also needs to be large enough?}

Corollary~1 is now Theorem~4 in the new version. We think the statement should be more clear now.

\emph{9. In the equation (52) the term ($2-z^{i-j}-z^{j-i}$) is missing from the middle term, defining the determinant.}

This section has been removed from the new version.
\end{document}
% ----------------------------------------------------------------
