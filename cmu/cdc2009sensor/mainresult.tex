This section is devoted to manipulating and relaxing the optimization problem $(P_0)$ to make it explicit and convex.

\subsection{Reformulation}

In this subsection, we will derive the relation between $P_{k|k}$ and $\vec \gamma$ in an explicit way. We will first consider the Kalman filter without sensor selection, which clearly provides a lower bound for the optimization problem $(P_0)$. Then we will propose a different formulation of Kalman filter that explicitly relates the error covariance matrix $P_{k|k}^*$ at step $k$ with all previous measurements $y_i$, for $i=1 \dots k$. To conclude, we will extend this formulation to the case of sensor selection and we will use it to express $(P_0)$ explicitly in terms of the $\vec \gamma$.

First let us consider the estimation problem for a linear systems without sensor selection, i.e. all the readings from sensors are used. It is well known that Kalman filter gives the optimal solution which it takes the following form
\begin{align}\label{eq_Kalman_filter}
    \hat x_{1| 0}^* & = 0 ,\quad P_{1|0}^*  = \Sigma\\
    \hat x _{k + 1|k}^* & = A \hat x^*_{k|k}  , \quad P_{k + 1|k}^*  = AP^*_{k|k} A^H  + Q \nonumber\\
    K_k^*& = P_{k|k - 1}^* C^H (CP_{k|k - 1}^* C^H  + R)^{ - 1}  \nonumber\\
    \hat x^*_{k|k}& = \hat x^*_{k|k - 1}  + K_k^* (y_k  - C \hat x _{k|k - 1}^* )  \nonumber\\
    P^*_{k|k}& = P^*_{k|k - 1}  -  K^*_k CP^*_{k|k - 1}.  \nonumber
  \end{align}
  This formulation of the Kalman filter explicitly shows the dependence of the estimated state $\hat x_{k|k}^*$ from $y_k$. However the dependence from the 'old' measurements is only recursive. In the following Lemma we express the estimated state $\hat x_{k|k}^*$ given by the Kalman filter in terms of all the observations, represented by $Y_k$.

\begin{lemma}\label{def_G}
Consider a linear system as in \eqref{eq:systemdiscription}. The state estimation $\hat x_{k|k}^*$ given by the Kalman filter is
\begin{equation}
  \hat x_{k|k}^* = G^*_k \left[ {\begin{array}{*{20}c}
    {y_1 }  \\
    \vdots   \\
    {y_{k-1} }  \\
    {y_k }  \\
  \end{array}} \right] = G^*_k Y_k \,,
  \label{eq:kalmanestimation}
\end{equation}
where $G_1^* = K_1^*$ and $G_k^*$ can be evaluated recursively as
\begin{equation}
  G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*]\,,
  \label{eq:kalmangain}
\end{equation}
where $K_k^*$ is the optimal Kalman gain given by \eqref{eq_Kalman_filter}.
\end{lemma}
\begin{proof}
We know that Kalman filter is the optimal linear estimator, so $\hat x_{k|k}^*$ is linear in $Y_k$ and can be expressed as
\begin{equation}
  \hat x_{k|k}^* = G^*_k \left[ {\begin{array}{*{20}c}
    {y_1 }  \\
    \vdots   \\
    {y_{k-1} }  \\
    {y_k }  \\
  \end{array}} \right] = G^*_k Y_k \,,
\end{equation}
where $G^*_k$ is the optimal gain which can be computed from Kalman filter equations \eqref{eq_Kalman_filter}. In particular, $\hat x^*_{1|1} = G_1^* Y_1 = K_1^* y_1$, and
\[
\begin{split}
\hat x_{k+1|k+1}^* &= G_{k+1}^* Y_{k+1} = K_{k+1}^* y_{k+1} + (A-K_{k+1}^*CA)\hat x_{k|k}^*\\
& = K_{k+1}^* y_{k+1} + (A-K_{k+1}^*CA)G_k^* Y_k\,.
\end{split}
\]
Thus, $G_k^*$ can be computed via the following recursive equation
\begin{equation}
  G_{k+1}^* = [(A-K_{k+1}^*CA)G_k^*,\;K_{k+1}^*]\,.
\end{equation}

\end{proof}
\begin{remark}
Note that, by the optimality of Kalman filter, $x_k - \hat x_{k|k}^* =  x_k - E(x_k|Y_k)$ is orthogonal to $Y_k$.
\end{remark}

Now, let us consider the sensor selection problem. In the case of sensor selection, the estimated state $\hat x_{k|k} = E(x_k|\Gamma_kY_k)$ can still be written as
\begin{equation}
  \begin{split}
  \hat x_{k|k} &= G_k\Gamma_k Y_k ,\\
    P_{k|k} &=Cov(\hat x_{k|k}  - x_k) = Cov(G_k\Gamma_k Y_k - x_k) , \\
  \label{eq:kalmanestimationforselection}
  \end{split}
\end{equation}
where $G_k$ is the optimal gain under the sensor selection scheme $\Gamma_k$. In general, $G_k$ is different from $G^*_k$ and depends on the sensor selection strategy $\Gamma_k$ we choose. However we will not write $G_k$ explicitly in terms of $\Gamma_k$, because that will lead to a Riccati equation, which is hard to analyze in general. In contrast, by exploiting the optimality of Kalman filter, we consider $G_k$ as a matrix that we need to optimize over. In the next Theorem, we propose a new formulation for the minimization problem of $trace(P_{k|k})$ over both $G_k$ and $\Gamma_k$.
\begin{theorem}\label{main_theor}
Consider the linear system \eqref{eq:systemdiscription} and let be $\Gamma_k$ the sensor selection matrix.  The minimization problem over $\Gamma_k$ of the $trace(P_{k|k})$, given by the Kalman filter, can be formulated as
\begin{align}
 \min_{G_k,\Gamma_k}\;  \left\|G_k\Gamma_k\mathcal S_k -  G^*_k \mathcal S_k \right\|_F^2
\end{align}
where $\left\|\cdot\right\|_F$ represents the Frobenius norm and
\begin{align}
&\mathcal S_k \in \mathbb R^{mk\times mk}\;:\;Cov(Y_k) = \mathcal S_k \mathcal S_k
\end{align}
and $G_k^*$ is given in \eqref{eq:kalmangain}.
\end{theorem}
\begin{proof}
In case of sensor selection, the covariance matrix can be written as
\begin{displaymath}
  \begin{split}
    P_{k|k} &=Cov(\hat x_{k|k}  - x_k) = Cov(G_k\Gamma_k Y_k - x_k) \\
    &= Cov [(G_k\Gamma_k - G_k^*) Y_k  + G_k^* Y_k - x_k ]\\
    & = Cov [(G_k\Gamma_k - G_k^*) Y_k]  + Cov(G_k^* Y_k - x_k )\\
    & = (G_k\Gamma_k - G_k^*) Cov(Y_k)(G_k\Gamma_k - G_k^*)^T  + P^*_{k|k}.\\
  \end{split}
\end{displaymath}
The fourth equality is true because $G_k^*Y_k - x_k$ is orthogonal to $Y_k$. By the optimality of Kalman filter, we have
\begin{equation}
  \begin{split}
&\min_{\Gamma_k} \;trace( P_{k|k})=\min_{\Gamma_k} trace(P_{k|k}^*)+\\
&\min_{\Gamma_k,G_k} \; trace\left[(G_k \Gamma_k - G_k^*) Cov(Y_k) (G_k \Gamma_k - G_k^*)^T\right].
  \end{split}
  \label{eq:optimazition1}
\end{equation}
Since the first term does not depend on $\Gamma_k$, we will focus only on the second term.  Let us decompose $Cov(Y_k)$ as $\mathcal S_k \mathcal S_k$, where $\mathcal S_k$ is symmetric. Since $Cov(Y_k)$ is positive semidefinite, we can always find $\mathcal S_k$. Thus
\begin{equation}
  \begin{split}
  &(G_k \Gamma_k - G_k^*) Cov(Y_k) (G_k \Gamma_k - G_k^*)^T \\
  &= (G_k\Gamma_k \mathcal S_k -  G_k^*\mathcal S_k)(G_k\Gamma_k \mathcal S_k -  G_k^*\mathcal S_k)^T.
  \end{split}
\end{equation}

As a result, the minimization problem $\min_{\Gamma_k}\; trace(P_{k|k})$ is equivalent to
\begin{align*}
\min_{\Gamma_k,G_k}& \; trace\left[(G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k)  (G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k)^T\right] = \\
\min_{\Gamma_k,G_k}& \;\left\|G_k \Gamma_k\mathcal S_k - G_k^*\mathcal S_k\right\|_F^2.
\end{align*}
\end{proof}
From Theorem \ref{main_theor} we can immediately derive the following corollary.
\begin{corollary}\label{cor1}
Consider the linear system \eqref{eq:systemdiscription} and let $\Gamma_k$ be the sensor selection matrix. The minimization problem over $\Gamma_k$ of the $trace(\mathcal Q_k P_{k|k} \mathcal Q_k^T)$ given by the Kalman filter can be formulated as
\begin{align}
 \min_{\Gamma_k,G_k} \;  \left\| \mathcal Q_k G_k\Gamma_k \mathcal S_k -  \mathcal Q_k G^*_k\mathcal S_k \right\|_F^2
\end{align}
where
\begin{align*}
&\mathcal S_k \in \mathbb R^{mk\times mk}\;:\; Cov(Y_k) = \mathcal S_k \mathcal S_k \nonumber
\end{align*}
and $G_k^*$ is the gain matrix given by the Kalman filter when all the sensors are used.
\end{corollary}
\begin{proof}
It directly follows from the proof of Theorem \ref{main_theor}.
\end{proof}
Now let us define the matrix
\begin{equation}
  \mathcal G_k \triangleq \mathcal Q_k G_k \Gamma_k \in \mathbb R^{n' \times mk}.
\end{equation}
The following theorem gives the relation between $\gamma_i$ and $\mathcal G_k$.
\begin{theorem}
  Denote the $i$th column of the matrix $\mathcal G_k$ as ${\vec {\mathcal G}}_{k,i}$. The following inequality 
  %of $\gamma_i$ and ${\vec {\mathcal G}}_{k,i}$ 
  holds:
 \begin{equation}
   \gamma_i \geq \left\|\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1\right\|_0,
 \end{equation}
 where $\left\|\cdot \right\|_0$ of a scalar is $0$ if the scalar is $0$ (it is $1$ otherwise) and $k'$ is the smallest number which satisfies $mk' > i$.
\end{theorem}
\begin{proof}
  It follows directly from the definition of $\mathcal G_k$.
\end{proof}

By all the previous arguments, we can prove the following theorem:
\begin{theorem}
The optimization problem $(P_0)$ is equivalent to
\begin{align} \label{non_conv_opt}
(P_0'):\, &\min_{\mathcal G_k} && \sum_{k=1}^T  \left\| \mathcal G_k \mathcal S_k - \mathcal Q_kG_k^*\mathcal S_k \right\|_F^2,\\
  &s.t.&&  H \vec \gamma \leq b \nonumber
\end{align}
with
\[
   \gamma_i \geq \left\|\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1\right\|_0.
\]
\end{theorem}
\begin{remark}
The main improvement of formulation $(P_0')$ over $(P_0)$ is that instead of $trace(\mathcal Q_k P_{k|k}\mathcal Q_k)$, we now have an explicit quadratic objective function and the relation between objective function and $\gamma_i$s is also explicit.
\end{remark}

In general, sensor selections are hard combinatorial problems by nature as they involve binary constraints. For large systems the problem becomes computationally infeasible. Solutions in this case are based on heuristic methods. As a result, $(P_0')$ is no exception , as it is also a combinatorial problem, since the last constraint of $(P_0')$ contains a zero norm. However, formulation $(P_0')$ allows us to easily relax it to a convex problem, which will be discussed in the next subsection.

\subsection{Reweighted $L_1$ approximation}
This subsection describes a reweighted $L_1$ approximation of problem $(P_0')$.

As being discussed in the previous subsection, $(P_0')$ is a combinatorial problem. In principle, to find the true optimal solution, we should check all possible sensor selection schedule verifying the constraints. However, if the number of sensors is fairly large, this evaluation will become infeasible. Hence it can necessary to consider solutions which are suboptimal, but computationally feasible. We will derive suboptimal solution based on a relaxed convex version of the original optimization problem \cite{BOYD:Convex}.

One possible convex approximation of the boolean-convex optimization problem $(P_0')$ is given by the reweighted $L_1$  minimization \cite{boydreweight}. According to this method, the $L_0$ norm is substituted with a weighted $L_1$ norm, where the weights are chosen to avoid the penalization, given by the $L_1$ norm, of the bigger coefficients. In \cite{boydreweight}, the authors propose an iterative algorithm that alternates between an estimation phase and a redefinition the weights, based on the empiric consideration that the weights should relate inversely to the true signal magnitudes. In the following we propose this algorithm to relax the optimization problem $(P_0')$. The algorithm is composed of $4$ steps:

\begin{enumerate}
\item Set the iteration count $l$ to zero and set the weights vector to $w_i^{0}=1$ for $i=1,...., mT$
\item Solve the weighted $L_1$ minimization problem
\begin{align} \label{conv_opt}
(P_1):\, &\min_{\mathcal G_k} && \sum_{k=1}^T  \left\| \mathcal G_k \mathcal S_k - \mathcal Q_kG_k^*\mathcal S_k \right\|_F^2,\\
  &s.t.&&  H \vec \gamma \leq b \nonumber
\end{align}
with
\[
\gamma_i \geq w_i^{l}\sum_{k = k'}^T \left\|{\vec {\mathcal G}}_{k,i}\right\|_1.
\]
Let the solution be $\gamma_1^l,\ldots,\gamma_{mT}^l$
\item Update the weights
\[
w_i^{l+1}=\frac{1}{\gamma_{i}^l+\epsilon}\quad,i=1, \dots ,mT
\]
\item Terminate on convergence or when $l$ reaches a specified maximum number of iterations $l_{max}$. Otherwise, increment $l$ and go to step 2.
\end{enumerate}
\begin{remark}
  As in \cite{boydreweight}, we introduce the parameter $\epsilon >0$ in step 3 in order to avoid inversion of zero-valued component in $\vec \gamma$.
\end{remark}
\begin{remark}
One thing worth noticing is that we do not make any assumption on the covariance matrices $Q,\,R,\,\Sigma$ during the reformulation and relaxation steps. As a result, the relaxed problem $(P_1)$ can handle correlated measurement noise.
\end{remark}

The validity of this method will be shown in the next section.
