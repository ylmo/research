This section is devoted to deriving a general framework which can capture a large number of sensor selection problems.

Consider a linear system
\begin{equation}
  \begin{split}
    x_{k+1} &= Ax_k + w_k,\\
    y_{k} &= C x_k + v_k.
  \end{split}
  \label{eq:systemdiscription}
\end{equation}
$w_k,v_k,x_1$ are independent Gaussian random variables, and $x_1 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. We suppose that $x_k \in \mathbb R^n$ and $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}  ]^T \in \mathbb R^m$ is the vector of the measurements from the sensors. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$.

Let us indicate with $\gamma_{j}, j=1, \ldots, mT$ the binary variables described in the following way: $\gamma_{m(k-1)+i}=1$ if sensor $i$ transmits at step time $k$, for $i=1 \dots m$ and $k=1 \dots T$, and it is $0$ otherwise. Thus, at time $k$, the estimator receives readings $[\gamma_{m(k-1)+1}y_{k,1},\ldots,\gamma_{m(k-1)+m}y_{k,m}]^T$.

The aim of this paper is to find a sensor selection sequence $\gamma_1,\ldots,\gamma_{mT}$ from time $1$ to time $T$, which provides the optimal estimation performance while satisfying specific energy and topology constraints.

In order to define a multi-step Kalman filter with sensor selection, let us introduce the following quantities
\begin{align}
  Y_k &\triangleq  [y_1^T,y_2^T,\ldots,y_k^T]^T &\in& \mathbb R^{mk},\\
  \Gamma_k &\triangleq diag(\gamma_1,\ldots,\gamma_{mk}) &\in& \mathbb R^{mk\times mk},\\
  \vec \gamma &\triangleq [\gamma_1,\ldots,\gamma_{mT}]^T &\in& \mathbb R^{mT},\\
  \hat x^*_{k|k} &\triangleq E\left(x_k|Y_k\right)&\in&\mathbb R^{n},\\
  P^*_{k|k} &\triangleq Cov\left(x_k|Y_k\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k|k} &\triangleq E\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n},\\
  P_{k|k} &\triangleq Cov\left(x_k|\Gamma_kY_k\right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}

The multi-step sensor selection problem can be formulated as the following optimization problem
\begin{align}\label{opt_prob}
 (P_0):\, &\min_{\vec\gamma} && \sum_{k=1}^T trace(\mathcal Q_kP_{k|k}\mathcal Q_k^T),\\
  &s.t.&&  H \vec \gamma \leq b, \nonumber\\
  &&& \gamma_i = 0\;or\;1,\;i=1,\ldots,mT, \nonumber
\end{align}
where $\mathcal Q_k \in \mathbb R^{n' \times n}$ and is of full row rank,  $H \in R^{h \times mT}$ and $b\in R^h$.

 We will show how this formulation can address several classes of sensor selection problems, despite that fact that it may look restrictive at first. In this framework the matrices $\mathcal Q_k$ can be used to define the minimization problem we want to tackle, while the matrix $H$ and the vector $b$ define the constraints we impose on the wireless sensors. In the following paragraph we outline some of the problems that can be tackled in the proposed framework, by  appropriate choice of the matrices $\mathcal Q_k$s.
\begin{itemize}
  \item \textit{Minimization of the final estimation error}

    Suppose that we only want to minimize the estimation error at time $T$. Thus, we can let \[\mathcal Q_1,\ldots,\mathcal Q_{T-1} = 0,\;\mathcal Q_T = I.\] 
  \item \textit{Minimization of the average estimation error}

    Suppose that we want to minimize the average estimation error from time $1$ to time $T$. Thus, we can choose \[\mathcal Q_k = I, k = 1,\ldots,T.\]

  \item \textit{Minimization of the estimation error of a single state}

    Suppose that at time $k$ we are only interested in the $i$th state $x_{k,i}$ of the system. Thus, we can choose \[\mathcal Q_k = [\delta_{1,i},\ldots,\delta_{n,i}],\] where $\delta_{i,j} = 1$ if $i = j$, it is $0$ otherwise.

    It is easy to see that this constraint can be implemented in both cases of final and average value of the $i-th$ state variable.

  \item \textit{Minimization of the objective function of a finite horizon Linear Quadratic Gaussian (LQG) regulation problem}

    Suppose that we have the following LQG cost function
    \begin{displaymath}
      J_T = E\left[x_T^T W_T x_T + \sum_{k=1}^{T-1} (x_k^TW_kx_k+u_k^TU_ku_k)\right],
    \end{displaymath}
    where $W_k, U_k$ are some positive semidefinite matrices. Given a sensor selection schedule, it is well known that the optimal controller and estimator, which minimize the LQG cost function, can be designed separately. The optimal control law is
    \begin{displaymath}
      \begin{split}
	S_k &= A^TS_{k+1}A+W_k\\
	&- A^TS_{k+1}B(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A,\\
	u_k &= -(B^TS_{k+1}B+U_k)^{-1}B^TS_{k+1}A\hat x_{k|k},
      \end{split}
    \end{displaymath}
where $S_T = W_T$. The optimal estimator is still the Kalman filter. The optimal value of cost function is
\begin{equation}
  \begin{split}
  J_T^* &= trace(S_1\Sigma)+ \sum_{k=1}^{T-1}trace(S_{k+1}Q)
  \\&+ \sum_{k=1}^{T-1}trace[(A^TS_{k+1}A+W_k-S_k)P_{k|k}],
\end{split}
\end{equation}
Thus, in order to find the best sensor selection schedule which minimizes $J_T^*$, we can let \[\mathcal Q_k^T\mathcal Q_k = A^TS_{k+1}A + W_k - S_k , \mathcal Q_T = 0.\]
\end{itemize}
So far we have concentrated exclusively on the cost function. Next we will show how several network constraints can be formulated within the proposed framework.
\begin{itemize}
  \item \textit{Fixed number of sensors to be used at each time step}

    Suppose that at each time step we only want to select no more than $p < m $ sensors. Thus, the constraints can be written as
    \begin{align}\label{fix_number}
    \sum_{i=1}^m\gamma_{m(k-1)+i} \leq p, \;k=1,\ldots,T
    \end{align}
  \item \textit{Sensor energy constraints}

    Suppose that each sensor has initial energy $\mathcal E_1,\ldots,\mathcal E_m$ and each observation consumes $e_1,\ldots,e_m$ energy units respectively. As a result, sensor $i$ can do at most $\mathcal E_i/e_i$ observations until its battery runs out. Thus, the energy constraints can be written as
\begin{align}\label{lifetime}
e_i\sum_{k=1}^T \gamma_{m(k-1)+i} \leq \mathcal E_i,\;i =1,\ldots,m
\end{align}
  \item \textit{Multi-hop sensor networks}

    Consider that the sensor network has a tree structure and the estimator is the root node. Define the set of child nodes of sensor $i$ to be $\mathcal C_i$. We can write the topology constraints of the network as
    \begin{align}\label{multi-hop}
    \gamma_{m(k-1)+i} \geq \gamma_{m(k-1)+j},\;k =1,\ldots,T,\;j \in \mathcal C_i\,.
    \end{align}
\end{itemize}

