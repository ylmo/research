\documentclass[review]{elsarticle}
\usepackage{color}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}

\newtheorem{thm}{Theorem}
\newtheorem{lem}[thm]{Lemma}
\newdefinition{rmk}{Remark}
\newdefinition{definition}{Definition}
\newproof{pf}{Proof}

\DeclareMathOperator*{\trace}{trace}     % trace
\DeclareMathOperator{\diag}{diag}

\title{On Infinite-Horizon Sensor Scheduling}
\author[cmu]{Y.~Mo\corref{cor1}\fnref{fn1}}
\ead{ymo@andrew.cmu.edu}

\author[bel]{E.~Garone}
\ead{egarone@ulb.ac.be}

\author[cmu]{B.~Sinopoli\fnref{fn1}}
\ead{brunos@ece.cmu.edu}

\cortext[cor1]{Corresponding author}

\fntext[fn1]{ This research was partly supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}

\address[cmu]{Department of Electrical and Computer Engineering, Carnegie Mellon University, 5000 Forbes Ave, Pittsburgh, PA, United States}

\address[bel]{ Service d'Automatique et d'Analyse des Syst\`{e}mes  Universit\`{e} Libre de Bruxelles, 50 Av. F.D. Roosevelt, B-1050 Brussels, Belgium.}

\begin{document}
\begin{abstract}
  In this paper we consider the problem of infinite-horizon sensor scheduling for estimation in linear Gaussian systems. Due to possible channel capacity, energy budget or topological constraints, it is assumed that at each time step only a subset of the available sensors can be selected to send their observations to the fusion center, where the state of the system is estimated by means of a Kalman filter. Several important properties of the infinite-horizon schedules will be presented in this paper. In particular, we prove that the infinite-horizon average estimation error and the boundedness of a schedule are independent of the initial covariance matrix. We further provide a constructive proof that any feasible schedule with finite average estimation error can be arbitrarily approximated by a bounded periodic schedule. These theoretical results provide valuable insights and guidelines for the design of computationally efficient sensor scheduling policies.
\end{abstract}
\begin{keyword}
  Wireless Sensor Networks, Kalman Filtering, Riccati Equation
\end{keyword}
\maketitle
\section{Introduction}

\input{intro.tex}

\section{Problem Formulation}
\label{sec:problem}
Consider the following discrete-time LTI system
\begin{equation}
  x_{k+1} = Ax_k + w_k,
  \label{eq:systemdescription}
\end{equation}
where $x_k \in \mathbb R^n$ represents the state and $w_k\in \mathbb R^n$ the process noise. It is assumed that $w_k$ and $x_0$ are independent Gaussian random vectors, $x_0 \sim \mathcal N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$, where $\Sigma,\,Q > 0$\footnote{All the comparisons between matrices are in the sense of positive semidefinite.}. A wireless sensor network composed of $m$ sensing devices $S = \{s_1,\ldots,s_m\}$ and one fusion center is used to monitor the state of system \eqref{eq:systemdescription}. The measurement equation is
\begin{equation}
  y_{k} = C x_k + v_k,
\end{equation}
where $y_k = [y_{k,1}, y_{k,2} ,\ldots, y_{k,m}]' \in \mathbb R^m$ is the measurement vector\footnote{The $'$ on a matrix always means transpose.}. Each element $y_{k,i}$ represents the measurement of sensor $i$ at time $k$, $C = \left[C_1',\ldots,C_m'\right]'$ is the observation matrix and the matrix pair $(C,\,A)$ is assumed observable, $v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed to be independent of $x_0$ and $w_k$.

Suppose that due to energy, bandwidth or topological constraints, only a subset of sensors can be chosen to send their measurements to the fusion center. Denote the collection of all eligible subsets as $\mathcal S \subseteq \mathcal P(S)$, where $\mathcal P(S)$ is the power set of $S$, i.e., the collection of all subsets of $S$.

For any $\mathcal I =\{s_{i_1},\ldots,s_{i_l}\} \subseteq S$, we define the selection matrix $\Gamma(\mathcal I)$
\begin{displaymath}
  \Gamma(\mathcal I) \triangleq  \left[e_{i_1},\ldots,e_{i_l}\right]' ,
\end{displaymath}
where $e_i$ is the $i$th vector of the canonical basis, i.e. a vector with entries $0$ everywhere, except a $1$ at the $i$th entry. By means of this selection matrix we can define the matrices
\begin{displaymath}
  C(\mathcal I) \triangleq \Gamma(\mathcal I)C,\, R(\mathcal I) \triangleq \Gamma(\mathcal I)R\Gamma(\mathcal I)',
\end{displaymath}
that allows one to define the matrix-valued function $g(X,\mathcal I)$ as
\begin{displaymath}
  g(X,\mathcal I) \triangleq \left[ (AXA' + Q)^{-1} + C(\mathcal I)'R(\mathcal I)^{-1}C(\mathcal I)\right]^{-1}.
\end{displaymath}

A schedule is defined as an infinite sequence of $\sigma \triangleq (\mathcal I_1,\,\mathcal I_2,\,\ldots)$, where $\mathcal I_k \in \mathcal S$. Clearly, if a schedule $\sigma$ is used, the covariance of the Kalman filter satisfies the following equation:
\begin{displaymath}
  P_{k} = g(P_{k-1},\mathcal I_k),\,P_0 = \Sigma.
\end{displaymath}
%
Being a function of both the sensor schedule $\sigma$ and the initial condition $\Sigma$, we will denote $P_k$ as $P_k(\sigma,\Sigma)$.
Let us define the cost function $J(\sigma,\Sigma)$ as
\begin{displaymath}
  J(\sigma,\Sigma) \triangleq \limsup_{N\rightarrow \infty} \frac{1}{N}\sum_{k=1}^N \trace(P_k(\sigma,\Sigma)).
\end{displaymath}
$J(\sigma,\Sigma)$ can be seen as the average estimation error. Moreover, let us define the average communication rate of sensor $i$ as
\begin{displaymath}
  r_i(\sigma) \triangleq \limsup_{N\rightarrow \infty}\frac{1}{N}\sum_{k=1}^N \mathbb I_{s_i\in \mathcal I_k} ,
\end{displaymath}
where $\mathbb I$ is the indicator function.
\begin{rmk}
  Our formulation can address a large class of sensor selection problems. In particular, the set $\mathcal S$ can be used to characterize the topological and communication constraints of the network at one given time, while $r_i$s define the average usage of sensors, i.e., the energy constraints on sensors.
\end{rmk}
We have the following definitions:

\begin{definition}
  A schedule $\sigma$ is called feasible if for all initial condition $\Sigma$, $J(\sigma,\Sigma)<\infty$.
\end{definition}

\begin{definition}
  A schedule $\sigma$ is called bounded if for all initial condition $\Sigma>0$, there exists a matrix $M(\Sigma)$, such that $P_k(\sigma,\Sigma)\leq M(\Sigma)$ for all $k$\footnote{Note that while boundedness clearly implies feasibility, the converse is not always true. It is enough to consider the sequence of $P_k$ is $\{1,0,2,0,0,3,0,0,0,4,,...\}$ to see that while $P_k$ is unbounded, the average cost $J(\sigma,\Sigma)$ would be bounded and in particular equals to 1.}.

\end{definition}
\begin{definition}
  A schedule $\sigma$ is called periodic if there exists $T > 0$, such that $\mathcal I_{k+T} = \mathcal I_k$ for all $k$.
\end{definition}

In the next section, we will prove that the cost function $J$ does not depend on the initial condition $\Sigma$. In Section~\ref{sec:approximation}, we will prove that we can arbitrarily approximate a feasible schedule by means of bounded periodic schedules.
\section{Independence of the Initial Condition}
\label{sec:independence}
%In this section, we prove that the cost function $J(\sigma,\Sigma)$ is independent of the initial condition $\Sigma$, which is given by the following theorem:
This section is devoted to prove the following Theorem:
\begin{thm}
  If there exists $\Sigma_0 > 0$ such that $J(\sigma,\Sigma_0) <\infty$, then for all $\Sigma > 0$, we have
  \begin{displaymath}
    J(\sigma,\Sigma) = J(\sigma,\Sigma_0) <\infty.
  \end{displaymath}
  Furthermore, if $P_k(\sigma,\Sigma_0)\leq M$ is bounded for all $k$, then the schedule $\sigma$ is bounded.
  \label{thm:independence}
\end{thm}
The main consequence of Theorem~\ref{thm:independence} is that the cost is independent from the initial conditions but only depends on the scheduling, i.e. $J(\sigma,\Sigma)=J(\sigma)$ and that to check the \emph{feasibility} [\emph{boundedness}] of a schedule $\sigma$ it is enough to check if $J(\sigma,\Sigma)$ [\emph{$P_k(\sigma,\Sigma)$}] is bounded for one initial condition $\Sigma>0$. To prove Theorem~\ref{thm:independence}, a few intermediate results have to be derived.

To this end, let us first define the functions:
\begin{align}
  h(X,Y)&\triangleq  X+Y, \label{eq:hfunction}
\end{align}
where $X,Y\in \mathbb R^{n\times n}$ are positive semidefinite and
\begin{align}
  f(X,Y)&\triangleq  (X^{-1} + Y)^{-1},\label{eq:ffunction}
\end{align}
where $Y\in \mathbb R^{n\times n}$ are positive semidefinite and $X\in \mathbb R^{n\times n}$ are strictly positive definite\footnote{We require $X$ to be strictly positive definite in order to ensure that $f$ is a well-defined function.}. Please note that these function are linked to the function $g$ as follows
\begin{equation}
  g(X,\mathcal I) = f(h(A'XA,Q),C(\mathcal I)'R^{-1}(\mathcal I)C(\mathcal I)).
  \label{eq:gfhrelation}
\end{equation}
Therefore, to prove the properties of $g$ it is convenient to first study the functions $f$ and $h$, whose main properties are summarized in the following Lemma:
%
\begin{lem}
  \label{lem:fhproperty}
  For the functions $h(X,Y)$ and $f(X,Y)$ defined in \eqref{eq:hfunction} and \eqref{eq:ffunction}, the following propositions hold true:
  \begin{itemize}
    \item[a)] $f(X,Y)$ and $h(X,Y)$  are monotonically increasing with respect to $X$, i.e., if $X_1\leq X_2$,
      \begin{equation}
	f(X_1,Y)\leq f(X_2,Y),\,h(X_1,Y)\leq h(X_2,Y).
      \end{equation}
    \item[b)] The following inequalities hold:
      \begin{align}
	f ( (1+\rho)X,Y)&\leq (1+\rho)f(X,Y) .\,\forall \rho \geq 0,  \label{eq:fineq}\\
	h ( (1+\rho)X,Y)&\leq (1+\rho)h(X,Y) .\,\forall \rho \geq 0.\label{eq:hineq1}
      \end{align}
    \item[c)] if $Y \geq \alpha X$, then
      \begin{equation}
	h( (1+\rho)X,Y)\leq \left(1+\frac{\rho}{1+\alpha}\right)h(X,Y).\,\forall \rho\geq 0.
	\label{eq:hineq2}
      \end{equation}
  \end{itemize}

\end{lem}
\begin{pf}
  \emph{a)}  The monotonicity of $h$ and $f$ can be proved by direct substitution in \eqref{eq:hfunction} and \eqref{eq:ffunction}.
  \\
  \emph{b)} For \eqref{eq:fineq}, since $X > 0 $ is strictly positive definite, there exists an invertible matrix $U \in \mathbb R^{n\times n}$ that can diagonalize $X^{-1}$ and $Y$ simultaneously, i.e.,
  \begin{equation}
    X^{-1} = U^{-1} (U^{-1})',\;Y = U^{-1}\Lambda (U^{-1})' ,
  \end{equation}
  where $\Lambda = \diag(\lambda_1,\ldots,\lambda_n)$ and $\lambda_i \geq 0$. Hence
  \begin{align*}
    &  f((1+\rho) X,Y) - (1+\rho) f(X,Y)\\
    & = U'\left\{\left[(1+\rho)^{-1}I + \Lambda\right]^{-1} - (1+\rho)(I+\Lambda)^{-1}   \right\}U \\
    & = U' \diag\left(\ldots,\frac{1+\rho}{1+(1+\rho)\lambda_i}-\frac{1+\rho}{1+\lambda_i},\ldots\right) U\,.
  \end{align*}
  For $\rho$ positive the diagonal matrix in the equation above is negative semidefinite and \eqref{eq:fineq} follows.  %Thus,
  %  \begin{align*}
  %  f((1+\rho) X,Y) \leq (1+\rho) f(X,Y). \forall \rho > 0\,.
  %  \end{align*}
  For \eqref{eq:hineq1},
  \begin{align*}
    h ( (1+\rho)X,Y)- (1+\rho)h(X,Y) = ((1+\rho)X+Y) - (1+\rho)(X+Y) =-\rho Y\leq 0 .
  \end{align*}
  \emph{c)}  It is enough to substitute and obtain 
  \begin{align*}
    &h( (1+\rho) X,Y) - \left(1+\frac{\rho}{1+\alpha}\right)h(X,Y)=\\
    &(1+\rho)X+Y - \left(1+\frac{\rho}{1+\alpha}\right)(X+Y)=\frac{\rho(\alpha X - Y)}{1+\alpha} \leq 0,
  \end{align*}
  which concludes the proof. $\hfill \Box$
\end{pf}
The following Lemma illustrates the monotonicity and the ``contraction'' property of the Riccati equation $g$, which follows directly from Lemma~\ref{lem:fhproperty} and the definition of $g$ in terms of $f$ and $h$ given in \eqref{eq:gfhrelation}:
\begin{lem}
  \label{lem:gproperty}
  For all $\rho\geq 0\,,X > X_0 > 0$, then the following inequalities hold,
  \begin{equation}
    g(X_0,\mathcal I)\leq  g(X,\mathcal I) .
    \label{eq:gineq3}
  \end{equation}

  \begin{equation}
    g( ( 1+\rho) )X,\mathcal I)\leq (1+\rho) g(X,\mathcal I) .
    \label{eq:gineq1}
  \end{equation}
  Furthermore, if $A'XA\leq \alpha Q$, then
  \begin{equation}
    g( (1+\rho) X,\mathcal I)\leq \left(1+\frac{\rho}{1+\alpha}\right) g(X,\mathcal I) .
    \label{eq:gineq2}
  \end{equation}
\end{lem}

\begin{lem}
  \label{lem:traceineq}
  Suppose that $X \in \mathbb R^{n\times n}$ is positive semidefinite, then
  \begin{displaymath}
    X\leq \trace(X) I_n,
  \end{displaymath}
  where $I_n\in \mathbb R^{n\times n}$ is the identity matrix.
\end{lem}
\begin{pf}
  Since $X$ is positive semidefinite, all the eigenvalues of $X$ are no greater than $\trace(X)$, which concludes the proof. $\hfill \Box$
\end{pf}

We are now ready to prove Theorem~\ref{thm:independence}
\begin{pf}[Theorem~\ref{thm:independence}]
  Let us choose an arbitrary $\Sigma > 0$. First we will prove that $J(\sigma,\Sigma)\leq J(\sigma,\Sigma_0)$. To this end define $\rho_k$ as\footnote{$\rho_k=0$ if $P_k(\sigma,\Sigma_0)\geq P_k(\sigma,\Sigma)$.}
  \begin{displaymath}
    \rho_k \triangleq \inf\{\rho\geq 0:\,(1+\rho)P_k(\sigma,\Sigma_0)\geq P_k(\sigma,\Sigma)\} .
  \end{displaymath}
  By its definition,we have that 
  \begin{displaymath}
    \begin{split}
      P_{k+1}(\sigma,\Sigma)& = g(P_k(\sigma,\Sigma),\mathcal I_{k+1})\leq  g( (1+\rho_k)P_k(\sigma,\Sigma_0),\mathcal I_{k+1}) \\
      &\leq (1+\rho_k) g(P_k(\sigma,\Sigma_0),\mathcal I_{k+1}) = (1+\rho_k)P_{k+1}(\sigma,\Sigma_0).
    \end{split}
  \end{displaymath}
  The second inequality is true due to \eqref{eq:gineq1}. Therefore, we know that $(1+\rho_k)P_{k+1}(\sigma,\Sigma_0)\geq P_{k+1}(\sigma,\Sigma)$, which implies that $\rho_{k+1} \leq \rho_k$. As a result, $\rho_k$ is monotonically non-increasing. At this point it remains to prove that $\rho_k\rightarrow 0$. To this end, let us define $\alpha > 0$, such that
  \begin{displaymath}
    2J(\sigma,\Sigma_0)A'A \leq \alpha Q.
  \end{displaymath}
  Since we assume that $Q >0$, we can always find such an $\alpha$. Now from the definition of $J(\sigma,\Sigma_0)$, the following inequality holds for infinitely many $k$s:
  \begin{displaymath}
    \trace(P_k(\sigma,\Sigma_0))\leq 2J(\sigma,\Sigma_0).
  \end{displaymath}
  By Lemma~\ref{lem:traceineq}, we know that
  \begin{equation}
    A'P_k(\sigma,\Sigma_0)A\leq A'\left[2J(\sigma,\Sigma_0)I_n\right]A  \leq \alpha Q,
    \label{eq:lessthantrace}
  \end{equation}
  for infinitely many $k$s. Let $k_i$ be the time index when \eqref{eq:lessthantrace} holds. By \eqref{eq:gineq2}, we have
  \begin{displaymath}
    \rho_{k_i+1}\leq \frac{1}{1+\alpha}\rho_{k_i}.
  \end{displaymath}
  Since \eqref{eq:lessthantrace} happens infinitely often and $\alpha > 0$, $\rho_k\rightarrow 0$. At this point, we are ready to prove that $J(\sigma,\Sigma)\leq J(\sigma,\Sigma_0)$. From the definition of $J$, we know that for all $k_0\in \mathbb N$, the following equality holds
  \begin{displaymath}
    J(\sigma,\Sigma_0) = \limsup_{N\rightarrow \infty}\frac{1}{N}\sum_{k=1}^N \trace(P_k(\sigma,\Sigma_0)) =   \limsup_{N\rightarrow \infty}\frac{1}{N}\sum_{k=k_0}^N \trace(P_k(\sigma,\Sigma_0)).
  \end{displaymath}
  Since $\rho_k$ is non-increasing and
  \begin{displaymath}
    \trace(P_k(\sigma,\Sigma))\leq (1+\rho_k)\trace(P_k(\sigma,\Sigma_0)) ,
  \end{displaymath}
  we know that $J(\sigma,\Sigma)\leq (1+\rho_k)J(\sigma,\Sigma_0)$ for all $\rho_k$. Now use the fact that $\rho_k\rightarrow 0$, we have
  \begin{displaymath}
    J(\sigma,\Sigma)\leq J(\sigma,\Sigma_0) < \infty.
  \end{displaymath}
  By using the very same argument, one can also prove that $J(\sigma,\Sigma_0)\leq J(\sigma,\Sigma)$. Therefore, for all $\Sigma > 0$,
  \begin{displaymath}
    J(\sigma,\Sigma_0)= J(\sigma,\Sigma).
  \end{displaymath}
  For the boundedness of schedule $\sigma$, since $\rho_k$ is non-increasing, it is clear that
  \begin{displaymath}
    P_k(\sigma,\Sigma)\leq (1+\rho_k)P_k(\sigma,\Sigma_0)\leq (1+\rho_0)M,
  \end{displaymath}
  which concludes the proof. $\hfill \Box$
\end{pf}

\section{Approximation of Feasible Schedule by Bounded Periodic Schedules}
\label{sec:approximation}

In this section, we prove that any feasible schedule can be arbitrarily approximated by bounded periodic schedules. The main result is summarized by the following theorem:
\begin{thm}
  \label{thm:periodicapproximation}
  For any feasible schedule $\sigma$ and $\varepsilon,\varepsilon_1,\ldots,\varepsilon_m > 0$, there exists a bounded periodic $\sigma_p$, such that\footnote{For simplicity, we write $J(\sigma,\Sigma)$ as $J(\sigma)$ due to Theorem~\ref{thm:independence}. }
  \begin{equation}
    J(\sigma_p)\leq J(\sigma) + \varepsilon,
    \label{eq:cost}
  \end{equation}
  and
  \begin{equation}
    r_i(\sigma_p)\leq r_i(\sigma)+\varepsilon_i,\,i=1,\ldots,m.
    \label{eq:commrate}
  \end{equation}
\end{thm}

The following lemma is needed to prove Theorem~\ref{thm:periodicapproximation}.

\begin{lem}
  \label{lem:averageineq}
  Let $\sigma_p$ be a periodic schedule with period $T>0$, i.e.,
  \begin{displaymath}
    \mathcal I_{k+T}  = \mathcal I_k,\,\forall k.
  \end{displaymath}
  If the following inequality holds for some initial condition $\Sigma > 0$
  \begin{equation}
    \label{eq:condz}
    P_T(\sigma_p,\Sigma) \leq \Sigma,
  \end{equation}
  then the average cost function satisfies the following inequality,
  \begin{equation}
    \label{eq:averageineq}
    J(\sigma_p)\leq \frac{1}{T}\sum_{k=1}^T \trace(P_k(\sigma_p,\Sigma)).
  \end{equation}
  Moreover, the schedule $\sigma_p$ is bounded.
\end{lem}
\begin{proof}
  The main observation behind this proof is that, if the following inequality holds for any $k>0$
  \begin{equation}
    \label{eq:deceasing}
    P_{k+T}(\sigma_p,\Sigma)\leq P_k(\sigma_p,\Sigma).
  \end{equation}
  then \eqref{eq:averageineq} holds true. Interestingly enough, if we assume that \eqref{eq:deceasing} holds at a certain time $k = k_0$ then it will hold true at all successive time steps, in fact:
  \begin{displaymath}
    \begin{split}
      P_{k_0+1+T}(\sigma_p,\Sigma)& = g(P_{k_0+T}(\sigma_p,\Sigma),\mathcal I_{k_0+1+T}) \leq g(P_{k_0}(\sigma_p,\Sigma),\mathcal I_{k_0+1+T}) \\
      &= g(P_{k_0}(\sigma_p,\Sigma),\mathcal I_{k_0+1}) = P_{k_0+1}(\sigma_p,\Sigma),
    \end{split}
  \end{displaymath}
  where we use the fact that $\sigma_p$ is $T$-periodic. At this point it is enough to remark that condition \eqref{eq:condz} is the inequality \eqref{eq:deceasing} evaluated at time $k=0$:
  \begin{displaymath}
    P_T(\sigma_p,\Sigma)\leq P_0(\sigma_p,\Sigma)=\Sigma .
  \end{displaymath}
  to conclude that \eqref{eq:condz} implies \eqref{eq:deceasing} which finally implies \eqref{eq:averageineq}. 
  Moreover the latter arguments allow us to always find $M > 0$ such that
  \begin{displaymath}
    P_{k}(\sigma_p,\Sigma)\leq M,\,k=1,\ldots,T.
  \end{displaymath}
  By \eqref{eq:deceasing}, we know that such an $M$ is a uniform bound for all $P_k(\sigma_p,\Sigma)$. By Theorem~\ref{thm:independence}, the schedule $\sigma_p$ is bounded since $P_k(\sigma_p,\Sigma)$ is bounded for one initial condition $\Sigma$.
\end{proof}
Now we are ready to prove Theorem~\ref{thm:periodicapproximation}.
\begin{pf}[Theorem~\ref{thm:periodicapproximation}]
  By Theorem~\ref{thm:independence}, $J$ is independent of the initial condition $\Sigma$. As a result, let us choose $\Sigma = 2J(\sigma)I_n$. By the definition of $J$ and $r_i$, there exists a $K>0$, such that\footnote{We write $P_k(\sigma,\Sigma)$ as $P_k(\sigma)$ as $\Sigma$ is fixed throughout the whole section.}
  \begin{align}
    \frac{1}{N}\sum_{k=1}^N \trace(P_k(\sigma,\Sigma))& \leq J(\sigma)+\varepsilon,\label{eq:cost1} \\
    \frac{1}{N}\sum_{k=1}^N \mathbb I_{s_i\in \mathcal I_k}&\leq r_i(\sigma) + \varepsilon_i.\,i=1,\ldots,m\label{eq:commrate1}
  \end{align}
  for all $N \geq K$.

  Furthermore, there must exist infinitely many $k$s, such that
  \begin{displaymath}
    \trace(P_k(\sigma,\Sigma))\leq 2J(\sigma),
  \end{displaymath}
  which implies that
  \begin{equation}
    P_k(\sigma,\Sigma)\leq 2J(\sigma) I_n = \Sigma\label{eq:lessthantrace2}.
  \end{equation}
  As a result, we can choose $T$ such that \eqref{eq:cost1}, \eqref{eq:commrate1} and \eqref{eq:lessthantrace2} hold at the same time, i.e.,

  \begin{align}
    \frac{1}{T}\sum_{k=1}^T \trace(P_k(\sigma,\Sigma))& \leq J(\sigma)+\varepsilon,\label{eq:cost2} \\
    \frac{1}{T}\sum_{k=1}^T \mathbb I_{s_i\in \mathcal I_k}&\leq r_i(\sigma) + \varepsilon_i.\,i=1,\ldots,m\label{eq:commrate2}\\
    P_T(\sigma,\Sigma)&\leq   \Sigma\label{eq:lessthantrace3}.
  \end{align}


  Now define a periodic schedule $\sigma_p = (\mathcal J_1,\mathcal J_2,\ldots)$, such that
  \begin{displaymath}
    \mathcal J_{kT+j} = \mathcal I_{j},\,\forall k\in \mathbb N_0,\,j=0,\ldots,T-1.
  \end{displaymath}
  Please note that the new schedule $\sigma_p$ is the same as $\sigma$ for the first $T$ steps and then repeats itself afterwards. By definition, it is clear that $\sigma_p$ is periodic and \eqref{eq:commrate} holds. By Lemma~\ref{lem:averageineq}, since
  \begin{displaymath}
    P_T(\sigma_p,\Sigma) = P_T(\sigma,\Sigma) \leq \Sigma,
  \end{displaymath}
  we know that
  \begin{displaymath}
    J(\sigma_p)\leq \frac{1}{T}\sum_{k=1}^T \trace(P_k(\sigma_p,\Sigma)) =   \frac{1}{T}\sum_{k=1}^T \trace(P_k(\sigma,\Sigma)) \leq J(\sigma)+\varepsilon.
  \end{displaymath}
  In addition, the schedule $\sigma_p$ is bounded, which completes the proof. $\hfill \Box$
\end{pf}
\begin{rmk}
  It is worth noticing that our proof is constructive. Therefore, we could construct a periodic approximated schedule from any schedules.
\end{rmk}
\section{Conclusion}
\label{sec:conclusion}
In this paper, we consider the problem of infinite-horizon sensor scheduling problem for linear Gaussian systems. We assume that at each time step only a subset of all sensors can be selected to send their observations to the fusion center. We prove that the infinite-horizon cost function and the boundedness of a schedule are independent of the initial covariance matrix. We further prove that any feasible schedule can be arbitrarily approximated by a periodic schedule. The proof of the latter result is constructive and it thus provides useful insights into the design of computationally efficient periodic approximation with quantifiable sub-optimality.

\bibliographystyle{elsarticle-num}
\bibliography{SC_Letters09-reference}

\end{document}

