Sensor networks span a wide spectrum of applications, e.g., environment and habitat monitoring, health care, home and office automation, and traffic control~\cite{wireless_sensor_network}. In many of these applications, a centralized fusion center is implemented to collect and process the measurements for estimation purposes. Sensor nodes are typically battery powered, and therefore energy constrained. Furthermore, their radios are low-power and may be subject to interference and fading. As a result of the bandwidth and energy constraints, it is not advisable, and sometimes infeasible, for all the sensors to communicate with the fusion center within each sampling period. Thus, it is of significant interest to determine sensor scheduling policies able to tradeoff energy/bandwidth consumption and estimation quality.

Sensor network energy consumption minimization and lifetime maximization problems have been active areas of research in recent years. Sensor networks energy management is typically carried out via efficient MAC protocols~\cite{mac_sensor_network} or via efficient scheduling of sensor states~\cite{wake_up_scheme, sleep_schedule}. Xue and Ganz~\cite{lifetime_analysis} showed that the lifetime of sensor networks is influenced by transmission schemes, network density and transceiver parameters with different constraints on network mobility, position awareness and maximum transmission ranges. Chamam and Pierre~\cite{optimal_state_scheduling} proposed a sensor scheduling scheme capable of optimally putting sensors in active or inactive modes. Shi et al.~\cite{Ling_cdc07} considered sensor energy minimization as a mean to maximize the network lifetime while guaranteeing a desired quality of the estimation accuracy. The same authors further proposed a sensor tree scheduling algorithm~\cite{shi-febid08} which leads to longer network lifetimes.

Performance optimization for sensor networks under given energy constraints, which can be seen as the dual problem of network energy minimization, has also been studied by several researchers. Such constrained optimization problem has been studied for continuous-time linear systems by Miller and Runggaldier~\cite{stochasticcontrol} and Mehra~\cite{sensorprecision}. Krishnamurthy~\cite{hmmsensorselection} derived the optimal sensor scheduling for the estimation of a Hidden Markov Model based system. For discrete-time linear systems, approaches using dynamic programming~\cite{dynmaicprogrammingsensorselection}, greedy algorithms~\cite{oshmansensor}, convex optimization~\cite{Joshi, Mo2011a, Mo2011} and branch and bound~\cite{Vitus2012} have been proposed to find the optimal or suboptimal sensor scheduling over finite time horizons. In general, the sensor scheduling problem is a combinatorial optimization problem~\cite{Yoo2002} and thus the exact optimal solution over long time horizons is computationally intractable. However, the exact optimal schedule can be computed in some very particular cases. For instance, Shi and Zhang~\cite{Shi2012} prove that the optimal schedule is periodic for a system with two smart sensors.

Power control has also been studied~\cite{Xiao2006,Shi2011} to increase the energy efficiency of sensors. To this end, a sensor could use a lower power level to communicate information, which results in either a lower SNR, an increase in communication delay or a larger packet drop probability. Conceptually, for sensors with finitely many power levels, a virtual sensor could be assigned to each power level. Hence, the usage of power control can be seen as a special case of sensor scheduling.
%Another important contribution on the topic is the work of Joshi and Boyd~\cite{Joshi}, where a general single-step sensor selection problem was formulated and solved by means of convex relaxation techniques. Such a paper provides a very general framework that can handle various performance criteria and energy and topology constraints. Following this work, Mo et al.~\cite{Mo2011a} show that multi-step sensor selection problems can also be relaxed into convex optimization problems and thus solved efficiently.

In most of the works cited above, the optimal schedule can only be computed for linear systems over a finite-horizon, while only for specific systems an infinite-horizon policy can be derived. Moreover, for general systems, the solution is usually given as the result of an optimization problem and thus implicit. In this paper, we consider the problem of sensor scheduling for state estimation of linear LTI systems with  Gaussian noise over an infinite-horizon. In particular we focus on proving several fundamental properties that can be used as guideline for the analysis and design of infinite-horizon sensor schedules. In particular, we prove the following two propositions concerning scheduling policies:
\begin{enumerate}
  \item The average estimation error of a schedule is independent of the initial covariance of $x_0$.
  \item Any schedule that has a bounded average estimation error can be arbitrarily approximated (both in terms of average estimation error and communication rate) by bounded periodic schedules.
\end{enumerate}
These results have important practical consequences as bounded periodic schedules are easier to compute than general ones.

The rest of the paper is organized as follows: in Section~\ref{sec:problem}, we formulate the infinite-horizon sensor scheduling problem. In Section~\ref{sec:independence}, we prove that the average estimation covariance is independent of the initial conditions. We further provide a constructive proof that any feasible schedule can be arbitrarily approximated by bounded periodic schedules in Section~\ref{sec:approximation}. We then generalize our results to lossy networks in Section~\ref{sec:extension}. A numerical example is presented in Section~\ref{sec:simulation} to illustrate the performance of periodic schedules. Finally, Section~\ref{sec:conclusion} concludes the paper.
\subsection*{Notations}
We summarize the notations used in this paper in Table~\ref{tab:notations}.
\begin{table}[h]
\centering  \label{tab:notations} \centering
\begin{tabular} {|c|l|}
  \hline $S$ & Set of sensors \\
  \hline $\mathcal S$ & Collection of all eligible subsets of $S$\\
  \hline $\mathcal I_k$ & Subset of sensors selected at time $k$\\
  \hline $\sigma$ & An infinite sensor schedule in the form of $(\mathcal I_1,\mathcal I_2,\dots)$\\
  \hline $\Sigma$ & The covariance of the initial state $x_0$\\
  \hline $Q$ & The covariance of process noise\\
  \hline $R$ & The covariance of measurement noise\\
  \hline $J(\sigma,\Sigma)$ & The average trace of the error covariance matrix\\
  \hline $r_i(\sigma)$ & The average communication rate of sensor $i$\\
  \hline
\end{tabular}
\caption{Notations}
\end{table}


