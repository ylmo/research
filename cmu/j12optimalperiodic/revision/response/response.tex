\documentclass[10pt,oneside,a4paper]{article}

\author{Yilin Mo, Emanuele Garone, Bruno Sinopoli}

\title{Answers to AE's and reviewers' comments}

\usepackage{amsfonts}
\usepackage[T1]{fontenc}
\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their remarks.

The detailed responses to each reviewer are listed below.
%\section* {To Associate Editor}
%
%\emph{The paper has now been reviewed. The paper is well written and clear. It essentially contains two results, the first one dealing with some consequences of contraction properties of the Riccati update, and the second one dealing with approximation with periodic schedules.}
%
%\emph{The reviewers have some concerns regarding the motivations of the paper. The authors claim the motivation is to save energy via efficient scheduling policies. It should be made clearer in what ways the results are helpful with respect to this goal. The main advantages of the proposed approach with respect to existing literature should be clearer.}
%
%\emph{Another concern is the novelty of Section 3 with respect to ref [15]. If the results rely on ref [15] it should be clearly stated and one should take this opportunity to reduce the paper's length.}
%
\newpage

\section* {To Reviewer \# 1}
\emph{The present work is very well written and clear. However, in my opinion for publication the following points should be taken into account:}
\begin{enumerate}
  \item \emph{In the introduction it is stated that one of the main motivations for the present work is energy constraints. A key method in wireless communications to deal with energy constraints is the use of power control, which is a more general technology than scheduling. Consequently, the reasons for not using power control need to be carefully justified. Also, literature on power control for remote estimation of linear Gaussian processes needs to be discussed in the introduction.}

    We first thank Reviewer \#1 for the insightful review. We add several references on power control in the introduction. Conceptually, one could treat a sensor with $k$ different power levels as $k$ virtual sensors and use the set $\mathcal S$ to ensure that at most one virtual sensor is selected. Hence, a large amount of the power control problems can be put into our framework.

  \item \emph{With wireless communications, due to interference and fading (as mentioned in the introduction), random packet loss is unavoidable. However, the current paper assumes that communication is perfect. How can the work be extended to take (at least, i.i.d.) dropouts into account?}

    We have added a section on packet-drop networks.

  \item \emph{The paper assumes that raw and unquantized measurements are transmitted. This begs two questions: Firstly, how can quantization be incorporated into the framework? Secondly, why don't the sensors compute local state estimates, as in some of the recent works by Ling Shi and others?}

   We have added Remark~1 on quantization. Since there are quite a few different quantization schemes, it is difficult and beyond the scope of this paper to provide a unified framework to handle all possible quantization algorithms. However, for some quantization algorithms, the approximated error covariance matrix follows a modified Riccati equation, which shares the same ``contraction'' properties of the $g$ function. Hence, all of our results can be generalized to these cases.

   For local state estimates, most of the works focus on single sensor case, or two sensors which monitor two completely decoupled systems respectively. For single or two sensor cases, since the update equation is still a Riccati or a degenerated Riccati, the ``contraction'' properties hold and all the results proved in this paper should still hold. For multi-sensor case, there is no general formulation to the best of our knowledge. Hence, we feel that it is beyond the scope of this paper to provide a unified formulation of state estimation using local estimate.

  \item \emph{Some of the development in Section 3, mirrors that used in reference [15]. Most prominently, at least parts of Lemma 2  have already been proven in the Appendix of [15]. I strongly suggest to not state existing results as contributions of the current paper.}

    We have added the reference to our other paper. However we believe that, for the reader's convenience, it is better to include the proof in this paper, as the proof is only about half a page.

  \item \emph{The main result is intuitive, since it follows directly from $J$ being bounded. To strengthen the work (and make it more useful for design, as stated in the conclusions), the relationship between period length and performance loss $\varepsilon$ should be further investigated.}

    The goal of the paper is not to propose an unorthodox sensor scheduling scheme, but rather to provide a theoretical foundation to illustrate that the widely-used periodic schedules are indeed ``close'' to the optimal ones. We do agree that the main result is and should be intuitive. However, to the best of our knowledge, this is the first attempt in the literature to prove such an intuitive result, which is the main contribution of the paper.

    For what regard the relationship between the period length and the performance gap between the original and approximated schedules, we wish to highlight that it could be arbitrary. It is essentially the same problem of investigating how fast an arbitrary sequence converges, as shown in the proof of Theorem 5. Depending on the original schedule, the gap could be arbitrarily large or small.

    On the other hand, the performance of the optimal periodic schedule with a given period is a very interesting and challenging problem, as searching the optimal sensor schedule is in general NP-hard. Furthermore, as shown by the numerical example, the cost $J$ is not a monotonically decreasing function of the period. In some special cases, this problem is equivalent to Diophantine approximation problem (approximate a real number with rational numbers), which has a very deep theory itself. We are planning to investigate this problem in the future.
\end{enumerate}
\newpage


\section* {To Reviewer \# 2}
\emph{The present paper studies the problem of infinite-horizon sensor scheduling for estimation in linear Gaussian systems.  The infinite-horizon cost function and the boundedness of a schedule are proved to be independent of the initial covariance matrix. Also, any feasible schedule can be arbitrarily approximated by a periodic schedule.}

\emph{The paper is generally correct, but there are some contentious issues which the reviewer point out.}

\begin{enumerate}
  \item \emph{The motivation on the paper should be further emphasized. In particular, the main advantages of the results in this paper over some existing ones should be clearly demonstrated.}

We first thank Reviwer \#2 for his/her insightful comments. The goal of this paper is not to propose a new sensor scheduling strategy which is better than the existing ones. In fact, periodic schedules are widely used in literature. However, the validity of periodic schedules is only proved for special cases. Hence, the main contribution of this paper is to provide a rigorous theoretical foundation which proves that periodic schedules are ``close'' to optimal for general cases.

  \item \emph{For Theorems 1, sufficient conditions are given. Are these sufficient conditions easy to be checked? Is there some conservativeness brought by these sufficient conditions? A remark might be needed to address this issue.}

    The converse of Theorem 1 is the definition of feasible schedules. A schedule is feasible if for all initial conditions, the cost $J$ is bounded. Theorem 1 implies that as long as for one initial condition, the cost $J$ is bounded, then the schedule is feasible. Therefore, no conservativeness is introduced by Theorem 1. A remark has been added to clarify this issue.

    It is worth noticing that, without Theorem 1, one needs to check all possible initial conditions to prove the feasibility of a schedule. On the other hand, only one initial condition needs to be checked thanks to Theorem 1. Such a task could still be difficult for an arbitrary schedule (essentially it is equivalent to checking the boundedness of an arbitrary sequence). However, for a periodic schedule, Lemma 6 could be used to prove its feasibility.

  \item \emph{It is suggested to add some examples in order to illustrate the effectiveness of the proposed design procedures.}

    We have added a numerical example section.

  \item \emph{Please check the manuscript carefully and correct the typos. Also, it is suggested to add Notation at the end of the part of the Introduction.}

    We have checked the manuscript thoroughly. We also summarize the uncommon notations used in this paper in a table at the end of introduction section, as is requested by the reviewer.

  \item \emph{Some future research directions can be given in the conclusion.}

    Some possible future research directions are added to the conclusion section.
\end{enumerate}

\emph{Overall, this paper can be publishable if the above comments have been answered satisfactorily.}

\end{document}
