Dear Associate Editor,

We are resubmitting the following manuscript to S&C Letters:

"On Infinite-Horizon Sensor Scheduling"

We have revised the paper taking into account the comments from reviewer #1.

Regarding the contribution of our work, two related papers have been mentioned by reviewer #1. The first paper from Zhang et al. established similar results as the ones in our manuscript, and we are aware of this paper. Unfortunately one key result (Corollary 3 on page 5) in Zhang10 is flawed, and this renders the main result in theorem 4 at page 5(periodic approximation) incorrect. We pointed out the error to the authors back in January 2011. The authors acknowledged the error. At the time we submitted our manuscript in February of 2013, we had no knowledge that the error had been addressed yet.

A counter example illustrating the flaw in the aforementioned corollary is provided in the response letter.

Furthermore, our results are more general than the results presented in Zhang10. To be specific:
1) We consider that a subset of the sensors can communicate with the fusion center at any given time. In Zhang10, only one sensor can communication with the fusion center. This is a significant limitation.
2) Our main results hold true for an arbitrary feasible schedule. In Zhang10, the invariance and periodic approximation only hold when the schedule is bounded.
3) We consider the communication rate constraints and other variations of the problem such as communication over lossy networks, which are not presented in Zhang10.

As for the paper by Hovareshti et al., we feel that our results are more general, since Hovareshti et al. only consider the case of two smart sensors and with no communication rate constraints.

We trust that the revised manuscript and the response letter will address all the questions regarding the novelty of our work. Thank you for your service. We are looking forward to hearing from you.

Best Regards,
Yilin Mo
