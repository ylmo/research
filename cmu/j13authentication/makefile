#The base name of the main file (without .tex)
latexproject = j13authentication

#bibtex file
reference = authentication.bib

#all latex files
latexfiles = j13authentication.tex intro.tex

#external tikz names
tikznames = systemdiagram replaydiagram betaalpha3 betacost3 betatime3 betaalphasmallrelative

#The directory of all tikz source code
tikzdir = tikz

tikzpdfdir = tikzpdf

#tikz generated pdf and ps files
tikzfiles = $(tikznames:%=$(tikzdir)/%.tikz)

tikzpdffiles = $(tikznames:%=$(tikzpdfdir)/%.pdf)

.PHONY: pdf ps dvi clean veryclean tikzpdf 

#make target for vim latex-suite
pdf: $(latexproject).pdf $(latexproject).tex.latexmain

ps: $(latexproject).ps $(latexproject).tex.latexmain

dvi: $(latexproject).dvi $(latexproject).tex.latexmain

#regenerate all pdf and ps versions of tikz files
tikzpdf: 
	-mkdir $(tikzpdfdir)
	-rm -f $(tikzpdfdir)/*
	pdflatex -shell-escape -synctex=1 --interaction=nonstopmode $(latexproject).tex

#clean the intermediate files
clean:
	  -rm -f *.log *.aux *.bbl *.blg *.ilg *.toc *.lof *.lot *.idx *.ind *.synctex.gz

#clean the intermediate file and output files
veryclean: clean
	-rm -f *.dvi *.ps *.pdf

#generate the main file indicator for vim latex-suite
$(latexproject).tex.latexmain: 
	touch $(latexproject).tex.latexmain

#generate pdf
$(latexproject).pdf: $(latexproject).bbl $(latexfiles) $(tikzfiles) 	
	-mkdir $(tikzpdfdir)
	while (pdflatex -shell-escape -synctex=1 --interaction=nonstopmode $(latexproject).tex; \
	grep -q "Rerun to get cross" $(latexproject).log ) do true ; \
	done
	touch $(latexproject).bbl

#generate dvi
$(latexproject).dvi: $(latexproject).bbl $(latexfiles) $(tikzfiles) 	
	while (latex --interaction=nonstopmode --src-specials $(latexproject).tex;\
	grep -q "Rerun to get cross" $(latexproject).log ) do true ; \
	done

#generate ps
$(latexproject).ps: $(latexproject).dvi
	dvips $(latexproject)

$(latexproject).bbl: $(latexproject).aux $(reference)
	if [ -n '$(reference)' ]; then bibtex $(latexproject).aux; else touch $(latexproject).bbl; fi

$(latexproject).aux: $(latexfiles) 
	-mkdir $(tikzpdfdir)
	pdflatex -shell-escape $(latexproject).tex 

#build single pdf pic of tikz
.PHONY: $(tikznames) 

$(tikznames):%:$(tikzpdfdir)/%.pdf

$(tikzpdffiles):$(tikzpdfdir)/%.pdf:$(tikzdir)/%.tikz $(latexfiles)
	-mkdir $(tikzpdfdir)
	-rm -f $@
	pdflatex -shell-escape -synctex=1 --interaction=nonstopmode $(latexproject).tex
