\begin{tikzpicture}[>=stealth',
box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick},
point/.style={coordinate},
every node/.append style={font=\small}
]
\matrix[row sep = 10mm, column sep = 7mm]{
%first row
\node (p10) [point] {};&
\node (plantv) [box] {Virtual Plant};&
\node (sensorv) [box] {Virtual Sensor};&
\node (plus) [circle,semithick,draw=blue!60,fill=blue!20] {};&
\node (p11) [point] {};\\
%second row
& \node (externalcontrol) [] {$u_k^a$};& 
\node  {Virtual System}; && \\
%first row
\node (p1) [point] {};&
\node (plant) [box] {Plant};&
\node (sensor) [box] {Sensor};&
\node (p3) [point] {};&\\
%second row
\node (p5) [point] {};&
\node (delay) [box] {$z^{-1}$};&
\node (p6) [point] {};&& \\
%third row
\node (p7) [circle,semithick,draw=blue!50,fill=blue!20] {};&
\node (controller) [box] {LQG Controller};&
\node (estimator) [box] {Estimator};&&
\node (p8) [point] {};\\
%fourth row
\node (auth)[rectangle,semithick]{Authentication Signal};&
\node (detector) [box] {Failure Detector};&
\node (p9) [point] {};&&\\
};
\draw [semithick] (p7.east)--(p7.west);
\draw [semithick] (p7.north)--(p7.south);
\draw [semithick] (plus.east)--(plus.west);
\draw [semithick] (plus.north)--(plus.south);

\draw [semithick,->] (plant) -- (sensor);
%\draw [semithick,->](sensor) -- (p3) -- node[midway,right]{$y_k$} (p8) -- (estimator);
\draw [semithick,->](plus)--(p11) -- node[midway,right]{$y^v_k$} (p8) -- (estimator);
\draw [semithick,->](estimator)-- node[midway,above]{$\hat x_k^c$}(controller);
\draw [semithick,->] (auth) -- node[midway,right]{$\zeta_k^c$}(p7);
\draw [semithick,->](controller)--  (p7);
\draw [semithick,->](p7)--node[midway,left]{$u_k^c$}(p1)--(plant);
\draw [semithick,->](p5)--(delay)-- node[midway,above]{$u^c_{k-1}$}(p6)--(estimator);
\draw [semithick,->](estimator)--(p9) --node[midway,below]{$z^c_k$}(detector);
\draw [semithick,->](plantv)--(sensorv);
\draw [semithick,->](externalcontrol)--(plant);
\draw [semithick,->](externalcontrol)--(plantv);
\draw [semithick,->](sensorv)--node[very near end,above]{$-$} (plus);
\draw [semithick,->](sensor)--node[midway,above]{$y_k^c$}(p3)--node[very near end,right]{$+$} (plus);
\node [semithick,draw=blue!50,rectangle,rounded corners,dashed,fit=(p10) (plantv)(sensorv)(p11)(externalcontrol)] {};
\end{tikzpicture}
