%\documentclass[9pt,technote,letterpaper]{IEEEtran}

%\documentclass[10pt,letterpaper]{IEEEtran}
\documentclass{article}
\usepackage{amsmath}
\usepackage{amsfonts,amssymb}
\usepackage{tikz} %add back

\newcommand{\alert}[1]{\textcolor{red}{#1}}
\author{Yilin Mo, Sean Weerakkody, Bruno Sinopoli}

\title{Answers to the reviewer's comments}

\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their remarks. In particular we have increased our discussion of $\mathcal{A}$ and made explicit the assumption that $\mathcal{A}$ is stable. In addition, we have modified our list of references to include updated journal versions of articles where applicable.


\section{Response to Reviewer \#1}

\emph{The revised version of the paper is substantially improved over the
previous versions.   The focus on replay attacks has reduced some of
the ambiguities and concerns attached with the more general model
considered previously, and the explicit statements of the capabilities
and information available to the detector and attacker make the paper
easier to read.}

\emph{There are a couple of minor typos: "want" should be "wants" on second
last line of page 3, and "approach" is written twice in the second
paragraph of page 7.}

We first thank the reviewer for the helpful suggestions. The above typos have been fixed.

\emph{Regarding the new references, it would be preferable to cite the most
comprehensive journal versions of papers whenever possible. For
example, reference [7] by Fawzi et al. has a journal version "Secure
Estimation and Control for Cyber-Physical Systems Under Adversarial
Attacks" in IEEE TAC June 2014, "Attack Detection and Identification in
Cyber-Physical Systems" by Pasqualetti et al. in IEEE TAC Nov 2013 is
relevant for the design of detectors in CPS systems, and "Distributed
Function Calculation via Linear Iterative Strategies in the Presence of
Malicious Agents" by Sundaram et al. in IEEE TAC July 2011 pertains to
distributed computation/consensus and detection.}

As suggested by the reviewer we have updated the references to consider journal versions of papers. In particular, as suggested by the reviewer, we have now included the following journal articles

 \noindent ``Secure Estimation and Control for Cyber-Physical Systems Under Adversarial Attacks" in IEEE TAC June 2014," by Fawzi et. al.

\noindent ``Attack Detection and Identification in Cyber-Physical Systems" in IEEE TAC Nov 2013," by Pasqualetti et. al.

\noindent 
in place of the original conference papers, (references [6] and [7] in the previous version of this manuscript.) 

While the third paper suggested by the reviewer by Sundaram is not a sequel to a previous paper cited in the manuscript, we have now also cited this reference for completeness. Finally, we have included updated journal versions of the paper by Liu et. al., titled ``False data injection attacks against state estimation in electric powergrids" and Mo et. al. titled ``Detecting integrity attacks on SCADA systems" in place of their original conference versions, (references [4] and [13] in the previous manuscript). Additionally, we have updated discussions of the references as well as table 1 to account for additional contributions made by the more comprehensive journal versions of papers.



\section{Response to Reviewer \#4}
\emph{The situation regarding the stability of ${\cal A}$ is still somewhat
unclear.  In theorem 1 you give a result which applies to the case
where ${\cal A}$ is unstable.   In the subsequent comments you say that
the stability of ${\cal A}$ is not usually directly considered in the
control design.   Your further results and watermarking design assume
that ${\cal A}$ is stable.}

\emph{Is ${\cal A}$ stable generic or common?  Is there another interpretation
of it?	Are designs where ${\cal A}$ is unstable reasonable.}

We first thank the reviewer for the insightful comments. In general, the stability of matrices is not a generic property. In the one dimensional case it is easy to directly analyze the stability of $\cal{A}$. In particular, since $(A+BL)$ and $(A-KCA)$ are stable due to the implementation of an LQG controller $(A+BL)(I-KC)  = (A+BL)(A-KCA)A^{-1}$ is stable if (but not only if) the system matrix $A$ is unstable. Such analysis does not hold in higher dimensions since the product of stable matrices may not be stable.

The stability of $\cal{A}$ implies that the open loop cyber system, consisting of the controller plus estimator, is stable. In particular, in the absense of watermarking, observe that  the state estimate $\hat{x}_{k|k-1}$ satisfies
\begin{equation*}
\hat{x}_{k+1|k} = \mathcal{A}\hat{x}_{k|k-1} + (A+BL)Ky_k^v.
\end{equation*}

Thus, in the absense of proper feedback, for instance during a replay attack, the new state estimate is obtained by multiplying the previous estimate by $\mathcal{A}$ and adding a function of the replayed output. In the case where $\mathcal{A}$ is unstable, the state estimate is sensitive to initial conditions. Under normal operation, closed loop behavior relates $\hat{x}_{k+1|k}$ and $\hat{x}_{k|k-1}$ through the stable matrix $(A+BL)$. That is
\begin{equation*}
\hat{x}_{k+1|k} = (A+BL)\hat{x}_{k|k-1}  + (A+BL)KCe_{k|k-1} + v_k.
\end{equation*}

Designs where $\mathcal{A}$ is unstable can be reasonable since the closed loop system itself will remain stable if the system is guaranteed to remain in normal operation. However, in some instances, operators may wish to enforce the open loop stability of the controller, for instance in the design of autonomous vehicles.

Remark 8 as well as equations 25 and 26 has been added to provide increased discussion of $\mathcal{A}$ for the benefit of the reader.


\emph{I suggest stating that ${\cal A}$ is stable as an assumption up front,
making the appropriate statement about its generality, and then
proceeding with your watermarking design method.  In this case theorem
1 could be removed as it isn't applicable to the case considered and
would help shorten what is a long paper.}

As suggested by the reviewer, at the beginning of the Watermark Design and Detection section we make an explicit assumption (Assumption 1) that $\cal A$ is stable. However, we believe that it is important to keep theorem 1 in the article. We feel that making the assumption that $\cal A$ is stable without the proper context provided by theorem 1 would be confusing to the reader. Specifically, the results of theorem 1 motivate the need for a watermarking approach when $\cal A$ is stable and show why a watermarking approach is unnecessary otherwise. Moreover, the statement and proof of theorem 1 is relatively short and does not significantly lengthen the article.

\emph{I do not understand the statement (p 18) "it is computationally hard to
solve this problem since expected KL divergence is a convex function." 
The opposite is usually (but not always) the case.  Is this statement
correct?}

The statement given in the manuscript is correct since it refers to a maximization problem. In maximization problems, a concave objective function, as opposed to a convex objective function, is desired for computational simplicity. 

 In the formulation given by (37), we attempt to maximize a convex function which is generally computationally challenging unless the objective function is affine (in which case it is both convex and concave). To address this confusion we rewrite the statement as follows:

``However, it is computationally hard to solve this maximization problem since the expected KL divergence is not a concave function of $\Gamma(d)$."


 

\end{document}
