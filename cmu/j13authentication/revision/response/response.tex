%\documentclass[9pt,technote,letterpaper]{IEEEtran}

%\documentclass[10pt,letterpaper]{IEEEtran}
\documentclass[11pt,twocolumn]{article}
\usepackage{amsmath}
\usepackage{amsfonts,amssymb}
\usepackage{tikz} %add back

\newcommand{\alert}[1]{\textcolor{red}{#1}}
\author{Yilin Mo, Sean Sweerakkody, Bruno Sinopoli}

\title{Answers to the reviewer's comments}

\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. We have revised the paper taking into account all their remarks. In particular, the paper has been revised to make it more accessible to the general audience. Moreover, we add several remarks to clarify the confusions on our attack model and connect our model to the Stuxnet example. The detailed responses are listed as below:

%All reviewers indicate various major changes needed in order to have the paper accessible to the average CSM reader. For example, as stated by Reviewer 3, "This is a technical paper written for regular technical journal on control." Instead the results should be put in a broader context of the existing literature on security for cyber-physical systems. As pointed out by Reviewer 4, notations and concepts need to be appropriately introduced, and in some cases the presentation could be simplified by considering special cases. 

% The assumptions made on the attacker is not well described. As indicated by Reviewers 1, 3, 4, there are several issues that even make the problem hard to understand. Reviewer 3, for instance, indicate that some assumptions on the attacker are counter-intuitive and need to be explicitly addressed in the new version of the paper. 


\section{Response to Reviewer \#1}

\emph{This paper studies the problem of detecting attacks on control systems.  The system under consideration is a linear system with Gaussian noise.  The attacker is allowed to inject inputs and corrupt sensor measurements.  The proposed scheme is to inject a ``watermark'' input, with a signature that can be detected at the output under normal conditions.  The paper provides an optimization algorithm to find a way to generate the watermark signals.}

\emph{The paper is interesting and generally well written. The main point for clarification is the knowledge available to the attacker. For example, while the attacker does not know the exact sequence of watermarks applied to the system, does the attacker know the parameters that the detector is using?  If so, can the attacker simply solve the optimization problem themselves and thus generate their own watermark that would pass the detection test?}

  We first thank the reviewer for the insightful review. In this paper, we do not assume that the statistics of the watermark signal is hidden from the attacker. However, we assume that the attacker does not know the true value of the watermark signal (he/she only knows its mean and autocovariance function). As a result, the attacker cannot ``fool'' the detector, since the detector is checking whether the behavior of the system is compatible with the injected watermark signal. The detailed scenarios discussed in the paper can be found in Generalization subsection.

\emph{In equation 4, it is worth noticing that the standard detectability and stabilizability assumptions are taken to be in force.}

We add an assumption that the system is both detectable and stabilizable.

\emph{ After Theorem 1, the authors say that the attacker can perform the stealthy attack for a long time if the matrix is stable. However, does the impact of the attacker asymptotically decay to zero under this scenario?  What effect can the attacker have?}

When $\mathcal A$ is stable, the impact of the attacker on the residue and the detector decays to zero asymptotically, as is proved in Theorem~1. This implies that the detector has a very small probability (the false alarm rate) to detect the presence of the attack. However, since the attacker can inject arbitrary control inputs to the system, it can drive the state of the physical system along its controllable subspace. The detailed discussion can be found in Remark~4.

\section{Response to Reviewer \#2}
\emph{The paper addresses the security of a control system through the introduction of a watermark signal in the control signal. The main idea is that if the system is under attack and the output measurements are modified, the system can detect the attack by leveraging the effect of the watermark signal on the measurements. The topic of the paper is interesting, and relevant to real applications, as emphasized by the authors through the Stuxnet example. The paper is well written and the results are sound. I have some concerns on whether or not the solution can actually be implemented in real applications, since this could potentially involve solving a large number of semi-definite optimization problems.}

We first thank the reviewer for the insightful review. The number of Semi-Definite Programming (SDP) problem depends on the accuracy which the designer wants to achieve. Hence, if the system can tolerate more control performance loss, then fewer SDPs need to be solved. Moreover, all the computations of the SDP can be carried out offline.


\emph{The paper could also be improved through a slight reorganization to help with the reading experience. The authors include in the main body of the paper references to figures and equations in the appendix section which is rather disruptive.}

We moved the proof of Theorem 1 from the appendix to the main body of the paper to eliminate the reference from the main text to the appendix. The figures are included at the end of the paper as is requested by the Control System Magazine format, which should be moved back to the main text in the end.


\emph{Also, as mentioned by the authors in the appendix section, the paper reuses some proofs first introduced in previous papers of the authors. It would help if the paper would mention more clearly the difference between the current paper and the previous ones.}

We have changed the introduction to emphasize the difference between this paper and [14]. In general, the main difference is that we increased our design choice to include all stationary Gaussian processes, not just i.i.d. stationary Gaussian processes for the watermark signal. This makes the problem more challenging, as in this case we have to find the optimal autocorrelation function rather the just the optimal covariance matrix, resulting in a completely different optimization problem. Simulation results show that the effort is rewarded with significant improvements in detection performance, especially for small false alarm probabilities. Ultimately this can be seen as a non-trivial generalization of previous results.

\emph{Compared to [14], where the watermark signal is assume to be a zero-mean, Gaussian i.i.d. process, the watermark signal in the current paper is assumed to be stationary, zero-mean Gaussian process. It would be interesting to know if the relaxation on the watermark signal assumptions brought improvements on the detection performance.}

In the simulation section, we compare the performance of our watermark signal and the i.i.d. signal proposed in [4]. The results are illustrated in Fig 3,4, 5 and 6. When the false alarm rate $\alpha$ is small (less than 2\%), which is typical for practical systems, the improvement is over 100\%. 

\emph{Specific comments:}
\begin{enumerate}
  \item Introduction section:

    \begin{enumerate}
      \item \emph{The authors state that ``(\dots) some form of this input will be present in the measurement of the true output, regardless of its dynamics. (\dots)'' Whether or not the additional signal will be found in the output measurements depends on the spectral properties of the signal and of the plant. For example if the plant's transfer function is a low pass filter and the injected signal is of high frequency, then this signal will have a very low presence in the output measurements.  In fact even the authors' results suggest the importance of the frequency of the signal, since they actually compute an optimal frequency to help with the detection.}

	This is an oversight on our part. The sentence has been changed from ``regardless of its dynamics'' to ``depending on its dynamics'' .

      \item \emph{The authors state that ``(\dots) since the adversary may inject an attack that is statistically compatible with the process and measurement noise (\dots)''. This statement is a little bit unclear.  Probably the authors meant to say the effect of the attack would generate signals that are statistical compatible with the signals generated by the healthy system.}

	We have changed the sentence to ``the adversary may inject an attack that renders the compromised system statistically indistinguishable from the healthy system''.

      \item \emph{The authors mention that the paper investigates ``(\dots) the problem of designing the optimal watermark signal in the class of stationary Gaussian processes.'' Well in fact, even in this class of signals, the optimal watermark signal is not derived. The authors do derive a watermark signal as a solution of a relaxed optimization problem that tries to maximize the KL divergence between the distributions corresponding to two hypotheses.}

	We have changed the introduction to clarify that the ``optimality'' of the signal implies that it is the optimal solution of Equation (37). While we clearly explain that the proposed solution is not optimal with respect to the original problem we state that when we talk about of optimality we refer to the optimal solution of the relaxed problem, but we felt that repeating ``the optimal solution of the relaxed problem'' in every instance could be annoying  and that is the only reason why we put the disclaimer at the beginning. We feel that the flow of the paper is much improved. If the reviewer finds this unacceptable we would welcome suggestions for modification.
	
	
    \end{enumerate}
  \item Problem formulation section:
    \begin{enumerate}
      \item
	\emph{ Traditionally, $K$ is used to denote the control gain, and $L$ is used to denote the Kalman filter gain.}

	In many of our previous work we have used this notation, followed by C.T. Chen in his Linear Systems book.
	
      \item \emph{For the sake of clarity, the authors should define the hypothesis H0 and H1 before using them (13).  Related to ``(\dots) the absense (typo) of a superscript in $z_k$ will imply that the probability is conditioned on the scenario where the system is performing under normal operation (\dots)'', (14) could be written as $\alpha = P(g(z_k)\geq \eta | H0)$.}

	We have modified the paper to make it more clear. The false alarm rate is defined as $P(g(z_k)\geq \eta)$ since we use $z_k$ as the residue of the healthy system. For compromised system, $z_k^c$ is used instead.
    \end{enumerate}

  \item	Attack Mode section:
    \begin{enumerate}
      \item \emph{In Remark 5, the authors state that ``(\dots) the controller cannot perform closed loop control since the sensory information is not available. (\dots)'' I would say the ``true'' sensory information is not available. The controller does have access to some measurements, but because the measurements do not reflect the real state of the system, closed loop control is not executed correctly.}

	We have changed the statement. However, we still believe that the control during the attack phase should not be called as ``closed loop'' control since the estimator does not receive information from the plant, which is illustrated by Fig~2.

      \item \emph{ After equation (23) the authors refer to Figure 2, which is in the appendix section. For the ease of readability, the author should move the figure in section three. }

	The figures are included at the end of the paper as is required by the Control System Magazine format at submission. The CSM will reformat the paper and at that point the figures will be placed in the most appropriate places.
    \end{enumerate}
  \item Watermark Design and Detection section
    \begin{enumerate}
      \item \emph{``Hence'' is not correctly used when referring to the statistical properties of $z_k$. They follow immediately from the linear model of the system. Hence, the definition of the covariance matrix of $z_k$.}

	Fixed.
      \item \emph{The authors refer to equation (52) which is in fact defined in the appendix section. I understand that is not easy to balance what to keep in the main body of the paper and what to send to the appendix section. However, for the readability of the paper, it would help if the main body of the paper would be self-contained, as much as possible.  }

	We have moved the proof to the main text to avoid this problem. We have also removed some algebraic manipulation steps of the proof to make it more readable. 
      \item \emph{In Theorem 3, the authors introduce the optimal structure of the Neyman-Pearson detector. The function $g_{NP}$ depends on the watermark signal through $\mu_k^c$ and $\Sigma$.  In Theorem 4, the authors introduce the KL divergence for the distributions corresponding to the two hypotheses, on which the design of the watermark signal algorithm is based. I wonder if it is actually possible the evaluate $g_{NP}$ function for the class of watermark signal the authors derived. It may help the fact that $\Gamma(d)$ is periodic due to  $A_{omega}$.}

	The proof of Theorem~4 is not based on the periodicity of $\Gamma$. The KL divergence can be computed as a function of $\mu_k^c$ and $\Sigma$, where $\mu_k^c$ is a random variable. As a result, we take the expectation on $\mu_k^c$ to derive the expected KL divergence.

      \item \emph{I believe that the results shown in Theorem 8 are indeed interesting since they provide a structure for the watermark signal. Together with Theorem 7, they provide at least a chance for computing the statistical properties of the signal. Still, note that obtaining (42) does involve solving an infinite number of semi-definite programs. In practice one would need use a reasonable number of frequency samples and solve a semi-definite optimization problem to compute the direction, for each of these sample points.} 

	We thank the reviewer for pointing this out. We have changed Remark 7 to discuss the problem of finite sample points. 
    \end{enumerate}
  \item 	Numerical Example section
    \begin{enumerate}
      \item \emph{ I would suggest some prudence in the use of the term ``optimal''.  In fact, the watermark signal computed using (49) is suboptimal. This is not necessarily bad, since it actually provides a reason for its structure, that is, it maximizes the expected KL divergence in (36), using the intuition that ``Distributions that have a sizable KL divergence are, roughly speaking, easier to distinguish.''  Just that it is not optimal.}

	We have changed the introduction to clarify that the ``optimality'' of the signal implies that it is the optimal solution of Equation (37). While we clearly explain that the proposed solution is not optimal with respect to the original problem we state that when we talk about of optimality we refer to the optimal solution of the relaxed problem, but we felt that repeating ``the optimal solution of the relaxed problem'' in every instance could be annoying  and that is the only reason why we put the disclaimer at the beginning. We feel that the flow of the paper is much improved. If the reviewer finds this unacceptable we would welcome suggestions for modification.

	    \end{enumerate}

\end{enumerate}
\section{Response to Reviewer \#3}
\emph{This paper addresses the security of cyber-physical systems. The authors consider the problem of identifying stealth attacks, i.e., attacks that are designed to be undetected while altering the normal operation of the physical system. The proposed solution for this attack identification problem is the use of physical watermarking by which the actuation signal is infused with a signal unknown to the attacker and whose presence or absence can be detected through the system sensors.}

\emph{The paper is clear and well written but does not take into account the Control Systems Magazine readership by placing the proposed results in the broader context of the existing literature on security for cyber-physical systems. This is a technical paper written for regular technical journal on control. The only difference, if any, is the fact that the technical results are quite simple.}

We first thank the reviewer for the insightful review. We have changed the paper to make it more accessible (hopefully) for general audience. In particular:
\begin{enumerate}
  \item We added discussions on the validity of our attack model in the Attack Model section, Generalization subsection. A summary of the discussion can be found in Table~1.
  \item We added a sidebar on Stuxnet to better motivate the attack model. Another sidebar on Kullback-Liebler Divergence is also added to motivate the use of KL divergence.
  \item In general, we added several discussions to make the transition between different theoretical results smoother.  
\end{enumerate}

\emph{ Although the issue discussed in the previous paragraph can easily be resolved by revising the manuscript, the reviewer has a much more serious concern regarding the proposed attack model. The attacker is assumed to replace the measurement and input signal with signals of his choosing. However, the authors never discuss what is the knowledge available to the attacker. The typical assumption in security problems is that the attacker has full knowledge of the system and of the defense strategy except, in this case, of the watermark signal. If this is the case, then the attacker can simply replace the sensor measurements by the signal resulting from feeding a copy of the plant (whose model is known to the attacker) with the legitimate input signal (that is known to the attacker). Since the resulting signal carries the watermark, the attacker is free to manipulate the plant without ever being detected by the proposed watermark scheme. Hence, this leads to the following two questions:
\begin{enumerate}
  \item is it reasonable to assume that the plant model is known?
  \item is it reasonable to assume that the attacker can measure the input signals?
\end{enumerate}
}
The reviewer raises very interesting and relevant issues, which perhaps were not very clear in our original submission. We have made a significant effort to try to clarify. The assumptions in the attack model do not define a specific attack model, but rather provide conditions under which the proposed scheme works. Given the high number of attack scenarios it would be a gargantuan task to analyze each and every one of them against the three conditions. Thus we prefer to leave that task to the designer or to future work. Nonetheless we agree that it is important to shed some light on some of this scenarios. Toward that goal we have added a subsection that accomplishes the goal and added a summarizing table. 

The key assumption is number 3, which states that the watermark should be kept secret. This assumption can be violated in a partial way, i.e., the attacker has partial knowledge of the watermark. In this case the proposed scheme will still be valid, although it's effectiveness may be diminished. In the case where the attacker is able to perfectly reconstruct the watermark then the proposed countermeasure will not work. Strong attacks such as the ones where all inputs, all outputs and the full model are known are impossible to defend against, as the whole system is compromised with no root of trust, as is illustrated by the following paper:


Smith, Roy S. "A decoupled feedback structure for covertly appropriating networked control systems." Network 6 (2011): 6.

\emph{Regarding question 1), not assuming the plant model to be known results in a very weak security guarantee since the plant model can always be learned over time by observing input-output signals. Even if input-output signals could not be measured, the resulting guarantee would be of the type ``security by obscurity'' which is considered weak and scientifically uninteresting.}

This attack model is first motivated by the malware Stuxnet where a replay attack was waged. When simply replaying previous outputs, the adversary does not require any knowledge of the model. However, because the synthetic outputs $y_k^v$ given by the attacker are real outputs of the system in steady state, they are statistically identical to $y_k$ under normal operation. Thus, even without knowledge of the system model, strong attacks can be waged. 

On the other hand, learning the system model from the input-output relation is a non-trivial system identification problem, rendered more difficult by the necessity of producing an accurate copy of the model. This is made even more difficult by the fact that the input of the system is not designed for the system identification purposes. Notice that the attacker should not inject anything before it learns the system, which implies the control signal is the optimal LQG control signal (at steady state) plus the watermark signal. While in theory this is a possibility, it may be inaccurate or take too long to converge. In practice, the attack may be uncovered by regular system testing before the system identification process converges.

Furthermore, even if the plant model is given to the attacker, we argue that if the system operator can guarantee the secrecy of a subset of actuators or sensors, the attacker would still not be able to create statistically correct measurements which carry the watermark. As a result, detection performance will degrade gracefully, as long as partial secrecy of the inputs and outputs can be guaranteed. A more detailed discussion can be found in Generalization subsection.

\emph{Regarding question 2), measuring the control signal is much simpler than replacing it with a different signal. Since the authors assume the attacker can alter the input signal they are implicitly assuming that this signal can be measured.}

In this paper, we only assume that the attacker can inject an external control input. As a result, it is entirely possible that the attacker employs his/her own actuator other than the actuators used by the system. Another possibility is that the attacker only compromised a subset of the actuators of the whole system. For example, to change the temperature distribution in the building, the attacker could deploy heaters of its own or even commit arson. Hence, it may not be able to read all the input signals. As a result, even though the attacker has a perfect model of the plant, he/she may not be able to reproduce the measurements which carry the watermark signal.


\section{Response to Reviewer \#4}
\emph{The paper is not easy to read for a general control systems audience. Several concepts require a bit more detail, and quite a bit of the mathematics includes formalisms which are unnecessary for describing the basic idea.}

We first thank the reviewer for the insightful review. We have added several remarks and simplified some proofs to make the paper more accessible to the general audience.

\emph{ The end result appears to be that a sinusoidal signal, of a particular frequency and direction, but with a random initial condition, is the best watermark signal. Given this, many of the formulas in the paper could probably be simplified. For example (29) and possible (34) contain infinite summations which may have a simpler expression when the argument is stable and constant.}

While the solution is simple, the formalism is necessary to derive the solution. We feel that, without it, it would be very hard to get to the solution in deductive manner.

\emph{The notation used in (13) is not common and should be defined.}

The ``$\lessgtr$'' notation has been removed from the paper.

\emph{Similarly the notation used for the Kullback-Leibler divergence in (36) is not obvious.  For the general reader a definition of KL divergence would help or is (36) intended as a definition?}

A sidebar on Kullback-Leibler divergence has been added to clarify this issue.

\emph{Is the formalism in the "Optimal Watermark Signal" section necessary?  Definition 1 and theorems 5 and 6 seem like standard formalism underlying all standard spectral analysis.  If so they could be replaced by a statement like, "under commonly used mild assumptions".}

We employ the notion of positive Hermitian measure to avoid using $\delta$ functions, since it is unclear if the value of the function is a positive Hermitian matrix when $\delta$ functions are involved. For example, consider the following positive Hermitian measure on $(-0.5,0.5]$:
\begin{displaymath}
  \nu([a,b]) = \left[ \begin{array}{cc}
	  1 & 1 \\
	  1 & 1 \\
	\end{array} \right](b-a) + \left[ \begin{array}{cc}
	  1 & 0 \\
	  0 & 1 \\
	\end{array} \right] \mathbb I_{a<0<b},
\end{displaymath}
the derivative of which is
\begin{displaymath}
  f(\omega) = \left[ \begin{array}{cc}
    1+\delta_0(\omega) & 1 \\
    1 & 1+\delta_0(\omega) \\
  \end{array} \right].
\end{displaymath}
To the best of our knowledge, there is no commonly accepted definition on whether $f(\omega)$ is positive Hermitian or not when $\omega = 0$.

Furthermore, since we are optimizing $\Gamma(d)$ over a very large class of autocovariance functions, it is unclear whether ``commonly used mild assumptions'' will be automatically satisfied. In fact, our solution is given by a point-mass measure, which corresponds to delta functions if we use Fourier transform directly. 

\emph{Attenuating the sinusoid (as in (50)) seems like it would be easily detected by an attacker watching the control command. Would it be better to randomly restart the oscillator from a new random initial state?}

In Equation (50), we propose another watermark signal design to address this issue, by letting the autocovariance function decay over time. A more detailed discussion on this design versus the optimal design can also be found in the Optimal Watermark Signal subsection.

\emph{More importantly I think that the discussion and remarks should make clear the assumptions on the attacker. These assumptions are quite strong and only apply to attackers that are ``weaker'' than those normally considered. For example the work by Sandberg et al (2013, Automatica) describes a range of attacks most of which assume that the attacker can both measure and modify the communicated signals. You significantly weaken this to have the attacker only blindly replace signals in the measurements. This assumption needs to be made more clear.}

It is unclear which paper is the reviewer referring to. However, we believe the following work of Teixeira et al. is relevant:

Andre Teixeira, Iman Shames, Henrik Sandberg, Karl H. Johansson, ``\emph{A Secure Control Framework for Resource-Limited Adversaries}'', arxiv:1212.0226

Teixeira et al. classify different attacks based on
\begin{enumerate}
  \item the system knowledge: the knowledge of the system parameters, such as $A,B,C$ matrices;
  \item disclosure resources: the knowledge of the real time signals, such as $u_k^c$, $y_k^c$; 
  \item disruption resources: the ability to change the real time signals. In our case, the attack can inject $u_k^a$ and replace $y_k^c$ to $y_k^v$.
\end{enumerate}

As is illustrated in Fig~1 of Teixeira et al., our attack model, which can be seen as a generalization of the replay attack model, is neither ``weaker'' nor ``stronger'' than the attack models commonly discussed in the literature, such as zero-dynamics attacks, bias injection attacks and DoS attacks, except for the covet attack. However, it has been proved that the covert attack is undetectable in the following paper:

Smith, Roy S. "A decoupled feedback structure for covertly appropriating networked control systems." Network 6 (2011): 6.

It is worth mentioning that it is beyond the scope of this paper to address all possible security issues in control systems. As a result, we mainly focus on the attack model motivated by the malware Stuxnet, where a replay attack is waged. However, the watermarking scheme may prove to be useful in other cases, as is discussed in Generalization subsection and summarized in Table~1. 

\emph{For example, the statement at the end of the first paragraph is not true.  The attacker need not generate outputs unique to the given inputs at a chosen time. If the attacker simply adds to the sensor signal, your nonce is still present and the attack may not be detected.}

\emph{Similarly remark 5 does not hold if the attacker can measure and add to the sensor signal.  Under this scenario the controller still has control of the plant - they are simply dealing with a fictitious output disturbance and it is the controller's action which moves the plant output to the attacker's objective.}

A discussion on the ``additive'' attack model is presented in the Generalization subsection. In general, the ``additive'' attack model requires the external measurement signal to exactly cancel the effect of the malicious control signal. However, several additional assumptions are needed to make the ``additive'' attack feasible:
\begin{enumerate}
  \item The adversary needs to have perfect knowledge of the system parameters to perform the cancellation exactly. It is difficult since the system parameters may change or the system may become non-linear when the attacker injects malicious control inputs to shift the operating point of the system. 
  \item The $A$ matrix needs to be stable. Otherwise, the cancellation error grows exponentially.
\end{enumerate}

As a result, we feel that even though the watermarking scheme may not be helpful to defend against an ``advanced'' attacker, it is still useful for many cases, as is illustrated in Table~1.

\emph{You need to make clear the scenarios under which your method works, and those under which it does not work.}

A discussion on the validity of our attack model under different scenarios is added in the Generalization subsection and the summary of it can be found in Table~1.
\end{document}
