falsealarmtarget = 0.1;
EtaMax = 1.55;
EtaMin = 1.45;
ETA_threshold = (EtaMax+EtaMin)/2;
numcalcs = 500;
entry = 15;
falsealarmavg = 0;

for falsealarmtarget = .065:.005:.1
%     if tablealphaIID(entry,1) > 0
%         continue
%     end
    
    while(abs(falsealarmtarget - falsealarmavg) > .0006)
        falsealarmavg = 0;
        detectionavg = 0;
        display(ETA_threshold);
        for indcalcalpha = 1:numcalcs
            runsystemIID3_seancheck;
            falsealarmavg = falsealarmavg+mean(falsealarm(11:100));
            detectionavg = detectionavg + mean(detection(11:100));
        end
        falsealarmavg = falsealarmavg/numcalcs;
        detectionavg = detectionavg/numcalcs;
        display(falsealarmavg);

        if(abs(falsealarmtarget - falsealarmavg) <= .0006)
            break;
%         elseif(mod(falsealarmavg,.02) <= .002)
%             display('hello');
%             falsealarmavg = falsealarmavg*numcalcs;
%             detectionavg = detectionavg*numcalcs;
%             numreps = 3;
%             for indcalcalpha = 1:numreps*numcalcs
%                 runsystemIID3_seancheck;
%                 falsealarmavg = falsealarmavg+mean(falsealarm(11:100));
%                 detectionavg = detectionavg + mean(detection(11:100));
%             end
%             falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
%             display(falsealarmavg)
%             detectionavg = detectionavg/(numreps+1)/numcalcs;
%             entries = round(falsealarmavg/.02);
%             tablealphaIID(entries,1) = .02*entries;
%             tablealphaIID(entries,2) = falsealarmavg;
%             tablealphaIID(entries,3) = detectionavg;
%             tablealphaIID(entries,4) = ETA_threshold;
        elseif(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        abs(falsealarmtarget - falsealarmavg) 
    end
    falsealarmavg = falsealarmavg*numcalcs;
    detectionavg = detectionavg*numcalcs;
    numreps = 6;
    for indcalcalpha = 1:numreps*numcalcs
        runsystemIID3_seancheck;
        falsealarmavg = falsealarmavg+mean(falsealarm(11:100));
        detectionavg = detectionavg + mean(detection(11:100));
    end
    falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
    display(falsealarmavg)
    detectionavg = detectionavg/(numreps+1)/numcalcs;
    tablealphaIIDsmall(entry,1) = falsealarmtarget;
    tablealphaIIDsmall(entry,2) = falsealarmavg;
    tablealphaIIDsmall(entry,3) = detectionavg;
    tablealphaIIDsmall(entry,4) = ETA_threshold;
        if(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        
    if falsealarmtarget <= 0.1
        EtaMin = EtaMax - .2;
    elseif falsealarmtarget <= 0.2
        EtaMin = EtaMax - 0.5;
    elseif falsealarmtarget <= 0.4
        EtaMin = EtaMax - 0.3;
    else
        EtaMin = EtaMax - 0.8;
    end
    ETA_threshold = (EtaMax+EtaMin)/2;
    entry = entry + 1;
end
