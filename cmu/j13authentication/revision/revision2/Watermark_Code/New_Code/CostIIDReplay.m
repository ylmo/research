% Plot Beta as a function of the Cost J for fixed alpha, using optimal
% watermark

% Instructions
% 1) Comment out delta in simulation.m code before running
% 2) Choose delta's which you wish to use for the for-loop
% 3) Choose alphas for the probability of false alarm.
% 4) Select Number of Runs we wish to consider in variable length
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystem2 if you wish to have each run be longer.
% 7) detectovercost vs costs provies Beta as a function of Del J
 
falsealarmtarget= .1; % originally .10 for CSM, changed on 11/22 for Yilin
entry = 4;
  EtaMax = 3;
    EtaMin = 0;
for delta = 2

    simulation; % run system to obtain parameters for chosen delta
    display(delta);
    length = 15000; % number of runs to consider
    Jestimated = zeros(length,1); % Store additional cost for each run (should be relatively constant design variable)
    falsetot = zeros(length,1); % Store false alarm average for each run, should be constant, it is a design variable.
    detecttot = zeros(length,1); % Store detection average for each each run
  
    ETA_threshold = (EtaMax+EtaMin)/2;
    falsealarmavg = 0;
    numcalcs = 16;
    
    while(abs(falsealarmtarget - falsealarmavg) > .0008)
        falsealarmavg = 0;
        detectionavg = 0;
        Jestimate = 0;
        display(ETA_threshold);
        for indcalcalpha = 1:numcalcs
            runsystemIID3_seancheck;
            falsealarmavg = falsealarmavg+mean(falsealarm(11:10000));
            detectionavg = detectionavg + mean(detection(11:10000));
            Jestimate = Jestimate + Jest - Jstar;
        end
        falsealarmavg = falsealarmavg/numcalcs;
        detectionavg = detectionavg/numcalcs;
        display(falsealarmavg);

        if(abs(falsealarmtarget - falsealarmavg) <= .0008)
            break;
        elseif(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        abs(falsealarmtarget - falsealarmavg) 
    end
    falsealarmavg = falsealarmavg*numcalcs;
    detectionavg = detectionavg*numcalcs;
    numreps = 2;
    for indcalcalpha = 1:numreps*numcalcs
        runsystemIID3_seancheck;
        falsealarmavg = falsealarmavg+mean(falsealarm(11:10000));
        detectionavg = detectionavg + mean(detection(11:10000));
        Jestimate = Jestimate + Jest - Jstar;
    end
    falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
    display(falsealarmavg)
    detectionavg = detectionavg/(numreps+1)/numcalcs;
    Javg = Jestimate/(numreps+1)/numcalcs;
    checkIIDcost2(entry,1) = falsealarmtarget;
    checkIIDcost2(entry,2) = falsealarmavg;
    checkIIDcost2(entry,3) = detectionavg;
    checkIIDcost2(entry,4) = ETA_threshold;
    checkIIDcost2(entry,5) = Javg;
%     tablealphaIIDcost2(entry,1) = falsealarmtarget;
%     tablealphaIIDcost2(entry,2) = falsealarmavg;
%     tablealphaIIDcost2(entry,3) = detectionavg;
%     tablealphaIIDcost2(entry,4) = ETA_threshold;
%     tablealphaIIDcost2(entry,5) = Javg;
    entry = entry + 1;
    EtaMax = EtaMax + 0.4;
    ETA_threshold = (EtaMax + EtaMin)/2;
end
    


