% Run system in the case that covariance of watermark is iid optimal. Eta
% is varied at each step to obtain desired false alarm rate.

%Instructions
%1) Check tottime to see how long each run is 
%2) Download MAT file ETAOUT as desired.
%3) Ensure alphas is chosen as dsired
%4) Run simulation.m first for chosen delta J (delta) to generate
%parameters

ETA_threshold = 5.408;

% first simulate virtual system.
tottime = 1001;
x = zeros(n,tottime);
y = zeros(m,tottime);
xhat = x; xpred = x;

xmean = 0;


% time step 0, virtual system initialize parameters
x(:,1) = mvnrnd(xmean*ones(n,1), P);
xpred(:,1) = xmean*ones(n,1);
v = mvnrnd(zeros(m,1),R);
y(:,1) = C*x(:,1)+v';
xhat(:,1) = xpred(:,1) + K*(y(:,1)-C*xpred(:,1));
falsealarm = zeros(tottime-1,1);
detection = falsealarm;

uu = zeros(p,tottime-1); % store input for all time
timedet = -1;

% run the virtual system
for nn = 2:tottime
       
    u = L*xhat(:,nn-1);
    uu(:,nn-1) = u;
    v = mvnrnd(zeros(m,1),R);
    w = mvnrnd(zeros(n,1),Q);
    
    x(:,nn) = A*x(:,nn-1) + B*u + w';
    if nn <= (tottime+1)/2
        y(:,nn) = C*x(:,nn) + v';
    else
        y(:,nn) = y(:,nn-((tottime-1)/2));   
    end
    xpred(:,nn) = A*xhat(:,nn-1)+B*u;
    xhat(:,nn) = xpred(:,nn) +  K*(y(:,nn)-C*xpred(:,nn));
end

z = y - C*xpred; % Stores residues for each k

Pcest = zeros(m,m);
Pcest2 = zeros(m,m);
Jest = 0;
Jtheory = 0;

% Detector Virtual System, run to characterize probability of false alarms
for nn = 2:tottime
    if(z(:,nn)'*cP^-1*z(:,nn) < ETA_threshold)
        detection(nn-1) = 0;
        
    else
        detection(nn-1) = 1;
        if(nn > (tottime+1)/2) && timedet == -1
             timedet = nn-((tottime-1)/2)-2;
        end
    end
end










