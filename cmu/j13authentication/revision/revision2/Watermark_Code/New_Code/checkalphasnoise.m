falsealarmtarget = 0.1;
EtaMax = 2.3;
EtaMin = 2.1;
ETA_threshold = (EtaMax+EtaMin)/2;
numcalcs = 100;
entry = 12;
falsealarmavg = 0;

for falsealarmtarget = .05:.005:.1
%    if tablealphanoise(entry,1) == 0
   
    while(abs(falsealarmtarget - falsealarmavg) > .0006)
        falsealarmavg = 0;
        detectionavg = 0;
        display(ETA_threshold);
        for indcalcalpha = 1:numcalcs
            runsystem_noise_2SeanCheck;
            falsealarmavg = falsealarmavg+mean(falsealarm(11:1000));
            detectionavg = detectionavg + mean(detection(11:1000));
        end
        falsealarmavg = falsealarmavg/numcalcs;
        detectionavg = detectionavg/numcalcs;
        display(falsealarmavg);

        if(abs(falsealarmtarget - falsealarmavg) <= .0006)
            break;
%         elseif(mod(falsealarmavg,.02) <= .001)
%             display('hello');
%             falsealarmavg = falsealarmavg*numcalcs;
%             detectionavg = detectionavg*numcalcs;
%             numreps = 3;
%             for indcalcalpha = 1:numreps*numcalcs
%                 runsystem_noise_2SeanCheck;
%                 falsealarmavg = falsealarmavg+mean(falsealarm(11:1000));
%                 detectionavg = detectionavg + mean(detection(11:1000));
%             end
%             falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
%             display(falsealarmavg)
%             detectionavg = detectionavg/(numreps+1)/numcalcs;
%             entries = round(falsealarmavg/.02);
%             tablealphanoise(entries,1) = .02*entries;
%             tablealphanoise(entries,2) = falsealarmavg;
%             tablealphanoise(entries,3) = detectionavg;
%             tablealphanoise(entries,4) = ETA_threshold;
        elseif(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        abs(falsealarmtarget - falsealarmavg) 
    end
    falsealarmavg = falsealarmavg*numcalcs;
    detectionavg = detectionavg*numcalcs;
    numreps = 4;
    for indcalcalpha = 1:numreps*numcalcs
        runsystem_noise_2SeanCheck;
        falsealarmavg = falsealarmavg+mean(falsealarm(11:1000));
        detectionavg = detectionavg + mean(detection(11:1000));
    end
    falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
    display(falsealarmavg)
    detectionavg = detectionavg/(numreps+1)/numcalcs;
    tablealphanoise_small(entry,1) = falsealarmtarget;
    tablealphanoise_small(entry,2) = falsealarmavg;
    tablealphanoise_small(entry,3) = detectionavg;
    tablealphanoise_small(entry,4) = ETA_threshold;
        if(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        
    if falsealarmtarget <= 0.1
        EtaMin = EtaMax - 0.2;
    elseif falsealarmtarget <= 0.2
        EtaMin = EtaMax - 0.2;
    elseif falsealarmtarget <= 0.4
        EtaMin = EtaMax - 0.1;
    else
        EtaMin = EtaMax - 1;
    end
    ETA_threshold = (EtaMax+EtaMin)/2;
    entry = entry + 1;
end
     

