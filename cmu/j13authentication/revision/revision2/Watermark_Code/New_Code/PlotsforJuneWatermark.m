close all;
figure
plot([0 ;tablealphanoise(1:49,2);1],[0 ;tablealphanoise(1:49,3);1],'g-o');
hold on;
plot([0 ;tablealphaIID(1:49,2);1],[0 ;tablealphaIID(1:49,3);1],'b-^');
xlabel('\alpha');
ylabel('Asymptotic \beta_k^c')
legend('Suboptimal Watermark \rho = 0.9', 'IID Watermark')

figure;
plot([0 ;tablealphanoise_small(1:22,2)],[0 ; tablealphanoise_small(1:22,3)],'g-o');
hold on;
plot([0 ;tablealphaIIDsmall(1:22,2)],[0 ;tablealphaIIDsmall(1:22,3)],'b-^');

%pout = polyfit([0; tablealphanoise_small(1:22,2)], [0; tablealphanoise_small(1:22,3)],20);
figure;
noisespline = spline([tablealphanoise_small(1:22,2)],[ tablealphanoise_small(1:22,3)],[tablealphanoise_small(1:22,1)]);
iidspline =  spline([tablealphaIIDsmall(1:22,2)],[tablealphaIIDsmall(1:22,3)],[tablealphaIIDsmall(1:22,1)]);
plot(tablealphanoise_small(1:22,1),100*(noisespline-iidspline)./iidspline,'g-o')
legend('Suboptimal Watermark \rho = 0.9');
xlabel('\alpha');
ylabel('Improvement of the Asymptotic \beta_k^c')


figure; plot([0; tablealphanoisecost(:,5)], [.02; tablealphanoisecost(:,3)], 'g-o')
hold on; plot([0; tablealphaIIDcost2(:,5)], [.02; tablealphaIIDcost2(:,3)], 'b-^')
plot([0;tablealphanoisecost_rho6(:,5)],[.02; tablealphanoisecost_rho6(:,3)],'r-*')
xlabel('\Delta J');
ylabel('Asymptotic \beta_k^c');
legend('Suboptimal Watermark \rho = 0.9', 'IID Watermark','Suboptimal Watermark \rho = 0.6')


figure; plot(-50:1:49,detectvector3,'g'); hold on;
plot(-50:1:49,detectvector2,'b'); 
plot(-50:1:49,detectvector1,'r');
xlabel('k')
ylabel('\beta_k^c');
legend('Suboptimal Watermark \rho = 0.9', 'IID Watermark','No Watermark')

figure;
plot(5:5:100,timenoise,'g-o')
hold on;
plot(5:5:100,timeIID,'b-o')
xlabel('\Delta J');
ylabel('Expected Time to Detectection');
legend('Suboptimal Watermark \rho = 0.9', 'IID Watermark','No Watermark');

