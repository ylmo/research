% Run system in the case that covariance of watermark is iid optimal. Eta
% is varied at each step to obtain desired false alarm rate.

%Instructions
%1) Check tottime to see how long each run is 
%2) Download MAT file ETAOUT as desired.
%3) Ensure alphas is chosen as dsired
%4) Run simulation.m first for chosen delta J (delta) to generate
%parameters



% first simulate virtual system.
tottime = 301;
x = zeros(n,tottime);
y = zeros(m,tottime);
xhat = x; xpred = x;

xmean = 0;


% time step 0, virtual system initialize parameters
x(:,1) = mvnrnd(xmean*ones(n,1), P);
xpred(:,1) = xmean*ones(n,1);
v = mvnrnd(zeros(m,1),R);
y(:,1) = C*x(:,1)+v';
xhat(:,1) = xpred(:,1) + K*(y(:,1)-C*xpred(:,1));
zeta = zeros(p,tottime-1);
muv = zeros(n,tottime-1); % stores mu_k^v, the advantage over the attacker as a function of time
falsealarm = zeros(tottime-1,1);
detection = falsealarm;
uu = zeros(p,tottime-1); % store input for all time
ind_nn = 0;
timedet =51;
% run the virtual system
for nn = 2:tottime%+10000
    
    zeta(:,nn-1) = mvnrnd(zeros(p,1),Gamma0);
    
    if(nn == 2)
        muv(:,1) = B*zeta(:,1);
    else
        muv(:,nn-1) = cA*muv(:,nn-2) + B*zeta(:,nn-1);
    end
    
    u = L*xhat(:,nn-1) + zeta(:,nn-1);
    uu(:,nn-1) = u;
    v = mvnrnd(zeros(m,1),R);
    w = mvnrnd(zeros(n,1),Q);
    
    x(:,nn) = A*x(:,nn-1) + B*u + w';
    xpred(:,nn) = A*xhat(:,nn-1)+B*u;
    if nn <= (tottime+1)/2
        y(:,nn) = C*x(:,nn) + v';
        z = y(:,nn)-C*xpred(:,nn);
        muvout = -C*muv(:,nn-1);
        if((z'*cP^-1*z - (z-muvout)'*(cP+SigmaTheoryIID)^-1*(z-muvout)) > ETA_threshold)
            detection(nn) = 1;
        end

    else
        y(:,nn) = y(:,nn-((tottime-1)/2));
        z = y(:,nn)-C*xpred(:,nn);
        muvout = -C*muv(:,nn-1);
        if((z'*cP^-1*z - (z-muvout)'*(cP+SigmaTheoryIID)^-1*(z-muvout))> ETA_threshold)
           if(timedet == 51)
                timedet = nn-((tottime-1)/2)-2;
           end
              detection(nn) = 1;
              break;
        end
        ind_nn = ind_nn+1;
    end
   
    xhat(:,nn) = xpred(:,nn) +  K*(y(:,nn)-C*xpred(:,nn));
end












