falsealarmtarget = 0.1;
EtaMax = 6;
EtaMin = 3;
ETA_threshold = (EtaMax+EtaMin)/2;
numcalcs = 5000;
entry = 11;
falsealarmavg = 0;

for falsealarmtarget = 0.005
    while(abs(falsealarmtarget - falsealarmavg) > .001)
        falsealarmavg = 0;
        detectionavg = 0;
        display(ETA_threshold);
        for indcalcalpha = 1:numcalcs
            runsystem3_seancheck;
            falsealarmavg = falsealarmavg+mean(falsealarm(11:20));
            detectionavg = detectionavg + mean(detection(11:20));
        end
        falsealarmavg = falsealarmavg/numcalcs;
        detectionavg = detectionavg/numcalcs;
        display(falsealarmavg);

        if(abs(falsealarmtarget - falsealarmavg) <= .001)
            break;
        end
        if(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        abs(falsealarmtarget - falsealarmavg) 
    end
    falsealarmavg = falsealarmavg*numcalcs;
    detectionavg = detectionavg*numcalcs;
    numreps = 3;
    for indcalcalpha = 1:numreps*numcalcs
        runsystem3_seancheck;
        falsealarmavg = falsealarmavg+mean(falsealarm(11:20));
        detectionavg = detectionavg + mean(detection(11:20));
    end
    falsealarmavg = falsealarmavg/(numreps+1)/numcalcs;
    display(falsealarmavg)
    detectionavg = detectionavg/(numreps+1)/numcalcs;
    tablealpha4(entry,1) = falsealarmtarget;
    tablealpha4(entry,2) = falsealarmavg;
    tablealpha4(entry,3) = detectionavg;
    tablealpha4(entry,4) = ETA_threshold;
        if(falsealarmavg > falsealarmtarget)
            EtaMin = ETA_threshold;
            ETA_threshold = (EtaMax+ETA_threshold)/2;
        else
            EtaMax = ETA_threshold;
            ETA_threshold = (EtaMin+ETA_threshold)/2;
        end
        
    if falsealarmtarget <= 0.1
        EtaMin = EtaMax - .6;
    elseif falsealarmtarget <= 0.2
        EtaMin = EtaMax - 0.5;
    elseif falsealarmtarget <= 0.4
        EtaMin = EtaMax - 0.3;
    else
        EtaMin = EtaMax - 0.8;
    end
    ETA_threshold = (EtaMax+EtaMin)/2;
    entry = entry + 1;
end
