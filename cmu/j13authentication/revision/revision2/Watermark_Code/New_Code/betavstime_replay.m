% Calculate Beta as a function of Time for Different Algorithms for fixed
% alpha and delta

% Instructions
% 1) Choose Appropriate Delta in Simulation.m and Run to Generate Parameters
% 2) Choose alphas for the probability of false alarm.
% 3) Select Type of watermark through variable choice.
% 4) Select Number of Runs we wish to consider in variable length
% 5) Select Window, number of time steps to consider
% 6) Change tottime in runsystem2, runsystemIID2, or runsystem_noise to agree with window.
% 7) mean(detectvect) gives average Beta as a function of time
% 8) Bonus Step, Check that the Alphas agree, to do this write additional
% code

choice = 2; % select algorithm
%ETA_threshold =  3.29976;%1.446; % select probability of false alarm
lengths = 1000; % select number of runs
window = 100; % select window


if choice == 1
    detectvect1_sean = zeros(lengths, window);
elseif choice == 2
    detectvect2_sean = zeros(lengths, window);
else
    detectvect3_sean = zeros(lengths, window);
end

for indice = 1:lengths
    if choice == 1
        NoWatermarkReplay; % run optimal watermark
        detectvect1_sean(indice,:) = (detection(1:window))'; % store detection as function of time for given run
    elseif choice == 2
        IID_Replay; % run optimal IID watermark
        detectvect2_sean(indice,:) = (detection(1:window))'; % store detection as function of time for given run
    else 
        RandomNoiseReplay; % run suboptimal IID watermark
        detectvect3_sean(indice,:) = (detection(1:window))'; % store detection as a funtion of time for given run
    end
    
    if(mod(indice,50) == 0) % Update how far we are in the simulation
        display(indice)
    end
end
    