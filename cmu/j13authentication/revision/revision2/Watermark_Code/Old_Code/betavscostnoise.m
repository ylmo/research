% Plot Beta as a function of the Cost J for fixed alpha, using suboptimal
% watermark

% Instructions
% 1) Comment out delta in simulation.m code before running
% 2) Choose delta's which you wish to use for the for-loop
% 3) Choose alphas for the probability of false alarm.
% 4) Select Number of Runs we wish to consider in variable length, can be
% smaller than optimal case due to ergodicity
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystem_noise if you wish to have each run be longer.
% 7) detectovercost4 vs costs4 provides Beta as a function of Del J
     

indexs = 1;
    
for delta = 10.01:10:40.01
    
    simulation;  % run system to obtain parameters for chosen delta
    display(delta);
    length = 10000;
    Jestimated = zeros(length,1);
    falsetot = zeros(length,1);
    detecttot = zeros(length,1);
    
    alphas = .0063; % originally .10 for CSM, changed on 11/22 for Yilin
    
    for mm = 1:length
        runsystem_noise; % number of runs to consider
        Jestimated(mm) = Jest - Jstar; % store additional cost in each run, should be constant design variable
        falsetot(mm) = mean(falsealarm(50:100)); % store false alarm rate each run, should stay constant design variable
        detecttot(mm) = mean(detection(50:100)); % store detection rate each run,

        falseovercost4(indexs) = mean(falsetot); %Store mean false alarm over all runs, want this to be close to alpha
        detectovercost4(indexs) = mean(detecttot);  %Store mean detection over all runs,
        cost4(indexs) = mean(Jestimated); % Store Detection cost over all runs, should be close to designed values
        indexs = indexs+1;
end

    
    
