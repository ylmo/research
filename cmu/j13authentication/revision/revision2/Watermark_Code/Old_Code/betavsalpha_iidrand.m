% Produce Beta vs Alpha curve for IID identity.
% stored in falseoveralp,and detectoveralp


length = 5;
falsetot = zeros(length,1);
tottime = 10000;
detecttot = falsetot;
ind = 1;

ETA_thresholdmax = 10;
ETA_thresholdmin = -3.5;


for alp = .005:.99/20:.995
    while abs(mean(falsealarm) - alp) > .002
    ETA_threshold = (ETA_thresholdmax + ETA_thresholdmin)/2;
    runsystemIIDrand;
        if(abs(mean(falsealarm - alp)) > .003)
            if(mean(falsealarm) - alp > 0)
                %display('hello')
                ETA_thresholdmin = ETA_threshold;
            else
                %display('helloman');
                ETA_thresholdmax = ETA_threshold;
            end
        end
        display(ETA_threshold);
        display(abs(mean(falsealarm)));
    end
    display(alp);
    display('good')
    ETA_thresholdmax = ETA_threshold;
    ETA_thresholdmin = -3.5;
    
    for qq = 1:length
    runsystemIIDrand;
    falsetot(qq) = mean(falsealarm); 
    detecttot(qq) = mean(detection);
    end

    falseoveralp(ind) = mean(falsetot);
    detectoveralp(ind) = mean(detecttot);
    ind = ind+1;
end
    

    