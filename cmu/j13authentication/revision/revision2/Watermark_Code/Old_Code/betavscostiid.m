% Plot Beta as a function of the Cost J for fixed alpha, using iid
% watermark

% Instructions
% 1) Comment out delta in simulation.m code before running
% 2) Choose delta's which you wish to use for the for-loop
% 3) Choose alphas for the probability of false alarm.
% 4) Select Number of Runs we wish to consider in variable length, can be
% smaller than optimal case due to ergodicity
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystemIID2 if you wish to have each run be longer.
% 7) detectovercost2 vs costs2 provies Beta as a function of Del J
 

indexs = 1;
alphas = .01; % originally .10 for CSM, changed on 11/22 for Yilin
    
for delta = 0.01:10:40.01
    
    simulation; % run system to obtain parameters for chosen delta
    display(delta); 
    length = 1000; % number of runs to consider
    Jestimated = zeros(length,1); % store additional cost in each run, should be constant design variable
    falsetot = zeros(length,1); % store false alarm rate each run, should stay constant design variable
    detecttot = zeros(length,1); % store detection rate each run, 
    
    
    for mm = 1:length
        runsystemIID2; 
        Jestimated(mm) = Jest - Jstar;
        falsetot(mm) = mean(falsealarm(50:100)); 
        detecttot(mm) = mean(detection(50:100));
    end

        falseovercost2(indexs) = mean(falsetot); %Store mean false alarm over all runs, want this to be close to alpha
        detectovercost2(indexs) = mean(detecttot); %Store mean detection over all runs,
        cost2(indexs) = mean(Jestimated); % Store Detection cost over all runs, should be close to designed values
        indexs = indexs+1;
end

    
    
