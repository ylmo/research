etaout = zeros(10001,20);
%length_etaout = 100000;
k  = 1;
    alpha = .0063;
    for j = 1:10001
        f1max = 10;
        f1min = 0;
        %center = 10*(j-1)/l00000-5;
        center = 10*(j-1);
        center = center/10000 - 5;
        f1 = 2.5;
        while(abs((normcdf(f1+center)-normcdf(center-f1) - 1+alpha)) > .00001)
            if(normcdf(f1+center)-normcdf(center-f1) - 1+alpha) >0
                f1max = f1;
            else
                f1min = f1;
            end
            f1 = (f1max+f1min)/2;
        end
        etaout(j,k) = f1;
        if(mod(j,1000)== 0)
            display(j);
        end
    end

