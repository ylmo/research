% run system with fixed eta, covariance of watermark is multiple of
% identity.

GammaRand = eye(p);



% Theoretical Additional cost
L1GammaRand = zeros(n);
for d = 0:100
    L1GammaRand = L1GammaRand + (A+B*L)^d*B*GammaRand*B'*((A+B*L)^d)';
end
Jtheory = trace(U*GammaRand)+ trace((W+L'*U*L)*L1GammaRand);
GammaRand = delta*GammaRand/Jtheory;

L1GammaRand = zeros(n);
for d = 0:100
    L1GammaRand = L1GammaRand + (A+B*L)^d*B*GammaRand*B'*((A+B*L)^d)';
end
Jtheory = trace(U*GammaRand)+ trace((W+L'*U*L)*L1GammaRand);

L2GammaRand = zeros(n);
for q = 0:100
    L2GammaRand = L2GammaRand + cA^q*B*GammaRand*B'*((cA)^q)';
end
SigmaTheoryIIDrand = C*L2GammaRand*C';





% virtual system.

%must change in multiple places.
tottime = 10000;
x = zeros(n,tottime);
y = zeros(m,tottime);
xhat = x; xpred = x;
xmean = 0;



% time step 0, virtual system
x(:,1) = mvnrnd(xmean*ones(n,1),P);
xpred(:,1) = xmean*ones(n,1);
v = mvnrnd(zeros(m,1),R);
y(:,1) = C*x(:,1)+v';
xhat(:,1) = xpred(:,1) + K*(y(:,1)-C*xpred(:,1));
zeta = zeros(p,tottime-1);
muv = zeros(n,tottime-1);
falsealarm = zeros(tottime-1,1);
detection = falsealarm;



% run virtual system
for nn = 2:tottime
    zeta(:,nn-1) = mvnrnd(zeros(p,1),GammaRand);
    
    if(nn == 2)
        muv(:,1) = B*zeta(:,1);
    else
        muv(:,nn-1) = cA*muv(:,nn-2) + B*zeta(:,nn-1);
    end
    
    u = L*xhat(:,nn-1) + zeta(:,nn-1);
    v = mvnrnd(zeros(m,1),R);
    w = mvnrnd(zeros(n,1),Q);
    
    x(:,nn) = A*x(:,nn-1) + B*u + w';
    y(:,nn) = C*x(:,nn) + v';
    
    xpred(:,nn) = A*xhat(:,nn-1)+B*u;
    xhat(:,nn) = xpred(:,nn) +  K*(y(:,nn)-C*xpred(:,nn));    
end
muv = -C*muv;
z = y-C*xpred;






% Detector Virtual System
for nn = 2:tottime
    center = round(-1000*muv(:,nn-1)/(SigmaTheoryIIDrand*cP^(-1/2)))/1000;
    if(abs(center) <= 5)
        etafunc = etaout(round(center*1000+5001),round(alphas*100));
    elseif(center > 5)
        etafunc = etaout(10001,round(alphas*100));
    else
        etafunc = etaout(1,round(alphas*100));
    end
    ETA_threshold = ((SigmaTheoryIIDrand)^2*cP^-1*etafunc^2- muv(:,nn-1)^2*(1+SigmaTheoryIIDrand*cP^-1))/((cP+SigmaTheoryIIDrand)*(SigmaTheoryIIDrand*cP^-1));
    
    if(z(:,nn)'*cP^-1*z(:,nn) - (z(:,nn)-muv(:,nn-1))'*(cP+SigmaTheoryIIDrand)^-1*(z(:,nn)-muv(:,nn-1)) < ETA_threshold)
        falsealarm(nn-1) = 0;
    else
        falsealarm(nn-1) = 1;
    end
end


  

% estimate covariance of zv, and additional cost.
for nn = 1:tottime
    if nn < tottime
        Jest = Jest + ((L*xhat(:,nn) + zeta(:,nn))'*U*(L*xhat(:,nn) + zeta(:,nn)) + x(:,nn)'*W*x(:,nn))/(tottime-1);
    end
    Pcest = Pcest + (y(:,nn)-C*xpred(:,nn))*(y(:,nn)-C*xpred(:,nn))'/tottime;
end









% Compromised System.
xhatc = zeros(n,tottime);
xpredc = xhatc;

xpredc(:,1) = xmean*ones(n,1);
xhatc(:,1) = xpredc(:,1) + K*(y(:,1)-C*xpredc(:,1));
zetac = zeros(p,tottime-1);
muc = zeros(n,tottime-1);








% run compromised system
for nn = 2:tottime
    zetac(:,nn-1) = mvnrnd(zeros(p,1),GammaRand);
    if(nn == 2)
        muc(:,1) = B*zetac(:,1);
    else
        muc(:,nn-1) = cA*muc(:,nn-2) + B*zetac(:,nn-1);
    end

    u = L*xhatc(:,nn-1) + zetac(:,nn-1);
    xpredc(:,nn) = A*xhatc(:,nn-1)+B*u;
    xhatc(:,nn) = xpredc(:,nn) +  K*(y(:,nn)-C*xpredc(:,nn));
end
    muc = -C*muc;
    
    
    
    
    
    
    
% estimate covariance of zc
for nn = 2:tottime
    Pcest2 = Pcest2 + (y(:,nn)-C*xpredc(:,nn)-muc(:,nn-1))*(y(:,nn)-C*xpredc(:,nn)-muc(:,nn-1))'/(tottime-1);
end

zc = y-C*xpredc;






% Detector Compromised  System



for nn = 2:tottime
    center = round(-1000*muc(:,nn-1)/(SigmaTheoryIIDrand*cP^(-1/2)))/1000;
    if(abs(center) <= 5)
        etafunc = etaout(round(center*1000+5001),round(alphas*100));
    elseif(center > 5)
        etafunc = etaout(10001,round(alphas*100));
    else
        etafunc = etaout(1,round(alphas*100));
    end
    ETA_threshold = ((SigmaTheoryIIDrand)^2*cP^-1*etafunc^2- muc(:,nn-1)^2*(1+SigmaTheoryIIDrand*cP^-1))/((cP+SigmaTheoryIIDrand)*(SigmaTheoryIIDrand*cP^-1));
    
        if(zc(:,nn)'*cP^-1*zc(:,nn) - (zc(:,nn)-muc(:,nn-1))'*(cP+SigmaTheoryIIDrand)^-1*(zc(:,nn)-muc(:,nn-1)) < ETA_threshold)
            detection(nn-1) = 0;
        else
            detection(nn-1) = 1;
        end
end



