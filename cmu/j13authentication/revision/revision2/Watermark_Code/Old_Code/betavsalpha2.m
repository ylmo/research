% Plot Beta as a function of Alpha for fixed Del J, using optimal
% watermark

% Instructions
% 1) Choose Desired Delta and Run simulation.m
% 2) Choose alpha's which you wish to use for the for-loop
% 3) Select Number of Runs we wish to consider in variable length, should
% be longer here because we do not have ergodicity
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystem2 if you wish to have each run be longer.
% 7) detectoveralp3 vs falseoveralp3 provides Beta vs Alpha for fixed cost
% 8) Check commented portion to see which system we are running;


length = 10000; % nummber of runs we wish to consider
falsetot = zeros(length,1); % store falsealarm rates for each run, should stay relatively constant
detecttot = zeros(length,1); % store detection rates for each run
ind = 1;

    
for alp = .50:.05:.70
    alphas = alp;
    display(alp);
    for mm = 1:length
        runsystem2;
        %runsystem3_seancheck;
        falsetot(mm) = mean(falsealarm(10:12)); 
        detecttot(mm) = mean(detection(10:12));
    end

    falseoveralp3(ind) = mean(falsetot); % store average false alarm rate for given alpha, should be close to design value
    detectoveralp3(ind) = mean(detecttot); % store average detection rate for given alpha
    ind = ind+1;
end
    

    
%  plot(falseoveralp3,100*(detectoveralp3 - interp(falseoveralp3)')./interp(falseoveralp3)','r-o')
% plot(falseoveralp5,100*(detectoveralp5 - interp(falseoveralp5)')./interp(falseoveralp5)','b-o')
