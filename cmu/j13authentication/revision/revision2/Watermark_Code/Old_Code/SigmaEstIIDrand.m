
% IID identity, repeatedly run the system for fixed eta. Make Estimates of Sigma and
% DeltaJ for multiple runs;

SigmaEstimateRandIIDrand = zeros(m);
DiffJtheoryJest1IIDrand = 0;
DiffJtheoryJest2IIDrand = 0;
SigmaTheoryIIDtot = zeros(m);
length = 5;
falsetot3 = zeros(length,1);
tottime = 10000;
false = zeros(length,tottime-1);
detect = false;
detecttot3 = falsetot3;

for qq = 1:length
    runsystemIIDrand;
    SigmaEstimateRandIIDrand = SigmaEstimateRandIIDrand + (Pcest2 -Pcest);
    SigmaTheoryIIDtot = SigmaTheoryIIDrand +   SigmaTheoryIIDtot;
    display(qq);
    DiffJtheoryJest1IIDrand =  DiffJtheoryJest1IIDrand  + (Jtheory);
    DiffJtheoryJest2IIDrand = DiffJtheoryJest2IIDrand + (Jest-Jstar);
    display((DiffJtheoryJest1IIDrand- DiffJtheoryJest2IIDrand)/qq);
    false(qq,:) = falsealarm';
    detect(qq,:) = detection';
    falsetot3(qq) = mean(falsealarm); 
    detecttot3(qq) = mean(detection);
end
    
SigmaEstimateRandIIDrand = SigmaEstimateRandIIDrand/qq;
 SigmaTheoryIIDtot =  SigmaTheoryIIDtot/qq;
