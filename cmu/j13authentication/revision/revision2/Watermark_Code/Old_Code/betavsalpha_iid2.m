% Plot Beta as a function of Alpha for fixed Del J, using IID optimal
% watermark

% Instructions
% 1) Choose Desired Delta and Run simulation.m
% 2) Choose alpha's which you wish to use for the for-loop
% 3) Select Number of Runs we wish to consider in variable length, can
% be shorter here because we have ergodicity
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystemIID2 if you wish to have each run be longer.
% 7) detectoveralp4 vs falseoveralp4 provides Beta vs Alpha for fixed cost



length = 500; % number of runs we wish to consider
falsetot = zeros(length,1);% store falsealarm rates for each run, should stay relatively constant
detecttot = zeros(length,1); % store detection rates for each run
ind = 1;

for alp = .10:.10:.30
    alphas = alp;
    display(alp);
    for qq = 1:length
        runsystemIID2;
        falsetot(qq) = mean(falsealarm(50:100)); 
        detecttot(qq) = mean(detection(50:100));
    end

    falseoveralp4(ind) = mean(falsetot); % store average false alarm rate for given alpha, should be close to design value
    detectoveralp4(ind) = mean(detecttot);  % store average detection rate for given alpha
    ind = ind+1;
end
    

    