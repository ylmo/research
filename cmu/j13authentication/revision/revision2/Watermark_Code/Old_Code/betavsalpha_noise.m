% Plot Beta as a function of Alpha for fixed Del J, using sub-optimal
% watermark

% Instructions
% 1) Choose Desired Delta and Run simulation.m
% 2) Choose alpha's which you wish to use for the for-loop
% 3) Select Number of Runs we wish to consider in variable length, can
% be shorter here because we have ergodicity
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystem_noise if you wish to have each run be longer.
% 7) detectoveralp5 vs falseoveralp5 provides Beta vs Alpha for fixed cost


length = 1000; % nummber of runs we wish to consider
falsetot = zeros(length,1); % store falsealarm rates for each run, should stay relatively constant
detecttot = zeros(length,1); % store detection rates for each run
Jtot = zeros(length,1); % Check to see that Delta J stays relatively constant for suboptimal method
ind = 1;

for alp = .02:.02:.98
    alphas = alp;
    display(alp);
    for mm = 1:length
        runsystem_noise;
        falsetot(mm) = mean(falsealarm(50:100)); 
        detecttot(mm) = mean(detection(50:100));
        Jtot(mm) = Jest;
    end

    falseoveralp6(ind) = mean(falsetot); % store average false alarm rate for given alpha, should be close to design value
    detectoveralp6(ind) = mean(detecttot);  % store average detection rate for given alpha
    Jrun(ind) = mean(Jtot); % store cost, should be close to design value for all alphas
    ind = ind+1;
end
    
