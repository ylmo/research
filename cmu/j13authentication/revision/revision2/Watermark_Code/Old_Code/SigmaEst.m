% Optimal, repeatedly run the system for fixed eta. Make Estimates of Sigma and
% DeltaJ for multiple runs;

SigmaTheory = zeros(m,m);

for d = 0:100
    Td = Ch*(Aw^d)'*Ch';
    L2Td = zeros(n,n);
    for rr = 0:50
        L2Td = L2Td + cA^rr*B*Td*B'*((cA)^rr)';
    end
    if d == 0
        SigmaTheory = SigmaTheory + C*L2Td*C';
    else
        SigmaTheory = SigmaTheory + C*cA^d*L2Td*C' + C*L2Td'*((cA)^d)'*C';
    end
end


SigmaEstimate = zeros(m);
DiffJtheoryJest1 = 0;
DiffJtheoryJest2 = 0;
length = 1000;
falsetot = zeros(length,1);
detecttot = falsetot;
for rr = 1:length;
    runsystem
    SigmaEstimate = SigmaEstimate + (Pcest2 -Pcest);
    if(mod(rr,10) == 0)
        display(rr);
        if m == 1
            display(((SigmaEstimate/rr)-SigmaTheory)/SigmaTheory);
        end
    end
    DiffJtheoryJest1 =  DiffJtheoryJest1  + (Jtheory2);
    DiffJtheoryJest2 = DiffJtheoryJest2 + (Jest-Jstar);
    %display((DiffJtheoryJest1- DiffJtheoryJest2)/rr);
    falsetot(rr) = mean(falsealarm); 
    detecttot(rr) = mean(detection);
end
    
SigmaEstimate = SigmaEstimate/rr;


