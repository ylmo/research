% Run system in the case that covariance of watermark is sub-optimal. Eta
% is varied at each step to obtain desired false alarm rate.

%Instructions
%1) Check tottime to see how long each run is 
%2) Download MAT file ETAOUT as desired.
%3) Ensure alphas is chosen as dsired
%4) Run simulation.m first for chosen delta J (delta) to generate
%parameters


% virtual system.
tottime = 101;
x = zeros(n,tottime);
y = zeros(m,tottime);
xhat = x; xpred = x;

xmean = 0;

% time step 0, virtual system
x(:,1) = mvnrnd(xmean*ones(n,1),P);
xpred(:,1) = xmean*ones(n,1);
v = mvnrnd(zeros(m,1),R);
y(:,1) = C*x(:,1)+v';
xhat(:,1) = xpred(:,1) + K*(y(:,1)-C*xpred(:,1));
xi = mvnrnd(zeros(2,1),eye(2));
xi = xi';
zeta = zeros(p,tottime-1);
muv = zeros(n,tottime-1); % our advantage over attacker as function of time
falsealarm = zeros(tottime-1,1);
detection = falsealarm;

uu = zeros(p,tottime-1); % store input for all time
% run virtual system
for nn = 2:tottime
    
    zeta(:,nn-1) = Ch*xi;
    
    if(nn == 2)
        muv(:,1) = B*zeta(:,1);
    else
        muv(:,nn-1) = cA*muv(:,nn-2) + B*zeta(:,nn-1);
    end
    
    u = L*xhat(:,nn-1) + zeta(:,nn-1);
    uu(:,nn-1) = u;
    v = mvnrnd(zeros(m,1),R);
    w = mvnrnd(zeros(n,1),Q); %w = 0; %w = (KK*v')'; 
    
    x(:,nn) = A*x(:,nn-1) + B*u + w';
    y(:,nn) = C*x(:,nn) + v';
    
    xpred(:,nn) = A*xhat(:,nn-1)+B*u;
    xhat(:,nn) = xpred(:,nn) +  K*(y(:,nn)-C*xpred(:,nn));
    xi = rho*Aw*xi + mvnrnd(zeros(2,1),(1-rho^2)*eye(2))';
    if(mod(nn,100000) == 0)
        display(nn);
    end
end
muv = -C*muv; % our advantage over attacker as function of time
z= y - C*xpred; % The attack residue

% Detector Virtual System
for nn = 2:tottime
     if(length_etaout == 100000)
        center = round(-10000*muv(:,nn-1)/(SigmaTheorySubOpt*cP^(-1/2)))/10000;
        if(abs(center) <= 5)
            if(min(size(etaout)) > 20)
                etafunc = etaout(round(center*10000+50001),round(alphas*100));
            else
                etafunc = etaout(round(center*10000+50001),round(alphas*200));
            end
        elseif(center > 5)
            if(min(size(etaout)) > 20)
                etafunc = etaout(100001,round(alphas*100));
            else
                etafunc = etaout(100001,round(alphas*200));
            end
        else
            if(min(size(etaout)) > 20)
                etafunc = etaout(1,round(alphas*100));
            else
                etafunc = etaout(1,round(alphas*200));
            end
        end     
    
    else    
    center = round(-1000*muv(:,nn-1)/(SigmaTheorySubOpt*cP^(-1/2)))/1000;
    if(abs(center) <= 5)
        if(min(size(etaout)) > 20)
            etafunc = etaout(round(center*1000+5001),round(alphas*100));
        else
            etafunc = etaout(round(center*1000+5001),round(alphas*200));
        end
    elseif(center > 5)
        if(min(size(etaout)) > 20)
            etafunc = etaout((10001),round(alphas*100));
        else
            etafunc = etaout((10001),round(alphas*200));
        end
    else
        if(min(size(etaout)) > 20)
            etafunc = etaout(1,round(alphas*100));
        else
            etafunc = etaout(1,round(alphas*200));
        end
    end
     end
     
    % Determine Eta at each time step for given probability of false alarm
    ETA_threshold = ((SigmaTheorySubOpt)^2*cP^-1*etafunc^2- muv(:,nn-1)^2*(1+SigmaTheorySubOpt*cP^-1))/((cP+SigmaTheorySubOpt)*(SigmaTheorySubOpt*cP^-1));
    
    % Determine if the false alarms have occurred.
    if(z(:,nn)'*cP^-1*z(:,nn) - (z(:,nn)-muv(:,nn-1))'*(cP+SigmaTheorySubOpt)^-1*(z(:,nn)-muv(:,nn-1)) < ETA_threshold)
        falsealarm(nn-1) = 0;
    else
        falsealarm(nn-1) = 1;
    end
end




Pcest = zeros(m,m);
Pcest2 = zeros(m,m);
Jest = 0;
Jtheory = 0;
% estimate covariance of zv, and additional cost.
for nn = 1:tottime
    if nn < tottime
        Jest = Jest + ((L*xhat(:,nn) + zeta(:,nn))'*U*(L*xhat(:,nn) + zeta(:,nn)) + x(:,nn)'*W*x(:,nn))/(tottime-1);
    end
    Pcest = Pcest + (y(:,nn)-C*xpred(:,nn))*(y(:,nn)-C*xpred(:,nn))'/tottime;
end



% Compromised System, initialized at time 0
xhatc = zeros(n,tottime);
xpredc = xhatc;

xpredc(:,1) = xmean*ones(n,1);
xhatc(:,1) = xpredc(:,1) + K*(y(:,1)-C*xpredc(:,1));
xic = mvnrnd(zeros(2,1),eye(2));
xic = xic';
zetac = zeros(p,tottime-1);
muc = zeros(n,tottime-1);
muc(:,1) = B*Ch*xic;

% run compromised system
for nn = 2:tottime
    zetac(:,nn-1) = Ch*xic;
    if(nn > 2)
        muc(:,nn-1) = cA*muc(:,nn-2) + B*zetac(:,nn-1);
    end

    u = L*xhatc(:,nn-1) + zetac(:,nn-1);
    xpredc(:,nn) = A*xhatc(:,nn-1)+B*u;
    xhatc(:,nn) = xpredc(:,nn) +  K*(y(:,nn)-C*xpredc(:,nn));
    xic = rho*Aw*xic + mvnrnd(zeros(2,1),(1-rho^2)*eye(2))';
end
muc = -C*muc; % calculate the advantage over the attacker mu_k^c
    
% estimate covariance of zc
for nn = 2:tottime
    Pcest2 = Pcest2 + (y(:,nn)-C*xpredc(:,nn)-muc(:,nn-1))*(y(:,nn)-C*xpredc(:,nn)-muc(:,nn-1))'/(tottime-1);
end

zc = y-C*xpredc; % determine the residue at each time k.

% detector compromised system

for nn = 2:tottime
     if(length_etaout == 100000)
        center = round(-10000*muc(:,nn-1)/(SigmaTheorySubOpt*cP^(-1/2)))/10000;
        if(abs(center) <= 5)
            if(min(size(etaout)) > 20)
                etafunc = etaout(round(center*10000+50001),round(alphas*100));
            else
                etafunc = etaout(round(center*10000+50001),round(alphas*200));
            end
        elseif(center > 5)
            if(min(size(etaout)) > 20)
                etafunc = etaout(100001,round(alphas*100));
            else
                etafunc = etaout(100001,round(alphas*200));
            end
        else
            if(min(size(etaout)) > 20)
                etafunc = etaout(1,round(alphas*100));
            else
                etafunc = etaout(1,round(alphas*200));
            end
        end     
    
    else    
    center = round(-1000*muc(:,nn-1)/(SigmaTheorySubOpt*cP^(-1/2)))/1000;
     if(abs(center) <= 5)
        if(min(size(etaout)) > 20)
            etafunc = etaout(round(center*1000+5001),round(alphas*100));
        else
            etafunc = etaout(round(center*1000+5001),round(alphas*200));
        end
    elseif(center > 5)
        if(min(size(etaout)) > 20)
            etafunc = etaout(round(10001),round(alphas*100));
        else
            etafunc = etaout(round(10001),round(alphas*200));
        end
    else
        if(min(size(etaout)) > 20)
            etafunc = etaout(1,round(alphas*100));
        else
            etafunc = etaout(1,round(alphas*200));
        end
     end
     end
     
    % Determine eta threshold at each time k for given probability of false alarms
    ETA_threshold = ((SigmaTheorySubOpt)^2*cP^-1*etafunc^2- muc(:,nn-1)^2*(1+SigmaTheorySubOpt*cP^-1))/((cP+SigmaTheorySubOpt)*(SigmaTheorySubOpt*cP^-1));
    
    % determine if detection has taken place.
    if(zc(:,nn)'*cP^-1*zc(:,nn) - (zc(:,nn)-muc(:,nn-1))'*(cP+SigmaTheorySubOpt)^-1*(zc(:,nn)-muc(:,nn-1)) < ETA_threshold)
        detection(nn-1) = 0;
    else
        detection(nn-1) = 1;
    end
end



