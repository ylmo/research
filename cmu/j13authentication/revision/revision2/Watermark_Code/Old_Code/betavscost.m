% Plot Beta as a function of the Cost J for fixed alpha, using optimal
% watermark

% Instructions
% 1) Comment out delta in simulation.m code before running
% 2) Choose delta's which you wish to use for the for-loop
% 3) Choose alphas for the probability of false alarm.
% 4) Select Number of Runs we wish to consider in variable length
% 5) Select Times we wish to count false alarms and detections in
% falsetot(mm) = .. and detecttot(mm), leave some time for convergence
% 6) Change tottime in runsystem2 if you wish to have each run be longer.
% 7) detectovercost vs costs provies Beta as a function of Del J
 
alphas = .01; % originally .10 for CSM, changed on 11/22 for Yilin
indexs = 1;
    
for delta = 10.01:10:40.01  
    
    simulation; % run system to obtain parameters for chosen delta
    display(delta);
    length = 15000; % number of runs to consider
    Jestimated = zeros(length,1); % Store additional cost for each run (should be relatively constant design variable)
    falsetot = zeros(length,1); % Store false alarm average for each run, should be constant, it is a design variable.
    detecttot = zeros(length,1); % Store detection average for each each run

    
    for mm = 1:length
        runsystem2;
        Jestimated(mm) = Jest - Jstar;
        falsetot(mm) = mean(falsealarm(50:100)); 
        detecttot(mm) = mean(detection(50:100));
    end

    
   falseovercost(indexs) = mean(falsetot);  %Store mean false alarm over all runs, want this to be close to alpha
   detectovercost(indexs) = mean(detecttot); % Store mean detection over all runs
   costs(indexs) = mean(Jestimated); % Store Detection cost over all runs
   indexs = indexs+1;
   
end

