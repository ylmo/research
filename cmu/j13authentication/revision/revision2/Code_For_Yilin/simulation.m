%clc
%clear
rand('twister',sum(clock)*100)

%initialization
rho = .5;
%delta = 10;
display(delta)
A = [1 1;0 1];
B = [0;1];
C = [1 0];
n = 2;
m = 1;
p = 1;

W = eye(n);
U = eye(p);
Q = .8*eye(n);  %display('This was used on 11/22 for Yilin');
R = eye(m);


P = eye(n);
S = eye(n);
KK = rand(n,m);

% Generate K,L cP, cA matrices, choose Delta
for j = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;

cP = C*P*C' + R;
invcP = inv(cP);
invcP = (invcP+invcP')/2;

Jstar=trace(S*Q)+trace((A'*S*A+W-S)*(P-K*C*P));
cA = (A+B*L)*(eye(n)-K*C);
display('Eigenvalues of Caligraphica A');
abs(eig(cA))
CinvcPC = (C'*invcP*C + (C'*invcP*C)')/2;



% Optimization to determine H and Omegastar
%i is reserved for imaginary part
%Q2 Q3 refer to Theta2 Theta 3 in the paper
index = 1;
for omega = 0:0.01:0.5
    s = exp (2*pi*1i*omega);
    cvx_begin quiet
    variable H(p,p) hermitian %semidefinite;
    H == semidefinite(p);
    variable F1;
    variable F2(n,n) hermitian;
    variable L1(n,n) hermitian;
    variable L2(n,n) hermitian;
    variable Q2(p,p) hermitian;
    variable Q3(n,n) hermitian;
    maximize (trace(F2*CinvcPC));
    subject to
    F1 <= delta;
    L1 == (A+B*L)*L1*transpose(A+B*L)+B*H*transpose(B);
    L2 == cA*L2*transpose(cA) + B*H*transpose(B);
    Q2 == 2*real(s*L*inv(eye(n)-s*(A+B*L))*B*H + transpose(s*L*inv(eye(n)-s*(A+B*L))*B*H) + H);
    Q3 == 2*real(inv(eye(n)-s*(A+B*L))*L1+transpose(inv(eye(n)-s*(A+B*L))*L1)-L1);
    F1 == trace(U*Q2) + trace((W+L'*U*L)*Q3);
    F2 == 2*real(inv(eye(n)-s*cA)*L2+transpose(inv(eye(n)-s*cA)*L2)-L2);
    cvx_end
    obj(index)=cvx_optval;
    Hopt{index} = H;
    index = index + 1;
end
plot(0:0.01:0.5,obj)

[opt index] = max(obj);
omegastar = 0.01*(index-1);
Hstar = Hopt{index};


% IID Optimal, Optimization Problem to Obtain IID optimal Watermark

cvx_begin quiet
    variable Gamma0(p,p) hermitian %semidefinite;
    Gamma0 == semidefinite(p);
    variable Sigg(m,m) hermitian;
    variable L11(n,n) hermitian;
    variable L22(n,n) hermitian;
    variable DelJ(1,1)
    
    maximize (trace(Sigg*invcP));
    subject to
    DelJ <= delta;
    DelJ == trace(U*Gamma0) + trace((W+L'*U*L)*L11);
    L11 == (A+B*L)*L11*transpose(A+B*L) + B*Gamma0*transpose(B);
    Sigg == C*L22*transpose(C);
    L22 ==  cA*L22*transpose(cA) + B*Gamma0*transpose(B)
cvx_end


% Determine Optimal IID Sigma, where Sigma is the additional Covariance
% SigmaTheoryIID

P = (P+P')/2;
L2Gamma0 = zeros(n);
for q = 0:100
    L2Gamma0 = L2Gamma0 + cA^q*B*Gamma0*B'*((cA)^q)';
end
SigmaTheoryIID = C*L2Gamma0*C';


% Calculate Covariance for IID diagonal, determine theoretical covariance
% SigmaTheoryIIDrand

GammaRand = eye(p);

L1GammaRand = zeros(n);
for d = 0:100
    L1GammaRand = L1GammaRand + (A+B*L)^d*B*GammaRand*B'*((A+B*L)^d)';
end
Jtheory = trace(U*GammaRand)+ trace((W+L'*U*L)*L1GammaRand);
GammaRand = delta*GammaRand/Jtheory;

L1GammaRand = zeros(n);
for d = 0:100
    L1GammaRand = L1GammaRand + (A+B*L)^d*B*GammaRand*B'*((A+B*L)^d)';
end
Jtheory = trace(U*GammaRand)+ trace((W+L'*U*L)*L1GammaRand);

L2GammaRand = zeros(n);
for q = 0:100
    L2GammaRand = L2GammaRand + cA^q*B*GammaRand*B'*((cA)^q)';
end
SigmaTheoryIIDrand = C*L2GammaRand*C';


% obtain Aw, Ch, and h used to generate the so called optimal watermark

P = (P+P')/2;
Aw = [cos(2*pi*omegastar) -sin(2*pi*omegastar); sin(2*pi*omegastar) cos(2*pi*omegastar)];
h = zeros(p,1); 
for nn = 1:p
    h(nn) = sqrt(Hstar(nn,nn));
    if(nn > 1)
        if(sign(h(1)*h(nn)) ~= sign(Hstar(1,nn)))
            h(nn) = -1*h(nn);
        end
    end
end
if(norm(h*conj(h)'-Hstar) > .001)
    display('Error H calculation');
end

Ch = sqrt(2)*[real(h) imag(h)];


% Obtain Theoretical Covariance of the so called optimal watermark
% SigmaTheory
SigmaTheory = zeros(m,m);

for d = 0:100
    Td = Ch*(Aw^d)'*Ch';
    L2Td = zeros(n,n);
    for q = 0:50
        L2Td = L2Td + cA^q*B*Td*B'*((cA)^q)';
    end
    if d == 0
        SigmaTheory = SigmaTheory + C*L2Td*C';
    else
        SigmaTheory = SigmaTheory + C*cA^d*L2Td*C' + C*L2Td'*((cA)^d)'*C';
    end
end

%Obtain Theoretical Covariance Suboptimal Watermark, SigmaTheorySubopt

SigmaTheorySubOpt = zeros(m,m);
for d = 0:100
    Tdsub = rho^(abs(d))*Ch*(Aw^d)'*Ch';
    L2Tdsub = zeros(n,n);
    for q = 0:50
        L2Tdsub = L2Tdsub + cA^q*B*Tdsub*B'*((cA)^q)';
    end
    if d == 0
        SigmaTheorySubOpt = SigmaTheorySubOpt + C*L2Tdsub*C';
    else
        SigmaTheorySubOpt = SigmaTheorySubOpt + C*cA^d*L2Tdsub*C' + C*L2Tdsub'*((cA)^d)'*C';
    end
end

JtheorySubOpt = 0;
% Determine Theoretical Additional Cost JtheorySubOpt for the Suboptimal
% Method
for dd = 0:100
    Tddsubopt = rho^dd*Ch*(Aw^dd)'*Ch';
    Tdd1subopt = rho^(dd+1)*Ch*(Aw^(dd+1))'*Ch';
    JtheorySubOpt = JtheorySubOpt + trace(U*((L*(A+B*L)^dd*B*Tdd1subopt)+(L*(A+B*L)^dd*B*Tdd1subopt)'));
    L1dsubopt = zeros(n);
    for qq = 0:50;
        L1dsubopt = L1dsubopt + (A+B*L)^qq*B*Tddsubopt*B'*((A+B*L)^qq)';
    end
    JtheorySubOpt = JtheorySubOpt + trace((W+L'*U*L)*((A+B*L)^dd*L1dsubopt + ((A+B*L)^dd*L1dsubopt)'));
    if dd == 0
        JtheorySubOpt = JtheorySubOpt + trace(U*Tddsubopt);
        JtheorySubOpt = JtheorySubOpt - trace((W+L'*U*L)*L1dsubopt);
    end
%    display(JtheorySubOpt);
end


% Display Objective functions for different methods

display(trace(SigmaTheory*cP^-1))

display(trace(SigmaTheorySubOpt*cP^-1))

display(trace(SigmaTheoryIID*cP^-1))

display(trace(SigmaTheoryIIDrand*cP^-1))
