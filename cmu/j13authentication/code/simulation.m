clc
clear
rand('twister',sum(clock)*100)

%initialization
A = [1 1;0 1];
B = [0;1];
C = [1 0];
n = 2;
m = 1;
p = 1;



W = eye(n);
U = eye(p);
Q = eye(n);
R = eye(m);

P = eye(n);
S = eye(n);

for j = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;

cP = C*P*C' + R;
invcP = inv(cP);
Jstar=trace(S*Q)+trace((A'*S*A+W-S)*(P-K*C*P));
cA = (A+B*L)*(eye(n)-K*C);

abs(eig(cA))


delta = 1;
%i is reserved for imaginary part
%Q2 Q3 refer to Theta2 Theta 3 in the paper
index = 1;
for omega = 0:0.01:0.5
    s = exp (2*pi*1i*omega);
    cvx_begin quiet
    variable H(p,p) hermitian semidefinite;
    variable F1;
    variable F2(n,n) hermitian;
    variable L1(n,n) hermitian;
    variable L2(n,n) hermitian;
    variable Q2(p,p) hermitian;
    variable Q3(n,n) hermitian;
    maximize (trace(F2*C'*invcP*C));
    subject to
    F1 <= delta;
    L1 == (A+B*L)*L1*transpose(A+B*L)+B*H*transpose(B);
    L2 == cA*L2*transpose(cA) + B*H*transpose(B);
    Q2 == 2*real(s*L*inv(eye(n)-s*(A+B*L))*B*H + transpose(s*L*inv(eye(n)-s*(A+B*L))*B*H) + H);
    Q3 == 2*real(inv(eye(n)-s*(A+B*L))*L1+transpose(inv(eye(n)-s*(A+B*L))*L1)-L1);
    F1 == trace(U*Q2) + trace((W+L'*U*L)*Q3);
    F2 == 2*real(inv(eye(n)-s*cA)*L2+transpose(inv(eye(n)-s*cA)*L2)-L2);
    cvx_end
    obj(index)=cvx_optval;
    Hopt{index} = H;
    index = index + 1;
end
plot(0:0.01:0.5,obj)

[opt index] = max(obj);
omegastar = 0.01*(index-1);
Hstar = Hopt{index};
%generate the signal
%zeta = authentication(omegastar,Hstar,100);

