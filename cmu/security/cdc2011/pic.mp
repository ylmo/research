input TEX;
input boxes;
verbatimtex
%&latex
\documentclass{article}
\begin{document}
etex


beginfig(1)
r:=0.5mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b,c,d,e,f,g;
a:=(0,0mm); b:=(0,20mm);c:=(30mm,10mm);d=(60mm,0mm);e=(60mm,20mm);
path na,nb,nc,nd,ne;
na=node shifted a;
nb=node shifted b;
nc=node shifted c;
nd=node shifted d;
ne=node shifted e;
draw na withpen pencircle scaled 0.5pt;
draw nb withpen pencircle scaled 0.5pt;
draw nc withpen pencircle scaled 0.5pt withcolor red;
draw nd withpen pencircle scaled 0.5pt withcolor red;
draw ne withpen pencircle scaled 0.5pt;
fill nc withcolor red;
fill nd withcolor red;
drawarrow a--b cutbefore na cutafter nd;
drawarrow b--c cutbefore nb cutafter nc;
drawarrow c--a cutbefore nc cutafter na;

drawarrow d--a cutbefore nd cutafter na withcolor red;
drawarrow e--b cutbefore ne cutafter nb withcolor red;

drawarrow d--c cutbefore nd cutafter nc;
drawarrow c--e cutbefore nc cutafter ne withcolor red;
drawarrow e--d cutbefore ne cutafter nd;

draw (b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle dashed evenly;
label.top(btex $T$ etex,b+(0,0.5cm));
label.llft(btex 1 etex,a);
label.ulft(btex 2 etex,b);
label.bot (btex 3 etex,c);
label.lrt (btex 4 etex,d);
label.urt (btex 5 etex,e);
endfig;

beginfig(2)
r:=0.5mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b,c,d,e,f,g;
a:=(0,0mm); b:=(0,20mm);c:=(30mm,10mm);d=(60mm,0mm);e=(60mm,20mm);
path na,nb,nc,nd,ne;
na=node shifted a;
nb=node shifted b;
nc=node shifted c;
nd=node shifted d;
ne=node shifted e;
draw na withpen pencircle scaled 0.5pt withcolor red;
draw nb withpen pencircle scaled 0.5pt;
draw nc withpen pencircle scaled 0.5pt;
draw nd withpen pencircle scaled 0.5pt withcolor red;
draw ne withpen pencircle scaled 0.5pt;
fill na withcolor red;
fill nd withcolor red;
drawarrow a--b cutbefore na cutafter nd;
drawarrow b--c cutbefore nb cutafter nc;
drawarrow c--a cutbefore nc cutafter na;

drawarrow d--a cutbefore nd cutafter na;
drawarrow e--b cutbefore ne cutafter nb withcolor red;

drawarrow d--c cutbefore nd cutafter nc withcolor red;
drawarrow c--e cutbefore nc cutafter ne withcolor red;
drawarrow e--d cutbefore ne cutafter nd;

draw (b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle dashed evenly;
label.top(btex $T$ etex,b+(0,0.5cm));
label.llft(btex 1 etex,a);
label.ulft(btex 2 etex,b);
label.bot (btex 3 etex,c);
label.lrt (btex 4 etex,d);
label.urt (btex 5 etex,e);
endfig;

beginfig(3)
r:=0.5mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b,c,d,e,f,g;
a:=(0,0mm); b:=(0,20mm);c:=(30mm,10mm);d=(60mm,0mm);e=(60mm,20mm);
path na,nb,nc,nd,ne;
na=node shifted a;
nb=node shifted b;
nc=node shifted c;
nd=node shifted d;
ne=node shifted e;
draw na withpen pencircle scaled 0.5pt;
draw nb withpen pencircle scaled 0.5pt;
draw nc withpen pencircle scaled 0.5pt;
draw nd withpen pencircle scaled 0.5pt;
draw ne withpen pencircle scaled 0.5pt;
drawarrow a--b cutbefore na cutafter nd;
drawarrow b--c cutbefore nb cutafter nc;
drawarrow c--a cutbefore nc cutafter na;

drawarrow d--a cutbefore nd cutafter na;
drawarrow e--b cutbefore ne cutafter nb;

drawarrow d--c cutbefore nd cutafter nc;
drawarrow c--e cutbefore nc cutafter ne;
drawarrow e--d cutbefore ne cutafter nd;

draw (b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle dashed evenly;
path p;
p = ((b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle) shifted (-20mm,0);
draw p; 

drawarrow a--(a-(20mm,0)) cutbefore na cutafter p;
drawarrow b--(b-(20mm,0)) cutbefore nb cutafter p;

label.top(btex $T$ etex,b+(0,0.5cm));
label.llft(btex 1 etex,a);
label.ulft(btex 2 etex,b);
label.bot (btex 3 etex,c);
label.lrt (btex 4 etex,d);
label.urt (btex 5 etex,e);
label.top(btex IDS etex,(a+b)/2-(20mm,0));
endfig;

beginfig(4)
r:=0.5mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b,c,d,e,f,g;
a:=(0,0mm); b:=(0,20mm);c:=(30mm,10mm);d=(60mm,0mm);e=(60mm,20mm);
path na,nb,nc,nd,ne;
na=node shifted a;
nb=node shifted b;
nc=node shifted c;
nd=node shifted d;
ne=node shifted e;
draw na withpen pencircle scaled 0.5pt;
draw nb withpen pencircle scaled 0.5pt;
draw nc withpen pencircle scaled 0.5pt withcolor red;
draw nd withpen pencircle scaled 0.5pt;
draw ne withpen pencircle scaled 0.5pt;
fill nc withcolor red;
drawarrow a--b cutbefore na cutafter nd;
drawarrow b--c cutbefore nb cutafter nc;
drawarrow c--a cutbefore nc cutafter na;

drawarrow d--a cutbefore nd cutafter na;
drawarrow e--b cutbefore ne cutafter nb;

drawarrow d--c cutbefore nd cutafter nc;
drawarrow c--e cutbefore nc cutafter ne;
drawarrow e--d cutbefore ne cutafter nd;

draw (b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle dashed evenly;

label.top(btex $T$ etex,b+(0,0.5cm));
label.llft(btex 1 etex,a);
label.ulft(btex 2 etex,b);
label.bot (btex 3 etex,c);
label.lrt (btex 4 etex,d);
label.urt (btex 5 etex,e);
endfig;

beginfig(5)
r:=0.5mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b,c,d,e,f,g;
a:=(0,0mm); b:=(0,20mm);c:=(30mm,10mm);d=(60mm,0mm);e=(60mm,20mm);
path na,nb,nc,nd,ne;
na=node shifted a;
nb=node shifted b;
nc=node shifted c;
nd=node shifted d;
ne=node shifted e;
draw na withpen pencircle scaled 0.5pt;
draw nb withpen pencircle scaled 0.5pt;
%draw nc withpen pencircle scaled 0.5pt withcolor red;
draw nd withpen pencircle scaled 0.5pt;
draw ne withpen pencircle scaled 0.5pt;
%fill nc withcolor red;
drawarrow a--b cutbefore na cutafter nd;
%drawarrow b--c cutbefore nb cutafter nc;
%drawarrow c--a cutbefore nc cutafter na;

drawarrow d--a cutbefore nd cutafter na;
drawarrow e--b cutbefore ne cutafter nb;

%drawarrow d--c cutbefore nd cutafter nc;
%drawarrow c--e cutbefore nc cutafter ne;
drawarrow e--d cutbefore ne cutafter nd;

drawarrow b..((a+b)/2+(5mm,0))..a cutbefore nb cutafter na withcolor red;
drawarrow b..((b+e)/2-(0,5mm))..e cutbefore nb cutafter ne withcolor red;
drawarrow d..((d+e)/2-(5mm,0))..e cutbefore nd cutafter ne withcolor red;

draw (b+(0.5cm,0.5cm))--(b+(0.5cm,0.5cm) rotated 90)--(a+(0.5cm,0.5cm) rotated 180)--(a+(0.5cm,0.5cm) rotated -90)--cycle dashed evenly;

label.top(btex $T$ etex,b+(0,0.5cm));
label.llft(btex 1 etex,a);
label.ulft(btex 2 etex,b);
%label.bot (btex 3 etex,c);
label.lrt (btex 4 etex,d);
label.urt (btex 5 etex,e);
endfig;

beginfig(6)
r:=2mm;
path node;
node:=(0,r)..(r,0)..(0,-r)..(-r,0)..cycle;
pair a,b;
a:=(0,0mm);b=a + (10mm,0);
path na,nb;
na=node shifted a;
nb=node shifted b;
draw na withpen pencircle scaled 0.5pt;
draw na shifted (1*b) withpen pencircle scaled 0.5pt ;
draw na shifted (2*b) withpen pencircle scaled 0.5pt;
draw na shifted (3*b) withpen pencircle scaled 0.5pt ;
draw na shifted (4*b) withpen pencircle scaled 0.5pt; 
draw na shifted (5*b) withpen pencircle scaled 0.5pt ;
draw na shifted (6*b) withpen pencircle scaled 0.5pt;

path p;
p = (a+(r,0))..(b-(r,0));
drawarrow p shifted (0*b);
drawarrow p shifted (1*b);
drawarrow p shifted (2*b);
drawarrow p shifted (3*b);
drawarrow p shifted (4*b);
drawarrow (a-(5mm,7mm))--(a+6*b+(5mm,-7mm));
label.rt(btex $x$ etex,(a+6*b+(5mm,-7mm)));

label.bot(btex $0$ etex, a+(0,-7mm) shifted (0*b));
label.bot(btex $d$ etex, a+(0,-7mm) shifted (1*b));
label.bot(btex $2d$ etex, a+(0,-7mm) shifted (2*b));
label.bot(btex $3d$ etex, a+(0,-7mm) shifted (3*b));
label.bot(btex $4d$ etex, a+(0,-7mm) shifted (4*b));
label.bot(btex $5d$ etex, a+(0,-7mm) shifted (5*b));
label.bot(btex $6d$ etex, a+(0,-7mm) shifted (6*b));

path q;
q = a-(0,6mm)..a-(0,7mm);
drawarrow q shifted (0*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (1*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (2*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (3*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (4*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (5*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (6*b) withpen pencircle scaled 0.25pt;
drawarrow p shifted (5*b);

drawarrow reverse p shifted (0*b);
drawarrow reverse p shifted (1*b);
drawarrow reverse p shifted (2*b);
drawarrow reverse p shifted (3*b);
drawarrow reverse p shifted (4*b);
drawarrow reverse p shifted (5*b);

boxit.ids(btex IDS etex);
ids.c = a+3b+(0,10mm);
drawboxed(ids);

drawarrow a..ids.c{dir 0} cutbefore na cutafter bpath.ids; 
drawarrow (a+3*b)..ids.c cutbefore (na shifted(3*b)) cutafter bpath.ids; 
drawarrow (a+6*b)..ids.c{dir 180} cutbefore (na shifted(6*b)) cutafter bpath.ids; 

label(btex 1 etex, a shifted (0*b));
label(btex 2 etex, a shifted (1*b));
label(btex 3 etex, a shifted (2*b));
label(btex 4 etex, a shifted (3*b));
label(btex 5 etex, a shifted (4*b));
label(btex 6 etex, a shifted (5*b));
label(btex 7 etex, a shifted (6*b));

path pp;
pp = a..(2*b){dir 40} cutbefore na cutafter (na shifted (2*b));

drawarrow pp;
drawarrow pp shifted b;
drawarrow pp shifted (2*b);
drawarrow pp shifted (3*b);
drawarrow pp shifted (4*b);

drawarrow reverse pp;
drawarrow reverse pp shifted b;
drawarrow reverse pp shifted (2*b);
drawarrow reverse pp shifted (3*b);
drawarrow reverse pp shifted (4*b);

endfig;

drawarrow (a-(5mm,7mm))--(a+6*b+(5mm,-7mm));
label.rt(btex $x$ etex,(a+6*b+(5mm,-7mm)));

label.bot(btex $0$ etex, a+(0,-7mm) shifted (0*b));
label.bot(btex $d$ etex, a+(0,-7mm) shifted (1*b));
label.bot(btex $2d$ etex, a+(0,-7mm) shifted (2*b));
label.bot(btex $3d$ etex, a+(0,-7mm) shifted (3*b));
label.bot(btex $4d$ etex, a+(0,-7mm) shifted (4*b));
label.bot(btex $5d$ etex, a+(0,-7mm) shifted (5*b));
label.bot(btex $6d$ etex, a+(0,-7mm) shifted (6*b));

path q;
q = a-(0,6mm)..a-(0,7mm);
drawarrow q shifted (0*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (1*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (2*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (3*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (4*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (5*b) withpen pencircle scaled 0.25pt;
drawarrow q shifted (6*b) withpen pencircle scaled 0.25pt;
