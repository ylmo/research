clc
clear
rand('twister',sum(clock)*100)
%initialization
n = 3;
m = 2;
p = 2;
A = rand(n);
B = rand(n,p);
C = rand(m,n);
W = eye(n);
U = eye(p);
Q = eye(n);
R = eye(m);
P = 0;
S = 0;

for i = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;
cP = C*P*C' + R;

invcP = inv(cP);

T = 10;
W = 40*eye(n);
Lambda = diag([0,1]);

cA = [A+B*L -B*L;zeros(n,n) A-K*C*A];
cB = [zeros(n,m);-K*Lambda];
S1(:,:,T) = [W zeros(n);zeros(n) zeros(n)];
S2(:,:,T) = W;
H(:,:,T) = [-W zeros(n)];

for index=T:-1:2
    tS1 = S1(:,:,index);
    tS2 = S2(:,:,index);
    tH = H(:,:,index);
    tmp1 = [zeros(m,n) Lambda * invcP *C*A] + cB' * tS1*cA;
    tmp2 = pinv(Lambda*invcP*Lambda + cB' * tS1* cB);
    S1(:,:,index - 1) = cA' * tS1 * cA + [zeros(n) zeros(n);zeros(n) A'*C'*invcP*C*A] - tmp1'*tmp2*tmp1;
    H(:,:,index - 1) = tH*cA - tH*cB*tmp2*tmp1;
    S2(:,:,index - 1) = tS2 - tH*cB*tmp2*cB'*tH';
end

[tmpV tmpD] = eig(S2(:,:,1));
for i = 1:n
    tmpDD(i) = tmpD(i,i);
end
[tmpD index] = min(tmpDD);

xstar=10*tmpV(:,index);

    tS1 = S1(:,:,1);
    tS2 = S2(:,:,1);
    tH = H(:,:,1);
    tmp1 = [zeros(m,n) Lambda * invcP *C*A] + cB' * tS1*cA;
    tmp2 = pinv(Lambda*invcP*Lambda + cB' * tS1* cB);   
    ya(:,1) = tmp2*cB'*tH'*xstar;
    v(:,1) = cB*ya(:,1);
    residuea(:,1) = Lambda*ya(:,1);
for index = 2:T
    tS1 = S1(:,:,index);
    tS2 = S2(:,:,index);
    tH = H(:,:,index);
    tmp1 = [zeros(m,n) Lambda * invcP *C*A] + cB' * tS1*cA;
    tmp2 = pinv(Lambda*invcP*Lambda + cB' * tS1* cB);   
    ya(:,index) = -tmp2*(cB'*tH'*xstar+tmp1*v(:,index-1));
    v(:,index) = cA*v(:,index-1) + cB * ya(:,index);
    residuea(:,index) = [zeros(m,n) C*A]*v(:,index-1)+Lambda*ya(:,index);
end
eig(S2(:,:,1))

for index = 1:3000
    x = sqrtm(P) * randn(n,1);
    hatx1 = zeros(n,1);%prediction
    for i = 1:10
        y(:,i) = C*x(:,i) + sqrtm(R) * randn(m,1);
        residue(:,i,index) = y(:,i)+ya(:,i) - C * hatx1(:,i);
        hatx2(:,i) = hatx1(:,i) + K * residue(:,i,index);
        u(:,i) = L*hatx2(:,i); 
        hatx1(:,i+1) = A*hatx2(:,i) + B*u(:,i) ;
        x(:,i+1) = A*x(:,i) + B*u(:,i)+sqrtm(Q)*randn(n,1);
    end
end

%chi-square detector
cT = 1;
thre = chi2inv(0.99,cT*m);
for i = 1:10
    for j = 1:3000
        g(i,j) = (residue(:,i,j)'*invcP*residue(:,i,j) > thre);
    end
end
beta = sum(g,2)/3000;
hold on
plot(1:10,beta,'b');
