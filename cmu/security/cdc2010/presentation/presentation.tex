\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\mode<presentation>

\title[False Data Injection]{False Data Injection Attacks against State Estimation in Wireless Sensor Networks}
\author[Yilin Mo]{Yilin Mo, Emanuele Garone, Alessandro Casavola, Bruno Sinopoli}
\institute[Carnegie Mellon University]{Department of Electrical and Computer Engineering, Carnegie Mellon University\\ Dipartimento di Elettronica, Informatica e Sistemistica, Universit\`{a} degli Studi della Calabria}
\date[2010 CDC]{2010 IEEE Conference on Decision and Control}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Control Systems}
  \begin{itemize}
    \item Wireless Sensor Networks (WSNs) are becoming ubiquitous.
    \item Typical applications include environmental monitoring and control, health care, home and office automation and traffic control.
    \item Lots of them are \alert{safety-critical}.
    \item Sensors are usually cheap, distributed in space and physically exposed, which makes it difficult to ensure security and availability for every single sensor.
    \item Our goal: analyze the performance of state estimation algorithm in the presence of compromised sensors.
  \end{itemize}
\end{frame}

\section{System Description}

\begin{frame}{System Model}
  We consider the control system is monitoring the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{equation}
	\begin{split}
	  x_{k+1} &= Ax_k  + w_k,\\
	  y_{k} &= C x_k + v_k.
	\end{split}
	\label{eq:systemdiscription}
      \end{equation}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$.
      \item The system is assumed to be stable and observable.
    \end{itemize}
  \end{frame}

  \begin{frame}{Kalman Filter and $\chi^2$ Failure Detector}
	  \begin{block}{Kalman filter}
		  The Kalman filter follows the following update equations:
		  \begin{displaymath}
			  \hat x_0 =\bar x_0,\,\hat x_{k+1} = A\hat x_k + K(y_{k+1} - CA\hat x_k). 
		  \end{displaymath}
		  The estimation error $e_k$ and innovation $z_k$ is defined as
		  \begin{displaymath}
			  e_k = x_k - \hat x_k,\,z_k = y_k - CA\hat x_{k-1}.
		  \end{displaymath}
	  \end{block}
	  \begin{block}{$\chi^2$ Detector}
		  The innovation of Kalman filter $z_k$ is i.i.d. Gaussian distributed with zero mean. The $\chi^2$ detector triggers an alarm based on the following event:
		  \begin{displaymath}
			  g_k= z_k^T\mathcal P^{-1}z_k> threshold.
		  \end{displaymath}
		  The probability of false alarm is defined as
		  \begin{displaymath}
			  \beta_k = P(g_k > threshold).
		  \end{displaymath}
	  \end{block}
  \end{frame}

\section{False Data Injection Attack}

\begin{frame}{Attack Model}
  We assume the following:
  \begin{enumerate}
    \item The attacker knows matrices $A,\,C,\,K$.
    \item The attacker can control the readings of a subset of sensors. Hence, the measurement received by the Kalman filter can be written as
      \begin{displaymath}
	y'_k = Cx_k' + v_k + \Gamma y_k^a,	
      \end{displaymath}
      where $y_k^a$ is the bias introduced by the attacker, $\Gamma = diag(\gamma_1,\ldots,\gamma_m)$ is the sensor selection matrix. $\gamma_i = 1$ if the attacker can control the readings of sensor $i$. $\gamma_i = 0$ otherwise. 
    \item The attack begins at time $1$.
    \item The sequence of attacker's inputs $(y_0^a,\ldots,y_k^a)$ is chosen before the attack. Hence, $y_k^a$ is independent of $w_k,\,v_k$.
  \end{enumerate}
\end{frame}

\begin{frame}{Healthy System v.s. Compromised System}
    \begin{minipage}[t]{0.45\textwidth}
      \begin{block}{Healthy System}
	\begin{align*}
	  x_{k+1} &= Ax_k +  w_k\\
	  y_k & = Cx_k + v_k \\
	  z_{k+1} & = y_{k+1} - CA\hat x_{k}\\
	  \hat x_{k+1} & = A\hat x_k + Kz_{k+1}\\
	  g_k &= z_k^T\mathcal P^{-1} z_k\\
	  \beta_k &= P(g_k > threshold)
	\end{align*}
      \end{block}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}[t]{0.45\textwidth}
      \begin{block}{Compromised System}
	\begin{align*}
	  x_{k+1} &= Ax_k +  w_k\\
	  y'_k & = Cx_k + v_k+\Gamma y_k^a \\
	  z'_{k+1} & = y'_{k+1} - CA\hat x'_{k}\\
	  \hat x'_{k+1} & = A\hat x'_k + Kz'_{k+1}\\
	  g'_k &= (z'_k)^T\mathcal P^{-1} z'_k\\
	  \beta'_k &= P(g'_k > threshold)
	\end{align*}
      \end{block}
    \end{minipage}
\end{frame}

\begin{frame}{Attack Sequence}
	\begin{definition}
		An attack sequence $\mathcal Y$ is an infinite sequence $\mathcal Y= (y_1^a,\,y_2^a,\ldots)$.
	\end{definition}
	Ideally to make the attack bypass the failure detector, the attacker need to make $\Delta \beta_k = \beta'_k - \beta_k$ small. However the probability is hard to calculate. As a result, we will use the following definition. 
  \begin{definition}
    An attack sequence is called $(T,\alpha)$-feasible if during the attack,
    \begin{displaymath}
	    \sqrt{D(z'_k\|z_k)} = \sqrt{\Delta z_{k}^T \mathcal P^{-1}\Delta z_k/2} = \|\Delta z_k\|_S \leq \alpha,\,\textrm{for } k=1,\ldots,T,
    \end{displaymath}
    where $D(z'_k\|z_k)$ is the KL distance between $z'_k$ and $z_k$, $\Delta z_k = z'_k - z_k$ and $S = \mathcal P^{-1}/2$. 
  \end{definition}
\end{frame}

\begin{frame}{Feasible Attack}
	\begin{theorem}
		For any $\varepsilon > 0$, there exists $\alpha > 0$, independent of $T$, such that if
		\begin{displaymath}
			\|\Delta z_k\|_S \leq \alpha,\,
		\end{displaymath}
		for all $k$ from $1$ to $T$, then
		\begin{displaymath}
			\beta'_k \leq \beta_k + \varepsilon,
		\end{displaymath}
		for all $k$ from  $1$ to $T$.
	\end{theorem}
\end{frame}

\begin{frame}{Constrained Control Problem}
	We are interested in $\Delta \hat x_k = \hat x'_k - \hat x_k$. It is easy to prove that it follows the following recursive equations:
	\begin{displaymath}
		\begin{split}
			\Delta \hat x_{k+1} &= (A - KCA)\Delta \hat x_k + K\Gamma y_{k+1}^a,\\
			\Delta z_k &=   y_k^a-CA\Delta \hat x_{k-1}.
		\end{split}
	\end{displaymath}
	To characterize the performance of the state estimator we need to study the reachable region of  $\Delta \hat x_k$.  
\end{frame}

\begin{frame}{Reachable Region}
	\begin{definition}
		The $(T,\alpha)$-reachable region of $\Delta \hat x_k$ is defined as follows
		\begin{equation}
			R_{T,\alpha} \triangleq \{x\in \mathbb R^n:x = \Delta \hat x_T(\mathcal Y),\, \mathcal Y \textrm{ is } (T,\alpha) \textrm{-feasible}\}
		\end{equation}
		and the $\alpha$-reachable region as
		\begin{equation}
			\mathcal R_{\alpha} \triangleq \bigcup_{T=1}^\infty R_{T,\alpha}.
		\end{equation}
		Moreover we define $R_T = R_{T,1}$ and $\mathcal R = \mathcal R_1$.
	\end{definition}
	Due to linearity, we will only focus on $R_T$ and $\mathcal R$.
\end{frame}

\begin{frame}{Recursive Definition of $R_k$}
	\begin{theorem}
		Given $R_{k-1}$, $R_k$ can be defined recursively as
		\begin{equation}
			\begin{split}
				R_k &= \{x' \in \mathbb R^n:\, \exists x\in R_{k-1},\, \exists y\in \mathbb R^{m}, \\
				x' &= (A- KCA)x+K\Gamma y,\, \|-CAx+\Gamma y\|_S \leq 1\}.
			\end{split}
		\end{equation}
	\end{theorem}
	However, the equations needed to describe $R_k$ quickly explodes as $k$ increases. Hence, it is computational infeasible to get the exact shape of $R_k$ and $\mathcal R$ even for a small system. As a result, we will focus on an ellipsoidal inner and outer approximation of $R_k$ and $\mathcal R$.
\end{frame}

\begin{frame}{Approximation}
	We will assume that:
	\begin{enumerate}
		\item The pair $(A-KCA, K\Gamma)$ is controllable.
		\item $(A-KCA)$ is invertible.
	\end{enumerate}
Define $\mathcal E(X)$
    \begin{equation}
      \mathcal E(X) = \{v\in\mathbb R^n:v^TXv\leq 1\}.
    \end{equation}
$R_k$ can then be evaluated recursively as
  \begin{equation}
    \begin{split}
      R_k &= \{x \in \mathbb R^n:\exists y\in \mathbb R^m, \\
      &\tilde Ax + \tilde B y \in R_{k-1}, \tilde Cx+\tilde Dy\in \mathcal E(S)\,\},
    \end{split}
  \end{equation}
where $\tilde A,\,\tilde B,\,\tilde C,\,\tilde D$ are defined as
\begin{displaymath}
  \begin{split}
    \tilde A &\triangleq (A - KCA)^{-\!1},\,\tilde B \triangleq -(A - KCA)^{-\!1} K\Gamma,\\
    \tilde C &\triangleq -CA(A - KCA)^{-\!1},\,\tilde D \triangleq [CA(A - KCA)^{-\!1}K\!+\!I_m]\Gamma.
  \end{split}
\end{displaymath}
\end{frame}

\begin{frame}{Outer Approximation}
	Let $\overline R_k = \mathcal E(U_k)$ to be the outer approximation. Then $\overline R_k$ can be defined as
  \begin{equation}
    \begin{split}
      R_k &=Out[\{x \in \mathbb R^n:\exists y\in \mathbb R^m, \\
      &\tilde Ax + \tilde B y \in \mathcal E(U_{k-1}), \tilde Cx+\tilde Dy\in \mathcal E(S)\,\}],
    \end{split}
  \end{equation}
  where $Out[\cdot]$ means outer approximation, which is divided into three steps.
  \begin{itemize}
	  \item Extend the space from $\mathbb R^n$ to $\mathcal R^{n+m}$.
	  \item Find the ellipsoidal outer approximation in $\mathcal R^{n+m}$.
	  \item Project back to $\mathbb R^n$.
  \end{itemize}
\end{frame}

\begin{frame}{Outer Approximation}
The condition 
\begin{displaymath}
      \tilde Ax + \tilde B y \in \mathcal E(U_{k-1}), \tilde Cx+\tilde Dy\in \mathcal E(S),
\end{displaymath}
is equivalent to 
\begin{displaymath}
  \begin{split}
  \left[ {\begin{array}{*{20}c}
  x\\
  y
\end{array}} \right]\in&\left\{[x^T, y^T] \left[ {\begin{array}{*{20}c}
  \tilde A^T U_k \tilde A & \tilde A^T U_k \tilde B  \\
  \tilde B^T U_k \tilde A & \tilde B^T U_k \tilde B  \\
\end{array}} \right]\left[ {\begin{array}{*{20}c}
  x\\
  y
\end{array}} \right]\leq 1\right\}\\
\bigcap
  &\left\{[x^T, y^T] \left[ {\begin{array}{*{20}c}
  \tilde C^T S \tilde C & \tilde C^T S \tilde D  \\
  \tilde D^T S \tilde C & \tilde D^T S \tilde D  \\
\end{array}} \right]\left[ {\begin{array}{*{20}c}
  x\\
  y
\end{array}} \right]\leq 1\right\}.
  \end{split}
\end{displaymath}
Next we need to find an outer approximation of the above intersections.
\end{frame}

\begin{frame}{Outer Approximation}

\begin{theorem}
  The minimum volume ellipsoidal outer approximation of the region
  \begin{displaymath}
    \{x^T S_1 x \leq 1\} \bigcap \{x^T S_2 x \leq 1\} ,
  \end{displaymath}
  where $S_1,\,S_2$ are positive semidefinite, is
  \begin{displaymath}
    \{x^T S_3 x \leq 1\},
  \end{displaymath}
  where $S_3$ is the solution of the following convex optimization problem:
  \begin{equation}
    \begin{split}
    \max_\alpha\qquad &logdet(S_3) \\
    &S_3 = \alpha S_1 + \beta S_2\\
    &\alpha+ \beta = 1,\,\alpha,\,\beta \geq 0.
    \end{split}
    \label{eq:optimalouter}
  \end{equation}
  \label{theorem:optouter}
\end{theorem}
\end{frame}

\begin{frame}{Outer Approximation}
\begin{theorem}
  \begin{equation}
    \begin{split}
    &Proj_x  \left\{[x^T, y^T] \left[ {\begin{array}{*{20}c}
      S_1 &S_2\\
      S_2^T &S_3
\end{array}} \right]\left[ {\begin{array}{*{20}c}
  x\\
  y
\end{array}} \right]\leq 1\right\} = \{x^T(S_1 - S_2 S_3^{+} S_2^T)x\leq 1\},
    \end{split}
  \end{equation}
  where $ \left[ {\begin{array}{*{20}c}
      S_1 &S_2\\
      S_2^T &S_3
    \end{array}} \right]$ is positive semidefinite and $S_3^+$ is the Moore-Penrose inverse.
\end{theorem}
In conclusion, $U_k$ follows the following recursive equation: 
\begin{equation}
  \begin{split}
  U_{k+1}& \!= \!\alpha_k \tilde A^T U_k \tilde A \!+\! \beta_k  \tilde C^T S \tilde C \!-\!(\alpha_k \tilde A^T U_k \tilde B \!+\! \beta_k  \tilde C^T S \tilde D)\\
  &\!\times \!(\alpha_k \tilde B^T U_k \tilde B \!+\! \beta_k  \tilde D^T S \tilde D )^{+}(\alpha_k \tilde A^T U_k \tilde B \!+\! \beta_k  \tilde C^T S \tilde D)^T.
  \end{split}
\end{equation}
\end{frame}

\section{Simulation}

\begin{frame}{Illustrative Example}
We consider a vehicle moving along the $x$-axis, which is monitored by a position sensor and velocity sensor. 
\begin{block}{System Description}
\begin{displaymath}
  \begin{split}
    \left[ {\begin{array}{*{20}c}
      \dot x_{k+1} \\
      x_{k+1} \\
\end{array}} \right] &= \left[ {\begin{array}{*{20}c}
   1 & 0  \\
   1 & 1  \\
\end{array}} \right]  \left[ {\begin{array}{*{20}c}
   \dot x_k  \\
   x_k  \\
\end{array}} \right] + w_k,\\
 y_{k,1} &= \dot x_k + v_{k,1},\\
 y_{k,1} &=  x_k + v_{k,2}.
  \end{split}
\end{displaymath}
\end{block}
We assume that $Q = R = W = I_2$, $U = 1$. The Kalman gain and LQG control gain are
\begin{displaymath}
  K =  \left[ {\begin{array}{*{20}c}
   0.5939&0.0793  \\
   0.0793&0.6944  \\
\end{array}} \right]
\end{displaymath}
\end{frame}

\begin{frame}{Position Sensor is Compromised}
   \begin{figure}
    \begin{center}
	\includegraphics[width = 0.49 \linewidth]{figure3.eps}
	\includegraphics[width = 0.49 \linewidth]{figure2.eps}
    \end{center}
    \caption{Inner and Outer Approximation of Reachable Region $R_k$}
  \end{figure} 
\end{frame}

\begin{frame}{Velocity Sensor is Compromised}
   \begin{figure}
    \begin{center}
      \includegraphics[width = 0.6 \textwidth]{figure1.eps}
    \end{center}
    \caption{Inner and Outer Approximation of Reachable Region $\bigcup_{k=1}^\infty R_k$}
  \end{figure} 
\end{frame}
\section{Conclusion}

\begin{frame}{Conclusion}
  In this presentation, we consider the false data injection attacks in control systems.
  \begin{itemize}
    \item We define the false data injection attack model.
    \item We formulate the action of the attacker as a constrained control problem.
    \item We analyze the performance of state estimation algorithms in the presence of compromised sensors. 
  \end{itemize}
\end{frame}

\end{document}

