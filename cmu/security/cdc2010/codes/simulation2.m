clc
clear
A = [1 0;1 1];
B = [1;0];
C = eye(2);
Q = eye(2);
R = eye(2);
W = eye(2);
U = 1;
n = 2;
m = 2;
P = 0;
for i = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
end
K = P*C'*inv(C*P*C'+R);

cP = C*P*C' + R;
S = inv(cP);

Gamma = [0;1];

U = {diag([10000,10000])};
L = {diag([100,1000])};

tA = inv(A-K*C*A);
tB = tA*K*Gamma;
tC = C*A*tA;
tD = (C*A*tA*K + eye(m))*Gamma;

for i = 1:9
    tmpU = U{i};
    S1 = [tA';tB']*tmpU*[tA tB];
    S2 = [tC';tD']*S*[tC tD];
    
%     cvx_begin
%     variable a;
%     variable b;
%     maximize (log_det(a*S1+b*S2))
%     subject to
%     a >= 0;
%     b >= 0;
%     a+b == 1;
%     cvx_end
    
    S3 = S1/2+S2/2;
    tmp = inv(S3);
    U{i+1} = inv(tmp(1:n,1:n));
    
%     tmpL = L{i};
%     S1 = [tA';tB']*tmpL*[tA tB];
%     S2 = [tC';tD']*S*[tC tD];
%     
%     [V D] = eig(S1,S2);
%     Lambda1 = V'*S1*V;
%     Lambda2 = V'*S2*V;
%     Lambda3 = max(Lambda1,Lambda2);
%     S3 = inv(V')*Lambda3*inv(V);
% 
%     tmp = inv(S3);
%     L{i+1} = inv(tmp(1:n,1:n));
end

%plot eclipse

hold on

theta = linspace(0,2*pi,200);

for i=1:9
    S = U{i};
    [V D] = eig(S);
    x = V(1,1) *sin(theta)/sqrt(D(1,1)) + V(1,2) *cos(theta)/sqrt(D(2,2));
    y = V(2,1) *sin(theta)/sqrt(D(1,1)) + V(2,2) *cos(theta)/sqrt(D(2,2));
    plot(x,y,'b--');
end
plot(x,y,'b--','linewidth',2);


    xlabel('$\Delta e_{k,1}$','fontsize',12)
    ylabel('$\Delta e_{k,2}$','fontsize',12)    
    
set(gca,'Fontsize',12);
set(get(gca,'XLabel'),'interpreter','latex');
set(get(gca,'YLabel'),'interpreter','latex');

hold off


