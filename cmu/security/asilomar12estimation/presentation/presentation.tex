\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{subfigure}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{tikz}

\newcommand{\MSE}{\text{\bf{MSE}}}
\newcommand{\Med}{\text{\bf{Med}}}
\newcommand{\Min}{\text{\bf{Min}}}
\newcommand{\Max}{\text{\bf{Max}}}
%for handout
\mode<presentation>

\title[Secure Estimation]{Secure Estimation in Cyber-Physical Systems}
\author[Yilin Mo]{Yilin Mo and Bruno Sinopoli}
\institute[CMU]{
Department of Electrical and Computer Engineering\\ Carnegie Mellon University\\
}
\date[Asilomar]{Asilomar Conference, 2012}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Cyber Physical Systems (CPSs) refer to the embedding of widespread sensing, computation, communication and control into physical spaces.
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. \alert{Safety Critical}
    \item The next generation CPS, such as smart grids, will make extensive use of information technology, which opens the possibility of cyber attacks on CPS.  
    \item The virus Stuxnet raised significant concerns on CPS security. 
    \item How to design secure CPS?
  \end{itemize}
\end{frame}

\begin{frame}{A Classical Estimation Problem}
  \begin{itemize}
    \item The goal is to estimate the state $x\in \mathbb R$.
    \item $m$ sensors are measuring the system. The measurement of sensor $i$ is denoted as $y_i\in \mathbb R$. We further define $y\in \mathbb R^m$ as the vector of all sensor measurements:
      \begin{displaymath}
	y\triangleq \left[ y_1,\dots,y_m \right]^T
      \end{displaymath}
    \item We assume that the probability of $(x,y)\in S$ is given by
      \begin{displaymath}
	P\left( (x,y)\in S \right) = \mu(S),
      \end{displaymath}
      where $\mu$ is a probability measure and $S$ is any Borel-measurable set.
    \item An estimator is a function $f:\mathbb R^m\rightarrow \mathbb R$.
    \item The optimal detector with minimum Mean Squared Error (MSE) is:
      \begin{align*}
	\hat x=f(y)=\mathbb E(x|y).
      \end{align*}
  \end{itemize}
\end{frame}

\begin{frame}{An Estimation Problem: the Gaussian Case}
  \begin{itemize}
    \item Assume that 
      \begin{displaymath}
	y_i = x + v_i,
      \end{displaymath}
      where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$.
    \item The optimal estimator (in the MSE sense)
      \begin{displaymath}
	\hat x=	f(y) = \frac{1}{m+1}\sum_{i=1}^m y_i.
      \end{displaymath}
    \item Suppose one sensor measurement is compromised by an adversary. Then the adversary can change $\hat x$ to an arbitrary value.
    \item \alert{ Not Secure!}
  \end{itemize}
\end{frame}

\begin{frame}{Countermeasures}
  \begin{block}{Information Security}
    \begin{itemize}
      \item Tamper-resistant microprocessor, Software attestation, Secure Communication Protocol,\dots
      \item It is hard to guarantee security for every single sensor. (A single compromised sensor can totally ruin the state estimation.)
      \item Physical attacks?
    \end{itemize}
  \end{block}
  \begin{block}{System Theory}
    \begin{itemize}
      \item Bad data detection, Robust estimation   
      \item The uncertainty models of system theoretic approaches are usually not quite different from the cyber attacks
    \end{itemize}
  \end{block}
  Our goal: To design the optimal estimator that can withstand Byzantine attacks on at most $l$ sensors.
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the estimation algorithm $f$ (Kerckhoffs' Principle);
    \item the true state $x$;
    \item all measurements $y$.
  \end{itemize}
  The attacker can manipulate up to $l$ measurements arbitrarily.
  \begin{displaymath}
    y^c = y + \gamma\circ y^a,
  \end{displaymath}
  where $\circ$ is element-wise multiplication and the \emph{sensor-selection} vector $\gamma$ taking values in 
  \begin{displaymath}
    S_\gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i = l\}
  \end{displaymath}
  and the \emph{bias} vector $y^a$ taking values in $\mathbb R^m$.
\end{frame}

\begin{frame}{The Optimal Attack Policy}
     To compute the worst-case MSE, let us define
      \begin{align*}
	f^+(y) \triangleq \sup_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a),\, f^-(y) \triangleq \inf_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a).
      \end{align*}
      Suppose that the true measurement is $y$, then the attack can only change the state estimation $\hat x = f(y^c)$ in the interval $[f^-(y),f^+(y)]$. 

     The optimal attack policy can be formulated as follows: 
      \begin{itemize}
	\item The attacker computes $f^+(y)$ and $f^-(y)$ and compare them with $x$.
	\item If $(f^+(y)-x)^2\geq (f^-(y)-x)^2$, then the attacker chooses $\gamma$ and $y^a$ such that $f(y+\gamma\circ y^a)$ equals $f^+(y)$ (or as close as possible)
	\item Otherwise, the attacker chooses $\gamma$ and $y^a$ such that $f(y+\gamma\circ y^a)$ equals $f^-(y)$ (or as close as possible).
      \end{itemize}
\end{frame}

\begin{frame}{The Worst-Case MSE}
  Define 
  \begin{displaymath}
   e(x,y) \triangleq \max \left( (f^+(y)-x)^2,(f^-(y)-x)^2 \right). 
  \end{displaymath}
  The worst-case MSE is define as
  \begin{displaymath}
    \MSE(f) \triangleq \int_{\mathbb R^{m+1}}e(x,y)d\mu = \mathbb Ee(x,y). 
  \end{displaymath}
  \begin{theorem}
    For two estimators $f_1$ and $f_2$, if the following inequalities hold
    \begin{displaymath}
      f^+_1 \geq f^+_2\geq f^-_2\geq f^-_1, 
    \end{displaymath}
    then
    \begin{displaymath}
      \MSE(f_1)\geq \MSE(f_2). 
    \end{displaymath}
  \end{theorem}
  To find the optimal $f$, we should try to make $f^+$ and $f^-$ as close as possible.
\end{frame}

\section{Main Results}
\begin{frame}{Optimal Estimator: $l\geq m/2$}
  \begin{theorem}
  If $l\geq m/2$, then the optimal estimator $f^*$ is given as $f^* = \mathbb Ex$.
  \end{theorem}
  If the attacker compromises half of the sensors, then the optimal estimator will depend only on the a-prior information.
  \begin{proof}[Sketch of the Proof]
    \begin{enumerate}
      \item Let $\mathcal I\subseteq\{1,\dots,m\}$ be an index set.
      \item Define $\gamma_{\mathcal I} \triangleq [\gamma_1,\dots,\gamma_m]^T$ such that $\gamma_i = 1$ if $i\in \mathcal I$ and $\gamma_i = 0$ otherwise.
      \item Define
	\begin{displaymath}
	  f^+_{\mathcal I}(y) \triangleq \sup_{y^a \in \mathbb R^m} f(y+\gamma_{\mathcal I}\circ y^a),\, f^-_{\mathcal I}(y) \triangleq \inf_{y^a \in \mathbb R^m} f(y+\gamma_{\mathcal I}\circ y^a).
	\end{displaymath}
	If the attacker compromises sensors in $\mathcal I$, then the maximum and minimum state estimation it can enforce will be $f^+_{\mathcal I}(y)$ and $f^-_{\mathcal I}(y)$.
    \end{enumerate}
  \end{proof}
\end{frame}

\begin{frame}{Optimal Estimator: $l\geq m/2$}
  \begin{proof}[Sketch of the Proof Continued]
    \begin{enumerate}
	\setcounter{enumi}{3}
      \item We only consider $m = 2,l =1$. 
      \item Pick two arbitrary measurements $y = [y_1,y_2]^T$ and $y' = [y_1',y_2']^T$. The following inequality holds
	\begin{displaymath}
	  f^+_{\{1\}}(y_1,y_2)=\sup_{\alpha}f(\alpha,y_2)\geq f(y_1',y_2) \geq \inf_{\beta}f(y_1',\beta)=f^-_{\{2\}}(y_1',y_2')
	\end{displaymath}
	\begin{figure}[h]
	  \begin{center}
	    \begin{tikzpicture}
	      \draw[->,very thick] (-5,0)--(5,0) node [anchor = north]{$y_1$};
	      \draw[->,very thick] (0,-1.8)--(0,1.8) node [anchor = west]{$y_2$};
	      \draw[thick,red] (-5,1)--(5,1);
	      \draw[thick,blue] (2,-1.8)--(2,1.8);

	      \filldraw [gray] (-2,1) circle (2pt) node [anchor=south]{$(y_1,y_2)$};
	      \filldraw [gray] (2,-1) circle (2pt) node [anchor=west]{$(y_1',y_2')$};
	      \filldraw [gray] (2,1) circle (2pt) node [anchor=south west]{$(y_1',y_2)$};
	    \end{tikzpicture}
	  \end{center}
	\end{figure}
   \end{enumerate}
  \end{proof}
\end{frame}

\begin{frame}{Optimal Estimator: $l\geq m/2$}
  \begin{proof}[Sketch of the Proof Continued]
    \begin{enumerate}
	\setcounter{enumi}{5}
      \item Therefore, there exists a constant $C_1$, such that for all $y$: 
	\begin{displaymath}
	  f^+_{\{1\}}(y)\geq C_1, f^-_{\{2\}}(y)\leq C_1.
	\end{displaymath}
      \item Similarly, there exists another constant $C$, such that for all $y$:
	\begin{displaymath}
	  f^+_{\{2\}}(y)\geq C_2, f^-_{\{1\}}(y)\leq C_2.
	\end{displaymath}
      \item Thus, $f^+\geq \max(C_1,C_2)$ and $f^-\leq \min (C_1,C_2)$.
      \item Let $\min (C_1,C_2)\leq C\leq \max (C_1,C_2)$, then the constant estimator $g(y) = C$ is better than $f$.
      \item The optimal constant estimator is $f^*(y) = \mathbb Ex$.
    \end{enumerate}
  \end{proof}
\end{frame}

\begin{frame}{Optimal Estimator: $l< m/2$}
    We consider the case where less than half of the sensors are compromised.

    \begin{definition}[Symmetry]
      An estimator $f$ is symmetric if for any permutation $({i_1},\dots,{i_m})$ of $(1,\dots,m)$ and any $y = [y_1,\dots,y_m]^T$, the following equality holds:
      \begin{displaymath}
	f(y_1,\dots,y_m) = f(y_{i_1},\dots,y_{i_m}).
      \end{displaymath}
    \end{definition}
    \begin{definition}[Monotone]
    An estimator $f$ is monotonically increasing if for any $y = [y_1,\dots,y_m]^T$ and $y'= [y_1',\dots,y_m']^T$ such that $y_i\geq y_i'$, for $i = 1,\dots,m$, the following inequality holds:
    \begin{displaymath}
      f(y_1,\dots,y_m) \geq f(y_{1}',\dots,y_{m}').
    \end{displaymath}
    An estimator $f$ is monotone if either $f$ is monotonically increasing or $-f$ is monotonically increasing.
  \end{definition}
  \end{frame}


  \begin{frame}{Optimal Estimator: $l< m/2$}
    \begin{definition}
      Define the function $\Med_{l}:\mathbb R^{m}\rightarrow \mathbb R^{m-2l}$ as a symmetric function, which satisfies
      \begin{align}
	\Med_l(y_1,\ldots,y_m) &\triangleq (y_{l+1},\ldots,y_{m-l}),
      \end{align}
      when $y_1\leq \dots\leq y_m$.
    \end{definition}
    \begin{itemize}
      \item Since we assume that $\Med_l$ is symmetric, we only need to define it when $y_1\leq \dots\leq y_m$.
    \item The $\Med_l$ function can be seen as removing the largest $l$ measurements and the smallest $l$ measurements. In particular, if $m =2l+1$, then $\Med_l(y)$ is just the median of $y$. 
    \end{itemize}
  \end{frame}

  \begin{frame}{Optimal Estimator: $l< m/2$}
    \begin{theorem}
      The optimal symmetric and monotone estimator $f$ is of the following form
      \begin{displaymath}
	f(y) =  \varphi(\Med_l(y)),
      \end{displaymath}
      where $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ is a symmetric and monotone function.
    \end{theorem}
  \end{frame}

  \begin{frame}{Optimal Estimator: $l< m/2$}
      Let $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ be an arbitrary symmetric function and $\{i_1,\dots,i_{m-2l}\} = \{1,\dots,m\}\backslash \mathcal I$. Define function $\varphi_{\mathcal I}:\mathbb R^m\rightarrow \mathbb R$ to be
      \begin{displaymath}
	\varphi_{\mathcal I}(y)\triangleq \varphi(y_{i_1},\dots,y_{i_{m-2l}}).
      \end{displaymath}
    \begin{theorem}
      The optimal symmetric estimator $f$ is of the following form
      \begin{displaymath}
	f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],
      \end{displaymath}
      where $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ is a symmetric function.
    \end{theorem}
   If $f$ is monotone, then the optimal estimator ignores $l$ largest and $l$ smallest measurements. For $f$ which is not necessarily monotone, $\{y_i:i\in \mathcal I\}$ can be seen as the $l$ ``largest'' measurements, while $\{y_j:j\in \mathcal J\}$ can be seen as the $l$ ``smallest'' measurement.
  \end{frame}

  \begin{frame}{Numerical Example: the Gaussian Case}
    \begin{itemize}
      \item We specialize our results for the Gaussian case, with $m = 3$ and $l = 1$.
      \item We assume
	\begin{displaymath}
	 y_i = x+ v_i,\,i=1,2,3, 
	\end{displaymath}
	where $x$ and $v_i$ are i.i.d. Gaussian with mean $0$ and variance $1$.
      \item The optimal symmetric estimator is
      \begin{displaymath}
	f(y) = \min_{|\mathcal I|=1}\left[\max_{|\mathcal J| = 1,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right] = \Med_1(\varphi(y_1),\varphi(y_2),\varphi(y_3)).
      \end{displaymath}
    \item The worst-case MSE is
      \begin{displaymath}
	\MSE(f) = \int_{\mathbb R^4}e(x,y)d\mu = \mathbb E\left[\max_{i=1,2,3}\; (\varphi(y_i)-x)^2\right]
      \end{displaymath}
    \end{itemize}
  \end{frame}


  \begin{frame}{Numerical Example: the Gaussian Case}
    \begin{itemize}
      \item We will try to find the optimal $\varphi$ in the space of polynomials of degree less than $5$, i.e.,
	\begin{displaymath}
	 \varphi = a_0 + a_1x + a_2 x^2 +a_3x^3 + a_4x^4. 
	\end{displaymath}
      \item $\MSE$ is a convex functional with respect to $\varphi$. Thus, we can use gradient descent to find the true optimal in the space of polynomials. 
      \item To compute the MSE, we generate $100000$ $(x^{(k)},y^{(k)})$. The MSE is approximated by Monte-Carlo method:
	\begin{displaymath}
	  \MSE(f) \approx \frac{1}{10000}\sum_{k=1}^{100000} \left[\max_{i=1,2,3}\; (\varphi(y_i^{(k)})-x^{(k)})^2\right]
	\end{displaymath}
    \end{itemize}
  \end{frame}

  \begin{frame}{Numerical Example: the Gaussian Case}
    \begin{itemize}
      \item The optimal $\varphi$ in the space of polynomials of degree less than $5$ is
	\begin{displaymath}
	 \varphi(y_i) = 0.29 y_i .
	\end{displaymath}
	The optimal estimator and MSE are 
	\begin{displaymath}
	 f(y) = \Med_1(0.29y_1,0.29y_2,0.29y_3),\,\MSE(f) = 0.9. 
	\end{displaymath}
      \item When there is no attack, the optimal estimator and MSE are
	\begin{displaymath}
	 f(y) = (y_1+y_2+y_3)/4,\,\MSE(f) = 0.25. 
	\end{displaymath}
      \item The MSE of a constant estimator $f = 0$ is $\MSE(f) = 1$.
      \item There is a large degradation of estimation performance when $1$ sensor is compromised.
    \end{itemize}
  \end{frame}
  \section{Conclusion}
  \begin{frame}{Conclusion}
    \begin{itemize}
      \item We present a robust estimator design which can withstand Byzantine attacks on up to $l$ sensors.
      \item In particular, if $l \geq m/2$, then the optimal estimator is a constant estimator  $f = \mathbb E(x)$.
      \item For general $l < m/2$, the optimal symmetric estimator is based on the solution of a minimax problem.
      \item For the Gaussian case of $m=3$ and $l = 1$, we are able to find the optimal estimator by gradient descent in the class of polynomials with degree less than $5$.
    \end{itemize}
  \end{frame}
  \end{document}
