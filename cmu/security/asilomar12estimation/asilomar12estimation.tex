\documentclass{IEEEtran}
\usepackage{color}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}
\usepackage{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newcommand{\MSE}{\text{\bf{MSE}}}
\newcommand{\Med}{\text{\bf{Med}}}
\newcommand{\Min}{\text{\bf{Min}}}
\newcommand{\Max}{\text{\bf{Max}}}
\DeclareMathOperator{\Proj}{Trunc}           % sign

\IEEEoverridecommandlockouts                            
\overrideIEEEmargins

\title{\LARGE \bf {Robust Estimation in the Presence of Integrity Attacks}}

\author{Yilin Mo, Bruno Sinopoli
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation and grant NGIT2009100109 from Northrop Grumman Information Technology Inc Cybersecurity Consortium. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
\thanks{
$*$: Yilin Mo and Bruno Sinopoli are with the ECE department of Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu, brunos@ece.cmu.edu}
}
}

\begin{document} \maketitle

\label{chap:staticestimation}


\section{Problem Formulation}
\label{sec:problem}

The goal is to estimate a random variable $x$ from a vector $y \triangleq [y_1,\ldots,y_m]^T\in\mathbb R^m$ consisting of $m$ sensor measurements $y_i\in\mathbb R$, $i\in\{1,2,...,m\} = \mathcal S$. We assume that $x$ and $y$ has the following joint distribution:
\begin{displaymath}
  P((x,y)\in S) = \mu(S),
\end{displaymath}
where $S\in \mathbb R^{m+1}$ is a Borel-measurable set and $\mu$ is a probability measure on $\mathbb R^{m+1}$.

We assume that an attacker wants to increase the mean squared error on estimating $x$. To this end, the attacker has the ability to manipulate up to $l$ of the $m$ sensor measurements. Moreover the estimator does not know which $l$ of the $m$ measurements have been manipulated. Formally, this means that our estimate has to rely on a vector $y^c\in\mathbb R^m$ of \emph{manipulated measurements} defined by
\begin{align}  \label{eq:compromisevector}
  y^c = y + \gamma \circ y^a,
\end{align}
where $\circ$ is element-wise multiplication and the \emph{sensor-selection} vector $\gamma$ taking values in 
\begin{displaymath}
  S_\gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i = l\}
\end{displaymath}
and the \emph{bias} vector $y^a$ taking values in $\mathbb R^m$. By selecting which values of $\gamma$ are nonzero, the attacker chooses which of the $n$ sensors will be manipulated. The ``level'' of manipulation is determined by $y^a$.

The estimation problem is formalized as a minimax problem where one wants to select an optimal estimator
\begin{align}\label{eq:estimate}
  \hat x = f(y^c) =f(y+\gamma\circ y^a)
\end{align}
so as to minimize the Mean Squared Error(MSE), for the worst case manipulation by the adversary. Following Kerckhoffs' Principle that security should not rely on the obscurity of the system, our goal is to design the estimator $f:\mathbb R^m\to \mathbb R$ assuming that $f$ is known to the attacker. We also take the conservative approach that the attacker has full information about the state of the system. Namely, the underlying $x$ and all the measurements $y_1,\ldots,y_m$ are assumed to be known to the attacker. However, due to  limited resources, he can only manipulate $l$ of the $m$ sensors.  We assume that the defender knowns how many sensors $l$ can be attacked, but cannot identify them.

To compute the worst-case MSE that we seek to minimize, let us first define 
\begin{align}
  f^+(y) \triangleq \sup_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a),\, f^-(y) \triangleq \inf_{\gamma\in  S_\gamma,\,y^a \in \mathbb R^m} f(y+\gamma\circ y^a).
\end{align}

Given values of $x$, $y$ and an estimator $f$, an optimal policy for the attacker can be written as follows: 

The attacker computes $f^+(y)$ and $f^-(y)$ and compare them with $x$. If $|f^+(y)-x| \geq |f^-(y)-x|$, then the attacker chooses $\gamma$ and $y^a$ such that $f(y+\gamma\circ y^a)$ equals $f^+(y)$ (or as close as possible). Otherwise, the attacker chooses $\gamma$ and $y^a$ such that $f(y+\gamma\circ y^a)$ equals $f^-(y)$ (or as close as possible).

Under this worst-case attacker policy, we can define the squared error function $e(x,y):\mathbb R\times\mathbb R^m\rightarrow \mathbb R$ to be
\begin{equation}
  e(x,y) \triangleq \max((x-f^+(y))^2,(x-f^-(y))^2)
  \label{eq:errorfunction}
\end{equation}

As a result, the worst-case MSE for an estimator $f$ can be expressed as
\begin{equation}
  \MSE(f) \triangleq \int_{\mathbb R^{m+1}}e(x,y)d\mu. 
  \label{eq:mse}
\end{equation}

\begin{remark}
  Even if the function $f$ is measurable, $f^+$ and $f^-$ may not be necessarily measurable. In that case, we can use upper Darboux integral instead of Lebesgue integral to define the MSE. However, all the discussion in this dissertation will hold regardless.
\end{remark}

The rest of this chapter is devoted to deriving the optimal estimator $f^*$ with minimum MSE under the worst attack policy. Before continuing on, we would like to state the following theorem regarding the optimality of an estimator $f$, which will be used in future analysis.
\begin{theorem}
  For two estimators $f_1$ and $f_2$, if the following inequalities hold
  \begin{equation}
    f^+_1 \geq f^+_2\geq f^-_2\geq f^-_1, 
    \label{eq:upperlower}
  \end{equation}
  then
  \begin{equation}
    \MSE(f_1)\geq \MSE(f_2). 
  \end{equation}
  \label{theorem:upperlower}
\end{theorem}
\begin{proof}
  We only need to prove that $e_1(x,y) \geq e_2(x,y)$. It is easy to verify the following equality:
  \begin{displaymath}
    \max(|x-a|,|x-b|) = \max(a-x,x-b),\,\text{if } a\geq b.
  \end{displaymath}
  Therefore,
  \begin{align*}
    e_1(x,y) = \left[\max(f^+_1(y) - x,x - f^-_1(y))\right]^2,\, e_2(x,y) = \left[\max(f^+_2(y) - x,x - f^-_1(y))\right]^2
  \end{align*}
  By \eqref{eq:upperlower}, we know that
  \begin{displaymath}
    f^+_1(y) - x \geq f^+_2(y) - x,\,x-f^-_1(y)\geq x-f^-_2(y), 
  \end{displaymath}
  which implies $e_1(x,y)\geq e_2(x,y)$.
\end{proof}
\begin{remark}
  Given the true measurement $y$, the attacker can manipulate $y^c$ such that $f(y^c)$ equals either $f^+(y)$ or $f^-(y)$. Therefore, Theorem~\ref{theorem:upperlower} implies that in order to find the optimal $f$, we should make $f^+(y)$ and $f^-(y)$ as ``close'' as possible to each other.
\end{remark}
\section{Optimal Estimator Design for $l \geq m/2$}
\label{sec:optimal1}
In this section we consider the case when half or more of the measurements can be manipulated by the attacker. We show that, in this case, the attacker can render the information provided by the manipulated measurement vector $y$ useless, forcing the optimal estimate to be determined exclusively from the a-priori distribution of $x$, which is formalized as the following theorem:
\begin{theorem}
  If $l\geq m/2$, then the optimal estimator $f^*$ is given as $f^* = \mathbb E(x)$.
  \label{theorem:optimal1}
\end{theorem}
The rest of the section is devoted to the proof of Theorem~\ref{theorem:optimal1}, which requires several intermediate results. First, consider a set of index $\mathcal I\subset\{1,2,\dots,m\} = \mathcal S$, we have the following definitions:
\begin{definition}
  Define the size $|\mathcal I|$ of set $\mathcal I$ as the number of elements in $\mathcal I$.
\end{definition}
\begin{definition}
  Define the complement of $\mathcal I$ as $\mathcal I^c \triangleq \{x\in\mathcal S:x\notin \mathcal I \}$.
\end{definition}
\begin{definition}
  Define vector $\gamma_{\mathcal I} \triangleq [\gamma_0,\ldots,\gamma_m]^T\in \mathbb R^m$, where $\gamma_i = 1$ if $i\in \mathcal I$ and $\gamma_i = 0$ otherwise.
\end{definition}
\begin{definition}
  Define function $f^+_{\mathcal I}:\mathbb R^{m}\rightarrow \mathbb R$ and $f^-_{m}:\mathbb R^{|\mathcal I|}\rightarrow \mathbb R$ as
  \begin{align}
    f^+_{\mathcal I}(y)\triangleq \sup_{y^a \in \mathbb R^m} f(y+ \gamma_{\mathcal I} \circ y^a),\, f^-_{\mathcal I}(y)\triangleq \inf_{y^a \in \mathbb R^m} f(y+ \gamma_{\mathcal I} \circ y^a).
  \end{align}
\end{definition}
\begin{remark}
  $f^+_{\mathcal I}$ ($f^-_{\mathcal I}$) can be seen as the maximum (minimum) state estimate the attack can enforce when the attacker compromised sensors in the set $\mathcal I$. Therefore, it is easy to see that 
  \begin{displaymath}
    f^+(y) = \max_{|\mathcal I|=l}\;f^+_{\mathcal I}(y), f^-(y) = \min_{|\mathcal I|=l}\;f^-_{\mathcal I}(y).
  \end{displaymath}
\end{remark}

Now we have the following lemma to characterize $f^+_{\mathcal I}$ and $f^-_{\mathcal I}$.
\begin{lemma}
  If $\mathcal I \bigcup \mathcal J = \mathcal S$, then there exists a constant $C$, such that
  \begin{equation}
    f^+_{\mathcal I}(y) \geq C \geq  f^-_{\mathcal J}(y).
    \label{eq:constantseparate}
  \end{equation}
  \label{lemma:constantseparate}
\end{lemma}
\begin{proof}
  We will prove Lemma~\ref{lemma:constantseparate} by contradiction. It is easy to see that \eqref{eq:constantseparate} is equivalent to 
  \begin{displaymath}  
    f^+_{\mathcal I}(y_1) \geq f^-_{\mathcal J}(y_2),\forall y_1,y_2\in \mathbb R^m.
  \end{displaymath}
  Suppose that on the contrary, there exist $y_1 = [y_{1,1},\dots,y_{1,m}]^T$ and $y_2=[y_{2,1},\dots,y_{2,m}]^T$, such that $f^+_{\mathcal I}(y_1)< f^-_{\mathcal J}(y_2)$. Now consider another $y\in \mathbb R^m$, such that
  \begin{displaymath}
    y_i = \begin{cases}
      y_{2,i}&\text{if }i\in \mathcal I\\
      y_{1,i}&\text{if }i\in \mathcal I^c\\
    \end{cases}
  \end{displaymath}
  Since $\mathcal I\bigcup \mathcal J = \mathcal S$, $\mathcal I^c\subseteq \mathcal J$. It is easy to verify that 
  \begin{displaymath}
    y = y_1 + \gamma_{\mathcal I}\circ(y-y_1),\,y = y_2 + \gamma_{\mathcal J}\circ(y-y_2). 
  \end{displaymath}
  Therefore, from the definition of $f^+_{\mathcal I}$ and $f^-_{\mathcal J}$, we have
  \begin{displaymath}
    f(y)\leq f^+_{\mathcal I}(y_1) < f^-_{\mathcal J}(y_2) \leq f(y), 
  \end{displaymath}
  which is impossible.
\end{proof}

Now we are ready to prove Theorem~\ref{theorem:optimal1}.

\begin{proof}[Proof of Theorem~\ref{theorem:optimal1}]
  Since $l\geq m/2$, for any set $\mathcal I$ with size $l$, we could always find another set $\mathcal J$, such that $|\mathcal J| = l$ and $\mathcal I\bigcup \mathcal J = \mathcal S$. Therefore, we know there exist two constants $C^+_{\mathcal I}$ and $C^-_{\mathcal J}$, such that
  \begin{displaymath}
    f^+_{\mathcal I}(y) \geq  C^+_{\mathcal I}\geq  C^-_{\mathcal J} \geq  f^-_{\mathcal J}(y).
  \end{displaymath}
  Therefore,
  \begin{align*}
    f^+(y) = \max_{|\mathcal I|=l}f^+_{\mathcal I}(y)\geq  \max_{|\mathcal I|=l} C^+_{\mathcal I},\\
    f^-(y) = \min_{|\mathcal I|=l}f^-_{\mathcal I}(y)\leq\min_{|\mathcal I|=l} C^-_{\mathcal I}.
  \end{align*}
  Therefore, we could always find a constant $C$, such that 
  \begin{displaymath}
    f^+(y)\geq C\geq f^-(y). 
  \end{displaymath}
  By Theorem~\ref{theorem:upperlower}, the estimator $f_1(y) = C$ has a smaller MSE than $f(y)$. Therefore, the optimal estimator must be a constant estimator and it is straight forward to prove that the true optimal is given by $f^*(y) = \mathbb E(x)$.
\end{proof}

\section{Optimal Estimator Design for $l< m/2$}
\label{sec:optimal2}
We now consider the case when less than half the measurements can be manipulated by the attacker, i.e., $l<m/2$. Throughout this section, we will assume the following:
\begin{assumption}
  $f(y)$ is symmetric, i.e., $f(y_1,\ldots,y_m) = f(y_{i_1},\ldots,y_{i_m})$, for any permutation $(i_1,\ldots,i_m)$.
  \label{assumption:symmetric}
\end{assumption}
\begin{remark}
  Assumption~\ref{assumption:symmetric} is reasonable if the joint distribution of $x$ and $y$ is also symmetric on $y$, e.g., if the sensors are homogeneous.
\end{remark}

\begin{definition}
  Let $\mathcal I=\{i_1,\ldots,i_k\}\subseteq \{1,\dots,m\}$ be a subset of $\mathcal S$. Define the ``truncation'' function $\Proj_{\mathcal I}:\mathbb R^m\rightarrow\mathbb R^{k}$ as
  \begin{equation}
    \Proj_{\mathcal I}(y) \triangleq [y_{i_1},\ldots,y_{i_k}]^T.
    \label{eq:projection}
  \end{equation}
\end{definition}
\begin{definition}
  Let $\varphi:\mathbb R^{m-k}\rightarrow \mathbb R$ be an arbitrary function and  $\mathcal I=\{i_1,\ldots,i_k\}\subseteq \{1,\dots,m\}$ be a subset of $\mathcal S$. Define $\varphi_{\mathcal I}:\mathbb R^m\rightarrow \mathbb R$ as
  \begin{equation}
    \varphi_{\mathcal I}(y) \triangleq \varphi(\Proj_{\mathcal I^c}(y)).
    \label{eq:varphiprojection}
  \end{equation}
\end{definition}
Now we have the following theorem characterizing the form of the optimal estimator $f$.
\begin{theorem}
  The optimal symmetric estimator $f$ is of the following form
  \begin{equation}
    f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],
    \label{eq:optimal2}
  \end{equation}
  where $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ is a symmetric function. Furthermore,
  \begin{align}
    f^+(y) &\leq \max_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],\label{eq:upperequality}\\
    f^-(y)& \geq \min_{|\mathcal I|=l}\left[\min_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right]\label{eq:lowerequality},
  \end{align}
  \label{theorem:optimal2}
\end{theorem}

The rest of the section is devoted to proving Theorem~\ref{theorem:optimal2}, which requires several intermediate results.
\begin{proposition}
  \label{proposition:fupper}
  Consider the estimator $f$ defined in \eqref{eq:optimal2} and an arbitrary set $\mathcal I_0\subseteq \mathcal S$ such that $|\mathcal I_0| = l$, then the following equality holds
  \begin{equation}
    f(y) \leq \max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I_0 = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y).
    \label{eq:fupper}
  \end{equation}
\end{proposition}

\begin{lemma}
  For the estimator $f$ defined in \eqref{eq:optimal2}, the following equality holds
  \begin{equation}
    f(y) \geq \max_{|\mathcal J|=l}\left[\min_{|\mathcal I| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right].
    \label{eq:fminmax}
  \end{equation}
\end{lemma}

\begin{proof}
  We only need to prove the following inequality 
  \begin{displaymath}
    \max_{|\mathcal J| = l,\mathcal J \bigcap \mathcal I_0 } \varphi_{\mathcal J \bigcup \mathcal I_0}(y)\geq \min_{|\mathcal I| = l,\mathcal I \bigcap \mathcal J_0 } \varphi_{\mathcal J \bigcup \mathcal I_0}(y),
  \end{displaymath}
  for all $|\mathcal I_0|=|\mathcal J_0| = l$. Let us find an index set $\mathcal K$, such that $\mathcal I_0\subset \mathcal K$, $\mathcal J_0\subset \mathcal K$ and $|\mathcal K| = 2l$. Therefore, we have
  \begin{displaymath}
    \varphi_{\mathcal K}(y) = \varphi_{\mathcal I_0\bigcup (\mathcal K\backslash \mathcal I_0)}(y)=  \varphi_{\mathcal J_0\bigcup (\mathcal K\backslash \mathcal J_0)}(y).
  \end{displaymath}
  Hence, for all $|\mathcal I_0|=|\mathcal J_0| = l$,
  \begin{displaymath}
    \max_{|\mathcal J| = l,\mathcal J \bigcap \mathcal I_0 } \varphi_{\mathcal J \bigcup \mathcal I_0}(y)\geq \varphi_{\mathcal K}(y)\geq  \min_{|\mathcal I| = l,\mathcal I \bigcap \mathcal J_0 } \varphi_{\mathcal J \bigcup \mathcal I_0}(y),
  \end{displaymath}
  which finishes the proof.
\end{proof}

\begin{proposition}
  \label{proposition:flower}
  Consider the estimator $f$ defined in \eqref{eq:optimal2} and an arbitrary set $\mathcal J_0\subseteq \mathcal S$ such that $|\mathcal J_0| = l$, then the following equality holds
  \begin{equation}
    f(y) \geq \min_{|\mathcal I| = l,\,\mathcal J_0\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J_0}(y).
    \label{eq:flower}
  \end{equation}
\end{proposition}

\begin{proposition}
  \label{proposition:invariant}
  The following equality holds for arbitrary function $\varphi$
  \begin{equation}
    \varphi_{\mathcal I\bigcup \mathcal J}(y+\gamma_{\mathcal I}\circ y^a)  = \varphi_{\mathcal I\bigcup \mathcal J}(y).  
  \end{equation}
\end{proposition}

\begin{lemma}
  Consider the estimator $f$ defined in \eqref{eq:optimal2}, then the following equality holds
  \begin{align}
    f^+_{\mathcal I}(y) \leq \max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y),\label{eq:alphaineq}\\
    f^-_{\mathcal I}(y) \geq \min_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y).\label{eq:betaineq}
  \end{align}
  \label{lemma:estimatorupperlower}
\end{lemma}

\begin{proof}
  By Proposition~\ref{proposition:fupper} and \ref{proposition:invariant}, we know that
  \begin{align*}
    f^+_{\mathcal I}(y) &= \sup_{y^a}f(y+\gamma_{\mathcal I}\circ y^a)\leq \sup_{y^a}\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y+\gamma_{\mathcal I}\circ y^a)= \max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y),
  \end{align*}
  which proves \eqref{eq:alphaineq}. Similarly one can prove \eqref{eq:betaineq}.
\end{proof}
Now we are ready to prove Theorem~\ref{theorem:optimal2}.
\begin{proof}[Proof of Theorem~\ref{theorem:optimal2}]
  Consider an arbitrary estimator $f$, let us define the following functions $\varphi^+,\,\varphi^-:\mathbb R^{m-2l}\rightarrow\mathbb R$ as
  \begin{align}
    \varphi^+(y_{2l+1},\ldots,y_{m}) &\triangleq \inf_{y_{1},\dots,y_{l}}\left[\sup_{y_{l+1},\dots,y_{2l}} f(y_1,\dots,y_m)\right]\\
    \varphi^-(y_{2l+1},\ldots,y_{m}) &\triangleq \sup_{y_{1},\dots,y_{l}}\left[\inf_{y_{l+1},\dots,y_{2l}} f(y_1,\dots,y_m)\right].
  \end{align}
  Since $f$ is symmetric, it is easy to see that the following inequality holds
  \begin{equation}
    \varphi^+(y_1,\dots,y_{m-2l}) \geq \varphi^-(y_1,\dots,y_{m-2l}).
  \end{equation}
  Therefore, we could define 
  \begin{displaymath}
    \varphi = (\varphi^+ + \varphi^-)/2 .
  \end{displaymath}
  It is straightforward to verify that $\varphi$ is symmetric and 
  \begin{displaymath}
    \varphi^+ \geq \varphi \geq \varphi^-. 
  \end{displaymath}
  Hence, we define a new estimator $g$ as
  \begin{displaymath}
    g(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right],
  \end{displaymath}
  and the corresponding $g^+_{\mathcal I}$ and $g^-_{\mathcal I}$ as
  \begin{align*}
    g^+_{\mathcal I}(y)\triangleq \sup_{y^a \in \mathbb R^m} g(y+ \gamma_{\mathcal I} \circ y^a),\, g^-_{\mathcal I}(y)\triangleq \inf_{y^a \in \mathbb R^m} g(y+ \gamma_{\mathcal I} \circ y^a).
  \end{align*}
  Now, by the definition of $f^+_{\mathcal I}$ and $f^-_{\mathcal I}$, we know that for the original estimator $f$:
  \begin{align}
    f^+_{\mathcal I}(y) \geq \max_{|\mathcal J| = l,\mathcal J\bigcap \mathcal I = \emptyset}  \varphi^+_{\mathcal I\bigcup \mathcal J}(y), f^-_{\mathcal I}(y) \leq \min_{|\mathcal J| = l,\mathcal J\bigcap \mathcal I = \emptyset} \varphi^-_{\mathcal I\bigcup \mathcal J}(y).
  \end{align}
  On the other hand, by Lemma~\ref{lemma:estimatorupperlower}, we have
  \begin{align*}
    g^+_{\mathcal I}(y)& \leq \max_{|\mathcal J| = l,\mathcal J\bigcap \mathcal I = \emptyset} \varphi_{\mathcal I\bigcup \mathcal J}(y)\leq f^+_{\mathcal I}(y),\\
    g^-_{\mathcal I}(y)& \geq \min_{|\mathcal J| = l,\mathcal J\bigcap \mathcal I = \emptyset} \varphi_{\mathcal I\bigcup \mathcal J}(y)\geq f^-_{\mathcal I}(y). 
  \end{align*}
  As a result, by Theorem~\ref{theorem:upperlower}, we know that $g$ has a smaller MSE than $f$. Thus, the optimal symmetric $f$ is of the form \eqref{eq:optimal2}. \eqref{eq:upperequality} and \eqref{eq:lowerequality} can be easily verified by the fact that
  \begin{displaymath}
    f^+ = \max_{|\mathcal I|=l}f^+_{\mathcal I},\, f^- = \min_{|\mathcal I|=l}f^-_{\mathcal I}.
  \end{displaymath}

\end{proof}

\begin{remark}
  One drawback of Theorem~\ref{theorem:optimal2} is that \eqref{eq:optimal2} is quite complicated. In particular, to determine $f(y)$, we need to compute all ${m}\choose{2l}$ $\varphi_{\mathcal I\bigcup \mathcal J}(y)$s, which could be a huge burden if $m$ is large. In the next section, we consider two special cases, where \eqref{eq:optimal2} can be simplified.
\end{remark}

\section{Special Cases}
In this section, we prove that under certain conditions, \eqref{eq:optimal2} can be simplified.
\begin{definition}
  $\varphi:\mathbb R^{m-2l}\rightarrow \mathbb R$ is monotonically increasing if the following inequality hold:
  \begin{displaymath}
    \varphi(y_1,\dots,y_{m-2l})\geq \varphi(y_1',\dots,y_{m-2l}'),\text{ if }y_i\geq y_i',\,\forall i. 
  \end{displaymath}
  $\varphi$ is monotonically decreasing if $-\varphi$ is monotonically increasing. $\varphi$ is monotone if it is either monotonically increasing or monotonically decreasing.
\end{definition}
\begin{definition}
  Define the function $\Med_{l}:\mathbb R^{m}\rightarrow \mathbb R^{m-2l}$ as a symmetric function, which satisfies\footnote{Due to symmetry, we only need to define the function when $y_1\leq \dots\leq y_m$.}
  \begin{align}
    \Med_l(y_1,\ldots,y_m) &\triangleq (y_{l+1},\ldots,y_{m-l}),
  \end{align}
  when $y_1\leq \dots\leq y_m$.
\end{definition}

\begin{remark}
  The $\Med_l$ function can be seen as removing the largest $l$ measurements and smallest $l$ measurements. In particular, if $m =2l+1$, then $\Med_l(y)$ is just the median of $y$. 
\end{remark}

Now we have the following theorem characterizing $f$ when $\varphi$ is monotone.
\begin{theorem}
  Consider $f(y)$ defined in \eqref{eq:optimal2}
  \begin{displaymath}
    f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right].
  \end{displaymath}
  If $\varphi$ is symmetric and monotone, then
  \begin{equation}
    f(y) = \varphi(\Med_l(y)). 
    \label{eq:optimalmonotone}
  \end{equation}
  \label{theorem:monotone}
\end{theorem}
\begin{proof}
  We will assume that $\varphi$ is monotonically increasing, since the case where $\varphi$ is monotonically decreasing can be proved by similar arguments. We can further assume that $y_1\leq\dots\leq y_m$ due to symmetry.

  Let us define $\mathcal I_0 = \{m-l+1,\ldots,m\}$ and $\mathcal J_0 = \{1,\ldots,l\}$. It is clear that
  \begin{align*}
    f(y) &= \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right]\leq \max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I_0 = \emptyset } \varphi_{\mathcal I_0 \bigcup \mathcal J}(y)
  \end{align*}
  Now by monotonicity of $\varphi$, we know that
  \begin{displaymath}
    \max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I_0 = \emptyset } \varphi_{\mathcal I_0 \bigcup \mathcal J}(y) =  \varphi_{\mathcal I_0 \bigcup \mathcal J_0}(y) = \varphi(\Med_l(y)),
  \end{displaymath}
  which implies that $f(y)\leq \varphi(\Med_l(y))$. On the other hand, we know that
  \begin{align*}
    f(y) &= \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right]\geq \max_{|\mathcal J|=l}\left[\min_{|\mathcal I| = l,\,\mathcal I\bigcap \mathcal J = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right]\geq\min_{|\mathcal I| = l,\,\mathcal I\bigcap \mathcal J_0 = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J_0}(y).
  \end{align*}
  By monotonicity of $\varphi$, we have that
  \begin{displaymath}
    \min_{|\mathcal I| = l,\,\mathcal I\bigcap \mathcal J_0 = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J_0}(y) = \varphi_{\mathcal I_0\bigcup \mathcal J_0}(y) = \varphi(\Med_l(y)),
  \end{displaymath}
  which implies that $f(y) \geq \varphi(\Med_l(y))$. Therefore
  \begin{displaymath}
    f(y) = \varphi(\Med_l(y)).
  \end{displaymath}
\end{proof}
\begin{remark}
  Since we could always perform a transformation on all the measurements, we could generalize Theorem~\ref{theorem:monotone} to a function $\varphi$ that is monotone on the transformed measurements, which is given by the following Corollary. 
\end{remark}
\begin{corollary}
  Consider $f(y)$ defined in \eqref{eq:optimal2}
  \begin{displaymath}
    f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right].
  \end{displaymath}
  If there exist a function $t:\mathbb R\rightarrow \mathbb R$ and another function $\phi:\mathbb R^{m-2l}\rightarrow\mathbb R$, which is monotone and symmetric, such that
  \begin{displaymath}
    \varphi(y_1,\ldots,y_{m-2l}) = \phi(t(y_1),\dots,t(y_{m-2l})). 
  \end{displaymath}
  Then $f(y)$ can be expressed as
  \begin{equation}
    f(y) = \phi(\Med_l(t(y_1),\dots,t(y_m))). 
  \end{equation}
  \label{corollary:monotonegeneralized}
\end{corollary}
If $ m = 2l+1$, then $\varphi$ itself is a mapping from $\mathbb R$ to $\mathbb R$. Therefore, we could choose $t(y_i) = \varphi(y_i)$ and $\phi(t) = t$. Thus, we have the following corollary
\begin{corollary}
  Consider $f(y)$ defined in \eqref{eq:optimal2},
  \begin{displaymath}
    f(y) = \min_{|\mathcal I|=l}\left[\max_{|\mathcal J| = l,\,\mathcal J\bigcap \mathcal I = \emptyset } \varphi_{\mathcal I \bigcup \mathcal J}(y)\right].
  \end{displaymath}
  If $m = 2l+1$, then $f(y)$ can be expressed as
  \begin{equation}
    f(y) = \Med_l(\varphi(y_1),\dots,\varphi(y_m)). 
  \end{equation}
  \label{corollary:boundarycase}
\end{corollary}
\section{Numerical Examples: Gaussian Case}
\label{sec:example}
We assume the state $x\sim \mathcal N(0,1)$ is normal distributed with zero mean and unit variance. Three sensors are deployed to monitor $x$, the measurement equations of which are given by
\begin{displaymath}
  y_i = x + v_i,\,i=1,2,3,
\end{displaymath}
where $v_i\sim \mathcal N(0,1)$ is also normal distributed with zero mean and unit variance. We further assume that $x,v_1,v_2,v_3$ are independent. We assume that only one measurement can be manipulated by the attacker, i.e., $l = 1$. As a result, by Theorem~\ref{theorem:optimal2} and Corollary~\ref{corollary:boundarycase}, we know the optimal $f(y)$ is of the following form
\begin{displaymath}
  f(y) = \Med_1(\varphi(y_1),\varphi(y_2),\varphi(y_3)) 
\end{displaymath}
Moreover, it is easy to prove that
\begin{align*}
  f^+(y) &= \max(\varphi(y_1),\varphi(y_2),\varphi(y_3)),\\
  f^-(y)&= \min(\varphi(y_1),\varphi(y_2),\varphi(y_3)) .
\end{align*}
Hence,
\begin{equation}
  \MSE(f) = \int_{\mathbb R^4}e(x,y)d\mu = \mathbb E\left[\max_{i=1,2,3}\; (\varphi(y_i)-x)^2\right]
\end{equation}
The following proposition characterizes the convexity of $\MSE(f)$:
\begin{proposition}
  \label{proposition:mseconvexity}
  Consider two estimator $f_1,\,f_2$ defined as
  \begin{align*}
    f_1(y) \triangleq \Med_1(\varphi_1(y_1),\varphi_1(y_2),\varphi_1(y_3)), f_2(y) \triangleq \Med_1(\varphi_2(y_1),\varphi_2(y_2),\varphi_2(y_3)) .
  \end{align*}
  Define $\varphi_3(y_i)$ as
  \begin{align*}
    \varphi_3(y_i) \triangleq a\varphi_1(y_i)+b\varphi_2(y_i),
  \end{align*}
  where $a,b\geq 0$ and $a+b = 1$. Moreover, define $f_3(y)$ as
  \begin{align*}
    f_3(y) \triangleq \Med_1(\varphi_3(y_1),\varphi_3(y_2),\varphi_3(y_3)). 
  \end{align*}
  Then the following inequality holds:
  \begin{equation}
    \MSE(f_3)\leq a\MSE(f_1)+b\MSE(f_2).
  \end{equation}
\end{proposition}
\begin{proof}
  First by the convexity of $x^2$, it is easy to see that
  \begin{displaymath}
    (\varphi_3(y_i)-x)^2 \leq  a (\varphi_1(y_i)-x)^2+b   (\varphi_2(y_i)-x)^2.
  \end{displaymath}
  Thus,
  \begin{align*}
    \max_{i}  (\varphi_3(y_i)-x)^2 &\leq \max_{i}\left[  a (\varphi_1(y_i)-x)^2+b   (\varphi_2(y_i)-x)^2\right]\\
    &\leq a\left[\max_{i}\;(\varphi_1(y_i)-x)^2\right]+b\left[\max_i\;(\varphi_2(y_i)-x)^2\right]\\
  \end{align*}
  By taking the expectation on both sides, we can finish the proof.
\end{proof}
If we assume that $\varphi$ belongs to some finite dimensional linear space, e.g., all polynomial of degree less than $k$, then $\varphi$ can be written as
\begin{displaymath}
  \varphi = a_1\varphi_1+a_2\varphi_2+\dots+a_k\varphi_k, 
\end{displaymath}
where $a_i$s are scalars and $\{\varphi_1,\dots,\varphi_k\}$ is the basis of the space. By Proposition~\ref{proposition:mseconvexity}, we know that $\MSE$ is a convex functional with respect to $\varphi$. Therefore, methods such as gradient descent can be used to find the optimal $a_i$s and thus the optimal $\varphi$ efficiently. 

In this section, we seek to find the optimal $\varphi$ over all polynomials of degree less than $5$. We will approximate $\MSE(f)$ by Monte-Carlo method. To be specific, we first generate a training set of $(x^{(k)},y_1^{(k)},y_2^{(k)},y_3^{(k)})$, where $k = 1,\dots,N$, based on the Gaussian assumption. The $\MSE(f)$ is then approximated by
\begin{displaymath}
  \MSE(f) \approx \frac{1}{N}\sum_{k=1}^N \left[\max_{i=1,2,3}\; (\varphi(y_i^{(k)})-x^{(k)})^2\right]
\end{displaymath}
We use gradient descent to find the optimal $a_k$s, such that
\begin{displaymath}
  \varphi(y_i) = \sum_{k=0}^4 a_k y_i^k. 
\end{displaymath}
In this simulation, we choose the size of the training set to be $N = 100000$. The optimal $\varphi$, $f$ and $\MSE$ found by gradient descent is given by
\begin{displaymath}
  \varphi(y_i)= 0.29 y_i,\,f(y) = \Med_1(0.29y_1,0.29y_2,0.29y_3),\,\MSE(f)= 0.90. 
\end{displaymath}
If no sensor is compromised, then the optimal estimator is $\hat x = (y_1+y_2+y_3)/4$, which has a mean squared error of $0.25$, which indicates that there is a more than threefold increase in MSE caused by the attacker. Further, the MSE of a constant estimator $\hat x = \mathbb Ex = 0$ is $1$. Therefore, it can be seen that one compromised sensor can potentially cause a large degradation in the estimation performance.

It is also worth noticing that the optimal $\varphi$ is linear (within the numerical error). However, as we only search over the space of polynomials of degree less than $5$, it is still an open problem to find the true optimal $\varphi$.
\end{document}

