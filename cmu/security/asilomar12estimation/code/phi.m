function hatx = phi(y,a,b)
    hatx = b*y;
    if abs(hatx) <= a
        hatx = 0;
    end
end