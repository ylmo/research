clc
clear

num = 100000; %number of experiments

%generate data
state = randn(num,1);
noise = randn(num,3);
measure = noise+[state state state];


indexb = 1;


for b = 0.2:0.02:0.4
    indexa = 1;
    for a = 0.05:0.05:1
        
        mse = 0;
        
        for i = 1:num
            x = state(i);
            y1 = measure(i,1);
            y2 = measure(i,2);
            y3 = measure(i,3);
            error = max([(phi(y1,a,b)-x)^2,(phi(y2,a,b)-x)^2,(phi(y3,a,b)-x)^2]);
            mse = mse + error;
        end
        
        mse = mse / num;
        result(indexa,indexb) = mse;
        indexa = indexa + 1;
    end
    indexb = indexb + 1
end
