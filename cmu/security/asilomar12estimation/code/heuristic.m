clc
clear

num = 20000; %number of experiments
degree = 1;%x,x^3,x^5
%generate data
state = randn(num,1);
noise = randn(num,3);
noise(:,1) = noise(:,1)*0.01;
measure = noise+[state state state];


coef1 = rand(1,degree);
coef2 = rand(1,degree);

for step = 1:10
    
    
    derivative1 = zeros(1,degree);
    hessian1 = zeros(degree,degree);
    derivative2 = zeros(1,degree);
    hessian2 = zeros(degree,degree);
    mse = 0;
    for  i = 1:num
        measurementpower = ones(degree,3);
        for j = 1:degree
            measurementpower(j,:) = measure(i,:).^(2*j-1);
        end
        
        error = zeros(1,3);
        
        error(1) = (coef1 * measurementpower(:,1) - state(i))^2;
        error(2) = (coef2 * measurementpower(:,2) - state(i))^2;
        error(3) = (coef2 * measurementpower(:,3) - state(i))^2;
        
        [maxerror index] = max(error);
        if index == 1
            derivative1 = derivative1 + 2 * measurementpower(:,1)' * (coef1 * measurementpower(:,1) - state(i));
            hessian1 = hessian1 + 2 * measurementpower(:,1) * measurementpower(:,1)';
        else
            derivative2 = derivative2 + 2 * measurementpower(:,index)' * (coef2 * measurementpower(:,index) - state(i));
            hessian2 = hessian2 + 2 * measurementpower(:,index) * measurementpower(:,index)';
        end
        
        mse = mse + maxerror;
    end
    coef1 = coef1 - 0.5*(hessian1\derivative1')';
    coef2 = coef2 - 0.5*(hessian2\derivative2')';

    
    step
    mse/num
end
