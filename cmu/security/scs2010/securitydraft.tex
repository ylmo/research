\documentclass{article}

\usepackage[dvips]{graphicx}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage{slashbox}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}


\begin{document}

\section{System Overview}\label{sec:classiccontrol}
In this section we will introduce linear system model, estimator, controller and failure detector for our case. 

Consider the following linear, time invariant (LTI) system whose state dynamics are given by
\begin{equation}
  x_{k+1} = Ax_k + Bu_k + w_k, 
  \label{eq:systemdescription}
\end{equation}
where  $x_k \in \mathbb R^n$ is the vector of state variables at time $k$, $u_k\in\mathbb R^p$ is the control input, $w_k\in \mathbb R^n$ is the process noise at time $k$ and $x_0$ is the initial state. We assume $w_k,\,x_0$ are independent Gaussian random variables, $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$.  

Sensor network is monitoring the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are sent to a base station. The observation equation can be written as  
\begin{equation}
  y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k \in \mathbb R^m$ is a vector of measurements from the sensors and $v_k \sim \mathcal N(0,\;R)$ is the measurement noise independent of $x_0$ and $w_k$. 

We suppose that the system is using a simple least square estimator for state estimation, which takes the following form:
\begin{equation}
\hat x_k = K y_k,  
\end{equation}
where 
\begin{equation}
  K = (C^T R C)^{-1} C^T R,  
\end{equation}
and $KC=I$.

The system also implement a fixed gain controller, which takes the following form:
\begin{equation}
u_k  = L \hat x_k.  
\end{equation}
We suppose the system is close loop stable, which means that $A+BL$ is stable.

Define innovation at step $k$ to be
\begin{equation}
z_k \triangleq y_k - C\hat x_k = (I-CK)y_k.  
\end{equation}

We would also like to assume that the system perform detection based on the state estimation $\hat x_k$ and innovation $z_k$, i.e. if $\hat x_k$ or $z_k$ is far away from $0$, then the system is likely to trigger an alarm. 

\section{Attack Model}\label{sec:deceptionattack}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section, we assume that a malicious third party wants to attack the integrity of the system described in Section~\ref{sec:classiccontrol}. We will first describe an attack model based on the assumptions that the attacker knows the system and can modify part of the sensor readings. We will then formulate the actions and goals of the attacker as an optimization problem.

We suppose the attacker has the capability to perform the following actions:
\begin{enumerate}
  \item It knows the system model and control strategy. 

  \item It can control the readings of a subset of the sensors, which we denote as $S_{bad}$.  Also we define $ \Lambda = diag(\lambda_1,\ldots,\lambda_m)$ to be the sensor selection matrix, where $\lambda_i$ is binary variable and $\lambda_i = 1$ if and only if $i \in S_{bad}$. 
  \item It can inject an malicious input, which takes the form of $B^a u_k^a$.
\end{enumerate}

Under the attack, we can write the system equations as
\begin{align*}
  x'_{k+1} &= Ax'_k + Bu'_k + B^a u_k^a  + w_k,\\
  y'_k&=Cx'_k+v_k,\\
  \hat x'_{k} &= K(y'_k +\Lambda y_k^a),\\
  u'_k &= L\hat x'_k,
\end{align*}
where $y_k^a$ is the observation changed by the attacker.

Now define 
\begin{equation}
  \begin{split}
  \Delta x_{k} &= x'_k - x_k,\\
  \Delta y_{k} &= y'_k - y_k,\\
  \Delta \hat x_{k} &= \hat x'_k - \hat x_k,\\
  \Delta u_{k} &= u'_k - u_k,\\
  \Delta z_{k} &= z'_k - z_k,\\
  \end{split}
\end{equation}

By manipulating equations, we can show the following equations holds 
\begin{equation}
  \begin{split}
    \Delta x_{k+1} &= (A+BL)\Delta x_k + BLK \Lambda y_k^a +B^au_k^a,\\
    \Delta \hat x_{k}&= \Delta x_k + K \Lambda y_k^a,\\
    \Delta z_{k}&= (I - C K) \Lambda y_k^a.
  \end{split}
\end{equation}

Based on the detection strategy of the controller, the attacker would like try to maximize $\Delta x_k$ while try to keep $\Delta \hat x_
k$ small. For a $T$ steps attack, we can formulate the problem as an optimization problem:
\begin{equation}
  \label{eq:optimization2}
  \begin{split}
    \max\;\;&\left\|\Delta x_{T-1}\right\|\\
    s.t.\;\;&\|\Delta \hat x_k\| \leq \alpha,\,k=0,\,1,\,\ldots,T-1\\
    &\|\Delta z_k\| \leq \beta,\,k=0,\,1,\,\ldots,T-1
  \end{split}
\end{equation}
\begin{remark}
  $\alpha,\,\beta$ characterize how subtle the attacker wants the attack to be. It is easy to see that certain scaling property holds for the above optimization problem, i.e., if we change $\alpha,\,\beta$ to $k\alpha,\,k\beta$, then the objective function will be $k$ times the original one.
\end{remark}

\section{Main Result}
In general, solving \eqref{eq:optimization2} is intractable for large $T$ and general control system. Hence, we would like to constrain what type of attack input $y_k^a, u_k^a$ we can choose and solve the problem on a subset of the original search space. The result we got can be seen as an lower bound for the original problem.

Suppose that we would like to choose $y_k^a,\, u_k^a$ of the following form:
\begin{equation}
y_k^a = \lambda^k y_0^a,  u_k^a = \lambda^k u_0^a
\end{equation}
where $|\lambda|>1$.

By manipulating equations, we know that
\begin{equation}
  \begin{split}
    \Delta x_{T-1} &= \sum_{i=0}^{T-1} (A+BL)^{i} BLK\Lambda y^a_{T-1-i}+ \sum_{i=0}^{T-1} (A+BL)^{i} B^a u^a_{T-1-i}\\
    &= \sum_{i=0}^{T-1} (A+BL)^{i}BLK\Lambda \lambda^{T-1-i} y_0^a + \sum_{i=0}^{T-1} (A+BL)^{i}B^a \lambda^{T-1-i} u_0^a.\\
    &=\sum_{i=0}^{T-1} (A+BL)^{i}BLK\Lambda \lambda^{-i-1} y_T^a + \sum_{i=0}^{T-1} (A+BL)^{i}B^a \lambda^{-i-1} u_T^a\\
    \Delta \hat x_k &=\Delta x_{k} + K\Lambda y_k^a\\
    &=   \sum_{i=0}^k (A+BL)^{i}BLK\Lambda \lambda^{k-1-i} y_0^a + K\Lambda \lambda^{k}y_0^a +\sum_{i=0}^{T-1} (A+BL)^{i}B^a \lambda^{-i-1} u_T^a\\
    & =  \left[\sum_{i=0}^k (A+BL)^{i}BLK\Lambda \lambda^{-i-1}  + K\Lambda\right]y_k^a + \sum_{i=0}^{T-1} (A+BL)^{i}B^a \lambda^{-i-1} u_T^a
  \end{split}
\end{equation}
Since $\lambda^{-1},\,A+BL$ is stable, we can assume that
\begin{equation}
  \begin{split}
    \Delta x_{T-1} &\approx \sum_{i=0}^{\infty} (A+BL)^{i}BLK\Lambda \lambda^{-i-1} y_T^a+ \sum_{i=0}^{\infty} (A+BL)^{i}B^a \lambda^{-i-1} u_T^a\\
    \Delta \hat x_k & \approx   \left[\sum_{i=0}^\infty (A+BL)^{i}BLK\Lambda \lambda^{-i-1}  + K\Lambda\right]y_k^a+ \sum_{i=0}^{\infty} (A+BL)^{i}B^a \lambda^{-i-1} u_T^a
  \end{split}
\end{equation}

Let \[X \triangleq \sum_{i=0}^\infty (A+BL)^{i}\lambda^{-i-1}.\]Hence
\begin{equation}
  X -\lambda^{-1}(A+BL)X =   \sum_{i=0}^\infty (A+BL)^{i} \lambda^{-i-1} - \sum_{i=1}^\infty (A+BL)^{i} \lambda^{-i-1} = \lambda^{-1} ,
\end{equation}
and
\begin{equation}
  X = \left[I-\lambda^{-1}(A+BL)\right]^{-1}\lambda^{-1} .  
\end{equation}
Therefore
\begin{equation}
  \begin{split}
    \Delta x_{T-1} &\approx  \left[I-\lambda^{-1}(A+BL)\right]^{-1}\lambda^{-1}\left(BLK\Lambda y_T^a+ B^a u_T^a \right)  \\
    \Delta \hat x_k & \approx  \left[I-\lambda^{-1}(A+BL)\right]^{-1}\left[(1-\lambda^{-1}A) K\Lambda y_k^a+\lambda^{-1}B^a u_k^a\right]
  \end{split}
\end{equation}

\section{Simulation}

\begin{equation}
A = \left[ {\begin{array}{*{20}c}
0.9501&	0.486&	0.4565\\
0.2311&	0.8913&	0.0185\\
0.6068&	0.7621&	0.8214
\end{array}} \right] , K =   \left[ {\begin{array}{*{20}c}
-0.6571&0.0153&	0.9318\\
-0.2275&0.7468&	0.466\\
-0.7186&0.4451&	0.4186
\end{array}} \right].
\end{equation}
Only the first sensor is compromised.



\end{document} 

