\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\mode<presentation>

\title[False Data Injection]{False Data Injection Attacks in Control Systems}
\author[Bruno Sinopoli]{Bruno Sinopoli, Yilin Mo}
\institute[Carnegie Mellon University]{Department of Electrical and Computer Engineering,\\ Carnegie Mellon University\\}
\date[1st SCS]{First Workshop on Secure Control Systems}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}

\begin{frame}{Control Systems}
  \begin{itemize}
    \item Control Systems are ubiquitous.
    \item Typical applications of control systems include aerospace, chemical processes, civil infrastructure, energy and manufacturing.
    \item Lots of them are \alert{safety-critical}.
    \item Advances in computation and communication technology have greatly increased the capability of control systems. But new challenges arise as the systems become more and more complicated.
    \item Our goal: verification and design of secure control algorithm.
  \end{itemize}
\end{frame}

\section{System Description}

\begin{frame}{System Model}
  We consider the control system is monitoring the following LTI(Linear Time-Invariant) system
  \begin{block}{System Description}
      \begin{equation}
	\begin{split}
	  x_{k+1} &= Ax_k + Bu_k + w_k,\\
	  y_{k} &= C x_k + v_k.
	\end{split}
	\label{eq:systemdiscription}
      \end{equation}
    \end{block}
    \begin{itemize}
      \item $x_k \in \mathbb R^n$ is the state vector.
      \item  $y_k \in \mathbb R^m$ is the measurements from the sensors.
      \item $u_k \in \mathbb R^p$ is the control inputs.
      \item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
    \end{itemize}
  \end{frame}

    \begin{frame}{Kalman Filter and LQG Controller}
      \begin{itemize}
	\item Kalman filter (Assume already in steady states)
	  \begin{displaymath}
	    \hat x_{0|-1} = \bar x_0, \hat x_{k+1|k} = A\hat x_{k|k} + Bu_k,\hat x_{k+1|k+1} = \hat x_{k+1|k}+ K(y_{k+1} - C \hat x_{k+1|k}).
	  \end{displaymath}
	\item The LQG controller tries to minimize
	  \begin{displaymath}
	    J =\min \lim_{T\rightarrow \infty}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right].
	  \end{displaymath}
	\item The solution is a fixed gain controller
	  \begin{displaymath}
	    u_k^* = -(B^TSB+U)^{-1}B^TSA\hat x_{k|k} = L \hat x_{k|k},
	  \end{displaymath}
	  where
	  \begin{displaymath}
	    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
	  \end{displaymath}
      \end{itemize}
    \end{frame}

    \begin{frame}{$\chi^2$ Failure Detector}
      The innovation of Kalman filter $z_k\triangleq y_k - C\hat x_{k|k-1}$ is i.i.d. Gaussian distributed with zero mean. 
      \begin{block}{$\chi^2$ Detector}
	The $\chi^2$ detector triggers an alarm based on the following event:
	\begin{displaymath}
	  g_k= (y_k - C\hat x_{k|k-1})^T\mathcal P^{-1}(y_k - C\hat x_{k|k-1})> threshold.
	\end{displaymath}
      \end{block}
    \end{frame}

\section{False Data Injection Attack}

\begin{frame}{Attack Model}
  We assume the following:
  \begin{enumerate}
    \item The attacker knows matrices $A,\,C,\,K$.
    \item The attacker can control the readings of a subset of sensors. Hence, the measurement received by the Kalman filter can be written as
      \begin{displaymath}
	y'_k = Cx_k' + v_k + \Gamma y_k^a,	
      \end{displaymath}
      where $y_k^a$ is the bias introduced by the attacker, $\Gamma = diag(\gamma_1,\ldots,\gamma_m)$ is the sensor selection matrix. $\gamma_i = 1$ if the attacker can control the readings of sensor $i$. $\gamma_i = 0$ otherwise. 
    \item The attack begins at time $0$.
    \item The sequence of attacker's inputs $(y_0^a,\ldots,y_k^a)$ is chosen before the attack. Hence, $y_k^a$ is independent of $w_k,\,v_k$.
      \note{Here first we assume the attacker only have partial knowledge on the system. However later we can show that the partial knowledge is enough. In the fourth assumption, since the attacker does not know the whole system, we assume it will not change its input according to $\Gamma y'_k$, which is what it observe. Hence, we may assume $y_k^a$ is designed off-line.} 
  \end{enumerate}
\end{frame}

\begin{frame}{System Diagram}
  \begin{figure}
    \begin{center}
      \includegraphics[width = 0.8 \textwidth]{diagram1.mps}
    \end{center}
    \caption{System Diagram}
  \end{figure}
\end{frame}

\begin{frame}{Healthy System v.s. Compromised System}
    \begin{minipage}[t]{0.45\textwidth}
      \begin{block}{Healthy System}
	\begin{align*}
	  x_{k+1} &= Ax_k + Bu_k + w_k\\
	  y_k & = Cx_k + v_k \\
	  z_{k+1} & = y_{k+1} - C(A\hat x_{k}+Bu_{k})\\
	  \hat x_{k+1} & = A\hat x_k+Bu_k + Kz_{k+1}\\
	  u_k & = L\hat x_k\\
	\end{align*}
      \end{block}
    \end{minipage}
    \hspace{0.5cm}
    \begin{minipage}[t]{0.45\textwidth}
      \begin{block}{Compromised System}
	\begin{align*}
	  x'_{k+1} &= Ax'_k + Bu'_k + w_k\\
	  y'_k & = Cx'_k + v_k+\Gamma y_k^a \\
	  z'_{k+1} & = y'_{k+1} - C(A\hat x'_{k}+Bu'_{k})\\
	  \hat x'_{k+1} & = A\hat x'_k+Bu'_k + Kz'_{k+1}\\
	  u'_k & = L\hat x'_k\\
	\end{align*}
      \end{block}
    \end{minipage}
\end{frame}

\begin{frame}{Difference between the Compromised System and Healthy System}
  \begin{block}{Dynamics of the Difference}
    \begin{align*}
      \Delta x_{k+1} &= A\Delta x_k + Bu_k, &
      \Delta z_{k+1} =& \Delta y_{k+1} - C(A\Delta \hat x_{k}+B\Delta u_{k}),\\
      \Delta y_k & = C\Delta x_k +\Gamma y_k^a,&
      \Delta \hat x_{k+1} =& A\Delta \hat x_k+B\Delta u_k + K\Delta z_{k+1},\\
      \Delta u_k & = L\Delta\hat x_k.&
      & 
    \end{align*}
  \end{block}

  Since $y_k^a$ is independent of $w_k,\,v_k$, we can actually prove that $x'_k$ is Gaussian and
  \[E(x'_k) = \Delta x_k,\,Cov(x'_k) = Cov(x_k).\]
  Similar statement is also true for $y'_k,\,z'_k,\,\hat x'_k,\,u'_k$. Hence, to characterize the performance of control systems under false data injection attacks, we only need to focus on $\Delta x_k,\,\Delta y_k,\,\Delta z_k,\,\Delta \hat x_k,\,\Delta u_k$.
\end{frame}

\begin{frame}{Successful Attack}
  \begin{definition}
    A sequence of attacker's input $(y_0^a,\ldots,y_N^a)$ is called $\alpha$-feasible if during the attack,
    \begin{displaymath}
      D(z'_k\|z_k) = \Delta z_{k}^T \mathcal P^{-1}\Delta z_k/2\leq \alpha,\,\textrm{for} k=0,\ldots,N,
    \end{displaymath}
    where $D(z'_k\|z_k)$ is the KL distance between $z'_k$ and $z_k$. 
  \end{definition}

  \begin{enumerate}
    \item It can be proved that the probability of triggering an alarm at time $k$ is an increasing function of $D(z'_k\|z_k)$.

    \item If $\alpha$ goes to $0$, then the compromised system and healthy system are undistinguishable by the $\chi^2$ detector.
  \end{enumerate}
\end{frame}

\begin{frame}{Constrained Control Problem}
  \begin{enumerate}
    \item Under the requirement that $\Delta z_k^T \mathcal P^{-1}\Delta z_k/2\leq \alpha$, the action of the attacker can be formulated as a constrained control problem, where $y_k^a$ is the input from the attacker. 
      
    \item To characterize the resilience of control system, we need to compute the reachable region $R_k$ of $\Delta x_k$.

    \item In this talk, we will focus on finding a necessary and sufficient condition under which the union of all $R_k$ is unbounded, i.e. there exists an $\alpha$-feasible attack sequence that can push $\Delta x_k$ arbitrarily far away from $0$.
  \end{enumerate}
\end{frame}

\begin{frame}{Main Result}
  \begin{theorem}
    $\bigcup_{k=1}^\infty R_k$ is unbounded if and only if $A$ has an unstable eigenvalue and the corresponding eigenvector $v$ satisfies:
  \begin{enumerate}
    \item $Cv \in span(\Gamma)$, where $span(\Gamma)$ is the column space of $\Gamma$.
    \item $v$ is in the reachable space of the pair $(A-KCA,\,K)$.
  \end{enumerate}
  \end{theorem} 
  \begin{enumerate}
    \item To check the resilience of control system, one can find all the unstable eigenvector of $A$ and compute $Cv$.
    \item If $Cv$ is sparse, then the attacker only need to compromise a few sensors to launch an attack along the direction $v$.
    \item To improve the resilience, the defender could add more redundant sensors to measure every unstable mode.
  \end{enumerate}
\end{frame}

\section{Simulation}

\begin{frame}{Illustrative Example}
We consider a vehicle moving along the $x$-axis, which is monitored by a position sensor and velocity sensor. 
\begin{block}{System Description}
\begin{displaymath}
  \begin{split}
    \left[ {\begin{array}{*{20}c}
      \dot x_{k+1} \\
      x_{k+1} \\
\end{array}} \right] &= \left[ {\begin{array}{*{20}c}
   1 & 0  \\
   1 & 1  \\
\end{array}} \right]  \left[ {\begin{array}{*{20}c}
   \dot x_k  \\
   x_k  \\
\end{array}} \right]  +  \left[ {\begin{array}{*{20}c}
   1   \\
   0.5   \\
\end{array}} \right]u_k + w_k,\\
 y_{k,1} &= \dot x_k + v_{k,1},\\
 y_{k,1} &=  x_k + v_{k,2}.
  \end{split}
\end{displaymath}
\end{block}
We assume that $Q = R = W = I_2$, $U = 1$. The Kalman gain and LQG control gain are
\begin{displaymath}
  K =  \left[ {\begin{array}{*{20}c}
   0.5939&0.0793  \\
   0.0793&0.6944  \\
\end{array}} \right],\, L =   \left[ {\begin{array}{*{20}c}
  - 1.0285&-0.4345  \\
\end{array}} \right].
\end{displaymath}
\end{frame}

\begin{frame}{Illustrative Example}
  \begin{itemize}
    \item It is easy to check the only unstable eigenvector is $v = [0,\,1]^T$. 
    \item If the position sensor is compromised, then the attacker could push the state $x_k$ to infinity.
    \item If only the velocity sensor is compromised, then $\bigcup_{k=1}^\infty R_k$ is bounded. Here we use an ellipsoidal approximation to compute the inner and outer approximation of $\bigcup_{k=1}^\infty R_k$.
  \end{itemize}
\end{frame}

\begin{frame}{Position Sensor is Compromised}
   \begin{figure}
    \begin{center}
      \includegraphics[width = 0.6 \textwidth]{figure.jpg}
    \end{center}
    \caption{Evolution of $\Delta \dot x_k$, $\Delta x_k$ and $D(z'_k\|z_k)$}
  \end{figure} 
\end{frame}

\begin{frame}{Velocity Sensor is Compromised}
   \begin{figure}
    \begin{center}
      \includegraphics[width = 0.6 \textwidth]{figure1.jpg}
    \end{center}
    \caption{Inner and Outer Approximation of Reachable Region $\bigcup_{k=1}^\infty R_k$ under Constraint $D(z'_k\|z_k)\leq 1$}
  \end{figure} 
\end{frame}
\section{Conclusion}

\begin{frame}{Conclusion}
  In this presentation, we consider the false data injection attacks in control systems.
  \begin{itemize}
    \item We define the false data injection attack model.
    \item We formulate the action of the attacker as a constrained control problem.
    \item We prove an algebraic condition under which the attacker could successfully destabilize the system.
    \item We give a design criterion to improve the resilience of control systems against such kind of attacks.
  \end{itemize}
\end{frame}

\end{document}

