\documentclass[letterpaper,10pt]{IEEEconf}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}


\title{\LARGE \bf {False Data Injection Attacks in Control Systems}}

\author{Yilin $\textrm{Mo}$, Bruno $\textrm{Sinopoli}$
\thanks{
Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}

\maketitle
\begin{abstract}
This paper analyzes the effects of false data injection attacks on Control System. We assume that the system, equipped with a Kalman filter and LQG controller, is used to monitor and control a discrete linear time invariant Gaussian system. We further assume that the system is equipped with a failure detector. An attacker wishes to destabilize the system by compromising a subset of sensors and sending corrupted readings to the state estimator. In order to inject fake sensor measurements without being detected the attacker needs to carefully design its inputs to fool the failure detector, since abnormal sensor measurements usually trigger an alarm from the failure detector. We will provide a necessary and sufficient condition under which the attacker could destabilize the system while successfully bypassing the failure detector. A design method for the defender to improve the resilience of the CPS against such kind of false data injection attacks is also provided.
\end{abstract}

\section{Introduction}\label{sec:Introduction}
Cyber Physical Systems (CPS) refer to the embedding of widespread sensing, computation, communication and control into physical spaces~\cite{Lee:EECS}. Application areas are as diverse as aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation, most of which are safety-critical. The availability of cheap communication technologies such as the internet makes such infrastructures susceptible to cyber security threats, which may affect national security as some of them, such as the power grid, are vital to the normal operation of our society.  Any successful attack may significantly hamper the economy, the environment or may even lead to loss of human life. As a result, security is of primary importance to guarantee safe operation of CPS. The research community has acknowledged the importance of addressing the challenge of designing secure CPS~\cite{securityrisks}\cite{challengessecurity}.

The impact of attacks on the control systems is addressed in \cite{securecontrol}. The authors consider two possible classes of attacks on the CPS: Denial of Service (DoS) attacks and deception attacks (or false data injection attacks). The DoS attack prevents the exchange of information, usually either sensor readings or control inputs between subsystems, while false data injection attack affects the data integrity of packets by modifying their payloads. A robust feedback control design against DoS attacks is further discussed in \cite{robustdos}. We feel that false data injection attacks can be subtler than DoS attacks as they are in principle more difficult to detect and have not been thoroughly investigated. In this paper, we want to analyze the impact of false data injection attacks on control systems.

A significant amount of research effort has been carried out to analyze, detect and handle failures in control systems. Sinopoli et al. study the impact of random packet drops on controller and estimator performance\cite{intermittentlqg}. In \cite{failuredetection}, the author reviews several failure detection algorithms in dynamic systems. Results from robust control and estimation~ \cite{robustcontrol}, a discipline that aims at designing controllers and estimators that function properly under uncertain parameters or unknown disturbances, is also applicable to some control system failures. However, a large proportion of the literature assumes that the failure is either random or benign. On the other hand, a cunning attacker can carefully design its attack strategy and deceive both detectors and robust estimators. Hence, the applicability of failure detection algorithms is questionable in the presence of a smart attacker.

Before describing our problem setup we wish to review some of the existing literature concerning secure data aggregation over networks in the presence of compromised sensors. 
In \cite{resilientaggregation}, the author provides a general framework to evaluate how resilient the aggregation scheme is against compromised sensor data. Liu et al. study the estimation scheme in power grids and show that under  some assumptions the attacker can modify the state estimate undetected \cite{falsedata}. However, in both studies, the authors only consider static systems with estimators that rely exclusively upon current sensor measurements. In reality, in a control system the actions taken by the attacker will not only affect the current states but also the future ones. An attacker could potentially use this fact to perform its attack over time and destabilize the system. On the other hand, the dynamics of the system could be used by the failure detector since the attack may be detected in the near future even if it results undetectable when it first occurs.

In this paper, we study the effects of false data injection attacks on control systems. We assume that the control system, which is equipped with a Kalman filter, LQG controller and failure detector, is monitoring and controlling a linear time-invariant system. The attacker's goal is to destabilize the system by compromising a subset of sensors and sending altered readings to the state estimator. The attacker also wants to guarantee that its action can bypass the failure detector. Under these assumptions, we will give a necessary and sufficient condition under which the attacker could destabilize the system without being detected.

The rest of the paper is organized as follows: In Section~\ref{sec:classiccontrol} we formulate the problem by revisiting and adapting Kalman filter, LQG controller and failure detector to our scenario. In Section~\ref{sec:deceptionattack}, we define the threat model of false data injection attacks. In Section~\ref{sec:main} we prove a necessary and sufficient condition under which the attacker could destabilize the system. We will also give some design criteria to improve the resilience of the CPS against false data injection attacks. A numerical example is provided in Section~\ref{sec:simulation} to illustrate the effects of false data injection attacks on the CPS. Finally Section~\ref{sec:conclusion} concludes the paper. 

\section{Problem Formulation}\label{sec:classiccontrol}

In this section we model the CPS as a linear control system, which is equipped with a Kalman filter, LQG controller and failure detector. 

\subsection{Physical System}
We assume that the physical system has Linear Time Invariant (LTI) dynamics, which take the following form:
\begin{equation}
  x_{k+1} = Ax_k + Bu_k +  w_k, 
  \label{eq:systemdescription}
\end{equation}
where $x_k \in \mathbb R^n$ is the vector of state variables at time $k$, $u_k \in \mathbb R^p$ is the control input, $w_k\in \mathbb R^n$ is the process noise at time $k$ and $x_0$ is the initial state. $w_k,\,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$.  

\subsection{Kalman filter}
A sensor network is deployed to monitor the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are collected and sent to a centralized estimator. The observation equation can be written as  
\begin{equation}
  y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k  = [y_{k,1},\ldots,y_{k,m}]^T \in \mathbb R^m$ is a vector of measurements from the sensors, and $y_{k,i}$ is the measurement made by sensor $i$ at time $k$. $v_k \sim \mathcal N(0,\;R)$ is the measurement noise independent of $x_0$ and $w_k$. 

A Kalman filter is used to compute state estimation $\hat x_k$ from observations $y_k$s:
\begin{align}\label{eq:Kalmanfilter}
  \hat x_{0| -1} & =0 ,\, P_{0|-1}  = \Sigma,\\
  \hat x _{k + 1|k} & = A \hat x_{k} +Bu_k  , \, P_{k + 1|k}  = AP_{k} A^T  + Q,\,\nonumber\\
  K_k&= P_{k|k - 1} C^T (CP_{k|k - 1} C^T  + R)^{ - 1},  \nonumber\\
  \hat x_{k}& = \hat x_{k|k - 1}  + K_k (y_k  - C \hat x _{k|k - 1} ) ,\\
   P_{k} &= P_{k|k - 1}  -  K_k CP_{k|k - 1}.  \nonumber
\end{align}
Although the Kalman filter uses a time varying gain $K_k$, it is well known that this gain will converge if the system is detectable. In practice the Kalman gain usually converges in a few steps. We can safely assume the Kalman filter to be already in steady state. Let us define
\begin{equation}
  P \triangleq \lim_{k\rightarrow\infty}P_{k|k-1},\,K \triangleq P C^T (CP C^T  + R)^{ - 1}.
\end{equation}
The update equations of Kalman filter are as follows:
\begin{equation}
  \hat x_{k+1} = A \hat x_{k} + B u_k  + K \left[y_{k+1} - C(A\hat x_{k} + Bu_k)\right],
  \label{eq:kalmanestimator}
\end{equation}
For future analysis, let us define the residue $z_{k+1}$ at time $k+1$ to be
\begin{equation}
  z_{k+1} \triangleq y_{k+1} - C(A\hat x_k + B u_k).
  \label{eq:residue}
\end{equation}
\eqref{eq:kalmanestimator} can be simplified as
\begin{equation}
  \hat x_{k+1} = A \hat x_{k} + B u_k + K z_{k+1}.  
\end{equation}
The estimation error $e_k$ at time $k$ is defined as 
\begin{equation}
e_k \triangleq x_k - \hat x_k.  
\end{equation}
Manipulating \eqref{eq:kalmanestimator}, \eqref{eq:residue}, we get the following recursive equation:
\begin{equation}
  e_{k+1} = (A-KCA)e_k + (I-KC)w_k -K v_k.
\end{equation}

\subsection{LQG Controller}
An LQG controller is used to stabilize the system by minimizing the following objective function\footnote{We assume an infinite horizon LQG controller is implemented.}:
\begin{equation}
  J = \lim_{T\rightarrow \infty}\min_{u_0,\ldots,u_{T}}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right],
\end{equation}
where $W, U$ are positive semidefinite matrices and $u_k$ is measurable with respect to $y_0,\ldots,y_k$, i.e. $u_k$ is a function of previous observations. It is well known that the optimal controller of the above minimization problem is a fixed gain controller, which takes the following form:
\begin{equation}
  \label{eq:lqgcontrolinput}
    u_k =  -(B^TSB+U)^{-1}B^TSA\hat x_{k},
\end{equation}
where $u_k$ is the optimal control input and $S$ satisfies the following Riccati equation
\begin{equation}
  \label{eq:lqgcontrolS}
    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
\end{equation}
Let us define $L \triangleq -(B^TSB+U)^{-1}B^TSA$, then $u_k = L x_{k|k}$.  

The systems is stable if and only if $Cov(e_k)$ and $J$ are both bounded. In particular that implies both matrices $A-KCA$ and $A+BL$ are stable. In the rest of the paper, we will only consider stable systems. Further, we assume to be already in steady state, which means $\{x_k,\,y_k,\,\hat x_k\}$are stationary random processes.

\subsection{Failure Detector}
A failure detector is often used in control system. For example, a $\chi^2$ failure detector computes the following quantity
\begin{equation}
  g_k = z_k^T\mathcal P^{-1}z_k,
\end{equation}
where $\mathcal P$ is the covariance matrix of the residue $z_k$. Since $z_k$ is Gaussian distributed, $g_k$ is $\chi^2$ distributed with $m$ degrees of freedom. As a result, $g_k$ cannot be far away from $0$. The $\chi^2$ failure detector will compare $g_k$ with a certain threshold. If $g_k$ is greater than the threshold, then an alarm will be triggered.

Other types of failure detectors have also been considered by many researchers. In \cite{Beard_Report}\cite{jones_report}, the authors design a linear filter other than the Kalman filter to detect sensor shift or shift in matrices $A$ and $B$. The gain of such filter is chosen to make the residue of the filter more sensitive to certain shift, which helps to detect a particular failure. Willsky et al. A generalized likelihood ratio test to detect dynamics or sensor jump is also proposed by Willsky et al. in \cite{willsky_glr}.  

To make the discussion more general, we assume the detector implemented in the CPS triggers an alarm based on following event:
\begin{equation}
  g_k > threshold,  
\end{equation}
where $g_k$ is defined as
\begin{equation}
  g_k \triangleq g (z_k,\,y_k,\,\hat x_k,\ldots,z_{k-\mathcal T+1},\,y_{k-\mathcal T+1},\,\hat x_{k-\mathcal T+1}).  
\end{equation}
The function $g$ is continuous and $\mathcal T \in \mathbb N$ is the window size of the detector. It is easy to see for $\chi^2$ detector, $g_k = z_k^T \mathcal P^{-1} z_k$. We further define the probability of alarm for the failure detector to be
\begin{equation}
 \beta_k = P(g_k > threshold). 
  \label{eq:detectionrate}
\end{equation}

At a first glance, it seems that certain choice of $g$ function will affect detection differently. However, since the $\chi^2$ detector along with many other detectors performs detection by computing a certain function of $\hat x_k,\,y_k,\,z_k$, then none of these detectors will be able to distinguish the healthy system from the partial compromised system if, under the malicious attack, the vectors $\hat x_k,\, y_k,\, z_k$ have the same statistical properties as those of healthy system. In Section~\ref{sec:main}, we show how the attacker can systematically attack the system without being noticed by the failure detector if a particular algebraic condition holds.

\section{False Data Injection Attacks}\label{sec:deceptionattack}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section, we assume that a malicious third party wants to compromise the integrity of the system described in Section~\ref{sec:classiccontrol}. The attacker is assumed to have the following capabilities:
\begin{enumerate}
  \item It knows the system model: We assume that the attacker knows matrices $A,\,B,\,C,\,Q,\,R$ as described in Section~\ref{sec:classiccontrol} and the observation gain and control gain $K$, $L$.

  \item It can control the readings of a subset of the sensors, denoted by $S_{bad}$. As a result, \eqref{eq:sensordescription} now becomes
    \begin{equation}
      y'_k = C x'_k + v_k + \Gamma y_k^a,
      \label{eq:sensormalicious}
    \end{equation}
    where $ \Gamma = diag(\gamma_1,\ldots,\gamma_m)$ is the sensor selection matrix. $\gamma_i$ is a binary variable and $\gamma_i = 1$ if and only if $i \in S_{bad}$. $y_k^a$ is the malicious input from the attacker. Here we write the observations and states as $y'_k$ and $x'_k$ since they are in general different from those of the healthy system due to the malicious attack.

\item The intrusion begins at time $0$. As a result, the initial conditions for the partial compromised system will be
  $\hat x'_{-1} = 0$, $Ex_0 = 0$. 
\end{enumerate}

Figure~\ref{fig:diagram} shows the diagram of the partial compromised system.

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width = 0.45 \textwidth]{diagram.1}
  \end{center}
  \caption{System Diagram}
  \label{fig:diagram}
\end{figure}

\begin{definition}
An attack sequence $\mathcal Y$ is defined as an infinite sequence which takes the following form $y_0^a,\,y_1^a,\ldots$. 
\end{definition}
It is easy to see that all the states of the partially compromised system are a function of $\mathcal Y$. For example, $x'_k$ can be written as $x'_k(\mathcal Y)$. However, in order to simplify the notation, we will use $x'_k$ when there is no confusion. Under the previous assumptions, the new system dynamics can be written as 
\begin{equation}
  \label{eq:dynofcompromisedsys}
  \begin{split}
    x'_{k+1} &= Ax'_k +Bu'_k + w_k,\\
    y'_k &= Cx'_k + v_k + \Gamma y_k^a ,\\
    \hat x'_{k+1} & = A\hat x'_k + B u'_k + K \left[y'_{k+1} - C(A\hat x'_k +Bu'_k)\right],\\
    u'_k &= L \hat x'_k.
  \end{split}
\end{equation}
We can also define the new residue and estimation error respectively as
\begin{equation}
  z'_{k+1} \triangleq y'_{k+1} - C(A\hat x'_{k} + B u'_k),\,\,\, e'_k \triangleq x'_k - \hat x'_k.
\end{equation}

Finally, the new probability of alarm is defined as
\begin{equation}
\beta'_k = P(g'_k >   threshold),
\end{equation}
where
\begin{equation}
    g'_k \triangleq g (z'_k,\,y'_k,\,\hat x'_k,\ldots,z'_{k-\mathcal T+1},\,y'_{k-\mathcal T+1},\,\hat x'_{k-\mathcal T+1}).
\end{equation}
The differences between the two systems are defined as
\begin{align}
    \Delta x_k &\triangleq& x'_k - x_k, \Delta \hat x_{k} &\triangleq& \hat x'_{k} - \hat x_{k},\nonumber\\
    \Delta u_k &\triangleq& u'_k - u_k, \Delta y_{k} &\triangleq& y'_{k} - y_{k},\nonumber\\
    \Delta z_k &\triangleq& z'_k - z_k, \Delta e_{k} &\triangleq& e'_{k} - e_{k}, \Delta \beta_k =\beta'_k - \beta_k,
\end{align}
where $x_k,\,\hat x_k,\,y_k,\,u_k,\,\beta_k$ are given by equations \eqref{eq:systemdescription}, \eqref{eq:sensordescription}, \eqref{eq:kalmanestimator}, \eqref{eq:lqgcontrolinput}, \eqref{eq:detectionrate}. $\Delta x_k,\,\Delta \hat x_k,\,\Delta u_k,\,\Delta y_k,\,\Delta z_k,\,\Delta e_k,\,\Delta \beta_k$ represent the differences between the partially compromised system and the healthy system. 

The following definition defines what constitutes a ``successful'' attack.

\begin{definition}
  An attack sequence $\mathcal Y$ is $(\varepsilon,\alpha)$-successful if there exists $T\in \mathbb N$, such that the following holds:
\begin{displaymath}
  \begin{split}
  \|\Delta x_T(\mathcal Y)\| \geq \alpha ,\, \Delta \beta_k (\mathcal Y) \leq \varepsilon,\,\forall k=0,1,\ldots,T-1.
  \end{split}
\end{displaymath}
The system is called $(\varepsilon,\alpha)$-attackable if there exists a $(\varepsilon,\alpha)$-successful attack sequence $\mathcal Y$ on the CPS.
\end{definition}

\begin{remark}
  It is worth noticing that simply injecting a large $y_k^a$ will result in a large $\Delta z_k$ which, in turn, will induce the failure detector to trigger an alarm immediately. 
\end{remark}

Although the definition of $(\varepsilon,\alpha)$-attackable is simple, it is not so easy to verify whether a system is $(\varepsilon,\alpha)$-attackable, especially when the form of $g$ is complex. As a result, we will consider a limit case of $(\varepsilon,\alpha)$-attackability.

\begin{definition}
  A control system is perfectly attackable if there exists an attack sequence $\mathcal Y$ such that the following holds:
\begin{displaymath}
  \begin{split}
    \limsup_{k\rightarrow\infty}\|\Delta x_k(\mathcal Y)\| =\infty ,\, \|\Delta z_k(\mathcal Y)\|\leq 1,\,\forall k=0,1,\ldots,.
  \end{split}
\end{displaymath}
\end{definition}

The next theorem shows that perfect attackability implies $(\varepsilon,\alpha)$-attackability.

\begin{theorem}
  \label{theorem:infiniteattackable} 
 If a control system is perfectly attackable, then it is also $(\varepsilon,\alpha)$-attackable for any $\varepsilon,\alpha > 0$, 
\end{theorem}
\begin{proof}
  Since the system is perfectly attackable, there exists an attack sequence $\mathcal Y$, such that
  \begin{equation}
    \limsup_{k\rightarrow\infty}\|\Delta x_k(\mathcal Y)\| = \infty,\,\|\Delta z_k(\mathcal Y)\|\leq 1,k=0,\,1,\,\ldots
  \end{equation}
  Manipulating equations \eqref{eq:kalmanestimator} \eqref{eq:lqgcontrolinput} \eqref{eq:dynofcompromisedsys}, we can prove that:
  \begin{equation}
    \begin{split}
    \Delta \hat x_{k+1} &= (A+BL)\Delta \hat x_{k} + K\Delta z_{k+1},\\
    \Delta y_{k+1} &= \Delta z_{k+1} + C(A+BL)\Delta \hat x_{k}.
    \end{split}
  \end{equation}
 Stability of $A+BL$ is guaranteed by the stability of the original system. Therefore, if $\|\Delta z_k(\mathcal Y)\|\leq 1$ for all $k =0,1,\ldots$, then $\Delta \hat x_k(\mathcal Y)$ and $\Delta y_k(\mathcal Y)$ will be uniformly bounded for all $k$. Define the bounds to be 
 \begin{equation}
  M_1= \sup_k\|\Delta \hat x_k(\mathcal Y)\|,\,M_2=\sup_k\|\Delta y_k(\mathcal Y)\|,
 \end{equation}
  where $M_1,\,M_2< \infty$ are constants. Due to the continuity of $g$, there exists $\varepsilon' > 0$ such that if $\|\Delta z_k\|\leq \varepsilon',\,\|\Delta \hat x_k\|\leq \varepsilon',\,\|\Delta y_k\|\leq \varepsilon'$, then 
  \begin{displaymath}
   |P(g'_k > threshold) - P(g_k > threshold )| \leq \varepsilon,
  \end{displaymath}
  Since $\Delta z_k(\mathcal Y),\,\Delta \hat x_k(\mathcal Y),\,\Delta y_k(\mathcal Y)$ are uniformly bounded, by linearity, we can find $\delta > 0$, such that
 \begin{displaymath}
   \|\Delta z_k(\delta \mathcal Y)\|\leq \varepsilon',\,\|\Delta \hat x_k(\delta \mathcal Y)\|\leq \varepsilon',\,\|\Delta y_k(\delta \mathcal Y)\|\leq \varepsilon',\forall k.
 \end{displaymath}
  By the stationarity of the random process $\{x_k,\,y_k,\,\hat x_k\}$, we know that 
  \begin{displaymath}
   |P(g'_k > threshold) - P(g_k > threshold )| \leq \varepsilon,\,\forall k.
  \end{displaymath}
  Finally by linearity, \[ \limsup_{k\rightarrow \infty} \Delta x_k(\delta\mathcal Y) =\delta\limsup_{k\rightarrow \infty} \Delta x_k(\mathcal Y) =  \infty.\] Hence, $\delta\mathcal Y$ is an $(\varepsilon,\alpha)$-successful attack sequence and the system is $(\varepsilon,\alpha)$-attackable.
\end{proof}
In the next section, we will give a necessary and sufficient condition for a system to be perfectly attackable. 

\section{Main Result}\label{sec:main}
In this section, we will provide an algebraic condition to identify perfectly attackable system, which is given by the following theorem:
\begin{theorem}
  \label{theorem:fundamental}
The control system (\ref{eq:systemdescription}) is perfectly attackable if and only if $A$ has an unstable eigenvalue and the corresponding eigenvector $v$ satisfies:
  \begin{enumerate}
    \item $Cv \in span(\Gamma)$, where $span(\Gamma)$ is the column space of $\Gamma$.
    \item $v$ is a reachable state of the dynamic system $\Delta e_{k+1} = (A-KCA)\Delta e_k - K\Gamma y_{k+1}^a$.
  \end{enumerate}
\end{theorem}
Before proving the theorem, we need the following lemmas:
\begin{lemma}
  \label{lemma:alter}
  The CPS is perfectly attackable if and only if there exists an attack sequence $\mathcal Y$ such that
\begin{equation}
  \limsup_{k\rightarrow \infty}\|\Delta e_k\| = \infty,\, \|\Delta z_k\|\leq 1,\,k=-1,0,\ldots
  \label{eq:conditionerror}
\end{equation}
\end{lemma}
\begin{proof}
  The proof follows from the boundedness of $\Delta \hat x_k$ and the fact that $\Delta x_k = \Delta \hat x_k + \Delta e_k$. Due to space limitation the complete proof will be omitted.
%First we want to prove the sufficiency. Since $\|\Delta z_k\| \leq 1$ for all $k$, $\|\hat x_k\| \leq M_1$ for all $k$ and some $M_1 > 0$, as is shown in the proof of Theorem~\ref{theorem:infiniteattackable}. Since
%  \begin{displaymath}
%    \Delta x_k = \Delta \hat x_k + \Delta e_k,
%  \end{displaymath}
%  by triangular inequality
%  \begin{displaymath}
%    \limsup_{k\rightarrow \infty}\|\Delta x_k\| \geq \limsup_{k\rightarrow\infty} \|\Delta e_k\| - \|\Delta \hat x_k\| =\infty.
%  \end{displaymath}
%  Hence, $\mathcal Y$ is a successful attack. 
%
%  Similarly, one can prove that \eqref{eq:conditionerror} is also necessary.
\end{proof}
Using Lemma~\ref{lemma:alter}, we can use $\Delta e_k$ to prove that the system is perfectly attackable. The main advantages of substituting $\Delta x_k$ with $\Delta e_k$ is that $\Delta e_k$ follows a simpler recursive equation:
\begin{equation}
  \Delta e_{k+1} = (A-KCA)\Delta e_k - K\Gamma y_{k+1}^a. 
  \label{eq:deltaestimationerror}
\end{equation}

Moreover, 
\begin{equation}
  \Delta z_{k+1} = CA\Delta e_k + \Gamma y_{k+1}^a.  
\end{equation}

Before proving Theorem~\ref{theorem:fundamental}, we need an additional lemma:
\begin{lemma}
  \label{lemma:jordan}
  Let $p \in \mathbb R^n$ be a vector, and $\lim_{k\rightarrow \infty} A^kp \neq 0$, then there exists an unstable eigenvector $v$ of matrix $A$, such that $p \in span(p,\,A^2p,\,\ldots,A^{n-1}p)$. 
\end{lemma}
%\begin{proof}
%  First consider the invariant subspace decomposition of matrix $A$. Let subspace
%  \begin{equation}
%    Y_i = Ker[(\lambda_i  I_n- A)^{\mu_i}] ,
%  \end{equation}
%  where $Ker(\cdot)$ is the null space of a matrix, $\lambda_i$ is an eigenvalue of $A$ and $\mu_i$ is the index of the eigenvalue $\lambda_i$ (the size of the largest Jordan block corresponding to eigenvalue $\lambda_i$). By the property of Jordan decomposition, we know that 
%  \begin{equation}
%    \label{eq:decomposition}
%    \mathbb R^n =  \oplus_{i=1}^l Y_i, 
%  \end{equation}
%  where $l$ is the number of distinguished eigenvalues and $\oplus$ indicates the direct sum. As a result, vector $p$ can be written as
%  \begin{displaymath}
%    p = \sum_{i=1}^l p_i y_i, 
%  \end{displaymath}
%  where $p_i$ is a scalar and $y_i \in Y_i$. Since $\lim_{k\rightarrow \infty}A^kp \neq 0$, we know that there exists at least one unstable invariant subspace $Y_i$, such that the corresponding $p_i \neq 0$. Without loss of generality, we assume that $Y_1$ is the unstable invariant subspace and $p_1 \neq 0$. 
%
%  Now consider the vector
%  \begin{displaymath}
%    \begin{split}
%    p' &= \prod_{i=2}^l (\lambda_iI_n - A)^{\mu_i} p =  \sum_{i=1}^l\left[ p_i \prod_{i=2}^l (\lambda_i - A)^{\mu_i} y_i\right] \\
%    &= p_1 \prod_{i=2}^l (\lambda_iI_n - A)^{\mu_i} y_1
%    \end{split}
%  \end{displaymath}
%  By \eqref{eq:decomposition}, we know that $\prod_{i=2}^l (\lambda_iI_n - A)^{\mu_i} y_1\neq 0$, which means $p' \neq 0$. Moreover, we know that
%  \begin{displaymath}
%    (\lambda_1I_n - A)^{\mu_1} p' = p_1 \prod_{i=2}^l (\lambda_iI_n - A)^{\mu_i} (\lambda_1I_n - A)^{\mu_1}y_1= 0. 
%  \end{displaymath}
%  As an result
%  \begin{displaymath}
%    A\left[(\lambda_1I_n - A)^{\mu_1-1} p'\right] =  \lambda_1 \left[(\lambda_1I_n - A)^{\mu_1-1} p'\right]. 
%  \end{displaymath}
%  Hence, either $v_1 = (\lambda_1I_n - A)^{\mu_1-1} p' = (\lambda_1I_n - A)^{\mu_1-1} \prod_{i=2}^l (\lambda_i - A)^{\mu_i}p$ is an eigenvector or $v_1 = 0$. If it is an eigenvector, the $v_1 \in span(p,\,Ap,\ldots,\,A^{n-1}p)$, which finishes the proof. Otherwise we know that  
%  \begin{displaymath}
%    A\left[(\lambda_1I_n - A)^{\mu_1-2} p'\right] =  \lambda_1 \left[(\lambda_1I_n - A)^{\mu_1-2} p'\right]. 
%  \end{displaymath}
%  Since $p' \neq 0$, eventually we can find $v_i = (\lambda_1I_n - A)^{\mu_1-i} p'\neq 0$, which is an unstable eigenvector of matrix $A$ and $v_i \in span(p,\,Ap,\,\ldots,\,A^{n-1}p)$.
%\end{proof}
The proof is based on the Jordan decomposition of the $A$ matrix and on Carley-Hamilton Theorem. The complete proof is omitted due to space limits.  Now we are ready to prove Theorem~\ref{theorem:fundamental}.
\begin{proof}[Proof of Theorem~\ref{theorem:fundamental}]
  First we will prove the necessity. Suppose that CPS is perfectly attackable, then by Lemma~\ref{lemma:alter}, there exists an successful attack sequence $\mathcal Y$ such that
\begin{displaymath}
  \limsup_{k\rightarrow\infty}\|\Delta e_k\| = \infty ,\, \|\Delta z_k\| \leq 1,\,k=0,1,\ldots.
\end{displaymath}

 A peak subsequence $\{\Delta e_{i_k}\}$ from $\Delta e_i$ is defined as 
\begin{equation}
  \Delta e_{i_0} = \Delta e_0,\,\Delta e_{i_k} = \min\{j:\|\Delta e_j\|> \|\Delta e_{i_{k-1}}\|\},
\end{equation}
which means that the norm $\|\Delta e_{i_k}\|$ is larger than the norm of any preceding term in the original sequence. Since $\Delta e_k$ is unbounded, there always exists such a subsequence and $\lim_{k\rightarrow\infty}\Delta e_{i_k} = \infty$. Now consider the normalized vectors defined as
\begin{equation}
  p_{k} \triangleq \frac{1}{\|\Delta e_{k}\|} \Delta e_{k}.
\end{equation}

It is trivial to see $\|p_{k}\|$ is bounded. As a result, there exists an index set $\{j_k\}\subset\{i_k\}$ such that all of the subsequences $\{p_{j_k}\},\,\{p_{j_k - 1}\},\ldots,\{p_{j_k - n+1}\}$ converge as $k$ goes to infinity, due to Bolzano-Weierstrass theorem. Let us define
\begin{equation}
  q_l \triangleq \lim_{k\rightarrow \infty} p_{j_k-l},\,l=0,\,1,\,\ldots,\,n-1.  
\end{equation}

In addition, since
\[\|\Delta e_{k+1}\| = \|A\Delta e_k - K \Delta z_{k+1}\|\leq \|A\|\|\Delta e_k\| + \|K\|,\] 
and $\Delta e_{j_k}$ is unbounded, $ \lim_{k\rightarrow \infty}\Delta e_{j_k -l} = \infty$ for all $l$ from $0$ to $n-1$. As a result 
\begin{displaymath}
  \begin{split}
    &\lim_{k\rightarrow \infty} \frac{\Delta e_{j_k}}{\|\Delta e_{j_k-1}\|} = \lim_{k\rightarrow\infty}\frac{A\Delta e_{j_k-1}-K\Delta z_{j_k}}{\|\Delta e_{j_k-1}\|} \\
    &=  A\lim_{k\rightarrow\infty}\frac{\Delta e_{j_k-1}}{\|\Delta e_{j_k-1}\|} = Aq_{1}. \\
  \end{split}
\end{displaymath}
Therefore
\begin{displaymath}
  q_{0} =  \lim_{k\rightarrow \infty} \frac{\|\Delta e_{j_k-1}\|}{\|\Delta e_{j_k}\|}\lim_{k\rightarrow \infty} \frac{\Delta e_{j_k}}{\|\Delta e_{j_k-1}\|} = Aq_1/\|Aq_1\|.
\end{displaymath}

Similarly, it is easy to show that $q_l = Aq_{l+1}/ \|Aq_{l+1}\|$. Hence, 
\[span(q_0,\ldots,q_{n-1}) = span(A^{n-1}q_{n-1}, \ldots, Aq_{n-1},\,q_{n-1}).\]
By definition of $\{\Delta e_{i_k}\}$,  $\|\Delta e_{j_k}\| \geq \|\Delta e_{j_k-1}\|$. Thus, $\|Aq_1\|\geq \|q_1\|$, which implies that $\lim_{k\rightarrow\infty}A^k q_{n-1} \neq 0$. From Lemma~\ref{lemma:jordan} it follows that there exists an unstable eigenvector $v$ in the span of $q_0,\ldots,q_{n-1}$. Since 
\begin{displaymath}
  \|\frac{\Delta z_{j_k+1}}{\|\Delta e_{j_k}\|}\| =\| C p_{j_k}+\Gamma\frac{ y_{j_k+1}^a }{\|\Delta e_{j_k}\|}\|\leq \frac{1}{\|\Delta e_{j_k}\|},
\end{displaymath}
\begin{displaymath}
  Cp_{j_k}\in span(\Gamma)+ B(0,(\|\Delta e_{j_k}\|)^{-1}), 
\end{displaymath}
where $B(0,(\|\Delta e_{j_k}\|)^{-1})$ is a ball center at $0$ with radius $(\|\Delta e_{j_k}\|)^{-1}$. As a result
\begin{displaymath}
  Cq_{0}\in \bigcap_{l=1}^\infty\left[ span(\Gamma)+ B(0,(\|\Delta e_{j_k}\|)^{-1}) \right]= span(\Gamma). 
\end{displaymath}

Similarly, $CAq_l$ belongs to $span(\Gamma)$ for all $l$ from $0$ to $n-1$. As a result, $CAv \in span(CAq_0,\ldots,CAq_{n-1})\subset span(\Gamma)$, which implies $Cv\in span(\Gamma)$.

For reachability, since $\Delta e_{k}$ is reachable, $\alpha \Delta e_k$ is reachable for any $\alpha \in \mathbb R$. In particular, $p_{k}$ is reachable for all $k$. Since the reachable subspace is closed, the limit $q_l$ is reachable, which implies $v$ is reachable, thus proving the necessary condition.

We now want to prove sufficiency. Since $Cv \in span(\Gamma)$, there exists $y^*$ such that $\Gamma y^* = Cv$. Furthermore, since $v$ is reachable, there exist $y_0^a,\ldots,y_{n-1}^a$, where $n$ is the dimension of state space, such that $\Delta e_{n-1} = v$. Define 
  \begin{equation}
    M = \max_{k = 0,\ldots,n-1}\|\Delta z_{k}\|.
  \end{equation}
  By linearity, if the attacker injects $y_0^a/M,\ldots,y_{n-1}^a/M$, then $\Delta e_{n-1} = v/M$ and $\|\Delta z_k\|\leq 1$ for $k = 0,\ldots,n-1$. As a result, the attacker could choose the attack sequence to be
  \begin{equation}
    y^a_{n+i} = y^a_{i} - \frac{\lambda^{i+1}}{M}y^* ,\,i =0,1,\ldots
  \end{equation}
  One can prove that with the above attack sequence, the following equality and inequality hold :
  \begin{equation}
    \Delta e_{n+i} = \Delta e_i + \frac{\lambda^{i+1}}{M} v,\,i=0,1,\ldots,
  \end{equation}
\begin{equation}
  \| \Delta z_{n+i}\| =\|\Delta z_{i}\|\leq 1,\,i=0,1,\ldots 
\end{equation}
Since $|\lambda|\geq 1$, $\Delta e_{k} \rightarrow \infty$, which implies that the system is perfectly attackable.
\end{proof}

\begin{remark}
The attacker could use the results of Theorem~\ref{theorem:fundamental} to design an attack sequence $\mathcal Y$ based on the eigendecomposition of $A$ and the $\Gamma$ matrix.

On the other hand, the defender could also perform an eigendecomposition on $A$ matrix, find all the unstable eigenvector $v$ and then compute $Cv$. For each $Cv$, the non-zero elements will indicate the sensors needed by the attacker to perform a successful attack along direction $v$. Therefore if $Cv$ is a sparse vector, an attacker could initiate an attack on the direction of $v$ by compromising only a few sensors. As a result, the defender could increase the resilience of the system by installing redundant sensors to measure mode $v$. 
\end{remark}

\section{Illustrative Examples}\label{sec:simulation}
In this section, we will provide a numerical example to illustrate the effects of false data injection attacks.

Consider a vehicle moving along the $x$-axis. The state space includes position $x$ and velocity $\dot x$ of the vehicle. An actuator is used to control the speed of the vehicle. As a result, the system dynamics is as follows:
\begin{equation}
  \begin{split}
    \dot x_{k+1} &= \dot x_{k} + u_k + w_{k,1},\\
    x_{k+1} &=x_k + (\dot x_{k+1}+ \dot x_k)/2+ w_{k,2}\\
    &= x_k + \dot x_k + u_k/2 +w_{k,1}/2+w_{k,2},
  \end{split}
\end{equation}
which can be written in the matrix form as
\begin{equation}
  X_{k+1} = \left[ {\begin{array}{*{20}c}
   1 & 0  \\
   1 & 1  \\
\end{array}} \right] X_k +  \left[ {\begin{array}{*{20}c}
   1   \\
   0.5   \\,\end{array}} \right]
\end{equation}
where
\begin{equation}
X_k =   \left[ {\begin{array}{*{20}c}
  \dot{x}  \\
   x \\
\end{array}} \right] ,\, w_k = \left[ {\begin{array}{*{20}c}
  w_{k,1}  \\
  w_{k,2}+0.5w_{k,1} \\
\end{array}} \right].
\end{equation}
Suppose two sensors are measuring the velocity and position respectively. Hence
\begin{equation}
y_k = X_k + v_k.
\end{equation}
We assume the position sensor is compromised, i.e. $\Gamma = diag(0,1)$. We further impose the following parameters on the system
 \[Q = R =   W = I_2,\, U = 1.\]

The steady state Kalman gain and the LQG control gain under the previous assumptions are respectively
\begin{displaymath}
K =  \left[ {\begin{array}{*{20}c}
   0.5939 & 0.0793  \\
   0.0793 & 0.6944  \\
\end{array}} \right], L =  \left[ {\begin{array}{*{20}c}
   -1.0285 & -0.4345  \\
\end{array}} \right].  
\end{displaymath}

Since $[0 1]'$ is an unstable eigenvector and is in the span of $\Gamma$ and reachable, by Theorem~\ref{theorem:fundamental}, the system is perfectly attackable. Using the result we derived in Section~\ref{sec:main}, we design the attack sequence $\mathcal Y$ to be
\begin{equation}
  \begin{split}
  \label{eq:simattacksequence}
  y_{0}^a &=\left[ 0,\, -1.000 \right]',\,y_{1}^a = \left[ 0,\, -0.367 \right]',\\
 y_{k}^a &= y_{k-2}^a -  \left[ 0,\, -0.485 \right]',\,k \geq 2.
  \end{split}
\end{equation}

Figure~\ref{fig:evoofdiff} shows the evolution of the $\Delta X_k$ and $\Delta z_k$. It is easy to see that $\|\Delta z_k\|$ is always less than $1$ and $\Delta x_k$ goes to infinity, showing that the system is perfectly attackable.

\begin{figure}[<+htpb+>]
  \begin{center}
    \includegraphics[width=0.45\textwidth]{figure1.eps}
  \end{center}
  \caption{Evolution of $\Delta \dot{x_k},\,\Delta x_k,\,\|\Delta z_k\|$}
  \label{fig:evoofdiff}
\end{figure}


\section{Conclusion and Future Work}\label{sec:conclusion}

This paper proposes a false data injection attack model and analyze the effects of such kind of attacks on a linear time-invariant Gaussian control system. We prove the existence of a necessary and sufficient condition under which the attack could destabilize the system while successfully bypassing a large set of possible failure detectors. We also provide a design criterion to improve the resilience of the system to false data injection attacks.

Future work will be directed toward deriving conditions under which the system is $(\varepsilon,\alpha)$-attackable. We also plan to combine both the false data injection attacks and DoS attacks and study their effects on control systems.
\bibliographystyle{IEEEtran}
\bibliography{SNsecurity}
\end{document} 

