\documentclass[letterpaper,10 pt]{IEEEconf}

\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage{slashbox}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}


\title{\LARGE \bf {False Data Injection Attacks in Cyber Physical Systems}}

\author{Yilin $\textrm{Mo}^*$, Bruno $\textrm{Sinopoli}^*$
\thanks{
$*$: Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}

\maketitle
\begin{abstract}
This paper analyzes the effect of false data injection attacks on states estimation carried over sensor networks. We assume the sensor network is monitoring a discrete time linear time invariant Gaussian system and Kalman filter is used to perform state estimation. We also assume that the system is equipped with a $\chi^2$ failure detector. We assume an attacker wishes to attack the integrity of the state estimator in steady state by compromising a subset of sensors and sending bogus readings to the state estimator. Since abnormal sensor measurements will result in an alarm, in order to inject an fake sensor measurements without being detected the attacker need to carefully design his inputs to fool the estimator, which can be formulated as an optimization problem. We proved that the optimal cost of the problem can characterize how vulnerable the system is to such kind of attacks and thus is valuable not only to the attacker but also to the defender to realize there is certain vulnerability in the state estimator. An algorithm is also provided to approximate the optimal cost and the optimal attacker inputs.
\end{abstract}

\section{Introduction}\label{sec:Introduction}

Design and analysis of systems based on wireless sensor networks (WSNs) involves cross disciplinary research which spans domains within computer science, communication systems, control theory. A WSN is composed of low power devices that integrate computing with heterogeneous sensing and wireless communication. WSN based systems are usually embedded in the physical world, with which they interact by collecting, processing and transmitting relevant data.  In these applications, state estimator like Kalman filters are wildly used to perform model-based state estimation based on lumped-parameter models of the physical phenomena. Sensor networks span a wide range of applications, including environment monitoring and control, health care, home and office automation and traffic control \cite{wireless_sensor_network}, lots of which are safety critical. Any successful attack may significantly hamper the functionality of the sensor networks or may even lead to economy loss. In the meantime, sensors in WSNs are usually unreliable and physically exposed, which makes it difficult to ensure security and availability for every single sensors. The research community has acknowledged the importance of addressing the challenge of designing secure control and estimation algorithms, which ensure graceful degradation as sensors are compromised~\cite{securityrisks}\cite{challengessecurity}.

The impact of attacks on the control and estimation systems is addressed in \cite{securecontrol}. The authors consider two possible classes of attacks on CPS: Denial of Service (DoS) and deception (or false data injection) attacks . The DoS attack prevents the exchange of information, usually either sensor readings or control inputs between subsystems, while the false data injection attack affects the data integrity of packets by modifying their payloads. A robust feedback control design against DoS attack is further discussed in \cite{robustdos}. We feel that false data inject attack can be subtler than DoS attack as it is in principle more difficult to detect and has not adequately addressed. Hence, in this paper, we want to analyze the impact of false data injection attack on state estimator.

A significant amount of research effort has been carried out to analyze, detect and handle failures in sensor networks. Sinopoli et al. study the impact of random packet drops on controller and estimator performance\cite{kalmanintermittent}\cite{intermittentlqg}. In \cite{failuredetection}, the author reviews several failure detection algorithm in dynamic systems. Results from robust control and estimation~ \cite{robustcontrol}, a discipline that aims to design controllers and estimators that function properly under uncertain parameter or unknown disturbances, is applicable to some  sensor network failure. However, a large proportion of the literature assumes that the failure is either random or benign. On the other hand, a cunning attacker can carefully design his attack strategy and deceive both detectors and robust estimator. Hence, the applicability of failure detection algorithms is questionable in the presence of a smart attacker.

Several researches have been done for secure data aggregation over networks in the presence of compromised sensors. In \cite{resilientaggregation}, the author provides a general framework to evaluate how resilient the aggregation scheme is against bogus sensor data. Liu et al. study the estimation scheme in power grid and shows that in same circumstances the attacker could changes the state estimation undetected \cite{falsedata}. In both of the above works, the authors just consider that the estimator generates the current estimation only based on the current measurements of sensors. However, in reality the estimator could combine both current measurements and previous estimator to get a more accurate estimation of the system. 

In this paper, we study the effect of false data injection attacks on states estimation carried over sensor networks. We assume the sensor network is monitoring a discrete time linear time invariant Gaussian system and Kalman filter is used to perform state estimation. We also assume that the system is equipped with a $\chi^2$ failure detector. An attacker wishes to attack the integrity of the state estimator in steady state by compromising a subset of sensors and sending bogus readings to the state estimator. Since abnormal sensor measurements will result in an alarm, in order to inject an fake sensor measurements without being detected the attacker need to carefully design his inputs to fool the estimator, which can be formulated as an optimization problem. We proved that the optimal cost of the problem can characterize how vulnerable the system is to such kind of attacks and thus is valuable not only to the attacker but also to the defender to realize there is certain vulnerability in the state estimator. An algorithm is also provided to approximate the optimal cost and the optimal attacker inputs.

The rest of the paper is organized as follows: In Section~\ref{sec:classiccontrol} we provide the problem formulation by revisiting and adapting Kalman filter and $\chi^2$ failure detector to our scenario. In Section~\ref{sec:deceptionattack}, we define the threat model of false data injection attack and formulate it as optimization problem. In Section~\ref{sec:main} we discuss how to approximately solve the problem and give bounds on the optimal cost. Section shows several numerical examples of false data injection attack. Finally Section~\ref{sec:conclusion} concludes the paper. 

\section{Preliminary}\label{sec:classiccontrol}
In this section we will model the CPS system as a linear control system, which is equipped with an estimator, controller and failure detector. We will use the notation below for the remainder of the paper.

\subsection{Physical System}
We assume that the physical system which is monitored and controlled by the CPS is a Linear Time Invariant (LTI) system, which takes the following form:
\begin{equation}
  x_{k+1} = Ax_k + Bu_k +  w_k, 
  \label{eq:systemdescription}
\end{equation}
where  $x_k \in \mathbb R^n$ is the vector of state variables at time $k$, $u_k \in \mathbb R^p$ is the control inputs from CPS system, $w_k\in \mathbb R^n$ is the process noise at time $k$ and $x_0$ is the initial state. We assume $w_k,\,x_0$ are independent Gaussian random variables, $x_0 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$.  

\subsection{Estimator}
We assume that a sensor network is deployed to monitor the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are sent to a centralized estimator. The observation equation can be written as  
\begin{equation}
  y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k  = [y_{k,1},\ldots,y_{k,m}]^T \in \mathbb R^m$ is a vector of measurements from the sensors, and $y_{k,i}$ is the measurement made by sensor $i$ at time $k$. $v_k \sim \mathcal N(0,\;R)$ is the measurement noise which is independent of $x_0$ and $w_k$. 

An estimator is needed to compute state estimation $\hat x_k$ from observation $y_k$s. In practice, Kalman filter, which is the optimal state estimator, is widely used to perform state estimation, which takes the following form:
\begin{align}\label{eq:Kalmanfilter}
  \hat x_{0| -1} & =0 ,\, P_{0|-1}  = \Sigma,\\
  \hat x _{k + 1|k} & = A \hat x_{k} +Bu_k  , \, P_{k + 1|k}  = AP_{k} A^T  + Q,\,\nonumber\\
  K_k&= P_{k|k - 1} C^T (CP_{k|k - 1} C^T  + R)^{ - 1},  \nonumber\\
  \hat x_{k}& = \hat x_{k|k - 1}  + K_k (y_k  - C \hat x _{k|k - 1} ) ,\\
   P_{k} &= P_{k|k - 1}  -  K_k CP_{k|k - 1}.  \nonumber
\end{align}
Although the Kalman filter uses a time varying gain $K_k$, it is well known that this gain will converge if the system is detectable. In practice the Kalman gain usually converges in a few steps. Hence, let us define
\begin{equation}
  P \triangleq \lim_{k\rightarrow\infty}P_{k|k-1},\,K^* \triangleq P C^T (CP C^T  + R)^{ - 1}.
\end{equation}
Then the Kalman filter will takes the following form:
\begin{equation}
  \hat x_{k+1} = A \hat x_{k} + B u_k  + K^* \left[y_{k+1} - C(A\hat x_{k} + Bu_k)\right],
  \label{eq:kalmanestimator}
\end{equation}

Another type of filters, which tries to maximize the likelihood of $x_k$ given $y_k$, is also widely used in power systems. It takes the following form
\begin{equation}
  \hat x_{k} = K' y_k,  
  \label{eq:maxlikelihood}
\end{equation}
where $K' = (C^TR^{-1}C)^{-1}C^TR^{-1}$. It is worth noticing that such kind of filters requires observation matrix $C$ to be full column rank. 

In general, lots of state estimator, including the fixed gain Kalman filter and maximum likelihood estimator or $x_k$ given $y_k$, can be written in the following form: 
\begin{equation}
  \hat x_{k+1} = A \hat x_{k} + B u_k  + K \left[y_{k+1} - C(A\hat x_{k} + Bu_k)\right], \hat x_{-1} = 0.
  \label{eq:fixedgainestimator}
\end{equation}
where $\hat x_k$ is the state estimation of $x_k$ at time $k$, and $K$ is the observation gain matrix. It is well known that the state estimator is stable if and only if $A-KCA$ is stable. We would also like to define the residue $z_{k+1}$ at time $k+1$ to be
\begin{equation}
  z_{k+1} \triangleq y_{k+1} - C(A\hat x_k + B u_k).
\end{equation}
Hence, \eqref{eq:fixedgainestimator} can be simplified as
\begin{equation}
  \hat x_{k+1} = A \hat x_{k} + B u_k + K z_{k+1}.  
\end{equation}
We would also like to define the estimation error $e_k$ at time $k$ to be
\begin{equation}
e_k = x_k - \hat x_k.  
\end{equation}

\subsection{Controller}
Given the state estimation $\hat x_{k}$, a controller is used to generalize control input $u_k$ to stabilize the system or to improve the dynamics of the system. A particular example is the LQG controller, which tries to minimize the following objective function\footnote{Here we just discuss the case of infinite horizon LQG control problem.}:
\begin{equation}
  J =\min \lim_{T\rightarrow \infty}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right],
\end{equation}
where $W, U$ are positive semidefinite matrices and $u_k$ is measurable with respect to $y_0,\ldots,y_k$, i.e. $u_k$ is a function of previous observations. It is well known that the solution of the above minimization problem will lead to a fixed gain controller, which takes the following form:
\begin{equation}
  \label{eq:lqgcontrolinput}
    u_k = u_k^* = -(B^TSB+U)^{-1}B^TSA\hat x_{k},
\end{equation}
where $u_k^*$ is the optimal control input and $S$ satisfies the following Riccati equation
\begin{equation}
  \label{eq:lqgcontrolS}
    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
\end{equation}
Let us define $L^* \triangleq -(B^TSB+U)^{-1}B^TSA$, then $u_k^* = L^* x_{k|k}$.  

In general, we will assume that the CPS is equipped with a fixed gain controller, which takes the following form
\begin{equation}
u_k = L \hat x_k,
\label{eq:fixedgaincontroller}
\end{equation}
where $L$ is the control gain. The controller stabilize the system if and only if $A + BL$ is stable.

It is trivial to see that the physical system, estimator and controller of the CPS system is completely characterized by $A,\,B,\,C,\,Q,\,R,\,K,\,L$ matrices. Hence, we can define the control part of CPS system $\mathcal C$ as a $7$-tuple,
\begin{equation}
  \mathcal C \triangleq (A,\,B,\,C,\,Q,\,R,\,K,\,L).
  \label{eq:defofcps}
\end{equation}
The CPS system is stable if and only if both $A-KCA$ and $A+BL$ are stable. In this paper, we will constraint ourselves to stable CPS system.

\subsection{Failure Detector}
A failure detector is often used in control system for various reasons such as sensor failures detection or intrusion detection. For example, the $\chi^2$ failure detector,  computes the following quantities
\begin{equation}
  g_k = z_k^T\mathcal P^{-1}z_k,
\end{equation}
where $\mathcal P$ is the covariance matrix of the residue $z_k$. Since $z_k$ is Gaussian distribute, $g_k$ is $\chi^2$ distributed with $m$ degree of freedom\footnote{It can also be proved that if the optimal Kalman filter is used, then $g_k$ are independent of each other. }. As a result, $g_k$ cannot be far away from $0$. The $\chi^2$ failure detector will compare $g_k$ with a certain threshold. If $g_k$ is greater than the threshold, then the detector will trigger an alarm.

Other kinds of failure detectors have also been developed to detector certain abnormality in control systems. (Add a few examples here.)

However, all the above failure detectors perform detection by calculating certain functions of $\hat x_k,\,y_k,\,z_k$. As a result, if during the malicious attack, the vectors $\hat x_k,\, y_k,\, z_k$ have the same statistical property as the those of healthy system, then none of the above detector can distinguish the healthy system and the system under attack. In Section~\ref{sec:main}, we will show how the attacker can systematically attack the system without being noticed by the failure detector.

\section{False Data Injection Attacks}\label{sec:deceptionattack}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section, we assume that a malicious third party wants to attack the integrity of the system described in Section~\ref{sec:classiccontrol}. We will first describe an attack model based on the assumptions that the attacker knows the system and can modify part of the sensor readings and inject malicious control inputs to the system. We will then describe under what circumstances can the attacker perform an false data injection attack without being noticed by the system.

We suppose the attacker has the capability to perform the following actions:
\begin{enumerate}
  \item It knows the system model: We assume that the attacker knows matrices $A,\,C,\,Q,\,R$ as is described in Section~\ref{sec:classiccontrol} and the observation gain and control gain $K$, $L$. 

  \item It can control the readings of a subset of the sensors, which we denote as $S_{bad}$. As a result, \eqref{eq:sensordescription} now becomes
    \begin{equation}
      y'_k = C x'_k + v_k + \Lambda y_k^a,
      \label{eq:sensormalicious}
    \end{equation}
    where $ \Lambda = diag(\lambda_1,\ldots,\lambda_m)$ is the sensor selection matrix, where $\lambda_i$ is binary variable and $\lambda_i = 1$ if and only if $i \in S_{bad}$ and $y_k^a$ is the malicious input from the attacker. Here we write the observations and states as $y'_k$ and $x'_k$ since there are in general different from those of the healthy system due to the malicious attack.

  \item It can inject malicious control input to the system. This can be done by either compromising certain actuators of the CPS or adding malicious actuators to the system. (For example, in power grid, the attack could add a huge load to the system.)  As a result, the system dynamic described in \eqref{eq:systemdescription} becomes
    \begin{equation}
      x'_{k+1} = Ax'_k + Bu'_k +w_k+ B^au_k^a ,
      \label{eq:sysdynamicmalicious}
    \end{equation}
where $B^a u_k^a$ is the malicious control input from the attacker. $B^a$ is the input matrix of the attack which represents the direction to which the attack can disturb the system. Not that here we also write the input as $u'_k$.

\item We suppose that the intrusion begins at time $0$, as a result, the initial conditions for the system under the attack will be
  $\hat x'_{-1} = 0$, $Ex_0 = 0$. 
\end{enumerate}

\begin{remark}
For large systems, it is not so difficult for the attacker to compromises several sensors or actuators. However, it is usually hard for the attacker to know the parameters of the system. One possible scenario will be an insider attack, where the attacker itself is an administrator of the system. In that case, the attacker could access all the system parameters. Another way to know the system parameters is to do system identification. 
\end{remark}

\begin{definition}
An attack sequence $(\mathcal Y,\mathcal U)$ is defined as an infinite sequence which takes the following form $(y_0^a,u_0^a),\,(y_1^a,u_1^a),\ldots$.
\end{definition}


By the previous assumptions, we can write the new system dynamics as 
\begin{equation}
  \begin{split}
    x'_{k+1} &= Ax'_k +Bu'_k + w_k + B^au_k^a,\\
    y'_k &= Cx'_k + v_k + \Lambda y_k^a ,\\
    \hat x'_{k+1} & = A\hat x'_k + B u'_k + K \left[y'_{k+1} - C(A\hat x'_k +Bu'_k)\right],\\
    u'_k &= L \hat x'_k.
  \end{split}
\end{equation}
We can also define the new residue and estimation error as
\begin{equation}
  z'_{k+1} = y'_{k+1} - C(A\hat x'_{k} + B u'_k),\,e'_k = x'_k - \hat x'_k.
\end{equation}
Following the intuition that if the system under the attack is "close" to the healthy system, then it is hard to distinguish two systems, we would like to define the difference of two systems as
\begin{align}
    \Delta x_k &\triangleq& x'_k - x_k, \Delta \hat x_{k} &\triangleq& \hat x'_{k} - \hat x_{k},\nonumber\\
    \Delta u_k &\triangleq& u'_k - u_k, \Delta y_{k} &\triangleq& y'_{k} - y_{k},\nonumber\\
    \Delta z_k &\triangleq& z'_k - z_k, \Delta e_{k} &\triangleq& e'_{k} - e_{k},\\
\end{align}
where $x_k,\,\hat x_k,\,y_k,\,u_k$ is given by equations \eqref{eq:systemdescription}, \eqref{eq:sensordescription}, \eqref{eq:fixedgainestimator}, \eqref{eq:fixedgaincontroller}. Hence $\Delta x_k,\,\Delta \hat x_k,\,\Delta u_k,\,\Delta y_k,\,\Delta z_k,\,\Delta e_k$ represent the difference between the system under the attack and the healthy system. 

Now we want to characterize how vulnerable the system is against false data injection attack:
\begin{definition}
Given $\Lambda,\,B^a$, the CPS system $\mathcal C$ is called $\alpha$ attackable if there exists an attack sequence of $(\mathcal Y, \mathcal U)$ and $T \in \mathbb N$ such that the following holds
\begin{displaymath}
\|\Delta x_T\| \geq \alpha,\, \|\Delta z_k\| \leq 1,\,k=0,1,\ldots
\end{displaymath}
where $\|\cdot\|$ is the Euclidean norm.
\end{definition}
By the linearity of the system, it is trivial to see the following proposition holds:
\begin{proposition}
  \label{proposition:scalingproperty}
 The CPS system is $\alpha/\beta$ attackable if and only if there exists an attack sequence $(\mathcal Y,\,\mathcal U$ and $T \in \mathbb N$ such that
\begin{displaymath}
\|\Delta x_T\| \geq \alpha,\, \|\Delta z_k\| \leq \beta,\,k=0,1,\ldots
\end{displaymath}
\end{proposition}
\begin{definition}
  Given $\Lambda,\,B^a$, the CPS system $\mathcal C$ is called infinitely attackable if it is $\alpha$ attackable for any $\alpha \geq 0$.
\end{definition}
The following theorem shows that if a stable system is infinitely attackable, then the attack would pass a large number of failure detectors.
\begin{theorem}
  \label{theorem:infiniteattackable} 
  Suppose that the CPS system $\mathcal C$ is stable and is equipped with a failure detector, which takes the following form
  \begin{equation}
  g_k \lessgtr threshold,  
  \end{equation}
  where $g_k$ is defined as
  \begin{equation}
    g_k \triangleq g (z_k,\,y_k,\,\hat x_k,\ldots,z_{k-\mathcal T+1},\,y_{k-\mathcal T+1},\,\hat x_{k-\mathcal T+1}),  
  \end{equation}
  where $g$ is a continuous function and $\mathcal T \in \mathbb N$ is the window size of the detector. Suppose the CPS system $\mathcal C$ is infinitely attackable, then $\forall \varepsilon,\alpha > 0$, there exists an attack sequence and $T\in \mathbb N$, such that
  \begin{equation}
  \|\Delta x_T\|\geq \alpha,  
  \end{equation}
  and
  \begin{equation}
   |P(g'_k > threshold) - P(g_k > threshold )| \leq \varepsilon, k=0,1,\ldots
  \end{equation}
  where $g'_k$ is defined as
  \begin{equation}
    g'_k \triangleq g (z'_k,\,y'_k,\,\hat x'_k,\ldots,z'_{k-\mathcal T+1},\,y'_{k-\mathcal T+1},\,\hat x'_{k-\mathcal T+1}),  
  \end{equation}
\end{theorem}
\begin{proof}
  First we would like to find the relation between $\Delta \hat x_k,\,\Delta y_k$ and $\Delta z_k$. It is easy to prove that the following is true
  \begin{equation}
    \begin{split}
    \Delta \hat x_{k+1} &= (A+BL)\Delta \hat x_{k} + K\Delta z_{k+1},\\
    \Delta y_{k+1} &= \Delta z_{k+1} + C(A+BL)\Delta \hat x_{k},
    \end{split}
  \end{equation}
 Since $A+BL$ is stable, we know that if $\|\Delta z_k\|\leq 1$ for all $k =0,1,\ldots$, then $\Delta x_k$ and $\Delta y_k$ is always bounded for all $k$. Suppose that \[\|\Delta \hat x_k\|\leq M_1,\,\|\Delta y_k\|\leq M_2,\] where $M_1,\,M_2>0$ are constants. By linearity, we know that if $\|\Delta z_k\| \leq \delta$, then \[\|\Delta \hat x_k\|\leq \delta M_1,\,\|\Delta y_k\|\leq \delta M_2,\,\forall K.\] 
 
  Since the system is infinitely attackable, by the Proposition~\ref{proposition:scalingproperty}, we know that $\forall \delta>0$, there exists an attack sequence $(\mathcal Y,\mathcal U)$, such that for some $T \in \mathbb N$,
  \begin{equation}
  \|\Delta x_T\| \geq \alpha,\,\|\Delta z_k\|\leq \delta,k=0,\,1,\,\ldots
  \end{equation}

  Now construct an positive sequence $\delta_1,\,\delta_2,\ldots$ such that $\lim_{n\rightarrow \infty} \delta_n = 0$. Suppose the corresponding attack sequence to be $(\mathcal Y^{(n)},\mathcal U^{(n)})$ and $z'_k,\,y'_k,\,\hat x'_k,\,g'_k$ to be $z^{(n)}_k,\,y^{(n)}_k,\,\hat x^{(n)}_k,\,g^{(n)}_k$, Hence we know that
  \begin{displaymath}
    \begin{split}
    \|\Delta z^{(n)}_k\| &= \|z^{(n)}_k - z_k \| \leq \delta_n,\\
    \|\Delta \hat x^{(n)}_k\| &= \|\hat x^{(n)}_k - \hat x_k \| \leq \delta_n M_1,\\
    \|\Delta y^{(n)}_k\| &= \|y^{(n)}_k - y_k \| \leq \delta_nM_2,
    \end{split}
  \end{displaymath}
  By the continuity of $g$, we know that $g^{(n)}_k = g (z^{(n)}_k,\,y^{(n)}_k,\,\hat x^{(n)}_k,\ldots,z^{(n)}_{k-\mathcal T+1},\,y^{(n)}_{k-\mathcal T+1},\,\hat x^{(n)}_{k-\mathcal T+1})$ converges almost surely to $g_k$. Since almost surely convergence implies weak convergence, we know that 
  \[
  \lim_{n\rightarrow \infty}P(g_k^{(n)} > threshold) = P(g_k > threshold),
  \]
  which finishes the proof.
\end{proof}
In the next section, we will give a necessary and sufficient conditions on what kind of system $\mathcal C$ is infinitely attackable given $\Lambda,\,B^a$. Also we will discuss how to construct the sequence of attack input $(\mathcal Y,\,\mathcal U)$.

\section{Main Result}\label{sec:main}
In the section, we will try to identify which kind of system is infinitely attackable. First we would like to find a recursive equation for $\Delta e_k$. By manipulating equation \eqref{eq:systemdescription}, \eqref{eq:sensordescription}, \eqref{eq:fixedgainestimator}, we can get
\begin{displaymath}
  \begin{split}
    e_{k+1} &= x_{k+1} - \hat x_{k+1} = Ax_k + Bu_k + w_k \\
    &-\left\{ A\hat x_k + Bu_k + K\left[y_{k+1} -C(A\hat x_k + Bu_k) \right]\right\}\\
   & = Ae_k + w_k - K \left[C(Ax_k+Bu_k+w_k)+v_k - C(A\hat x_k + Bu_k)\right]\\
   & = (A-KCA)e_k+(I-KC)w_k -Kv_k. 
  \end{split}
\end{displaymath}

By the similar argument, we can get
\begin{displaymath}
  \begin{split}
    e'_{k+1} &= (A-KCA)e'_k + (I-KC)w_k - Kv_k \\
    &+ (I-KC)B^au_k^a - K\Lambda y_{k+1}^a. 
  \end{split}
\end{displaymath}

As a result,
\begin{equation}
  \Delta e_{k+1} = (A-KCA)\Delta e_k +(I-KC)B^au_k^a - K\Lambda y_{k+1}^a. 
  \label{eq:deltaestimationerror}
\end{equation}

Now we want to find the relation between $\Delta e_k$ and $\Delta z_k$. Since 
\begin{displaymath}
  \begin{split}
  z_{k+1} &= y_{k+1} - C(A\hat x_k + B u_k)\\
   &= C(A x_k + Bu_k+w_k)+v_k - C(A\hat x_k + B u_k)\\
   &=CAe_k +Cw_k+ v_k,
  \end{split}
\end{displaymath}
and
\begin{displaymath}
  \begin{split}
  z'_{k+1} &= y'_{k+1} - C(A\hat x'_k + B u'_k)\\
  &= C(A x'_k + Bu'_k+w_k+B^au_k^a)+v_k +\Lambda y_{k+1}^a - C(A\hat x'_k + B u'_k)\\
  &=CAe'_k + Cw_k+ v_k+ CB^au^a_k +\Lambda y_{k+1}^a,
  \end{split}
\end{displaymath}
we know that
\begin{equation}
  \Delta z_{k+1} = CA\Delta e_k + CB^au_k^a+\Lambda y_{k+1}^a.  
\end{equation}
Now we are ready to prove the following theorem:
\begin{theorem}
Given $\Lambda,\,B^a$ the CPS system $\mathcal C$ is infinitely attackable if and only if for all $\alpha \geq 0$, there exists a $T\in\mathbb N$ and an attack sequence $(\mathcal Y,\,\mathcal U)$, such that the following holds
\begin{equation}
  \|\Delta e_T\|\geq \alpha,\, \|CA \Delta e_k + CB^au_k^a + \Lambda y^a_{k+1}\|\leq 1,\,k=-1,0,\ldots
  \label{eq:conditionerror}
\end{equation}
\end{theorem}
\begin{proof}
  First we want to prove the sufficiency. Assume for any $\alpha > 0$, there exists an attack sequence $(\mathcal Y,\,\mathcal U)$, such that \eqref{eq:conditionerror} holds. By the fact that 
  \begin{displaymath}
  \Delta z_{k+1} = CA\Delta e_k + CB^au_k^a+\Lambda y_{k+1}^a,
  \end{displaymath}
  we know that $\|\Delta z_k\| \leq 1$ for all $k$. As a result, we know that $\|\hat x_k\| \leq M_1$ for all $k$ and some $M_1 > 0$, which is shown in the proof of Theorem~\ref{theorem:infiniteattackable}. Since
  \begin{displaymath}
    \Delta x_k = \Delta \hat x_k + \Delta e_k,
  \end{displaymath}
  by triangular inequality
  \begin{displaymath}
    \|\Delta x_T\| = \| \Delta e_T\| - \|\Delta \hat x_T\| \geq \alpha - M_1.
  \end{displaymath}
  Since $\alpha$ can be arbitrary large, the system is infinitely attackable. 

  By similar argument, we can show that \eqref{eq:conditionerror} is also necessary.
\end{proof}

Now we would like to define invariant set, which is useful for future analysis
\begin{definition}
  An $\beta$ invariant set $S_\beta$, where $\beta \geq 0$, is a subset of $\mathbb R^n$, such that $\forall e\in S_\beta$, there exists $u\in \mathbb R^p,\,y\in \mathbb R^m$, such that the following holds\footnote {Note that the set $\{0\}$ are $\beta$ invariant for all $\beta \geq 0$. Hence for any $\beta \geq 0$, there exists at least one $\beta$ invariant set.}
  \begin{equation}
    \begin{split}
 (A - KCA) e + (I - KC) B^a u  - K\Lambda y\in S_\beta,\\
  \|CAe+CB^au+\Lambda y\|\leq \beta. 
    \end{split}
    \label{eq:betainvariant}
  \end{equation}
\end{definition}

It is easy to see that the following proposition holds
\begin{proposition}
  \label{proposition:feasibleregion}
  Suppose that $S,\,S_i \subset \mathbb R^n,\,i\in \mathcal I$ are $\beta$ invariant sets, where $\mathcal I$ is the index set, then the following holds:  
  \begin{enumerate}
    \item $\cup_{i\in\mathcal I}S_i$ is a $\beta$ invariant set. 
    \item $\alpha S$ is a $\beta$ invariant set, for all $\alpha \in [0,1]$. 
    \item The closure of $S$ is a $\beta$ invariant set.  
    \item There exists the largest $\beta$ invariant region $\mathcal S_\beta$, such that $\forall S$ a $\beta$ invariant set, $S\subseteq \mathcal S_\beta$. Moreover, $\mathcal S_\beta$ is closed.
  \end{enumerate}
Moreover, if $\beta = 0$, then the following holds
\begin{enumerate}
  \item If $S$ is $0$ invariant, then $\alpha S$ is $0$ invariant, for all $\alpha \in \mathbb R$.
  \item If $S_1,\,S_2$ are $0$ invariant, then $S_1 + S_2 = \{x+y|x\in S_1,y\in S_2\}$ is also $0$ invariant.
  \item The maximal $0$ invariant set $\mathcal S_0$ is a subspace.
\end{enumerate}
\end{proposition}

By the linearity of \eqref{eq:betainvariant}, we can also prove the following proposition
\begin{proposition}
  \label{proposition:scaling2}
  Suppose that $\mathcal S_{\beta_1}$ and $\mathcal S_{\beta_2}$ are the maximal $\beta_1,\,\beta_2$ invariant sets with $\beta_1 > \beta_2 \geq 0$, then
  \begin{equation}
    \mathcal S_{\beta_2} \subset \mathcal S_{\beta_1}  
    \label{eq:orderofinvariant}
  \end{equation}
  and if $\beta_1 > \beta_2 > 0$, then
  \begin{equation}
    \mathcal S_{\beta_1} = \frac{\beta_1}{\beta_2}\mathcal S_{\beta_2},
  \end{equation}
  where set $\beta_1/\beta_2 \mathcal S_{\beta_2} = \{\beta_1x/\beta_2|x \in \mathcal S_{\beta_2}\}$.
\end{proposition}

By \eqref{eq:orderofinvariant}, we know that 
\begin{displaymath}
\mathcal S_0 \subset \cap_{\beta > 0 } \mathcal S_\beta.
\end{displaymath}
The following theorem states that $\mathcal S_0 =  \cap_{\beta > 0 } \mathcal S_\beta$.
\begin{theorem}
  \label{theorem:zeroinvariant}
  \begin{equation}
\mathcal S_0  = \cap_{\beta > 0 } \mathcal S_\beta.
  \end{equation}
\end{theorem}

The following theorem characterize under which condition the system is infinitely attackable
\begin{theorem}
  The CPS system $\mathcal C$ is infinitely attackable only if the following holds:
    \[\mathcal S_0 \neq \{0\}.\]
  Moreover, if the generalized eigenvalue problem 
  \begin{equation}
   P_B A x = \lambda P_B x, 
  \end{equation}
  where $P_B$ is the projection matrix defined as $P_B = B^a(B^{aT}B^a)^{-1}B^{aT}$, has an unstable eigenvector $v$, which satisfies
  \begin{enumerate}
    \item $v \in \mathcal S_0$,
    \item $Cv \in span(\Lambda)$,
    \item $v$ is reachable by linear system $\Delta e_{k+1} = (A-KCA)\Delta e_k + (I-KC)B^au^a_k -K\Lambda y_k^a$.
  \end{enumerate}
\end{theorem}
\begin{proof}
  Due to space limit, we will just give a sketch of the proof.

  First we would like to prove the necessity. Suppose that CPS system $\mathcal C$ is infinitely attackable, it is easy to show that $\mathcal S_1$ must be unbounded. Suppose ${a_1,a_2,\ldots}$ is an unbounded sequence of $\mathcal S_1$ with $\|a_{i+1}\| \geq \|a_i\|$. Hence, by scaling property, $\{\beta a_1,\,\beta a_2,\ldots\}$ is an unbounded sequence of $\mathcal S_\beta$. It is also easy to prove that the following sequence is bounded
  \begin{displaymath}
    \frac{\beta a_i}{\|\beta a_i\|+1} \in \mathcal S_\beta,
  \end{displaymath}
  and has exactly the same cluster points as
  \begin{displaymath}
    \frac{a_i}{\|a_i\|+1} \in \mathcal S_1.
  \end{displaymath}
  Moreover, all the cluster points are on the unit sphere. As a result, we know that all the cluster points belongs to $\cap_{\beta > 0} \mathcal S_\beta = \mathcal S_0$, and since the cluster point are on the unit sphere, $\mathcal S_0\neq \{0\}$.
  
  Then we want to prove the sufficiency. Since $v$ is reachable, we know that we could find an sequence $y_0^a, u_0^a,\ldots, y_{n-1}^a,u_{n-1}^a$, where $n$ is the dimension of $e_k$.  since the generalized eigenvalue problem has a solution, we know that
  \begin{displaymath}
   P_B A v - \lambda P_B v = B^a u^a.
  \end{displaymath}
  and 

\end{proof}

\section{Conclusion}\label{sec:conclusion}
In this paper we defined a false data injection attack model on state estimation over sensor networks. By formulation the attacker's actions and goals as an optimization problem, we quantify the resilience of estimation and failure detection schemes against the attack. We also provide a theoretical framework to approximately find the optimal attacker inputs and the resilience of the system.

\bibliographystyle{IEEEtran}
\bibliography{SNsecurity}
\end{document} 

