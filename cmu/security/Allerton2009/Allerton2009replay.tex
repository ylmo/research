\documentclass[letterpaper,10 pt,twocolumn]{IEEEconf}

\usepackage{color}
\usepackage[dvips]{graphicx}
%\DeclareGraphicsRule{*}{mps}{*}{}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\title{\LARGE \bf {Secure Control Against Replay Attacks}}


\author{Yilin Mo, Bruno Sinopoli\\
\begin{affiliation}
Department of Electrical and Computer Engineering\\
Carnegie Mellon University
\end{affiliation}\\
\email{ymo@andrew.cmu.edu},\email{brunos@ece.cmu.edu}
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\author{Yilin Mo, Bruno Sinopoli
\thanks{
The authors are with the Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}
\maketitle
\begin{abstract}
This paper analyzes the effect of replay attacks on a control system. We assume an attacker wishes to disrupt the operation of a control system in steady state. In order to inject an exogenous control input without being detected the attacker will hijack the sensors, observe and record their readings for a certain amount of time and repeat them afterwards while carrying out his attack. This is a very common and natural attack (we have seen numerous times intruders recording and replaying security videos while performing their attack undisturbed) for an attacker who does not know the dynamics of the system but is aware of the fact that the system itself is expected to be in steady state for the duration of the attack. We assume the control system to be a discrete time linear time invariant gaussian system applying an infinite horizon Linear Quadratic Gaussian (LQG) controller. We also assume that the system is equipped with a $\chi^2$ failure detector. The main contributions of the paper, beyond the novelty of the problem formulation, consist in 1) providing conditions on the feasibility of the replay attack on the aforementioned system and 2) proposing a countermeasure that guarantees a desired probability of detection (with a fixed false alarm rate) by trading off either detection delay or LQG performance, either by decreasing control accuracy or increasing control effort.
\end{abstract}

\section{Introduction}\label{Introduction}
Cyber Physical Systems (CPS) refer to the embedding of widespread sensing, computation, communication and control into physical spaces~\cite{Lee:EECS}. Application areas are as diverse as aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. Many of these applications are safety-critical. The availability of cheap communication technologies as the internet makes such infrastructures susceptible to cyber security threats. National security may be affected as infrastructures such as the power grid, the telecommunication networks are vital to the normal operation of our society. Any successful attack may significantly hamper the economy, the environment or may even lead to loss of human life. As a result, the role security of CPS is of primary importance to guarantee safe operation of CPS. The research community has acknowledged the importance of addressing the challenge of designing secure CPS~\cite{securityrisks}\cite{challengessecurity}.

The impact of attacks on the cyber physical systems is addressed in \cite{securecontrol}. The authors consider two possible classes of attacks on CPS: Denial of Service (DoS) and deception attacks. The DoS attack prevents the exchange of information, usually either sensor readings or control inputs between subsystems, while the deception attack affects the data integrity of packets by modifying their payloads. A robust feedback control design against DoS attack is further discussed in \cite{robustdos}. We feel that the deception attack can be subtler than DoS attack as it is in principle more difficult to detect and it has not adequately addressed. Hence, in this paper, we will develop a methodology to detect a particular kind of deception attack.

A significant amount of research effort has been carried out to analyze, detect and handle failures in CPS. Sinopoli et al. study the impact of random packet drops on controller and estimator performance\cite{kalmanintermittent}\cite{intermittentlqg}. In \cite{failuredetection}, the author reviews several failure detection algorithm in dynamic systems. Results from robust control~ \cite{robustcontrol}, a discipline that aims to design controllers that function properly under uncertain parameter or unknown disturbances, is applicable to some CPS scenarios. However, a large proportion of the literature assumes that the failure is either random or benign. On the other hand, a cunning attacker can carefully design his attack strategy and deceive both detectors and robust controllers. Hence, the applicability of failure detection algorithms is questionable in the presence of a smart attacker.

In this paper, we study the effect of a data replay attack on control systems. We assume an attacker wishes to disrupt the operation of a control system in steady state. In order to inject an exogenous control input without being detected the attacker will hijack the sensors, observe and record their readings for a certain amount of time and repeat them afterwards while carrying out his attack. This is a very common and natural attack (we have seen numerous times intruders recording and replaying security videos while performing their attack undisturbed) for an attacker who does not know the dynamics of the system but is aware that the system itself is expected to be in steady state for the duration of the attack. We assume the control system to be a discrete time linear time invariant (LTI) Gaussian system applying an infinite horizon Linear Quadratic Gaussian (LQG) controller. We also assume that the system is equipped with a $\chi^2$ failure detector. The main contributions of the paper, beyond the novelty of the problem formulation, consist in providing conditions on the feasibility of the replay attack on the aforementioned attack and suggesting a countermeasure that guarantees a desired probability of detection (with a fixed false alarm rate) by trading off either detection delay or LQG cost, i.e. either by decreasing control accuracy or increasing control effort.

The rest of the paper is organized as follows: In Section~\ref{sec:classiccontrol}, we provide the problem formulation by revisiting and adapting Kalman filter, LQG controller and $\chi^2$ failure detector to our scenario. In Section~\ref{sec:replayattack}, we define the threat model of replay attack and analyze its effect on the control schemes discussed in Section~\ref{sec:classiccontrol}. In Section~\ref{sec:secure} we discuss one possible countermeasure, the efficiency of which is illustrated by several numerical examples in Section~\ref{sec:Simulation}. Finally Section~\ref{sec:conclustion} concludes the paper. The appendix contains several proofs, some of which had to be removed due to space constraints.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Problem Formulation}\label{sec:classiccontrol}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section we will formulate the problem by deriving the Kalman filter, the LQG controller and $\chi^2$ detector for our case. We will use the notation below for the remainder of the paper.

Consider the following linear, time invariant (LTI) system whose state dynamics are given by
\begin{equation}
  x_{k+1} = Ax_k +  Bu_k + w_k, 
  \label{eq:systemdescription}
\end{equation}
where  $x_k \in \mathbb R^n$ is the vector of state variables at time $k$, $w_k\in \mathbb R^n$ is the process noise at time $k$ and $x_0$ is the initial state. We assume $w_k,\,x_0$ are independent Gaussian random variables, $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$.  

A sensor network is  monitoring the system described in \eqref{eq:systemdescription}. At each step all the sensor readings are sent to a base station. The observation equation can be written as  
\begin{equation}
  y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k \in \mathbb R^m$ is a vector of measurements from the sensors and $v_k \sim \mathcal N(0,\;R)$ is the measurement noise independent of $x_0$ and $w_k$. 
%%%%%%%%%%%%%%%
\subsection{Kalman Filter}
%%%%%%%%%%%%%%%
It is well known that for the system of equations \eqref{eq:systemdescription}, \eqref{eq:sensordescription} the Kalman filter is the optimal estimator as it provides the minimum variance unbiased estimate of the state $x_k$ given the previous observations $y_0,\ldots,y_k$. The Kalman filter is recursive and it takes the following form:
\begin{align}\label{eq:Kalmanfilter}
  \hat x_{0| -1} & = \bar x_0 ,\, P_{0|-1}  = \Sigma,\\
  \hat x _{k + 1|k} & = A \hat x_{k|k} + Bu_k  , \, P_{k + 1|k}  = AP_{k|k} A^T  + Q,\,\nonumber
  \\  K_k&= P_{k|k - 1} C^T (CP_{k|k - 1} C^T  + R)^{ - 1},  \nonumber\\
  \hat x_{k|k}& = \hat x_{k|k - 1}  + K_k (y_k  - C \hat x _{k|k - 1} ) ,\, P_{k|k} = P_{k|k - 1}  -  K_k CP_{k|k - 1}.  \nonumber
\end{align}
Although the Kalman filter uses a time varying gain $K_k$, it is known that this gain will converge if the system is detectable. In practice the Kalman gain usually converges in a few steps. Hence, let us define
\begin{equation}
  P \triangleq \lim_{k\rightarrow\infty}P_{k|k-1},\,K \triangleq P C^T (CP C^T  + R)^{ - 1}.
\end{equation}

Since control systems usually run for a long time, we can assume to be running at steady state from the beginning. Hence, we assume initial condition $\Sigma = P$. In that case, the Kalman filter is a fixed gain estimator, taking the following form
\begin{align}\label{eq:Kalmanfilter2}
  \hat x_{0| -1} & = \bar x_0,\,\hat x _{k + 1|k}  = A \hat x_{k|k} +Bu_k, \, \hat x_{k|k} = \hat x_{k|k - 1}  + K (y_k  - C \hat x _{k|k - 1} ) . \nonumber
\end{align}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Linear Quadratic Gaussian (LQG) Optimal Control}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given the state estimation $\hat x_{k|k}$, the LQG controller minimizes the following objective function\footnote{Here we just discuss the case of infinite horizon LQG control problem.}:
\begin{equation}
  J =\min \lim_{T\rightarrow \infty}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right],
\end{equation}
where $W, U$ are positive semidefinite matrices and $u_k$ is measurable with respect to $y_0,\ldots,y_k$, i.e. $u_k$ is a function of previous observations.  It is well known that the solution of the above minimization problem will lead to a fixed gain controller, which takes the following form:
\begin{equation}
  \label{eq:lqgcontrolinput}
    u_k = u_k^* = -(B^TSB+U)^{-1}B^TSA\hat x_{k|k},
\end{equation}
where $u_k^*$ is the optimal control input and $S$ satisfies the following Riccati equation
\begin{equation}
  \label{eq:lqgcontrolS}
    S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
\end{equation}
Let us define $L \triangleq -(B^TSB+U)^{-1}B^TSA$, then $u_k^* = L x_{k|k}$.  

The objective function given by the optimal estimator and controller is in our case is
\begin{equation}
  \label{eq:optlqgcostfunction}
  J =  trace(SQ)+trace[(A^TSA+W-S)(P-KCP)].
\end{equation}
%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{$\chi^2$ Failure Detector}
%%%%%%%%%%%%%%%%%%%%%%%
The $\chi^2$ detector\cite{chisquare} is widely used to detect anomalies in control systems. Before introducing the detector, we will characterize the probability distribution of the residue of the Kalman filter: 
\begin{theorem}
  \label{theorem:pdf}
For the LTI system defined in \eqref{eq:systemdescription} with Kalman filter and LQG controller, the residues $y_i - C\hat x_{i|i-1}$ of Kalman filter are i.i.d. Gaussian distributed with $0$ mean and covariance $\mathcal P$, where $\mathcal P = CPC^T + R.$
\end{theorem}
\begin{proof}
  Due to space constraints, we cannot give the proof here. Please refer to \cite{chisquare} for the details.
\end{proof}

By Theorem~\ref{theorem:pdf},  we know that the probability to get the sequence $y_{k-\mathcal T+1},\ldots,y_k$ when the system is operating normally is
\begin{equation}
  P(y_{k-\mathcal T+1},\ldots,y_k) = \left[\frac{1}{(2\pi)^{N/2}|\mathcal P|}\right]^{\mathcal T} exp(-\frac{1}{2}g_k),
\end{equation}
where 
\begin{equation}
   g_k=\sum_{i = k-\mathcal T+1}^k (y_i - C\hat x_{i|i-1})^T\mathcal P^{-1}(y_i - C\hat x_{i|i-1}).
\end{equation}
When this probability is low, it means that the system is likely to be subject to certain failure. In order to check the probability, we only need to compute $g_k$. Hence, the $\chi^2$ detector at time $k$ takes the following form
\begin{equation}
  g_k=\sum_{i = k-\mathcal T+1}^k (y_i - C\hat x_{i|i-1})^T\mathcal P^{-1}(y_i - C\hat x_{i|i-1})\lessgtr threshold,
  \label{eq:chisquredetector}
\end{equation}
where $\mathcal T$ is the window size of detection. By Theorem~\ref{theorem:pdf}, the left of the equation is $\chi^2$ distributed with $m\mathcal T$ degrees of freedom\footnote{The degrees of freedom is from the definition of $\chi^2$ distribution. Please refer to \cite{scharf} for more details.}.  Hence, it is easy to calculate the false alarm rate from $\chi^2$ distribution. If $g_k$ is greater than the  threshold, then the detector will trigger an alarm.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Replay Attack against Control System}\label{sec:replayattack}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
In this section, we assume that a malicious third party wants to break the control system described in Section~\ref{sec:classiccontrol}. We will define an attack model similar to the replay attack in computer security and analyze the feasibility of such kind of attack on the control system. We will later generalize our analysis to other classes of control systems. 

We suppose the attacker has the capability to perform the following actions:
\begin{enumerate}
  \item It can inject a control input $u_k^a$ into the system anytime.
    %For example she can put an entire floor of the skyscraper on fire raising the temperature drastically and changing the overall state.
  \item It knows all sensor readings and can modify them. We will denote the reading modified by the attacker by $ y'_k$.  %This can be done by either break the secure protocol of the sensor networks, or simply measuring and changing the physical reality the sensors are sensing. For the skyscraper example, the attacker can use thermometers to get the reading of sensors and then use heating or cooling devices to manipulate the reading of sensors.  
\end{enumerate}

Given these abilities, the attacker will implement the following attack strategy, which can be divided into two stages:
\begin{enumerate}
  \item The attacker records a sufficient number of $y_k$s without giving any input to the system. 
  \item The attacker gives a sequence of desired control input while replaying the previous recorded $y_k$s.
\end{enumerate}

\begin{remark}
 The attack on the sensors can be done by breaking the cryptography algorithm. Another way to perform an attack, which we think is much harder to defend, is to induce false sensor readings by changing the local conditions around it. Such attack may be easy to carry out when sensors are spatially distributed in remote locations. 
\end{remark}

\begin{remark}
 We assume that the attacker has control over all the sensors. This could be accomplished for a smaller system consisting of few sensors. For a large system, usually the whole system can be break down to several small and weakly coupled subsystems. For example consider the temperature control problem in a building. One can think of the temperature in each room as subsystems, which will hardly affects each other. Hence, the attacker only needs to control the sensors of a small subsystem in order to perform the replay attack on the subsystem. 
 \end{remark}
\begin{remark}
The attack strategy is fairly simple. In principle, if the attacker has more knowledge of the system model, the controller design, it can perform a much more subtle and powerful attack. However, to identify the underlying model of the system is usually a hard problem and not all the attackers have the knowledge and power to do so. Hence, we will only focus on a simple, easy to implement attack strategy which is easy to implement.
 \end{remark}
\begin{remark}
When the system is under attack, the central computer will be unable to perform close loop control on the system since the sensory information is not available. Hence, we cannot guarantee any control performance of the system under this attack. Any counterattack will need to be able to detect the attack.
 \end{remark}

It is worth noticing that in the attacking stage, the goal of the attacker is to make the fake readings $y'_k$s look normal $y_k$s. Replaying the previous $y_k$s is just the easiest way to achieve this goal. There are other methods, such as machine learning, to generate a fake sequence of readings. In order to provide a unified framework to analyze such kind of attack, we can think of $y'_k$s as the output of the following virtual system (this does not necessarily mean that the attacker runs a virtual system):
\begin{align*}
  x'_{k+1} &= Ax'_{k} + Bu'_k + w'_k,\; y'_k  = Cx'_{k} + v'_k,\\
  \hat x'_{k+1|k}&= A\hat x'_{k|k} + Bu'_k,\; \hat x'_{k+1|k+1}  = \hat x'_{k+1|k} + K(y'_k-\hat x'_{k+1|k}),\; \\
  u'_k &= L\hat x'_{k|k},
\end{align*}
with initial conditions $x'_0$ and $\hat x'_{0|-1}$. If the attacker actually learns the system, then the virtual system will be the system the attacker runs. For the replay attack, suppose that the attacker records the sequence $y_k$s from time $t$ time. Then the virtual system is just a time shifted version of the real system, with $x'_k = x_{t+k}$, $\hat x'_{k|k} = \hat x_{t+k|t+k}$ (Note that the attacker may not know $x_{t+k}$ and $\hat x_{t+k|t+k}$).

Suppose the system is under attack and the defender is using the $\chi^2$ detector to perform intrusion detection. We will rewrite the estimation of the Kalman filter $\hat x_{k|k-1}$ in the following recursive way: 
\begin{equation}
\begin{split}
\hat x_{k+1|k} &= A\hat x_{k|k} + Bu_k = (A+BL)\hat x_{k|k}\\
&=(A+BL) [\hat x_{k|k-1} + K(y'_k - C\hat x_{k|k-1})]\\
&=(A+BL)(I-KC)\hat x_{k|k-1} +(A+BL) Ky'_k. \\
  \end{split}
  \label{eq:estimationrecursive}
\end{equation}
For the virtual system, it is easy to see that the same equation holds true for $\hat x'_{k|k-1}$:
\begin{equation}
    \hat x'_{k+1|k} =(A+BL)(I-KC)\hat x'_{k|k-1} +(A+BL) Ky'_k. \\
\end{equation}
Define $\mathcal A \triangleq (A+BL)(I-KC)$, then\footnote{For simplicity, here we consider the time the attack begins as time $0$.}
\begin{equation}
  \hat x_{k|k-1} - \hat x'_{k|k-1} = \mathcal A^k( \hat x_{0|-1} - \hat x'_{0|-1}).
\end{equation}
Define $\hat x_{0|-1} - \hat x'_{0|-1}\triangleq \zeta$. Now write the residue as
\begin{equation}
  y_k' - C\hat x_{k|k-1} =( y_k' - C\hat x'_{k|k-1}) + C\mathcal A^k\zeta,
\end{equation}
and
\begin{equation}
  \begin{split}
    g_k & = \sum_{i = k-\mathcal T+1}^k \left[(y'_i - C\hat x'_{k|k-1})^T\mathcal P^{-1}(y'_i - C\hat x'_{k|k-1})\right. \\
    & \left.+ 2 (y'_i - C\hat x'_{k|k-1})^T\mathcal P^{-1}C\mathcal A^{i}\zeta+ \zeta^T(\mathcal A^i)^TC^T\mathcal P^{-1}C\mathcal A^{i}\zeta\right].
  \label{eq:chisquareresult}
\end{split}
\end{equation}
By the definition of the virtual system, we know that $y'_k-C\hat x'_{k|k-1}$ follows exactly the same distribution as $y_k - C\hat x_{k|k-1}$. Hence, if $\mathcal A$ is stable, the second term and the third term in \eqref{eq:chisquareresult} will converge to $0$. As a result, $y'_{k} - C\hat x_{k|k-1}$ will converges to the same distribution as $y_k - C\hat x_{k|k-1}$, and the detection rate given by $\chi^2$ detector will be the same as false alarm rate. In other words, the detector is useless.  

On the other hand, if $\mathcal A$ is unstable, the attacker cannot replay $y'_k$ for long since $g_k$ will soon become unbounded. In this case, the system is resilient to the replay attack, as the detector will be able to detect the attack.
It turns out the feasibility result derived for a special estimator, controller, and detector implementations is actually applicable to virtually any system.
In fact we can generalize the technique used here to analyze more general controller, estimator and detectors. Suppose the state of the estimator at time $k$ is $s_k$ and it evolves according to
\begin{equation}
  s_{k+1} = f(s_k,y_k).
\end{equation}
Define the norm of $f$ to be 
\begin{equation}
  \left\| f \right\| \triangleq \sup_{\Delta s\neq 0, y,s} \frac{\left\|f(s,y)-f(s+\Delta s,y)\right\|}{\left\|\Delta s\right\|}.
\end{equation}
Suppose that the defender is using the following criterion to perform intrusion detection
\begin{equation}
g(s_k,y_k)\lessgtr threshold,
\end{equation}
where $g$ is an arbitrary continuous function. 
\begin{theorem}
If $\left\|f\right\| \leq 1$, then
\begin{equation}
  \lim_{k\rightarrow \infty} g(s_k,\,y'_k) = g(s'_k,\,y'_k),  
\end{equation}
where $s'_k$ is the states variables of the virtual system. The detection rate $\beta_k$ at time $k$ converges to
\begin{equation}
  \lim_{k\rightarrow \infty}\beta _k = \alpha_k,
\end{equation}
where $\alpha_k$ is the false alarm rate of the virtual system at time $k$.
\end{theorem}
\begin{proof}
 Due to space limit, we will just give an outline of the proof. First, $\left\|f\right\| \leq 1$ will ensure that $s_k$ converges to $s'_k$. By the continuity of $g$, $g(s_k,\,y'_k)$ converges to $g(s'_k,\,y'_k)$. The detection rate of the system and the false alarm rate of the virtual system are given by
 \begin{equation}
   \begin{split}
  \beta_k &= Prob(g(s_k,\,y'_k) > threshold),\\
  \alpha_k &= Prob(g(s'_k,\,y'_k) > threshold).
   \end{split}
 \end{equation}
Hence $\beta_k$ converges to $\alpha_k$.
\end{proof}
The LQG controller, Kalman filter and $\chi^2$ detector becomes just a special case, where the state $s_k$ of the estimator at time $k$ is $y_{k-\mathcal T+1}, \ldots, y_k$ and $\hat x_{k-\mathcal T+1|k-\mathcal T},\ldots,\hat x_{k|k-1}$. The $f$ function is given by \eqref{eq:Kalmanfilter} and $g$ is given by \eqref{eq:chisquredetector}.

\begin{remark}
 The convergence of detection rate under the replay attack to the false alarm rate indicates that the information given by the detector will asymptotically go to $0$. In the other word, the detector becomes useless and the system is not resilient to replay attack. 
\end{remark}

\section{Detection of Replay Attack}\label{sec:secure}
As discussed in the previous section, there exist control systems that are not resilient to the replay attack. In this section, we want to design a detection strategy against replay attacks. Throughout this section we will always assume that $\mathcal A$ is stable. 

The main problem of LQG controller and Kalman filter is that they use a fixed gain, or a gain that converges really fast. Hence, the whole control system is static in some sense. In order to detect replay attack, we redesign the controller as
\begin{equation}
 u_k = u_k^* + \Delta u_k,
  \label{eq:securecontrolinput}
\end{equation}
where $u_k^*$ is the optimal LQG control signal and $\Delta u_k$s are drawn from an i.i.d. Gaussian distribution with zero mean and covariance $\mathcal Q$, and $\Delta u_k$s are chosen to be also independent of $u_k^*$. Figure~\ref{fig:diagram} shows the diagram of the whole system. 
\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width = 0.55 \textwidth]{diagram.1}
  \end{center}
  \caption{System Diagram}
  \label{fig:diagram}
\end{figure}


We add $\Delta u_k$ as an authentication signal. We choose it to be zero mean because we do not wish to introduce any bias to $x_k$. It is clear that without the attack, the controller is not optimal in the LQG sense anymore, which means that in order to detect the attack, we need to sacrifice control performance. The following theorem characterizes the loss of LQG performance when we inject $\Delta u_k$ into the system:
\begin{theorem}
  \label{theorem:lqgloss}
The LQG performance after adding $\Delta u_k$ is given by
\begin{equation}
 J' = J + trace[(U+B^TSB)\mathcal Q]. 
  \label{eq:lqgnewperformance}
\end{equation}
\end{theorem}
\begin{proof}
  See the appendix.
\end{proof}
We now wish to consider the $\chi^2$ detector after adding the random control signal.  The following theorem shows the effectiveness of the detector under the modified control scheme.
\begin{theorem}
  In the absence of an attack,
  \begin{align}
    E[( y_k - C\hat x_{k|k-1} )^T  \mathcal P^{-1}(y_k-C\hat x_{k|k-1})] = m.\label{eq:trueobservation}
  \end{align}
  Under attack
  \begin{align}
    &\lim_{k\rightarrow \infty} E[(y'_k - C\hat x_{k|k-1} )^T  \mathcal P^{-1}(y'_k-C\hat x_{k|k-1})] \\
    &= m+ 2trace(C^T\mathcal P^{-1}C \mathcal U),\label{eq:fakeobservation}\nonumber
  \end{align}
  where $\mathcal U$ is the solution of the following Lyapunov equation
  \begin{equation}
   \mathcal U - B\mathcal Q B^T = \mathcal A\mathcal U\mathcal A^T.  
  \end{equation}
\end{theorem}
\begin{proof}
  The first equation is trivial to prove using Theorem~\ref{theorem:pdf}. Rewrite $\hat x_{k+1|k}$ as
\begin{equation}
    \hat x_{k+1|k} = \mathcal A\hat x_{k|k-1} +(A+BL) Ky'_k + B\Delta u_k. 
  \label{eq:estimationrecursive2}
\end{equation}
For the virtual system
\begin{equation}
  \hat x'_{k+1|k}=\mathcal A\hat x'_{k|k-1} +(A+BL) Ky'_k + B\Delta u'_k. 
\end{equation}
Hence,
\begin{equation}
  \hat x_{k|k-1} - \hat x'_{k|k-1} = \mathcal A^k (\hat x_{0|-1} - \hat x'_{0|-1}) + \sum_{i=0}^{k-1}\mathcal A^{k-i-1}B(\Delta u_i - \Delta u'_i) .
\end{equation}
As a result,
\begin{equation}
  \begin{split}
    y'_k - C\hat x_{k|k-1} &=  y'_k - C\hat x'_{k|k-1} + C\mathcal A^k (\hat x_{0|-1} - \hat x'_{0|-1})\\
    & + C\sum_{i=0}^{k-1}\mathcal A^{k-i-1}B(\Delta u_i - \Delta u'_i). 
  \end{split}
\end{equation}
The first term has exactly the same distribution as $y_k - C\hat x_{k|k-1}$. The second term will converge to $0$ when $\mathcal A$ is stable. Also $\Delta u_i$ is independent of the virtual system and for the virtual system, $y'_k - C\hat x'_{k|k-1}$ is independent of $\Delta u'_i$. Hence
\begin{displaymath}
  \begin{split}
 & \lim_{k\rightarrow \infty} Cov(y'_k - C\hat x_{k|k-1}) = \lim_{k\rightarrow\infty} Cov(y'_k-C\hat x'_{k|k-1}) \\
  &+\sum_{i=0}^{\infty} Cov(C\mathcal A^{i}B\Delta u_i) + \sum_{i=0}^{\infty} Cov(C\mathcal A^{i}B\Delta u'_i)\\
  &= \mathcal P + 2\sum_{i=0}^{\infty}C\mathcal A^{i} B \mathcal Q B^T(\mathcal A^{i})^TC^T.
  \end{split}
\end{displaymath}
By the definition of $\mathcal U$, it is easy to see that 
\begin{displaymath}
 \mathcal U =  \sum_{i=0}^{\infty}\mathcal A^{i} B \mathcal Q B^T(\mathcal A^{i})^T.
\end{displaymath}
Hence, $\lim_{k\rightarrow \infty}Cov(y'_k - C\hat x_{k|k}) = \mathcal P + 2C\mathcal UC^T$ and
\begin{equation}
  \begin{split}
    &\lim_{k\rightarrow \infty} E[(y'_k - C\hat x_{k|k-1} )^T  \mathcal P^{-1}(y'_k-C\hat x_{k|k-1})]\\
    &=trace\left[\lim_{k\rightarrow \infty}Cov(y'_k - C\hat x_{k|k})\times\mathcal P^{-1}\right] \\
    &= m + 2trace(C^T\mathcal P^{-1}C\mathcal U).
  \end{split}
\end{equation}
\end{proof}
\begin{corollary}
 In the absence of an attack, the expectation of $\chi^2$ detector is
  \begin{align}
    E(g_k) = m\mathcal T.
  \end{align}
  Under attack, the asymptotic expectation becomes
  \begin{align}
    &\lim_{k\rightarrow \infty} E(g_k) = m\mathcal T+ 2trace(C^T\mathcal P^{-1}C \mathcal U)\mathcal T.
  \end{align}
\end{corollary}

The difference in the expectation of $g_k$ illustrates that the detection rate will not converges to the false alarm rate, which will also be shown in the next section. Another thing worth noticing is that to design $\mathcal Q$, one possible criterion is to minimize $J' - J = trace[(U+B^TSB)\mathcal Q]$ while maximizing $trace(C^T\mathcal P^{-1}C\mathcal U)$. 
%todo: add a bound for detection rate.
\section{Simulation Result}\label{sec:Simulation}
In this section we provide some simulation results on the detection of replay attack. Consider the control system described in Section~\ref{sec:classiccontrol} is controlling the temperature inside one room. Let $T_k$ be the temperature of the room at time $k$ and $T^*$ to be the desired temperature. Define the state as $x_k = T_k - T^*$. Suppose that
\begin{equation}
  x_{k+1} = x_k + u_k + w_k,   
\end{equation}
where $u_k$ is the input from air conditioning unit and $w_k$ is the process noise. Suppose that just one sensor is measuring the temperature, which is
\begin{equation}
y_k = x_k + v_k,
\end{equation}
where $v_k$ is the measurement noise. We choose $R = 0.1,\,Q = W = U =1$. One can compute that $ P =  1.092,\,K = 0.9161,\, L = -0.6180$.  Hence $\mathcal A = 0.0321$ and the system is vulnerable to replay attack. The LQG cost is $ J = 1.7076,\,J' = J +  2.618  \mathcal Q$.

We will first fix the window size $\mathcal T = 5$ and show the detection rate for different $\mathcal Q$s. We assume that the attacker records the $y_k$s from time $1$ to time $10$ and then replays it from time $11$ to time $20$. We also fixed the false alarm rate to be $5\%$ at each step.  
\begin{figure}[htpb]
    \includegraphics[width=0.45\textwidth]{fig1.eps}
    \caption{Detection rate at each time step for $\mathcal Q = 0.6$ (blue dashed line), $\mathcal Q = 0.4$ (brown dot line), $\mathcal Q = 0.2$ (red dash-dot line) and $\mathcal Q = 0$ (black solid line).}
    \label{fig:betavsq}
\end{figure}
\begin{figure}[htpb]
    \includegraphics[width=0.45\textwidth]{fig2.eps}
    \caption{Detection rate at each time step for $\mathcal T = 5$ (blue dashed line), $\mathcal T = 4$ (brown dot line), $\mathcal T = 3$ (red dash-dot line) and $\mathcal T = 2$ (black solid line).}
    \label{fig:betavst}
\end{figure}

Figure~\ref{fig:betavsq} shows the detection rate at each time step for different $\mathcal Q$s. Each detection rate is the average of 10,000 experiments. Note that the attack starts at time $11$. Hence, each line starts at the false alarm rate $5\%$ at time $10$. One can see that without additional input signal, the detection rate will soon converge to $5\%$, which proves that the detector is inefficient for replay attack. With $\mathcal Q = 0.6$, the loss of LQG performance is $2.618\times 0.6/1.7076=91\%$ with respect to the optimal LQG cost. As a result of the high control performance lost, one can get more than $35\%$ detection rate at each step.

Next we would like to fix $\mathcal Q = 0.6$ and compare the detection rate of different window size $\mathcal T$. We still assume the attack starts at time $11$ and the false alarm rate is $5\%$. Fig~\ref{fig:betavst} shows the detection rate for different window size. It is worth noticing that choosing a small window size will make the detector response faster to replay attack. However, the asymptotic detection rate will be lower than that of larger window size. On the other hand, by the law of large numbers, the asymptotic detection rate will converges to $1$ as $\mathcal T$ increases. However the detector will respond very slowly to the replay attack. For more details on the choice of window size, please refer to \cite{failuredetection}.

\section{Conclusions}\label{sec:conclustion}
In this paper we defined a replay attack model on cyber physical system and analyzed the performance of the control system under the attack. We discovered that for some control systems, the classical estimation, control, failure detection strategy are not resilience to the replay attack. For such kind of system, we provide a technique that can improve detection rate in the expense of control performance.

\section{Appendix: Proof of Theorem~\ref{theorem:lqgloss}}\label{appendix}
To simplify notation, let us first define the sigma algebra generated by $y_k,\ldots,y_0,\,\Delta u_{k-1},\ldots,\Delta u_0$ to be $\mathcal F_k$. Due to space limit, we will just list the outlines of the proof. Before proving Theorem~\ref{theorem:lqgloss}, we need the following lemmas:
\begin{lemma}
  The following equations about Kalman filter are true:
  \begin{align*}
    \hat x_{k|k} &= E(x_k|\mathcal F_k), P_{k|k} = E(e_{k|k}e_{k|k}^T|\mathcal F_k),
  \end{align*}
  where $e_{k|k} = x_k-\hat x_{k|k}$. 
\end{lemma}
\begin{lemma}
  \label{lemma:quadratic}
The following equations are true
  \begin{align}
    E(x_k^T\mathcal S x_k|\mathcal F_k) = trace(\mathcal SP_{k|k}) + (\hat x_{k|k})^T\mathcal S\hat x_{k|k},
  \end{align}
where $\mathcal S$ is any positive semidefinite matrix.
\end{lemma}
  Now define
  \begin{equation}
  J_N \triangleq  \min E\left[\sum_{i=0}^{N-1} (x_i^TWx_i+u_i^TUu_i)\right].
  \end{equation}
  By the definition of $J'$, we know that $J' = \lim_{N\rightarrow \infty} J_N/N$.

  Now fix $N$, let us define 
  \begin{equation}
  V_k(x_k) \triangleq  \min E\left[\sum_{k=i}^{N-1} (x_i^TWx_i+u_i^TUu_i)|\mathcal F_k\right],
  \end{equation}
  and $V_N(x_N) = 0$. By definition, we know that $E(V_0) = J_N$. Also from dynamic programming, we know that $V_k$ satisfies the following backward recursive equation:
  \begin{equation}
    V_k(x_k) =  \min_{u_k^*} E\left[x_k^TWx_k+u_k^TUu_k + V_{k+1}(x_{k+1})|\mathcal F_k\right].
  \label{eq:bellmanequation}
  \end{equation}

  Let us define 
  \begin{align*}
    S_{k-1} &\triangleq A^TS_{k}A+W - A^TS_{k}B(B^TS_{k}B+U)^{-1}B^TS_{k}A,\\
    c_{k-1} &\triangleq c_k + trace[(W+A^TS_{k}A-S_{k-1})P_{k-1|k-1}]+trace(S_{k}Q)\\
    &+trace[(B^TS_{k}B+U)\mathcal Q],
  \end{align*}
  with $S_N = 0$, $c_{N} = 0$.	

  \begin{lemma}
    $V_k(x_k)$ is given by
    \begin{equation}
    V_k(x_k) = E[x_k^TS_kx_k|\mathcal F_k] + c_k,\,k=N,\ldots,0 . 
    \label{eq:valuefunction}
    \end{equation}
  \end{lemma}
  
  \begin{proof}
    We will use backward induction to prove \eqref{eq:valuefunction}. First it is trivial to see that $V_N = 0$ satisfies \eqref{eq:valuefunction}. Now suppose that $V_{k+1}$ satisfies \eqref{eq:valuefunction}, then by \eqref{eq:bellmanequation}
    \begin{align*}
    V_k(x_k) &=  \min E\left[x_k^TWx_k+u_k^TUu_k + V_{k+1}(x_{k+1})|\mathcal F_k\right]\\
    &=\min E[x_k^TWx_k+(u_k^*+\Delta u_k)^TU(u_k^*+\Delta u_k)\\
    &+x_{k+1}^TS_{k+1}x_{k+1} + c_{k+1}|\mathcal F_k].
    \end{align*}
  First we know that $u_k^*$ is measurable to $\mathcal F_k$ and $\Delta u_k$ is independent of $\mathcal F_k$, hence
  \begin{equation}
     E[(u_k^*+\Delta u_k)^TU(u_k^*+\Delta u_k)|\mathcal F_k]=(u_k^*)^TUu_k^* + trace(U\mathcal Q).
    \label{eq:secondterm}
  \end{equation}
  Then let us write $x_{k+1}$ as
  \begin{displaymath}
    x_{k+1} = Ax_k + Bu_k^* + B\Delta u_k + w_k.
  \end{displaymath}
  By the fact that $\Delta u_k,\,w_k$ are independent of $Ax_k+Bu_k^*$, one can finally get 
  \begin{equation}
    \begin{split}
    &E(x_{k+1}^TS_{k+1}x_{k+1}|\mathcal F_k) = %E(x_k^TA^TS_{k+1}Ax_k|\mathcal F_k) + 2(u_k^*)^TB^TS_{k+1}AE(x_k|\mathcal F_k) + (u_k^*)^TB^TS_{k+1}Bu_k^*\\
   % &+ E(w_k^TS_{k+1}w_k)+ E(\Delta u_k^TB^TS_{k+1}B\Delta u_k) \\
    E(x_k^TA^TS_{k+1}Ax_k|\mathcal F_k) + \\
    &2(u_k^*)^TB^TS_{k+1}A\hat x_{k|k}+ (u_k^*)^TB^TS_{k+1}B(u_k^*) \\
    &+ trace(S_{k+1}Q)+trace(B^TS_{k+1}B\mathcal Q).
    \label{eq:thirdterm}
    \end{split}
  \end{equation}
  By \eqref{eq:secondterm} and \eqref{eq:thirdterm}, we know that
  \begin{displaymath}
    \begin{split}
      V_k(x_k) &= \min_{u_k^*} [(u_k^*)^T(U+B^TS_{k+1}B)u_k^* + 2(u_k^*)^TB^TS_{k+1}A\hat x_{k|k}] \\
      & + E[x_k^T(W+A^TS_{k+1}A)x_k|\mathcal F_k]+trace(S_{k+1}Q)\\
      &+E(c_{k+1}|\mathcal F_k)+trace[(B^TSB+U)\mathcal Q].
    \end{split}
  \end{displaymath}
  Hence, the optimal $u_k^*$ is
  \begin{equation}
    u_k^* = -(U+B^TS_{k+1}B)^{-1}B^TS_{k+1}A\hat x_{k|k},
  \end{equation}
  and $V_k(x_k)$ is
  \begin{equation}
    \begin{split}
    V_k(x_k) &= (\hat x_{k|k})^T A^TS_{k+1}B(B^TS_{k+1}B+U)^{-1}B^TS_{k+1}A \hat x_{k|k}\\
    & + E[x_k^T(W+A^TS_{k+1}A)x_k|\mathcal F_k]+trace(S_{k+1}Q)\\
      &+c_{k+1}+trace[(B^TS_{k+1}B+U)\mathcal Q]\\
      &=E(x_k^TS_kx_k|\mathcal F_k) + trace[(W+A^TS_{k+1}A-S_k)P_{k|k}]\\
      &+c_{k+1}+trace(S_{k+1}Q)+trace[(B^TS_{k+1}B+U)\mathcal Q]\\
      &=E(x_k^TS_kx_k|\mathcal F_k) + c_k,
    \end{split}
  \end{equation}
  which completes the proof\footnote{We use Lemma~\ref{lemma:quadratic} in the second equality.}.
  \end{proof}

  Now we are ready to prove Theorem~\ref{theorem:lqgloss}. 

  \begin{proof}[Proof of Theorem~\ref{theorem:lqgloss}]
Since
\begin{displaymath}
  \begin{split}
J_N &= EV_0 =E(x_0^TS_0x_0) + trace[\sum_{k=0}^{N-1}(W+A^TS_{k+1}A-S_k)P_{k|k}]\\
&+trace(\sum_{k=0}^{N-1}S_{k+1}Q)+trace[\sum_{k=0}^{N-1}(B^TS_{k+1}B+U)\mathcal Q],
\end{split}
\end{displaymath}
we know that
\begin{equation}
  \begin{split}
  J' &= \lim_{N\rightarrow\infty} J_N/N = trace[(W+A^TSA-S)(P-KCP)] +trace(SQ)\\
  &+trace[(B^TSB+U)\mathcal Q]=J + trace[(B^TSB+U)\mathcal Q].
  \end{split}
\end{equation}
\end{proof}

\bibliographystyle{IEEEtran}
\bibliography{SNsecurity}
\end{document} 
