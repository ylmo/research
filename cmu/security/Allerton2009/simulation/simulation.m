clc
clear
rand('twister',sum(clock)*100)

%initialization
A = 1;
B = 1;
C = 1;
W = 1;
U = 1;
Q = 1;
R = 1;
n = 1;
m = 1;
P = 0;
S = 0;

for i = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;

%change cQ here
cQ = 1;
cP = C*P*C' + R;
invcP = inv(cP);

for index = 1:10000
    %recording
    x = sqrt(P) * randn;
    hatx1 = 0;%prediction
    for i = 1:10
        y(i) = x(i) + sqrt(R) * randn;
        residue(i,index) = y(i) - C * hatx1(i);
        hatx2(i) = hatx1(i) + K * residue(i,index);
        u(i) = L*hatx2(i) + sqrt(cQ)*randn; 
        hatx1(i+1) = A*hatx2(i) + B*u(i) ;
        x(i+1) = A*x(i) + B*u(i)+sqrt(Q)*randn;
    end
    %replay
    for i = 11:20
        y(i) = y(i-10);
        residue(i,index) = y(i) - C * hatx1(i);
        hatx2(i) = hatx1(i) + K * residue(i,index);
        u(i) = L*hatx2(i) + sqrt(cQ)*randn; 
        hatx1(i+1) = A*hatx2(i) + B*u(i) ;
        x(i+1) = A*x(i) + B*u(i)+sqrt(Q)*randn;
    end
end

%chi-square detector
cT = 1;
thre = chi2inv(0.99,cT*m);
for i = 10:20
    for j = 1:10000
        g(i,j) = (sum(residue(i-cT+1:i,j).^2)/cP > thre);
    end
end
beta = sum(g,2)/10000;
hold on
plot(1:20,beta(1:20),'b');