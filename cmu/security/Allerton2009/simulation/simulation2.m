clc
clear
rand('twister',sum(clock)*100)
%initialization
A = 1;
B = 1;
C = 1;
W = 1;
U = 1;
Q = 1;
R = 0.1;
n = 1;
m = 1;
P = 0;
S = 0;
for i = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;
cQ = 0.6;
cP = C*P*C' + R;
invcP = inv(cP);
J = trace(S*Q+(A'*S*A+W-S)*(P-K*C*P));
Jnew = J + trace((U+B'*S*B)*cQ);
count = 0;
cT = 1;
thre = chi2inv(0.95,cT*m);
for j = 1:10000
    %recording
    x = sqrt(P) * randn;
    hatx1 = 0;%prediction
    for i = 1:10
        y = x + sqrt(R) * randn;
        residue(i) = y - C * hatx1;
        hatx2 = hatx1 + K * residue(i);
        u = L*hatx2+ sqrt(cQ)*randn; 
        hatx1 = A*hatx2 + B*u ;
        x = A*x + B*u+sqrt(Q)*randn;
    end
    %replay
    vx = sqrt(P) *randn;
    vhatx1 = 0;
    while(1)
        i = i+1;
        %virtual system
        vy = vx + sqrt(R) * randn;
        vresidue = vy - C * vhatx1;
        vhatx2 = vhatx1 + K * vresidue;
        vu = L*vhatx2+ sqrt(cQ)*randn; 
        vhatx1 = A*vhatx2 + B*vu ;
        vx = A*vx + B*vu+sqrt(Q)*randn;
        %real system
        y = vy;
        residue(i) = y - C * hatx1;
        hatx2 = hatx1 + K * residue(i);
        u = L*hatx2 + sqrt(cQ)*randn; 
        hatx1 = A*hatx2 + B*u ;
        if (sum(residue(i-cT+1:i).^2)/cP > thre)
            break;
        end
    end
    count = count + i - 10;
end
count = count / 10000;
