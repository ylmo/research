clc
clear
rand('twister',sum(clock)*100)
%initialization
A = 1;
B = 1;
C = 1;
W = 2;
U = 0.5;
Q = 1;
R = 0.1;
n = 1;
m = 1;
P = 0;
S = 0;
for i = 1:100
    P = A*P*A'+Q-A*P*C'*inv(C*P*C'+R)*C*P*A';
    S = A'*S*A +W - A'*S*B*inv(B'*S*B+U)*B'*S*A;
end
K = P*C'*inv(C*P*C'+R);
L = -inv(B'*S*B+U)*B'*S*A;
cQ = 0.6;

tP = [1 0; 0 1];
tA = [A+B*L -B*L;0 A-K*C*A];
tQ = [B^2*cQ+Q Q*(1-K*C);(1-K*C)*Q (1-K*C)^2*Q+K^2*R];
for i = 1:100
    tP = tP - [tP(1)*C; -K*R] * inv(C^2*tP(1)+R)*[C*tP(1) -K*R];
    tP = tA*tP*tA' + tQ;
end
tK = [tP(1)*C;-K*R]*inv([C 0]*tP*[C 0]'+R);

for index = 1:10000
    %recording
    x = sqrt(P) * randn;
    hatx1 = 0;%prediction
    for i = 1:10
        y(i) = x(i) + sqrt(R) * randn;
        residue(i,index) = y(i) - C * hatx1(i);
        hatx2(i) = hatx1(i) + K * residue(i,index);
        u(i) = L*hatx2(i) + sqrt(cQ)*randn; 
        hatx1(i+1) = A*hatx2(i) + B*u(i) ;
        x(i+1) = A*x(i) + B*u(i)+sqrt(Q)*randn;
    end
    %replay
    tx1 = [0;0];
    for i = 11:20
        y(i) = y(i-10);
        residue(i,index) = y(i) - [C 0]* tx1;        
        tx2 = tx1 + tK*residue(i,index);
        tx1 = tA * tx2;
    end
end
disp 'experience variance of residue'
var(residue(20,:))
disp 'asymptotic variance of residue'
C^2*tP(1) + R