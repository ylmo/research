\documentclass[final,t]{beamer}
\mode<presentation> {\usetheme{cmu}}
% additional settings
\setbeamerfont{itemize}{size=\normalsize}
\setbeamerfont{itemize/enumerate body}{size=\normalsize}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}

% additional packages
\usepackage{times}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{booktabs, array}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[orientation=portrait,size=a0,scale=1.3]{beamerposter}
\listfiles

\title{\huge Secure Control against Replay Attack}
\author[Mo et al.]{\large Yilin Mo, Bruno Sinopoli}
\institute[Carnegie Mellon]{\large Dept of Electrical and Computer Engineering, Carnegie Mellon University}
\date[Oct. 14, 2000]{Oct. 14, 2009}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{frame}{} 
  \begin{columns}[t]
    \begin{column}{.48\linewidth}
      \vskip-2ex
      \begin{block}{Introduction}
	    \begin{itemize}
	      \item Cyber Physical System (CPS) are becoming ubiquitous, with lots of \alert{safety critical} applications: civil infrastructure, energy, transportation.
	      \item Information security can provide secure communication among sensors, controllers and actuators. 
	      \item However, sensors of CPS are often \alert{exposed}, easily accessible by malicious third party.
	    \end{itemize}
	    \begin{itemize}
	      \item How to \alert{get} and \alert{modify} sensor measurements? Place attacker's sensors and actuators besides CPS sensors. Attacker then can use his sensors and actuators to monitor and control the environments around CPS sensors in order to get and modify their readings.
	      \item Is information security enough for CPS in insecure environment? Is traditional control system resilient to such attack?
	    \end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \vskip1ex
      \begin{block}{Classical Control and Failure Detection System}
	\begin{itemize}
	  \item Physical System(Linear Time Invariant Gaussian System)
	    \begin{equation}
	      \alert{	      x_{k+1} = Ax_k +  Bu_k + w_k,\, y_{k} = C x_k + v_k.}
	    \end{equation}
	    \begin{itemize}
	      \item $x_k \in \mathbb R^n$ is the state variables.
	      \item $y_k \in \mathbb R^m$ is the measurements from the sensors.
	      \item $u_k \in \mathbb R^p$ is the control input.
	      \item $v_k,\,w_k$ are white Gaussian process and measurement noise.
	    \end{itemize}
	  \item Kalman Filter (Already in steady state)
	    \begin{equation}
	      \alert{	      \hat x _{k + 1|k}  = A \hat x_{k|k} +Bu_k,\, \hat x_{k|k} = \hat x_{k|k - 1}  + K (y_k  - C \hat x _{k|k - 1} ).}
	    \end{equation}
	    $K$ is the Kalman Gain. $\hat x_{k|k}$ is the optimal estimation of $x_k$ at time $k$.
	  \item LQG Controller

	    LQG controller tries to minimize:
	    \begin{equation}
	      J =\min \lim_{T\rightarrow \infty}E\frac{1}{T}\left[\sum_{k=0}^{T-1} (x_k^TWx_k+u_k^TUu_k)\right].
	    \end{equation}
	    Solution is a fixed gain controller:
	    \begin{equation}
	      \alert{ u_k = u_k^* = -(B^TSB+U)^{-1}B^TSA\hat x_{k|k} = L\hat x_{k|k}.}
	    \end{equation}
	    $u_k^*$ is the optimal control input and $S$ is satisfies following equation
	    \begin{equation}
	      \label{eq:lqgcontrolS}
	      S = A^TSA+W - A^TSB(B^TSB+U)^{-1}B^TSA.
	    \end{equation}
	  \item $\chi^2$ Failure Detector
	    \begin{equation}
	      \alert{ g_k=\sum_{i = k-\mathcal T+1}^k (y_i - C\hat x_{i|i-1})^T\mathcal P^{-1}(y_i - C\hat x_{i|i-1})\lessgtr threshold.}
	    \end{equation}
	    The detector checks if $y_k$ is far away from its expected value $C\hat x_{k|k-1}$. $\mathcal P$ is the covariance matrix of innovation $y_i - C\hat x_{i|i-1}$. If $g_k$ is larger than the threshold, then trigger the alarm.
	\end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


      \vskip1ex
      \begin{block}{Replay Attack}
%	\begin{columns}[t]
%	  \begin{column}{0.48\linewidth}
	    The attacker can:
	    \begin{itemize}
	      \item \alert{Inject} a control input $u_k^a$ into the system anytime.
	      \item \alert{Record} all sensor measurements and can \alert{modify} them. We will denote the readings modified by the attacker by $y'_k$.  
	    \end{itemize}

%	  \end{column}
%	  \begin{column}{0.48\linewidth}
	    The attacker: 
	    \begin{itemize}
	      \item \alert{records} a sufficient number of $y_k$s from time $-T$ without giving any input to the system. 
	      \item \alert{injects} malicious control input while \alert{replays} the previous recorded $y_k$s.
	    \end{itemize}

	    Can traditional control and failure detection strategy \alert{detect} such attack?
%	  \end{column}
%	\end{columns}
      \end{block}

      \vskip1ex
      \begin{block}{System Diagram}
	\begin{figure}
	  \begin{center}
	    \includegraphics[width=0.9\linewidth]{diagram.mps}
	  \end{center}
	\end{figure}
      \end{block}
    \end{column}

    \begin{column}{.48\linewidth}
      \vskip-2ex
      \begin{block}{Main Result}
	\begin{itemize}
	  \item Suppose record begins at time $-T$ and replay begins at time $0$. Under the attack, the update equation of Kalman filter is
	    \begin{align*}
	      \hat x_{k+1|k} &=(A+BL)(I-KC)\hat x_{k|k-1} +(A+BL) Ky'_k\\
	      &= \mathcal A \hat x_{k|k-1} + (A+BL)Ky'_k.
	    \end{align*}
	  \item Since the attacker records from time $-T$, $y_k' = y_{k-T}$. The Kalman filter at time $k-T$ satisfies:
	    \begin{align*}
	      \hat x_{k-T+1|k-T}  &= \mathcal A \hat x_{k-T|k-T-1} + (A+BL)Ky_{k-T}\\
	      &=  \mathcal A \hat x_{k-T|k-T-1} + (A+BL)Ky'_{k}.
	    \end{align*}
	  \item Hence, the innovation $y_k' - C\hat x_{k|k-1}$ satisfies
	    \begin{displaymath}
	      \begin{array}{ccccc}
		\alert{	\boxed{y_k' - C\hat x_{k|k-1}}}&=&\alert{\boxed{( y_{k-T} - C\hat x_{k-T|k-T-1})}}& +& \alert{\boxed{C\mathcal A^k( \hat x_{0|-1} - \hat x_{-T|-T-1})}},\\
		\uparrow&&\uparrow&&\uparrow\\
		\alert{\textrm{innovation under replay}}&&\alert{\textrm{innovation without replay}}&&\alert{\textrm{converges to 0 if $\|\mathcal A\|<1$}}
	      \end{array}
	    \end{displaymath}
	  \item If $\mathcal A^k$ converges fast, it is \alert{hard to distinguish} innovation under replay and innovation without replay!! (See \alert{dark blue} line in Experimental result)
	\end{itemize}
      \end{block}

      \begin{block}{Counter Measures}
	Add a random control input $\Delta u_k$ to the system:
	\begin{itemize}
	  \item If the system responds to $\Delta u_k$, then there is no replay attack.
	  \item If the system does not respond, then there is a replay attack.
	  \item Random control inputs act like time stamps. 
	  \item Cost: The controller is not optimal any more.
	\end{itemize}	
	Design control input to be
	\begin{align*}
	  u_k = u_k^* + \Delta u_k.
	\end{align*}
	\begin{itemize}
	  \item $u_k^*$ is the optimal LQG control input.
	  \item $\Delta u_k\sim\mathcal N(0,\mathcal Q)$ is a i.i.d. Gaussian random control input.
	\end{itemize} 
	Increase in LQG cost: \[trace[(U+B^TSB)\mathcal Q].\]

	Innovation under replay:
	\begin{equation}
	  \begin{split}
	    y'_k - C\hat x_{k|k-1} &=  y_{k-T} - C\hat x_{k-T|k-T-1} + C\mathcal A^k (\hat x_{0|-1} - \hat x_{-T|-T-1})\\
	    & + \alert{\boxed{C\sum_{i=0}^{k-1}\mathcal A^{k-i-1}B(\Delta u_i - \Delta u'_i)}}\leftarrow \textrm{\alert{Can be detected!}}. 
	  \end{split}
	\end{equation}

      \end{block}

      \begin{block}{Experimental Results}
	Single state, Single sensor system:
	\begin{align*}
	  x_{k+1}=x_{k} + u_k + w_k, y_k =x_{k} + v_k
	\end{align*}
	Parameters:
	\begin{itemize}
	  \item $R = 0.1$, $Q=W=U=1$.
	  \item Detector window size $\mathcal T = 5$, false alarm rate $5\%$.
	\end{itemize}
	Replay starts at time $11$.
	\begin{columns}[c]
	  \begin{column}{0.75\linewidth}
	    \begin{figure}
	      \begin{center}
		\includegraphics[width = 0.95\linewidth]{fig1.eps}
	      \end{center}
	      \caption{Detection Rate over Time}
	    \end{figure}
	  \end{column}
	  \begin{column}{0.23\linewidth}
	    \begin{itemize}
	      \item Blue: \\$\mathcal Q = 0.6$
	      \item Brown:\\ $\mathcal Q = 0.4$
	      \item Red: \\$\mathcal Q = 0.2$
	      \item Dark Blue: \\$\mathcal Q = 0$
	    \end{itemize}
	  \end{column}
	\end{columns}
	\alert{Without additional control input, the detector is useless. Larger random inputs increase rate of detection while sacrificing control accuracy.}
      \end{block}
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      \begin{block}{Conclusion}
	\begin{itemize}
	  \item Tradition control and failure detection scheme can fail to detect replay attacks on control systems.
	  \item By adding random control signals, the system can always detect replay attacks at the cost of control accuracy.
	\end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \end{column}
    \end{columns}
  \end{frame}

  \end{document}
