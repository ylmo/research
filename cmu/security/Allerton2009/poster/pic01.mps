%!PS
%%BoundingBox: -59 -22 110 42 
%%HiResBoundingBox: -58.08838 -21.50983 109.71361 41.71173 
%%Creator: MetaPost 1.005
%%CreationDate: 2009.09.18:1300
%%Pages: 1
%*Font: cmss10 10.9091 9.96265 45:8000000a8be7c
%*Font: simpsons 191.28268 127.52179 00:8008
%%BeginProlog
%%EndProlog
%%Page: 1 1
 0 0 1 setrgbcolor
14.4828 -2.42426 moveto
(senso) cmss10 10.9091 fshow
38.4828 -2.42426 moveto
(r) cmss10 10.9091 fshow
 0 0.5 dtransform truncate idtransform setlinewidth pop [] 0 setdash
 1 setlinejoin 10 setmiterlimit
newpath 45.2856 0 moveto
45.2856 5.49841 36.04152 5.49976 28.34645 5.49976 curveto
20.65138 5.49976 11.4073 5.49841 11.4073 0 curveto
11.4073 -5.49841 20.65138 -5.49976 28.34645 -5.49976 curveto
36.04152 -5.49976 45.2856 -5.49841 45.2856 0 curveto closepath stroke
63.6151 -3.78786 moveto
(controller) cmss10 10.9091 fshow
newpath 60.6151 -6.78786 moveto
109.46361 -6.78786 lineto
109.46361 6.78786 lineto
60.6151 6.78786 lineto
 closepath stroke
-47.2101 -3.1169 moveto
(actuato) cmss10 10.9091 fshow
-13.2101 -3.1169 moveto
(r) cmss10 10.9091 fshow
newpath -5.94113 0 moveto
-5.94113 6.65695 -18.26347 6.65857 -28.34645 6.65857 curveto
-38.42943 6.65857 -50.75177 6.65695 -50.75177 0 curveto
-50.75177 -6.65695 -38.42943 -6.65857 -28.34645 -6.65857 curveto
-18.26347 -6.65857 -5.94113 -6.65695 -5.94113 0 curveto closepath stroke
 1 setlinecap
newpath 33.4043 5.44543 moveto
39.79546 11.0188 48.04869 14.17323 56.6929 14.17323 curveto
64.61674 14.17323 72.21205 11.52264 78.34573 6.78798 curveto stroke
newpath 74.43195 7.61475 moveto
75.74866 7.41344 77.05544 7.1377 78.34573 6.78798 curveto
77.68065 7.94766 76.95163 9.06667 76.16293 10.1401 curveto
 closepath
gsave fill grestore stroke
newpath 64.68997 -6.78778 moveto
52.83864 -9.80289 40.63092 -11.33841 28.34645 -11.33841 curveto
14.55289 -11.33841 0.8561 -9.40245 -12.34807 -5.60971 curveto stroke
newpath -8.38025 -5.10481 moveto
-9.70021 -5.29102 -11.02293 -5.45932 -12.34807 -5.60971 curveto
-11.30473 -6.4404 -10.25041 -7.25671 -9.18542 -8.05843 curveto
 closepath
gsave fill grestore stroke
 0.4 0 0 setrgbcolor
-28.84856 -17.66231 moveto
(Environment) cmss10 10.9091 fshow
-11.20323 25.64662 moveto
(\000) simpsons 191.28268 fshow
-5.26323 25.51163 moveto
(\000) simpsons 191.28268 fshow
-30.37334 9.31152 moveto
(\014) simpsons 191.28268 fshow
newpath 5.02676 23.2494 moveto
16.11436 20.39667 25.08958 14.71837 27.62674 5.49974 curveto stroke
newpath 24.851 8.38 moveto
25.86215 7.53163 26.79097 6.57378 27.62674 5.49974 curveto
27.79524 6.85017 27.8158 8.18425 27.70073 9.49915 curveto
 closepath
gsave fill grestore stroke
16.41855 21.99968 moveto
(attack) cmss10 10.9091 fshow
newpath -57.83838 -14.17322 moveto
-57.83838 -18.08704 -54.66559 -21.25983 -50.75177 -21.25983 curveto
45.2856 -21.25983 lineto
49.19942 -21.25983 52.37221 -18.08704 52.37221 -14.17322 curveto
52.37221 34.01566 lineto
52.37221 37.92947 49.19942 41.10226 45.2856 41.10226 curveto
-50.75177 41.10226 lineto
-54.66559 41.10226 -57.83838 37.92947 -57.83838 34.01566 curveto
 closepath stroke
showpage
%%EOF
