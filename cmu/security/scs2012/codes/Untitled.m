
A = [1 0;1 1];
B = [1;0];
Gamma = [0;1];
U = 1;
W = eye(2);
Q = eye(2);
R = eye(2);
C = eye(2);
S = eye(2);
P = eye(2);
for i = 1:100
S = A'*S*A+W-A'*S*B/(B'*S*B+U)*B'*S*A;
end
L = -inv(B'*S*B+U)*B'*S*A;
for i = 1:100
P = A*P*A'+Q-A*P*C'/(C*P*C'+R)*C*P*A';
end
K = P*C'/(C*P*C'+R);
Pcal = C*P*C'+R;
invP = inv(Pcal);
AA = [A+B*L -B*L;zeros(2) A-K*C*A];
BB = [zeros(2,1);-K*Gamma];
CC = [zeros(2,2) C*A];
DD = [Gamma];

QQ{1} = zeros(4);

for i = 1:20
    QQ{i+1} = 0.5*(AA'*QQ{i}*AA + CC'*invP*CC - (AA'*QQ{i}*BB+CC'*invP*DD)/(BB'*QQ{i}*BB+DD'*invP*DD)*(BB'*QQ{i}*AA+DD'*invP*CC));
end