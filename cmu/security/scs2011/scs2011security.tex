\documentclass[letterpaper,10pt]{IEEEtran}
\usepackage{subfigure}
\usepackage{algorithm}
\usepackage{algorithmic}
\usepackage{color}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{cite}
\usepackage[dvips]{graphicx}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}


\title{\LARGE \bf {Secure Data Transmission Protocol in Multi-Hop Sensor Networks}}

\author{Yilin $\textrm{Mo}$, Bruno $\textrm{Sinopoli}$
\thanks{
Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
This research is supported in part by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office Foundation and grant NGIT2009100109 from Northrop Grumman Information Technology Inc Cybersecurity Consortium. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}

\maketitle
\begin{abstract}
In this paper, we discuss how to design secure data transmission protocol in wireless sensor networks. The network is assumed to have $p$ sensors and $n-p$ routers which relay the observations from the sensors to the fusion center. A maximum of $f$ routers can be compromised by an adversary. The fusion center wants to detect and identify malicious nodes in the system and reconstruct the observations. Due to computational constraints, we could not use advance information security technique, such as digital signature to protect the integrity of the data. Instead, we model the whole network as a linear system. By leveraging fault detection and identification theory and generic properties of structured system, we can provide a necessary and sufficient condition for generic detectability of the malicious nodes and reconstructability of the observations, which is related to the topology of the network. We also show that resilience can be further improved by using trustworthy nodes.
\end{abstract}

\section{Introduction}\label{sec:Introduction}
Design and analysis of systems based on sensor networks involve cross disciplinary research spanning domains within computer science, communication systems and control theory. A sensor network is composed of low-power devices that integrate computing with heterogeneous sensing and communication. Sensor network based systems are usually embedded in the physical world, with which they interact by collecting, processing and transmitting relevant data.

Sensor networks span a wide range of applications, including environmental monitoring and control, health care, home and office automation and traffic control \cite{wireless_sensor_network}. Many of these applications are safety critical. Any successful attack may significantly hamper the functionality of the sensor networks and lead to economic losses. In addition, sensors in large distributed sensor networks may be physically exposed, making it difficult to ensure security and availability for each and every single sensor. The research community has acknowledged the importance of addressing the challenge of designing secure estimation and control systems~\cite{securityrisks}\cite{challengessecurity}.

The impact of attacks on control systems is discussed in \cite{securecontrol}. The authors consider two possible classes of attacks on CPS: Denial of Service (DoS) and deception (or false data injection) attacks. The DoS attack prevents the exchange of information, usually either sensor readings or control inputs between subsystems, while a false data injection attack affects the data integrity of packets by modifying their payloads. A robust feedback control design against DoS attacks is discussed in \cite{robustdos}. We feel that false data injection attacks can be a subtler attack than DoS as they are in principle more difficult to detect. In this paper we want to analyze the resilience of data transmission protocol against such types of attacks.

Secure data transmission protocols have been extensively studied by the information security community. Many tools, such as digital signature, have been developed to protect data integrity. However, these cryptographic methods are usually computationally expensive and may not be applicable in sensor networks, in which sensors usually have limited computational power. 

Another approach is to model the data transmission protocol as a linear system and malicious behavior in the network as an external input. Therefore the whole problem of detection and identification of malicious nodes can be reformulated as a fault detection and identification (FDI) problem. Over the past few decades, a significant amount of research effort has been spent on FDI problems \cite{failuredetection,willsky_glr}. Recently, Pasqualetti et al. \cite{fp-ab-fb:09b} and Sundaram et al. \cite{wirelesscontrol} show how to use FDI to detect and identify malicious behavior in consensus algorithm and wireless control networks. In this paper, we discuss how to use FDI to design a secure data transmission protocol in sensor networks. The network is assumed to have $p$ sensors and $n-p$ routers which relay observations from the sensors to the fusion center. A maximum of $f$ routers can be compromised by an adversary. The fusion center wants to detect and identify malicious nodes in the system and reconstruct the observations. We show that the achievability of such goals is determined by the topology of the network. We also provide a way to increase the resilience of the network by using trustworthy nodes. 

The rest of the paper is organized as follows: in Section~\ref{sec:problem}, we introduce the sensor network model and the attack models. We also introduce the concepts of weak and strong resilience. In Section~\ref{sec:main}, we prove a necessary and sufficient condition for the network to be weakly or strongly resilient to the attack based on network topology. In Section~\ref{sec:enhancing}, we propose a way to enhance the resilience of the network by using trustworthy routers. Finally Section~\ref{sec:conclusion} concludes the paper.

\section{Problem Formulation}\label{sec:problem}
In this section we want to introduce the system which will be discussed in the rest of the paper.

\subsection{System Description}
Consider a set of $p$ sensors $S = \{1,\ldots,p\}$ monitoring the environment. At each time step $k$, each sensor makes a scalar observation denoted as $w_{k,i}$. To simplify notation, let us define the measurement vector $w_k = [w_{k,1},\ldots,w_{k,p}]' \in \mathbb R^p$. 

The sensors need transmit their measurements back to the fusion center. However, due to certain constraints, such as energy or bandwidth, they cannot directly communicate with the fusion center. As a result, a multi-hop network is used to relay the observations from the sensors to the fusion center. We assume that the network consists of $n-p$ routers $\{p+1,\ldots,n\}$, where $n \geq p$ and the fusion center is listening to the last $m$ ($m\leq n$) nodes $\{n-m+1,\ldots,n\}$, the set of which is denoted as $T$. 

We model the whole network (both the routers and sensors) as a directed graph $G= (V,\,E)$. $V \triangleq \{s_1,\ldots,s_n\}$ is the set of vertices\footnote{We use vertex or node to denote both sensors and routers.}. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if node $i$ can transmit to node $j$ directly. We denote the set of incoming neighbors of node $i$ as
\begin{equation}
N^I_i = \{j\in V:(j,i)\in E\},
\label{eq:inneighbor}
\end{equation}
and the set of outgoing neighbors as
\begin{equation}
N^O_i = \{j\in V:(i,j)\in E\},
\label{eq:outneighbor}
\end{equation}
The topology of the network is illustrated in Fig~\ref{fig:diagram}.

Let $W$ and $W'$ be two subset of $V$. A path from $W$ to $W'$ is a sequence of vertices $i_1,\ldots,i_k$, where $(i_j,i_{j-1})\in E$, $i_1\in W$ and $i_k \in W'$. A path is called simple if there is no repetition of vertices. Two paths are disjoint if they do not share common vertex. We call $l$ paths from $W$ to $W'$ disjoint if they are mutually disjoint, i.e. any two of them are disjoint. A set of $l$ disjoint and simple paths from $W$ to $W'$ is called a linking of size $l$ or an $l$ linking from $W$ to $W'$.

We assume that at each step, a node can only broadcast one scalar to its outgoing neighbors due to the bandwidth and energy constraints. One way for the fusion center to recover the observations $w_k$ is to construct a $p$ linking from $S$ to $T$. Each node in the $p$ linking simply forwards the messages from the incoming neighbor to the its unique outgoing neighbor in the linking. 

In this paper we want to study a more general transmission scheme. Instead of simply relaying the messages, each sensor computes a linear function of the messages from its incoming neighbors and its own state and broadcasts the result to the outgoing neighbors. To be specific, suppose each node keeps a state $x_{k,i}$, where $k$ is the time index and $i$ is the node index. At each time step, each node $i$ broadcasts its state to its outgoing neighbors, 
\begin{equation}
	z_{k,i,j} = x_{k,i},\,j\in \mathcal N^O(i),
	\label{eq:broadcast}
\end{equation}
where $z_{k,i,j}$ is the data transmitted from node $i$ to node $j$ at time $k$. Once node $i$ receives the data from all its incoming neighbors, it performs an update as follows:
\begin{equation}
	x_{k+1,i} = a_{i,i}x_{k,i} + \sum_{j\in N^I_i} a_{i,j}z_{k,j,i} + \sum_{j\in S} \delta_{i,j}w_{k,j}, 
\label{eq:update}
\end{equation}
with initial condition $x_{0,i}=0$, and $\delta_{i,j}$ is 1 if $i=j$ and is $0$ otherwise. 

To simplify notation, let us define $x_k = [x_{k,1},\ldots,x_{k,n}]'\in \mathbb R^N$, $A \triangleq [a_{i,j}] \in \mathbb R^{n\times n}$ and $B \triangleq [e_{1},\ldots,e_{p}]\in \mathbb R^{n\times p}$, where $e_i \triangleq [\delta_{1,i},\ldots,\delta_{n,i}]'\in \mathbb R^{n}$. \eqref{eq:update} can be written in the matrix form as
\begin{equation}
x_{k+1} = Ax_k + Bw_k,\, x_0=0.
\label{eq:matrixupdate}
\end{equation}
Define $C = [e_{n-m+1},\ldots,e_{n}]'\in \mathbb R^{m\times p}$. At each time the fusion center receives
\begin{equation}
y_k =[x_{k,n-m+1},\ldots,x_{k,n}]'= C x_k.
\label{eq:observe}
\end{equation}

\begin{figure}[htpb]
  \begin{center}
    \includegraphics[width = 0.45 \textwidth]{pic.1}
  \end{center}
  \caption{Multi-Hop Sensor Networks}
  \label{fig:diagram}
\end{figure}

\subsection{Attack Model}
In this section we describe the attack model. We assume that several routers are compromised\footnote{We assume the sensors are always benign in the whole paper.}. We model the malicious behavior of a compromised router as an external input. To be specific, we assume the compromised router follows the following update rule
\begin{equation}
x_{k+1,i}= a_{i,i}x_{k,i} + \sum_{j\in N^I_i} a_{i,j}z_{k,j,i} + u_{k,i}.
\end{equation}

\begin{remark}
If a compromised router chooses $u_{k,i}=0$ for all $k$, then there is no chance for the fusion center to detect it since it behaves exactly as a benign node. Moreover the compromised router does no damage to the network. Therefore, we assume that each compromised router uses a non-zero $u_{k,i}$ for at least once. 
\end{remark}

The set of compromised routers is denoted as $F$, which is assumed to be unknown to the fusion center in the beginning. However, we assume that $|F|\leq f$, where $f$ is known to fusion center. Let us denote $\mathcal F$ as the set of all possible $F$, which is denoted as\footnote{Note that the empty set $\phi$ also belongs to $\mathcal F$.}
\begin{equation}
\mathcal F = \{F\subseteq V:|F|\leq f,\,F\bigcap S=\phi\}.
\end{equation}
It can be seen that the capability of the attacker is completely characterized by $\mathcal F$. As a result, we will call such attack an $\mathcal F$-attack.

Define $B^F = [e_{i_1},\ldots,e_{i_j}]$, where $F=\{i_1,i_2,\ldots,i_j\}$. Then the update equation can be written as
\begin{equation}
x_{k+1} = A x_{k} + Bw_k + B^F u_k^F.
\end{equation}

Define $w,\,y,\,u^F$ as infinite sequences $(w_0,w_1,\ldots),\,(y_0,y_1,\ldots),\,(u^F_0,u^F_1,\ldots)$ respectively. It is easy to see that $y$ is a function of $w,\,u^F$ and $F$. As a result, we will denote $y$ as
\begin{equation}
y = \Phi(w,\,u^F,\,F).
\end{equation}

Next we define the concept of resilient to $\mathcal F$-attacks:
\begin{definition}
The system is called weakly resilient to the $\mathcal F$-attacks if and only if 
\begin{equation}
\Phi(w,\,u^F,\,F)=\Phi(0,\,0,\,\phi)
\end{equation}
implies that $F=\phi$ and $w = 0$.
\end{definition}

\begin{definition}
The system is called strongly resilient to the $\mathcal F$-attacks if and only if 
\begin{equation}
\Phi(w,\,u^F,\,F)=\Phi(w',\,v^{F'},\,F')
\end{equation}
implies that $w=w'$, $u^F=v^{F'}$ and $F=F'$.
\end{definition}

The following theorems relate the weak and strong resilience to the detectability of the attack and reconstructability of the sensor measurements.
\begin{theorem}
If the system is weakly resilient to the $\mathcal F$-attacks, then the system can successfully detect any $\mathcal F$-attack.
\end{theorem}
\begin{proof}
If there is no attack, then the trajectory of the output will be in the following set
\begin{displaymath}
\mathcal Y_{\phi} = \{y=\Phi(w,\,0,\,\phi)\}.
\end{displaymath}
When an attack is present, the trajectory will be in the following set\footnote{Note that we assume that for each compromised node, $u_{k,i}$ will be non zero for at least once.}
\begin{displaymath}
\mathcal Y_{F} = \{y=\Phi(w,\,u^F,\,F)\}.
\end{displaymath}
The system can detect the attack if and only if $\mathcal Y_{\phi}\bigcap\mathcal Y_F=\phi$. Now suppose the opposite, i.e. $\mathcal Y_{\phi}\bigcap\mathcal Y_F\neq \phi$. Then there exists $w,\,w',\,u^F,\,F\neq \phi$, such that
\begin{displaymath}
\Phi(w,\,0,\,\phi) = \Phi(w',\,u^F,\,F).
\end{displaymath}
By linearity of the system, we have
\begin{displaymath}
\Phi(0,\,0,\,\phi) = \Phi(w'-w,\,u^F,\,F),
\end{displaymath}
which further implies that $F=\phi$ due to the weak resilience. However, that contradicts the assumption that $F\neq \phi$. Thus $\mathcal Y_{\phi}\bigcap\mathcal Y_F=\phi$ and the system is able to detect the attack.
\end{proof}

\begin{theorem}
If the system is strongly resilient to the $\mathcal F$, then given $y$, the system can successfully identify the set of malicious nodes $F$ and reconstruct the input $w$.
\end{theorem}
\begin{proof}
Suppose $F$ is the set of compromised nodes. Define 
\begin{displaymath}
\mathcal Y_{F} = \{y=\Phi(w,\,u^F,\,F)\}.
\end{displaymath}
The set of malicious nodes can be identified if and only if $\mathcal Y_F$s, for all $F\in \mathcal F$ are mutually disjoint. Now consider the opposite, there exists $F_1\neq F_2$ and $\mathcal Y_{F_1} \bigcap \mathcal Y_{F_2} \neq \phi$, which implies that 
\begin{displaymath}
\Phi(w,\,u^{F_1},\,F_1)=\Phi(w',\,v^{F_2},\,F_2),
\end{displaymath}
for some $w,\,w',\,u^{F_1},\,v^{F_2}$. However, due to strong resilience, $F_1=F_2$, which contradicts the assumption that $F_1\neq F_2$. Hence, the system can identify the set of malicious nodes. 

The reconstructability follows directly from the fact that $w$ is uniquely determined by $y =\Phi(w,\,u^F,\,F)$.
\end{proof}
In the next section we will relate the concepts of weak and strong resilience to the topology of the network.

\section{Main Result}\label{sec:main}

In this section we will characterize the resilience of the network by studying its topology. 

\subsection{Algebraic Conditions}
First, we wish to relate the concepts of strong and weak resilience via the following theorem.
\begin{theorem}
A system is strongly resilient to the $\mathcal F$-attacks if and only if it is weakly resilient to the $\mathcal G$-attacks, where the set $\mathcal G$ is defined as
\begin{displaymath}
\mathcal G = \{P\subset V:P=F_1\bigcup F_2,\,F_1,\,F_2\in \mathcal F\}.
\end{displaymath}
\label{theorem:weakstrongrelation}
\end{theorem}
\begin{proof}
First let us prove that $\mathcal F$ strong resilience implies weak resilience to $\mathcal G$-attacks. We prove that by contradiction. Suppose not, then there exists $G\in \mathcal G$ and $G\neq \phi$, such that
\begin{displaymath}
	\Phi(w,\,u^{G},\,G) = \Phi(0,\,0,\,\phi).
\end{displaymath}
From definition, we know that $G = F_1\bigcup F_2$, where $F_1,\,F_2 \in \mathcal F$ and $F_1\neq F_2$. It can be shown that 
\begin{displaymath}
u^G = u^a + u^b,	
\end{displaymath}
where $u^a$ ($u^b$) is non-zero only for the nodes that belong to $F_1$ ($F_2$). By linearity, it can be proved that 
\begin{displaymath}
	\Phi(w,\,u^{a},\,G) = \Phi(0,\,-u^b,\,G).
\end{displaymath}
Moreover, we can find $u^{F_1}$ and $u^{F_2}$ such that
\begin{displaymath}
	\Phi(w,\,u^{F_1},\,F_1) =\Phi(w,\,u^{a},\,G), \Phi(0,\,u^{F_2},\,F_2)=\Phi(0,\,-u^b,\,G),
\end{displaymath}
which contradicts the fact that the system is strongly resilient to $\mathcal F$-attacks.

The proof of the converse is similar and hence is omitted.
\end{proof}

As a result, we only need to consider weak resilience. The following definition is needed: 
\begin{definition}
Consider a linear system $(A,\,B,\,C)$ of the following form:
\begin{displaymath}
\begin{split}
x_{k+1} &= Ax_k + Bu_k,\,x_0=0,\\
y_k&=Cx_k.
\end{split}
\end{displaymath}
The system is called left-invertible if $y_k=0$ for all $k$ implies that $u_k=0$ for all $k$.
\end{definition}

From the definition, it is easy to see that the following theorem holds.
\begin{theorem}
The network is weakly resilience to $\mathcal F$-attacks if and only if for all possible $F\in \mathcal F$, the system $(A,\,[B\;B^F],\,C)$ is left invertible.
\end{theorem}

The following theorem characterize the relationship between the left invertibility and $(A,\,[B\;B^F],C)$.
\begin{theorem}[\cite{controltheory}]
The following statements are equivalent:
\begin{enumerate}
\item The system $(A,\,B,\,C)$ is left invertible.
\item The transfer function $K(z) = C(zI-A)^{-1}B$ has normal rank\footnote{If a rational matrix M(z) has normal rank $r$, it means that the rank of $M(z)$ is $r$ for all but finitely many $z$.} $m$.
\end{enumerate}
\label{theorem:invertibility}
\end{theorem}
Once the matrix $A$ is specified, one can use the above theorem to check if the system is weakly or strongly resilient to $\mathcal F$-attacks. However, this could be cumbersome since one needs to check for all the possible $F\in \mathcal F$. Moreover, the above theorem does not provide any insight on the relationship between network topology and resilience. In the next subsection, we characterize such relationship, based on the generic properties of structured systems.

\subsection{Topological Conditions}
In this subsection, we want to relate weak and strong resilience with the topology of the network. We first introduce the concept of structured system. Consider the linear system
\begin{displaymath}
\begin{split}
x_{k+1} &= Ax_k + Bu_k,\\
y_k&=Cx_k.
\end{split}
\end{displaymath}
The system is structured if the elements of matrices $A,\,B,\,C$ are either fixed zeros or free parameters. Suppose that there are $l$ elements in $A,\,B,\,C$ matrices that are not fixed zeros. We can parameterize the system by a vector $\lambda \in \mathbb R^l$. As a result, we will write the structured system as
\begin{displaymath}
\begin{split}
x_{k+1} &= A_{\lambda}x_k + B_{\lambda}u_k,\\
y_k&=C_{\lambda}x_k.
\end{split}
\end{displaymath}

A structured linear system can be represented by a directed graph $H = \{V_H,\,E_H\}$, where $V_H = U\bigcup X\bigcup Y$. $U = \{u_1,\ldots,u_p\}$, $X = \{x_1,\ldots,x_n\}$ and $Y=\{y_1,\ldots,y_m\}$ represent the inputs, states and outputs respectively. $p,\,n,\,m$ are the dimensions of inputs, states and outputs. The edge set is defined as
\begin{displaymath}
\begin{split}
E_H &= \{(u_j,x_i):\textrm{$B_{ij}$ is not a fixed zero.}\} \\
&\bigcup \{(x_j,x_i):\textrm{$A_{ij}$ is not a fixed zero.}\}\\
&\bigcup \{(x_j,y_i):\textrm{$C_{ij}$ is not a fixed zero.}\}.
\end{split}
\end{displaymath}

For a structured system, we have the following theorem:
\begin{theorem}[\cite{generic}]
Let the transfer function be $K_\lambda = C_\lambda (zI-A_\lambda)^{-1}B_\lambda$ and let $nrank(K_\lambda)$ denote its normal rank. Define
\begin{displaymath}
r = \max_{\lambda\in \mathbb R^k}\{nrank (K_\lambda)\},\,R = \{\lambda\in\mathbb R^k:nrank(K_\lambda)\neq r\}.
\end{displaymath}
Then $R$ has Lebesgue measure 0.
\label{theorem:grank}
\end{theorem}
$r$ is call the generic normal rank of the transfer function $K_\lambda$. The above theorem indicates that for almost all possible realizations of the structured system, the normal rank of the transfer function will be $r$. We can further relate the generic normal rank with the graph associated to the structured system.

\begin{theorem}[\cite{generic}]
The generic normal rank of the transfer function $K_\lambda$ equals the maximal size of linking in $H$ from $U$ to $Y$. 
\label{theorem:granktopology}
\end{theorem}

For our system, the $A$ matrix is structured. The matrices $B,\,B^F,\,C$ have fixed zeros and ones. However, the fixed ones can be changed to free parameters by proper scaling. Therefore, the system is structured. We are now ready to prove the main theorem
\begin{theorem}
If for all $F \in \mathcal F$, there exists a $p+|F|$ linking from $S\bigcup F$ to $T$, then for almost all possible realizations of $A$, the system is weakly resilient against $\mathcal F$-attacks. If there exists an $F\in \mathcal F$, such that there does not exists a $p+|F|$ linking from $S\bigcup F$ to $T$, then the system is not weakly resilient against $\mathcal F$-attacks regardless of $A$.
\label{theorem:topologyweak}
\end{theorem}
\begin{proof}
Consider the graph $H = \{V_H,\,E_H\}$ associated with the structured system $(A,\,[B\;B^F],\,C)$. It can be easily seen that $H$ is an augmented graph of the original topology $G = \{V,\,E\}$. To be specific, $V_H = V\bigcup U\bigcup Y$, where $U$ contains $p+|F|$ vertices and $Y$ contains $m$ vertices. Each vertex in $U$ is connected to a vertex in $S\bigcup F$ and each vertex in $Y$ is connected by a vertex in $T$. As a result, for every linking in $H$ from $U$ to $Y$, there exists a linking from $S\bigcup F$ to $T$ and vice-versa. Therefore, by Theorem~\ref{theorem:invertibility}, \ref{theorem:grank} and \ref{theorem:granktopology} we can complete the proof.
\end{proof}
Combining Theorem~\ref{theorem:topologyweak} and \ref{theorem:weakstrongrelation}, we have the following corollary:
\begin{corollary}
If for all $F_1,\,F_2 \in \mathcal F$, there exists a $p+|F_1\bigcup F_2|$ linking from $S\bigcup F_1\bigcup F_2$ to $T$, then for almost all possible realizations of $A$, the system is strongly resilient against $\mathcal F$-attacks. If there exist $F_1,\,F_2\in \mathcal F$, such that there does not exists a $p+|F_1\bigcup F_2|$ linking from $S\bigcup F_1\bigcup F_2$ to $T$, then the system is not strongly resilient against $\mathcal F$-attacks regardless of $A$.
\end{corollary}

Since the generic resilience of the system is only related to the topology of the system, as shown in Theorem~\ref{theorem:topologyweak}, we have the following definition:
\begin{definition}
	A topology $G = (V,E)$ is said to be weakly (strongly) resilient to $\mathcal F$-attacks, if and only if for any $F\in \mathcal F$ ($F_1,\,F_2\in \mathcal F$), there exists an $p+|F|$ ($p+|F_1+F_2|$) linking from $S\bigcup F$ ($S\bigcup F_1\bigcup F_2$) to $T$. 
\end{definition}

To illustrate Theorem~\ref{theorem:topologyweak}, let us consider the network shown in Fig~\ref{fig:example}. Suppose at most two nodes can be compromised. There are 4 possible scenarios due to symmetry shown in Fig~\ref{fig:example}\subref{fig:subfig1}\subref{fig:subfig2}\subref{fig:subfig3}\subref{fig:subfig4} respectively. The compromised nodes are denoted as the red nodes and the linking is marked with red lines. It can be seen that the topology is weakly resilient to two compromised nodes and is strongly resilient to one compromised node.

\begin{figure*}[ht]
\centering
\subfigure[]{
\includegraphics[width=0.4\textwidth]{pic.4}
\label{fig:subfig1}
} \qquad
\subfigure[]{
\includegraphics[width=0.4\textwidth]{pic.5}
\label{fig:subfig2}
}\\
\subfigure[]{
\includegraphics[width=0.4\textwidth]{pic.6}
\label{fig:subfig3}
}\qquad
\subfigure[]{
\includegraphics[width=0.4\textwidth]{pic.7}
\label{fig:subfig4}
}
\caption{Linking from $S\bigcup F$ to $T$}
\label{fig:example}
\end{figure*}

In the next section we propose a technique to improve the resilience of the system using trustworthy nodes.

\section{Enhancing the Resilience with Trustworthy Nodes}\label{sec:enhancing}
It is clear from the Section~\ref{sec:main} that the resilience of network is directly related to the connectivity of the topology. As a result, one trivial way to improve the resilience is to add more communication links to the network. However, such a solution is not always feasible due to various constraints such as bandwidth and energy. Another approach is to augment some routers with more states without modifying the topology, which will be discussed in this section.

To be specific, let us assume that a router $i$ has $n^O_i$ outgoing neighbors. We assume that router $i$ updates a vector state $X_{k,i} \in \mathbb R^{n^O_i}$ using the following update equation:
\begin{equation}
	X_{k+1,i} = A_{i,i} X_{k,i} + \sum_{j\in N^I{i}}A_{i,j} z_{k,j,i} ,
	\label{eq:updatevector}
\end{equation}
where $A_{i,i}\in \mathbb R^{n^O_i\times n^O_i}$ and $A_{i,j} \in \mathbb R^{n^O_i}$. The router sends its outgoing neighbors a scalar message, which is a linear combination of all its states: 
\begin{equation}
	z_{k,i,j} = D_{i,j} X_{k,i}, j\in \mathcal N^O_{i},
	\label{eq:transmissionvector}
\end{equation}
where $D_{i,j}\in \mathbb R^{1\times n^O_i}$. 

\begin{remark}
An augmented node needs more computational power to update its states. Also more bandwidth is needed since the node needs to singlecast instead of broadcasting.
\end{remark}
Since each state variable is a vertex in graph $H$ associated with the structured linear system, an augmented node $i$ will be represented by a collection of $n^O_i$ nodes in $H$, all of which are connected from the incoming neighbors and to the outgoing neighbors of $i$ and interconnected with each other. As a result, augmented nodes increase the connectivity of the graph $H$. However, it is harder to detect an attack when the attacker compromises augmented nodes. As a result, it is important that such nodes are well protected, which leads to the following definition: 
\begin{definition}
	An augmented node is called trustworthy if it does not belong to $T$ or $S$ and is guaranteed to follow the update equation \eqref{eq:updatevector} and the transmission equation \eqref{eq:transmissionvector}. 
\end{definition}
\begin{remark}
	Tamper-resistant microprocessors can be used for augmented nodes to prevent the adversary from compromising them. 
\end{remark}
In the rest of the paper we will assume that all the augmented nodes are trustworthy.

Given a graph $G$ and the set of trustworthy node $Q$, we can define a new graph $G' = \{V',E'\} = \mathcal M(G,Q)$ by removing trustworthy nodes. To be specific, first suppose there is only one trustworthy node in the network. As a result, $Q = \{q\}$, where $p < q < n-m+1$. Then 
\begin{displaymath}
V' = V\backslash \{q\}.
\end{displaymath}
Define the set of incoming edges of $q$ as $E^I(q) = \{(i,q)\in E:i\in N^I_q\}$ and set of outgoing edges as $E^O_q = \{(q,i)\in E:i\in N^O_q\}$. Also define the set $E^{IO}_q = \{(i,j):i\in N^I_q,\,j\in N^O_q\}$, which connects all the incoming neighbors of $q$ to its outgoing neighbors. Note that $E^{IO}_q$ may not be a subset of $E$, which means some of the links may not exist in $E$. Then $E'$ can be defined as
\begin{displaymath}
E' = (E\bigcup E^{IO}_q) \backslash (E^I_q\bigcup E^O_q).
\end{displaymath}
In other words, $E'$ is defined as removing all the edges containing $q$ and then directly connecting all the incoming neighbors of $q$ to its outgoing neighbors. Suppose $Q$ contains more than one trustworthy node, then the function $\mathcal M$ can be defined recursively as
\begin{displaymath}
\mathcal M(G,\,Q) = \mathcal M(\mathcal M(G,\,Q\backslash\{q\}),\{q\}),
\end{displaymath}
where $q \in Q$. We call $G' = \mathcal M(G,\,Q)$ the reduced topology. Now we have the following theorem to characterize the effect of trustworthy node on the network.
\begin{theorem}
The sensor network with a set of trustworthy nodes $Q$ is generically weakly(strongly) resilient to $\mathcal F$-attacks if the reduced topology $G' = \mathcal M(G,Q)$ is weakly(strongly) resilient to $\mathcal F$-attacks, where $\mathcal F$ is given by
\begin{displaymath}
\mathcal F = \{F \subset V:|F|\leq f,\,F\bigcap S = \phi,\,F\bigcap Q =\phi\}.
\end{displaymath}
\label{thereom:reduce}
\end{theorem}
\begin{proof}
The proof can be carried out by constructing a one to one correspondence of a linking in $H$(from $U$ to $Y$) to a linking in $G'$ (from $S\bigcup F$ to $T$). The technical details of the proof is omitted.
\end{proof}

The effect of trustworthy node can be illustrated by the following example. Consider the network shown in Fig~\ref{fig:trust}. Suppose that each node updates the state according to the following equation:
\begin{displaymath}
	x_{k+1,i} = \sum_{j\in \mathcal N^I(i)} x_{k,j} + \delta_{1,i}w_{k,1}.
\end{displaymath}
In other words, each router updates its state to the sum of states of its incoming neighbors. Suppose that the red node $2$ is compromised. The fusion center cannot detect that (see Theorem~\ref{theorem:topologyweak}). In fact, if the attacker injects a sequence $u$, it can be proved that
\begin{displaymath}
	\Phi(w,\,u,\,\{2\}) = \Phi(w+u,\,0,\,\phi).	
\end{displaymath}
\begin{figure}[htpb]
	\centering
    \includegraphics[width = 0.45 \textwidth]{pic.2}
  \caption{Wireless Sensor Network with Trustworthy Node}
  \label{fig:trust}
\end{figure}

Such vulnerability is caused by the blue node $4$ which acts as a bottle-neck for the network. If we instead replace node $4$ with a trustworthy node, and change its update equation to
\begin{displaymath}
	X_{k+1,4} =[x_{k,2},\,x_{k,3}]', 	
\end{displaymath}
and transmission equations to
\begin{displaymath}
	z_{k,4,5} = [1,\,0]X_{k,4},\, z_{k,4,6} = [0,\,1]X_{k,4},
\end{displaymath}
then it can be proved that the system is weakly resilient to one malicious node by Theorem~\ref{thereom:reduce}. The reduced topology is illustrated in Fig~\ref{fig:removetrust}.

\begin{figure}[htpb]
	\centering
    \includegraphics[width = 0.45 \textwidth]{pic.3}
  \caption{Reduced Topology of the Sensor Network after Removing of Trustworthy Node}
  \label{fig:removetrust}
\end{figure}

\section{Conclusion and Future Work}\label{sec:conclusion}

In this paper we propose a secure data transmission protocol for sensor networks. We define the resilience of the protocol against malicious attacks and relate it with the topology of the network. We further discuss how to improve the resilience of the network by using trustworthy sensors. Future work includes the development of efficient algorithms to identify the set of malicious nodes and reconstruct the input. We would also like to generalize the results to distributed control systems.

\bibliographystyle{IEEEtran}
\bibliography{SNsecurity}
\end{document} 

