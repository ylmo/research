\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{subfigure}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
\mode<presentation>

\title[False Data Injection]{Secure Data Transmission in Multi-Hop Sensor Networks}
\author[Yilin Mo]{Yilin Mo, Bruno Sinopoli}
\institute[Carnegie Mellon University]{Department of Electrical and Computer Engineering, Carnegie Mellon University}
\date[CPS Week 2011]{Workshop on Foundations of Dependable and Secure Cyber-Physical Systems} 
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\begin{frame}{Motivations}
  \begin{itemize}
    \item Sensor Networks are widely used in: manufacturing, health care, traffic control\dots
    \item Sensors usually have limited computation and communication power. 
    \item Advance cryptography methods may not be applicable.
    \item The measurements may need to be transmitted back to the fusion center through a multi-hop network.
    \item Our goal is to analyze and design the transmission protocol in the presence of compromised routers.
  \end{itemize}
\end{frame}

\section{Problem Formulation}
\begin{frame}{Network Model}
  \begin{enumerate}
    \item $p$ sensors are used for monitoring, the set of which is defined as $S$.
    \item $n-p$ routers are used to relay the measurements back to the fusion center.
    \item The fusion center listens to $m$ routers, the set of which is defined as $T$.
    \item The network is modeled as a directed graph $G = \{V,\,E\}$.
  \end{enumerate}
  \begin{figure}[]
    \begin{center}
      \includegraphics[width=0.7\textwidth]{../pic.1}
    \end{center}
    \caption{Multi-Hop Sensor Network}
  \end{figure}
\end{frame}

\begin{frame}{Graph Theory}
  \begin{itemize}
    \item The sets of incoming and outgoing neighbors of node $i$ are defined as
      \begin{displaymath}
	N^I_i = \{j\in V:(j,i)\in E\},\;N^O_i = \{j\in V:(i,j)\in E\}.	
      \end{displaymath}
    \item A path from $W$ to $W'$ ($W,\,W'\subset V$) is defined as a sequence of vertices $i_1,\ldots,i_l$, where $(i_j,i_{j+1})\in E$ and $i_1\in W$, $i_l\in W'$.
    \item Two paths are disjoint if they do not share common vertices.
  \end{itemize}
  \begin{figure}[]
    \begin{center}
      \includegraphics[width=0.4\textwidth]{../pic.8}
    \end{center}
    \caption{$2$ disjoint paths from $S$ to $T$}
  \end{figure}
\end{frame}

\begin{frame}{Transmission Protocol}
    \begin{itemize}
      \item Each node keeps a scalar state $x_{k,i}$
      \item At each time step, every node broadcasts its own state.
      \item After receiving all the data from its incoming neighbors, each node uses the following update rule:
	\begin{displaymath}
	  x_{k+1,i} = \left\{ {\begin{array}{*{20}c}
	    \sum_{j\in N_i^I}a_{i,j}x_{k,i}&\textrm{$i$ is a router} \\
w_{k,i}&\textrm{$i$ is a sensor} 
\end{array}} \right.,\,x_{0,i}=0.
	\end{displaymath}
      \item The fusion center receives
	\begin{displaymath}
	  y_k = [x_{k,i_1},\ldots,x_{k,i_m}]',\, 
	\end{displaymath}
	where $\{i_1,\ldots,i_m\} = T$.
    \end{itemize}
\end{frame}

\begin{frame}{False Data Injection Attack Model}
  \begin{itemize}
    \item We assume that $f$ routers are compromised, the set of which is defined as $F$.
    \item The compromised routers use the following update equation:
      \begin{displaymath}
	x_{k+1,i} =  \sum_{j\in N_i^I}a_{i,j}x_{k,i}+u_{k,i},\,x_{0,i}=0.
      \end{displaymath}
    \item Given observations $y_0,\ldots,y_k,\ldots$, the fusion center wants to \alert{detect} the presence of the attack, \alert{identify} the compromised sensors and \alert{reconstruct} the sensor measurements. 
  \end{itemize}
\end{frame}
\section{Main Results}
\begin{frame}{Matrix Form}
  \begin{itemize}
    \item Benign nodes:
      \begin{displaymath}
	x_{k+1,i} =  \sum_{j\in N_i^I}a_{i,j}x_{k,i}\mathbf I_{\textrm{router}}+w_{k,i}\mathbf I_{\textrm{sensor}},\,x_{0,i}=0.
      \end{displaymath}
    \item Compromised Routers:
      \begin{displaymath}
	x_{k+1,i} = \sum_{j\in N_i^I}a_{i,j}x_{k,i}+u_{k,i},\,x_{0,i}=0.
      \end{displaymath}
    \item Fusion Center:
      \begin{displaymath}
	y_k = [x_{k,i_1},\ldots,x_{k,i_m}]',\, 
      \end{displaymath}
    \item Matrix Form:
      \begin{displaymath}
	\begin{split}
	  x_{k+1} &= Ax_k + Bw_k + B^F u_k,x_0 = 0.\\
	  y_k&=Cx_k.
	\end{split}
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Weak Resilience and Strong Resilience}
  Define the trajectory of $y$ as a function of $u,\,w$ and $F$.  
  \begin{displaymath}
    y = \Phi(w,F,u).
  \end{displaymath}
  \begin{definition}
    A system is weakly resilient to $f$ compromised routers if for any $|F| = f$, 
    \begin{displaymath}
     \Phi(w^1,F,u) = \Phi(w^2,F,0) 
    \end{displaymath}
    implies that $w^1 = w^2$, $u = 0$.

    It is strongly resilient to $f$ compromised routers if for any $|F^1|=|F^2| = f$, 
    \begin{displaymath}
     \Phi(w^1,F^1,u^1) = \Phi(w^2,F^2,u^2).
    \end{displaymath}
    implies that $w^1 = w^2$, $F^1 = F^2$ and $u^1 = u^2$.
  \end{definition}
\end{frame}

\begin{frame}{Weak and Strong Resilience}
  \begin{itemize}
    \item Weak resilience implies that the system can \alert{detect} the presence of malicious behavior.
      \begin{displaymath}
	\begin{split}
     \Phi(w^1,F,u) = \Phi(w^2,F,0)\; &\Rightarrow \;u =0\\
     \Phi(w^1,F,u) \neq \Phi(w^2,F,0)\; &\Leftarrow \;u \neq 0
	\end{split}
      \end{displaymath}
    \item Strong resilience implies that the system can not only detect the presence of malicious behavior, but also \alert{identify} the malicious nodes and \alert{reconstruct} the sensor measurements.
    \item A system is strongly resilient to $f$ compromised routers if and only if it is weakly resilient to $2f$ compromised routers.
    \item We only need to consider weak resilience.
  \end{itemize}
\end{frame}

\begin{frame}{Algebraic Characterization of Weak Resilience}
  \begin{definition}
    A linear system $(A,B,C)$:
    \begin{displaymath}
      \begin{split}
      x_{k+1} &= Ax_k + Bu_k,\,x_0 = 0,\\
      y_k &= Cx_k,
      \end{split}
    \end{displaymath}
    is left-invertible if $y = 0$ implies $u = 0$.
  \end{definition}
  \begin{theorem}
    The following statements are equivalent:
    \begin{itemize}
      \item The system is weakly resilient to $f$ compromised routers.
      \item For any $|F| = f$, the system $(A,[B\,B^F],C)$ is left-invertible.
      \item For any $|F| = f$, the transfer function 
	$  K(\lambda,F) = C( \lambda I_n - A)^{-1} [B\,B^F]$
	has full normal rank $p+f$.
    \end{itemize}
  \end{theorem}
\end{frame}

\begin{frame}{Parameter Space}
  \begin{itemize}
    \item The elements in $A$ are either fixed $0$, or free design parameters.
    \item $A$ is completely parameterized by $\vec{a} \in \mathbb R^{|E|}$.
  \end{itemize}
  \begin{theorem}
    Define the set
    \begin{displaymath}
      \begin{split}
	R &= \{\vec{a}\in \mathbb R^{|E|}:\\
	&\textrm{The system is weakly resilient to $f$ compromised routers.}\}.
      \end{split}
    \end{displaymath}
    Then there are two possibilities:
    \begin{enumerate}
      \item $R = \phi$:\alert{ no parameter is good}.
      \item $R$ is open and dense in parameter space, the complement of $R$ has Lebesgue measure 0:\alert{ almost all parameters are good}.
    \end{enumerate}
  \end{theorem}
\end{frame}

\begin{frame}{Topological Characterization of Weak Resilience}
  \begin{theorem}
    $R$ is open and dense in parameter space if and only if for any $|F| = f$, there exists $p+f$ mutually disjoint paths from $F\bigcup S$ to $T$.
    
    $R$ is empty if and only if there exists $|F| = f$, such that there is no $p+f$ mutually disjoint paths from $F\bigcup S$ to $T$.
  \end{theorem}
  \begin{figure}[]
    \begin{center}
      \includegraphics[width=0.7\textwidth]{../pic.9}
    \end{center}
  \end{figure}

\end{frame}

\section{Enhancing Resilience}

%\begin{frame}{Connectivity}
%  The maximum size of a linking from $S\bigcup F$ to $T$ equals the minimum number of vertices which need to be removed to separate $S\bigcup F$ and $T$. (similar to Max-flow Min-cut Theorem.)
% \begin{overlayarea}{\textwidth}{2cm}
 %  \begin{figure}
  %   \centering
%   \only<1|handout:0>{\includegraphics[width=0.7\textwidth]{../pic.2}}
%   \only<2-|handout:1>{\includegraphics[width=0.7\textwidth]{../pic.10}}
% \end{figure}
%  \end{overlayarea}
%\end{frame}

\begin{frame}{Augmented and Trustworthy Router}
  \begin{figure}[]
    \begin{center}
      \includegraphics[width=0.5\textwidth]{../pic.2}
    \end{center}
  \end{figure}
  \begin{itemize}
    \item Node $4$ is the bottleneck.
    \item We can give node $4$ more computational power and bandwidth to increase the connectivity.
    \item Suppose node $4$ can keep $2$ states instead of one, and updates them by
      \begin{displaymath}
	X_{k+1,4} = A_{4,4}X_{k,4} + A_{4,2}x_{k,2} + A_{4,3}x_{k,3},
      \end{displaymath}
    \item Suppose node $4$ sends $D_{5,4}X_{k,4}$ and $D_{6,4}X_{k,4}$ to node $5$ and $6$ respectively instead of broadcasting.
  \end{itemize}
\end{frame}

\begin{frame}{Augmented and Trustworthy Router}
  \begin{figure}[]
    \begin{center}
      \includegraphics[width=0.5\textwidth]{../pic.3}
    \end{center}
    \caption{Equivalent Topology}
  \end{figure}
  \begin{itemize}
    \item If we use an augmented node $4$, the equivalent topology is given by the above plot. 
    \item However, if the attacker compromises node $4$, then it has ``control'' over two nodes.
    \item It is important that the augmented nodes are well protected.
    \item A node is trustworthy if it cannot be compromised by the attacker. (Tamper resistance devices)
  \end{itemize}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
In this paper, we consider the resilience of the transmission protocol in SNs against false data injection attacks:
\begin{itemize}
  \item We define the false data injection attack model.
  \item We define the concepts of weak and strong resilience and relate them with the detectability, identifiability of the attack and reconstructability of the sensor measurements.
  \item We provide a complete characterization of the weak and strong resilience, both algebraic and topological.
  \item We propose a method to increase the resilience of the system.
\end{itemize}
\end{frame}

\end{document}

