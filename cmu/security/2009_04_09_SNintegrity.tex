\documentclass[letterpaper,10 pt,conference]{IEEEconf}
%\overrideIEEEmargins
%\IEEEoverridecommandlockouts

\usepackage{color}
\usepackage[dvips]{graphicx}
\DeclareGraphicsRule{*}{mps}{*}{}
\usepackage{stmaryrd}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{cite}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{definitionNoNumber}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{propositionNoNumber}{Proposition}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}

\title{\LARGE \bf {Semantic Integrity in Sensor Networks}}

\author{Yilin $\textrm{Mo}^*$, Annarita $\textrm{Giani}^{\dag}$ Roberto $\textrm{Ambrosino}^{\ddag}$, Bruno $\textrm{Sinopoli}^*$, Adrian $\textrm{Perrig}^*$, Shankar $\textrm{Sastry}^{\dag}$
\thanks{
$*$: Department of Electrical and Computer Engineering, Carnegie Mellon University, Pittsburgh, PA. Email: {ymo@andrew.cmu.edu}, {brunos@ece.cmu.edu}}
\thanks{
$\dag$: Department of Electrical Engineering and Computer Science, University of California, Berkeley, CA. Email: {agiani@eecs.berkeley.edu}
}
\thanks{
$\ddag$: Dipartimento per le Tecnologie, Universit\`{a} degli Studi di Napoli Parthenope, Centro Direzionale di Napoli, Isola C4, 80143 Napoli, Italy. Email: {ambrosino@uniparthenope.it}.
}
\thanks{
This research has been partially supported  by TRUST (Team for Research in Ubiquitous Secure Technology) which receives support from the National Science Foundation (NSF award number CCF-0424422) and by CyLab at Carnegie Mellon under grant DAAD19-02-1-0389 from the Army Research Office. Foundation. The views and conclusions contained here are those of the authors and should not be interpreted as necessarily representing the official policies or endorsements, either express or implied, of ARO, CMU, or the U.S. Government or any of its agencies.}
}

\begin{document}
\maketitle
\begin{abstract}
Sensor networks are critical to many applications. So assuring data authenticity is a requirement. Successful attacks aim to influence the supervisor perception of the environment modifying the semantic of the information but leaving hardware and software untouched. This is done deceiving some of the nodes. Traditional intrusion detection systems hardly work against these type of attacks. In this paper we give a mathematical formulation of the problem, propose an integrity detection algorithm and analyze different attack strategy. The attack strategy that minimizes the detection probability is reported.


\end{abstract}

\section{Introduction}\label{Introduction}
Consider a sensor network. Each node senses the environment and transmits the sensed information to a base station that performs aggregation and data analysis. Examples are perimeter monitoring systems, equipment maintenance, habitat monitoring~\cite{Estrin}, medical application and structure health monitoring systems~\cite{Culler}. Based on the sensor data the base station acts as a correlation engine and gains knowledge of the condition of the environment. Such state can then be used to make decisions or control. For example can directly send an alert or the state can be given as input to a control system that operates on the monitored surrounding.

It is crucial to assure that the result is reliable, the aggregation algorithm is not compromised by an attacker and the input data reflect the real status of the environment surrounding the node. So the sensed data is authentic, not compromised during retrieval or transmission and comes from a well functioning sensor. Researchers have studied ways to verify if a software component was compromise and approaches have been proposed. In~\cite{bruno} the authors propose the use of software-only schemes that can be implemented on computing devices to provide verification of code integrity, untampered code execution and secure code updates. Key management and cryptography mechanism are powerful techniques to assure privacy and integrity of data. The data cannot be eavesdropped during transmission or the value modified. But even if these techniques are well implemented and operational, the system is not robust to certain attacks on integrity. Packets, for example, can be captured, and then retransmitted at a later time or not retransmitted at all.

We concentrate on a category of attacks that are performed at the sensing stage. Sensor networks are often integrated with the physical infrastructure and in the majority of the cases the infrastructure is not physically secured. This allows an attacker to alter the sensor reading approaching the node and modifying its neighborhood so that it senses artificial values. These are not technical attacks against the sensor network itself since hardware and software are genuine. So traditional intrusion detection techniques [Which ones... say something more] are not adequate. The attacker achieves her goal deceiving the node to believe a misrepresentation of the reality and naively introducing a lie into the monitoring system. We also assume that the attacker knows the reading of a subset of sensors. For example she knows the characteristics of the environment around those nodes. Integrity detection mechanisms against these types of attacks must be based on the semantic of the data transmitted from the nodes to the base station.

In this paper we focus on the class of attacks that modify the semantic of the data that circulate in the sensor network without tampering any physical or software device. The detector, implemented in the base station, only works on the values of the data. Given the state in which the monitored environment is in, it checks if the new incoming data are accordingly to the prediction. If the new readings are not within the expectation an alarm is triggered. We assume that the base station has the computational capability (in terms of memory, power and processor requirements) to perform the detection algorithm (... when? at each step?).

The proposed solution does not guarantee other security requirements. For example it does not protect the system against passive information gathering, data leaking or attacks against routing protocols. Different security mechanism must be implemented to guarantee confidentiality, authentication and freshness.

The main contributions of this paper are as follows.

\begin{itemize}
\item The paper gives a precise formulation, in mathematical term, of what assuring semantic integrity of a sensor network means.
\item We present a detector implemented in the base station and based on classical linear system estimation techniques. The proposed techniques is also used to calculate the false alarm rate at each step.
\item Assuming an attack goal, we present different attack strategies. The attack strategy that minimizes the probability of being detected is reported.
\end{itemize}

Section~\ref{Formulation} gives a mathematical formulation of the system architecture and the threat model together with a description of the security goals considered in the paper. The following section describes the detection algorithm. In section~\ref{Formulation} we present simulation results. And we finish the paper with our conclusions and future work.

\bigskip


\section{Problem Formulation}\label{Formulation}

\subsection{Physical System}
Consider the following linear, time invariant (LTI) system whose state dynamics are given by
\begin{equation}
    x_{k+1} = Ax_k + w_k + B^du_k^d +  B^a u_k^a
  \label{eq:systemdescription}
\end{equation}
where  $x_k \in \mathbb R^n$ is the vector of the state variables at time $k$, $w_k\in \mathbb R^n$ is the process noise at time $k$ and $x_0$ is the initial state. We assume $w_k,\,x_0$ are independent Gaussian random variables, $x_0 \sim \mathcal N(0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$.  $u^d_k$ is the control from controller and $u^a_k$ is the control from attacker. $u^a_k = 0$ if there is no attack. Matrix $B^a$ restrict in which way the attacker can control the system. 

The system represents the evolution of the state of a particular environment with respect to some characteristics of interest. Imagine our system is a smart skyscraper and we are interested in monitoring the variation of temperature. $x_{k}$ is the state of the skyscraper, in which case the temperature, at step $k$. System \eqref{eq:systemdescription} mathematically describe that the state at the following step depends on the state at the current step, some process noise(uncertainties in the environment), the control of the central AC unit, and the input of the attacker.  

\subsection{Sensor Network}
A wireless sensor network is composed of $m$ sensing devices $s_1,\ldots,s_m$. At each step all the sensor readings are sent to a base station that monitors the state of system \eqref{eq:systemdescription}. State \eqref{eq:systemdescription} can be observed thought the sensor data in the following way
\begin{equation}
    y_{k} = C x_k + v_k.
  \label{eq:sensordescription}
\end{equation}
where $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}]^T \in \mathbb R^m$ is the vector of the true measurements from the sensors and $v_k \sim \mathcal N(0,\;R)$ is the measurement noise independent of $x_0$ and $w_k$. Each element $y_{k,i}$ represents the measure of the sensor $i$ at time $k$.

\subsection{Threat Model}

Let us assume that a malicious third party is attacking the LTI system and the sensor network described by \eqref{eq:systemdescription} simultaneously. In particular, we suppose the attacker has the capability to perform the following actions:
\begin{itemize}
\item She can give an arbitrary input $u^a_k$ to the system at anytime.

For example she can put an entire floor of the skyscraper on fire raising the temperature drastically and changing the overall state. The possible points of attacks are restricted by the matrix $B^a$ in \eqref{eq:systemdescription}.
\item She knows the reading of a subset $\mathcal S$ of sensors.

To get the reading of sensors, the attacker needs to break the encryption schema. Which may be realistic since sensor networks often use weak encryption protocol, and some of the sensor information may be not encrypted at all.

\item She can spoof herself to be the sensors in set $\mathcal T$, which means that she can modify the $y_{k,i}$ into arbitrary $\tilde y_{k,i}$, if $i \in \mathcal T$.

  Depending on her goal, the attacker can control the measurements. Bringing a cooler next to the sensors make the temperature decrease while with a heater she easily forces the temperature to increase. Also the attacker can break the authentication protocol to send wrong readings. Hence, the readings that the estimator received is not $y_k$, but
  \begin{equation}
    y_k^d  = y_k + y^a_k.
  \end{equation}
\end{itemize}

Sometimes the attacker can compromise all the sensors, which means that the subset $\mathcal S,\,\mathcal T$ is the set of all sensor. In that case, we say the attacker is doing a full attack over the sensor network. Otherwise we say the attacker only does a partial attack.

\subsection{Controller and Estimator}

We assume that a linear estimator and linear controller is implemented at the base station. The controller and estimator are of the following form:
\begin{align}
  \hat x_{k|k-1} &= A \hat x_{k-1|k-1} + B^d u^d_k,\\
  \hat x_{k|k} & = \hat x_{k|k-1} + K_e (y_k^d - C \hat x_{k|k-1}),\\
  u^d_k&=K_c\hat x_{k|k} + \Delta u_k.
\end{align}
We also assume that the control gain and estimation gain $K_c$ and $K_e$ are fixed, which is often the case for LQG controller and Kalman filter since they reach stationary state very fast. $\Delta u_k$ is the control signal to detect the presence of the attacker, for which we will explain in the next subsection.

\subsection{Detector}
Besides the estimator and controller, the fusion center will also implement an detector to detect abnormality in the system and trigger an alarm. After that we assume that much more powerful tools will be used and the attack can be stopped.

An detector is said to be passive if $\Delta u_k = 0$ for all $k$. It is active otherwise. 
\section{Main Result}\label{Result}
\subsection{Passive detector v.s. Partial Attack}



\subsection{Active detector v.s. Full Attack}

Suppose that the attacker can actually controls all the reading from the sensors.  In that case, the attacker can actually drive the states of the system to arbitrary points (as long as it is reachable) and a passive detector will fail to detect the attack since the attacker can carefully adjust the reading of the sensors to cheat the detector. In that case, only a active detector can detect abnormality in the system.

The key idea of active detect is to insert certain control signal into the system and see if the system gives back a corresponding response. Let us suppose that the detector chooses $\Delta u_k$ as an i.i.d. Gaussian random variable with $0$ mean and covariance to be $S$. In that case, when there is no attacker, the control feedback is not the optimal feedback for the original LQG problem. Suppose the objective function of the LQG control is
\begin{equation}
  J = \lim_{T\rightarrow \infty}\frac{1}{T}E\left[ \sum_{k=1}^T x_k^TW_kx_k+u^{dT}_kU_ku^d_k \right], 
\end{equation}
Then the loss in the LQG performance is

It is clear that the defender lost LQG performance in order to detect abnormality. When an attacker exists, let us suppose that he has observed the system for a long time, i.e. he knows the statistics of $y_k$ when there is no attack. His strategy is to let $y_k^d$ simulate the same $y_k$ and send it back to the fusion center.

The fusion center will check the covariance on $\Delta u_k$ and $y_k - C\hat x_{k|k-1}$. Without the attack, we know that
\begin{equation}
  E\Delta u_{k-1}(y_k - C\hat x_{k|k-1})^T = E\Delta u_{k-1} (y_k - Cx_k)^T + E\Delta u_{k-1} (Cx_k - C\hat x_{k|k-1})^T = 0.
\end{equation}
Under the attack, the fusion center will still check the covariance on $\Delta u_k$ and $y_k^d - C\hat x_{k|k-1}$. Since $y_k^a$ is completely generated by the attacker, it is independent of $\Delta u_k^a$. While $\hat x_{k|k-1}$ follows
\begin{align}
  \hat x_{k|k-1} = A\hat x_{k-1|k-1} + B^du^d_k,\\
  \hat x_{k|k} = \hat x_{k|k-1} + K_e (y_k^d - C\hat x_{k|k-1}).
\end{align}

Since $\hat x_{k-1|k-1}$ depends only on $y_k^d$ and $u^d_{1},\ldots,u^d_{k-2}$, it is independent of $\Delta u_{k-1}^d$. Hence, the covariance will be 
\begin{equation}
  E\Delta u_{k-1}(y_k^d - C\hat x_{k|k-1})^T = \Delta u_{k-1} \Delta u_{k-1}^T B^{dT} = SB^{dT}.  
\end{equation}

%\begin{align}
% \left[ {\begin{array}{*{20}c}
%   x_{k+1}\\
%   \hat x_{k+1}\\
%\end{array}} \right] &= \left[ {\begin{array}{*{20}c}
%  A&B^dK_c\\
%  0&(I-K_eC)(A + B^dK_c)
%\end{array}} \right] \left[ {\begin{array}{*{20}c}
%   x_{k}\\
%   \hat x_{k}\\
%\end{array}} \right]+ \left[ {\begin{array}{*{20}c}
%  B^au^a_k\\
%  K_ey^a_k\\
%\end{array}} \right]+\left[ {\begin{array}{*{20}c}
%  w_k + B^d\Delta u_k\\
%  (I-K_eC)B^d\Delta u_k\\
%\end{array}} \right] ,\\
%y_k &= Cx_k + v_k.
%\end{align}
%
%From the attacker's point of view, both $x_{k}$ and $\hat x_{k}$ are unknown and can be treated as the states of the whole cyber physical system. $u^a_k$ and $y^a_k$ are known input. $w_k$ and $\Delta u_k$ are process noise. $y_k$ is the measurement and $v_k$ is the measurement noise.

\section{Simulation Result}\label{Simulation}

\section{Conclusions}\label{Conclusion}


\bibliographystyle{IEEEtran}
\bibliography{SNsecurity}
\end{document} 
