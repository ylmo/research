clc
clear

N = 10;
d = 4;

%generate Laplacian
L = d*eye(N);
for i = 1:N
    for j = i+1:N
        if j-i <= d/2 || j-i - N >= -d/2
                L(i,j) = -1;
                L(j,i) = -1;
        end
    end
end

%generate optimal deterministic P
lambda = eig(L);
lambda = sort(lambda);
alpha = 2/(lambda(2) + lambda(N));
P = eye(N) - alpha*L;

%accuracy
epsilon = 10^-8;

%compute the stopping time for det case
lambda = eig(P);
lambda = sort(lambda);
rho = max(lambda(N-1),-lambda(2));
numiter = ceil(log(epsilon)/log(rho));
tmpP = P;
for i=1:N
    tmpP(i,i) = 0;
end
commperiter = size(find(tmpP>=0.0001),1);
commdet = commperiter*numiter;

%projection matrix
Proj = eye(N) - ones(N)/N;

Prob = P/N;


%generate W matrix
collectW = cell(N,N);
for i=1:N
    for j = 1:N
        tmpv = zeros(N,1);
        tmpw = zeros(N,1);
        tmpv(i) = 1;
        tmpw(j) = 1;
        W = eye(N) - (tmpv - tmpw)*(tmpv-tmpw)'/2;
        W = Proj*W;
        collectW{i,j} = W;
    end
end

%simulate first stopping time
kr = zeros(N^2);
for i = 1:N
    for j = 1:N
        W = collectW{i,j};
        kr = kr + Prob(i,j)*kron(W,W);
    end
end
rho = max(eig(kr));
commrand1 = 2*ceil(log(epsilon)/log(rho));

%generate tilde P
for i = 1:N
    Prob(i,i) = 0;
end
Prob = Prob/sum(sum(Prob));

%simulate second stopping time
sqrtepsilon = sqrt(epsilon);
commrand2 = zeros(1,1000);
for index = 1:1000
    alpha = 1;
    W = eye(1);
    while alpha >= sqrtepsilon
        %pick a random pair
        r = rand();
        for i = 1:N
            for j = 1:N
                r = r - Prob(i,j);
                if r <= 0
                    break
                end
            end
            if r <= 0
                break
            end
        end
        tmpW = collectW{i,j};
        W = tmpW*W;
        alpha = norm(W,2);
        commrand2(index) = commrand2(index) + 2;
    end
end
Ecommrand2=mean(commrand2)

