clear
clc

N = 10;
d = 5;

Proj = eye(N) - ones(N)/N;
collectW = cell(N,N);
for i=1:N
    for j = 1:N
        tmpv = zeros(N,1);
        tmpw = zeros(N,1);
        tmpv(i) = 1;
        tmpw(j) = 1;
        W = eye(N) - (tmpv - tmpw)*(tmpv-tmpw)'/2;
        W = Proj*W;
        collectW{i,j} = W;
    end
end

for k = 1:100
    k
    %generate graph
    A = zeros(N);

    for index = 1:N*d/2
        while true
            tmp = randperm(N);
            i = tmp(1);
            j = tmp(2);
            if A(i,j) == 0 
                A(i,j) = 1;
                A(j,i) = 1;
                break
            end
        end
    end

    L = diag(sum(A)) - A;

    %check connectivity
    lambda = eig(L);
    lambda = sort(lambda);
    if lambda(2) == 0
        break
    end
    %compute 
    P = eye(N) - 2*L/(lambda(2)+lambda(N));
    
    lambda = eig(P);
    Omega = -N*d/2/log(lambda(N-1));
    
    tP = P/N;
    
    kr = zeros(N^2);
    for i = 1:N
        for j = 1:N
            W = collectW{i,j};
            kr = kr + tP(i,j)*kron(W,W);
        end
    end
    rho = max(eig(kr));
    omega = -2/log(rho);
    
    plot(Omega,omega,'+');
    hold on
end

axis equal
axis([20,1000,20,1000])
hold on
plot([20 1000],[20 1000],'b','linewidth',2)

set(gca,'Fontsize',14);
xlabel('$\Omega(P)$','fontsize',14)
ylabel('$\omega(Q)$','fontsize',14)
set(get(gca,'XLabel'),'interpreter','latex');
set(get(gca,'YLabel'),'interpreter','latex');