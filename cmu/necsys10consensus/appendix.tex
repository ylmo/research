Before Proving Theorem~\ref{theorem:rategossip}, we need the following lemmas:
\begin{lemma}
  \label{lemma:randomwalk}
Let $\alpha_i$ be i.i.d. random variable that take values on finite alphabet. Moreover suppose the alphabet contains only non-positive values and $E\alpha_1 < 0$ is strictly negative. Define hitting time
\begin{equation}
  T_\varphi \triangleq \inf\{k\in \mathbb N: \sum_{i=1}^k \alpha_i \leq \varphi\}. 
  \label{eq:hittingtime}
\end{equation}
Then
\begin{displaymath}
  \lim_{\varphi\rightarrow -\infty}\frac{\mathbb ET_\varphi}{\varphi} =  \frac{1}{\mathbb E\alpha_1}.
\end{displaymath}
\end{lemma}
The proof involves applying optional sampling theorem on martingale $\{\sum_{i=1}^k (\alpha_i - E\alpha_i)\}$ and is omitted due to the space limit.

\begin{lemma}
  \label{lemma:asrate}
Suppose
\begin{displaymath}
  \gamma(P) = \inf_n \frac{1}{n}\mathbb E\log\|\prod_{k=1}^n \mathcal P W_k\mathcal P\|_2.
\end{displaymath}
Then almost surely the following holds
\begin{displaymath}
  \lim_{n\rightarrow \infty}  \frac{1}{n}\log\|(\prod_{k=1}^n \mathcal P W_k\mathcal P) \|_2 = \gamma(P).
\end{displaymath}
\end{lemma}
\begin{pf}
  This is a direct result of Theorem~2 in \cite{productrandommatrix}.
\end{pf}
Now we are ready to prove Theorem~\ref{theorem:rategossip}:
\begin{pf}
  \begin{enumerate}
    \item The proof for \eqref{eq:ineqcomplexityone} and \eqref{eq:eqcomplexityone} is omitted due to space limit. The readers can refer to \cite{boydgossip} and \cite{convergencerate} for more details. 
    \item Now we want to prove \eqref{eq:ineqcomplexitytwo}. Let us pick $\phi = (\rho(P)+\delta)^{-1}$, where $\delta>0$ is a small positive number. Let us define $z_k = \phi^k y_k'y_k/y_0'y_0$. By Theorem 2.3 from \cite{convergencerate}, one can prove
  \begin{displaymath}
    \mathbb E(z_{k}) \leq \eta (\rho(P)\phi)^{k}.
  \end{displaymath}
  Let us define a new random variable $Z = \sup_k z_k$. It is easy to see that $Z = \sup_k z_k \leq \sum_{k=0}^\infty z_k$. Take the expectation on both side and use the Monotone convergence theorem yields
  \begin{displaymath}
    \mathbb E Z \leq \mathbb E \left(\sum_{k=0}^\infty z_k \right)= \sum_{k=0}^\infty \mathbb Ez_k \leq\eta \sum_{k=0}^\infty (\rho(P)\phi)^{k} .
  \end{displaymath}
  The right side is less than infity since $\rho(P)\phi < 1$. Hence, we know that $Z$ is almost surely bounded. By definition, $y_k'y_k= \phi^{-k} z_k y_0'y_0 \leq \phi^{-k} Zy_0'y_0. $ Hence, let 
  \begin{displaymath}
    k_\varepsilon = \frac{\log(Z) - \log(\varepsilon)}{\log{\phi}}.
  \end{displaymath}
It is easy to show that for any $y_0\neq 0$, $y'_{k_\varepsilon} y_{k_\varepsilon}/y_0'y_0 \leq \phi^{-k_\varepsilon} Z \leq \varepsilon.$ As a result, $T_{2,\varepsilon} \leq k_\varepsilon$, which implies 
\begin{displaymath}
  \mathbb E T_{2,\varepsilon} \leq  \frac{\mathbb E\log(Z)-\log(\varepsilon)}{\log{\phi}}
\end{displaymath}
By Jensen's inequality, $\mathbb E\log(Z)  \leq \log(\mathbb EZ) < \infty$. Hence, 
\begin{displaymath}
  \limsup_{\varepsilon\rightarrow 0^+} -\frac{2T_{2,\varepsilon}}{\log(\varepsilon)} \leq -\frac{2}{\log{\phi}} = -\frac{2}{\log(\rho(P) + \delta)}. 
\end{displaymath}
Since $\delta$ can be arbitrary small, we know that \eqref{eq:ineqcomplexitytwo} holds.
\item Finally we want to prove \eqref{eq:eqcomplexitytwo}. First we want to prove that $\omega_2(P) \geq -\gamma(P)^{-1}$. By definition of $\varepsilon_k$, it is easy to see that
  \begin{displaymath}
    \varepsilon_k = \sup_{y_0\neq 0}\frac{y_k'y_k}{y_0'y_0} = \|\prod_{i=0}^{k-1}\mathcal PW_i\mathcal P\|_2^2
  \end{displaymath}
  By Lemma~\ref{lemma:asrate}, we know that almost surely
  \begin{displaymath}
    \lim_{k\rightarrow\infty} \frac{1}{k}\log \|\prod_{i=0}^{k-1}\mathcal P W_i \mathcal P \|_2 = \gamma(P),
  \end{displaymath}
  which implies that 
  \begin{displaymath}
    \lim_{k\rightarrow\infty} \frac{1}{k}\log \varepsilon_k = 2\gamma(P),
  \end{displaymath}
  By the definition of $T_{2,\varepsilon}$, it is easy to prove that 
  \begin{displaymath}
    \lim_{\varepsilon\rightarrow 0^+}-\frac{2T_{2,\varepsilon}}{\log(\varepsilon)} = -\frac{1}{\gamma(P)},\textrm{ almost surely}.
  \end{displaymath}
  By Fatou's lemma
  \begin{displaymath}
    \begin{split}
    -\frac{1}{\gamma(P)}&=\mathbb E\lim_{\varepsilon\rightarrow 0^+}-\frac{2T_{2,\varepsilon}}{\log(\varepsilon)} \leq \liminf_{\varepsilon\rightarrow 0^+}\mathbb E-\frac{2T_{2,\varepsilon}}{\log(\varepsilon)}\\
    &\leq  \limsup_{\varepsilon\rightarrow 0^+}\mathbb E-\frac{2T_{2,\varepsilon}}{\log(\varepsilon)} =\omega_2(P). 
    \end{split}
  \end{displaymath}
  Then we will prove $\omega_2(P) \leq -\gamma(P)^{-1}$. By the definition of $\gamma(P)$, we know that $\forall \delta > 0$, there exists an $m$, such that
  \begin{displaymath}
   \frac{1}{m}\mathbb E\log\|\prod_{k=1}^m \mathcal P W_k\mathcal P\|_2 \leq \gamma(P) + \delta.
  \end{displaymath}
  Let us define $\alpha_k = \log\|\prod_{i=km-m}^{km-1} \mathcal P W_i\mathcal P\|_2$. Since $T_{2,\varepsilon}$ is defined as $T_{2,\varepsilon} = \inf \{k:\varepsilon_k\leq \varepsilon\}$, we know that $T_{2,\varepsilon} \leq mT_\varphi$, where $T_\varphi$ is a stopping time defined as
\begin{displaymath}
  T_\varphi = \inf\{k:\sum_{i=1}^k\alpha_i\leq \varphi\}, 
\end{displaymath}
where $\varphi = \log(\varepsilon)/2$. It is easy to check $\alpha_k$ is non-positive since $\|W_k\|_2\leq 1$ and $\mathbb E \alpha_k \leq m(\gamma(P)+\delta)$. Moreover, $\alpha_k$ can only take values on a finite alphabet. As a result, we could apply Lemma~\ref{lemma:randomwalk}, which yields
\begin{displaymath}
  \lim_{\varphi\rightarrow -\infty}\mathbb E -\frac{T_\varphi}{\varphi} = \lim_{\varepsilon\rightarrow 0^+}\mathbb E -\frac{2T_\varphi}{\log(\varepsilon)} \leq -\frac{1}{m(\gamma(P)+\delta)}.
\end{displaymath}
By the definition of $\omega_2(P)$ and $T_{2,\varepsilon}$, we know that
\begin{displaymath}
  \begin{split}
  \omega_2(P)  & \leq   \limsup_{\varepsilon\rightarrow 0^+}\mathbb E-\frac{2mT_\varphi}{\log(\varepsilon)}\leq -\frac{1}{\gamma(P) + \delta}. 
  \end{split}
\end{displaymath}
Since $\delta$ can be arbitrary small, $\omega_2(P) \leq -1/\gamma(P)$, which finishes the proof.
\end{enumerate}
\end{pf}

%\begin{pf}[Proof of Lemma~\ref{lemma:ineq}]
%  \begin{enumerate}
%    \item Let 
%      \begin{displaymath}
%	f(x) = (1+\frac{x}{N})^N. 
%      \end{displaymath}
%      Suppose on the contrary, there exists $\alpha\in [-N,0]$, such that $f(\alpha) >e^\alpha$. Let us define 
%      \begin{displaymath}
%	\beta = \inf\{x\geq \alpha:f(\beta) = e^{\beta}\}.
%      \end{displaymath}
%     Since $f(0) = 1 = e^0$, $\beta \leq 0$ and $\beta > \alpha$. Moreover for any $x \in [\alpha,\beta]$, $f(x) \geq e^x$. Now by mean value theorem,
%     \begin{displaymath}
%       (f(\beta) - e^\beta) - (f(\alpha) - e^\alpha) = (\dot{f}(\gamma)-e^\gamma)(\beta-\alpha),
%     \end{displaymath}
%     for some $\gamma\in [\alpha,\beta]$. Since $f(\beta) = e^\beta$ and $f(\alpha) > e^\alpha$, 
%     \begin{displaymath}
%       \dot{f}(\gamma) - e^\gamma = \frac{f(\gamma)}{1+\gamma/N} - e^\gamma < 0.
%     \end{displaymath}
%     However
%     \begin{displaymath}
%        \frac{f(\gamma)}{1+\gamma/N} \geq \frac{e^\gamma}{1+\gamma/N} \geq e^\gamma,
%     \end{displaymath}
%     which contradicts with the fact $\dot{f}(\gamma) - e^\gamma <0$. Hence, \eqref{eq:ineqexp} holds.
%   \item If $y \geq 1$, then by mean value theorem
%     \begin{displaymath}
%       \log(x) -\log(y) = \xi^{-1}(x-y) \leq x-y,
%     \end{displaymath}
%     where $\xi \in [y,x]$. Hence, let us assume that $y< 1$. We will first prove that
%     \begin{equation}
%      \log(x) - \log(1/x) \leq x - 1/x,\;\;x\geq 1. 
%     \end{equation}
%     Let $g(x) = \log(x) -\log(1/x) = 2\log(x)$ and $h(x) = x - 1/x$. Hence $g(1) = h(1) = 0$ and
%     \begin{displaymath}
%       \dot{g}(x) - \dot{h}(x) = 2/x - 1 -1/x^2 = -(1-1/x)^2 \leq 0.  
%     \end{displaymath}
%     Using Taylor expansion, we know that when $x\geq 1$, 
%     \begin{displaymath}
%       g(x) - h(x) = g(1) - h(1) + [\dot{g}(\xi_1)-\dot{h}(\xi_1)](x-1) \leq 0,
%     \end{displaymath}
%     where $\xi_1\in[1,x]$. 
%     
%     Now we only need to prove that $\log(y) - \log(1/x) \geq y -1/x$. Again by mean value theorem
%     \begin{displaymath}
%       \log(y) - \log(1/x) = \xi_2^{-1}(y-1/x) \geq (y-1/x),
%     \end{displaymath}
%     where $\xi_2 \in [1/x,y]$.
% \end{enumerate}
%\end{pf}
