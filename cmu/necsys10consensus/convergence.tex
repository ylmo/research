In this section we will study the communication complexity, that characterizes the energy efficiency properties of average consensus. Let us define the accuracy of a consensus algorithm at $k^{th}$ step as
\begin{equation}
  \varepsilon_k \triangleq \sup_{y_0\neq 0} y_k'y_k/(y_0'y_0).
\end{equation}
The communication complexity can be seen as the average number of communications needed to reach certain accuracy. Depending on the stopping strategy, the communication complexity may vary. Let us consider two possible stopping scenarios:
\begin{enumerate}
  \item The consensus algorithm terminates after $k$ iterations, corresponding to the fact that the expected accuracy $\mathbb \varepsilon_k$ is less than $\varepsilon$.
  \item The consensus algorithm stops after reaching a certain accuracy $\varepsilon$. \footnote{For deterministic case, the two stopping strategy are essentially the same. However they are different in stochastic settings as will be proved in Theorem~\ref{theorem:rategossip}}
\end{enumerate}

Based on the above stopping scenarios, we define the following stopping times:
\begin{definition}
  Consider the accuracy $\varepsilon > 0$, stopping time $T_{1,\varepsilon}$ is defined as
  \begin{equation}
    T_{1,\varepsilon} \triangleq \inf\{k:\mathbb E\varepsilon_k \leq \varepsilon \}.
  \end{equation}
  Stopping time $T_{2,\varepsilon}$ is defined as
  \begin{equation}
    T_{2,\varepsilon} \triangleq \inf\{k:\varepsilon_k \leq \varepsilon \}.
  \end{equation}
\end{definition}
It is easy to see that each stopping time corresponds to a stopping criterion. To evaluate the performance of consensus, we have the following definitions.
\begin{definition}
  Define $c_k$ to be the number of communications incurred at the $k^{th}$ iteration. The communication complexity correspond to the two stopping times is defined as:
  \begin{equation}
    \Omega_i = \omega_i = \limsup_{\varepsilon\rightarrow0^+}-\frac{\mathbb E\sum_{k=0}^{T_{i,\varepsilon}}c_k}{\log(\varepsilon)},\,i=1,2. 
    \label{eq:communicationcomplexity}
  \end{equation}
\end{definition}

\begin{remark}
  To avoid confusion, we will use $\Omega_i$ to indicate the communication complexity of the deterministic algorithm and $\omega_i$ to indicate the one relative to the gossip algorithm.
\end{remark}

\begin{remark}
 % It is worth noticing that in the literature, usually the performance of a consensus algorithm is judged by the convergence rate per iteration. We feel that in certain applications of sensor networks, where the energy constraint is much more important than the time constraint, the communication complexity is more suitable, since the energy cost of the consensus algorithm is proportional to the number of communications.  
We wish to underline that for different types of algorithms, usually the time required for one iteration is not the same. Hence, in some situations, the comparison of convergence rate per iteration is not so meaningful since it cannot be translated to real-time performance. An extreme example is that all the communication is conducted through a single channel. In that case, in order to avoid collision, the time for one iteration is roughly propotional to the number of broadcasts at each iteration. 
  
Furthermore, for the second stopping strategy, convergence rate per iteration is not so meaningful to determine the performance of the algorithm, as the algorithm will not stop after a fixed number of iterations. 
\end{remark}
We will now characterize $\Omega_i, \omega_i$ for both deterministic average consensus and gossip algorithm, via the following theorems.
\begin{theorem}
  \label{theorem:complexitydet}
  For the deterministic algorithm, the communication complexity is given by
  \begin{equation}
    \Omega_1(P) =\Omega_2(P) = -\frac{N\bar d(P)}{2\max_{i=2,\ldots,N} \log(|\lambda_i(P)|)}.
  \end{equation}
\end{theorem}

For a stochastic algorithm, computing complexities $\omega_1, \omega_2$ is a more complicated affair. Before giving an explicit expression of $\omega_1$ and $\omega_2$, let us first define the following quantities:
\begin{definition}
  \begin{enumerate}
    \item The projection matrix $\mathcal P$ is defined as
      \begin{equation}
	\label{eq:lyapunoveq}
	\mathcal P \triangleq I - \mathbf 1 \mathbf 1'/N.	
      \end{equation}
    \item Matrix $\mathcal W_{ij}$ is defined as
      \begin{equation}
	\mathcal W_{ij} \triangleq \mathcal P W_{ij} \mathcal P.	
      \end{equation}
    \item The linear operator $\mathcal A_P$ from $\mathbb R^{N\times N}$ to $\mathbb R^{N\times N}$ is defined as
      \begin{equation}
	\mathcal A_P(X) \triangleq \sum_{i,j} P_{ij}\mathcal W_{ij} X\mathcal W_{ij}.
      \end{equation}
      The spectral radius of the above operator is defined as $\rho(P)$.
    \item $\gamma$ is defined as
   \begin{equation}
     \gamma(P) \triangleq \inf_n \frac{1}{n}\mathbb E\log\|\prod_{k=1}^n \mathcal P W_k\mathcal P\|_2.
   \end{equation}
  \end{enumerate}
\end{definition}
We can now prove the main theorem on the communication complexity of gossip:
\begin{theorem}
  \label{theorem:rategossip}
  For a gossip algorithm, if $P_{ii} = 0$, for all $i$, then the following equalities hold: 
  \begin{align}
    \omega_1(P) &= -\frac{2}{\log(\rho(P))},\label{eq:eqcomplexityone}\\
    \omega_2(P) &= -\frac{1}{\gamma(P)}\label{eq:eqcomplexitytwo}.
  \end{align}
  Moreover, the following inequalities hold:
  \begin{align}
    -\frac{1}{\log(\lambda_2(W))}&\leq \omega_1(P) \leq -\frac{2}{\log(\lambda_2(W))},\label{eq:ineqcomplexityone}\\
    \omega_2(P) &\leq \omega_1(P).\label{eq:ineqcomplexitytwo}
  \end{align}
\end{theorem}
The proof is reported in the appendix for the sake of legibility. The following corollary address the case of non-zero diagonal elements:
\begin{corollary}
  \label{corollary:gossip}
  Suppose $P$ is a gossip matrix. Define the matrix $\tilde P$ to be
  \begin{displaymath}
    \tilde P = \frac{1}{\sum_{i\neq j}P_{ij}} [P - diag(P)],
  \end{displaymath}
 where $diag(P)$ is a diagonal matrix with same diagonal elements as $P$. Then $\tilde P$ is a gossip matrix and 
 \begin{equation}
   \omega_1(P) = -\frac{2\sum_{i\neq j}P_{ij}}{\log(\rho(P))},\,\omega_2(P) = \omega_2(\tilde P).
 \end{equation}
 Moreover inequalities \eqref{eq:ineqcomplexityone} and \eqref{eq:ineqcomplexitytwo} still hold.
%  \begin{equation}
%    \begin{split}
%    -\frac{1}{\log(\lambda_2(W))}&\leq \omega_1(P) \leq -\frac{2}{\log(\lambda_2(W))},\\
%    \omega_2(P) &\leq \omega_1(P).
%    \end{split}
%  \end{equation}
\end{corollary}
\begin{pf}
The proof is similar to the proof of Theorem~\ref{theorem:rategossip}.
\end{pf}
\begin{remark}
  For gossip algorithms, the second stopping criterion yields a lower communication complexity and hence is more energy efficient since $\omega_2(P) \leq \omega_1(P)$. 
\end{remark}
