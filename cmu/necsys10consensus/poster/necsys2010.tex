\documentclass[final,t]{beamer}
\mode<presentation> {\usetheme{cmu}}
% additional settings
\setbeamerfont{itemize}{size=\normalsize}
\setbeamerfont{itemize/enumerate body}{size=\normalsize}
\setbeamerfont{itemize/enumerate subbody}{size=\normalsize}

% additional packages
\usepackage{times}
\usepackage{amsmath,amsthm, amssymb, latexsym}
\usepackage{exscale}
\usepackage{booktabs, array}
\usepackage[english]{babel}
\usepackage[latin1]{inputenc}
\usepackage[orientation=portrait,size=a0,scale=1.3]{beamerposter}
\listfiles

\title{\huge Communication Complexity and Energy Efficient Consensus Algorithm}
\author[Mo et al.]{\large Yilin Mo, Bruno Sinopoli}
\institute[Carnegie Mellon]{\large Dept of Electrical and Computer Engineering, Carnegie Mellon University}
\date[Sept. 14, 2010]{Sept. 14, 2010}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{document}
\begin{frame}{} 
  \begin{columns}[t]
    \begin{column}{.48\linewidth}
      \vskip-2ex
      \begin{block}{Introduction}
	\begin{itemize}
	  \item Average consensus problems have been extensively studied by many researchers over the past few years.
	  \item Usually the consensus algorithms are designed to achieve the fastest convergence rate per iteration. \alert{But is it a good metric?}
	  \item The time needed for one iteration of different algorithms may not be the same.
	  \item For some application in wireless sensor networks, \alert {the energy constraints} are much more important than the real-time requirement.
	  \item \alert{Need to develop a new metric to judge the energy efficiency.}
	\end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \vskip1ex
      \begin{block}{Deterministic and Gossip Algorithm}
	\begin{itemize}
	  \item Consensus algorithms can be usually classified into two categories: deterministic or stochastic. 
	  \item We will consider both deterministic algorithms and gossip algorithms.
	  \item We model the network as a connected undirected graph $G = \{V,\,E\}$.
	  \item Deterministic Algorithm:
	    \begin{itemize}
	      \item Update Equation:
		\begin{equation}
		  x_{k+1} = P x_k.
		  \label{eq:matrixupdate}
		\end{equation}
	      \item If $P$ satisfies the following conditions, then average consensus will be achieved:
		\begin{enumerate}
		  \item $ \lambda_1(P) = 1$ and  $|\lambda_i(P)| < 1$ for all $i = 2,\ldots, N$.
		  \item $P\mathbf 1 = \mathbf 1$, i.e. $\mathbf 1$ is an eigenvector of $P$.
		\end{enumerate}
	      \item Moreover we assume $P$ is symmetric and non-negative.
	      \item The average number of communication per node for each iteration is defined as: 
		\begin{equation}
		  \bar d(P) \triangleq \sum_{i\neq j} \mathbb I_{\{P_{ij}\neq 0\}}/N.
		  \label{eq:avgdeg}
		\end{equation}
	    \end{itemize}
	  \item Gossip Algorithm:
	    \begin{itemize}
	      \item For each iteration, a pair of node $(i,j)$ is selected with probability $Q_ij$.
	      \item The pair exchange information and update their states to the average of the two.
	      \item Define
		\begin{equation}
		  W_{ij} =  I - (\mathbf e_i - \mathbf e_j)(\mathbf e_i-\mathbf e_j)'/2 .
		\end{equation}
		where $\mathbf e_i\in\mathbb R^{N}$ is a vectors of all zeros with only the $i$th element equal to $1$.
	      \item The update equation:
		\begin{equation}
		  x_{k+1} = W_k x_k, 
		  \label{eq:stochasticupdate}
		\end{equation}
		where $W_k$ is a random matrix and the probability that $W_k$ equals $W_{ij}$ is $P_{ij}$. 
	      \item We assume $Q$ satisfies:
		\begin{enumerate}
		  \item $\mathbf 1' Q\mathbf 1 = 1.$
		  \item $Q$ is symmetric and non-negative.
		\end{enumerate}
	    \end{itemize}
	  \item  The accuracy of consensus algorithm at $k$th step is defined as:
	    \begin{equation}
	      \varepsilon_k \triangleq \sup_{y_0\neq 0} y_k'y_k/(y_0'y_0).
	    \end{equation}
	\end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


      \vskip1ex
      \begin{block}{Communication Complexity}
	\begin{itemize}
	  \item The communication complexity measures the average number of communications needed to reach certain accuracy. 
	  \item Consider the accuracy $\varepsilon > 0$, stopping time $T_{\varepsilon}$ is defined as
	    \begin{equation}
	      T_{\varepsilon} \triangleq \inf\{k:\mathbb E\varepsilon_k \leq \varepsilon \}.
	    \end{equation}
	  \item Define $c_k$ to be the number of communications incurred at the $k^{th}$ iteration.
	  \item The communication complexity is defined as:
	    \begin{equation}
	      \Omega = \omega = \limsup_{\varepsilon\rightarrow0^+}-\frac{\mathbb E\sum_{k=0}^{T_{\varepsilon}}c_k}{\log(\varepsilon)}. 
	      \label{eq:communicationcomplexity}
	    \end{equation}
	  \item To clarify, we will write the complexity of deterministic algorithms as $\Omega$ and gossip as $\omega$.
	  \item The goal: Find the consensus algorithm with \alert{least communication complexity}.
	\end{itemize}
      \end{block}

      \vskip1ex
\begin{block}{Complexity of Deterministic Algorithms}
	\begin{itemize}
	  \item For the deterministic algorithm, $\Omega$ is given by
	    \begin{equation}
	      \Omega(P) = = -\frac{N\bar d(P)}{2\max_{i=2,\ldots,N} \log(|\lambda_i(P)|)}.
	    \end{equation}
	  \item \alert{$\Omega(P)$ is hard to minimize since it is in fractional form and contains $\bar d(P)$. }
	\end{itemize}
      \end{block}

    \end{column}

    \begin{column}{.48\linewidth}
      \vskip-2ex
      
      \begin{block}{Complexity of Gossip Algorithms}
	\begin{itemize}
	  \item The projection matrix $\mathcal P$ is defined as
	    \begin{equation}
	      \label{eq:lyapunoveq}
	      \mathcal P \triangleq I - \mathbf 1 \mathbf 1'/N.	
	    \end{equation}
	  \item Matrix $\mathcal W_{ij}$ is defined as
	    \begin{equation}
	      \mathcal W_{ij} \triangleq \mathcal P W_{ij} \mathcal P.	
	    \end{equation}
	  \item The linear operator $\mathcal A_Q$ from $\mathbb R^{N\times N}$ to $\mathbb R^{N\times N}$ is defined as
	    \begin{equation}
	      \mathcal A_Q(X) \triangleq \sum_{i,j} Q_{ij}\mathcal W_{ij} X\mathcal W_{ij}.
	    \end{equation}
	    The spectral radius of the above operator is defined as $\rho(Q)$.
	  \item For the gossip algorithm, $\omega$ is given by
	    \begin{equation}
	      \omega(Q) = -\frac{2\sum_{i\neq j}Q_{ij}}{\log(\rho(Q))}.
	    \end{equation}
	  \item $\omega(Q)$ is still in fractional form. However we can prove the following inequality
	    \begin{equation}
	    \omega(Q) \geq \omega(\tilde Q),  
	    \end{equation}
	    where $\tilde Q$ is defined as  
	    \begin{equation}
	      \tilde Q = \frac{1}{\sum_{i\neq j}Q_{ij}} [Q - diag(Q)].
	    \end{equation}
	    In other word, removing the null operation can reduce communication complexity.
	  \item \alert{Optimizing $\omega(Q)$ is equivalent to solving the following problem:
	    \begin{align*}
	      &\mathop{\textrm{minimize}}\limits_{Q\in\mathcal S}&
	      & \rho(Q)\\
	      &\textrm{subject to}&
	      &\mathbf 1'Q\mathbf 1 = 1,\,Q_{ii} = 0,
	    \end{align*}
	    which is convex and can be solved efficiently.}
	\end{itemize}
      \end{block}

      \begin{block}{Comparison between Deterministic and Gossip Algorithm}
	\begin{itemize}
	  \item Finding the optimal gossip algorithm is easy while finding the optimal deterministic algorithm is hard.
	  \item Can we compare the energy efficiency of these two algorithms?
	  \item There exists a natural mapping between deterministic and gossip algorithm. 
	    \begin{align*}
	      f:&P\rightarrow Q\\
	      &P\mapsto P/N.
	    \end{align*}
	  \item The following condition is sufficient for $\Omega(P) \geq \omega(P/N)$
	    \begin{equation}
	      \label{eq:fundamentalcondition}
	      \lambda_2(P) \geq \frac{16}{\bar d(P)^2}.
	    \end{equation}
	  \item In general inequality \eqref{eq:fundamentalcondition} is true for a large class of networks. The main reason is that the condition does not depend on the size of the graph $N$. 
	  \item \alert{For lots of graphs, the deterministic algorithm will be less energy efficient than the gossip algorithm.}
	\end{itemize}
      \end{block}

      \begin{block}{Illustrative Examples}
	\begin{columns}
	  \begin{column}{0.48\linewidth}
	  \begin{figure}[htpb]
	    \begin{center}
	      \includegraphics{figure1.eps}
	    \end{center}
	    \caption{$\Omega(P)$ v.s. $\omega(P/N)$}
	    \label{fig:complexity1}
	  \end{figure}
	  \end{column}

	  \begin{column}{0.48\linewidth}
	  \begin{itemize}
	    \item  We use a random generated connected graph of $10$ vertices and $50$ edges. 
	    \item Consider a consensus matrix $P$ of the following form:
	      \begin{displaymath}
		P = I - \alpha L,	
	      \end{displaymath}
	      where $L$ is the Laplacian matrix of the graph with eigenvalues $\lambda_1(L)\geq \ldots\geq \lambda_{N-1}(L)>\lambda_N(L) = 0$ and
	      \[\alpha = 2/(\lambda_{1}(L) + \lambda_{N-1}(L)).\]
	    \item Figures~\ref{fig:complexity1} compares the communication complexity $\Omega(P)$ and $\omega(P/N)$ of $100$ randomly generated graphs. 
	  \end{itemize}
	  \end{column}
	\end{columns}
      \end{block}
      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      \begin{block}{Conclusion}
	\begin{itemize}
	  \item A new energy metric for consensus algorithms is defined and explicit formulas are provided to compute the communication complexity for both deterministic and gossip algorithms.
	  \item The optimal gossip algorithm with minimum communication complexity is formulated as a convex optimization problem.
	  \item A comparison between the complexity of deterministic and gossip algorithms are also provided.
	\end{itemize}
      \end{block}

      %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

      \end{column}
    \end{columns}
  \end{frame}

  \end{document}
