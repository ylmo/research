\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Communication Complexity]{Communication Complexity and Energy Efficient Consensus Algorithm}
\author[Yilin Mo]{Yilin Mo and Bruno Sinopoli}
\institute[Carnegie Mellon University]{Department of Electrical and Computer Engineering,\\ Carnegie Mellon University}
\date[Necsys 2010]{Necsys'10}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}{Introduction}
	\begin{itemize}
		\item Average consensus problems have been extensively studied by many researchers over the past few years.
		\item Usually the consensus algorithms are designed to achieve the fastest convergence rate per iteration. \alert{But is it a good metric?}
			\pause
		\item The time needed for one iteration of different algorithms may not be the same.
		\item For some application in wireless sensor networks, \alert {the energy constraints} are much more important than the real-time requirement.
			\begin{table}[]
				\centering
				\begin{tabular}{cccc}
					\hline
					& Convergence Rate & \#Communications & Average time\\
					\hline 
					1& 0.25&10& 1s\\
					2& 0.5& 4 & 0.5s\\
					\hline
				\end{tabular}
			\end{table}
			\pause
		\item We would like to propose a new metric to assess the energy efficiency. 
	\end{itemize}
\end{frame}

\begin{frame}{Preliminary: Graph Theory}
	\begin{itemize}
		\item We model the network as a connected undirected graph $G = \{V,\,E\}$.
		\item $V = \{1,2,\ldots,N\}$ is the set of vertices.
		\item $E \subset V\times V$ is the set of edges.
		\item $(i,j)\in E$ if and only if $i$ and $j$ can communicate directly with each other.
		\item The neighborhood of sensor $i$ is defined as
			\begin{displaymath}
				\mathcal N(i) \triangleq \{j\in V:(i,j)\in E\}. 
			\end{displaymath}
	\end{itemize}
\end{frame}

\begin{frame}{Preliminary: Deterministic Consensus Algorithm}
	\begin{itemize}
		\item Each agent starts with an initial value $x_{0,i}$.
		\item Update equation for agent $i$:
			\begin{displaymath}
				x_{k+1,i} = P_{ii} x_{k,i} + \sum_{j\in \mathcal N(i)}x_{k,j}	
			\end{displaymath}
		\item Let $P\triangleq [P_{ij}]\in \mathbb R^{N\times N}$, $x_k \triangleq [x_{k,1},\ldots,x_{k,N}]'$.
		\item Update equation:
			\begin{displaymath}
				x_{k+1} = Px_k.	
			\end{displaymath}
	\end{itemize}
\end{frame}


\begin{frame}{Preliminary: Deterministic Consensus Algorithm}
	\begin{itemize}
		\item  Define
			\begin{displaymath}
				x_{ave} \triangleq \frac{\mathbf 1'x_0}{N}\mathbf 1.	
			\end{displaymath}
		\item $x_k$ converges to $x_{ave}$ if the following holds:
			\begin{itemize}
				\item $\lambda_1(P) = 1$ and $|\lambda_i(P)|<1$ for $i=2,\ldots,N$.
				\item $P\mathbf 1 = \mathbf 1$.
			\end{itemize}
		\item In addition we assume that $P$ is symmetric and non-negative, i.e. $P\in \mathcal S$, where $\mathcal S$ is defined as
			\begin{displaymath}
				\mathcal S \triangleq \{P\in \mathbb R^{N\times N}:P=P',\,P_{ij}\geq 0,\,P_{ij}=0\textrm{ if $i\neq j$ and $(i,j)\notin E$}\}.		
			\end{displaymath}
		\item The average number of communication per node is defined as
			\begin{displaymath}
				\bar d(P) \triangleq \frac{1}{N} \sum_{i\neq j}\mathbb I_{\{P_{ij}\neq 0\}}	
			\end{displaymath}
	\end{itemize}
\end{frame}

\begin{frame}{Preliminary: Gossip Algorithm}
	\begin{itemize}
		\item Let $Q \in \mathcal S$ and $1'Q1 = 1$.
		\item For every iteration, one pair of nodes $(i,j)$ are selected with probability $Q_{i,j}$.
		\item Both of them update the state to
			\begin{displaymath}
				x_{k+1,i}=x_{k+1,j} = (x_{k,i}+x_{k,j})/2.	
			\end{displaymath}
		\item Define $W_{ij}$ to be
			\begin{displaymath}
				W_{ij} \triangleq I - (e_i-e_j)(e_i-e_j)'/2.	
			\end{displaymath}
		\item Update equation:
			\begin{displaymath}
				x_{k+1} = W_k x_k,	
			\end{displaymath}
			where $W_k = W_{ij}$ with probability $Q_{ij}$.
	\end{itemize}
\end{frame}

\begin{frame}{Communication Complexity}
	\begin{itemize}
		\item The error vector $y_k$ is defined as
			\begin{displaymath}
				y_k \triangleq x_k -x_{ave}.	
			\end{displaymath}
		\item The accuracy of consensus algorithm at $k$ step is defined as
			\begin{displaymath}
				\varepsilon_k \triangleq \sup_{y_0\neq 0}(y_k'y_k)/(y_0'y_0).
			\end{displaymath}
		\item Stopping time $T_\varepsilon$ is defined as
			\begin{displaymath}
				T_{\varepsilon} \triangleq \inf\{k:\mathbb E\varepsilon_k \leq \varepsilon\}.	
			\end{displaymath}
		\item Communication complexity is defined as
			\begin{displaymath}
				\Omega  = \omega = \limsup_{\varepsilon\rightarrow 0^+}-\frac{\mathbb E\sum_{k=0}^{T_\varepsilon}c_k}{\log(\varepsilon)},
			\end{displaymath}
			where $c_k$ is the number of communication incurred at time $k$.
	\end{itemize}
\end{frame}

\begin{frame}{Communication Complexity}
	\begin{itemize}
		\item We will use $\Omega$ and $\omega$ for deterministic and gossip algorithms respectively.
		\item Communication complexity measures how many communications are needed in order to reach certain accuracy.
		\item Given an algorithm, how to compute the communication complexity?
		\item How to design the optimal algorithm?
	\end{itemize}
\end{frame}

\begin{frame}{Communication Complexity for Deterministic Algorithm}
	\begin{theorem}
		\label{theorem:complexitydet}
		For the deterministic algorithm, the communication complexity is given by
		\begin{equation}
			\Omega(P)  = -\frac{N\bar d(P)}{2\max_{i=2,\ldots,N} \log(|\lambda_i(P)|)}.
		\end{equation}
	\end{theorem}
	\pause
	$\Omega(P)$ is very hard to optimize since:
	\begin{itemize}
		\item It is in fractional form.
		\item $\bar d(P)$ is not a convex function (not even continuous).
	\end{itemize}
\end{frame}


\begin{frame}{Communication Complexity for Gossip Algorithm}
  \begin{itemize}
    \item The projection matrix $\mathcal P$ is defined as
      \begin{equation}
	\label{eq:lyapunoveq}
	\mathcal P \triangleq I - \mathbf 1 \mathbf 1'/N.	
      \end{equation}
    \item Matrix $\mathcal W_{ij}$ is defined as
      \begin{equation}
	\mathcal W_{ij} \triangleq \mathcal P W_{ij} \mathcal P.	
      \end{equation}
    \item The linear operator $\mathcal A_Q$ from $\mathbb R^{N\times N}$ to $\mathbb R^{N\times N}$ is defined as
      \begin{equation}
	\mathcal A_Q(X) \triangleq \sum_{i,j} Q_{ij}\mathcal W_{ij} X\mathcal W_{ij}.
      \end{equation}
      The spectral radius of the above operator is defined as $\rho(Q)$.
      \item The matrix $W$ is defined as
	      \begin{displaymath}
		     W = \mathbb EW_k =  I-D+Q,
	      \end{displaymath}
	      where $D = diag(D_1,\ldots,D_N)$ is a diagonal matrix and $D_i = \sum_j Q_{ij}$. 
  \end{itemize}

\end{frame}

\begin{frame}{Communication Complexity for Gossip Algorithm}
	\begin{theorem}
		\label{theorem:rategossip}
		For a gossip algorithm, the following equality holds: 
		\begin{align}
			\omega(Q) = -\frac{2\sum_{i\neq j}Q_{ij}}{\log(\rho(Q))}.
		\end{align}
		Moreover, the following inequalities hold:
		\begin{align}
			-\frac{1}{\log(\lambda_2(W))}&\leq \omega(Q) \leq -\frac{2}{\log(\lambda_2(W))},\label{eq:ineqcomplexityone}\\
		\end{align}
	\end{theorem}
	$\omega(Q)$ is still in the fractional form. We need to do some manipulations in order to make the problem convex.
\end{frame}

\begin{frame}{Communication Complexity for Gossip Algorithm}
	\begin{itemize}
		\item First we want to prove that $\rho(Q)$ is convex.
		\item Some results:
			\begin{itemize}
				\item The spectral radius of a positive semidefinite matrix is a convex function.
				\item If $f(x)$ is convex, then $f(\mathbf\alpha' \mathbf x)$ is also convex with respect to $x$.
			\end{itemize}
		\item Recall that
      \begin{displaymath}
	\mathcal A_Q(X) \triangleq \sum_{i,j} Q_{ij}\mathcal W_{ij} X\mathcal W_{ij}.
      \end{displaymath}
      \item We only need to prove that $\mathcal A_{ij}(X)=\mathcal W_{ij}X\mathcal W_{ij}$ is a positive semidefinite operator.
	\end{itemize}
\end{frame}


\begin{frame}{Communication Complexity for Gossip Algorithm}
	\begin{itemize}
		\item Instead of using the matrix form of the operator, we will use inner product.
		\item The inner product of two matrix $X$ and $Y$ is defined as
			\begin{displaymath}
				<X,Y> \triangleq \sum_{i,j}X_{ij}Y_{ij} = trace(XY')=trace(X'Y). 
			\end{displaymath}
		\item An operator $\mathcal A$ is positive semidefinite if and only if
			\begin{displaymath}
				<X,\mathcal AY> = <\mathcal AX,Y>,\,<X,\mathcal AX>\geq 0,\forall X,Y. 
			\end{displaymath}
			\begin{displaymath}
				\begin{split}
			&<\mathcal A_{ij}X,Y> = trace(\mathcal W_{ij}X\mathcal W_{ij}Y')	=trace(X\mathcal W_{ij}Y'\mathcal W_{ij})\\
				&=trace[X(\mathcal W_{ij}Y\mathcal W_{ij})'] = <X,\mathcal A_{ij}Y>.
				\end{split}
			\end{displaymath}
			\begin{displaymath}
				\begin{split}
			&<\mathcal A_{ij}X,X> = trace(\mathcal W_{ij}X\mathcal W_{ij}X')	=trace( \mathcal W_{ij}\mathcal W_{ij}X\mathcal W_{ij} \mathcal W_{ij}X')\\
				&=trace[\mathcal W_{ij}X\mathcal W_{ij}(\mathcal W_{ij}X\mathcal W_{ij})']=	<\mathcal A_{ij}X,\mathcal A_{ij}X>\geq 0.
				\end{split}
			\end{displaymath}
		\item $\rho(Q)$ is convex.
	\end{itemize}
\end{frame}

\begin{frame}{Communication Complexity for Gossip Algorithm}
	\begin{itemize}
		\item  Define $\tilde Q$ as
			\begin{displaymath}
				\tilde Q \triangleq \frac{1}{\alpha}[Q -diag(Q)],	
			\end{displaymath}
			where $\alpha = \sum_{i\neq j}Q_{ij}$.
		\item $\tilde Q$ removes the null operations from $Q$ (pair $(i,i)$ are selected).
		\item We can prove that
			\begin{displaymath}
				\omega(Q) = -\frac{2\alpha}{\log(\rho(Q))} = \frac{2\alpha}{\log(1-\alpha(1-\rho(\tilde Q)))}\geq -\frac{2}{\log(\rho(\tilde Q)}=\omega(\tilde Q),	
			\end{displaymath}
			which is actually equivalent to
			\begin{displaymath}
				1-\rho(\tilde Q)^{\alpha}\geq \alpha(1-\rho(\tilde Q)).
			\end{displaymath}
		\item As a result, we only need to consider those $Q$s with $0$ on the diagonal. Therefore we can simplified $\omega(Q)$ to
			\begin{displaymath}
				\omega(Q) = -\frac{2\sum_{i\neq j}Q_{ij}}{\log(\rho(Q))} = \frac{2}{\log(\rho(Q))},\textrm{ if $Q_{ii} = 0$}.	
			\end{displaymath}
	\end{itemize}
\end{frame}

\begin{frame}{Communication Complexity for Gossip Algorithm}
To find the optimal gossip algorithm, we only need to solve the following problem:
	\begin{align*}
		&\mathop{\textrm{minimize}}\limits_{Q\in\mathcal S}&
		& \rho(Q)\\
		&\textrm{subject to}&
		&\mathbf 1'Q\mathbf 1 = 1,\,Q_{ii} = 0.
	\end{align*}
\end{frame}

\begin{frame}{Comparison between Deterministic and Gossip Algorithm}
	\begin{itemize}
		\item Finding the optimal gossip algorithm is easy while finding the optimal deterministic one is hard.
		\item If we can implement both of them, which one is more energy efficient?
	  \item There exists a natural mapping between deterministic and gossip algorithm. 
	    \begin{align*}
	      f:&P\rightarrow Q\\
	      &P\mapsto P/N.
	    \end{align*}
	\end{itemize}
\end{frame}

\begin{frame}{Comparison between Deterministic and Gossip Algorithm}
	\begin{theorem}
		The following condition is sufficient for $\omega(P/N) \leq \Omega(P)$
		\begin{equation}
			\label{eq:fundamentalcondition}
			\lambda_2(P) \geq \frac{16}{\bar d(P)^2}.
		\end{equation}
	\end{theorem}
	\begin{proof}[Sketch of the Proof]

  \begin{displaymath}
	  \begin{split}
		  \omega(Q)& \leq -\frac{2}{\log(\lambda_2(W))}\rightarrow -\frac{2}{\log(\lambda_2(W))}\leq -\frac{N\bar d(P)}{2\max_{i=2,N} \log(|\lambda_i(P)|)}.\\
    W& = I-D+Q =\frac{N-1}{N}I + \frac{P}{N} \rightarrow \lambda_2(W) = \frac{N-1}{N} + \frac{\lambda_2(P)}{N}.
	  \end{split}
  \end{displaymath}
	\end{proof}
\end{frame}

\begin{frame}{Comparison between Deterministic and Gossip Algorithm}
	\begin{itemize}
		\item The condition does not depend on $N$.
		\item For lots of algorithms, $\lambda_2(P)\rightarrow 1$ when $N\rightarrow \infty$. (Slow convergence rate when number of nodes is large.)
		\item Such a condition holds for lots of graphs.
		\item It is safe to consider only gossip algorithms.
	\end{itemize}
\end{frame}

\begin{frame}{Illustrative Examples}
	\begin{itemize}
		\item We randomly generate connected graph of $N$ vertices and $N\times d$ edges. 
		\item Consensus matrix $P$ is chosen to be of the following form:
			\begin{displaymath}
				P = I - \alpha L,	
			\end{displaymath}
			where $L$ is the Laplacian matrix of the graph and $\alpha = 2/(\lambda_{1}(L) + \lambda_{N-1}(L))$.
	\end{itemize}
	\begin{figure}
		\centering
		\begin{minipage}[t]{0.3\linewidth}
			\centering
			\includegraphics[width=\linewidth]{figure1.eps}
			\caption{$N =10,d=5$}
		\end{minipage}
		\begin{minipage}[t]{0.3\linewidth}
			\centering
			\includegraphics[width=\linewidth]{figure2.eps}
			\caption{$N =20,d=5$}
		\end{minipage}
		\begin{minipage}[t]{0.3\linewidth}
			\centering
			\includegraphics[width=\linewidth]{figure3.eps}
			\caption{$N =20,d=10$}
		\end{minipage}
	\end{figure}
\end{frame}

\begin{frame}{Conclusion}
	\begin{itemize}
	  \item A new energy metric for consensus algorithms is defined and explicit formulas are provided to compute the communication complexity for both deterministic and gossip algorithms.
	  \item The optimal gossip algorithm with minimum communication complexity is formulated as a convex optimization problem.
	  \item A comparison between the complexity of deterministic and gossip algorithms are also provided.
	\end{itemize}
\end{frame}

\begin{frame}
	\begin{center}
		\Huge{ Thank you for your time!!\\
		Questions?}
	\end{center}
\end{frame}

\end{document}

