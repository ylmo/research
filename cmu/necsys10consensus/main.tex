This section is devoted to developing an efficient algorithm to solve Problem~\ref{opt_dyn}. In the first two subsections, we will reformulate the problem by manipulating and relaxing  both $P_k(V)$ and $\mathcal E(E_T)$ to make them explicit and convex functions of $V$. In the last subsection, we will present an algorithm that improves the quality of our solution recursively solving the relaxed formulation of Problem~\ref{opt_dyn}.
\subsection{Manipulation of $P_k(V)$}
This subsection is devoted to finding an explicit form of $P_k(V)$.\\
Let us indicate with $\gamma_{i},\;i=1,\ldots,m$ the binary variable such that $\gamma_i  = 1$ if and only if $s_i \in V$. By the definition of $\gamma_{i}$, the fusion center will receive at time $k$ the readings $\tilde y_k=[\gamma_{1}y_{k,1},\ldots,\gamma_{m}y_{k,m}]^T$. In order to find the relationship between $P_k$ and $\gamma_{i}$s, we need to define the following quantities:
\begin{align}
  \Gamma &\triangleq diag(\gamma_1,\ldots,\gamma_{m}) &\in& \mathbb R^{m\times m},\\
  \vec \gamma &\triangleq [\gamma_1,\ldots,\gamma_{m}]^T &\in& \mathbb R^{m},\\
  \hat x^*_{k} &\triangleq E\left(x_k|y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0} \right)&\in&\mathbb R^{n},\\
  P^*_{k} &\triangleq Cov\left(x_k|y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0}\right)&\in&\mathbb R^{n\times n},\\
  \hat x_{k} &\triangleq E\left(x_k|\Gamma y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0}\right)&\in&\mathbb R^{n},\\
  P_{k} &\triangleq Cov\left(x_k|\Gamma y_k \,,\; \tilde y_{k-1}\,, \ldots, \tilde y_{1}\,,\; \tilde y_{0} \right)&\in&\mathbb R^{n\times n}.
  \label{eq:estimationerror}
\end{align}
Let us first consider the case that all the sensors are used at time $k$. The Kalman filter for the system takes the following form
\begin{align}\label{eq:Kalman_filter}
    \hat x_{k|k-1} &= A \hat x_{k-1|k-1} \nonumber\\
    P_{k|k-1}  &= A P_{k-1|k-1} A^T  + Q  \nonumber\\
    K_k^*        &= P_{k|k-1} C^T (CP_{k|k-1} C^T  + R)^{ - 1} \nonumber \\
    \hat x^*_{k} &= K_k^* (y_k -C\hat x_{k|k-1}) + \hat x_{k|k-1} \nonumber\\
    P^*_{k}      &= P_{k|k-1}  -  K^*_k CP_{k|k-1},
  \end{align}
  where $\hat x_{k}^*$ is the estimate of the state at time $k$ and $P_{k}^*$ is the estimation error covariance matrix.

Now, let us consider any linear filter with gain $K_k$ under the sensor scheduling scheme\footnote{Note that this filter is not necessarily optimal}. We denote the estimate produced by this filter as $\check x_{k}$. It is easy to show that
\begin{equation}
  \check x_{k} = K_k\Gamma (y_k - C\hat x_{k|k-1}) + \hat x_{k|k-1},
  \label{eq:kalmanestimationforselection}
\end{equation}
where $K_k$ is the linear gain under the sensor selection scheme $\Gamma$. In general, the optimal gain $K_k$ is different from $K^*_k$ as it depends on the sensor selection strategy $\Gamma$. By exploiting the optimality and linearity of the Kalman filter, the following theorem holds.
\begin{theorem}
  \label{theorem:covarianceafterscheduling}
  The estimation error covariance matrix $P_{k}$ verifies the following inequality
  \begin{equation}
    P_{k} \leq P_{k}^* + (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T,
  \end{equation}
  where $S_k = C(AP_{k-1}A^T+Q)C^T + R$, $\mathcal K_k = K_k\Gamma$ and $K_k$ is any gain. Furthermore, there exists a $\mathcal K_k$ such that the equation holds.
\end{theorem}
\begin{pf}
The error covariance matrix of a linear filter under sensor scheduling scheme is given by
\begin{equation} \label{cov_matrix}
  \begin{split}
    & Cov(\check x_{k} - x_k) = Cov(\check x_{k} - \hat x^*_k + \hat x^*_k - x_k)\\
    & = Cov [(K_k\Gamma - K_k^*) (y_k-C\hat x_{k|k-1})  +  \hat x^*_{k} - x_k ]\\
    & = Cov [(K_k\Gamma - K_k^*) (y_k-C\hat x_{k|k-1})]  + Cov( \hat x^*_{k} - x_k )\\
    & = (K_k\Gamma - K_k^*) Cov(y_k-C\hat x_{k|k-1})(K_k\Gamma - K_k^*)^T  + P^*_{k}.\\
  \end{split}
\end{equation}
The decomposition of the covariance matrix in the third equation of \eqref{cov_matrix} is possible because, by the optimality of Kalman filter,
$x_k - \hat x_{k}^*$ is orthogonal to $y_k-C\hat x_{k|k-1}$. \\
We also know that
\begin{align}
  y_k - C\hat x_{k|k-1} &= C(Ax_{k-1} + w_{k-1}) + v_k - CA\hat x_{k-1}\nonumber\\
  &=CA(x_{k-1} - \hat x_{k-1}) + Cw_{k-1} + v_k\,, \label{innovation}
\end{align}
where the first term in \eqref{innovation} only depends on $w_0,\ldots,w_{k-2}$ $v_0,\ldots,v_{k-1}$. Thus, the three terms in the above equation are independent. As a result,
\begin{equation}
  Cov(y_k - C\hat x_{k|k-1}) = CAP_{k-1}A^TC^T + CQC^T + R = S_k\,.
\end{equation}
Since the Kalman filter is the optimal linear filter, we know
  \begin{displaymath}
    P_{k} \leq P_{k}^* + (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T,
  \end{displaymath}
  and there exists a $\mathcal K_k$ such that the equality holds.
\end{pf}
Applying the result of theorem~\ref{theorem:covarianceafterscheduling}, it is straightforward to see that the constraint $P_k \leq P_d$ can be rewritten as
\begin{equation}
    \exists \mathcal K_k, \; s.t.\;(\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d - P_{k}^*\,.
\end{equation}
\begin{remark}
Considering that $\mathcal K_k = K_k \Gamma$, if we denote the $j$th column of $\mathcal K_k$ as $\vec{\mathcal K}_{k,j}$, the following inequality holds
  \begin{equation}
    \gamma_{j} \geq \left\| \left\| \vec{\mathcal K}_{k,j}\right\|_1 \right\|_0,
  \end{equation}
  where, given a scalar $v\in \mathbb R$, $\left\| v\right\|_0$ is equal to $0$ if $v=0$, otherwise it is $1$.
\end{remark}
Combining all the previous results, we can conclude that the following problem is equivalent to Problem~\ref{opt_dyn}.
\begin{prob}\label{opt_dyn2}
\begin{align*}
  &\min_{T\in \mathcal T,\,\mathcal K_k}&&\mathcal E(E_T) \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0.
\end{align*}
\end{prob}
\begin{remark}
In Problem \ref{opt_dyn2}, the objective function still depends on the transmission tree $T$ while the constraints are in terms of the optimization variables $\mathcal K_k$ and $\vec \gamma$.
\end{remark}

\subsection{Manipulation of $\mathcal E(E_T)$}
This subsection is devoted to manipulating the objective function $\mathcal E(E)$ to make it an explicit function of $V$ and hence of the sensor selection vector $\vec \gamma$.\\
Given a set of vertices $V$, let us indicate the Steiner Tree of $V$ as $ST(V) = \{V_{ST}(V),\,E_{ST}(V)\}$.
\begin{remark}
 Note that the Steiner Tree $ST(V)$ is a function of $V$ and hence it is also a function of $\vec \gamma$.
 Thus, we can write $ST(V) = ST(\vec \gamma) = \{V_{ST}(\vec \gamma),\,E_{ST}(\vec \gamma)\}$.
\end{remark}

Since the Steiner Tree is the minimum cost tree that connects all the vertices in $V$, the following problem is equivalent to Problem~\ref{opt_dyn} and \ref{opt_dyn2}.

\begin{prob}[Energy minimization over $\vec \gamma$]\label{opt_dyn3}
\begin{align*}
  &\min_{\vec \gamma\,,\,\mathcal K_k}&&\mathcal E(E_{ST}(\vec \gamma)) \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0.
\end{align*}
\end{prob}

The objective function is now a function of the selected sensor set $V$. However, given a vertex set $V$, constructing $ST(V)$ is in general a NP-hard problem. As a result, to make this problem numerically solvable, we approximate $\mathcal E(E_{ST}(\vec \gamma))$ with a convex function of $\gamma$.\\
Let us indicate with $\overline{d}_i$, with $i=1,\dots m$, the cost of the shortest path from sensor $s_i$ to the fusion center $s_0$ and let us define the vector
$\vec{\overline{d}} = [\overline d_1,\ldots,\,\overline d_m]^T$ \footnote{$\overline d_i$ can be efficiently computed by Dijkstra's algorithm}.
%Let us also define $\underline{d}_i = \min_j\{cost(e_{i,j})|e_{i,j} \in E\}$ and $\vec{\underline{d}}= [\underline d_1,\ldots,\underline d_m]^T$.
%The following lemma provides an upper and lower bound for $\mathcal E(E_{ST}(\vec \gamma_k))$.
\begin{lemma} \label{lem:relaxation}
%\begin{lem}
  The following inequality for $\mathcal E(E_{ST}(\vec\gamma))$ holds
  \begin{equation}
   \mathcal E(E_{ST}(\vec\gamma)) \leq \vec{\overline{d}}^T\vec\gamma
    \label{eq:basicinequality}
  \end{equation}
\end{lemma}
\begin{pf}
%We will just give a hint of the proof.
%If we choose sensor $s_i$, then $s_i$ either directly connects to the fusion center or another sensor $s_j$. As a result, there must be a $e_{i,j} \in E_{ST}(V)$. Since $\underline d_{i}$ is the minimum cost of all edges connecting $s_i$, the lower bound holds.
It is sufficient to observe that the union of all the shortest paths connecting each sensor in $V$ to fusion center is a graph (with duplicated edges).
\end{pf}

The following problem will be a relaxed version of Problem~\ref{opt_dyn} and its solution, in terms of objective function, is an upper bound of the one of Problem~\ref{opt_dyn}.
%
%\begin{prob}[Lower bound for Problem~\ref{opt_dyn}]\label{opt_dynlower}
%\begin{align*}
%  &\min_{\vec \gamma \,,\,\mathcal K_k}&&\vec{\underline{d}}^T \vec \gamma \\
%	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d, \\
%        &&&\gamma_{i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0,
%\end{align*}
%\end{prob}
\begin{prob}[Upper bound for Problem~\ref{opt_dyn}]\label{opt_dynupper}
\begin{align*}
  &\min_{\vec \gamma \,,\,\mathcal K_k}&&\vec{\overline{d}}^T \vec \gamma \\
	& s.t.&& (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d- P_{k}^*, \\
        &&&\gamma_{i} \geq \left\| \left\| \vec{\mathcal K}_{k,i}\right\|_1 \right\|_0,
\end{align*}
\end{prob}
\subsection{Algorithm}

This subsection is devoted to describing the algorithm that, solving Problem \ref{opt_dynupper} iteratively, approximately solve Problem~\ref{opt_dyn}. The algorithm goes as follows:
\begin{algorithm}[H] \label{Alg}
  \caption{Algorithm for Approximately Solve Problem~\ref{opt_dyn}}
  %\algsetup{indent=2cm}
  \begin{algorithmic}[1]
 %   \STATE $V_{old} \Leftarrow S$
    \STATE $V_{new} \Leftarrow S$
    \REPEAT
    \STATE Let us define the graph $G_{old}=\{V_{old}\,,E_{old}\}$ where
            \begin{align*}
            V_{old}& \Leftarrow V_{new} \\
            E_{old}&=\{e_{ij}\in E \,:\,i,j \in V_{old} \}
            \end{align*}
    \STATE Solve Problem \ref{opt_dynupper} on subgraph $G_{old}$.
            Indicate with $V$ the solution of the minimization problem

    \STATE Evaluate an approximation of the Steiner tree $ST(V)$ generated by $V$ and set
    \[
     V_{new}=V_{ST}(V)
    \] \label{step3}
    \UNTIL {$\mathcal E(E_{ST}(V_{old}))== \mathcal E(E_{ST}(V))$}
    \RETURN $ST (V)$
  \end{algorithmic}
\end{algorithm}
\begin{algorithm}[H] 
  \caption{Algorithm for Approximately Solve Problem~\ref{opt_dyn}}
  %\algsetup{indent=2cm}
  \begin{algorithmic}[1]
 %   \STATE $V_{old} \Leftarrow S$
    \STATE $V_{new} \Leftarrow S$
    \REPEAT
    \STATE Let us define the graph $G_{old}=\{V_{old}\,,E_{old}\}$ where
            \begin{align*}
            V_{old}& \Leftarrow V_{new} \\
            E_{old}&=\{e_{ij}\in E \,:\,i,j \in V_{old} \}
            \end{align*}
    \STATE Solve Problem \ref{opt_dynupper} on subgraph $G_{old}$.
            Indicate with $V$ the solution of the minimization problem

    \STATE Evaluate an approximation of the Steiner tree $ST(V)$ generated by $V$ and set
    \[
     V_{new}=V_{ST}(V)
    \] 
    \UNTIL {$\mathcal E(E_{ST}(V_{old}))== \mathcal E(E_{ST}(V))$}
    \RETURN $ST (V)$
  \end{algorithmic}
\end{algorithm}

\begin{remark}
	The algorithm will certainly terminate since $\mathcal E(E_{ST}(V)) \leq \mathcal E(E_{ST}(V_{old}))$ and there are only finitely possible vertex sets $V$.
\end{remark}
\begin{remark}
Algorithm $1$ needs, in Step \ref{step3}, the evaluation of the Steiner tree. We will consider just an approximation of the Steiner tree given by the Matlab function \textit{sfo\_pspiel\_get\_cost.m} of the Matlab Toolbox \textit{SFO}.
\end{remark}
To solve Problem~\ref{opt_dynupper}, we use a reweighted $L_1$ approach to relax the zero norm constraint to and $L_1$ norm constraint \cite{boydreweight}. The details of this relaxation approach are described below:
\begin{enumerate}
\item Set the iteration count $l$ to zero and set the weights vector to $w_j^{1}=1$ for $j=1.... m$.
\item Solve the weighted $\textit{L}_1$ Problem
\begin{align*} \label{conv_opt}
	&&& \min_{\vec \gamma \,,\,\mathcal K_k} \vec d^T \vec \gamma \\
	& s.t.&&  (\mathcal K_k-K_k^*)S_k(\mathcal K_k-K_k^*)^T \leq P_d - P_{k}^*, \\
    &&&\gamma_{j}  \geq w_j^{l}  \|\vec {\mathcal K}_{k,j}\|_1.
\end{align*}
Indicate with $\gamma_1^l,\ldots,\gamma_{m}^l$ the solution of this problem. 
\item Update the weights
\[
w_j^{l+1}=\frac{1}{\gamma_{j}^l+\epsilon}\quad,j=1,\ldots,m,
\]
where $\epsilon$ is a small constant.
\item Terminate on convergence or when $l$ attains a specified maximum number of iterations $l_{max}$. Otherwise, increment $l$ and go to step 2.
\end{enumerate}
