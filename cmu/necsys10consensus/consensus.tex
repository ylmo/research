In this section we wish to briefly introduce the average consensus algorithm, the notation of which will be used later in the paper.

\subsection{Graph Theory}

We model a network composed of $N$ agents as a graph $G = \{V,\,E\}$. $V = \{1,2,\ldots,N\}$ is the set of vertices representing the agents. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if sensor $i$ and $j$ can communicate directly with each other. In this paper we will always assume that $G$ is undirected, i.e. $(i,j)\in E$ if and only if $(j,i)\in E$. The neighborhood of sensor $i$ is defined as
\begin{equation}
  \mathcal N(i) \triangleq \{j\in V:(i,j)\in E\}. 
\end{equation}
A graph is called connected if for any pair $i,j \in V$, there always exists a path that connects $i$ and $j$. In this paper we will always assume the graph is connected.
%The degree of sensor $i$ is defined as 
%\begin{equation}
% d_i \triangleq |\mathcal N(i)|. 
%\end{equation}
%Now we can define the Laplacian matrix $L$ of graph $G$ as
%\begin{equation}
%L \triangleq D - A,  
%\end{equation}
%where $D = diag(d_1,\ldots,d_n)$. It is well know that $L$ is a positive semidefinite matrix, with at least one eigenvalue located at origin. Let us arrange its eigenvalues in the increasing order\footnote{The eigenvalues of all the other matrices except the Laplacian matrix in this paper will be arranged in decreasing order.} as 
%\begin{equation}
% 0 = \lambda_1(L)\leq \lambda_2(L)\leq\cdots\leq \lambda_N(L).  
%\end{equation}
%If the graph $G$ is connected, then $\lambda_2(L) >0$ is strictly positive. In this paper we will always assume that $G$ is connected.% Let us further define
%\begin{equation}
%  d_{min} \triangleq \min_i d_i,\,d_{max}  \triangleq \max_i d_i,\,\bar d \triangleq \sum_i d_i/N.
%\end{equation}
%
%A graph is called $d$-regular graph if all the vertices have the same degree $d$,i.e. $d_{min} = d_{max} = d$. The following theorem characterize the relation between the degree $d_i$ and the eigenvalues of $L$.
%\begin{theorem}[Fiedler]
%\begin{equation}
%  \lambda_2(L) \leq \frac{N}{N-1} d_{min},\, \lambda_N(L) \geq \frac{N}{N-1} d_{max}. 
%\end{equation}
%\end{theorem}
%
%\begin{theorem}[Alon and Boppana]
%  Let $\{G_k\}$ be a sequence of $d$-regular graphs, where the size of vertex set $|V_k|\rightarrow \infty$ as $k\rightarrow \infty$, then
%  \begin{equation}
%    \begin{split}
%    \limsup_{k\rightarrow \infty}\lambda_2(L_k) &\leq k-\sqrt{2k-1},\\
%    \liminf_{k\rightarrow \infty}\lambda_{N_k}(L_k) &\geq k+\sqrt{2k-1}.
%    \end{split}
%  \end{equation}
%\end{theorem}

\subsection{Average Consensus Algorithm: Deterministic Case}
In this subsection we will briefly review the average consensus algorithm in the deterministic case. Suppose that each agent has an initial state $x_{0,i}$. At each iteration, sensor $i$ will communicate with all its neighbors and update its state according to the following update equation
\begin{equation}
  x_{k+1,i} = P_{ii} x_{k,i} + \sum_{j\in \mathcal N(i)} P_{ij} x_{k,j}.  
  \label{eq:singleupdate}
\end{equation}
Let us define the vector $x_k \triangleq [x_{k,1},\ldots,x_{k,N}]'\in \mathbb R^N$ and matrix $P \triangleq \left[P_{ij}\right] \in \mathbb R^{N\times N}$. Now we can rewrite \eqref{eq:singleupdate} in its matrix form as
\begin{equation}
  x_{k+1} = P x_k.  
  \label{eq:matrixupdate}
\end{equation}

We will call $P$ the consensus matrix. In the rest of the paper, we will always assume that $P$ is symmetric and each element $P_{ij}$ is non-negative.  Let us further define the set $\mathcal S \subset \mathbb R^{N\times N}$, such that
\begin{equation}
  \begin{split}
  \mathcal S \triangleq& \{P \in \mathbb R^{N\times N}: P = P',\, P_{ij} \geq 0,\\
  &\,P_{ij}  = 0\textrm{ if }i\neq j\textrm{ and }(i,j)\notin E\}.
  \end{split}
\end{equation}
It is easy to see that with the additional constraints of symmetry and non negativity, the consensus matrix $P\in \mathcal S$. Now let us define the average vector to be
\begin{equation}
  x_{ave} \triangleq \frac{\mathbf 1' x_0}{N} \mathbf 1,  
\end{equation}
where $\mathbf 1\in \mathbb R^N$ is a vector whose elements are all equal to $1$. Also let us define the error vector $y_k$ to be
\begin{equation}
  y_k \triangleq x_k - x_{ave} . 
\end{equation}
The goal of average consensus is to guarantee that $y_k\rightarrow 0$ as $k \rightarrow \infty$ through the update equation \eqref{eq:matrixupdate}. Let us arrange the eigenvalues of $P$ in decreasing order as $\lambda_1(P)\geq \lambda_2(P)\ldots\geq \lambda_N(P)$. It is well known that the following conditions are necessary and sufficient in order to achieve average consensus from any initial condition $x_0$:
\begin{enumerate}
  \item $ \lambda_1(P) = 1$ and  $|\lambda_i(P)| < 1$ for all $i = 2,\ldots, N$.
  \item $P\mathbf 1 = \mathbf 1$, i.e. $\mathbf 1$ is an eigenvector of $P$.
\end{enumerate}
Under the above conditions, $\lim_{k\rightarrow \infty} P^k = \mathbf 1' \mathbf 1/N $ and $\lim_{k\rightarrow \infty} y_k = 0$. 

For further analysis, let us define
\begin{equation}
  \bar d(P) \triangleq \sum_{i\neq j} \mathbb I_{\{P_{ij}\neq 0\}}/N,
  \label{eq:avgdeg}
\end{equation}
where $\mathbb I$ is the indicator function. Hence, $\bar d(P)$ is the average number of communications for each sensor in one iteration.

\subsection{Gossip Algorithm}
In the previous subsection, we discussed deterministic consensus algorithm. In this subsection we will briefly introduce the gossip algorithm, which can be seen as a particular type of stochastic algorithm. The readers can refer to \cite{boydgossip} for more information.

Consider the matrix $P \in \mathcal S$, which satisfies\footnote{The $P$ matrix defined here is different from the $P$ matrix in \cite{boydgossip}.}
\begin{displaymath}
 \mathbf 1'P\mathbf 1 = 1. 
\end{displaymath}
Such a matrix $P$\footnote{This is a slight abuse of notation as $P$ is also used for the deterministic case. The reader can easily disambiguate from the context} will be called the gossip matrix. Suppose that, at each time, a pair of agents $(i,j)$ is chosen with probability $P_{ij}$. By the constraints we placed on $P$, we know that $P_{ij}$ is a well defined probability distribution. If $(i,j)$ is chosen and $i\neq j$, then $i$ and $j$ will exchange information and update their states to $x_{k+1,i} = x_{k+1,j} = (x_{k,i}+x_{k,j})/2$. If $i = j$, then no communication is incurred and $x_{k+1} = x_k$. Define the matrix $W_{ij} \in \mathbb R^{N \times N}$ to be
\begin{equation}
  W_{ij} =  I - (\mathbf e_i - \mathbf e_j)(\mathbf e_i-\mathbf e_j)'/2 ,
\end{equation}
where $\mathbf e_i\in\mathbb R^{N}$ is a vectors of all zeros with only the $i$th element equal to $1$. The update equation at time $k$ can be written as
\begin{equation}
  x_{k+1} = W_k x_k, 
  \label{eq:stochasticupdate}
\end{equation}
where $W_k$ is a random matrix and the probability that $W_k$ equals $W_{ij}$ is $P_{ij}$. Define $W \triangleq \mathbb EW_k$. It can be shown that
\begin{equation}
  W = I - D + P,
\end{equation}
where $D = diag(D_1,\ldots,D_N)$ is a diagonal matrix and $D_i = \sum_j P_{ij}$. It is easy to see that $W$ is symmetric. Since $W = \sum_{i,j}P_{ij}W_{ij}$, each $W_{ij}$ is positive semidefinite and $W_{ij}\mathbf 1 = 1$, $W$ is positive semidefinite and $W\mathbf 1 = 1$. Let us arrange the eigenvalues of $W$ in decreasing order as $1 = \lambda_1(W) \geq \lambda_2(W)\geq\ldots\geq\lambda_N(W)\geq 0$. It can be shown that
\begin{equation}
  \mathbb E\left(y_{k+1}'y_{k+1}|y_k,\ldots,y_0\right) \leq \lambda_2(W) y_k'y_k. 
  \label{eq:martingale}
\end{equation}
Hence, if $\lambda_2(W) < 1$, then $y_k$ converges to $0$ in the mean square sense and average consensus is achieved. 
