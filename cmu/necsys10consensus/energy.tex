In this section, we will focus on the design of consensus algorithms with minimum communication complexity. First we will show that under certain conditions, the gossip algorithm will have a lower communication complexity than its determinisitic counterpart. Then we will show that the problem of finding the optimal gossip algorithm can be formulated as a convex optimization problem, which can be solved efficiently. 

\subsection{Comparison of Deterministic and Gossip Algorithms}
In this subsection we will compare the communication complexity of deterministic and gossip algorithms. The main motivation is that minimizing the communication complexity of a deterministic algorithm is computational hard since it involves $\bar d(P)$, which is related to the zero norm of each element of $P$. On the other hand, it is much easier to optimize the complexity for gossip algorithm, as will be shown in the next subsection. Hence, in this section we will give a sufficient condition under which the gossip algorithm will beat the deterministic one.

Consider a consensus matrix $P\in \mathcal S$. It is trivial to see that $P/N$ is a gossip matrix. Hence, for each deterministic consensus algorithm, there exists a corresponding gossip algorithm. The following theorem states under which condition $\Omega(P) \geq \omega(P/N)$:
\begin{theorem}
  \label{theorem:complexity}
  If the following inequality holds
  \begin{equation}
    \label{eq:fundamentalcondition}
    \lambda_2(P) \geq \frac{16}{\bar d(P)^2},  
  \end{equation}
  then 
  \begin{displaymath}
    \Omega_1(P) = \Omega_2(P) \geq \omega_1(P/N) \geq \omega_2(P/N).
  \end{displaymath}
\end{theorem}

Before proving the theorem, we need the following lemma:
\begin{lemma}
  \label{lemma:ineq}
  The following inequalities are true:
  \begin{align}
    (1+\frac{x}{N})^N &\leq e^x,&
    &-N \leq x \leq 0\label{eq:ineqexp}\\
    \log(x) - \log(y) &\leq x-y,&
    &x\geq 1,\, 1/x\leq y\leq x.\label{eq:ineqlog}
  \end{align}
\end{lemma}
The lemma can be proved by using mean value theorem and the details is omitted due to the space limit. Now we are now ready to prove Theorem~\ref{theorem:complexity}.
\begin{pf}[Proof of Theorem~\ref{theorem:complexity}]
  Using Theorem~\ref{theorem:complexitydet} and Corollary~\ref{corollary:gossip}, we only need to prove that condition \eqref{eq:fundamentalcondition} is sufficient for the following inequality to hold:
  \begin{equation}
    -\frac{2}{\log(\lambda_2(W))} \leq -\frac{N\bar d(P)}{2\max_{i=2,\ldots,N} \log(|\lambda_i(P)|)}.
    \label{eq:tmpineq}
  \end{equation}
  Since $\lambda_2(P) < 1$, without loss of generality we can assume $\bar d(P) \geq 4$. For gossip algorithm, it can be easily proved that when $P$ is a consensus matrix, $W = (N-1)/N I + P/N$,  which implies that the eigenvalue of $W$ satisfies
  \begin{equation}
    \lambda_2(W) = \frac{N-1}{N} + \frac{\lambda_2(P)}{N}.
    \label{eq:relationeigenvalue}
  \end{equation}
  Taking the exponential on both sides of \eqref{eq:tmpineq} and use \eqref{eq:relationeigenvalue}, one can see that the following inequality is equivalent to \eqref{eq:tmpineq}:
  \begin{displaymath}
     \left(1+\frac{\lambda_2(P)-1}{N}\right)^{N\bar d(P)/4}  \leq \max(\lambda_2(P),-\lambda_N(P)).
  \end{displaymath}
  Using \eqref{eq:ineqexp}, we know the following condition is sufficient:
  \begin{displaymath}
     e^{(\lambda_2(P) - 1)\bar d(P)/4}  \leq \lambda_2(P).
  \end{displaymath}
  Taking the logarithm on both sides yields
  \begin{displaymath}
     \log\left(\frac{\bar d(P)}{4}\right) - \log\left(\frac{\bar d(P)}{4}\lambda_2(P)\right)  \leq   \frac{\bar d(P)}{4}- \frac{\bar d(P)}{4}\lambda_2(P).
  \end{displaymath}
 As a result, from \eqref{eq:ineqlog} the following condition is sufficient :
 \begin{displaymath}
     \frac{\bar d(P)}{4}\lambda_2(P) \geq \frac{4}{\bar d(P)},
 \end{displaymath}
which completes the proof.
\end{pf}
\begin{remark}
  In general inequality \eqref{eq:fundamentalcondition} is true for a large class of networks. The main reason is that the condition does not depend on the size of the graph $N$. For many types of graphs (for example small world \citep{smallworld} and Abelian Cayley graphs \citep{abeliangraph}), $\lambda_2$ converges to $1$ as $N \rightarrow \infty$ when the average degree is fixed. Hence, the condition will hold when the size of the graph is large and $\bar d(P) > 4$.
\end{remark}

\subsection{Optimal Gossip Algorithm}
This subsection is devoted to formulating the optimal, i.e. with the lowest communication complexity, gossip algorithm as a convex optimization problem. We will only consider $\omega_1(P)$ since it has a simpler expression and it is an upper bound for $\omega_2(P)$. The optimization problem can be written as
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{P\in\mathcal S}&
  & \omega_1(P)=-\frac{2\sum_{i\neq j}P_{ij}}{\log(\rho(P))} \\
  &\textrm{subject to}&
 &\mathbf 1'P\mathbf 1 = 1.
\end{align*}
However the main difficulty here is that the objective function is in fractional form and it is not clear whether it is convex or not. In the next theorem we will show that we can constrain ourselves to the case where the diagonal elements of $P$ are $0$.
\begin{theorem}
  \label{theorem:null}
  \begin{equation}
  \omega_1(P) \geq \omega_1(\tilde P).  
  \end{equation}
\end{theorem}

\begin{pf}
  Let us consider the isomorphism from $\mathbb R^{N\times N}$ to $\mathbb R^{N^2}$, mapping an $N\times N$ matrix $X$ to the vector $\hat X$. Hence $\mathcal A_P$ , which is defined in \eqref{eq:lyapunoveq}, will also be mapped into an linear operator $\mathcal {\hat A}_P$ from $\mathbb R^{N^2}$ to $\mathbb R^{N^2}$. We want to prove that the corresponding operator $\mathcal {\hat A}_P$ is positive semidefinite. To do so, let us consider the inner product on $\mathbb R^{N^2}$. It is easy to see that
  \begin{equation}
    <\hat X,\hat Y> = \sum_{ij} X_{ij}Y_{ij} = trace(XY') = trace(X'Y).
    \label{eq:innerproduct}
  \end{equation}
Using the inner product, one can prove that an operator is positive semidefinite if and only if
\begin{align}
<\mathcal {\hat A}_P \hat X, \hat Y> &= <\hat X,\mathcal {\hat A}_P \hat Y>.\\
<\mathcal {\hat A}_P \hat X, \hat X> &\geq 0.  \label{eq:positivecheck}
\end{align}
It is easy to check both of them hold. Hence, $\mathcal {\hat A}_P$ is positive semidefinite. Since $\mathcal A_P$ shares the same eigenvalues as $\mathcal {\hat A}_P$, we know that all the eigenvalues of $\mathcal A_P$ are non-negative and real. Moreover the largest eigenvalue is less than $1$. Now we claim, although we omit the proof, that 
\begin{displaymath}
  \rho(P) = 1- \left(\sum_{i\neq j} P_{ij}\right)[1-\rho(\tilde P)].
\end{displaymath}
Let  $\alpha = \sum_{i\neq j} P_{ij} \leq 1$. Now we only need to prove that
\begin{displaymath}
  \omega_1(P) = -\frac{2\alpha}{\log(1- \alpha(1-\rho(\tilde P)))} \geq -\frac{2}{\log(\rho(\tilde P))}=\omega_1(\tilde P), 
\end{displaymath}
which is equivalent to
\begin{displaymath}
1-\rho(\tilde P)^\alpha \geq \alpha(1-\rho(\tilde P)),
\end{displaymath}
which can be easily proved using the mean value theorem.
\end{pf}
Using Theorem~\ref{theorem:null}, we can always constrain ourselves to the case where $P_{ii} = 0$. When $P_{ii} = 0$, $\omega_1(P) = -2/\log(\rho(P))$, minimizing the communication complexity is equivalent to minimizing $\rho(P)$. Therefore, the optimization problem can be written as 
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{P\in\mathcal S}&
  & \rho(P)\\
  &\textrm{subject to}&
 &\mathbf 1'P\mathbf 1 = 1,\,P_{ii} = 0.
\end{align*}
Since $\mathcal A_P$ is a positive semidefinite operator and it is linear with respect to $P$, $\rho(P)$ is convex and the above formulation is a convex optimization problem and can be solved efficiently.
