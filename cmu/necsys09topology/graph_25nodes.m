load dati
load Dati_25nodes

for i=1:3
    subplot(1,3,i)
    tree=Tree_vet{1,i};
    displaytopology;
end



% This part is used to enaluate the energy consumption of the minimum spanning tree of all the nodes 
% 
% V = 1:m;
% [cost,edges,steinernodes] = sfo_pspiel_get_cost(V,graph);
% SteinerT = sparse(m,m);
% energy = 0;
% for i = 1:size(edges,1)
%     SteinerT(edges(i,1),edges(i,2)) = graph(edges(i,1),edges(i,2));
%     energy = energy + graph(edges(i,1),edges(i,2));
% end