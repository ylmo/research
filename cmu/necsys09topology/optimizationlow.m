maxstep = 5; 
epsilon = 0.1;
gammaold =sum(abs(Gstar/S));
gammaold = 1./(gammaold+epsilon);
weight = min(graph(V,V)+ 1000*eye(size(V,2)));
weight(1)=0.0001;
for i = 1:maxstep
    cvx_begin
    %cvx_solver sedumi
    if i ~= maxstep
        cvx_precision low
    else
        cvx_precision default
    end
        cvx_quiet(true);
        variable G(n,mtmp)
        variable gamma(1,mtmp)
        variable totalgamma(1,mtmp);
        minimize (norm(weight.*totalgamma,1))
        subject to
        norm (G * S - Gstar,2)   <= 1;
        gamma >= sum(abs(G))

        totalgamma == gammaold.*gamma
    cvx_end
    gammaold = 1./(gamma+epsilon*0.5^i);
   % totalgamma
end
gamma = ((gammaold.*gamma) >= 0.001);