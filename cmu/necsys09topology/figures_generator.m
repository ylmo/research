load dati
load Dati_25nodes

for i=1:3
    subplot(1,3,i)
    tree=Tree_vet{1,i};
    displaytopology;
end



% This part is used to enaluate the energy consumption of the minimum spanning tree of all the nodes 
% 
% V = 1:m;
% [cost,edges,steinernodes] = sfo_pspiel_get_cost(V,graph);
% SteinerT = sparse(m,m);
% energy = 0;
% for i = 1:size(edges,1)
%     SteinerT(edges(i,1),edges(i,2)) = graph(edges(i,1),edges(i,2));
%     energy = energy + graph(edges(i,1),edges(i,2));
% end

load statistical_data

for i=1:length(ENERGY)
    Energy_vet=ENERGY{i};
    mean(i,:)=sum([Energy_vet(:,2)./Energy_vet(:,1) Energy_vet(:,3)./Energy_vet(:,1)])/length(Energy_vet);

end
i=30:5:50;
plot(i,mean(:,1),'k','LineWidth',2)
hold on
plot(i,mean(:,2),'k--','LineWidth',2)
axis([30 50 0.2 0.8])
legend('Algorithm 1 implemented with Problem 6','Algorithm 1 implemented with Problem 5')
xlabel('number of nodes (m)','fontsize',12,'fontweight','b')
ylabel(' normalized energy consumption','fontsize',12,'fontweight','b')
