maxstep = 5; 
epsilon = 0.1;
gammaold =sum(abs(Gstar/S));
gammaold = 1./(gammaold+epsilon);
weight = zeros(1,mtmp);
tmp = 1;
H = zeros(1,mtmp);
for i = 1:mtmp
    j = parent(i);
    if i == 1
        weight(1) = 0.0001;%prevent degeneracy
    elseif i > 1
        weight(i) = graph(V(i),V(j));
    end
    if j > 1
        H(tmp,i) = -1;
        H(tmp,j) = 1;
        tmp = tmp + 1;
    end
end
%weight = dist(1,2:m+1);
for i = 1:maxstep
    cvx_begin
    %cvx_solver sedumi
    if i ~= maxstep
        cvx_precision low
    else
        cvx_precision default
    end
        cvx_quiet(true);
        variable G(n,mtmp)
        variable gamma(1,mtmp)
        variable totalgamma(1,mtmp);
        minimize (norm(weight.*totalgamma,1))
        subject to
        norm (G * S - Gstar,2)   <= 1;
        gamma >= sum(abs(G))
        totalgamma == gammaold.*gamma
        H * totalgamma' >= 0
    cvx_end
    gammaold = 1./(gamma+epsilon*0.5^i);
    %totalgamma;
end
gamma = ((gammaold.*gamma) >= 0.001);