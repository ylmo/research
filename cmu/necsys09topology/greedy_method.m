
%load dati
%graph=graph+100*eye(m);

sel_sens=[1];
remaining_sens=2:length(graph);
energy=0;
cond=0;
while cond==0
    graph_temp=graph(sel_sens,remaining_sens);
    if length(sel_sens)==1
        [cost new_node]=min(graph_temp);
    else
    [cost new_node]=min(min(graph_temp));
    end
    sel_sens=[sel_sens remaining_sens(new_node)];
    remaining_sens=remaining_sens(find(remaining_sens~=sel_sens(end)));
   
    energy=energy+cost;
    V = sel_sens;
    P = Sigma;
    P = A* P *A' + Q;
    Ctmp = C(V,:);
    Rtmp = R(V,V);
    K = P * Ctmp' *inv(Ctmp*P*Ctmp'+Rtmp);
    P = P - K*Ctmp*P;
   % min(eig(Pd-P))
    if min(eig(Pd-P))>0 | length(sel_sens)==m
        cond=1;
    end
end


