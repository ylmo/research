plot (xaxis(1)-1,yaxis(1)-1,'ro','LineWidth',3)
hold on
plot (xaxis(2:m)-1,yaxis(2:m)-1,'k*')
for i = 1:m
    for j = 1:m
        if tree(i,j) ~= 0 
            plot([xaxis(i)-1 xaxis(j)-1],[yaxis(i)-1 yaxis(j)-1],'k','LineWidth',2);
            axis equal
            axis([0 4 0 4])
            xlabel('x_1','fontsize',12,'fontweight','b')
            ylabel('x_2','fontsize',12,'fontweight','b')
        end
    end
end
hold off