P = Sigma
for step = 1:1000  
    [tmp Index] = sort(E,'descend');
    Gamma = zeros(m);
    gamma = zeros(1,m);
    P = A* P *A' + Q;
    for i = 1:m
        Ptmp = P;
        Ctmp = C(Index(1:i),:);
        Rtmp = R(Index(1:i),Index(1:i));
        K = Ptmp * Ctmp' *inv(Ctmp*Ptmp*Ctmp'+Rtmp);
        Ptmp = Ptmp - K*Ctmp*Ptmp;
        if (min(eig(Pd-Ptmp)) >= 0)
            P = Ptmp;
            break
        end
    end
    gamma(Index(1:i)) = 1;
    step
    disp 'gap is:'
    [max(eig(Pd-P)) min(eig(Pd-P))]
    mingreedygap(step) = min(eig(Pd-P));
    disp 'energy used'
    gamma
    greedyschedule(step,:)=gamma;
    disp 'energy left'
    E = E - gamma
    save
    if min(E) < 0
        break
    end
end