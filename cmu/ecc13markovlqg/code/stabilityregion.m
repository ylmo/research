clc
clear

%initialization
A = diag([1.05,-1.05]);
B = [1;1];
C = eye(2);
W = eye(2);
Q = eye(2);
R = eye(2);
U = 1;

%Kalman P
P = eye(2);
for i = 1:100
    P = A*P*A'+Q;
    P = P-P*C'/(C*P*C'+R)*C*P;
end

%LQG
J = zeros(1000);
stable =zeros(1000);
for i = 93:1000
    tic
    for j = 1:1000
        alpha = i/1000;
        beta = j/1000;
        S = eye(2);
        R = eye(2);
        for k = 1:300
            Tmp = A'/(R\eye(2)+B'*U*B)*A';
            Snew = W + (1-alpha)*A'*S*A + alpha* Tmp;
            Rnew = W + beta*A'*S*A + (1-beta)*Tmp;
            deltaS = Snew - S;
            deltaR = Rnew - R;
            S = Snew;
            R = Rnew;
            if norm(deltaS) <=0.01 && norm(deltaR) <=0.01
                stable(i,j) = true;
                break
            end
        end
        J(i,j) = trace(beta*S*Q+alpha*R*Q+alpha*A*R*B*inv(U+B'*R*B)*B'*R*A'*P)/(alpha+beta);
    end
    toc
    i
end

%surf([0.001:0.001:1],[0.001:0.001:1],J)

maxbeta = zeros(1,1000);

for i = 1:1000
    for j = 1000:-1:1
        if stable(i,j) == 1
            maxbeta(i) = j;
            break
        end
    end
end

for i = 1:1000
    if maxbeta(i)~=0
        alphastart = i;
        break
    end
end

fill([0 0 1 1],[0 1 1 0],'r')
hold on
fill([alphastart/1000 (alphastart:1000)/1000 1],[0 maxbeta(alphastart:1000)/1000 0],'b')