\documentclass[10pt,oneside,a4paper]{article}
\pdfminorversion=4

\author{Yilin Mo, Jo\~{a}o Hespanha, Bruno Sinopoli}

\title{Answers to Comments of Reviewer 2}

\usepackage{amsfonts}
\begin{document}\maketitle

We are grateful to the reviewer for his/her thorough and constructive reviews. We feel that there is some confusion on the problem formulation and worst-case approaches presented in the paper, which we wish to address in the following response letter. 
\vspace{1cm}

\emph{In the revised version and the authors' response, most of the issues I raised have been satisfactorily addressed or clarified. The only exceptions are the threat model and a related issue, the worst case approach used in the paper. The threat model assumes that an adversary can manipulate any combinations of $n$ out of $m$ sensor' measurements (see the set Gamma for Eq. (1)). I disagree with the authors that this threat model is the same as the worst case (when the ``worst'' n sensors are manipulated) of the more realistic threat model I described (i.e., a fixed set of n sensors, unknown to the detector, can be manipulated). Under my threat model, Gamma for Eq. (1) is a set with fixed indexes for gamma, which is always a proper subset of the paper's Gamma except for the case n=m. Therefore, the paper's threat model is not the worst case of my threat model, and many results or conclusions may no longer be valid under my threat model.}
\vspace{.5cm}

We want to further elaborate on the equivalence of the reviewer's threat model and ours. In our model, the detector will commit an error if there exists $\gamma\in\Gamma$ and $y^a$, such that 
\begin{displaymath}
x\neq \hat x  = f(y+\gamma\circ y^a).
\end{displaymath}
Let us assume that $\gamma_*$ and $y^a_*$ are such sensor selection and bias vectors, such that $x \neq \hat x$. In other words, if the attacker chooses to compromise $\gamma_*$ and inject $y^a$, the detector will commit an error.

On the other hand, the reviewer suggested that $\gamma$ should be fixed and ``unknown'' to the detector. Therefore, in the worst case, the adversary will compromise $\gamma_*$, which implies that he/she can inject $y^a_*$ and enforce the detector to commit an error, i.e., $x \neq \hat x$.

To conclude, the detector will commit an error under both the reviewer's threat model and ours. Therefore, both models are equivalent since we are only concerned with the probability of error.

\vspace{.5cm}
\emph{ For example, Lemma 2 may not hold since its proof uses a fact, not true under my threat model, that an adversary can manipulate the last m-n sensors and at the same time can also manipulate the first n sensors, equivalently to assuming that an adversary is capable to manipulate all the m sensors but would restrict himself/herself to only manipulate up to n sensors each time. This restriction does not make much sense from security's point of view. In security studies, we usually assume that an adversary would apply his/her full capability assumed by the threat model in malicious attacks. This is why I conclude that the paper's threat model is unrealistic. Note that the threat model is essential in any security study.}
\vspace{.5cm}

The proof of Lemma 2 does not imply that the attacker can control both the first $n$ sensors and the last $m-n$ sensors simultaneously. In fact, the proof should be interpreted as: the detector will receive the same manipulated measurement $y$, if either (not both) of the two following scenarios occurs:
\begin{enumerate}
  \item the true measurement is $y^+$ and the attacker compromises the last $m-n$ sensors;
  \item the true measurement is $y^-$ and the attacker compromises the first $n$ sensors.
\end{enumerate}
Since all the information that the detector receives is $y$, there is no way for the detector to distinguish the first case from the second case as the set of the compromised sensors is ``unknown''. 

It is worth noticing that our proof is very similar to the impossibility proof of Byzantine generals problem (Lamport, Leslie, Robert Shostak, and Marshall Pease. \emph{"The Byzantine generals problem."} ACM Transactions on Programming Languages and Systems (TOPLAS) 4.3 (1982): 382-401). The impossibility result is established since a loyal lieutenant receives exactly the same messages, for two possibilities:
\begin{enumerate}
  \item The commander is malicious and the other lieutenant is loyal;
  \item The commander is loyal and the other lieutenant is malicious.
\end{enumerate}
The proof does not imply that both the commander and the other lieutenant are malicious at the same time. It only states that one of them is malicious and that the loyal lieutenant is unaware of which one.

Furthermore, we want to clarify that we did not consider sequential detection problems in this paper. As a result, the detector only gets one measurement from each sensor. Thus, it is not true that the adversary ``would restrict himself/herself to only manipulate up to n sensors each time'', as the attacker will only be able to manipulate the measurement once. The one-step detection problem presented in this paper is particularly interesting since
\begin{enumerate}
  \item It is a fundamental step towards the secure sequential detection problem;
  \item For some systems, such as power grids, the interval between each sampling period is long enough such that the attacker could inflict serious damage. As a result, the detection problem should be securely solve for each sampling period.
\end{enumerate}

\vspace{.5cm}

\emph{Another issue that the paper should mention but does not do so is that the minimax approach used in the paper may not offer much insightful information in a security study (and also signals detection) when the worst case is nothing from the observations can be used (i.e., the poorest performance for a detector) such as the case when n >= m/2 (which might be true when the minimax detector's performance is very poor).} 
\vspace{.5cm}

We respectfully disagree with the reviewer on the importance of our results. To our knowledge, many impactful results in security and signal processing are impossibility results. To list a few:
\begin{enumerate}
  \item The impossibility proof of Byzantine generals problem states that if there are no less than $n$ malicious general among $3n$ generals, then consensus cannot be achieved. 
  \item In the field of coding theory, we are aware that the error correction code cannot correct $n$ errors if the minimum distance between the codes is less than $2n$. 
  \item In robust statistics, we know that there is no ``good'' estimator with breakdown point more than 0.5. 
\end{enumerate}
In our paper, we provide a similar result by saying that the detector cannot extract information about the state from the measurements if more than half of them are manipulated. All these impossibility results illustrate the fundamental limitations of security and signal processing, which are all of great importance. 
 
Furthermore, the ``impossibility'' result in Section III is only a small portion of the paper. In Section IV, V and VI, we consider the case where less than half of the sensors are malicious and derive optimal or heuristic detectors for these cases, which is the main contribution of the paper.

\vspace{.5cm}
\emph{For example, the worst case might hardly occur.}
\vspace{.5cm}

We respectfully disagree with the reviewer's argument that ``a worst-case approach is not important since the worst case might hardly occur''. To our knowledge, a substantial amount of robustness researches are based on the worst-case approaches, e.g., robust statistics, robust decision theory and robust control. In our paper, we consider an intelligent adversary, who is more than likely to be able to perform a worst-case attack. Also knowing the worst case scenario is a widely accepted methodology in safety critical systems. 

\vspace{.5cm}
\emph{Other types of detectors may be much better in this case.}

\vspace{.5cm}
As we have proved in the paper, our detector is the optimal detector for the worst-case attack. Thus, no detector can performance better. It will be really helpful if the reviewer could elaborate on this point by either providing a counterexample or pointing out a flaw in our proof.

\vspace{.5cm}
\emph{I agree with the first reviewer that the paper is a parameter estimation problem under manipulations rather than addressing a security problem as the paper tries to present to audience. A security approach for the same problem might be as follows: first identify (even with a certain probability) the malicious n sensors and then use the benign sensors to detect (i.e., estimate the parameter). Identification of malicious n sensors may rely on the a-priori information such as the distribution of theta and each sensor's distributions for theta equaling to 1 and -1. The n sensors with the largest deviations from their a-priori distributions may be identified as malicious and excluded from parameter estimation. We may also use different subsets of sensors to estimate the parameter theta, and check the distributions of their estimated thetas against the a-priori distribution to identify n malicious sensors. These methods may be combined in an approach. They may offer a reasonably good estimation of the parameter theta even when an adversary can manipulate n >= m/2 sensors, for which case the paper's minimax approach concludes that no observed information should be used in estimating the parameter theta and a guess meeting the a-priori distribution should be used. This is particularly true if the adversary is not careful enough that a manipulated sensor produces a distribution very different from the a-priori distribution (for example, when a measurement output is invalid such as a negative value when valid measurements are non-negative).}

\vspace{.5cm}
We prove in the paper that our detector is optimal in the class of all detectors of the form $\hat x = f(y)$. The reviewer claims that a security approach will first find a bad data detector $g$ to remove the malicious measurements and then to use another detector $h$, which is based on the benign measurements. In a nut shell, the reviewer suggests that a better design should be of the following form $\hat x = h(g(y))$, which is already included in our more general form $\hat x = f(y)$. As a result, the secure detector that the reviewer suggested cannot perform better than ours. 

We feel that the reviewer's argument is based on a conjecture that the suggested detector ``may offer a reasonably good estimation of the parameter theta''. The performance of the such a detector is neither quantified nor proved to be better. In contrast to that we offer a mathematical proof of optimality. We believe that a better justification should be provided to support such a design over ours.

Furthermore, in this paper, we only consider a one-step detection problem instead of a sequential detection problem. In other words, we only have one measurement from each sensor. Therefore, there is no way to compute a meaningful empirical distribution (other than a point mass measure) based on the sensor measurement, as the reviewer suggested.

\vspace{.5cm}
\emph{In conclusion, the paper is an interesting math exercise to solve an artificial problem unrelated to realistic security problems, and the results and conclusions may not offer insightful information to security people. I would recommend rejection of the paper with the current inconvincibly justification of the problem addressed by the paper. If the authors can justify the addressed problem that solving the problem is meaningful, I am happy to recommend publishing the paper.}

\vspace{.5cm}
We hope to have addressed the reviewer's issues. We also urge the reviewer to take into consideration the venue where this paper is submitted. We are trying to propose methodologies that can be complementary to the ones commonly used in security, by using the tools from signal processing. We feel that this should be seen as an attempt of establishing meaningful connections between signal processing and security.

\end{document}
