\documentclass[10pt,oneside]{article}
\pdfminorversion=4
\usepackage{tikz}
\usepackage{amsfonts}

\author{Yilin Mo, Jo\~{a}o Hespanha, Bruno Sinopoli}

\title{Answers to AE's and reviewers' comments}

\begin{document}\maketitle

We are grateful to the editor and to the reviewers for their constructive reviews. 


We have revised the paper taking into account all their remarks. 


As for reviewer 2 we have provided thorough explanations to all of the issues raised. Despite providing a mathematical proof in the paper the reviewer was not convinced of the optimality the constant detector in the case where $n \geq m/2$. Rather than finding a flaw in our proof the reviewer proposes a detection+estimation algorithm which he claims to outperform our constant detector.  We show in our response, via a counterexample, how the detector proposed by reviewer underperforms our constant detector. Reviewer 2 also casted doubts about our threat model saying that it is not a byzantine attack model. We show in our response that our model is a byzantine attack model. 

Finally we have added several relevant references to articles published in the IEEE TSP.
 
The detailed responses to each reviewer are listed below: 

\section* {To Reviewer \# 1}

\emph{This paper studies the problem of detecting a binary state based on possibly manipulated measurements. The paper is highly mathematical and rather abstract, but I nevertheless find it quite interesting and novel. However, I do think the authors could try to make the material a bit more accessible, for example by including some simple examples to illustrate the introduced concepts (see suggestion below).}

\emph{Other comments (page numbers refer to 1-column version):}

\begin{itemize}
  \item \emph{Page 2: It is said that SCADA systems are mostly isolated. This is not really true. Even if a SCADA system is not directly connected to the internet, service technicians regularly connect laptops and flash drives to upgrade or fix the system, and can thereby easily transfer malware to the SCADA system.}

    We thank the reviewer for comment. We have modified the introduction from ``SCADA systems are currently mostly isolated'' to ``most SCADA systems are currently running on dedicated networks''. 

  \item \emph{Page 2: To me the most critical assumption appears to be that the defender knows the number of attacked sensors $n$, since this highly affects the policy he/she should use. Do the authors have any idea of how a defender possibly could estimate this number?}

    The number $n$ can be seen as a design parameter for the defender. In general, increasing $n$ will increase the resilience of the detector under attack. However, a large $n$ will result in performance degradation during normal operation when no sensor is compromised. For example, when the defender want to be resilient to more than half compromised measurements, the detector becomes a constant. Therefore, there exists a trade-off between resilience and efficiency (under normal operation), which can be tuned by choosing a suitable parameter $n$. A remark has been added into the paper to clarify this issue.

  \item \emph{Theorem 1: What if $p^+=p^-$? Is not $f_*=-1$ also an optimal detector then? The theorem statement seems to imply there is only one optimal detector.}

    The reviewer is correct. The optimal detector may not be unique and we have changed the statement of the theorem accordingly.

  \item \emph{Definition 1: This definition is critical to understand the following material. I would recommend the authors to spend a bit more time explaining this concept, for example, by means of a simple example.}

    An example has been added to make the definition more clear. 

  \item \emph{Theorem 4: It is not so clear to me how to compute the $\eta_i$ in the theorem. I assume Remark 3 refers to this computation? This could be made more clear.}

    In general, computing $\eta_i$ is not trivial as the optimization problem (13) is not necessarily convex. Hence, numerical methods such as exhaustive search may be needed to find the optimal or suboptimal $\eta_i$. 

    However, the optimization problem (13) is much easier than the general problem (11), as the search space has been reduced from a pair of $m$ dimensional sets to a $m$ dimensional vector. 

\end{itemize}

\emph{Typos:
\begin{itemize}
  \item Page 4: Borel measurable set $->$ Borel measurable sets
  \item Page 6: The use inner $->$ The use of inner
  \item Page 9: such set $X$ $->$ such a set $X$
  \item Page 15: $|| -> |\cdot|$
\end{itemize}}

We thank the reviewer for the careful review. We have fixed all the listed typos.
\newpage

\section* {To Reviewer \# 2}

\emph{This version is almost the same as the last version. The main issue that prevents me from recommending acceptance of the paper as is is the threat model and the corresponding restriction of the problem addressed in the paper. I strongly encourage the authors to discuss them sufficiently in the paper so that readers know the restrictions of the addressed problem and its conclusions.}

We thank the reviewer for the effort placed in the review of the paper. We hope to clarify once and for all the issues raised by the reviewer. 

\begin{itemize}
  \item \emph{Multiple measurements are typical for modern sensors in real applications. It is possible to have a solution for multiple measurements when there is no solution for a single measurement (I don't consider always outputting 1 as a solution).}

 We believe that the reviewer refers to multi-step measurements (multiple measurements taken at multiple time-steps) when he talks about ``Multiple measurements'', as our approach does consider multiple measurements in a single time-step. We agree with the reviewer that multi-step measurements are typical for modern systems and we plan to consider this case in future work.  However the single step case is of fundamental importance as 
\begin{enumerate}
\item It is an open problem in robust detection where a solution does not exist to the best of our knowledge. It is in our view a nontrivial problem, as the mathematical sophistication of the  paper clearly corroborates;
  \item It is a critical step towards understanding the multi-step measurements problem;
  \item It is a relevant problem in many applications. For some systems, such as power grids, the interval between each sampling period is long enough (5-10 min) such that the attacker may induce misdirection and inflict serious damage between sampling intervals. 
 Another common objective of attackers is the injection of false alarms. False alarms can be used as decoys to distract attention from other parts of the systems (techniques like these are often used in battlefield scenarios). Furthermore false alarms can be costly as the resources need to be scheduled to respond to an alarm. Finally a large class of systems has sufficiently fast dynamics that waiting to collect multi-step measurements would incur an unacceptable delay in the response. 
 %It is often the case that numerous measurements are needed to empirically estimate a distribution.
  %data As a result, the detection problem should be securely solved for each sampling period. A detector based on empirical distribution of sensor measurements, as is suggested by the reviewer, will be too slow for such systems.
\end{enumerate}

%Furthermore, we agree with the reviewer that a constant detector is not a good choice. Therefore, our result should be interpreted as no good detector exists when more than half of the sensor are compromised. Hence, other methods, such as software authentication, tamper resistant devices should be used to ensure that no more than half of the sensors are compromised.


  \item \emph{An optimal solution for a single measurement is not necessarily an optimal solution for multiple measurements. The authors' claim (in the response) that the optimal solution discussed in the paper is also an optimal solution for the case of multiple measurements is questionable since additional information can be used for multiple measurements, for example the probability distribution of $\theta$. I think the following simple detection and estimation method should provide a better result that the authors' optimal solution for multiple measurements: for the case  $n \geq m/2$, the paper's conclusion is that the measurements are useless and the a-priori distribution of $\theta$ is used, which is to always output 1. In my approach (assuming adversaries controls a fixed set of $n$ sensors unknown to detector), we can observe sufficient measurements from each sensor, and check their collective behaviors as well as each sensor's observed distribution. For a sensor that adversaries cannot touch, the distribution is not disturbed, and thus matches the expected distribution (obtained by using the a-priori knowledge on $\theta$'s distribution and each sensor's conditional distribution given $\theta$. For a sensor that adversaries modify its output to incur detection errors, the distribution is disturbed, and thus mismatches the expected distribution. That would provide us a probability to detect malicious sensors. We can then rely on unlikely manipulated sensors to estimate $\theta$.}

    \emph{To show the detection + estimation approach, we use a simplified example. Assume each sensor outputs only 1 and -1 and highly accurate (i.e., when $\theta$ is 1 or -1, a sensor is very likely output 1 or -1) that we ignore possible errors from sensors. This simplified problem is similar to binary value (attack or retreat) in the Byzantine generals problem. To further simplify the problem, assume $m=2n-1$ (i.e., $n$ malicious and $n-1$ benign), and adversaries adopt the worst case approach to manipulate malicious sensors in order to maximize the detection’s error probability. Then when $\theta$ is 1, the $n-1$ benign sensors reports 1, and adversaries would output -1 for each malicious sensor in order to incur a detection error. We can get similar result for the case that $\theta$ is -1. We can easily group sensors into two groups based on their outputs: the benign sensors (output 1) in one group and malicious sensors (output -1) in another group. We don't know which group is malicious at this point, but we can observe sufficient measurements. Then we can estimate each group's distribution and compare it with theta's distribution (which is known). The benign group matches theta's distribution while the malicious group matches the opposite distribution of theta (since malicious sensors would always generate an opposite result in order to incur detection error). If theta's distribution is biased, we would be able to tell which group is malicious and which is benign. We can then change the detector to ignore malicious sensor’s outputs, and to use only benign sensors to estimate theta. This detection and estimation method is much better than the ``optimal'' solution the authors mentioned in the response for the case of multiple measurements, which is to always output 1. Of course, this example has been simplified too much, but the intuition of the detection and estimation method has been displayed.}


    %We agree with the reviewer that the optimal multi-measurement detector is in general different from single-measurement detector. 
We thank the reviewer for the illustrative example. First of all we wish to make it clear that we never claimed that the constant detector derived for the case where $n \geq m/2$ is also optimal in the multi-step measurement case, which we do not consider at all in the paper. Conversely we have a mathematical proof that the constant detector is optimal in the single step measurement case when $n \geq m/2$. If the reviewer does not believe this to be the case, he should point out an error in our proof, rather than speculating. 

Secondly the reviewer proposes a detector+estimation algorithm claimed to perform better, i.e. to have lower error probability, than our constant detector in the multi-step case for $n \geq m/2$. Although this has nothing to do with our paper we will comment on it.


It is true that the detector proposed by the reviewer can perfectly detect $\theta$, given that the adversary employs a simple flip strategy. However, the reviewer's argument is flawed as the proposed attack strategy is not worst case. We will show, via a counterexample, that the probability of error of the detector proposed by the reviewer is at least twice as large as that of the constant detector. We will use the same assumptions as the reviewer, i.e. that the sensors are perfect, and the detection strategy proposed by the reviewer and contrast it with a constant detection strategy we use in the single-step case.

%However, the main assumption in this paper is that the adversary knows the detector algorithm and hence will design an optimal attack to increase the probability of error. Therefore, the performance of the detector is judged by the ``worst-case'' performance. Hence, the argument that the proposed detector is better than the constant detector is not valid since the reviewer only consider one attack strategy. In the following example, we will show 

Consider the problem where we cast a fair die for each sampling period. We let $\theta = -1$ if the result is $1$ and $\theta = 1$ if the result is $2,3,4,5$ or $6$. Therefore, $p^- = 1/6$ and $p^+=5/6$. Furthermore, we assume that the sensors are perfect, as the reviewer proposes, i.e., $y_i = \theta$.
    
    It is trivial to see that a constant detector with output $1$ will only commit an error when $\theta = -1$ (or the result of the die is $1$). Hence, it has a probability of error $P_e = p^- = 1/6$ regardless of the attacker's action.

    Let us describe the detection strategy of the reviewer as follows:
    \begin{itemize}
      \item The detector checks whether the empirical frequency of each sensor measurement $y_i = -1$ equals $1/6$. If the empirical frequency is not $1/6$, then the sensor is flagged as a ``bad'' sensor. Otherwise, it is flagged as a ``good'' sensor. 
      \item The detector makes a decision based on the majority voting of the ``good'' sensors.
    \end{itemize}

    Consider the following attacker's strategy. For every compromised sensor, if the result of the die is $2$, then the sensor sends $y_i' = -1$. On the other hand, if the result is $1,3,4,5$ or $6$, the sensor sends $y_i' = 1$. It is clear that the compromised sensors will send $-1$ with probability $1/6$, since we assume that the die is fair. Hence, no sensor will be flagged as ``bad'' by the reviewer's detector. 

    Now consider the output of the reviewer's detector, which is summarized in the following table:

    \begin{table*}[h]
      \centering
      \begin{tabular}{c|cccc} \hline
	Die & $\theta$ & $n-1$ benign sensors & $n$ compromised sensors & $\hat \theta$   \\ \hline
	1 & -1 & -1 & 1 & 1\\
	2 & 1 & 1 & -1 & -1\\
	3,4,5,6 & 1 & 1 & 1 & 1\\\hline
      \end{tabular}
    \end{table*}

Whenever the result of the die is $1$ or $2$, the detector proposed by the reviewer will commit an error, which implies that the probability of error of such detector is at least $1/3$\footnote{Our attack strategy is not necessarily worst-case. Therefore, the ``worst-case'' probability of error could be even larger than $1/3$.}. Furthermore, in no case the detector proposed by the reviewer will succeed when the constant one fails.

% the constant detector outperforms the proposed detector, in the sense that whenever the constant detector commits an error, the proposed detector also commits an error. As a consequence, the proposed detector is even worse than a constant detector.


  \item    \emph{I don't think the authors' argument for Lemma 2 under my threat model (a fixed set of sensors under adversary's control but unknown to the detector) is correct. There is subtle difference between my threat model and the paper's threat model. The paper's model (Eq. 1) is equivalent to that adversaries can modify any sensor but only choose to modify up to $n$ arbitrary sensors. Under my threat model, the two cases listed in the responses (i.e., modify the last $m-n$ sensors and modify the first $n$ sensors) cannot hold at the same time, and thus the conflict derived in Lemma 2 would never occurs. The two cases can be true at different sessions, but we should restrict conflict to the same session (i.e., the set of malicious sensor stays the same for a given session) instead of cross sessions since under my threat model, if one session has occurred, the other session cannot occur (i.e., given one session, the probability of another session is 0). This is very different from Byzantine generals problem wherein they have never derived any conflict between the two cases. Instead, it uses the fact that a lieutenant general has no way to tell the two cases, and has to follow the commander, which causes violation of IC1. In the paper's case, the detector cannot tell the two cases, but that does not incur any violation (or conflict as the proof tries to derive since the two cases have to occur in two different sessions under my threat model – given one occurs, the other can never occur). I would like to see further elaboration/explanation by the authors on this issue.}

 We do not understand the subtle point the reviewer makes, as we believe there is no difference between his threat model and ours. To make our point and hopefully bring the discussion to a conclusion we will show how the secure detection problem proposed in our paper, in its simplest form, can be seen as a Byzantine generals problem. 
Consider we have two perfect sensors, such that $y_i = \theta,i=1,2$. However, one sensor is compromised.

On the other hand, consider the Byzantine generals problem, where the general sends two messages to lieutenant 1 and 2. Lieutenant 2 also forwards the message sent by the general to lieutenant 1. We assume the message only contains a number, which is either $-1$ or $1$.

Let us define the number sent from the general to lieutenant 2 as $\theta$. We further define the number sent from the general to lieutenant 1 as $y_1$ and the number sent from lieutenant 2 to lieutenant 1 as $y_2$. The diagram of the proposed communication strategy is illustrated in the following graph:

\begin{center}
  \begin{tikzpicture}
    \node (general) at (0,0) [rectangle, draw, semithick] {General};
    \node (l1) at (-2,-2) [rectangle, draw, semithick] {Lieutenant 1}
    edge [<-,semithick] node[auto]{$y_1$} (general);
    \node (l2) at (2,-2) [rectangle, draw, semithick] {Lieutenant 2}
    edge [<-,semithick] node[auto,swap]{$\theta$} (general)
    edge [->,semithick] node[auto]{$y_2$} (l1);
  \end{tikzpicture}
\end{center}

The general is called benign if $y_1 = \theta$, and lieutenant 2 is called benign if $y_2 = \theta$. The goal of the Byzantine general problem is 
\begin{itemize}
  \item If the general is benign, then lieutenant 1 should agree with the general, i.e., the decision of lieutenant 1 is $y_1$, which equals $\theta$.
  \item If the general is malicious and lieutenant 2 is benign, then lieutenant 1 should agree with lieutenant 2, i.e., the decision of lieutenant 1 is $y_2$, which again equals $\theta$.
\end{itemize}
As a result, the Byzantine general problem can be seen as estimating $\theta$ based on message $y_1$ and $y_2$, which is essentially the same as the simple detection problem with two perfect sensors proposed above. Hence, the undecidability of Byzantine general problem implies that $\theta$ cannot be determined from $y_1$ and $y_2$ and should be decided purely based on the a-priori distribution. We hope that this example clarifies the reviewer's doubts and provides intuition for our constant detector. WIth no prior our problem would be also undecidable. 
\end{itemize}

\emph{I don't say that my threat model should be used in the paper. It may likely make proofs and results hard to get. At least the paper should justify the threat model and discuss its limitations in the paper since, as I have pointed out in the past reviews, the threat model is way off the reality and has assumed that adversaries have too much power.}

As shown above, our model is a Byzantine attack model, which is widely accepted in the literature.
\end{document}
