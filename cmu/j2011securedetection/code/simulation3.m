x = [-4:0.002:4];
pplus = 0.6;
pminus = 1 - pplus;
a = 1;
y1 = 1-pplus*qfunc(x-a)-pminus*qfunc(-x-a);
y3 = 1-pplus*qfunc(x-a).^3-pminus*qfunc(-x-a).^3;
y5 = 1-pplus*qfunc(x-a).^5-pminus*qfunc(-x-a).^5;

[mini1,index1] = min(y1);
x1 = x(index1);

[mini3,index3] = min(y3);
x3 = x(index3);

figure(1)
plot(x,y1,'linewidth',1)
hold on
plot(x1,mini1,'or','linewidth',1)
set(gca,'Fontsize',14);
xlabel('zeta','fontsize',14)
ylabel('Probability of Error','fontsize',14)

figure(2);
plot(x,y3,'linewidth',1)
hold on
plot(x3,mini3,'or','linewidth',1)
set(gca,'Fontsize',14);
xlabel('zeta','fontsize',14)
ylabel('Probability of Error','fontsize',14)

figure(3);
plot(x,y5,'linewidth',1)
set(gca,'Fontsize',14);
xlabel('zeta','fontsize',14)
ylabel('Probability of Error','fontsize',14)