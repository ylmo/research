% a = 1;
% P = zeros(20,5);
% tic
% for m = 1:20
%     for experiments = 1:100000
%         y = randn(1,m)+ a;
%         z = sort(y);
%         for n = 0:min((m-1)/2,4)
%             if sum(z(1:(m-2*n))) < 0
%                 P(m,n+1) = P(m,n+1)+1;
%             end
%         end
%     end
% end
% toc
% P = P/100000;

%n=0
semilogy(1:20,P(:,1),'linewidth',2)
hold on
semilogy(3:20,P(3:20,2),'linewidth',2)
semilogy(5:20,P(5:20,3),'linewidth',2)
semilogy(7:20,P(7:20,4),'linewidth',2)
%semilogy(9:20,P(9:20,5),'linewidth',2)
%semilogy(1:20,zeros(1,20)+0.5)
%set(gca,'Fontsize',14);
xlabel('m')
ylabel('Pe')
xlim([1 20])