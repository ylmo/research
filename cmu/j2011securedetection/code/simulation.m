pplus = 0.6;
pminus = 0.4;

a = 1;
m = 5;

zeta = 0;
for j = 1:50
    ratio = Qfunc(-zeta - a)/Qfunc(zeta-a);
    zeta = ((m-1)*log(ratio)+log(pminus)-log(pplus))/2/a
end