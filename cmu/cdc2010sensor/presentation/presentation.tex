\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi
%for handout
\mode<presentation>

\title[Sensor Selection] {Stochastic Sensor Scheduling in Wireless Sensor Networks with General Graph Topology}
\author[Yilin Mo]{Yilin $\textrm{Mo}^*$, Emanuele $\textrm{Garone}^\dag$, Alessandro $\textrm{Casavola}^\ddag$, Bruno $\textrm{Sinopoli}^*$}
\institute[Carnegie Mellon University]{$*$: Department of Electrical and Computer Engineering,\\ Carnegie Mellon University\\
$^{\dag}$:Service \\
$^{\ddag}$: Dipartimento di Elettronica, Informatica e Sistemistica,\\Universit\`{a} degli Studi della Calabria}
\date[ACC 2012]{2012 American Control Conference}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\begin{frame}{Wireless Sensor Networks}
	\begin{itemize}
		\item Wireless Sensor Networks (WSNs) are becoming ubiquitous, with lots of interesting applications. 
		\item Sensors are usually battery powered. 
		\item Transmission costs too much energy.
		\item Solutions: new devices, energy efficient physical layer, topology control, \alert{sensor selection}\dots
	\end{itemize}
\end{frame}
\begin{frame}{Motivation}
	\begin{itemize}
		\item Suppose $m$ unique sensors measuring the same quantity, the error covariance of the estimation is the covariance of each measurement divided by $m$.
			\begin{displaymath}
				\sigma^2 = \sigma_y^2 / m.
			\end{displaymath}
		\item The energy cost is at least proportional to $m$.
		\item Depending on application, not all sensors are needed!
		\item Put sensors to sleep to save energy.
	\end{itemize}
\end{frame}

\begin{frame}{System Model}
	We consider the WSN is monitoring the following LTI(Linear Time-Invariant) system
	\begin{block}{System Description}
			\begin{equation}
				\begin{split}
					x_{k+1} &= Ax_k +  w_k,\\
					y_{k} &= C x_k + v_k.
				\end{split}
				\label{eq:systemdiscription}
			\end{equation}
		\end{block}
		\begin{itemize}
			\item $x_k \in \mathbb R^n$ is the state vector.
			\item  $y_k \in \mathbb R^m$ is the measurements from the sensors. $y_{k,i}$ is the measurement of sensor $i$ at time $k$.
			\item $w_k,v_k,x_0$ are independent Gaussian random variables, and $x_0 \sim \mathcal N(\bar x_0,\;\Sigma)$, $w_k \sim \mathcal N(0,\;Q)$ and $v_k \sim \mathcal N(0,\;R)$. 
			\item In addition we assume that $R$ is diagonal.
		\end{itemize}
	\end{frame}


	\begin{frame}{Topology}
		\begin{itemize}
			\item We assume the sensor network has a tree topology $G = (V,E)$, with fusion center as root node.
			\item $V= \{s_0,s_1,\ldots,s_m\}$ is the set of fusion center and sensors.
			\item $E\subseteq V\times V$ is the set of edges. We also assign a weight $c(e_{i,j})$ to each edge to indicate the cost of communication.
			\item At each time, a subtree $T = \{V_T, E_T\}$ is chosen, where $s_0 \in V_T \subseteq V$ and $E_T \subseteq E$. All the sensors in $V_T$ will transmit their measurements back to the fusion center.
			\item The energy cost associated with tree $T$ is defined as
				\begin{displaymath}
					\mathcal E(T) = \sum_{e\in E_T} c(e). 
				\end{displaymath}
			\item The set of all possible subtrees is denoted as $\mathcal T$.
		\end{itemize}
	\end{frame}

	\begin{frame}{Kalman filter}
		A Kalman filter is implemented at the fusion center to perform state estimation, which takes the following form:	
		\begin{block}{Kalman Filter}
			\begin{align}
				\hat x_{0} & = \bar x_0 ,\quad P_{0}  = \Sigma\\
				\hat x _{k + 1|k} & = A \hat x_{k}  , \quad P_{k + 1|k}  = AP_{k} A^H  + Q \nonumber\\
				K_k& = P_{k|k - 1}  C_{T_k}^H ( C_{T_k}P_{k|k - 1}  C_{T_k}^H  +  R_{T_k})^{ - 1}  \nonumber\\
				\hat x_{k}& = \hat x_{k|k - 1}  + K_k (y_k  - C_{T_k} \hat x _{k|k - 1} )  \nonumber\\
				P_{k}& = P_{k|k - 1}  -  K_k  C_{T_K}P_{k|k - 1}  \nonumber,
			\end{align}
		\end{block}
		where $\hat x_{k}$ is the state estimation at time $k$, $P_{k}$ is the estimation error covariance matrix. $T_k$ is the transmission tree used at time $k$. $C_{T_k} =[C_{i_1}',\ldots,C_{i_j}']'$, $R_{T_k} = diag(r_{i_1},\ldots,r_{i_j})$.
	\end{frame}

	\begin{frame}{Information Filter}
		Let 
		\begin{displaymath}
			Z_k = P_k^{-1},\,Z_{k+1|k} = P_{k+1|k}^{-1}.
		\end{displaymath}
		$Z_k$ follows the following recursive equation
		\begin{displaymath}
			Z_k = Z_{k|k-1} + C_{T_k}'R_{T_k}^{-1}C_{T_k} = Z_{k|k-1} + \sum_{s_i\in V_{T_k},i\neq 0}\frac{C_iC_i'}{r_i}, 	
		\end{displaymath}
		where
		\begin{displaymath}
			Z_{k|k-1} = (AZ_{k-1}^{-1}A' + Q)^{-1}.
		\end{displaymath}
	\end{frame}


	\begin{frame}{Deterministic Formulation}
		Determine the transmission tree that minimizes the trace of the covariance matrix $P_k$ under the constraints that the overall transmission energy consumption is lower than a prescribed maximum
		energy budget $\mathcal E_d$, i.e.
		\begin{align*}
			\min_{T_k\in \mathcal T }\;\;&trace(P_k) \\
			&   \mathcal E(T_k)\leq\mathcal E_d, \;
		\end{align*}
		\pause
		It is a hard combinatorial problem. Exhaustive search is usually not computational feasible for even moderate large network.
	\end{frame}

	\begin{frame}{Stochastic Sensor Selection}
		\begin{itemize}
			\item Instead of choosing a optimal transmission tree $T_k$, we assign each tree a probability $\pi_{k,T}$.
			\item Let $\delta_{k,T}$ to be the event that tree $T$ is picked at time $k$.
			\item Define
				\begin{displaymath}
					p_{k,i}=\sum_{s_i\in V_T} \pi_{k,T}, \gamma_{k,i}=\sum_{s_i\in V_T}\delta_{k,T}.	
				\end{displaymath}
			\item The covariance matrix $P_k$ and information matrix $Z_k$ satisfy the following matrix recursions:
				\begin{displaymath}
					\begin{split}
						P_k &= P_{k|k-1} - \sum_{T\in\mathcal T}\delta_{k,T}  P_{k|k-1}C_T'(C_TP_{k|k\!-\!1}C_T'\!+\!R_T)^{-1}C_TP_{k|k\!-\!1},\\
						Z_k & = Z_{k|k-1} + \sum_{i=1}^m \gamma_{k,i} \frac{C_i'C_i}{r_i},
					\end{split}
				\end{displaymath}
				where 
				\begin{displaymath}
					P_{k|k-1} =AP_{k-1}A' + Q, Z_{k|k-1} =(AZ_{k-1}^{-1}A' + Q)^{-1}. 
				\end{displaymath}
		\end{itemize}
	\end{frame}

	\begin{frame}{Stochastic Formulation}
		\begin{align*}
			\min_{\pi_{k,T}}\;\;&trace(\mathbb EP_k) \\
			&   \mathbb E\mathcal E(T) = \sum_{T\in \mathcal T} \pi_{k,T}\mathcal E(T)\leq\mathcal E_d, \\
			&\pi_{k,T}\geq 0,\, \sum_{T\in\mathcal T} \pi_{k,T} = 1.
		\end{align*}
		\begin{itemize}
			\item The main advantage of this formulation is that instead of optimizing over a discrete space $\mathcal T$, we are optimizing over $\pi_{k,T}$, which is continuous and also convex.
			\item  However there are two problems.
			\item It is hard to write $\mathbb EP_k$ as an explicit function of $\pi_{k,T}$.
			\item The number of optimization variables may not be polynomial with respect to the number of nodes since $|\mathcal T|$ is usually large.
		\end{itemize}
	\end{frame}

	\begin{frame}{Upper and Lower Bounds}
		\begin{theorem}
			Let $L_0 =  U_0 = P_0$ and
			\begin{align*}
				L_k& = \left(L_{k|k-1} ^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_iC_i'}{r_i}\right)^{-1},\\
					U_k &= U_{k|k-1} - \sum_{T\in\mathcal T}\pi_{k,T}  U_{k|k-1}C_T'(C_TU_{k|k-1}C_T'+R_T)^{-1}C_TU_{k|k-1},
			\end{align*}
			where
			\begin{displaymath}
				L_{k|k-1} = AL_{k-1}A' + Q,\, U_{k|k-1} = AU_{k-1}A'+Q.
			\end{displaymath}
			Then the following inequality holds
			\begin{displaymath}
				U_k \geq \mathbb EP_k  \geq L_k.
			\end{displaymath}
		\end{theorem}
		$L_k$ only depends on the marginal probability $p_{k,i}$!
	\end{frame}

	\begin{frame}{Reformulation}
		\begin{align*}
			\min_{p_{k,i}}\;\;&trace(L_k) \\
			&L_k = \left(L_{k|k-1}^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_i'C_i}{r_i}\right)^{-1},  \\
			&L_{k|k-1} = AL_{k-1}A' + Q, \\
			&\sum_i c_ip_{k,i}\leq \mathcal E_d,\\
			&0\leq p_{k,i}\leq 1,\qquad i=1,\ldots,m,\\
			&p_{k,i}\leq p_{k,j},\qquad \textrm{if $j$ is parent of $i$}.\\
		\end{align*}
	\end{frame}

	\begin{frame}{Implementation}
		\begin{itemize}
			\item Each sensor knows its own $p_{1,i},\ldots,p_{k,i}$ for a long time horizon.
			\item It also knows its children's $p_{1,i},\ldots,p_{k,i}$.
			\item Each sensor is equipped with the same random number generator and same seed at time $0$.
			\item At time $k$, each sensor draws a random number $\alpha_k$ from the random number generator.
			\item The sensors compare $\alpha_k$ with its own $p_{k,i}$ as well as its children's $p_{k,i}$ to decide whether to wait for its children or transmit directly or not transmit at all.
			\item Since $\alpha_k$ is drawn from the same RNG, all the sensors will agree on the same topology.
		\end{itemize}
	\end{frame}

	\begin{frame}{Illustrative Example}
		\begin{itemize}
			\item We consider a 2D defusion process in a closed region.
			\item System model (continuous time):
\begin{displaymath}
    \frac{\partial u}{\partial t} = \alpha \left(\frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2}\right),
\end{displaymath}
with boundary conditions
\begin{displaymath}
  \left.\frac{\partial u}{\partial x_1}\right|_{t,0,x_2}= \left.\frac{\partial u}{\partial x_1}\right|_{t,l,x_2}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,0}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,l}= 0,
\end{displaymath}
\item We use the discretized model.
\item 20 random placed sensors.
		\end{itemize}
	\end{frame}
	

	\begin{frame}{Illustrative Example}
		\begin{figure}[<+htpb+>]
			\begin{center}
				\includegraphics[width=0.6\textwidth]{figure2.eps}
			\end{center}
			\caption{Topology}
		\end{figure}
	\end{frame}

	\begin{frame}{Illustrative Example}
		\begin{figure}[<+htpb+>]
			\begin{center}
				\includegraphics[width=0.6\textwidth]{figure1.eps}
			\end{center}
			\caption{Evolution of the trace of $U_k$, $L_k$ and $EP_k$}
		\end{figure}
\end{frame}

	\begin{frame}{Illustrative Example}
\begin{figure}[<+htpb+>]
  \begin{center}
    \includegraphics[width=0.6\textwidth]{figure3.eps}
  \end{center}
  \caption{Evolution of the trace of $P_k$ given by the Optimal Deterministic Schedule, Samples of $P_k$ for Stochastic Schedule, $EP_k$ for Stochastic Schedule}
  \end{figure}
	\end{frame}

	\begin{frame}{Conclusion}
		In this presentation,  we considered the sensor selection problem.
		\begin{itemize}
			\item  A convex optimization framework based on stochastic sensor selection scheme is provided
			\item  A numerical example is given to illustrate the performance of the proposed strategy.
		\end{itemize}
		Possible future works:
		\begin{itemize}
			\item Distributed sensor selection strategies
			\item Robust sensor selection strategies in case of lossy networks
			\item Generalization to general graphs.
		\end{itemize}
	\end{frame}

	\begin{frame}
		\begin{center}
			\Huge{ Thank you for your time!!\\
			Questions?}
		\end{center}
	\end{frame}

	\end{document}

