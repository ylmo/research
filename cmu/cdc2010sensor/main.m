%clc
%clear
rand('twister',sum(clock)*100)

%generate network graph
gridlength = 5;
n = gridlength^2 ;
m = 20;
xaxis = (gridlength-1) * rand(1,m) + 1;
yaxis = (gridlength-1) * rand(1,m) + 1;
xaxis(1) = gridlength/2 + 0.5;
yaxis(1) = gridlength/2 + 0.5;
graph = zeros(m);
for i = 1:m
    for j = 1:m
        graph(i,j) = (xaxis(i)-xaxis(j))^2 + (yaxis(i)-yaxis(j))^2;
    end
end
baseenergy = 1;
graph = graph + baseenergy;

%generate A matrix for heat transfer system
coef = 0.1;
A = eye(n);
for i = 1:gridlength
    for j = 1:gridlength
        k = (i-1)*gridlength + j;
        if i >= 2
            l = (i-2)*gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if i <= gridlength - 1
            l = i * gridlength + j;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j >= 2
            l = (i-1)*gridlength + j - 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
        if j <= gridlength - 1
            l = (i-1)*gridlength + j + 1;
            A(k,k) = A(k,k) - coef;
            A(k,l) = coef;
        end
        
    end
end

%generate C matrix
C = zeros(m,n);
for i = 1:m
    x = xaxis(i);
    y = yaxis(i);
    xgrid = floor(x);
    ygrid = floor(y);
    for j = 0:1
        for k = 0:1
            l = (xgrid+j-1) * gridlength + ygrid + k;
            C(i,l) = (1-abs(xgrid+j-x))*(1-abs(ygrid+k-y));
        end
    end
end

%generate noise
Q = eye(n);
R = eye(m);
Sigma = eye(n);
L = Sigma;

%generate graph

MST = graphminspantree(sparse(graph));

[dist path parent] = graphshortestpath(MST+MST',1,'directed',false);

cost = zeros(1,m);
for i = 2:m
    cost(i) = graph(i,parent(i));
end
 
%optimization
horizon = 20;
prob = zeros(horizon+1,m);
threshold = 10;
for k = 1:horizon
    Ltmp = A*L(:,:,k)*A'+Q;
    Ztmp = Ltmp^(-1);
    cvx_begin
    cvx_quiet(true)
    variable p(1,m);
    variable Z(n,n) symmetric;
    minimize (trace_inv(Z));
    subject to
    Z == semidefinite(n);
    Z == Ztmp + C'*diag(p)/R*C;
    p(1) >= 0;
    p(1) <= 1;
    for i = 2:m,
        p(i) >= 0;
        p(i) <= 1;
        p(i) <= p(parent(i));
    end
    p*cost' <= threshold;
    cvx_end
    L(:,:,k+1) = inv(Z);
    prob(k+1,:) = p;
end

%upper bound
U = Sigma;
for k = 1:horizon
    Utmp = A*U(:,:,k)*A' + Q;
    [probtmp indextmp] = sort(prob(k+1,:));
    Ctmp = C;
    Rtmp = R;
    ptmp = probtmp(1);
    Utmp = Utmp - ptmp * Utmp* Ctmp' * inv(Ctmp*Utmp*Ctmp'+Rtmp)*Ctmp * Utmp;
    for i = 2:m
        Ctmp = C(indextmp(i:m),:);
        Rtmp = R(indextmp(i:m),indextmp(i:m));
        ptmp = probtmp(i) - probtmp(i-1);
        Utmp = Utmp - ptmp * Utmp* Ctmp' * inv(Ctmp*Utmp*Ctmp'+Rtmp)*Ctmp * Utmp;
    end
    U(:,:,k+1) = Utmp;
end
%simulation
P = Sigma;
avgtrace = zeros(1,horizon+1);
avgtrace(1) = trace(P);
for sample = 1:1000
    for k = 1:horizon
        Ptmp = A*P(:,:,k)*A' + Q;
        Ztmp = inv(Ptmp);
        alpha = rand();
        gamma = (alpha <= prob(k+1,:));
        Ztmp = Ztmp + C'*diag(gamma)/R*C;
        P(:,:,k+1) = inv(Ztmp);
        avgtrace(k+1) = avgtrace(k+1) + trace(P(:,:,k+1));
    end
end
avgtrace(2:horizon+1) = avgtrace(2:horizon+1)/1000;
for k = 1:horizon+1
    uppertrace(k) = trace(U(:,:,k));
    lowertrace(k) = trace(L(:,:,k));
    sampletrace(k) = trace(P(:,:,k));
end


%display result
% hold on
maxtmp = max(prob(horizon+1,2:m));
mintmp = min(prob(horizon+1,2:m));
for i = 2:m
    j = parent(i);
    ptmp = prob(horizon+1,i);
    tmp = 0.05 + 0.9 * (ptmp - mintmp)/(maxtmp - mintmp);
    colortmp = [tmp 1-tmp 0];
    plot([xaxis(i)-1 xaxis(j)-1],[yaxis(i)-1 yaxis(j)-1],'k','LineWidth',2,'color',colortmp);
    axis equal
    axis([0 4 0 4])
    xlabel('x_1','fontsize',12,'fontweight','b')
end
plot (xaxis(1)-1,yaxis(1)-1,'bo','LineWidth',3)
plot (xaxis(2:m)-1,yaxis(2:m)-1,'bo','LineWidth',3)
% hold off