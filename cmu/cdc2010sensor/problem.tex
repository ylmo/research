Consider the following LTI system whose state-space representation
is  given by
\begin{equation}
    x_{k+1} = Ax_k + w_k
  \label{eq:systemdescription}
\end{equation}
where:  $x_k \in \mathbb R^n$ represents the state, $w_k$ disturbances and $x_0$ the initial state, all of them
assumed i.i.d. Gaussian random vectors. In particular  $x_0 \sim \mathcal
N(0,\;\Sigma)$ and $w_k \sim \mathcal N(0,\;Q)$, where $\Sigma,\,Q
\geq 0$ are positive semidefinite matrices.

A wireless sensor network composed of $m$ sensing devices $s_1,\ldots,s_m$ and one fusion
center $s_0$ is used to monitor the state of system \eqref{eq:systemdescription}.
The measurement equation is given by
\begin{equation}
    y_{k} = C x_k + v_k,
  \label{eq:sensordescription}
\end{equation}
where $y_k = [y_{k,1} , y_{k,2} ,\ldots, y_{k,m}]' \in \mathbb R^m$ is the
measurement vector.\footnote{The $'$ on a matrix always means transpose.} Each element
$y_{k,i}$ represents the measurement of sensor $i$ at time $k$ and
$v_k \sim \mathcal N(0,\;R)$ is the measurement noise, assumed to be independent of
$x_0$ and $w_k$. We also assume $R = diag(r_1,\ldots,r_m) $ diagonal,
meaning that the measurement noise at each sensor is independent of each other
for each $r_i > 0$. Finally, $C = \left[C_1',\ldots,C_m'\right]'$ denotes the observation
matrix.

An oriented communication graph
$G = \{V\,,E\}$ is introduced in order to model the communication amongst the nodes,
whose vertex set $V = \{s_0,\,s_1,\ldots,s_m\}$ contains all sensor nodes including
the fusion center. The set of edges $E \subseteq V \times V$ represents the available
connections between pairs of nodes, i.e.  $(i,j) \in E$ implies that the node $s_i$ can
send information to the node $s_j.$ Moreover, it is assumed that each node of the sensor
network acts as a gateway, i.e. at every time instant it is able to aggregate its own measurement to the received ones and forward to its parent.
%
%to a specific allowed node its own measurements along with all data received from other nodes
%which need to be there dird in a single packet.

In this paper we assume that for every sensor there exists one and only one path toward
the fusion center, i.e. the sensor network has a directed tree topology. Moreover, we also
assume that each $i$-th link has associated a weight $c(e_{i,j})$ which indicates the
energy consumed by $s_i$ to directly transmit a packet to $s_j$. For notational simplicity,
we will sometimes abbreviate $c(e_{i,j})$ as $c_i$, $i= 1,\ldots,m$,
because, in the assumed topology, each sensor node has only one outgoing edge.

%\begin{remark}
%  The tree topology assumption may be a restrictive hypothesis in the general case where usually one sensor can communicate with several nearby nodes. However it is worth to remark that typical communication network graphs can be approximated by a collection of "representative" spanning trees (e.g. the first $m$ spanning trees of the spanning tree enumeration \cite{EnumerateSpanningTrees}).
%\end{remark}

Because sensor measurements usually contain redundant information it is desirable to only use a subset of sensors at each sampling time in order to reduce energy consumption. However, in a tree topology, we cannot arbitrary select subsets of nodes. On the contrary,
we are forced to select nodes (and connections) in such a way that for each sensor node
there exists an allowed path to the fusion node. As a result, we define a transmission
topology of $G$ as a possible subtree $T = \{V_T\,,E_T\}$ of $G$ with fusion center
$s_0 \in V_T$. Correspondingly, $V_T\subseteq V$ will denote the selected subset of sensors and
$E_T$ the set of communication links used by the sensors to transmit the observations back to
the fusion center. We also denote by $\mathcal T$ the set of all possible transmission topologies
for $T$, i.e. the set of all possible subtrees of $G$ containing $s_0$.

It is straightforward to see that, for a transmission tree $T$, the total energy
required for the transmission over the network is given by\footnote{Here we have to assume
that $cost(e_{i,j})$ is constant regardless of number of observations contained in the packet.
This is realistic in many cases. In fact, if measurements are of simple type (e.g. low
precision scalar values), then the transmission overhead (e.g. header, hand shaking protocol)
dominates the payload.}
\begin{displaymath}
\vspace{-0.1 cm}
  \mathcal E(T) = \sum_{e \in E_T} c(e). \vspace{-0.1 cm}
\end{displaymath}

Suppose now that at time $k$ a particular transmission tree $T$ is used with
 $V_T = \{s_0,\,s_{i_1},\ldots,s_{i_j}\}$ and consider matrices
\vspace{-0.1 cm}
\begin{equation}
  C_T \triangleq [C_{i_1}',\,C_{i_2}',\ldots,C_{i_j}']',\,R_T \triangleq diag(r_{i_1},\ldots,r_{i_j}). \vspace{-0.1 cm}
\end{equation}
%
It is easy to prove that the Kalman filter with a time-varying observation matrix $C_T$
is still optimal for sensor selection problems. Hence, once the fusion center
collects all observations at time $k$, it will use the Kalman filter to compute
the optimal estimate of current state $\hat x_{k}$ and  the covariance matrix $P_{k}$
of the estimation error, given by the following expressions
\begin{align}\label{eq:Kalman_filter}
    \hat x_{k} &= K_k (y_k -C_T\hat x_{k|k-1}) + \hat x_{k|k-1}, \\[-0.1 cm]
    P_{k}      &= P_{k|k-1}  -  K_k C_TP_{k|k-1},
\end{align}
where
\begin{align}
    \hat x_{k|k-1} &= A \hat x_{k-1},  P_{k|k-1}  = A P_{k-1} A'  + Q , \\[-0.1 cm]
    K_k        &= P_{k|k-1} C_T' (C_TP_{k|k-1} C_T'  + R_T)^{ - 1}.
\end{align}
%
Consider now the Information form of the Kalman filter characterized by the information matrix \vspace{-0.1 cm}
\begin{equation}
  Z_k \triangleq P_k^{-1}. \vspace{-0.1 cm}
  \label{eq:defofinformationmatrix}
\end{equation}
which satisfies the following matrix recursions
\begin{equation}
  Z_k = Z_{k|k-1} + C_T'R_T^{-1}C_T,
  \label{eq:informationfilter}
\end{equation}
where
\begin{equation}
  Z_{k|k-1} =   (AZ_{k-1}^{-1}A'+Q)^{-1}.
\end{equation}
Being $R$ diagonal, it is easy to see that \eqref{eq:informationfilter} can be rewritten as
\begin{equation}
  Z_k = Z_{k|k-1} + \sum_{s_i \in V_T,\, s_i\neq s_0} \frac{C_iC_i'}{r_i}.
\end{equation}
%%%%%%%%%%%%%%%
Then, the optimal sensor selection problem under investigation can be formulated as follows:
\begin{prob}[Sensor Selection over Transmission Tree]\label{opt_dyn}
Determine the transmission tree that minimizes the trace of the
covariance matrix $P_k$ under the constraints that
the overall transmission energy consumption is lower than a prescribed maximum
energy budget $\mathcal E_d$, i.e.
\begin{align*}
  \min_{T\in \mathcal T }\;\;&trace(P_k) \\
&   \mathcal E(T)\leq\mathcal E_d, \;
\end{align*}
\end{prob}
\begin{remark}\label{computational_prob}
Observe that solving Problem~\ref{opt_dyn} with an exhaustive search over the
space $\mathcal T$ is a very hard problem even for a small sensor network. To give an idea
of the size of the problem, consider the case when $G$ is a perfect binary tree of $63$ nodes:
the number of candidate solutions, i.e. all possible transmission graphs $T$, is of the order
$10^{11}$.
\end{remark}
\begin{remark}
In Problem 1 we require that at each sampling instant the total energy cost does not exceed
a certain prescribed energy budget. In real applications other different constraints might
appear (e.g. requirements on the sensor lifetime). However, it can be shown (see e.g.
\cite{Joshi}) that many of these constraints are linear or convex and therefore can
be easily integrated in our formulation.
\end{remark}
In order to address the impracticality of exhaustive searches we propose a stochastic selection strategy.
