In order to show the effectiveness of the proposed method we have
applied our stochastic sensor selection algorithm to a numerical
example in which a sensor network is deployed to monitor a
diffusion process in a planar closed region. The model of the
diffusion process is given by
\begin{equation}
    \frac{\partial u}{\partial t} = \alpha \left(\frac{\partial^2 u}{\partial x_1^2} + \frac{\partial^2 u}{\partial x_2^2}\right),
    \label{eq:diffussionpdfcont}
\end{equation}
with boundary conditions
\begin{equation}
  \left.\frac{\partial u}{\partial x_1}\right|_{t,0,x_2}= \left.\frac{\partial u}{\partial x_1}\right|_{t,l,x_2}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,0}= \left.\frac{\partial u}{\partial x_2}\right|_{t,x_1,l}= 0,
\end{equation}
where  $x_1,x_2$ indicate the coordinates of the region; $u(t,x_1,x_2)$ denotes the
temperature at time $t$ at location $(x_1,x_2)$ and $\alpha$ indicates the speed of the
diffusion process. We use the finite difference method to discretize this model and
assume that the planar region is a square of side $l$ meters. As a result, we obtain a $N \times N$
grid with grid step $h = l/(N-1)$. Finally the system is discretized in time using a sampling frequency of $1$ Hz. By applying the finite difference method,
equation \eqref{eq:diffussionpdfcont} becomes
\begin{align} \label{eq:diffusiondisc}
    &u(k+1,i,j) - u(k,i,j) = \alpha/h^2 [u(k,i-1,j)+u(k,i,j-1) \nonumber\\
    &+u(k,i+1,j)+u(k,i,j+1)-4u(k,i,j)],
\end{align}
where $u(k,i,j)$ denotes the temperature at time $k$ at location $(ih,jh)$.
\begin{remark}
If either $i$ or $j$ is greater than $N$ or lower than $0$, then the location is outside
the grid. Then, $u(k,i,j)$ is replaced by the value of its nearest neighbor in the grid
as a consequence of the imposed boundary conditions.
\end{remark}
If we group all the temperature values at $k$ in the vector $U_k = [u(k,0,0),\ldots,u(k,0,N-1),
u(k,1,0),\ldots,u(k,N-1,N-1)]^T$, we can write the evolution of the discretized system as
$U_{k+1} = A U_{k}$, where the $A$ matrix can be calculated from \eqref{eq:diffusiondisc}.
When process noise is present, $U_k$ evolves according to
\begin{equation} \label{sys}
    U_{k+1} = A U_{k} + w_k,
\end{equation}
with $w_k \in \mathcal N(0,\;Q)$. We assume also that the fusion center is located in the center of the room at position $(2,2)$, that the $m$ sensors are randomly distributed
in the region and that each sensor measures a linear combination of temperature of the
grid around it. Define $\Delta \hat x_1 =\hat x_1-i$ and $\Delta \hat x_2 =\hat x_2-j$. Then,
by denoting with $(\hat x_1,\hat x_2)$ the location of sensor
$\hat s$  such that $\hat x_1 \in [i,i+1)$ and $\hat x_2 \in [j,j+1)$ the measurements
provided by the sensors can be characterized as
\begin{equation}
    \begin{split}
     y_{\hat s} = \left[\right.&(1-\Delta \hat x_1)(1-\Delta \hat x_2) u(k,i,j)+ \\ &\Delta \hat x_1(1-\Delta \hat x_2) u(k,i+1,j)+\\
    &(1-\Delta \hat x_1)\Delta \hat x_2 u(k,i,j+1)+ \\ &\Delta \hat x_1\Delta \hat x_2 u(k,i+1,j+1)\left.\right]/h^2+ noise  .
    \end{split}
    \label{eq:diffusionmeasure}
\end{equation}
Moreover, by indicating with $Y_k$ the vector of all the measurements at time $k$,
it follows that:
\begin{equation}\label{sensors}
Y_k = C U_k + v_k,
\end{equation}
where $v_k$ denotes the measurement noise at time $k$ assumed to have normal distribution
$\mathcal N(0,\;R)$ and $C$ is the observation matrix that can be computed from
\eqref{eq:diffusionmeasure}. Finally, we assume that the sensor network is fully connected
and that the communication cost from sensor $i$ to $j$ is given by
\[
cost(e_{i,j}) = c+d_{i,j}^2
\]
with $d_{ij}$ the Euclidean distance from sensor $i$ to sensor $j$ and $c$
a constant related to energy consumption\footnote{$c$ models the fact that as the distance
goes to zero the communication cost is non negligible even when sensors are infinitely close}.
Then, we can compute the minimum spanning tree of the graph and force the sensors to 
communicate with its parents accordingly. For the simulations, we have imposed the following
values to the parameters:
\begin{itemize}
  \item $m = 20$, $\alpha = 0.1 \; m^2/s$, $l=4\; m$ and $N=5$.
\item $Q = I $, $R = I $, $\Sigma = 4I $.
\item $\mathcal E_d = 5$,$c=1$.
\end{itemize}

Figure~\ref{fig:evo} shows the evolution of the trace of the upper-bound, lower-bound and
covariance of the expected estimation error. The latter is computed by averaging $100$ sample
paths. It is easy to see that the upper-bound is about $15\%$ higher than the lower-bound,
which means that the lower-bound is quite tight. Figure~\ref{fig:evo2} shows the trace of
$P_k$ for the optimal solution of Problem~\ref{opt_dyn}, which is found by exhaustive search,
together with the trace of $P_k$ from a sample path of the stochastic schedule and
$EP_k$ of the stochastic schedule. The optimal deterministic schedule is worse than $EP_k$,
which highlights the fact that the stochastic formulation, as discussed in
Remark~\ref{remark:stochasticvsdeterministic}, provides better results than its
deterministic counterpart in the expected sense. It is also worth noticing that the
computation time for solving Problem~\ref{randoptlowerfinal} is 1.8 seconds on an
Intel Core2 2GHz, achieved by
using Matlab R2006b and CVX 1.2, while the exhaustive search takes 104 seconds to be completed.
