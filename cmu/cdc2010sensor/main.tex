%
The main idea behind the proposed method is to optimally design
the probability, denoted as $\pi_{k,T}$, that a certain transmission tree $T$ is selected
at time $k$. This approach allows to substitute the binary variables in Problem 1 with
corresponding continuous ones and thus to provide a natural relaxation method. The proposed
approach can then be summarized as follows: first, we determine the
probabilities $\pi_{k,T}, \forall k>0, \forall T \in
{\mathcal{T}}$ that minimize an objective function related to the
expected covariance $\mathbb EP_k$ of the estimation error. Then, at each
time instant $k$ we randomly select a transmission tree $T \in
{\mathcal{T}}$ based on the probabilities $\pi_{k,T},
\forall T \in {\mathcal{T}}.$

%
The remainder of the section is organized as follows. In the first subsection,
we will introduce the used notation and the main features of our algorithm. Then, in the
second subsection, we will show how to compute upper and lower bounds of $\mathbb EP_k$
and how to relax the random sensor selection design to a convex optimization problem.
Finally, in the last subsection, we will discuss how to implement the proposed sensor
selection strategy so as to avoid communication overhead.
%
\subsection{Description of Random Transmission Tree Selection Algorithm}
Suppose that at each time $k$, we randomly select a tree $T$ from $\mathcal T$  and that
each sensor in $T$ transmits its observation back to the fusion node according to the
topology $T$. Let $\pi_{k,T}$ be the probability that the transmission tree $T$ is selected
at time $k$. \footnote{Note that $T_0 = \{\{s_0\},\phi\}$ also belongs to $\mathcal T$.
Hence $\pi_{k,T_0}$ is the probability that no sensors are used at time $k$.} Then, we may
define the marginal probability
\vspace{-0.1 cm}
\begin{equation}
  p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T} \vspace{-0.1 cm}
\end{equation}
that sensor $i$ is selected at time $k$. Moreover, we can introduce the binary random
variable $\delta_{k,T}$ such that $\delta_{k,T}=1$, if the transmission tree $T$ is selected
at time $k$, and $\delta_{k,T}=0$ otherwise. Similarly, we can define the binary random
variable $\gamma_{k,i}$ to be $1$ if sensor $i$ is selected at time $k$ and $0$ otherwise.
Hence, the covariance matrix $P_k$ and information matrix $Z_k$ satisfy the following
matrix recursions:
\begin{displaymath}
  \begin{split}
  P_k &= P_{k|k-1}\\
  &- \sum_{T\in\mathcal T}\delta_{k,T}  P_{k|k-1}C_T'(C_TP_{k|k\!-\!1}C_T'\!+\!R_T)^{-1}C_TP_{k|k\!-\!1},\\
  Z_k & = Z_{k|k-1} + \sum_{i=1}^m \gamma_{k,i} \frac{C_i'C_i}{r_i}
  \end{split}
\end{displaymath}
where \vspace{-0.2 cm}
\begin{displaymath}
  P_{k|k-1} =AP_{k-1}A' + Q, Z_{k|k-1} =(AZ_{k-1}^{-1}A' + Q)^{-1}. where \vspace{-0.2 cm}
\end{displaymath}
Since transmission trees are randomly selected, $P_k$ becomes a random matrix. Thus, we
can only minimize the covariance matrix $\mathbb EP_k$ of the expected one-step ahead
estimation error and require that the expected energy consumption does not exceed
the designated threshold $\mathcal E_d$. These considerations lead to the formulation of
the following optimization problem
\begin{prob}[Random Transmission Tree Selection]\label{randopt}
\vspace{-0.2 cm}
\begin{align*}
  \min_{\pi_{k,T}}\;\;&trace(\mathbb EP_k) \\[-0.1 cm]
  &   \sum_{T\in \mathcal T} \pi_{k,T}\mathcal E(T)\leq\mathcal E_d, \\[-0.1 cm]
  &\pi_{k,T}\geq 0,\, \sum_{T\in\mathcal T} \pi_{k,T} = 1.
\end{align*}
\end{prob}
\vspace{-0.2 cm}
\begin{remark}
  \label{remark:stochasticvsdeterministic}
The main difference between Problem~\ref{randopt} and Problem~\ref{opt_dyn} is in
the set over which the optimization takes place. In (\ref{opt_dyn}) the optimization takes place over the discrete space $\mathcal T$ while in (\ref{randopt}) optimization is performed over a continuous space, being $\pi_{k,T}$ continuous variables.
This brings two advantages. First, it can be seen that if we add the condition that
$\pi_{k,T}$ is either  $0$ or $1$, Problem~\ref{randopt}  and Problem~\ref{opt_dyn}
are equivalent. Hence, the optimal cost of Problem~\ref{randopt} will be a lower-bound to
the optimal cost of Problem~\ref{opt_dyn}, having the latter more choices. As a result,
the stochastic sensor selection strategy can actually improve the performance
(at least in the expected sense). The second advantage is that the feasible set of $\pi_{k,T}$
is convex having removed the binary constraint.
\end{remark}

Two drawbacks still arise in the above formulation which make
Problem~\ref{randopt} intractable:
\begin{enumerate}
  \item It is usually difficult to write $\mathbb EP_k$ as an explicit function of
  $\pi_{k,T}$.\footnote{The reader can refer to \cite{sinopoli} for more details.}
  \item Because $|\mathcal T|$ is large, the number of optimization variables and constraints
  may not be polynomial with respect to the number of nodes.
\end{enumerate}
In the next subsection, we will devise a relaxation method that allows to
overcome the above drawbacks.
\subsection{Toward a Convex Optimization Formulation}
%
As a first step to relax Problem~\ref{randopt} we can define an upper-bound $U_k$ and a
lower-bound $L_k$ for $\mathbb EP_k$ by means of the following results:
%
\begin{theorem}\label{theorem:lowerbound}
  Let $L_0 =  P_0$ and
  \begin{equation}
      L_k = \left(L_{k|k-1} ^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_iC_i'}{r_i}\right)^{-1},
  \end{equation}
  where
  \begin{equation}
    L_{k|k-1} = AL_{k-1}A' + Q.
  \end{equation}
  Then, the following inequalities hold true:
  \begin{equation}
    L_k^{-1} \geq \mathbb EZ_k,\, \mathbb EP_k \geq (\mathbb EZ_k)^{-1} \geq L_k.
  \end{equation}
\end{theorem}
%
\begin{theorem}\label{theorem:upperbound}
  Let $U_0 = P_0$ and
  \begin{displaymath}
    \begin{split}
    U_k& = U_{k|k-1}\\[-0.1 cm]
&    - \sum_{T\in\mathcal T}\pi_{k,T}  U_{k|k-1}C_T'(C_TU_{k|k-1}C_T'+R_T)^{-1}C_TU_{k|k-1},
    \end{split}
  \end{displaymath}
  where
  \begin{displaymath}
    U_{k|k-1} = AU_{k-1}A'+Q.
  \end{displaymath}
  Then, the following inequality holds true:
  \begin{displaymath}
  U_k \geq \mathbb EP_k.
  \end{displaymath}
\end{theorem}
The proofs of both Theorems involve the use of the Jensen's inequality and are omitted
due to space limitations.

Compared to $\mathbb EP_k$, it can be observed that the matrices $U_k$ and $L_k$ are deterministic and explicit
functions of $\pi_{k,T}$.  Notice also that, while $U_k$ depends on the variables $\pi_{k,T}$,
the number of which may be not polynomial in the number of sensors, $L_k$ only depends on
the marginal probability $p_{k,i}$. Then, if we minimize the trace of the lower-bound $L_k$
instead of the trace $\mathbb EP_k,$ the problem simplifies as follows
%
\begin{prob}[Lower-Bound for Random Tree Selection]\label{randoptlower}
\begin{align*}
  \min_{\pi_{k,T},\,p_{k,i}}\;\;&trace(L_k) \\[-0.1 cm]
  &L_k = \left(L_{k|k-1}^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_i'C_i}{r_i}\right)^{-1},  \\[-0.1 cm]
    &L_{k|k-1} = AL_{k-1}A' + Q, \\[-0.1 cm]
  &\sum_{T\in \mathcal T} \pi_{k,T}\mathcal E(T)\leq\mathcal E_d, \\[-0.1 cm]
  &\pi_{k,T}\geq 0,\,\sum_{T\in\mathcal T} \pi_{k,T} = 1,\\[-0.1 cm]
  &p_{k,i} \triangleq \sum_{T\in \mathcal T, s_i \in V_T} \pi_{k,T}.
\end{align*}
\end{prob}
\vspace{-0.2 cm}
\begin{remark}
It is worth remarking that, once computed the marginal probability $p_{k,i}$ that minimizes
the lower-bound, we can easily find the corresponding  $\pi_{k,T}, \forall T \in \mathcal T$,
and then compute the upper-bound $U_k$ without solving an additional optimization problem. Computation of the upper bound is important as it allows to assess the tightness of the lower-bound.
\end{remark}
The last step to undertake in order to make the problem tractable is the removal of
the variables $\pi_{k,T}$ from the constraints. To this end, the following propositions can
be shown to hold true:
\begin{proposition} The energy cost of a given collection of tree selection probabilities
$\pi_{k,T}, \forall T \in {\mathcal{T}}$ is a linear function of the resulting marginal
probability:
  \begin{equation}
    \sum_{T \in \mathcal T} \pi_{k,T} \mathcal E(T)= \sum_{i = 1}^m c_i p_{k,i}.
  \end{equation}
\end{proposition}
\begin{proposition}
  If $p_{k,i}\in [0,1]$ with
  \begin{equation}
    p_{k,i} \leq  p_{k,j},\qquad \textrm{if $j$ is parent of $i$,}
  \end{equation}
  then there exists at least one collection of tree selection probabilities $\pi_{k,T},
  \forall T \in \mathcal{T}$\footnote{It is worth noticing that in general there may
  exist more than one set of $\pi_{k,T}, \forall T \in \mathcal{T} $ with the same
  marginal probabilities.} such that
  \begin{align*}
  \pi_{k,T}\geq 0,\, &\sum_{T\in\mathcal T} \pi_{k,T} = 1,\, p_{k,i} \triangleq \sum_{T\in
  \mathcal T, s_i \in V_T} \pi_{k,T}.
  \end{align*}
\end{proposition}
%
The above Propositions allow us to rewrite, without loss of generality,
Problem~\ref{randoptlower} as follows
%
\begin{prob}[Lower-Bound for Random Tree Selection]\label{randoptlowerfinal}
\begin{align*}
  \min_{p_{k,i}}\;\;&trace(L_k) \\[-0.1 cm]
  &L_k = \left(L_{k|k-1}^{-1} + \sum_{i=1}^m p_{k,i} \frac{C_i'C_i}{r_i}\right)^{-1},  \\[-0.1 cm]
    &L_{k|k-1} = AL_{k-1}A' + Q, \\[-0.1 cm]
  &0\leq p_{k,i}\leq 1,\,i=1,\ldots,m,\\[-0.1 cm]
  &p_{k,i}\leq p_{k,j},\qquad \textrm{if $j$ is parent of $i$}.\\[-0.1 cm]
\end{align*}
\end{prob}
\vspace{-0.2 cm}
%
The above problem is a convex optimization problem with a number of variables and
constraints which is a polynomial function of the dimension of the original problem, thus
solvable in polynomial time.
