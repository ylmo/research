input TEX;
input boxes;
verbatimtex
%&latex
\documentclass{beamer}
\everymath{\displaystyle}
\begin{document}
etex

beginfig(1);
circleit.theta(btex $x$ etex);
circleit.sa(btex $S_1$ etex);
circleit.sb(btex $S_2$ etex);
circleit.sc(btex $S_3$ etex);
boxit.detector(btex Detector etex);
boxit.algorithm(btex $\hat x = sgn\left[\Lambda(y) - \log\left(\frac{p^-}{p^+}\right)\right]$ etex);

theta.c = (0,0);
sa.c = (2.5cm,1.5cm);
sb.c = (2.5cm,0cm);
sc.c = (2.5cm,-1.5cm);
detector.c = (5cm,0);
algorithm.c= (8.5cm,0);

drawboxed(sa,sb,sc,detector);
drawunboxed(theta,algorithm);

drawarrow theta.c--sa.c cutbefore bpath.theta cutafter bpath.sa;
drawarrow theta.c--sb.c cutbefore bpath.theta cutafter bpath.sb;
drawarrow theta.c--sc.c cutbefore bpath.theta cutafter bpath.sc;

drawarrow sa.c--detector.c cutbefore bpath.sa cutafter bpath.detector;
drawarrow sb.c--detector.c cutbefore bpath.sb cutafter bpath.detector;
drawarrow sc.c--detector.c cutbefore bpath.sc cutafter bpath.detector;

drawarrow detector.c--algorithm.c cutbefore bpath.detector cutafter bpath.algorithm;

label.urt(btex $y_1$ etex, 0.5*(sa.c+detector.c));
label.top(btex $y_2$ etex, 0.5*(sb.c+detector.c));
label.lrt(btex $y_3$ etex, 0.5*(sc.c+detector.c));
endfig;

beginfig(2);
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(1.9cm,1.9cm)--cycle withcolor 0.8 white + 0.2 red;
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(-1.9cm,-1.9cm)--cycle withcolor 0.8 white + 0.2 green;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-2cm)--(0,2cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,2cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.9cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.9cm));
draw (-0.5cm,-0.3cm) withpen pencircle scaled 3pt;
label.lft(btex $y$ etex,(-0.5cm,-0.3cm));
draw (-0.5cm,-0.3cm)--(-0.5cm,0.7cm) dashed evenly;
draw (-0.5cm,0.7cm) withpen pencircle scaled 3pt;
label.lft(btex $y^c$ etex,(-0.5cm,0.7cm));
endfig;

beginfig(3);
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(1.9cm,1.9cm)--cycle withcolor 0.8 white + 0.2 red;
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(-1.9cm,-1.9cm)--cycle withcolor 0.8 white + 0.2 green;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-2cm)--(0,2cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,2cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.9cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.9cm));
draw (-0.5cm,-0.3cm) withpen pencircle scaled 3pt;
label.lft(btex $y^c=y$ etex,(-0.5cm,-0.3cm));
endfig;

beginfig(4);
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(1.9cm,1.9cm)--cycle withcolor 0.8 white + 0.2 red;
fill (-1.9cm,1.9cm)--(1.9cm,-1.9cm)--(-1.9cm,-1.9cm)--cycle withcolor 0.8 white + 0.2 green;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-2cm)--(0,2cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,2cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.9cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.9cm));
draw (-0.5cm,-0.3cm) withpen pencircle scaled 3pt;
draw (-0.5cm,-2cm)--(-0.5cm,2cm) dashed evenly ;
draw (-2cm,-0.3cm)--(2cm,-0.3cm) dashed evenly ;
endfig;

beginfig(5);
fill (-1.9cm,1.9cm)--(1.9cm,1.9cm)--(1.9cm,-1.9cm)--(-1.9cm,-1.9cm)--cycle withcolor 0.8 white + 0.2 green;
fill (0cm,1.9cm)--(1.9cm,1.9cm)--(1.9cm,0cm)--(0cm,0cm)--cycle withcolor 0.8 white + 0.2 red;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-2cm)--(0,2cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,2cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.9cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.9cm));
draw (-0.5cm,-0.3cm) withpen pencircle scaled 3pt;
draw (-0.5cm,-2cm)--(-0.5cm,2cm) dashed evenly ;
draw (-2cm,-0.3cm)--(2cm,-0.3cm) dashed evenly ;
endfig;

beginfig(6);
fill (-1.9cm,1.3cm)--(1.9cm,1.3cm)--(1.9cm,-1.3cm)--(-1.9cm,-1.3cm)--cycle withcolor 0.8 white + 0.2 green;
fill (0cm,1.3cm)--(1.9cm,1.3cm)--(1.9cm,0cm)--(0cm,0cm)--cycle withcolor 0.8 white + 0.2 red;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-1.35cm)--(0,1.35cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,1.35cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.3cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.3cm));
endfig;

beginfig(7);
fill (-1.9cm,0cm)--(0cm,0cm)--(0cm,-1.3cm)--(-1.9cm,-1.3cm)--cycle withcolor  green;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-1.35cm)--(0,1.35cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,1.35cm));
label.llft (btex $Y^+=\emptyset$ etex,(1.9cm,1.3cm));
label.urt (btex $Y^-$ etex,(-1.9cm,-1.3cm));
endfig;

beginfig(8);
fill (-1.9cm,1.3cm)--(1.9cm,-1.3cm)--(1.9cm,1.3cm)--cycle withcolor 0.8 white + 0.2 red;
fill (-1.9cm,1.3cm)--(1.9cm,-1.3cm)--(-1.9cm,-1.3cm)--cycle withcolor 0.8 white + 0.2 green;
drawarrow (-2cm,0)--(2cm,0);
drawarrow (0,-1.35cm)--(0,1.35cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,1.35cm));
label.llft (btex $\hat x = 1$ etex,(1.9cm,1.3cm));
label.urt (btex $\hat x = -1$ etex,(-1.9cm,-1.3cm));
endfig;

beginfig(9);
drawarrow (-2cm,0)--(2cm,0); 
drawarrow (0,-1.35cm)--(0,1.35cm);
label.bot (btex $y_1$ etex,(2cm,0));
label.rt (btex $y_2$ etex,(0,1.35cm));
label.llft (btex $Y^+ = \emptyset$ etex,(1.9cm,1.3cm));
label.urt (btex $Y^- = \emptyset$ etex,(-1.9cm,-1.3cm));
endfig;

beginfig(10);
draw (4cm,0)..(0,2cm)..(-4cm,0)..(0,-2cm)..cycle;
fill fullcircle scaled 3cm shifted (-2cm,0) withcolor (0.8 white + 0.2 green);
fill fullcircle scaled 1cm shifted (-2cm,0) withcolor green;
label(btex $Y^-$ etex,(-2cm,0));
label.bot(btex $d(y,Y^-)\leq n$ etex,(-2cm,-0.5cm));
label.top(btex $\hat x = -1$ etex,(-2cm,0.5cm));
fill fullcircle scaled 3cm shifted (2cm,0) withcolor (0.8 white + 0.2 red);
fill fullcircle scaled 1cm shifted (2cm,0) withcolor red;
label(btex $Y^+$ etex,(2cm,0));
label.bot(btex $d(y,Y^+)\leq n$ etex,(2cm,-0.5cm));
label.top(btex $\hat x = 1$ etex,(2cm,0.5cm));
label.bot(btex $\mathbb R^m$ etex,(0,-1cm));
endfig;

beginfig(11);
draw (4cm,0)..(0,2cm)..(-4cm,0)..(0,-2cm)..cycle;
path a,b,c,d;
a = fullcircle scaled 3cm shifted (-1.2cm,0);
b = fullcircle scaled 1cm shifted (-1.2cm,0);
c = fullcircle scaled 3cm shifted (1.2cm,0);
d = fullcircle scaled 1cm shifted (1.2cm,0);
fill a withcolor (0.8 white + 0.2 green+0.2 red);
fill buildcycle(a,reverse(c)) withcolor 0.8 white + 0.2 green ;
fill b withcolor green;
label(btex $Y^-$ etex,(-1.2cm,0));
label.top(btex $\hat x = -1$ etex,(-1.2cm,0.5cm));
fill buildcycle(a,reverse(c)) rotated 180 withcolor 0.8 white + 0.2 red ;
fill d withcolor red;
label(btex $Y^+$ etex,(1.2cm,0));
label.top(btex $\hat x = 1$ etex,(1.2cm,0.5cm));
drawarrow (1.2cm,0)..(-1.2cm,0) cutbefore d cutafter b;
drawarrow (-1.2cm,0)..(1.2cm,0) cutbefore b cutafter d;
label.top(btex $\leq 2n$ etex,(0,0));
drawarrow (1.2cm,0)..((1.5cm,0) rotated -45 shifted (1.2cm,0)) cutbefore d;
drawarrow (-1.2cm,0)..((1.5cm,0) rotated -135 shifted (-1.2cm,0)) cutbefore b;
label.urt(btex $n$ etex, ((1cm,0) rotated -45 shifted (1.2cm,0)));
label.urt(btex $n$ etex, ((1cm,0) rotated -135 shifted (-1.2cm,0)));
label.bot(btex $\mathbb R^m$ etex,(0,-1.5cm));
endfig;

beginfig(12);
draw (3cm,0)..(0,2cm)..(-3cm,0)..(0,-1.5cm)..cycle;
path b,d;
b = fullcircle scaled 1cm shifted (-2cm,0);
d = fullcircle scaled 1cm shifted (2cm,0);
pair p;
p = (1.5cm,0) rotated 45 shifted (-2cm,0);
fill b withcolor green;
fill d withcolor red;
label(btex $X^-$ etex,(-2cm,0));
label(btex $X^+$ etex,(2cm,0));
drawarrow (2cm,0)..(-2cm,0) cutbefore d cutafter b;
drawarrow (-2cm,0)..(2cm,0) cutbefore b cutafter d;
label.top(btex $\geq 2l+1$ etex,(0,0));
drawarrow p..(-2cm,0) cutafter b;
drawarrow p..(2cm,0) cutafter d;
%label.ulft(btex $\leq n$ etex, ((1cm,0) rotated 45 shifted (-2cm,0)));
%label.urt(btex $\geq n+1$ etex, 0.5(p + (2cm,0)));
label.top(btex $y$ etex,p);
draw p withpen pencircle scaled 4pt;
label.bot(btex $\mathbb R^m$ etex,(0,-1cm));
endfig;
