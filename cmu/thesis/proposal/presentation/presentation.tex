\documentclass{beamer}
\usepackage{pgfpages}
\usepackage{subfigure}
\usepackage{amsfonts}
\usepackage{wasysym}
\usepackage{amsmath}
\usepackage{ifpdf}
\usepackage{booktabs}
\usepackage{tikz}
\usetikzlibrary{matrix,arrows}
\usepackage{pgfplots}
\ifpdf
\DeclareGraphicsRule{*}{mps}{*}{}
\fi

\newlength\figureheight
\newlength\figurewidth
\let\Tiny\tiny

%for handout
\mode<presentation>

\title[Secure CPS]{Secure Detection, Estimation and Control in Cyber-Physical Systems}
\author[Yilin Mo]{Yilin Mo}
\institute[Carnegie Mellon University]{
Department of Electrical and Computer Engineering\\ Carnegie Mellon University\\
}
\date[April 12, 2012]{April 12, 2012}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\begin{document}

\begin{frame}
  \titlepage
\end{frame}


\section{Introduction}

\frame{\tableofcontents}

\begin{frame}{Motivation}
  \begin{itemize}
    \item Cyber-Physical Systems (CPS) refer to the embedding of widespread sensing, computation, communication and control into physical spaces.
    \item Applications: aerospace, chemical processes, civil infrastructure, energy, manufacturing and transportation. \alert{Safety Critical}
    \item Currently, CPSs are usually running in isolated networks: limited access points for the attacker.
    \item The next generation CPS, such as Smart Grid and VANET, will make extensive use of widespread networking: lots of possible entry points.
    \item Stuxnet raised significant concerns on CPS security. 
    \item How to design secure CPS?
  \end{itemize}
\end{frame}

\begin{frame}{Information Security}
  \begin{itemize}
    \item Information security protects the confidentiality, integrity and availability of information.
    \item Tamper-resistant microprocessor, Software attestation, Secure Communication Protocol, etc.
    \item Lack of the model of the physical system: hard to predict the effect of attacks and countermeasures on the physical system.
    \item Difficult to guarantee security for every single device. 
    \item Physical attacks?
  \end{itemize}
\end{frame}

\begin{frame}{System Theory}
  \begin{itemize}
    \item System theory based approaches seek to identify failures of the system and design algorithms which can withstand certain types of failures.
    \item Bad data detection, Robust estimation and control, etc. 
    \item The system theoretic models of failure are usually quite different from the cyber-physical attacks.
    \item Lack of the model of the cyber system
    \item Difficult to prevent an attack from happening
  \end{itemize}
\end{frame}

\begin{frame}{Our Contribution}
  The main focus of this dissertation is secure detection, estimation and control in CPS. In particular, we provide
  \begin{enumerate}
    \item Models of cyber-physical attacks;
    \item Analytical tools to quantify the performance degradation of CPS under the attacks;
    \item Countermeasures to detect the attacks or to alleviate the effect of the attacks.
  \end{enumerate}
\end{frame}

\section{Static Detection}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Static Detection Problem}
  \begin{itemize}
    \item We want to decide whether the state $x$ is $1$ or $-1$.
      \begin{align*}
	x=\begin{cases}
	  -1 &\text{w.p.~}p^-\\
	  +1 &\text{w.p.~}p^+\\
	\end{cases}
      \end{align*}
    \item $m$ sensors are measuring the system:
      \begin{displaymath}
	\nu_i(S) \triangleq P(y_i\in S|x = -1),\,\mu_i(S) \triangleq P(y_i\in S|x = 1).
      \end{displaymath}
    \item $y_i$ are conditionally independent of each other. 
    \item Define the product measure $\nu,\mu$ as
      \begin{displaymath}
	\nu = \nu_1\times\cdots\times \nu_m,\,	\mu = \mu_1\times\cdots\times \mu_m	
      \end{displaymath}
    \item Define 
      \begin{displaymath}	
	\Lambda_i(y_i) \triangleq \log\left(\frac{d\mu_i}{d\nu_i}\right)
      \end{displaymath}
      as the log likelihood ratio of the $i$th measurements. 
  \end{itemize}
\end{frame}

\begin{frame}{Naive Bayes Detector}
  \begin{itemize}
    \item A detector is a function $f:\mathbb R^m\rightarrow \{-1,1\}$.
    \item The optimal detector (without attacks) with minimum detector error is a Naive Bayesian Detector:
      \begin{align*}
	\hat x=f(y)=\begin{cases}
	  -1 &\text{if }\sum_{i}\Lambda_i(y_i) < \log(p^-/p^+)\\
	  +1 &\text{if }\sum_{i}\Lambda_i(y_i) \geq \log(p^-/p^+)\\
	\end{cases}
      \end{align*}
    \item  One corrupted measurement will possibly change the detection result. 
    \item Not very secure!
  \end{itemize}
\end{frame}

\begin{frame}{Attack Model}
   \begin{enumerate}
     \item What does the attacker know? \\
     \item What can the attacker modify? \\
     \item What is the goal of the attacker? \\
       \begin{itemize}
	 \item Maximizing damage?
	 \item Being undetectable? 
	 \item Drive the state of the system to some point?
       \end{itemize}
     \item What is the strategy of the attacker?\\
   \end{enumerate}
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the detection algorithm $f$ (Kerckhoffs' Principle);
    \item the true state $x$;
    \item all measurements $y$.
  \end{itemize}
  The attacker can manipulate up to $l$ measurements arbitrarily.
  \begin{displaymath}
    y^c = y + \gamma\circ y^a, 
  \end{displaymath}
  where
  \begin{displaymath}
    \gamma\in  \Gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i \leq l\}.
  \end{displaymath}
  The attacker wants to maximize the probability of error:
  \begin{displaymath}
    P_e(f) =\max_{\gamma\in \Gamma,y^a} P(f(y^c)\neq x).
  \end{displaymath}
\end{frame}

\begin{frame}{Attacker's Strategy}
  The optimal strategy for the attacker:
  \begin{align*}
    (\gamma,y^a)= \begin{cases}
      \displaystyle argmin_{\gamma\in \Gamma, y^a} f(y+\gamma\circ y^a ) & (x =1)\\
      \displaystyle argmax_{\gamma\in \Gamma,y^a} f(y+ \gamma\circ y^a) & (x =-1)
    \end{cases}
  \end{align*}
  \begin{figure}[<+htpb+>]
    \centering
    \subfigure[$x = -1$]{
    \includegraphics{pic.2}}
    \subfigure[$x = 1$]{
    \includegraphics{pic.3}}
  \end{figure}
\end{frame}

\begin{frame}{Partitioning the Space of Measurements}
  \begin{figure}[<+htpb+>]
    \centering
    \subfigure[First Detector $f_1$]{
    \includegraphics{pic.4}}
    \subfigure[Second Detector $f_2$]{
    \includegraphics{pic.5}}
  \end{figure}
  There are three categories of point in $R^m$:
  \begin{enumerate}
    \item for all $\gamma\in \Gamma,y^a$, $f(y+\gamma\circ y^a)=1$. (\alert{Good})
    \item for all $\gamma\in \Gamma,y^a$, $f(y+\gamma\circ y^a)=-1$. (\alert{Good})
    \item for some $\gamma\in \Gamma,y^a$, $f(y+\gamma\circ y^a)=1$ and for some $\gamma\in \Gamma,y^a$, $f(y+\gamma\circ y^a)=-1$. (Bad)
  \end{enumerate}
\end{frame}

\begin{frame}{Worst-Case Probability of Error}
  Define
  \begin{align*}
    Y^+(f) &\triangleq \big\{y\in \mathbb R^m:f(y+\gamma\circ y^a)=1, \; \forall y^a\in\mathbb R^m,\gamma\in\Gamma\big\},\\
    Y^-(f) &\triangleq \big\{y\in \mathbb R^m: f(y+\gamma\circ y^a)=-1,\; \forall y^a\in\mathbb R^m,\gamma\in\Gamma\big\}.
  \end{align*}
  The detector can correctly detect $x$ if and only if 
  \begin{displaymath}
    (x,y)\in\Big\{ (-1,y): y\in Y^-(f) \Big\} \cup \Big\{ (+1,y): y\in Y^+(f) \Big\}
  \end{displaymath}
  As a result, the worst-case probability of error is
  \begin{displaymath}
    P_e(f) = (1-\mu(Y^+(f)))p^+ +(1-\nu(Y^-(f)) p^-.
  \end{displaymath}
  If $Y^-(f)\subseteq Y^-(g)$ and $Y^+(f)\subseteq Y^+(g)$, then $P_e(f)\geq P_e(g)$.
\end{frame}

\begin{frame}{$Y^-$ and $Y^+$ for different detectors}
  \begin{figure}[<+htpb+>]
    \centering
    \subfigure[First Detector]{\includegraphics{pic.8}}
    \subfigure[$Y^+$ and $Y^-$]{\includegraphics{pic.9}}\\
    \subfigure[Second Detector]{\includegraphics{pic.6}}
    \subfigure[$Y^+$ and $Y^-$]{\includegraphics{pic.7}}
  \end{figure}
\end{frame}

\begin{frame}{A Hamming-like distance and the Trade-off Between $Y^+(f)$ and $Y^-(f)$}
  Define metric $d:\mathbb R^m\times \mathbb R^m\rightarrow \mathbb N_0$ as
  \begin{displaymath}
    d(y_1,y_2)=\|y_1-y_2\|_0. 
  \end{displaymath}
  Let $Y_1,Y_2$ be two subsets of $\mathbb R^m$, define
  \begin{displaymath}
    d(Y_1,Y_2) = \min_{y_1\in Y_1,y_2\in Y_2}d(y_1,y_2),\,d(y,Y) = d(\{y\},Y). 
  \end{displaymath}

  \begin{lemma}
    For any detector $f$, $d(Y^-(f),Y^+(f))\geq 2l+1$.
  \end{lemma}
  \begin{lemma}
    Given $X^-,\,X^+$ two subsets of $\mathbb R^m$, and $d(X^-,X^+)\geq 2l+1$, there exists a detector $f$, such that $X^-\subseteq Y^-(f)$ and $X^+\subseteq Y^+(f)$.
  \end{lemma}
\end{frame}

\begin{frame}{The structure of the optimal detector}
  \begin{theorem}
    The optimal $f^*$ is of the following form
    \begin{align*}
      f^*(y) = \begin{cases}
	1 & d(y,X^-)\ge d(y,X^+)\\
	-1 & d(y,X^-)< d(y,X^+),
      \end{cases}
    \end{align*}
    where $X^{+}$ and $X^{-}$  are the solutions of the following optimization problem:
    \begin{align*}
      &\mathop{\textrm{minimize}}\limits_{X^+,X^-}&
      & (1-\nu(X^-))p^-+(1-\mu(X^+))p^+\\
      &\textrm{subject to}&
      & d(X^-,\,X^+)\geq 2l+1
    \end{align*}
  \end{theorem}
\end{frame}

\begin{frame}{The structure of the optimal detector}
  \begin{figure}[<+htpb+>]
    \begin{center}
      \includegraphics{pic.12}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Optimal Detector for $m \leq 2l$ and $m = 2l+1$}
  \begin{itemize}
    \item If $m \leq 2l$, then the optimal detector is $f=1$ or $f = -1$.
    \item If $ m = 2l+1$, the optimal detector is based on voting:
      \begin{enumerate}
	\item The detector computes $m$ individual estimates $\hat x_i$ by a N-P detector based on individual measurements $y_i^c$:
	  \begin{align*}
	    \hat x_i \triangleq \begin{cases}
	      -1 & \Lambda_{i}(y_i^c) <\eta_i\\
	      1 & \Lambda_{i}(y_i^c) \geq\eta_i\\
	    \end{cases}.
	  \end{align*}
	\item The detector decides $\hat x = 1$ if there are more than half of the $\hat x_i = 1$. Otherwise it decides $\hat x = -1$.
      \end{enumerate}
  \end{itemize}
\end{frame}

\begin{frame}{Heuristic Detector for $m>2l+1$}
  \begin{itemize}
    \item The true optimal detector is difficult to compute for general $m> 2l+1$. We propose a heuristic detector by choosing 
      \begin{align*}
	X^- &= \{y\in \mathbb R^m:\sum_{i\in\mathcal I}\Lambda_i(y_i)<\log(p^-/p^+),\forall |\mathcal I|=m-2n\} ,\\
	X^+ &= \{y\in \mathbb R^m:\sum_{i\in\mathcal I}\Lambda_i(y_i)\geq\log(p^-/p^+),\forall |\mathcal I|=m-2n\} .
      \end{align*}
    \item If $\inf_{y_i}\Lambda_i(y_i) = -\infty$ and $\sup_{y_i}\Lambda_i(y_i) = \infty$, then $f$ can be computed by the following procedures:
      \begin{enumerate}
	\item The detector sorts all the $\Lambda_i(y_i)$s in descending order.
	\item The detector throws away $l$ measurements with the largest $\Lambda_i(y_i)$s and $l$ measurements with the least $\Lambda_i(y_i)$s.
	\item The detector sums the remaining $m-2l$ $\Lambda_i(y_i)$s and compares the sum to $\log(p^-/p^+)$. The detector chooses $\hat x = -1$ if the truncated sum is less than $\log(p^-/p^+)$, otherwise the detector chooses $\hat x = 1$.
      \end{enumerate}
    \item The heuristic detector is asymptotically optimal if the measurements are identitically distributed.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: i.i.d. Gaussian case}
  \begin{figure}[<+htpb+>]
    \setlength\figureheight{4cm}
    \setlength\figurewidth{5cm}
    \begin{center}
      \input{../../tikz/fig4.tikz}
    \end{center}
    \caption{Probability of Error v.s. Number of Sensors($m$).}
  \end{figure}
\end{frame}

\section{Static Estimation}
\frame{\tableofcontents[currentsection]}
\begin{frame}{Static Estimation Problem}
  \begin{itemize}
    \item The state $x$ is Gaussian with mean 0 and variance of $\sigma^2$.  
    \item The measurement $y_i =  x_i + v_i$, where $v_i$s are i.i.d. Gaussian random variables with mean 0 and variance of $r$.
    \item An estimation algorithm $f:\mathbb R^m\rightarrow\mathbb R$ is a mapping from the space of measurements $y$ to the estimation $\hat x$. 
  \end{itemize}
\end{frame}

\begin{frame}{Attack Model}
  We assume the attacker knows the following:
  \begin{itemize}
    \item the estimation algorithm $f$;
    \item the true state $x$;
    \item all measurements $y$.
  \end{itemize}
  The attacker can manipulate up to $l$ measurements arbitrarily.
  \begin{displaymath}
    y^c = y + \gamma\circ y^a, 
  \end{displaymath}
  where
  \begin{displaymath}
    \gamma\in  \Gamma \triangleq \{\gamma\in \mathbb R^m:\gamma_i =0\;or\;1,\,\sum_{i=1}^m\gamma_i \leq l\}.
  \end{displaymath}
  The attacker wants to maximize the mean squared error:
  \begin{displaymath}
    \textrm{MSE}(f) \triangleq \sup_{\gamma\in \Gamma,y^a} E\left[ (f(y+\gamma\circ y^a)- x)^2\right].
  \end{displaymath}

  The estimation problem is formalized as a minimax problem:
  \begin{displaymath}
    \textrm{MSE}^* = \inf_f\; \textrm{MSE}(f).
  \end{displaymath}
\end{frame}

\section{Dynamic Estimation and Control}
\frame{\tableofcontents[currentsection]}
\subsection{System Description}

\begin{frame}{System Diagram}
  \begin{figure}[htpb]
    \begin{center}
      \input{../../tikz/systemdiagram.tikz}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{System Model}
  \begin{block}{System Description}
    \begin{displaymath}
      \begin{split}
	x(k+1) &= Ax(k)  + Bu(k)+w(k),\\
	y(k) &= C x(k) + v(k).
      \end{split}
    \end{displaymath}
  \end{block}
  \begin{itemize}
    \item $x(k) \in \mathbb R^n$ is the state vector.
    \item  $y(k) \in \mathbb R^m$ is the measurements from the sensors.
    \item  $u(k) \in \mathbb R^p$ is the control input.
    \item $w(k),v(k),x(0)$ are independent Gaussian random vectors, and $x(0) \sim \mathcal N(0,\;\Sigma)$, $w(k) \sim \mathcal N(0,\;Q)$ and $v(k) \sim \mathcal N(0,\;R)$.
    \item The system is assumed to be controllable and observable.
  \end{itemize}
\end{frame}

\begin{frame}{Kalman Filter}
  The Kalman filter (assumed to be in steady state) follows the following update equations:
  \begin{displaymath}
    \hat x(k+1) = A\hat x(k)+Bu(k) + K[y(k+1) - C(A\hat x(k)+Bu(k))],\;\hat x(0)= 0,
  \end{displaymath}
  where $K =  P C' (CP C'  + R)^{ - 1}$ and $P$ is the solution of the following Riccati equation
  \begin{displaymath}
    P = A\left(P^{-1}+C'RC\right)^{-1}A' + Q. 
  \end{displaymath}
  We further assume that $\Sigma =  P$.

  The estimation error $e(k)$ and innovation $z(k)$ is defined as
  \begin{displaymath}
    e(k) = x(k) - \hat x(k),\,z(k) = y(k) - C(A\hat x(k-1)+Bu(k)).
  \end{displaymath}
\end{frame}

\begin{frame}{LQG Controller}
  An LQG controller is used to stabilize the system by minimizing the following objective function:
  \begin{displaymath}
    J = \lim_{T\rightarrow \infty}\min_{u(0),\ldots,u(T)}E\frac{1}{T}\left[\sum_{k=0}^{T-1} x(k)'Wx(k)+u(k)'Uu(k)\right],
  \end{displaymath}
  where $W, U$ are positive semidefinite matrices. The optimal controller is a fixed gain controller, which takes the following form:
  \begin{displaymath}
    u(k) =  L\hat x(k),
  \end{displaymath}
  where $L \triangleq -(B'SB+U)^{-1}B'SA$ and $S$ satisfies the following Riccati equation
  \begin{displaymath}
    S = A'SA+W - A'SB(B'SB+U)^{-1}B'SA.
  \end{displaymath}
\end{frame}

\begin{frame}{$\chi^2$ Failure Detector}
  The innovation of Kalman filter $z(k)$ is i.i.d. Gaussian distributed with zero mean. The $\chi^2$ detector triggers an alarm based on the following event:
  \begin{displaymath}
    g(k)= \sum_{j = k-\mathcal T+1}^k z(j)^T\mathcal P^{-1}z(j)> threshold,
  \end{displaymath}
  where $\mathcal P = CPC' + R$ is the covariance matrix of $z(j)$ and $\mathcal T$ is the window size.

  The alarm rate at time $k$ is 
  \begin{displaymath}
    \beta(k) = P(g(k) > threshold).
  \end{displaymath}
  If the system is operating normally, then the alarm rate is a constant, which we denote as the false alarm rate $\alpha$.
\end{frame}


\subsection{Replay Attacks}

\frame{\tableofcontents[currentsection,currentsubsection]}
\begin{frame}{Replay Attack Model}
  \begin{enumerate}
    \item The attacker knows all the measurements $y(k)$.
    \item It can inject an external control input $u^a(k)$ into the system. Moreover, it can modify all the sensor readings. As a result, the dynamics of the compromised system follow:
      \begin{align*}
	x^c(k+1) &= Ax^c(k) + Bu^c(k) + B^a u^a(k) + w(k), \\
	y^c(k) &= Cx^c(k) +y^a(k)+ v(k).
      \end{align*}
      where $B^a\in \mathbb R^{n\times q}$ characterizes the direction of control inputs the attacker could inject to the system.
  \end{enumerate}
\end{frame}

\begin{frame}{Replay Attack Model}

  The strategy of the attacker can be divided into two stages:
  \begin{enumerate}
    \item The attacker records a sufficient number of $y^c(k)$s from time $1$ to time $T$ without injecting any control input $y^a(k)$ and $u^a(k)$. 
    \item The attacker injects a sequence of desired control input $u^a(k)$ while replaying the previous recorded $y^c(k)$s from time $T+1$ to time $2T$.
  \end{enumerate}

  When the system is under attack, the controller will be unable to perform closed-loop control. Hence the only way to counter this attack is to detect its presence. 
\end{frame}

\begin{frame}{Compromised System Diagram}
  \begin{figure}[htpb]
    \begin{center}
      \input{../../tikz/compromisedsystem.tikz}
    \end{center}
  \end{figure}
\end{frame}

\begin{frame}{Detection of Replay Attacks} 
  Manipulating the Kalman filter equations, we have
  \begin{displaymath}
    z^c(k+T+1) - z^c(k+1) = -C\mathcal A^{k}[\hat x^c(T+1|T) - \hat x^c(1|0)].\;1\leq k\leq T,
  \end{displaymath}
  where $\mathcal A \triangleq (A+BL)(I-KC)$. The detectability of replay attack depends on the stability of $\mathcal A$.
  \begin{enumerate}
    \item If $\mathcal A$ is stable, then the residue during replay $z^c(k+T)$  converges to the ``healthy'' residue $z^c(k)$. As a result, the alarm rate $\beta^c(k)$ converges to the false alarm rate $\alpha$. In other words, the detector cannot distinguish the ``healthy'' system and the system under attack. 
    \item If $\mathcal A$ is unstable, then $g^c(k+T)$ grows exponentially fast. Therefore, the replay can be detected shortly after it begins.
  \end{enumerate}
\end{frame}

\begin{frame}{Detection of Replay Attacks}
  \begin{itemize}
    \item Replay is possible since the Kalman filter and LQG control use constant gains $K,L$.
    \item In order to detect replay, we change the control law to the following:
      \begin{displaymath}
	u(k) = L\hat x(k) + \Delta u(k), 
      \end{displaymath}
      where $\Delta u(k)$s are i.i.d. Gaussian distributed with zero mean and covariance of $\mathcal Q$.
    \item The random signal $\Delta u(k)$ acts as a time stamp. If the system is operating normally, then the output $y(k)$ and $\Delta u(k)$ should be correlated. On the other hand, such correlation ceases to exist when the replay begins.
    \item However, we sacrifice LQG performance since the control is not optimal.
      \begin{displaymath}
	J = J_* + trace[(U+B'SB)\mathcal Q]. 
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example}
  \begin{itemize}
    \item We consider the following scalar system
      \begin{align*}
	x(k+1) &= x(k) + u(k) + w(k),\\
	y(k) &= x(k) + v(k).
      \end{align*}
    \item $R = 0.1$, $Q = W = U =1$, $\alpha = 5\%$.
    \item One can compute that $ P =  1.092,\,K = 0.9161,\, L = -0.6180$ and $\mathcal A = 0.0321$.
    \item The optimal LQG cost is $ J_* = 1.7076$. The LQG cost after adding $\Delta u(k)$ is $J = J_* +  2.618  \mathcal Q$.
    \item The attacker records from $k=1$ to $k = 10$ and replay starts at $k = 11$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Detection rate with different $\mathcal Q$}
  \begin{figure}[htpb]
    \setlength\figureheight{4cm}
    \setlength\figurewidth{5cm}
    \begin{center}
      \input{../../tikz/replay1.tikz}
    \end{center}
    \caption{Detection rate at each time step with window size $\mathcal T=5$}
  \end{figure}
\end{frame}

\begin{frame}{Numerical Example: Detection rate with different $\mathcal T$}
  \begin{figure}[htpb]
    \setlength\figureheight{4cm}
    \setlength\figurewidth{5cm}
    \begin{center}
      \input{../../tikz/replay2.tikz}
    \end{center}
    \caption{Detection rate at each time step with $\mathcal Q=0.6$}
  \end{figure}
\end{frame}

\subsection{Integrity Attacks}

\frame{\tableofcontents[currentsection,currentsubsection]}
\begin{frame}{Integrity Attack Model}
  \begin{enumerate}
    \item The adversary knows the static parameters of the system, namely $A,\,B,\,C,\,K,\,L$. 
    \item The adversary compromised a subset of sensors, and can add arbitrary bias to the reading of compromised sensors. 
      \begin{displaymath}
	y^c(k) = Cx^c(k) + \gamma\circ y^a(k) + v(k).
      \end{displaymath}
    \item The adversary can inject external control inputs to the system. As a result, the system equation becomes 
      \begin{displaymath}
	x^c(k+1) = Ax^c(k) + Bu^c(k) + B^au^a(k) + w(k), 
      \end{displaymath}
  \end{enumerate}
\end{frame}

\begin{frame}{Integrity Attack Model}
  \begin{itemize}
    \item We assume that the adversary wants its attack to be stealthy.
    \item Ideally, the attacker would choose $u^a(k),\,y^a(k)$, such that:
      \begin{displaymath}
	\beta^c(k) \leq \alpha',\, k=1,\ldots,T. 
      \end{displaymath}
      However, the alarm rate is difficult to compute.
    \item We assume that attacker would enforce the following condition: 
      \begin{displaymath}
	\|z^c(k)-z(k)\| \leq r.\;\forall k=1,\ldots,T.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Comparison between Integrity Attack and Replay Attack}
  \begin{enumerate}
    \item In order to perform an integrity attack, the adversary need to know system parameters, while the knowledge of parameters are not needed for the replay attack. 
    \item To perform a replay attack, the adversary need to be able to read and modify the measurements from all sensors, while the adversary only need to be able to modify the measurements from some of the sensors to perform an integrity attack. 
    \item For both attacks, the adversary wants its action to be stealthy.
  \end{enumerate}
\end{frame}

\begin{frame}{Preliminary Results}
  We assume $B^a =  0$.
  \begin{block}{Healthy System}
    \begin{align*}
      x(k+1) &= Ax(k) + BL\hat x(k) + w(k),\,y(k)  = Cx(k) + v(k) \\
      z(k+1) &= y(k+1) - C(A+BL)\hat x(k),\\
      \hat x(k+1) & = (A+BL)\hat x(k) + Kz(k+1)\\
    \end{align*}
  \end{block}
  \begin{block}{Compromised System}
    \begin{align*}
      x^c(k+1) &= Ax^c(k) +BL\hat x^c(k) +  w(k),\\
      y^c(k)  &= Cx^c(k) + v(k)+\gamma\circ y^a(k) \\
      z^c(k+1) &= y^c(k+1) - C(A+BL)\hat x^c(k),\\
      \hat x^c(k+1)  &= (A+BL)\hat x^c(k) + Kz^c(k+1)\\
    \end{align*}
  \end{block}
\end{frame}

\begin{frame}{Preliminary Results}
  \begin{block}{Difference between Compromised and Healthy Systems}
    \begin{align*}
      \Delta e(k+1) &= (A - KCA)\Delta e(k) - K\gamma\circ y^a(k+1),\\
      \Delta z(k) &= CA\Delta e(k-1) +\gamma\circ y^a(k).
    \end{align*}
  \end{block}
  \begin{itemize}
    \item We are interested in $e^c(k)$
    \item $e^c(k)= \Delta e(k) + e(k)$, and $e(k)$ is a stationary Gaussian process.
    \item To analyze the resilience of the system, we only need to characterize the reachable region of $\Delta e(k)$ under constraint
      \begin{displaymath}
	\|\Delta z(k)\|\leq r.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Reachable Region}
  \begin{itemize}
    \item Define the reachable region of $\Delta e(k)$ as
      \begin{displaymath}
	R_r(k) = \{\Delta e(k)\in \mathbb R^n: \exists y^a(0),\ldots,y^a(k),\,s.t.\,\|\Delta z(j)\|\leq r,\,0\leq j\leq k\} .
      \end{displaymath}
    \item Due to linearity, we only need to consider $R_1(k)$  ($R(k)$) which can be approximated by ellipsoid.
    \item If $R(k)$ is uniformly bounded, then the attacker can only introduce finite bias to $e^c(k)$. If $R(k)$ is unbounded, then the attacker can destabilize the estimator.
  \end{itemize}
  \begin{theorem}
    The reachable region $R(k)$ is unbounded if and only if $A$ has an unstable eigenvalue and the corresponding eigenvector $v$ satisfies:
    \begin{enumerate}
      \item $Cv  = \gamma\circ y^a$, for some $y^a\in \mathbb R^m$. 
      \item $v$ is a reachable state of the dynamic system $\Delta e(k+1) = (A-KCA)\Delta e(k) - K\gamma \circ y^a(k+1)$.
    \end{enumerate}
  \end{theorem}
\end{frame}

\begin{frame}{Numerical Example}
  \begin{itemize}
    \item We consider a vehicle which is moving along the $x$-axis. 
      \begin{displaymath}
	\left[ {\begin{array}{*{20}c}
	  \dot{x}(k+1)  \\
	  x(k+1) \\
	\end{array}} \right]= \left[ {\begin{array}{*{20}c}
	  1 & 0  \\
	  1 & 1  \\
	\end{array}} \right]  \left[ {\begin{array}{*{20}c}
	  \dot{x}(k)  \\
	  x(k) \\
	\end{array}} \right] + w(k).
      \end{displaymath}
    \item Suppose two sensors are measuring velocity and position respectively. Hence
      \begin{equation}
	y(k) =   \left[ {\begin{array}{*{20}c}
	  \dot{x}(k)  \\
	  x(k) \\
	\end{array}} \right] + v(k).
      \end{equation}
    \item $Q = R  = 1$.
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Example: Compromising Velocity Sensor}
  \begin{figure}[<+htpb+>]
    \setlength\figureheight{4cm}
    \setlength\figurewidth{4cm}
    \begin{center}
      \input{../../tikz/integrity1.tikz}
    \end{center}
    \caption{Inner and Outer Approximation When the Velocity Sensor is Compromised}
  \end{figure}
\end{frame}

\begin{frame}{Numerical Example: Compromising Position Sensor}
  \begin{figure}[<+htpb+>]
    \begin{center}
      \subfigure[Outer Approximation]{
      \setlength\figureheight{3cm}
      \setlength\figurewidth{3cm}
      \input{../../tikz/integrity2.tikz}
      }
      \subfigure[Inner Approximation]{
      \setlength\figureheight{3cm}
      \setlength\figurewidth{3cm}
      \input{../../tikz/integrity3.tikz}
      }
    \end{center}
    \caption{Reachable set When the Position Sensor is Compromised}
  \end{figure}
  The reachable region $R(k)$ is unbounded. In fact, one can check that $v = [0,1]'$ is the unstable eigenvector.
\end{frame}

\section{Future Work}
\frame{\tableofcontents[currentsection]}
\begin{frame}{Future Work: Static Estimation}
  \begin{enumerate}
    \item The optimal policy for the attacker
    \item The structure of the optimal estimator
    \item The true optimal estimator for special cases (if possible)
    \item Heuristic estimators (asymptotic optimality?)
  \end{enumerate}
\end{frame}

\begin{frame}{Future Work: Replay Attacks}
  The replay can be detected in two cases:
  \begin{itemize}
    \item  $\mathcal A$ is unstable;
    \item A random signal $\Delta u(k)$ is added to the control input.
  \end{itemize}
   We would like to provide algorithms to find estimation and control gain $K$ and $L$ which stabilize the system (stabilize $A+BL$ and $A-KCA$) while rendering $\mathcal A$ unstable. Moreover, we would like to provide conditions on the existence of such $K$ and $L$.

  We also want to provide metrics to compare the performance of the two countermeasures.
\end{frame}

\begin{frame}{Future Work: Integrity Attacks}
  We want to generalize our result to $B^a\neq 0$. To be specific, we would like to
  \begin{enumerate}
    \item Provide an ellipsoidal approximation algorithm of reachable set to quantify the performance degradation of CPS under attack;
    \item Prove necessary and sufficient conditions for the boundedness of the reachable region. 
  \end{enumerate}
\end{frame}

\begin{frame}{Time Line}
  \begin{table}[!h]
    \centering
    \caption{Proposed research time-line. Graduation in December~2011.}
    \label{tab:schedule}
    \begin{tabular}{@{}lccccc@{}}\toprule
      Research topic&   Apr. 12 & Jun. 12 &  Aug. 12 & Oct. 12 \\
      &   May. 12 & July. 12 & Sep. 12  & Nov. 12 \\
      \midrule
      Integrity Attacks    &  \checkmark\\
      Replay Attacks & & \checkmark\\
      Static Estimation && \checkmark & \checkmark\\
      Write thesis &&&\checkmark&\checkmark\\
      \bottomrule
    \end{tabular}
  \end{table}

\end{frame}
\end{document}


