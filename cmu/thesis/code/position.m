A = [1 0;1 1];
B = [1;0];
Gamma = [0;1];
U = 1;
W = eye(2);
Q = eye(2);
R = eye(2);
C = eye(2);
S = eye(2);
P = eye(2);
for i = 1:100
S = A'*S*A+W-A'*S*B/(B'*S*B+U)*B'*S*A;
end
L = -inv(B'*S*B+U)*B'*S*A;
for i = 1:100
P = A*P*A'+Q-A*P*C'/(C*P*C'+R)*C*P*A';
end
K = P*C'/(C*P*C'+R);
Pcal = C*P*C'+R;
invP = inv(Pcal);
tA = [A+B*L -B*L;zeros(2) A-K*C*A];
tB = [zeros(2,1);-K*Gamma];
tC = [zeros(2,2) C*A];
tD = [Gamma];

Tmax = zeros(4);

epsilon = 0.1;

for i = 1:20
    S1 = [tA';tB']*Tmax*[tA tB];
    S2 = [tC';tD']*invP*[tC tD];
    
    cvx_begin
        variable a
        variable b
        maximize(log_det(a*S1+b*S2+epsilon*eye(5)))
        subject to
            a>=0
            b>=0
            a+b == 1
    cvx_end
    
    Tmax = a*tA'*Tmax*tA + b*tC'*invP*tC - (a*tA'*Tmax*tB+b*tC'*invP*tD)/(a*tB'*Tmax*tB+b*tD'*invP*tD)*(a*tB'*Tmax*tA+b*tD'*invP*tC);
end

RR{1} = diag([10000 10000 10000 10000])
AA = inv(tA);
BB = -inv(tA)*tB;
CC = tC * inv(tA);
DD = tD - tC* inv(tA)*tB;
for i=1:19
    S1 = [AA';BB']*RR{i}*[AA BB];
    S2 = [CC';DD']*invP*[CC DD];
    cvx_begin
        variable a
        variable b
        maximize(log_det(a*S1+b*S2+epsilon*eye(5)))
        subject to
            a >= 0
            b >= 0
            a+b == 1
    cvx_end
    a = 0.5;
    b = 0.5;
    RR{i+1} = (a*AA'*RR{i}*AA + b*CC'*invP*CC - (a*AA'*RR{i}*BB+b*CC'*invP*DD)*inv(a*BB'*RR{i}*BB+b*DD'*invP*DD)*(a*BB'*RR{i}*AA+b*DD'*invP*CC));

    cvx_begin
        variable a(1)
        variable b(1)
        maximize(log_det(a*RR{i+1}+b*Tmax+epsilon*eye(4)))
        subject to
            a>=0
            b>=0
            a+b == 1
    cvx_end
    RR{i+1} = RR{i+1}*a + Tmax*b;
end

for i = 1:9
    X = inv(RR{i});
    X = inv(X(1:2,1:2));
    plotellipsoid
    hold on
end
