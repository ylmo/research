\documentclass{beamer}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,plotmarks,circuits.ee.IEC,positioning,fit}

\usepackage{pgfplots}
\pgfplotsset{every axis/.append style={line width=1pt}}
\pgfdeclarelayer{background}
\pgfsetlayers{background,main}

\newlength\figureheight
\newlength\figurewidth
\setlength\figureheight{2cm} 
\setlength\figurewidth{6cm} 

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}


\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[Time Domain]{Time Domain Analysis: Response of the First and Second Order Systems}

\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{ } 
\date[Sept 4, 2014]{Sept 4, 2014 at Nanyang Technological University}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{System Modelling}

\begin{frame}{Electrical System}
  Voltage(u) - Current (i) relationship for different components:
  \begin{enumerate}
    \item Resistor: $u = R i$
      \begin{center}
	\begin{tikzpicture}[circuit ee IEC,semithick,
	  every info/.style={font=\footnotesize},
	  small circuit symbols,
	  set resistor graphic=var resistor IEC graphic]
	  \draw (0,0) to [current direction={near start,info=$i$},resistor={near end, direction info={info=$R$}, info'=$u$}] (3,0);
	\end{tikzpicture}
      \end{center}
    \item Capacitor: $u = \frac{Q}{C } = \frac{1}{C }\int i dt$
      \begin{center}
	\begin{tikzpicture}[circuit ee IEC,semithick,
	  every info/.style={font=\footnotesize},
	  small circuit symbols,
	  set resistor graphic=var resistor IEC graphic]
	  \draw (0,0) to [current direction={near start,info=$i$},capacitor={near end, direction info={info=$C$}, info'=$u$}] (3,0);
	\end{tikzpicture}

      \end{center}
    \item Inductor: $u = L \frac{di}{dt} $
      \begin{center}
	\begin{tikzpicture}[circuit ee IEC,semithick,
	  every info/.style={font=\footnotesize},
	  small circuit symbols,
	  set resistor graphic=var resistor IEC graphic]
	  \draw (0,0) to [current direction={near start,info=$i$},inductor={near end, direction info={info=$L$}, info'=$u$}] (3,0);
	\end{tikzpicture}
      \end{center}
  \end{enumerate}
\end{frame}

\begin{frame}{First Order System Example: RC circuit}
  \begin{center}
    \begin{tikzpicture}[circuit ee IEC,semithick,
      every info/.style={font=\footnotesize},
      small circuit symbols,
      set resistor graphic=var resistor IEC graphic,
      set diode graphic=var diode IEC graphic,
      set make contact graphic= var make contact IEC graphic,
      >=latex]
      \node [contact,label=right:+] (contact1) at (0,0) {}; 
      \node [contact,label=left:-] (contact2) at (6,0) {}; 
      \node [contact,label=right:+] (contact3) at (3,2) {};
      \node [contact,label=left:-] (contact4) at (6,2) {};
      \draw (contact1) to [current direction={info=$i$}] ++(up:1)
      to [resistor={direction info={info=$R$}, info'=$u_r$}] ++(right:3)
      to [capacitor={direction info={info=$C$}, info'=$u_c$}] ++(right:3)
      to (contact2); 
      \node at (3,0) {$u(t)$};
      \draw (3,1) -- (contact3);
      \draw (6,1) -- (contact4);
      \node at (4.5,2) [anchor=south] {out};
    \end{tikzpicture}
  \end{center}
  Kirchhoff's voltage law:
  \begin{displaymath}
    u_r + u_c = u(t)\;\Rightarrow \; R i + u_c = u(t)
  \end{displaymath}
  Capacitor:
  \begin{displaymath}
    u_c = \frac{1}{C}\int i dt \;\Rightarrow \; i = C\frac{du_c}{dt} 
  \end{displaymath}
  Hence,
  \begin{displaymath}
    RC\frac{du_c}{dt}+u_c = u(t) 
  \end{displaymath}
\end{frame}

\begin{frame}{ODE of the First-Order System: Standard Form}
  Standard Form:
  \begin{displaymath}
    \tau \frac{d}{dt}y(t) + y(t) = \gamma x(t).
  \end{displaymath}
  Parameters:
  \begin{enumerate}
    \item  $\gamma$ is the gain of the system.
    \item  $\tau$ is the time constant. It determines how fast the system reacts to the input. Large $\tau$ implies slow response.
  \end{enumerate}
  RC circuit example:
  \begin{displaymath}
    RC \frac{d}{dt}u_c(t) + u_c(t) = u(t).
  \end{displaymath}
  Hence, $\gamma = 1$ and $\tau = RC$.
\end{frame}

\begin{frame}{Second Order System Example: LCR circuit}
  \begin{center}
    \begin{tikzpicture}[circuit ee IEC,semithick,
      every info/.style={font=\footnotesize},
      small circuit symbols,
      set resistor graphic=var resistor IEC graphic,
      set diode graphic=var diode IEC graphic,
      set make contact graphic= var make contact IEC graphic,
      >=latex]
      \node [contact,label=right:+] (contact1) at (0,0) {}; 
      \node [contact,label=left:-] (contact2) at (6,0) {}; 
      \node [contact,label=right:+] (contact3) at (4,2) {};
      \node [contact,label=left:-] (contact4) at (6,2) {};
      \draw (contact1) to [current direction={info=$i$}] ++(up:1)
      to [inductor={direction info={info=$L$}, info'=$u_l$}] ++(right:2)
      to [resistor={direction info={info=$R$}, info'=$u_r$}] ++(right:2)
      to [capacitor={direction info={info=$C$}, info'=$u_c$}] ++(right:2)
      to (contact2); 
      \node at (3,0) {$u(t)$};
      \draw (4,1) -- (contact3);
      \draw (6,1) -- (contact4);
      \node at (5,2) [anchor=south] {out};
    \end{tikzpicture}
  \end{center}
  KVL:
  \begin{displaymath}
    u_l + u_r + u_c = u(t)\;\Rightarrow \; L \frac{di}{dt} +R i +  u_c = u(t)
  \end{displaymath}
  Capacitor:
  \begin{displaymath}
    u_c = \frac{1}{C}\int i dt \;\Rightarrow \; i = C\frac{du_c}{dt} 
  \end{displaymath}
  Hence,
  \begin{displaymath}
    LC \frac{d^2u_c}{dt^2} +  RC\frac{du_c}{dt} + u_c = u(t) 
  \end{displaymath}
\end{frame}

\begin{frame}{ODE of the Second-Order System: Standard Form}
  Standard Form:
  \begin{displaymath}
    \frac{d^2}{dt^2}y(t) + 2\zeta\omega_n \frac{d}{dt} y(t) + \omega_n^2 y(t) = \gamma \omega_n^2 x(t).
  \end{displaymath}
  Parameters:
  \begin{enumerate}
    \item  $\gamma$ is the gain of the system.
    \item  $\zeta$ is the damping ratio.
    \item  $\omega_n$ is the natural frequency.
  \end{enumerate}
  LCR circuit example:
  \begin{displaymath}
    \frac{d^2}{dt^2}u_c(t)+2\left(\frac{R}{2}\sqrt{\frac{C}{L}}\right) \frac{1}{\sqrt{LC}}\frac{d}{dt}u_c(t) +\frac{1}{LC} u_c(t) =\frac{1}{LC} u(s). 
  \end{displaymath}
  Hence, $\gamma = 1$, $\zeta = \frac{2}{R}\sqrt{\frac{C}{L}}$ and $\omega_n = \frac{1}{\sqrt{LC}}$.
\end{frame}

\section{Typical Input Signals}
  \frame{\tableofcontents[currentsection]}

\begin{frame}{Step Input}
  Definition:
  \begin{displaymath}
    x(t) = \begin{cases}
      A &(t\geq 0)\\
      0&(t< 0)
    \end{cases}
  \end{displaymath}
  \begin{center}
    \setlength\figureheight{4cm} 
    \setlength\figurewidth{8cm} 
    \inputtikz{step}
  \end{center}
  Unit Step Input: $A = 1$.
\end{frame}

\section{Response of the First-Order Systems}

\frame{\tableofcontents[currentsection]}

\begin{frame}{RC circuit}
  \begin{center}
    \begin{tikzpicture}[circuit ee IEC,semithick,
      every info/.style={font=\footnotesize},
      small circuit symbols,
      set resistor graphic=var resistor IEC graphic,
      set diode graphic=var diode IEC graphic,
      set make contact graphic= var make contact IEC graphic,
      >=latex]
      \node [contact,label=right:+] (contact1) at (0,0) {}; 
      \node [contact,label=left:-] (contact2) at (6,0) {}; 
      \node [contact,label=right:+] (contact3) at (3,2) {};
      \node [contact,label=left:-] (contact4) at (6,2) {};
      \draw (contact1) to [current direction={info=$i$}] ++(up:1)
      to [resistor={direction info={info=$R$}, info'=$u_r$}] ++(right:3)
      to [capacitor={direction info={info=$C$}, info'=$u_c$}] ++(right:3)
      to (contact2); 
      \node at (3,0) {$u(t)$};
      \draw (3,1) -- (contact3);
      \draw (6,1) -- (contact4);
      \node at (4.5,2) [anchor=south] {out};
    \end{tikzpicture}
  \end{center}
  \begin{displaymath}
    RC\frac{d}{dt}u_c(t)+u_c(t) = u(t) 
  \end{displaymath}

  \begin{itemize}
    \item What is $u_c(\infty)$?
    \item How fast does $u_c(t)$ grow?
  \end{itemize}
\end{frame}



\begin{frame}{Unit Step Response of the First Order System}
  Using the standard form:
  \begin{displaymath}
    \tau \frac{d}{dt}y(t) + y(t) = \gamma x(t). 
  \end{displaymath}
  Assuming that $x(0) = 0$, the solution of this equation is given by
  \begin{displaymath}
    y(t) = \gamma \left(1-  e^{-t/\tau}\right). 
  \end{displaymath}
\end{frame}

\begin{frame}{Unit Step Response}
  \begin{center}
    \setlength\figureheight{7cm} 
    \setlength\figurewidth{12cm} 
    \inputtikz{stepfirst}
  \end{center}
\end{frame}

\begin{frame}{Unit Step Response: Which is which?}
  \begin{minipage}{0.6\textwidth}
    \begin{center}
      \setlength\figureheight{7cm} 
      \setlength\figurewidth{7cm} 
      \inputtikz{question1}
    \end{center}
  \end{minipage}
  \begin{minipage}{0.35\textwidth}
    \begin{enumerate}
      \item $\gamma = 2,\,\tau = 1$ 
      \item $\gamma = 1,\,\tau = 1$ 
      \item $\gamma = 1,\,\tau = 2$ 
    \end{enumerate}
  \end{minipage}
\end{frame}

\section{Response of the Second-Order System}
\frame{\tableofcontents[currentsection]}

\begin{frame}{Unit Step Response of the Second Order System}
  \begin{center}
    \setlength\figureheight{6cm} 
    \setlength\figurewidth{12cm} 
    \inputtikz{second}
  \end{center}
\end{frame}

\begin{frame}{Unit Step Response of the Second Order System}
  Using the standard form:
  \begin{displaymath}
    \frac{d^2}{dt^2}y(t) + 2\zeta\omega_n \frac{d}{dt} y(t) + \omega_n^2 y(t) = \gamma \omega_n^2 x(t).
  \end{displaymath}
  Assuming zero initial condition:
  \begin{displaymath}
    y(t) =\gamma \left(1  + \frac{p_2}{p_1-p_2}e^{p_1t} +\frac{p_1}{p_2-p_1} e^{p_2t} \right),
  \end{displaymath}
  where $p_1$ and $p_2$ are the solution of
  \begin{displaymath}
   p^2 + 2\zeta \omega_n p + \omega_n^2 = 0. 
  \end{displaymath}
  We will assume that $\gamma = 1$.
\end{frame}

\begin{frame}{Roots of the Characteristic Polynomial}
  \begin{align*}
    p_1 &=\omega_n\left(-\zeta + \sqrt{\zeta^2-1}\right)\\
    p_2 &=\omega_n\left(-\zeta - \sqrt{\zeta^2-1}\right)
  \end{align*}
  Three possibilities:
  \begin{enumerate}
    \item $0\leq \zeta < 1$ (underdamped): $p_1$ and $p_2$ are complex conjugates:
  \begin{align*}
    p_1 &=\omega_n\left(-\zeta + j\sqrt{1-\zeta^2}\right)\\
    p_2 &=\omega_n\left(-\zeta - j\sqrt{1-\zeta^2}\right)
  \end{align*}
    \item $\zeta = 1$ (critically damped): $p_1 = p_2 = -\omega_n$ is a double root.
    \item $\zeta > 1$ (overdamped): $p_2<p_1<0$ are real.
  \end{enumerate}
\end{frame}

\begin{frame}{Exponent of a Complex Number}
  Euler's Formula:
  \begin{displaymath}
    e^{j\theta} = \cos\theta + j\sin\theta.
  \end{displaymath}
  Suppose $p = \sigma + j\omega$, by Euler's Formula:
  \begin{displaymath}
    e^{pt} = e^{\sigma t + j\omega t} =e^{\sigma t}e^{  j\omega t}=e^{\sigma t} \left( \cos\omega t + j\sin\omega t \right). 
  \end{displaymath}
  $\mathfrak{Re}(p) = \sigma$ affects the convergence speed.
  \begin{itemize}
    \item If $\sigma =0$, then?
  \end{itemize}
     $\mathfrak{Im}(p) = \omega$ affects the  oscillation frequency.
     \begin{itemize}
       \item If $\omega=0$, then?
     \end{itemize}
\end{frame}

\begin{frame}{Roots of the Characteristic Polynomial: $\zeta =0$}

  \begin{tikzpicture}[>=latex,thick]
    \draw [thin,gray,->] (-3,0)--(-5,0);
    \draw [thin,gray,->] (-3,0)--(-1,0);
    \draw [->] (-8,0)--(4,0);
    \node [label=above:$\sigma$] at (4,0) {};
    \draw [->] (0,-4)--(0,4);
    \node [label=left:$j\omega$] at (0,4) {};
    \draw [thin,gray] (0,3) arc (90:270:3);
    \draw [thin,gray,->] (0,3) arc (90:150:3);
    \draw [thin,gray,->] (0,-3) arc (270:210:3);
    \node [label=right:$\omega_n$] at (0,3) {};
    \node [label=right:$-\omega_n$] at (0,-3) {};
    \node [label=135:$-\omega_n$] at (-3,0) {};

    \coordinate (p1) at (90:3);
    \coordinate (p2) at (270:3);
    \coordinate (o) at (0,0);

    \draw [red](p1)--+(45:0.1);
    \draw [red](p1)--+(135:0.1);
    \draw [red](p1)--+(225:0.1);
    \draw [red](p1)--+(315:0.1);

    \draw [red](p2)--+(45:0.1);
    \draw [red](p2)--+(135:0.1);
    \draw [red](p2)--+(225:0.1);
    \draw [red](p2)--+(315:0.1);

    \node [label=left:{$p_1$}] at (p1) {};
    \node [label=left:{$p_2$}] at (p2) {};
  \end{tikzpicture}
\end{frame}

\begin{frame}{Underdamped System: $\zeta = 0$}
  \begin{displaymath}
    y(t) = 1-\cos (\omega_n t). 
  \end{displaymath}
  \begin{center}
    \setlength\figureheight{5cm} 
    \setlength\figurewidth{10cm} 
    \inputtikz{nodamp}
  \end{center}
\end{frame}

\begin{frame}{Roots of the Characteristic Polynomial: $0<\zeta < 1$}
  \begin{tikzpicture}[>=latex,thick]
    \draw [thin,gray,->] (-3,0)--(-5,0);
    \draw [thin,gray,->] (-3,0)--(-1,0);
    \draw [->] (-8,0)--(4,0);
    \node [label=above:$\sigma$] at (4,0) {};
    \draw [->] (0,-4)--(0,4);
    \node [label=left:$j\omega$] at (0,4) {};
    \draw [thin,gray] (0,3) arc (90:270:3);
    \draw [thin,gray,->] (0,3) arc (90:150:3);
    \draw [thin,gray,->] (0,-3) arc (270:210:3);
    \node [label=right:$\omega_n$] at (0,3) {};
    \node [label=right:$-\omega_n$] at (0,-3) {};
    \node [label=135:$-\omega_n$] at (-3,0) {};

    \coordinate (p1) at (135:3);
    \coordinate (p2) at (225:3);
    \coordinate (o) at (0,0);

    \draw [red](p1)--+(45:0.1);
    \draw [red](p1)--+(135:0.1);
    \draw [red](p1)--+(225:0.1);
    \draw [red](p1)--+(315:0.1);

    \draw [red](p2)--+(45:0.1);
    \draw [red](p2)--+(135:0.1);
    \draw [red](p2)--+(225:0.1);
    \draw [red](p2)--+(315:0.1);

    \draw [dashed] (p1)--(p2);
    \draw [dashed] (p1)--(p1-|o);
    \draw [dashed] (p2)--(p2-|o);

    \node [label=right:{$\omega_d = \omega_n\sqrt{1-\zeta^2}$}] at (p1-|o){};
    \node [label=-45:{$-\zeta\omega_n$}] at (p1|-o){};
    \draw [->] (o) -- node [fill=white] {$\omega_n$} (p1);
    \node [label=135:{$p_1$}] at (p1) {};
    \node [label=225:{$p_2$}] at (p2) {};
  \end{tikzpicture}
\end{frame}

\begin{frame}{Underdamped System: $0<\zeta<1 $}
  \begin{center}
    \setlength\figureheight{7cm} 
    \setlength\figurewidth{12cm} 
    \inputtikz{underdamp}
  \end{center}
\end{frame}

\begin{frame}{Underdamped System: Effect of the Damping Ratio}
  \begin{center}
    \setlength\figureheight{7cm} 
    \setlength\figurewidth{12cm} 
    \inputtikz{underdamp2}
  \end{center}
  Large damping ratio ($<1$) implies faster convergence and less oscillation.
\end{frame}

\begin{frame}{Roots of the Characteristic Polynomial: $\zeta = 1$}
  \begin{tikzpicture}[>=latex,thick]
    \draw [thin,gray,->] (-3,0)--(-5,0);
    \draw [thin,gray,->] (-3,0)--(-1,0);
    \draw [->] (-8,0)--(4,0);
    \node [label=above:$\sigma$] at (4,0) {};
    \draw [->] (0,-4)--(0,4);
    \node [label=left:$j\omega$] at (0,4) {};
    \draw [thin,gray] (0,3) arc (90:270:3);
    \draw [thin,gray,->] (0,3) arc (90:150:3);
    \draw [thin,gray,->] (0,-3) arc (270:210:3);
    \node [label=right:$\omega_n$] at (0,3) {};
    \node [label=right:$-\omega_n$] at (0,-3) {};
    \node [label=135:$-\omega_n$] at (-3,0) {};

    \coordinate (p1) at (180:3);
    \coordinate (o) at (0,0);

    \draw [red](p1)--+(45:0.1);
    \draw [red](p1)--+(135:0.1);
    \draw [red](p1)--+(225:0.1);
    \draw [red](p1)--+(315:0.1);

    \node [label=225:{$p_1$}] at (p1) {};
  \end{tikzpicture}
\end{frame}

\begin{frame}{Roots of the Characteristic Polynomial: $\zeta > 1$}
  \begin{tikzpicture}[>=latex,thick]
    \draw [thin,gray,->] (-3,0)--(-5,0);
    \draw [thin,gray,->] (-3,0)--(-1,0);
    \draw [->] (-8,0)--(4,0);
    \node [label=above:$\sigma$] at (4,0) {};
    \draw [->] (0,-4)--(0,4);
    \node [label=left:$j\omega$] at (0,4) {};
    \draw [thin,gray] (0,3) arc (90:270:3);
    \draw [thin,gray,->] (0,3) arc (90:150:3);
    \draw [thin,gray,->] (0,-3) arc (270:210:3);
    \node [label=right:$\omega_n$] at (0,3) {};
    \node [label=right:$-\omega_n$] at (0,-3) {};
    \node [label=135:$-\omega_n$] at (-3,0) {};

    \coordinate (p1) at (-1.5,0);
    \coordinate (p2) at (-6,0);
    \coordinate (o) at (0,0);

    \draw [red](p1)--+(45:0.1);
    \draw [red](p1)--+(135:0.1);
    \draw [red](p1)--+(225:0.1);
    \draw [red](p1)--+(315:0.1);

    \draw [red](p2)--+(45:0.1);
    \draw [red](p2)--+(135:0.1);
    \draw [red](p2)--+(225:0.1);
    \draw [red](p2)--+(315:0.1);

    \node [label=225:{$p_1$}] at (p1) {};
    \node [label=225:{$p_2$}] at (p2) {};
  \end{tikzpicture}
\end{frame}

\begin{frame}{Critically Damped and Overdamped Response}
  Critically damped response:
  \begin{displaymath}
    y(t) = 1-e^{-\omega_n t}(1+\omega_n t).
  \end{displaymath}
  Overdamped Response:
  \begin{displaymath}
    y(t) = 1 + \frac{p_2}{p_1-p_2}e^{p_1t} +\frac{p_1}{p_2-p_1} e^{p_2t} ,
  \end{displaymath}
  where
  \begin{align*}
    p_1 &=\omega_n\left(-\zeta + \sqrt{\zeta^2-1}\right)\\
    p_2 &=\omega_n\left(-\zeta - \sqrt{\zeta^2-1}\right)
  \end{align*}
  If $\zeta$ is very large, then $p_2\rightarrow-\infty$ while $p_1\rightarrow 0$. Hence, $y(t)$ can be approximated as the response of a slow first-order system:
  \begin{displaymath}
    y(t) \approx 1 - e^{p_1t}.
  \end{displaymath}
\end{frame}

\begin{frame}{Critically Damped and Overdamped System: $\zeta\geq 1 $}
  \begin{center}
    \setlength\figureheight{6cm} 
    \setlength\figurewidth{12cm} 
    \inputtikz{overdamp}
  \end{center}
  Increasing $\zeta$ makes the convergence speed slower!! 

  The optimal convergence speed is achieved when $\zeta = 1$.
\end{frame}

\begin{frame}{Unit Step Response: Which is which?}
  \begin{minipage}{0.6\textwidth}
    \begin{center}
      \setlength\figureheight{7cm} 
      \setlength\figurewidth{7cm} 
      \inputtikz{question2}
    \end{center}
  \end{minipage}
  \begin{minipage}{0.35\textwidth}
    Assume $\gamma = \omega_n = 1$:
    \begin{enumerate}
      \item $\zeta = 0.2$ 
      \item $\zeta = 0.6$ 
      \item $\zeta = 1$ 
      \item $\zeta = 2$ 
    \end{enumerate}
  \end{minipage}
\end{frame}

\section{Conclusion}
\begin{frame}{Step Response of the First and Second-Order System}
  \begin{block}{First-Order System}
    Converges exponentially, the speed of convergence is determined by the time constant $\tau$.
  \end{block}
  \begin{block}{Second-Order System}
    $\zeta$ affects the shape of the unit step response. 
    \begin{table}[h]
      \footnotesize
      \centering
      \begin{tabular}{c|ccc}
	\toprule
	&range of $\zeta$ & $y(t)$ & $\zeta\uparrow$\\
	\midrule
	\multirow{2}{*} {Underdamped} & \multirow{2}{*}{$0\leq \zeta<1$} &\multirow{2}{*}{ Oscillating} & Convergence Speed$\uparrow$\\
	& & & Oscillation Frequency$\downarrow$\\
	Critically Damped&$\zeta = 1$&No Oscillation&\\
	Overdamped&$\zeta > 1$&No Oscillation&Convergence Speed$\downarrow$\\
	\bottomrule
      \end{tabular}  
    \end{table}
  \end{block}
\end{frame}
\end{document}
