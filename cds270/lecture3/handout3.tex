\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,plotmarks,shapes.geometric}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\why}{({\bf why?})}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}
\newcommand{\alert}[1]{\textcolor{red}{#1}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\der}{d}

\title{Lecture 3: Functions of Symmetric Matrices}

\author{Yilin Mo}

\begin{document} \maketitle
\section{Recap}
\begin{enumerate}
  \item \emph{Bayes Estimator:}
    \begin{enumerate}
  \item {\bf Initialization:}
    \begin{displaymath}
      f(x_0|Y_{-1}) = f(x_0).
    \end{displaymath}
  \item {\bf Correction:}
    \begin{displaymath}
      f(x_k|Y_k) = \alpha f(y_k|x_k)f(x_k|Y_{k-1}),
    \end{displaymath}
    where
    \begin{displaymath}
      \alpha = \left(\int_{\mathbb R^n} f(y_k|x_k)f(x_k|Y_{k-1})\der x_k \right)^{-1}. 
    \end{displaymath}
    The MMSE estimation can be derived as
    \begin{displaymath}
      \hat x = \mathbb E(x_k|Y_k) = \int_{\mathbb R^n}x_kf(x_k|Y_k)\der x_k. 
    \end{displaymath}
  \item {\bf Prediction:}
    \begin{displaymath}
      f(x_{k+1}|Y_k) = \int_{\mathbb R^n} f(x_{k+1}|x_k)f(x_k|Y_k)\der x_k. 
    \end{displaymath}
\end{enumerate}
  \item \emph{Kalman Filter:}
    \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x_{0|-1} = 0,\,P_{0|-1} = \Sigma. 
	  \label{eq:init}
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x_{k+1|k} = A \hat x_{k|k},\,  P_{k+1|k} = AP_{k|k}A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \label{eq:correction1}
	  \hat x_{k+1|k+1} &= \hat x_{k+1|k} + P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}(y_{k+1} - C\hat x_{k+1|k}),\\
	  \label{eq:correction2}
	  P_{k+1|k+1} &= P_{k+1|k} - P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}CP_{k+1|k}.
	\end{align}
    \end{enumerate}
  \item Linear Estimator:
    \begin{enumerate}
      \item Initialization:
	\begin{displaymath}
	  \hat x_{0|-1} = 0. 
	\end{displaymath}
      \item Prediction:
	\begin{displaymath}
	  \hat x_{k+1|k} = A\hat x_{k|k}.
	\end{displaymath}
      \item Correction:
	\begin{displaymath}
	  \hat x_{k+1|k+1} = \hat x_{k+1|k} + K_{k+1}\left(y_{k+1} - C\hat x_{k+1|k}\right). 
	\end{displaymath}
    \end{enumerate}
    Estimation error covariance of the linear filter satisfies:
    \begin{align*}
      P_{0|-1} &= \Sigma,\, P_{k+1|k} = AP_{k|k}A^T + Q,\\
      P_{k+1|k+1} &= (I-K_{k+1}C) P_{k+1|k} (I-K_{k+1}C)^T + K_{k+1}RK_{k+1}.
    \end{align*}
\end{enumerate}
\section{Kalman Filtering with Intermittent Observations: Problem Formulation}

Suppose the sensor send its measurements through an erasure channel:
\begin{figure}[<+htpb+>]
  \begin{center}
    \begin{tikzpicture}
      \node (sensor) [draw,rectangle] at (0,0) {Sensor};
      \node (KF) [draw,rectangle,->] at (6,0) {KF};
      \draw [semithick,->] (sensor)--(KF);
      \draw [semithick,draw=white] (3,0)--(4,0);
      \draw [semithick] (3,0)--(3.87,0.5);
      \draw [semithick,<-] (3.75,0) arc [start angle=0, end angle=40, radius=0.75];
      \node [anchor=north] at (3.5,0) {$\gamma_k$};
      \draw [semithick,fill=white] (3,0) circle [radius=0.1];
    \end{tikzpicture}
  \end{center}
  \caption{Kalman Filtering with Intermittent Observations}
\end{figure}


Let $\gamma_k$ be a binary variable, such that $\gamma_k = 0$ implies that the KF does not receive $y_k$ and $\gamma_k = 1$ implies that the KF receives $y_k$.

We assume that $\gamma_k$ is an i.i.d. Bernoulli random variable with $P(\gamma_k = 1) = \lambda$, which is independent from $x_0,\{w_k\},\{v_k\}$. 

Hence, the information that the KF has at time $k$ is
\begin{displaymath}
 \gamma_0,\dots,\gamma_k,\gamma_0y_0,\dots,\gamma_ky_k. 
\end{displaymath}

The optimal estimator is a time varying KF:
    \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x_{0|-1} = 0,\,P_{0|-1} = \Sigma. 
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x_{k+1|k} = A \hat x_{k|k},\,  P_{k+1|k} = AP_{k|k}A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \hat x_{k+1|k+1} &= \hat x_{k+1|k} +\alert{\gamma_{k+1}} P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}(y_{k+1} - C\hat x_{k+1|k}),\\
	  P_{k+1|k+1} &= P_{k+1|k} -\alert{\gamma_{k+1}} P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}CP_{k+1|k}.
	\end{align}
    \end{enumerate}

   To simplify notations, we define
   \begin{displaymath}
     P_k \triangleq P_{k|k}.
   \end{displaymath}
   Furthermore, define
   \begin{displaymath}
     h(X) \triangleq AXA^T+Q, \,g(X) \triangleq h(X) - h(X)C^T(Ch(X)C^T+R)^{-1}Ch(X).
   \end{displaymath}
   As a result,
   \begin{displaymath}
     P_k = \begin{cases}
       h(P_{k-1})&\text{ if }\gamma_k = 0\\
       g(P_{k-1})&\text{ if }\gamma_k = 1\\
     \end{cases}
   \end{displaymath}
   $h$ is called a Lyapunov equation and $g$ is called a discrete-time algebraic Riccati equation.
\section{Properties of Discrete-time Algebraic Riccati Equation}

\subsection{Symmetric Matrix}
Let $\mathbb S^n$ be the space of real symmetric $n$ by $n$ matrices. $\mathcal S^n$ is a linear space with dimension $n(n+1)/2$.

\begin{definition}
  $\mathbb S^n_+\subset \mathbb S^n$ is the set of all positive semidefinite matrices. $\mathcal S^n_{++}\subset \mathbb S^n$ is the set of all positive definite matrices.
\end{definition}

\begin{enumerate}
  \item For any $X,Y\in \mathbb S^n_+$, $\alpha,\beta\geq 0$, $\alpha X+\beta Y\in \mathbb S^n_+$. $\mathbb S^n_+$ is a convex cone.
  \item $\mathbb S^n_+\bigcap \left(-\mathbb S^n_+\right) = \{0\}$.
\end{enumerate}

$\mathbb S^n_+$ induces a partial order on $\mathbb S^n$:
\begin{displaymath}
 X \geq Y \implies X-Y\in \mathbb S^n_+. 
\end{displaymath}

\begin{enumerate}
  \item $0\in \mathbb S^n_+\implies X\geq X $.
  \item $ \mathbb S^n_+\bigcap \left(-\mathbb S^n_+\right) = \{0\}$ implies that if $X\geq Y$ and $Y\geq X$, then $X=Y$.
  \item Convexity implies that if $X\geq Y$ and $Y\geq Z$, then $X\geq Z$. 
\end{enumerate}

However, it is not a total order:
\begin{displaymath}
  X = 0,\,Y = \begin{bmatrix} 1&0\\0&-1\end{bmatrix}. 
\end{displaymath}
Neither $X\geq Y$ nor $Y\geq X$.

\begin{theorem}
  If the sequence $\{X_k\}$ is monotonically increasing, i.e., $X_{k+1}\geq X_{k}$, and there exists an $M$, such that for all $k$, $X_k\leq M$, then the following entrywise limit is well-defined
  \begin{displaymath}
    \lim_{k\rightarrow\infty} X_k = X.
  \end{displaymath}
\end{theorem}

\begin{proof}
  \begin{itemize}
    \item \emph{Diagonal Elements:} 

      $X_{k+1}(i,i)\geq X_k(i,i)$ implies that the diagonal element $X_{k+1}(i,i)\geq X_k(i,i)$. Hence, $X_k(i,i)$ is increasing and is bounded by $M(i,i)$. Therefore $X_k(i,i)$ converges.

    \item \emph{Off-diagonal Elements:}

      Consider $k_1\geq k_2$, then $X_{k_1}\geq X_{k_2}$, which implies that all principal minor is non-negative, i.e.,
      \begin{displaymath}
	|X_{k_1}(i,j)-X_{k_2}(i,j)|^2	\leq  |X_{k_1}(i,i)-X_{k_2}(i,i)||X_{k_1}(j,j)-X_{k_2}(j,j)|
      \end{displaymath}

      Use Cauchy Criterion to prove that the off-diagonal elements also converge.

  \end{itemize}
\end{proof}
  \subsection{Functions on $\mathbb S^n$}
  \begin{definition}
    A function $f:\mathbb S^n\rightarrow\mathbb S^n$ is monotonically increasing if for any $X\geq Y$, $f(X)\geq f(Y)$. A function $f$ is decreasing if $-f$ is increasing.
  \end{definition}
  \begin{definition}
    A function $f:\mathbb S^n\rightarrow\mathbb S^n$ is convex if for any $X,\, Y$ and $\alpha,\beta>0,\alpha +\beta =1$, the following inequality holds
    \begin{displaymath}
     \alpha f(X)+\beta f(Y) \geq f(\alpha X + \beta Y).
    \end{displaymath}
    A function $f$ is concave if $-f$ is convex.
  \end{definition}

  Some functions:
  \begin{enumerate}
    \item Affine function:
      \begin{displaymath}
	h(X) = AXA^T + Q.
      \end{displaymath}
      $h(X)$ is increasing, convex and concave.
    \item Inverse function:
      \begin{displaymath}
	f(X) = X^{-1}.
      \end{displaymath}
      $f(X)$ is decreasing and convex on $\mathbb S^n_{++}$.
      \begin{proof}
	Consider $X,Y\in \mathbb S^n_{++}$. There exists an orthogonal matrix $Q_1$, such that
	\begin{displaymath}
	 Q_1XQ_1^T =  \Lambda_X,
	\end{displaymath}
	where $\Lambda_X$ is a diagonal matrix. Define $\Lambda_X^{1/2}$ as the square root of $\Lambda_X$. Hence,
	\begin{displaymath}
	 Q_1 \Lambda_X Q_1^T \times  Q_1\Lambda_X Q_1^T = X.
	\end{displaymath}
	Let $X^{1/2} = Q_1 \Lambda_X^{1/2} Q_1^T$. Then there exists another orthogonal matrix $Q_2$, such that
	\begin{displaymath}
	  Q_2 X^{-1/2} Y X^{-1/2} Q_2^T = \Lambda_Y, 
	\end{displaymath}
	On the other hand
	\begin{displaymath}
	  Q_2 X^{-1/2} X X^{-1/2} Q_2^T = I. 
	\end{displaymath}
	The proof can be done by using the matrix $Q_2 X^{-1/2}$ to diagonalize both $X$ and $Y$ and use the fact that $1/x$ is decreasing and concave on $\mathbb R^+$
      \end{proof}
    \item Discrete-time algebraic Riccati equation:

      {\bf Matrix Inversion Lemma:}
      \begin{equation}
	(A+UCV)^{-1} = A^{-1}-A^{-1}U(C^{-1}+VA^{-1}U)^{-1}VA^{-1}.
      \end{equation}
      Therefore, 
      \begin{displaymath}
	g(X) = \left[(h(X))^{-1} + C^TR^{-1}C\right]^{-1}.
      \end{displaymath}
      $g(X)$ is increasing, concave and non-negative on $\mathbb S^n_{+}$. \why

      Another way of thinking:
      
      Consider the update equation of a linear filter:
      \begin{align*}
	\varphi(X,K) &= (I-KC)h(X)(I-KC)^T + KRK^T\\
	&= K(Ch(X)C^T+R)K^T - KCh(X) - h(X)C^TK^T + h(X).
      \end{align*}
      Define $K^* = h(X)C^T (Ch(X)C^T+R)^{-1}$, then
      \begin{displaymath}
	\varphi(X,K) = g(X) + (K-K^*)(Ch(X)C^T+R)(K-K^*)^T
      \end{displaymath}
      Thus
      \begin{displaymath}
	g(X) = \min_K \varphi(X,K).
      \end{displaymath}
      Fix $K$, $\varphi(X,K)$ is increasing and affine. Thus, $g(X)$ is increasing, concave and non-negative on $\mathbb S^n_+$. \why
  \end{enumerate}
\end{document}


