\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi
\title{Distributed Estimation}

\author{Yilin Mo}

\begin{document} \maketitle

We model a network composed of $m$ agents as a graph $G = \{V,\,E\}$. $V = \{1,2,\ldots,m\}$ is the set of vertices representing the agents. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if sensor $i$ and $j$ can communicate directly with each other. We will always assume that $G$ is undirected, i.e. $(i,j)\in E$ if and only if $(j,i)\in E$. We further assume that there is no self loop, i.e., $(i,i)\notin E$.

\section{Static Case}
Let $x\in \mathbb R^n$ be the state. We assume that $x\sim \mathcal N(0,\Sigma)$. Each sensor make a measurement of $x$:
\begin{displaymath}
 y_i = h_i x + v_i.
\end{displaymath}
We will assume that $v_i\sim \mathcal N(0,R_i)$. $x,v_1,\dots,v_m$ are jointly independent from each other.

The optimal state estimate of $x$ given $y$ is
\begin{displaymath}
  \hat x = \mathbb E(x|y) = \Sigma H^T(H \Sigma H^T + R)^{-1} y,
\end{displaymath}
with error covariance
\begin{displaymath}
  P  = \Sigma - \Sigma H^T(H \Sigma H^T + R)^{-1} H \Sigma = \left(\Sigma^{-1} + H^T R^{-1} H\right)^{-1} = \left( \Sigma^{-1}  + \sum_{i=1}^m h_i^T R_i^{-1} h_i\right) ^{-1}
\end{displaymath}

Furthermore, we have
\begin{align*}
  P^{-1} \hat x &= (\Sigma^{-1} + H^T R^{-1} H)\Sigma H^T (H\Sigma H^T + R)^{-1} y\\
  & =\left( H^TR^{-1}R(H\Sigma H^T + R)^{-1} +H^T R^{-1}  H\Sigma H^T (H\Sigma H^T + R)^{-1} \right)y\\
  & = H^TR^{-1} y = \sum_{i=1}^m h_i^T R_i^{-1} y_i.
\end{align*}

Let
\begin{displaymath}
  S = \frac{1}{m}\sum_{i=1}^m h_i^T R_i^{-1} h_i,
\end{displaymath}
and
\begin{displaymath}
  z = \frac{1}{m}h_i^T R^{-1} y_i.
\end{displaymath}

$S$ and $z$ can be computed via consensus algorithm.

\begin{remark}
  $S$ is of dimension $n\times n$ and $z$ is of dimension $n$.
\end{remark}

Thus, each sensor can compute the state estimate and the corresponding error covariance matrix:
\begin{displaymath}
  P = \frac{1}{m}\left[(m\Sigma)^{-1}+S  \right] ^{-1},
\end{displaymath}
and
\begin{displaymath}
  \hat x =  \left[(m\Sigma)^{-1}+S  \right]^{-1} z.
\end{displaymath}
\section{Dynamic Case: Distributed Kalman Filter}

  \emph{Kalman Filter:}
  \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x(0|-1) = 0,\,P(0|-1) = \Sigma. 
	  \label{eq:init}
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x(k+1|k) = A \hat x(k),\,  P(k+1|k) = AP(k)A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \label{eq:correction1}
	  \hat x(k+1) &= \hat x(k+1|k) + P(k+1|k)C^T(CP(k+1|k)C^T+R)^{-1}(y(k+1) - C\hat x(k+1|k)),\\
	  \label{eq:correction2}
	  P(k+1) &= P(k+1|k) - P(k+1|k)C^T(CP(k+1|k)C^T+R)^{-1}CP(k+1|k).
	\end{align}
  \end{enumerate}
\subsection{Type I Filter: Fusion of Sensory Data}

Similar to the static case, we have 
	\begin{align*}
	  P(k+1) &= \left(P(k+1|k)^{-1} + C^T R^{-1} C\right)^{-1}\\
	  & = \left(P(k+1|k)^{-1} + \sum_{i=1}^m c_i^T R_i^{-1} c_i\right)^{-1},
	\end{align*}
and
	\begin{align*}
	  P(k+1)^{-1} \hat x(k+1) - P(k+1)^{-1}\hat x(k+1|k) &= C^T R^{-1}(y(k+1) - C\hat x(k+1|k)),\\
	  & =\sum_{i=1}^m c_i^T R_i^{-1} \left( y_i(k+1) - c_i \hat x(k+1|k) \right) \\
	  & = \sum_{i=1}^m c_i^T R_i^{-1} y_i(k+1) - \left(\sum_{i=1}^m c_i^T R_i^{-1}c_i\right) \hat x(k+1|k).
	\end{align*}

Let us define 
\begin{displaymath}
  S =  \frac{1}{m} c_i^T R_i^{-1} c_i,
\end{displaymath}
and 
\begin{displaymath}
  z(k+1) =   \frac{1}{m}\sum_{i=1}^m c_i^T R_i^{-1} y_i(k).
\end{displaymath}
Then
\begin{align*}
  P(k+1) = \frac{1}{m}\left[(mP(k+1|k))^{-1} + S\right]^{-1},
\end{align*}
and
\begin{align*}
   \hat x(k+1) = \hat x(k+1|k) + \left[(mP(k+1|k))^{-1} + S\right]^{-1}\left( z(k+1) - S \hat x(k+1|k)\right)
\end{align*}

\begin{remark}
  Infinite amount of communication is needed in order to reach consensus on $S$ and $z(k+1)$.
\end{remark}
\subsection{Type II Filter: Consensus on the State Estimate}
For each sensor $i$:
  \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x_i(0|-1) = 0,\,P_i(0|-1) = \Sigma. 
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x_i(k+1|k) = A \hat x_i(k),\,  P_i(k+1|k) = AP_i(k)A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \hat x_i(k+1) &= \hat x_i(k+1|k) + P_i(k+1|k)C^T(CP_i(k+1|k)C^T+R)^{-1}(y_i(k+1) - C\hat x_i(k+1|k))\nonumber\\
	  &+ \comment{\varepsilon P_i(k+1)^{-1} \sum_{j\in \mathcal N_i}\left[ \hat x_j(k+1|k) - \hat x_i(k+1|k)\right]},\\
	  P_i(k+1) &= P_i(k+1|k) - P_i(k+1|k)C^T(CP_i(k+1|k)C^T+R)^{-1}CP_o(k+1|k).
	\end{align}
  \end{enumerate}
  \begin{remark}
    Each sensor runs its own Kalman filter and they try to fuse the state estimation by running consensus.
  \end{remark}

  \subsection{Type III Filter: Constant Gain Strategy}
  We know that KF has the same asymptotic performance as a constant gain filter. Hence, we may be able to forget the covariance update.

  Consider a simple case, where $x \in \mathbb R$ is a scalar. $A = 1$, $Q = q$, $c_i = 1$ and $R_i = r$.

  The KF will eventually have the same asymptotic performance as the following estimator:
  \begin{displaymath}
    \hat x(k+1) = (1-\alpha) \hat x(k) + \alpha \frac{\mathbf 1^T y(k+1)}{m},
  \end{displaymath}
  where $\alpha\in \mathbb R$ is the optimal gain.

  Now we consider a distributed strategy:
  \begin{enumerate}
    \item Each sensor computes the local $\hat x_i(k)$ based on its local measurement:
      \begin{displaymath}
	\hat x_i(k) = (1-\alpha) \hat x_i(k|k-1) + \alpha y_i(k).
      \end{displaymath}
      Denote $\hat x(k)$ to be
      \begin{displaymath}
	\hat x(k) \triangleq \begin{bmatrix}
	  \hat x_1(k)\\
	  \vdots\\
	  \hat x_m(k)
	\end{bmatrix}
      \end{displaymath}
    \item We then run consensus for $l$ times. Hence,
      \begin{displaymath}
	\hat x^+(k) = M^l \hat x(k),
      \end{displaymath}
      where $M$ is the consensus matrix.
    \item For the prediction, since $A = 1$, we have
      \begin{displaymath}
	\hat x(k+1|k) = \hat x^+(k).
      \end{displaymath}
  \end{enumerate}

  Define $P(k) \triangleq \Cov(\hat x(k) - x(k)\mathbf 1)$, $P^+(k) \triangleq \Cov(\hat x^+(k) - x(k)\mathbf 1)$.

  Thus,
  \begin{displaymath}
    P^+(k) = M^l P(k) M^{lT}.
  \end{displaymath}
  and 
  \begin{align*}
   P(k+1) &= (1-\alpha)^2P^+(k) + (1-\alpha)^2q \mathbf 1\mathbf 1^T + \alpha^2 r I\\
   & = (1-\alpha)^2 M^l P(k) M^{lT} + (1-\alpha)^2q \mathbf 1\mathbf 1^T + \alpha^2 r I.
  \end{align*}

  We only care about the asymptotic performance. Consider the case where $k\rightarrow\infty$, we have
  \begin{align*}
    \lim_{k\rightarrow\infty} P(k) &=q\mathbf 1\mathbf 1^T\sum_{k=1}^\infty  (1-\alpha)^{2k} + \alpha^2 r \sum_{k=0} ^\infty (1-\alpha)^{2k} M^{kl} M^{klT}. 
  \end{align*}
  Therefore, define the cost to be
  \begin{displaymath}
    J = \lim_{k\rightarrow\infty} \tr\left(P(k) \right)= \frac{mq(1-\alpha)^2}{1- (1-\alpha)^2} + \sum_{i=1}^m \frac{r\alpha^2 }{1-(1-\alpha)^2|\lambda_i|^{2l}},
  \end{displaymath}
  where $\lambda_i$ is the $i$th eigenvalue of $M$. 

  \begin{remark}
    In general, it is difficult to jointly design $M$ and $\alpha$. If we fix $M$, then $\alpha$ can be found by numerical methods. One interesting observation is that if we increase the number of consensus steps $l$ between each time interval, then we will increase the gain and decrease the cost $J$.
  \end{remark}
\end{document}



