\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\newif\ifcomment
%display the comments
\commenttrue
%do not display the comments, used for the submitted version
%\commentfalse
\ifcomment
\newcommand{\comment}[1]{\textcolor{blue}{#1}}
\else
\newcommand{\comment}[1]{}
\fi
\title{Non-negative Matrices and Distributed Control}

\author{Yilin Mo}

\begin{document} \maketitle

We model a network composed of $m$ agents as a graph $G = \{V,\,E\}$. $V = \{1,2,\ldots,m\}$ is the set of vertices representing the agents. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if $j$ can send information to $i$. \comment{The graph can be directed.}

Define the neighbors $\mathcal N_i$ of agent $i$ as the set of agents who can send information to $i$, i.e.,
\begin{displaymath}
  \mathcal N_i \triangleq \{j:(i,j)\in E,\,j\neq i\}.
\end{displaymath}

Suppose each agent has a state $x_i(t)$. The agent update the state based on the following update equation:
\begin{itemize}
  \item \emph{Continuous time:}
    \begin{displaymath}
      \frac{d}{dt} x_i(t) = a_{ii} x_i(t) + \sum_{j\in \mathcal N_i} a_{ij} x_j(t).
    \end{displaymath}
  \item \emph{Discrete time:}
    \begin{displaymath}
       x_i(t+1) = a_{ii} x_i(t) + \sum_{j\in \mathcal N_i} a_{ij} x_j(t).
    \end{displaymath}
\end{itemize}

We can write everything in matrix form:
\begin{itemize}
  \item \emph{Continuous time:}
    \begin{displaymath}
      \frac{d}{dt} x(t) = A x(t). 
    \end{displaymath}
  \item \emph{Discrete time:}
    \begin{displaymath}
       x(t+1) = A x(t). 
    \end{displaymath}
\end{itemize}

Question: is the system stable? Can we know the answer in a distributed fashion?

\section{Some Definitions}

Let $\mathbb R^{n\times m}_+$ and $\mathbb R^{n\times m}_{++}$ be convex cones defined as
\begin{align*}
  \mathbb R^{n\times m}_+ &\triangleq \{M\in \mathbb R^{n\times m}: M_{ij}\geq 0,\forall i,j\} \\
  \mathbb R^{n\times m}_{++}& \triangleq \{M\in \mathbb R^{n\times m}: M_{ij}> 0,\forall i,j\} 
\end{align*}
Hence, we can define
\begin{align*}
  X&\geq Y \Leftrightarrow X-Y\in \mathbb R^{n\times m}_+,\\ 
  X&> Y \Leftrightarrow X-Y\in \mathbb R^{n\times m}_{++}
\end{align*}

Moreover, we define $Q \triangleq \mathbb R^n_+ \backslash\{0\}$.

A matrix $A$ is called positive if $A > 0$. It is called non-negative if $A \geq 0$. It is called a Metzler matrix if all the off-diagonal entries are non-negative, i.e., $A = B-sI$, where $B$ is non-negative.

Some observations:
\begin{itemize}
  \item If $A\geq 0$ and $x\geq 0$, then $Ax\geq 0$.
  \item On the contrary, if for all $x\geq 0$ and $Ax\geq 0$, then $A \geq 0$.
  \item Similarly, if $A> 0$ and $x\in Q$, then $Ax > 0$.
  \item On the contrary, if for all $x\in  Q$ and $Ax> 0$, then $A > 0$.
  \item If $A$ is Metzler, then $exp(At)$ is non-negative.
\end{itemize}


A matrix $A$ is called Hurwitz if all its eigenvalues have strictly negative real part. $A$ is called stable if all its eigenvalues satisfy $|\lambda|<1$.

A non-negative matrix is called primitive if there exists an $k$, such that $A^k$ is positive.

A non-negative matrix is called irreducible if for any $i,j$, there exists an $k$, such that $\left(A^k\right)_{ij}$ is positive.

In general, a matrix $A$ is called irreducible if $|A|$ is irreducible.

Define $G(A) = (V,E)$ aw the graph associated with $A$, where $V = \{1,\dots,n\}$ and $(i,j)\in E$ if and only if $a_{ij}\neq 0$.

Let the period of a vertex $i$ to be the greatest common divisor of the lengths of all cycles starting from $i$. 

Some observations:
\begin{itemize}
  \item If $A$ is irreducible then $A+I$ is primitive. 
  \item $(A^k)_{ij}> 0$ if and only if there exists a path of length $k$ from $j$ to $i$.
  \item $A$ is irreducible is equivalent to $G(A)$ to be strongly connected.
  \item If $G(A)$ is strongly connected, then all the vertices have the same period.
  \item $A$ is primitive if $A$ is irreducible and $G(A)$ has period $1$ (aperiodic).
\end{itemize}

\section{Important Properties of Non-negative matrices and Metzler matrices}
\begin{theorem}[Perron Frobenius Theorem]
  Let $A$ be an irreducible matrix, then the following propositions hold:
  \begin{enumerate}
    \item Let the spectral radius of $A$ to be $\rho(A)$, then there exists an eigenvalue $\lambda$ of $A$, such that $\lambda = \rho(A)$.
    \item $\lambda$ has geometric and algebraic multiplicity of $1$.
    \item The left and right eigenvectors of $\lambda$ is strictly positive. Any other eigenvector has negative entries.
    \item If $A$ is primitive, then all the other eigenvalues satisfy $|\lambda| < \rho(A)$.
    \item $\rho(A)$ satisfies:
      \begin{displaymath}
	\min_i \sum_{j} a_{ij} \leq \rho(A) \leq \max_i \sum_i a_{ij}.
      \end{displaymath}
  \end{enumerate}
\end{theorem}

\begin{proof}
First let us define the following function $L:Q\rightarrow \mathbb R_+$:
\begin{displaymath}
  L(x)\triangleq \max \{s:sx\leq  Ax\}.
\end{displaymath}

Clearly $L(\alpha x) = L(x)$ for any $\alpha > 0$. Define $P = (I+A)^k$, where $k$ is large enough such that $P$ is positive. Hence, if $sx \leq Ax$,
\begin{displaymath}
P(sx)\leq PAx = APx,  
\end{displaymath}
which implies that 
\begin{displaymath}
 L(Px) \geq L(x). 
\end{displaymath}
Furthermore, if $L(x)x \neq Ax$, then $L(Px) > L(x)$.

Now define:
\begin{displaymath}
  \lambda \triangleq \max\{L(x):\|x\|_2 = 1,\,x\in Q\}.
\end{displaymath}

Suppose $\lambda$ is achieve at $v$. Then $\lambda v = A v$. (otherwise $L(Pv)\geq L(v)$.) Hence, $\lambda$ is an eigenvalue of $A$ with a positive eigenvector $v$.

Applying the same procedure to $A^T$, since the spectral radius of $A$ is the same as $A^T$, we can find a strictly positive left eigenvector of $A$. Let us denote it as $w$.

Now let $\mu\neq \lambda$ be an eigenvalue of $A$ with eigenvector $y$. Then
\begin{displaymath}
 w^T A y = \lambda w^T y = \mu w^T y. 
\end{displaymath}
Hence, $w^Ty = 0$, which implies that $y$ must have negative entries. Furthermore,
\begin{displaymath}
  |\mu| |y| = |A y| \leq A |y|.
\end{displaymath}
Hence, $|\mu|\leq L(|y|)\leq \lambda$, which finishes the proof of item 1.

To prove item 2, one can consider
\begin{displaymath}
 \left. \frac{d}{d\lambda} \det(\lambda I - A)\right|_{\lambda = \rho(A)},
\end{displaymath}
and prove that it is strictly positive. The detail is omitted. Please check the reference.

If $A$ is primitive, then $A^k$ is positive. Clearly the eigenvalues of $A^k$ is the $k$-th power of the eigenvalues of $A$. Hence, without loss of generality, we can assume that $A$ is positive and $\rho(A) =1$ to prove item 4. Let $y$ be an eigenvalue of $A$ with corresponding eigenvalue $\mu$, where $|\mu| = 1$, then
\begin{displaymath}
 z = A|y| -  |y|\geq 0.
\end{displaymath}
Suppose that $z \neq 0$, then 
\begin{displaymath}
 Az > 0,  
\end{displaymath}
which implies that there exists an $\varepsilon > 0$, such that
\begin{displaymath}
 Az \geq \varepsilon A|y|,
\end{displaymath}
which is equivalent to
\begin{displaymath}
  \frac{A}{1+\varepsilon} A|z| \geq A|z|. 
\end{displaymath}
Thus, for all $k$, 
\begin{displaymath}
  \left( \frac{A}{1+\varepsilon} \right)^k A|z| \geq A|z|,
\end{displaymath}
which contradicts with the fact that $\rho(A) = 1$. As a result, $z = 0$. Thus,
\begin{displaymath}
  |y| = A |y|,\,\text{ and } y=Ay.
\end{displaymath}
Hence, $y$ is either all non-negative or all non-positive, which implies that $y$ is just a scalar multiplication of $v$.

Now to prove item 5 we have
\begin{displaymath}
  L(\mathbf 1) = \min_i\sum_j a_{ij} \leq \lambda, 
\end{displaymath}
and
\begin{displaymath}
  A \mathbf 1 \leq \left(\max_i\sum_j a_{ij}\right)\mathbf 1. 
\end{displaymath}
Hence,
\begin{displaymath}
 w^T A \mathbf 1  = \lambda w^T\mathbf 1\leq \left(\max_i\sum_j a_{ij}\right)w^T\mathbf 1, 
\end{displaymath}
which implies that $\lambda\leq \max_i\sum_j a_{ij}$.
\end{proof}

For a general $A$ matrix, to prove it is stable, we need to consider a Lyapunov function of the following form:
\begin{displaymath}
 V(x) = x^T P x, 
\end{displaymath}
where $P$ is positive definite and $A^TPA - P$ is negative definite. Since there is no guarantee that $P$ is diagonal (or comply with the network topology), this criterion cannot be easily distributed.

However if $A$ is non-negative and irreducible, then we have
\begin{theorem}
  If $A$ is non-negative and irreducible, then $A$ is stable if and only if there exists a positive $w\in \mathbb R^n$ and $0<\delta <1$, such that
  \begin{equation}
   w^T A < \delta w^T. 
   \label{eq:lyapunovcondition}
  \end{equation}
  The corresponding Lyapunov function is given by
  \begin{displaymath}
   V(z) = w^T |z|. 
  \end{displaymath}
\end{theorem}
\begin{proof}
  ``if'': \eqref{eq:lyapunovcondition} is equivalent to
  \begin{displaymath}
   V(Az) < \delta V(z). 
  \end{displaymath}
  ``only if'': If $A$ is stable, then we can choose $w$ as the left eigenvector associated with $\lambda = \rho(A)$.
\end{proof}

We can generalize this result to continuous time and consider Metzler matrix. Assuming that $A$ is a Metzler matrix with $A = B - sI$, where $B$ is irreducible. Hence, $A$ is Hurwitz if and only if $\rho(B) < s$, which is equivalent to the existence of a positive $w$, such that
\begin{displaymath}
  w^TB < sw^T\Leftrightarrow w^TA < 0.
\end{displaymath}
To see this, let $v$ be the right eigenvector associated with $\rho(B)$, then
\begin{displaymath}
 w^TBv = \rho(B) w^Tv < s w^T v,
\end{displaymath}
which implies that $\rho(B) < s$. Thus, we have the following theorem:
\begin{theorem}
  If $A$ is Meltzer and irreducible, then $A$ is Hurwitz if and only if there exists a positive $w\in \mathbb R^n$, such that
  \begin{equation}
   w^T A < 0. 
   \label{eq:lyapunovcondition2}
  \end{equation}
  The corresponding Lyapunov function is given by
  \begin{displaymath}
   V(z) = w^T |z|. 
  \end{displaymath}
\end{theorem}

\comment{Eq~\eqref{eq:lyapunovcondition} and \eqref{eq:lyapunovcondition2} can be verified in a distributed fashion.}
\end{document}



