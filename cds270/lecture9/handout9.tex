\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\title{Variants of Average Consensus}

\author{Yilin Mo}

\begin{document} \maketitle
\section{Finite Time Average Consensus}

Consider the following update equation:
\begin{equation}
 x(k+1) = (I-\alpha(k) L)x(k), 
\end{equation}
We know if we fix $0<\alpha(k) <2/\lambda_n(L)$, then
\begin{displaymath}
  \prod_{k=0}^\infty(I-\alpha(k) L) = J = \frac{\mathbf 1\mathbf 1^T}{n}.
\end{displaymath}

Assume that $L = U \Lambda U^T$, then
\begin{displaymath}
  \prod_{k=0}^{n-2} = U\begin{bmatrix} 
    1& & & \\
    &f(\lambda_2(L))&&\\
    &&\ddots&\\
    &&&f(\lambda_n(L))
  \end{bmatrix} U^T,
\end{displaymath}
where $f(x)$ is an $n-1$th degree polynomial of the following form:
\begin{displaymath}
  f(x) = \prod_{k=0}^{n-2}(1-\alpha(k)x) .
\end{displaymath}
Hence, if we choose $\alpha(0) = 1/\lambda_2(L)$,\dots,$\alpha(n-2) = 1/\lambda_n(L)$, then $f(\lambda_i(L))=0$, for any $i=2,\dots,n$. Thus, we can reach consensus in $n-1$ steps.

In general, if we do not know all the eigenvalues of $L$, but suppose that we know $\lambda_i(L)\in [a,b]$, for all $i=2,\dots,n$. Further assume that we can use a periodic $\alpha(k)$, with $\alpha(k+T) = \alpha(k)$, then the problem becomes finding a $T$th polynomial $f(x)$, such that
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{f(x)}&
  & \max_{x\in [a,b]} |f(x)|\\
  &\textrm{subject to}&
  & f(0) = 1 \\
  &&&f(x)\text{ is a }T\text{th degree polynomial}
\end{align*}
If $T = 1$, then the best function is $f(x) = 1-\frac{2}{a+b}x $.

For higher $T$, $f(x)$ will be a scaled and shifted Chebyshev polynomial, which gives
\begin{displaymath}
  \alpha(k) = \frac{b-a}{2} \cos\left(  \frac{2k+1}{2T} \pi\right)+  \frac{a+b}{2} ,k=0,\dots,T-1.
\end{displaymath}
\section{Consensus with Noise}
We use the following consensus scheme:
\begin{displaymath}
  x(k+1) = (I-\alpha L) x(k),
\end{displaymath}
where $L$ is the Laplacian matrix and $\alpha >0$. Hence,
\begin{equation}
  x_i(k+1) = (1 - d_i\alpha) x_i(k) + \alpha \sum_{j\in \mathcal N_i} x_j(k),
  \label{eq:nodeupdate}
\end{equation}
where $\mathcal N_i$ is the set of the neighboring node of $i$ and $d_i = |\mathcal N_i|$ is the degree of node $i$.
 
Notice that $x_j(k)$ in \eqref{eq:nodeupdate} is the message received by node $i$ from node $j$. Now consider that instead of receiving $x_j(k)$, the node is receives $z_{ij}(k)$, which is a noisy version of $x_j(k)$:
\begin{displaymath}
  z_{ij}(k) = x_j(k) + w_{ij}(k). 
\end{displaymath}
Hence, \eqref{eq:nodeupdate} becomes:
\begin{displaymath}
  x_i(k+1) = (1 - d_i\alpha) x_i(k) + \alpha \sum_{j\in \mathcal N_i} x_j(k) + \alpha \sum_{j\in \mathcal N_i} w_{ij}(k).
\end{displaymath}

Define
\begin{displaymath}
  v(k) = \begin{bmatrix}\sum_{j\in \mathcal N_1} w_{1j}(k)\\\vdots\\\sum_{j\in \mathcal N_n} w_{nj}(k)\end{bmatrix}.
\end{displaymath}
Therefore,
\begin{displaymath}
 x(k+1) = (I-\alpha L)x(k) + \alpha w(k), 
\end{displaymath}
where we assume that $w(k)$ is i.i.d., zero mean and has a bounded second moment. The covariance of $w(k)$ is defined as $Q$

If $\alpha$ is fixed, then we cannot achieve consensus. Hence, we need to use a time varying $\alpha(k)$.

\begin{equation}
 x(k+1) = (I-\alpha(k) L)x(k) + \alpha(k) v(k), 
 \label{eq:matrixform}
\end{equation}

We choose $\alpha(k) \geq 2/(\lambda_2(L)+\lambda_n(L))$ to satisfies the following condition:
\begin{enumerate}
  \item $\sum_{k=0}^\infty \alpha(k) = \infty$.
  \item $\sum_{k=0}^\infty \alpha(k)^2 < \infty$.
\end{enumerate}

Condition 2 implies that $\alpha(k)\rightarrow 0$.

One possible choice $\alpha(k) = 1/(k+1)$. 
\begin{displaymath}
  \frac{1}{1^2}+ \frac{1}{2^2}+\frac{1}{3^2} = \frac{\pi^2}{6}<0.
\end{displaymath}
In fact, we can choose $\alpha(k) = (k+1)^{-\varphi}$, for any $0.5<\varphi<1$.

Define $y(k) = x(k) - J x(k)$. (Notice that this definition is different from our previous one, where $y(k) = x(k) - J x(0)$. \emph{why?}) Define $\theta(k) = \mathbf 1^T x(k)/n$. Hence, $x(k) = \theta(k)\mathbf 1 + y(k)$.

By \eqref{eq:matrixform}, we have
\begin{displaymath}
 \theta(k+1) = \theta(k) + \alpha(k) \mathbf 1^Tv(k)/n. 
\end{displaymath}
Hence, for any $k_1 > k_2$,
\begin{displaymath}
  \mathbb E(\theta(k_1) -\theta(k_2))^2 = \frac{\mathbf 1^T Q\mathbf 1}{n^2}\sum_{t=k_2}^{k_1-1}\alpha(t)^2.
\end{displaymath}
Hence, $\theta(k)$ converges in $L_2$. Define $\theta$ as the $L_2$ limit of $\theta_k$.

Now let us look at $y(k)$. By \eqref{eq:matrixform},
\begin{displaymath}
 y(k+1) = [(I - \alpha(k)L)(I-J)]y(k) + \alpha(k) (I-J)v(k).
\end{displaymath}

Let us define $\mathcal P(k) = (I - \alpha(k)L)(I-J)$. Therefore,
\begin{displaymath}
  y(k+1) = \prod_{t=0}^k \mathcal P(t) y(0) + \sum_{\tau=0}^k\left(\prod_{t = \tau+1}^k \mathcal P(t)\right)\alpha(\tau)(I-J)v(\tau).
\end{displaymath}

Clearly,
\begin{displaymath}
 \| \mathcal P(k)\|  = 1-\alpha(k)\lambda_2(L).
\end{displaymath}
Hence,
\begin{displaymath}
  \left\|\prod_{t=k_1}^{k_2}\mathcal P(t)\right\| \leq  \prod_{t=k_1}^{k_2} (1-\alpha(t)\lambda_2(L))\leq  \prod_{t=k_1}^{k_2} \exp\left(-\alpha(t)\lambda_2(L)\right) = \exp\left(-\sum_{t=k_1}^{k_2}\alpha(t)\lambda_2(L)\right),
\end{displaymath}
which implies that
\begin{displaymath}
  \lim_{k\rightarrow \infty} \prod_{t=0}^{k}\mathcal P(k)= 0.
\end{displaymath}
On the other hand,
\begin{displaymath}
  \mathbb E \left\|\left(\prod_{t = \tau+1}^k \mathcal P(t)\right)\alpha(\tau)(I-J)v(\tau)\right\|^2 \leq\beta \exp\left(-2\sum_{t=\tau+1}^{k}\alpha(t)\lambda_2(L)\right)\alpha(\tau)^2 .
\end{displaymath}
where $\beta = \tr\left( (I-J)Q(I-J) \right)$. Hence
\begin{align*}
&\mathbb E\left\| \sum_{\tau=0}^k\left(\prod_{t = \tau+1}^k \mathcal P(t)\right)\alpha(\tau)(I-J)v(\tau)\right\|^2 =  \sum_{\tau=0}^k\mathbb E\left\|\left(\prod_{t = \tau+1}^k \mathcal P(t)\right)\alpha(\tau)(I-J)v(\tau)\right\|^2\\
& \leq \beta\sum_{\tau=0}^k\left[ \exp\left(-2\sum_{t=\tau+1}^{k}\alpha(t)\lambda_2(L)\right)\alpha(\tau)^2\right]\rightarrow 0.
\end{align*}
Hence, $y(k)\rightarrow 0$. As a result, $x(k)$ converges to $\theta\mathbf 1$ in the mean square sense ($L_2$).

\end{document}


