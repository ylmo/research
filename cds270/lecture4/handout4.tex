\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,plotmarks,shapes.geometric}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\why}{({\bf why?})}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}
\newcommand{\alert}[1]{\textcolor{red}{#1}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\der}{d}

\title{Lecture 4: Kalman Filtering with Intermittent Observations}

\author{Yilin Mo}

\begin{document} \maketitle
The update equation of $P_k$:
\begin{displaymath}
  P_k = (1-\gamma_k) h(P_{k-1}) + \gamma_k g(P_{k-1}). 
\end{displaymath}
$\{P_k\}$ is a stochastic process depending on $\{\gamma_k\}$.
\section{Critical Value}
We want to characterize $\mathbb EP_k$. The main difficulty is that $g$ is not affine, hence
\begin{displaymath}
  \mathbb EP_k \alert{\neq} (1-\gamma_k) h(\mathbb EP_{k-1}) + \gamma_k g(\mathbb EP_{k-1}). 
\end{displaymath}
As a result, we want to approximate the function $g$ using linear functions:
\begin{displaymath}
 0\leq g(X)\leq \varphi(X,K). 
\end{displaymath}
\begin{enumerate}
  \item lower bound for $P_k$:
    \begin{displaymath}
      L_k = (1-\gamma_k) h(L_{k-1}),\,L_0 = P_0.
    \end{displaymath}
    \begin{theorem}
      $L_k\leq P_k$, for all $k$.
      \label{theorem:lower}
    \end{theorem}
    \begin{proof}
     Use the monotonicity and non-negativity of $h$ and $g$.
    \end{proof}
  \item upper bound for $P_k$:

    \begin{displaymath}
      U_k = (1-\gamma_k) h(U_{k-1}) + \gamma_k \varphi(U_{k-1},K), \,U_0 = P_0.
    \end{displaymath}
    \begin{theorem}
      $U_k\geq P_k$, for all $k$.
      \label{theorem:upper}
    \end{theorem}
    \begin{proof}
     Use the monotonicity of $h$ and $g$ and the fact that $g(X)\leq \varphi(X,K)$ for any $K$.
    \end{proof}
\end{enumerate}

Now we have
\begin{displaymath}
  \mathbb EL_k = (1-\lambda)h(\mathbb EL_{k-1}),
\end{displaymath}
and
\begin{displaymath}
      \mathbb E U_k = (1-\lambda) h(\mathbb EU_{k-1}) + \lambda \varphi(\mathbb EU_{k-1},K).
\end{displaymath}
By Theorem~\ref{theorem:lower} and \ref{theorem:upper}, we have
\begin{displaymath}
 \mathbb E L_k \leq \mathbb E P_k \leq \mathbb E U_k. 
\end{displaymath}
Now we consider whether $\mathbb EP_k$ is bounded. 
\begin{itemize}
  \item If $A$ is stable, then the following trivial estimator $\hat x_{k|k} = 0$ is stable. Hence $\mathbb EP_k$ is bounded.
  \item If $A$ is unstable, $(A,C)$ is observable and $(A,Q^{1/2})$ is controllable:
    \begin{itemize}
      \item If $\lambda = 1$, we goes back to the classical case, $\mathbb EP_k$ is bounded. 
      \item If $\lambda = 0$, then the estimator does not receive any measurements, $\mathbb E P_k$ is unbounded.
    \end{itemize}
\end{itemize}
Therefore, if $A$ is unstable, $(A,C)$ is observable and $(A,Q^{1/2})$ is controllable, then there exists a critical value $\lambda_c$, such that
\begin{itemize}
  \item If $\lambda > \lambda_c$, then $\mathbb EP_k$ is bounded.
  \item If $\lambda < \lambda_c$, then $\mathbb EP_k$ is unbounded.
\end{itemize}
\subsection{Boundedness of $\mathbb E L_k$}

\begin{theorem}
  $\mathbb E L_k$ is bounded if and only if there exists an $X >0$, such that
  \begin{equation}
   (1-\lambda)h(X) \leq X. 
   \label{eq:lyapunov}
  \end{equation}
\end{theorem}
\begin{proof}
  If $\lambda = 1$, then theorem is trivial. Consider the case where $\lambda < 1$.

  First suppose that \eqref{eq:lyapunov} is true. For any $\mathbb EL_0$, there exists an $\alpha \geq 1$, such that $\alpha X \geq\mathbb E L_0$. Define $X_0 = \alpha X$ and $X_k = (1-\lambda)h(X_{k-1})$. Therefore
  \begin{displaymath}
   X_k \geq \mathbb E L_k. 
  \end{displaymath}
  On the other hand, by \eqref{eq:lyapunov}
  \begin{displaymath}
   X_1 =\alpha (1-\lambda)A X A' + (1-\lambda)Q =  \alpha h(X) - (1-\lambda)(\alpha-1)Q\leq \alpha X = X_0
  \end{displaymath}
  Thus $\{X_k\}$ is decreasing\why. Hence, $X_k$ is bounded, which implies that $\mathbb EL_k$ is bounded.

  Now suppose that $\mathbb EL_k$ is bounded. As a result, let us choose $\mathbb L_0 = 0$. Hence,
  \begin{displaymath}
   \mathbb L_1 = (1-\lambda)h(\mathbb E L_0) \geq 0 = \mathbb L_0. 
  \end{displaymath}
  Thus $\{\mathbb EL_k\}$ is increasing\why. On the other hand $\{\mathbb EL_k\}$ is bounded. Hence the following limit is well defined
  \begin{displaymath}
    X = \lim_{k\rightarrow\infty} \mathbb E L_k = \sum_{k=0}^\infty (1-\lambda)^{k+1} A^k Q (A^k)^T.
  \end{displaymath}
  and
  \begin{displaymath}
   X = (1-\lambda)h(X). 
  \end{displaymath}
  Only need to prove that $X > 0$. If $(A,Q^{1/2})$ is controllable, then $(\sqrt{1-\lambda}A,(1-\lambda)Q)$ is also controllable. Hence, $X > 0$ is full rank.
\end{proof}

\begin{displaymath}
 X \geq (1-\lambda) h(X) = (1-\lambda) AXA^T + (1-\lambda)Q, 
\end{displaymath}
has a positive definite solution if and only if $\sqrt{1-\lambda}A$ is stable, i.e.,
\begin{displaymath}
  \sqrt{1-\lambda} \rho(A) < 1\implies \lambda > 1-\frac{1}{\rho(A)^2}. 
\end{displaymath}
Hence, $\lambda_c \geq 1-\rho(A)^{-2}$.

\subsection{Boundedness of $\mathbb E U_k$}
\begin{theorem}
  $\mathbb E U_k$ is bounded if and only if there exists an $X >0$, such that
  \begin{equation}
   (1-\lambda)h(X) + \lambda \varphi(X,K) \leq X. 
   \label{eq:lyapunov2}
  \end{equation}
\end{theorem}
Define
\begin{displaymath}
  \overline \lambda \triangleq \inf\{\lambda\in [0,1]:\text{there exists }K,X>0\text{ such that Eq~\eqref{eq:lyapunov2} holds}\}.
\end{displaymath}
Then $\lambda_c \leq \overline \lambda$.
\subsubsection{Special Case: $C$ invertible}
If $C$ is invertible (or in general if $C$ is of rank $n$), then we can choose $K = C^{-1}$. As a result, 
\begin{displaymath}
  \varphi(X,C^{-1}) = (I-C^{-1}C)h(X)(I-C^{-1}C)^T + C^{-1}RC^{-T} =  C^{-1}RC^{-T}.
\end{displaymath}
Therefore, \eqref{eq:lyapunov2} becomes
\begin{displaymath}
  (1-\lambda)h(X) + \lambda C^{-1}RC^{-T} \leq X. 
\end{displaymath}
Therefore, if $\lambda > 1-\rho(A)^{-2}$, then the above equation has a positive definite solution. Hence, $\overline \lambda \leq 1-\rho(A)^{-2}$, which implies that $\lambda_c = 1-\rho(A)^{-2}$.
\section{Observability in NCS}
If $C$ is invertible, then the critical value $\gamma_c = 1-\rho(A)^{-2}$.

However, for general systems, this may not be true. For the following system, one can prove that critical value is $1-\rho^{-4}$.

\begin{equation}
  A = \begin{bmatrix}\rho&0\\0&-\rho\end{bmatrix},\,C = \begin{bmatrix}1&1\end{bmatrix}. 
\end{equation}

The reason being that although $(A,C)$ is observable, $(A^2,C)$ (or in general $(A^{2k},C)$) is not observable.

\begin{itemize}
  \item $C$ invertible implies that we can reconstruct the state $x_k$ using only 1 measurements: $\hat x_{k|k} = C^{-1} y_k$. 
  \item $(A,C)$ observable implies that we can reconstruct the state $x_k$ using at most $n$ sequential measurements $y_k,y_{k-1},\dots,y_{k-n+1}$.
\end{itemize}

\begin{theorem}
  If the linear system satisfies:
  \begin{enumerate}
    \item $A$ is diagonalizable;
    \item $(A^r,C)$ is observable for any $r \in \mathbb R^+$,
  \end{enumerate}
   then the critical value is given by
  \begin{displaymath}
    \lambda_c = 1-\frac{1}{\rho(A)^2}.
  \end{displaymath}
\end{theorem}

Condition 1 and 2 are called non-degeneracy condition and essentially they implies that we can reconstruct the state $x_k$ using any $n$ measurements.


\section{References on Kalman Filter with Intermittent Observations}
\begin{itemize}
  \item The original paper on critical value: \cite{Sinopoli2004}
  \item The concept of non-degeneracy and its relationship with critical value: \cite{Mo2012}
  \item A counter example where the critical value of an observable but degenerate system is not the lower bound: \cite{Mo2008}
  \item A special case where one can drive the pdf of $P_k$: \cite{Censi2009}
  \item Contraction properties of Riccati and Lyapunove equation: \cite{Censi2011}
  \item A survey paper on networked control problem: \cite{Hespanha2007}
\end{itemize}
\bibliographystyle{plain}
\bibliography{bib}
\end{document}


