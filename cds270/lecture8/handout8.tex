\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}

\title{Average Consensus}

\author{Yilin Mo}

\begin{document} \maketitle
\section{Average Consensus Algorithm}

We model a network composed of $n$ agents as a graph $G = \{V,\,E\}$. $V = \{1,2,\ldots,n\}$ is the set of vertices representing the agents. $E \subseteq V\times V$ is the set of edges. $(i,j)\in E$ if and only if sensor $i$ and $j$ can communicate directly with each other. We will always assume that $G$ is undirected, i.e. $(i,j)\in E$ if and only if $(j,i)\in E$. We further assume that there is no self loop, i.e., $(i,i)\notin E$. The neighborhood of sensor $i$ is defined as
\begin{equation}
  \mathcal N(i) \triangleq \{j\in V:(i,j)\in E\}. 
\end{equation}

A path $p = (v_0,v_1)(v_1,v_2)\dots(v_{l-1},v_l)$ is a sequence of edges, such that each $(v_k,v_{k+1})\in E$.

A graph is called connected if for any pair $i,j \in V$, there always exists a path that connects $i$ and $j$. 

 Suppose that each agent has an initial state $x_{i}(0)$. At each iteration, sensor $i$ will communicate with all its neighbors and update its state according to the following update equation
\begin{equation}
  x_{i}(k+1) = p_{ii} x_{i}(k) + \sum_{j\in \mathcal N(i)} p_{ij} x_{j}(k).  
  \label{eq:singleupdate}
\end{equation}

Let us define the vector $x(k) \triangleq [x_{1}(k),\ldots,x_{N}(k)]'\in \mathbb R^n$ and matrix $P \triangleq \left[p_{ij}\right] \in \mathbb R^{n\times n}$. Now we can rewrite \eqref{eq:singleupdate} in its matrix form as
\begin{equation}
  x_{k+1} = P x_k.  
  \label{eq:matrixupdate}
\end{equation}

Let us define the average vector to be
\begin{equation}
  x_{ave} \triangleq \frac{\mathbf 1' x(0)}{N} \mathbf 1,  
\end{equation}
where $\mathbf 1\in \mathbb R^n$ is a vector whose elements are all equal to $1$. Also let us define the error vector $y(k)$ to be
\begin{equation}
  y(k) \triangleq x(k) - x_{ave} . 
\end{equation}
The goal of average consensus is to guarantee that $y(k)\rightarrow 0$ as $k \rightarrow \infty$ through the update equation \eqref{eq:matrixupdate}.


Let us arrange the eigenvalues of $P$ in decreasing order as $\lambda_1(P)\geq \lambda_2(P)\ldots\geq \lambda_n(P)$. 
\begin{theorem}
  The following conditions are necessary and sufficient in order to achieve average consensus from any initial condition $x(0)$:
\begin{enumerate}
  \item $ \lambda_1(P) = 1$ and  $|\lambda_i(P)| < 1$ for all $i = 2,\ldots, N$.
  \item $P\mathbf 1 = \mathbf 1$, i.e. $\mathbf 1$ is an eigenvector of $P$.
  \item $\mathbf 1^T P = \mathbf 1^T$, i.e. $\mathbf 1$ is also a left-eigenvector of $P$.
\end{enumerate}
\end{theorem}
\begin{proof}
First, suppose condition 1-3 hold. Hence, $P$ can be written as
\begin{displaymath}
  P = J + Q. 
\end{displaymath}
where $J = \mathbf 1\mathbf 1^T/n$ and $Q$ is stable and 
\begin{displaymath}
 J B = B J = 0.
\end{displaymath}
Hence
\begin{displaymath}
  \lim_{k\rightarrow\infty}P^k = J +\lim_{k\rightarrow\infty} Q^k = J.
\end{displaymath}
On the other hand, suppose that $y(k)\rightarrow 0$ for any initial condition $x(0)$. As a result
\begin{displaymath}
  \lim_{k\rightarrow\infty} P^k= J.
\end{displaymath}
However, 
\begin{equation}
  P^k =\sum_{i=1}^n \lambda_i^k w_iv_i^T ,
  \label{eq:Ppower}
\end{equation}
where $w_i,v_i$ are the right and left eigenvectors of $P$.

(we assume $P$ is diagonalizable. The proof can be revise to consider $P$ has a Jordan form.)

Hence, condition 1-3 hold.
\end{proof}

Define the convergence rate $\rho$ as
\begin{displaymath}
  \rho \triangleq \lim_{k\rightarrow\infty}\sup_{y(0)\neq 0} \sqrt[k]{\frac{\|y(k)\|_2}{\|y(0)\|_2}}
\end{displaymath}

By \eqref{eq:Ppower}, $\rho= \max(\lambda_2(P),-\lambda_n(P))$.
\section{Fast Convergence via Convex Optimization}
We want to solve the following problem:
\begin{align*}
  &\mathop{\textrm{minimize}}\limits_{P}&
  & \rho\\
  &\textrm{subject to}&
  & \mathbf 1^TP =\mathbf 1^T \\
  &&&P\mathbf 1 = \mathbf 1\\
  &&&a_{ij} = 0\text{ if }(i,j)\notin E\text{ and } i\neq j.
\end{align*}
In general, this problem is very difficult for arbitrary $P$, since $\rho$ is not a convex function of $P$. 

In general, the largest eigenvalue is not a convex function. For example,
\begin{displaymath}
  \rho\left(\begin{bmatrix} 0&\alpha\\ \beta&0\end{bmatrix}\right) =\sqrt{\alpha\beta}.\; \rho\left(\begin{bmatrix} 0&(\alpha+\beta)/2\\ (\alpha+\beta)/2&0\end{bmatrix}\right) = (\alpha+\beta)/2.
\end{displaymath}
However, if $P$ is assumed to be symmetric, then the problem is a convex optimization problem and can be solved efficiently.

\section{Laplacian based Consensus}

The degree of sensor $i$ is defined as 
\begin{equation}
  d_i \triangleq |\mathcal N(i)|. 
\end{equation}

A graph is called $d$-regular graph if all the vertices have the same degree $d$,i.e. $d_{min} = d_{max} = d$. 

Now we can define the Laplacian matrix $L$ of graph $G$ as
\begin{equation}
  L \triangleq D - A,  
\end{equation}
where $D = diag(d_1,\ldots,d_n)$ is the degree matrix. $A$ is the adjacency matrix, $a_{ij} = 1$ if and only if $(i,j)\in E$. 
\begin{theorem}
  $L$ is positive semidefinite. Furthermore, $L$ has an eigenvalue 0 and the corresponding eigenvector $\mathbf 1$. As a result, arrange the eigenvalues of $L$ in the ascending order:
\begin{equation}
0 = \lambda_1(L)\leq \lambda_2(L)\leq\cdots\leq \lambda_n(L).  
\end{equation}
Furthermore the graph $G$ is connected if and only if $\lambda_2(L) >0$ is strictly positive.
\end{theorem}
\begin{proof}
Assume $v = [v_1,\dots,v_n]^T\in \mathbb R^n$, then
\begin{displaymath}
  v^TLv = \frac{1}{2}\sum_{(i,j)\in E} (v_i - v_j)^2\geq 0.
\end{displaymath}
Hence, $L$ is positive semidefinite and has a eigenvalue $0$ and the corresponding eigenvector $\mathbf 1$. 

If the graph is connected, then $v^TLv=0$ implies $v = \mathbf 1$. Hence, $\lambda_2(L) > 0$. If the graph is disconnected, then we can construct a $v\neq \mathbf 1$, such that $v^TLv = 0$. Hence, $\lambda_2(L) = 0$.
\end{proof}

We now have the following corollary:
\begin{corollary}
  There exists an $P$ satisfies condition 1-3 if and only if $G$ is connected.
\end{corollary}
\begin{proof}
If $G$ is not connected, then clearly consensus cannot be achieved.

On the other hand, if $G$ is connected, then we can choose $P = I - \alpha L$, where $\alpha < 2/\lambda_n(L)$.
\end{proof}

Since $\rho = \max (\lambda_2(P),-\lambda_n(P)$, if we consider the $P$ of the form $I - \alpha L$, then the optimal $\alpha$ is given by
\begin{displaymath}
  \alpha^* = \frac{2}{\lambda_2(L)+\lambda_n(L)} 
\end{displaymath}
and
\begin{displaymath}
  \rho^* = \frac{\lambda_n(L)-\lambda_2(L)}{\lambda_n(L) + \lambda_2(L)}. 
\end{displaymath}

\section{Laplacian from some graph}
\subsection{Complete Graph}
\begin{displaymath}
L = nJ + nI
\end{displaymath}
Hence, $L$ has eigenvalue $0$ with multiplicity $1$ and eigenvalue $n$ with multiplicity $n-1$. 
\subsection{Complete Bipartite graph $K_{a,b}$}
\begin{displaymath}
  L = \begin{bmatrix}bI_a&-\mathbf 1_{a\times b}\\-\mathbf 1_{b\times a}&aI_b\end{bmatrix} 
\end{displaymath}
The eigenvalues are
\begin{displaymath}
 0,a,b,a+b 
\end{displaymath}
with multiplicities
\begin{displaymath}
 1,b-1,a-1,1. 
\end{displaymath}
The corresponding eigenvectors are
\begin{displaymath}
  \mathbf 1_{a+b},\,\begin{bmatrix}v\\\mathbf 0\end{bmatrix} ,\,\begin{bmatrix}\mathbf 0\\w\end{bmatrix} ,\, \begin{bmatrix}b\mathbf 1_a \\-a\mathbf 1_b\end{bmatrix},
\end{displaymath}
where $\mathbf 1_a v^T = 0$ and $\mathbf 1_b w^T=0$.
\subsection{Cayley Graph}
Let $H = (V,*)$ be a group and $S = S^{-1}$ be a symmetric set. We can define a graph $G = (V,E)$, such that
\begin{displaymath}
  (x,y)\in E\Leftrightarrow x^{-1}y \in E.	 
\end{displaymath}

Cayley graph is $d$-regular with $d = |S|$. For example,
\begin{itemize}
  \item $H=(\mathbb Z,+)$ and $S = \{-1,1\}$ is an infinite line.
  \item $H=(\mathbb Z_n,+)$ and $S = \{1,n-1\}$ is a cyclic graph.
\end{itemize}

We consider the Cayley graph generated by $H = (\mathbb Z_n,+)$ and $S$. 

\begin{theorem}
  Define $\omega = \exp(2j\pi/n)$.
  \begin{displaymath}
    \lambda_k(L) = |S|-\sum_{s\in S}\omega^{ks},
  \end{displaymath}
  with eigenvector 
  \begin{displaymath}
    \begin{bmatrix}1& \omega^{k}&\dots&\omega^{(n-1)k}\end{bmatrix}^T.
  \end{displaymath}
\end{theorem}

In general, one can consider 
\begin{displaymath}
  f_k(i) = \omega^{ki}. 
\end{displaymath}
Hence, for any $x,y\in \mathbb Z_n$, $f_k(x)f_k(y) = f_k(x+y)$. Such an $f_k$ is called a character of the graph $G$.

The above theorem can be generalized to
\begin{theorem}
  For any Cayley graph of group $H$ and symmetric set $S$. Define vector
  \begin{displaymath}
    \begin{bmatrix}\chi(1),\chi(2),\dots,\chi(n)\end{bmatrix}^T,
  \end{displaymath}
  where $\chi$ is a character of $G$. Then $v$ is an eigenvector with eigenvalue:
  \begin{displaymath}
     |S|-\sum_{s\in S}\chi(s),
  \end{displaymath}
\end{theorem}

The nice thing about Cayley graph is that we can construct an infinitely many $d$-regular graphs, called Ramanujan Graphs, which satisfy
\begin{displaymath}
  \lambda_2(L)\geq d-2\sqrt{d-1},\,\lambda_n(L)\leq d+2\sqrt{d-1}.
\end{displaymath}
Hence, the convergence rate of a consensus algorithm on these graphs is given by 
\begin{displaymath}
  \rho^* \leq 2\frac{\sqrt{d-1}}{d},
\end{displaymath}
which does not grow with respect to the number of node $n$.
\end{document}


