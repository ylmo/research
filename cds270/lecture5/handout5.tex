\documentclass{article}

\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{amsthm}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,plotmarks,shapes.geometric}
\usepackage{pgfplots}

\newtheorem{proposition}{Proposition}
\newtheorem{theorem}{Theorem}
\newtheorem{definition}{Definition}
\newtheorem{lemma}{Lemma}
\newtheorem{conjecture}{Conjecture}
\newtheorem{corollary}{Corollary}
\newtheorem{remark}{Remark}
\newtheorem{assumption}{Assumption}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{tikz/#1.tikz}
\newcommand{\why}{({\bf why?})}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}
\newcommand{\alert}[1]{\textcolor{red}{#1}}

\DeclareMathOperator*{\argmin}{arg\; min}     % argmin
\DeclareMathOperator*{\argmax}{arg\; max}     % argmax
\DeclareMathOperator*{\tr}{tr}     % trace
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\der}{d}

\title{Lecture 5: Control Over Lossy Networks}

\author{Yilin Mo}
\begin{document} \maketitle
\section{Classical LQG Control}
The system:
\begin{align*}
  x_{k+1} &= A x_k + B u_k + w_k,\\
  y_k & = C x_k + v_k
\end{align*}
$x_0 \sim \mathcal N(0,\Sigma)$, $w_k \sim \mathcal N(0,Q)$, $v_k \sim \mathcal N(0,R)$.

Information available for the controller at time $k$:
\begin{displaymath}
 Y_k = (y_0,\dots,y_k). 
\end{displaymath}

The control at time $k$ is a function of the information $Y_k$: $u_k(Y_k)$.

The goal of a finite horizon LQG problem is to find a controller that minimizes the following quadratic cost:
\begin{displaymath}
  J(N) =\min_{u_0,\dots,u_N} \mathbb E \sum_{k=0}^N\left(  x_k^T W x_k + u_k^T U u_k \right).
\end{displaymath}
\subsection{Optimal Estimator Design}
Since the system is linear, the following Kalman filtering equations holds:
    \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x_{0|-1} = 0,\,P_{0|-1} = \Sigma. 
	  \label{eq:init}
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x_{k+1|k} = A \hat x_{k} + B u_k,\,  P_{k+1|k} = AP_{k}A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \label{eq:correction1}
	  \hat x_{k+1} &= \hat x_{k+1|k} + P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}(y_{k+1} - C\hat x_{k+1|k}),\\
	  \label{eq:correction2}
	  P_{k+1} &= P_{k+1|k} - P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}CP_{k+1|k}.
	\end{align}
    \end{enumerate}
    And we have that
    \begin{displaymath}
      \mathbb E(x_k|Y_k) = \hat x_{k},\,\Cov(x_k|Y_k) = P_{k}. 
    \end{displaymath}
    One important thing to notice: the $P_k$ is independent from $u_k$. This is because the system is linear and hence we can subtract $u_k$. 

\subsection{Optimal Controller Design and Separation Principle}

Now we can try to solve the optimal control design problem by dynamical programming.

Define the value function
\begin{displaymath}
  V(t) = \min_{u_t,\dots,u_N} \mathbb E \left[ \sum_{k=t}^N\left(  x_k^T W x_k + u_k^T U u_k \right) \right]
\end{displaymath}
Clearly
\begin{displaymath}
 J(N) =  V(0), 
\end{displaymath}
and
\begin{displaymath}
 V(N) =\mathbb E x_N^TWx_N.
\end{displaymath}
Now, by Bellman equation
\begin{equation}
  V(t) = \min_{u_t} \mathbb E\left( x_t^T W x_t + u_t^T U u_t + V(t+1)\right)
  \label{eq:bellman}
\end{equation}

We will guess that 
\begin{equation}
  V(t) = \mathbb Ex_t^T S_t x_t + c_t
  \label{eq:valuefunction}
\end{equation}

To prove this, we will use induction.

Clearly $S_N = W$ and $c_N = 0$.

Now suppose \eqref{eq:valuefunction} holds for $t+1$, and we look at the following quantity: 
\begin{align*}
  \mathbb E&\left( x_t^T W x_t + u_t^T U u_t + V(t+1)\right)\\
  &= \mathbb E x^T (W + A^TS_{t+1}A) x+ \tr(S_{t+1}Q)+ c_{t+1} \\
  &+ \mathbb E \left[  u_t^T (U + B^T S_{t+1} B) u_t + x_t^TA^TS_{t+1}Bu_t + u_t^TB^TS_{t+1}Ax_t\right]
\end{align*}
Notice that the controller do not know $x_t$. Hence, let us rewrite $x_t$ as
\begin{displaymath}
 x_t = \hat x_t + x_t - \hat x_t = \hat x_t + e_t.
\end{displaymath}
\begin{theorem}
  \begin{enumerate}
    \item $e_k$ is independent of $Y_k$ and hence $\hat x_k$ and $u_k$. 
    \item 
      \begin{displaymath}
	\mathbb E\hat x_k^T S\hat x_k = \mathbb Ex_k^TSx_k - \tr(SP_k)
      \end{displaymath}
  \end{enumerate}
\end{theorem}
\begin{proof}
  \begin{enumerate}
    \item Notice that
      \begin{displaymath}
	\mathbb E(e_k|Y_k) = \mathbb E(x_k|Y_k) - \mathbb E(\hat x_k|Y_k) = \hat x_k - \hat x_k = 0. 
      \end{displaymath}
      Hence, $e_k$ is \alert{linearly} independent from $Y_k$. Since $e_k$ and $Y_k$ are jointly Gaussian, $e_k$ and $Y_k$ are independent.
    \item Since
      \begin{align*}
	 \mathbb Ex_k^TSx_k &= \mathbb E\hat x_k^T S\hat x_k  + \mathbb E\hat x_k^T Se_k + \mathbb Ee_k^T S\hat x_k + \mathbb E e_k^T S e_k = \mathbb E\hat x_k^T S\hat x_k + 0 + 0 + \tr (SP_k)
      \end{align*}
  \end{enumerate}
\end{proof}

Now let us look at

\begin{align*}
  \mathbb E &\left[  u_t^T (U + B^T S_{t+1} B) u_t + x_t^TA^TS_{t+1}Bu_t + u_t^TB^TS_{t+1}Ax_t\right]\\
  &=\mathbb E\left[ u_t^T (U + B^T S_{t+1} B) u_t + \hat x_t^TA^TS_{t+1}Bu_t + u_t^TB^TS_{t+1}A\hat x_t\right]\\
  & = \mathbb E\left[(u_t - u_t^*)^T (U+B^TS_{t+1}B)(u_t - u_t^*) -\hat x_t^T A^TS_{t+1}B(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A\hat x_t\right]
\end{align*}
where $u_t^* = -(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A \hat x_t$. Hence
\begin{align*}
  V(t) &= \mathbb Ex_t^T(W + A^TS_{t+1}A - A^TS_{t+1}B(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A)x_t\\
  &+ c_{t+1} + \tr(A^TS_{t+1}B(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A P_k) + \tr(S_{t+1}Q)
\end{align*}
Therefore
\begin{equation}
 S_t =  W + A^TS_{t+1}A - A^TS_{t+1}B(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A,
 \label{eq:riccati}
\end{equation}
and
\begin{displaymath}
  c_t = c_{t+1} + \tr(A^TS_{t+1}B(U+B^TS_{t+1}B)^{-1}B^TS_{t+1}A P_k) + \tr(S_{t+1}Q).
\end{displaymath}
Thus, 
\begin{displaymath}
 J(N) = \mathbb E(x_0^T S_0 x_0 ) + c_0 = \tr(S_0\Sigma) + c_0. 
\end{displaymath}

\subsection{Infinite Horizon LQG problem}

Define $J$ as
\begin{displaymath}
  J = \lim_{N\rightarrow\infty}\frac{J(N)}{N} .
\end{displaymath}
We consider the problem of finding a controller that minimizes the infinite horizon cost $J$.

Notice that \eqref{eq:riccati} is a Riccati equation. Hence, if $N\rightarrow\infty$, then $S_k$ converges to $S$, which is the fixed solution of
\begin{equation}
 S =  W + A^TSA - A^TSB(U+B^TSB)^{-1}B^TSA,
\end{equation}
The optimal controller is given by
\begin{displaymath}
u_k^* = -(U+B^TSB)^{-1}B^TSA \hat x_k.
\end{displaymath}

\section{Witsenhausen's Counterexample}

Consider $x_0\sim \mathcal N(0,\sigma^2)$.
\begin{enumerate}
  \item The first player knows $x_0$ and he computes an 
    \begin{displaymath}
     x_1 = f(x_0), 
    \end{displaymath}
    which is a function of $x_0$.
  \item The first player sends $x_1$ to the second player though a noisy channel. Therefore, the second player receives
    \begin{displaymath}
     y_2 = x_1 + v, 
    \end{displaymath}
    where $v\sim\mathcal N(0,1)$.
  \item The second player then computes $x_2 = g(y_2)$.
\end{enumerate}
The goal is to minimize the following cost function
\begin{displaymath}
  J = \min_{f,g}\; \mathbb E \;k^2(x_0 - x_1)^2 + (x_1 - x_2)^2
\end{displaymath}
Alternatively, one can consider the following equivalent scheme:
\begin{enumerate}
  \item The controller knows $x_0$ and it computes an control $u$
    \begin{displaymath}
     u = f(x_0), 
    \end{displaymath}
    which is a function of $x_0$. 
  \item The state of the system satisfies the following update equation:
    \begin{displaymath}
     x_1 = x_0 + u. 
    \end{displaymath}
  \item  The second player observe the system via a noisy sensor: 
    \begin{displaymath}
     y_2 = x_1 + v, 
    \end{displaymath}
    where $v\sim\mathcal N(0,1)$.
  \item The second player then computes the state estimate $x_2 = g(y_2)$.
\end{enumerate}

The goal is to minimize the following cost function
\begin{displaymath}
  J = \min_{f,g}\; \mathbb E \;k^2u^2 + (x_1 - x_2)^2
\end{displaymath}
\subsection{Optimal linear strategy}
We adopt the first setting. Consider that both $f(x) = \lambda x$ and $g(x) = \mu x$ are linear, then
\begin{displaymath}
  J = \min_{\lambda, \mu} \mathbb E\; k^2 (1-\lambda)^2x_0^2 + (\lambda x_0  -\mu (\lambda x_0+v))^2
\end{displaymath}
Therefore,
\begin{displaymath}
  \mu = \frac{\lambda^2\sigma^2}{1+\lambda^2\sigma^2}, 
\end{displaymath}
and 
\begin{displaymath}
  \lambda = \argmin_{\lambda} \;k^2\sigma^2 (1-\lambda)^2 + \frac{\lambda^2\sigma^2}{1+\lambda^2\sigma^2}. 
\end{displaymath}
If $k^2\sigma^2 = 1$ and $k \rightarrow 0$, then $\lambda \approx 1$ and $J \approx 1$.
\section{Nonlinear strategy}
One can prove that for small $k$ and $k^2\sigma^2 = 0$, the following design is better than the linear design:
\begin{displaymath}
  f(x) = \sigma\text{sgn}(x),\,g(x) = \sigma\frac{1-e^{-2\sigma x}}{1+e^{-2 \sigma x}}. 
\end{displaymath}
\section{Control Over Lossy Networks}
The system:
\begin{align*}
  x_{k+1} &= A x_k +\nu_k B u_k + w_k,\\
  y_k & = C x_k + v_k
\end{align*}
$x_0 \sim \mathcal N(0,\Sigma)$, $w_k \sim \mathcal N(0,Q)$, $v_k \sim \mathcal N(0,R)$.

$\nu_k$ is an i.i.d. Bernouli process with $P(\nu_k = 1) = \lambda$.

The goal of a finite horizon LQG problem is to find a controller that minimizes the following quadratic cost:
\begin{displaymath}
 J = \min_{u_0,\dots,u_N} \lim_{N\rightarrow\infty} \frac{1}{N} \mathbb E \sum_{k=0}^N\left(  x_k^T W x_k + \nu_k u_k^T U u_k \right).
\end{displaymath}
\subsection{TCP case}
Information available for the controller at time $k$:
\begin{displaymath}
 \mathcal I_k = (y_0,\dots,y_k,\nu_0,\dots,\nu_k). 
\end{displaymath}

$J$ is finite if and only if the following Riccati equation has a positive semidefinite solution:

\begin{equation}
  S =  W + A^TSA - \alert{\lambda} A^TSB(U+B^TSB)^{-1}B^TSA,
  \label{eq:riccati2}
\end{equation}

Optimal Filter:
    \begin{enumerate}
      \item {\bf Initialization:}
	\begin{equation}
	  \hat x_{0|-1} = 0,\,P_{0|-1} = \Sigma. 
	\end{equation}
      \item {\bf Prediction:} 
	\begin{equation}
	  \hat x_{k+1|k} = A \hat x_{k} +\nu_k B u_k,\,  P_{k+1|k} = AP_{k}A^T + Q. 
	\end{equation}
      \item {\bf Correction:}
	\begin{align}
	  \hat x_{k+1} &= \hat x_{k+1|k} + P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}(y_{k+1} - C\hat x_{k+1|k}),\\
	  P_{k+1} &= P_{k+1|k} - P_{k+1|k}C^T(CP_{k+1|k}C^T+R)^{-1}CP_{k+1|k}.
	\end{align}
    \end{enumerate}

Optimal Control:

The optimal controller is given by
\begin{displaymath}
u_k^* = -(U+B^TSB)^{-1}B^TSA \hat x_k,
\end{displaymath}
where $S$ is the solution of \eqref{eq:riccati2}.

\subsection{UDP case}

Information available for the controller at time $k$:
\begin{displaymath}
 Y_k = (y_0,\dots,y_k). 
\end{displaymath}
 We do not know whether $u_k$ has been applied to the system or not. The control actually affect the estimation performance. The optimal control law and the stability of the system is unknown.
\end{document}


