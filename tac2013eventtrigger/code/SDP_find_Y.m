%SDP, solving the suboptimal Y
clear all;
close all;
clc;

m = 2; n = 2;
%A = [0.9 0 0;0 0.77 0; 0 0 0.85];
%C = [1 0 0;0 1.2 0; 0 0 0.9];
A=[0.8 1;0 0.95];
C=[0.5 0.3; 0 1.4];
Q = eye(n);
inQ = inv(Q);
R = eye(n); 
inR = inv(R);
L = chol(R);
I = eye(n);

subrate=zeros(1,50);
kappa=zeros(1,50);
lbound=zeros(1,50);

[XX,YY,ZZ]=dare(A',C',Q,R,zeros(n),eye(n));

for i=3:50
    Delta=XX+0.02*i*eye(n);%[2.8 0.7075;0.7075 2.3] ;%%P=[1.6089 0.7075;0.7075 2.1838]
    invDelta=inv(Delta);

    Px = dlyap(A,Q);      %asymptotic covariance of x
    Pi = C * Px * C' + R; %asymptotic covariance of y

    PU = chol(Pi);
    lgPi = log(det(Pi));
    lginvR = log(det(inv(R)));
    invPi = inv(Pi);

    %constant = log((1-rate)^(-2))-lginvR-lginvR;
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    cvx_begin sdp
    variable Y(n,n) symmetric
    variable M1(n,n) symmetric
    variable M2(n,n) symmetric
    variable S(n,n) symmetric
    minimize( trace(Pi*Y) )
    subject to
        [A'*inQ*A+C'*inR*C+S A'*inQ C'*inR;
            inQ*A inQ-S zeros(n,m);
            inR*C zeros(m,n) Y+inR]>=0
        [S eye(n);
            eye(n) Delta]>=0
        Y>=0
        -S >= -inQ
    cvx_end
    kappa(i)=1/sqrt(1+trace(Pi*Y))-1/sqrt(det(eye(n)+Pi*Y));
    lbound(i)=1-1/sqrt(1+trace(Pi*Y));
    subrate(i)=1-1/sqrt(det(eye(n)+Pi*Y));
end

plot(0.02*(3:2:50),subrate(1,3:2:50),'b-O','linewidth',1.5);
hold on
plot(0.02*(3:50),lbound(1,3:50),'r-','linewidth',1.5)
xlabel('\varpi');
legend('Suboptimal \gamma^{Y^*}','Lower bound of \gamma^{opt}')
print('tac-13-1', '-dpng')