%Compare the periodic schedule, the closed-loop schedule, the open loop
%schedule and the random schedule.

close all;
clear all;
clc;

%%%% Since there is no close-form relationship between rate and  Z.
%%%% we need to find the empirical mapping from rate to Z.

% parameter setting
n = 1;
m = 1;
A = 0.8; % generate a stable A
C = 1;
Q = 1*eye(n);
sqrtQ = sqrtm(Q);
R = 1*eye(m);
sqrtR = sqrtm(R);

N = 40000;
sample = 20; % x-axis data points

Px = dlyap(A,Q);      %asymptotic covariance of x
Py = C * Px * C' + R; %asymptotic covariance of y


Z_data = zeros(1,sample);
Z_data(1)=0.01;
Z_data(2)=0.04;
Z_data(3)=0.09;

for i=1:sample
    Z_data(i)=exp(0.1*i)-1; %generates uniform Z data points
end


j=0;
h = waitbar(0,'please wait...');

rate=zeros(1,sample);
close_empiricalrate_data = zeros(1,n);
close_empiricalmse_data = zeros(1,n);
open_empiricalrate_data = zeros(1,n);
open_empiricalmse_data = zeros(1,n);
periodic_empiricalrate_data = zeros(1,n);
periodic_empiricalmse_data = zeros(1,n);
periodic_rate = zeros(1,n);
random_empiricalrate_data = zeros(1,n);
random_empiricalmse_data = zeros(1,n);
jf_empiricalrate_data = zeros(1,n);
jf_empiricalmse_data = zeros(1,n);

for i=1:sample
    
    %timer update
    j=j+1;
    waitbar(j/sample,h);
    
    
    x=zeros(1,N+1);
    Sigma = eye(n);
    x(1) = sqrtm(Sigma)*randn(1,n);
    
    y=zeros(1,N+1);

    Z = Z_data(i); 
    invZ = 1/Z;

    
    close_empiricalmse = zeros(n); %average error*error'
    close_mse = zeros(n); %average P_k
    close_empiricalrate = 0; 
    
    open_empiricalmse = zeros(n); 
    open_mse = zeros(n); 
    open_empiricalrate = 0;
 
    periodic_empiricalmse = zeros(n); 
    periodic_mse = zeros(n); 
    
    random_empiricalmse = zeros(n); 
    random_mse = zeros(n); 
    
    for k=1:N  %linear dynamic system
        x(k+1) = A * x(k) + sqrtQ * randn(1,n);
        y(k) = C * x(k) + sqrtR * randn(1,m);
    end
    
    %% closed-loop event-based MMSE under different rate
    hatx = zeros(1,n);
    P = Sigma;
    
    for k = 1:N


        %generate a uniform random variable
        zeta = rand();
        threshold = exp(-0.5*(y(k)-C*hatx)'*(Z)*(y(k)-C*hatx));
        if zeta < threshold % not send y
            F = P * C' / ( C*P*C' + R + invZ);
            hatx = hatx;
            P = P - F*C*P;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y(k);
            P = P - L*C*P;
            close_empiricalrate = close_empiricalrate + 1;
        end
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        close_mse = close_mse + P;
        close_error = x(k) - hatx;
        close_empiricalmse = close_empiricalmse + close_error * close_error';
    end

    close_mse_data(i)=close_mse / N;
    close_empiricalmse_data(i) = close_empiricalmse / N;
    close_empiricalrate_data(i) = close_empiricalrate / N;

    %% open-loop event-based MMSE under different rate
    
    Y = ((1/(1-close_empiricalrate_data(i)))^2-1)/Py; % compute Y according to the empirical rate in the closed-loop case
   
    hatx = zeros(1,n);
    P = Sigma;
    
    for k = 1:N
        %prediction step

        %uniform random variable
        zeta = rand();
        threshold = exp(-0.5*y(k)'*Y*y(k));
        if zeta < threshold % not send y
            L = P * C' / ( C*P*C' + R + 1/Y);
            hatx = (eye(n) - L*C)*hatx;
            P = P - L*C*P;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y(k);
            P = P - L*C*P;
        end
        hatx = A * hatx; 
        P = A * P * A' + Q;
        open_mse = open_mse + P;
        open_error = x(k) - hatx;
        open_empiricalmse = open_empiricalmse + open_error * open_error';
    end
   
    open_mse_data(i)=open_mse / N;
    open_empiricalmse_data(i) = open_empiricalmse / N;

    %% periodic offline MMSE under different rate   
    hatx = zeros(1,n);
    P = Sigma;
    
    if i<sample/2
        for a=1:N
            if mod(a,i+1)==1 % send y
                L = P * C' / ( C*P*C' + R);
                hatx = (1 - L*C)*hatx + L*y(a);
                P = P - L*C*P;
            else % not send y
                hatx = hatx;
                P = P;
            end
            hatx = A * hatx; 
            P = A * P * A' + Q;
            periodic_mse = periodic_mse + P;
        end
    else
        for a=1:N

            if mod(a,i-sample/2+3)==1 % not send y
                hatx = hatx;
                P = P;  
            else % send y
                L = P * C' / ( C*P*C' + R);
                hatx = (1 - L*C)*hatx + L*y(a);
                P = P - L*C*P;
            end
            hatx = A * hatx; 
            P = A * P * A' + Q;
            periodic_mse = periodic_mse + P;
        end 
    end
    if i<sample/2
        periodic_rate(sample/2-i)=1/(i+1); 
        periodic_mse_data(sample/2-i)=periodic_mse / N;
    else
        periodic_rate(i)=1-1/(i-sample/2+3);
        periodic_mse_data(i)=periodic_mse / N;
    end
end

rate_rand=zeros(1,sample);
rate_rand=[close_empiricalrate_data(1):(close_empiricalrate_data(sample)-close_empiricalrate_data(1))/19:close_empiricalrate_data(sample)];


for i=1:sample
    random_empiricalmse = zeros(n); 
    random_mse = zeros(n); 
    %% random offline MMSE under different rate   
    hatx = zeros(1,n);
    P = Sigma;
    
    for k = 1:N

        if zeta < 1-rate_rand(i) % not send y
            hatx = hatx;
            P = P;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y(k);
            P = P - L*C*P;
        end
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        zeta = rand();
        random_mse = random_mse + P;
        random_error = x(k) - hatx;
        random_empiricalmse = random_empiricalmse + random_error * random_error';
    end

    random_mse_data(i)=random_mse / N;
    random_empiricalmse_data(i) = random_empiricalmse / N;
 
end

%%% plot the figures
close(h);
figure;
plot(close_empiricalrate_data,close_mse_data,'LineWidth',2);
hold on;
plot(close_empiricalrate_data,open_mse_data, 'r--','LineWidth',2);
hold on;
plot(rate_rand,random_mse_data,'k-o','LineWidth',2);
hold on;
plot(periodic_rate(3:sample-9),periodic_mse_data(3:sample-9),'g-+','LineWidth',2);
% hold on;
% plot(close_empiricalrate_data,jf_mse_data,'m-*','LineWidth',2);
legend('Closed-loop event-based schedule','Open-loop event-based schedule','Random offline schedule','Periodic offline schedule');
xlabel('\gamma');
ylabel('$\lim_{k\rightarrow\infty}\rm E[P_k^-]$','Interpreter','LaTex');
%ylabel('lim_{k\rightarrow \infty} E[P_k^-]');
grid on;