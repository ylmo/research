% plot the upper bound, the lower bound and the empirical expected error of
% covariance of the closed loop schedule
close all;
clear all;

% parameter setting
n = 2;
m = 1;
A = [1.001 1;0 0.95]; % generate an unstable A
C = [1 1];
Q = eye(n);
sqrtQ = sqrtm(Q);
R = eye(m);
sqrtR = sqrtm(R);

N = 5000;
sample2=20;


j=0;
h = waitbar(0,'please wait...');

Z_data = zeros(1,sample2);

% for i=1:sample2
%     Z_data(i)=0.1+9.9*(i-1)/sample2; %generates uniform Z data points
% end
Z_data = 0.1*[0.1 0.15 0.2 0.3 0.5 0.8 1.3 1.8 2.9 3.5 4.3 5.2 6.3 7.4 8.5 9.6 10.7 11.8 12.9 150];
rate=zeros(1,sample2);
close_empiricalrate_data = zeros(1,sample2);
close_empiricalmse_data = zeros(1,sample2);
close_mse_data=zeros(1,sample2);
upper_mse_data=zeros(1,sample2);
lower_mse_data=zeros(1,sample2);

%% close-loop event-based MMSE under different rate
for i=1:sample2
    
    %timer update
    j=j+1;
    waitbar(j/sample2,h);
    
    x=zeros(n,N+1);
    Sigma = eye(n);
    x(:,1) = sqrtm(Sigma)*randn(n,1);
    
    y=zeros(m,N+1);
    
       
    close_empiricalmse = zeros(n); %average error*error'
    close_mse = zeros(n); %average P_k
    close_empiricalrate = 0; 
 
    
    hatx = zeros(n,1);
    P = Sigma;
    
    for k=1:N  %linear dynamic system
        x(:,k+1) = A * x(:,k) + sqrtQ * randn(n,1);
        y(:,k) = C * x(:,k) + sqrtR * randn(m,1);
    end
     
    Z = Z_data(i); 
    invZ = 1/Z;
    for k = 1:N

        %uniform random variable
        zeta = rand();
        threshold = exp(-0.5*(y(:,k)-C*hatx)'*(Z)*(y(:,k)-C*hatx));
        if zeta < threshold % not send y
            F = P * C' / ( C*P*C' + R + invZ);
            hatx = hatx;
            P = P - F*C*P;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y(:,k);
            P = P - L*C*P;
            close_empiricalrate = close_empiricalrate + 1;
        end
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        
        close_mse = close_mse + P;
        close_error = x(:,k) - hatx;
        close_empiricalmse = close_empiricalmse + close_error * close_error';
    end
   
    close_mse_data(i) = trace(close_mse) / N;
    close_empiricalmse_data(i) = trace(close_empiricalmse) / N;
    close_empiricalrate_data(i) = trace(close_empiricalrate) / N;
    
    upper_mse = zeros(n); %average P_k    
    hatx = zeros(n,1);
    P = Sigma;   
       for k = 1:N


        L = P * C' / ( C*P*C' + R +1/Z);
        hatx = (eye(n) - L*C)*hatx + L*y(:,k);
        P = P - L*C*P;
        
       %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        
        upper_mse = upper_mse + P;
       end
   
    upper_mse_data(i)=trace(upper_mse) / N;
    
    upper_gamma= 1-1/sqrt(1+(C*upper_mse*C'/N+R)*Z);
    %upper_gamma= 1-1/sqrt(1+(C*C*0.5781+R)*Z);
    lower_mse = zeros(n); %average P_k
    Sigma = eye(n);

    hatx = zeros(n,1);
    P = Sigma;   
       for k = 1:N

        L = P * C' / ( C*P*C' + 1/(upper_gamma/R + (1-upper_gamma)/(R+1/Z)) );
        %L = P * C' / ( C*P*C' + R );
        hatx = (eye(n) - L*C)*hatx + L*y(:,k);
        P = P - L*C*P;
        
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        
        lower_mse =lower_mse + P;
       end
   
    lower_mse_data(i)=trace(lower_mse) / N;

end


close(h);
figure;
plot(close_empiricalrate_data,upper_mse_data, 'b-o','LineWidth',2);
hold on;
plot(close_empiricalrate_data,close_mse_data, 'k-','LineWidth',2);
hold on;
plot(close_empiricalrate_data,lower_mse_data, 'r-*','LineWidth',2);

legend('Upper bound','Closed-loop event-based schedule','Lower bound');
ylabel('$\lim_{k\rightarrow\infty}trace(\rm E[P_k^-])$','Interpreter','LaTex');
xlabel('\gamma');
%h = legend;
%set(h, 'interpreter', 'latex');
grid on;