% plot the upper bound, the lower bound and the empirical expected error of
% covariance of the open loop schedule
close all;
clear all;

n = 2;
m = 1;
A = [0.8 1;0 0.95]; % generate a stable A
C = [1 1];
Q = eye(n);
sqrtQ = sqrtm(Q);
R = eye(m);
sqrtR = sqrtm(R);

N = 50000;
sample = 30; % x-axis data points

Px = dlyap(A,Q);      %asymptotic covariance of x
Py = C * Px * C' + R; %asymptotic covariance of y

j=0;
h = waitbar(0,'please wait...');

rate=zeros(1,sample);
open_empiricalrate_data = zeros(1,sample);
open_empiricalmse_data = zeros(1,sample);
test_mse_data = zeros(1,sample);
seq = zeros(1,N);

%% open-loop event-based MMSE under different rate
for i=1:sample
    j=j+1;
    waitbar(j/sample,h);
    
    rate(i)=0.001+i/sample*0.9;
         
    Y = ((1/(1-rate(i)))^2-1)/Py; % compute Y according to the  rate
    
    x=zeros(n,N+1);
    Sigma = eye(n);
    x(:,1) = sqrtm(Sigma)*randn(n,1);
    
    y=zeros(m,N+1);
    
       
    open_empiricalmse = zeros(n); %average error*error'
    open_mse = zeros(n); %average P_k
    open_empiricalrate = 0; 
    upper_mse = zeros(n); %average P_k
    lower_mse = zeros(n); %average P_k
    
    hatx = zeros(n,1);
    P = Sigma;   
    
       for k = 1:N
        L = P * C' / ( C*P*C' + R +1/Y);
        hatx = (eye(n) - L*C)*hatx + L*y(:,k);
        P = P - L*C*P;
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;        
        upper_mse = upper_mse + P;
       end
   
    upper_mse_data(i)=trace(upper_mse) / N;
    
    hatx = zeros(n,1);
    P = Sigma;   
    
       for k = 1:N
        L = P * C' / ( C*P*C' + 1/(rate(i)/R + (1-rate(i))/(R+1/Y)) );
        hatx = (eye(n) - L*C)*hatx + L*y(:,k);
        P = P - L*C*P;
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;        
        lower_mse =lower_mse + P;
       end
   
    lower_mse_data(i)=trace(lower_mse) / N;
    
 
    
    hatx = zeros(n,1);
    P = Sigma;
    
    for k=1:N  %linear dynamic system
        x(:,k+1) = A * x(:,k) + sqrtQ * randn(n,1);
        y(:,k) = C * x(:,k) + sqrtR * randn(m,1);
    end
     
    for k = 1:N

        %uniform random variable
        zeta = rand();
        threshold = exp(-0.5*y(:,k)'*Y*y(:,k));
        if zeta < threshold % not send y
            L = P * C' / ( C*P*C' + R + 1/Y);
            hatx = (eye(n) - L*C)*hatx;
            P = P - L*C*P;
            seq(k) = 0;
        else % send y
            L = P * C' / ( C*P*C' + R);
            hatx = (eye(n) - L*C)*hatx + L*y(:,k);
            P = P - L*C*P;
            seq(k) = 1;
        end
        %prediction step
        hatx = A * hatx; 
        P = A * P * A' + Q;
        open_mse = open_mse + P;
        open_error = x(:,k) - hatx;
        open_empiricalmse = open_empiricalmse + open_error * open_error';
    end
   
    open_mse_data(i)=trace(open_mse) / N;
    open_empiricalmse_data(i) = trace(open_empiricalmse) / N;
end

close(h);
figure;
plot(rate,upper_mse_data, 'b-o','LineWidth',2);
hold on;
plot(rate,open_mse_data, 'k-','LineWidth',2);
hold on;
plot(rate,lower_mse_data, 'r-*','LineWidth',2);
hold on;
legend('Upper bound','Open-loop event-based schedule','Lower bound');
ylabel('$\lim_{k\rightarrow\infty}trace(\rm E[P_k^-])$','Interpreter','LaTex');
xlabel('\gamma');
%h = legend;
%set(h, 'interpreter', 'latex');
grid on;