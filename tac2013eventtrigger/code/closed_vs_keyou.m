% Compare the closed-loop event-based schedule with the Keyou deterministic
% schedule

close all;
clear all;

% parameter setting
T = 1;
alpha = 0.01;
var_acceleration = 5;

n = 3;
A = [1 T T^2;0 1 T; 0 0 1]; % generate a stable A
m = 3;
C = eye(m);

Q = 2*alpha*var_acceleration*[(T^5)/20 (T^4)/8 (T^3)/6;(T^4)/8 (T^3)/3 (T^2)/2; (T^3)/6 (T^2)/2 T];
sqrtQ = sqrtm(Q);
R = 1*eye(m);
sqrtR = sqrtm(R);

%Z_data = 0.047*eye(m); %smaller Z, smaller rate ///for C = eye(3)
%delta_data = [4.30]; %smaller delta, larger rate///for C = eye(3)
Z_data = 0.52*eye(m); %smaller Z, smaller rate ///for C = eye(3)
delta_data = [1.6]; %smaller delta, larger rate///for C = eye(3)

N = 100;
sample2=1;
countermax=5000;


j=0;
h = waitbar(0,'please wait...');


% for i=1:sample2
%     Z_data(i)=0.1+9.9*(i-1)/sample2; %generates uniform Z data points
% end

rate=zeros(1,sample2);
close_empiricalrate_data = zeros(1,sample2);
close_empiricalmse_data = zeros(1,sample2);
close_mse_data=zeros(1,sample2);
close_hatx_data=zeros(n,N+1);
close_P_data=zeros(1,N);
close_empiricalP_data=zeros(1,N);

keyou_empiricalrate_data = zeros(1,sample2);
keyou_empiricalmse_data = zeros(1,sample2);
keyou_mse_data=zeros(1,sample2);
keyou_hatx_data=zeros(n,N+1);
keyou_P_data=zeros(1,N);
keyou_empiricalP_data=zeros(1,N);

Sigma = 0.1*[1 1/T 0;1/T 2/(T^2) 0; 0 0 0.00001];

%% close-loop event-based MMSE under different rate
for counter=1:countermax
        % timer update
        j=j+1;
        waitbar(j/countermax,h);
        
        x(:,1) = sqrtm(Sigma)*randn(n,1);

        % generate linear dynamic system
        x=zeros(n,N+1);
        y=zeros(m,N+1);
        
        for k=1:N 
            x(:,k+1) = A * x(:,k) + sqrtQ * randn(n,1);
            y(:,k) = C * x(:,k) + sqrtR * randn(m,1);
        end

        hatx = zeros(n,1);
        P = Sigma;
        
        close_empiricalmse = zeros(n); %average error*error'
        close_mse = zeros(n); %average P_k
        close_empiricalrate = 0; 
        
      %%%%%%%%%%% proposed closed loop scheduler    %%%%%%%%%%%%%%%%%%%%%%%%%
        Z = Z_data; 
        invZ = inv(Z);
        
        for k = 1:N
           
            %prediction step
            hatx = A * hatx; 
            P = A * P * A' + Q;

            %generate a uniform random variable
            zeta = rand();
            threshold = exp(-0.5*(y(:,k)-C*hatx)'*(Z)*(y(:,k)-C*hatx));
            if zeta < threshold % not send y
                F = P * C' / ( C*P*C' + R + invZ);
                hatx = hatx;
                P = P - F*C*P;
            else % send y
                L = P * C' / ( C*P*C' + R);
                hatx = (eye(n) - L*C)*hatx + L*y(:,k);
                P = P - L*C*P;
                close_empiricalrate = close_empiricalrate + 1;
            end
            
            close_mse = close_mse + P;
            close_error = x(:,k) - hatx; 
                %close_P_data(:,k) = close_P_data(:,k) + trace(P);
            close_P_data(:,k) = close_P_data(:,k) + P(1,1);
            temp1 = close_error * close_error';
            close_empiricalP_data(:,k) = close_empiricalP_data(:,k) + temp1(1,1);
                %close_empiricalP_data(:,k) = close_empiricalP_data(:,k) + trace(temp1);
           
        end

        %close_mse_data = close_mse(1,1) / N;
        close_mse_data = trace(close_mse) / N;
        %close_empiricalmse_data = close_empiricalmse_data(i)+close_empiricalmse(1,1) / N;
        close_empiricalmse_data = close_empiricalmse_data+trace(close_empiricalmse) / N;
        %close_empiricalrate_data = close_empiricalrate_data + close_empiricalrate(1,1) / N;
        close_empiricalrate_data = close_empiricalrate_data + trace(close_empiricalrate) / N;

        %%%%%%%%% keyou's scheduler %%%%%%%%%%%%%%%%%%%%%
        hatx = zeros(n,1);
        P = Sigma;

        keyou_empiricalmse = zeros(n); %average error*error'
        keyou_mse = zeros(n); %average P_k
        keyou_empiricalrate = 0; 

        delta = delta_data; 
        
        for k = 1:N
            
            %prediction step
            hatx = A * hatx; 
            P = A * P * A' + Q;

            zk = inv(sqrt(C*P*C'+R))*(y(:,k)-C*hatx);
            if abs(zk) < delta % not send y
                L = P * C' / ( C*P*C' + R);
                hatx = hatx;
                hx = sqrt(2/pi)*delta*exp(-(delta^2)/2)/(1-2*qfunc(delta));
                P = P - hx*L*C*P;
            else % send y
                L = P * C' / ( C*P*C' + R);
                hatx = (eye(n) - L*C)*hatx + L*y(:,k);
                P = P - L*C*P;
                keyou_empiricalrate = keyou_empiricalrate + 1;
            end

            keyou_mse = keyou_mse + P;
            keyou_error = x(:,k) - hatx;
            keyou_empiricalmse = keyou_empiricalmse + keyou_error * keyou_error';
            
            keyou_hatx_data(:,k) = hatx;
            keyou_P_data(:,k) = keyou_P_data(:,k) + P(1,1);
               %keyou_P_data(:,k) = keyou_P_data(:,k) + trace(P);
            temp2 = keyou_error * keyou_error';
            keyou_empiricalP_data(:,k) = keyou_empiricalP_data(:,k) + temp2(1,1);  
                %keyou_empiricalP_data(:,k) = keyou_empiricalP_data(:,k) + trace(temp2);

        end

        %keyou_mse_data(i) = keyou_mse(1,1) / N;
        keyou_mse_data = trace(keyou_mse) / N;
        %keyou_empiricalmse_data(i) = keyou_empiricalmse_data(i)+ keyou_empiricalmse(1,1) / N;
        keyou_empiricalmse_data = keyou_empiricalmse_data+ trace(keyou_empiricalmse) / N;
        %keyou_empiricalrate_data = keyou_empiricalrate_data + keyou_empiricalrate(1,1)/ N;
        keyou_empiricalrate_data = keyou_empiricalrate_data + trace(keyou_empiricalrate)/ N;
end

close_P_data = close_P_data/countermax;
close_empiricalP_data = close_empiricalP_data/countermax;
close_empiricalrate_data= close_empiricalrate_data/countermax;
keyou_P_data = keyou_P_data/countermax;
keyou_empiricalP_data = keyou_empiricalP_data/countermax;
keyou_empiricalrate_data = keyou_empiricalrate_data/countermax;

    
Keyou_rate=keyou_empiricalrate_data
Mine_rate=close_empiricalrate_data
close(h);
figure;
subplot(1,2,1);
plot(1:N,close_empiricalP_data, 'b-.','LineWidth',2);
hold on;
plot(1:N,close_P_data, 'k-','LineWidth',2);
title('CLSET-KF');
legend('Empirical P_{11}','Theoretical P_{11}');
xlabel('Time');
axis([0 N 0 1.5]); % for high rate
%axis([0 N 0 30]); % for low rate
grid on;
subplot(1,2,2);
plot(1:N,keyou_empiricalP_data, 'b-.','LineWidth',2);
hold on;
plot(1:N,keyou_P_data, 'k-','LineWidth',2);
title('DET-KF');
legend('Empirical P_{11}','Theoretical P_{11}');
xlabel('Time');
axis([0 N 0 1.5]);% for high rate
%axis([0 N 0 30]);% for low rate
grid on;

%figure;
%plot(1:N,x(2,1:N), 'k-','LineWidth',2);
%hold on;
%plot(1:N,x(2,1:N)-close_hatx_data(2,1:N), 'r-','LineWidth',2);
%hold on;
%plot(1:N,x(2,1:N)-keyou_hatx_data(2,1:N), 'b-','LineWidth',2);