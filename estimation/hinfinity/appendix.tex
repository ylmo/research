\comment{Yilin: We need to take into account that some estimator will terminate in finite time.}

Let $T_0,\dots,T_{N-1} \in \mathbb N\cup\{\infty\}$. If $T_i = \infty$, then we define $\{x_i(k)\}_{k\in \mathbb N}$ as an infinite sequence of points in $\mathbb R^n$. Otherwise if $T_i$ is finite, then we define $\{x_i(k)\}_{k=0,\dots,T_i}$ as a finite sequence of length $T_i+1$. 

Define the index set $\mathcal L(k)$ as
\begin{displaymath}
  \mathcal L(k) \triangleq \{i: T_i \geq k\}.
\end{displaymath}

In other words, $\mathcal L(k)$ is the index set of the sequence that does not terminate before $k$.

We can define the center $c(k)$ as
\begin{displaymath}
  c(k) \triangleq \frac{1}{\left|\mathcal L(k)\right|} \sum_{i\in \mathcal L(k)} x_i(k).
\end{displaymath}
By symmetry, we assume that
\begin{displaymath}
  T_0 \geq T_1 \geq \dots\geq T_{N-1}, 
\end{displaymath}
and $T_0 = \infty$. 

\begin{theorem}
If for all $i,j$, the following inequality holds:
\begin{equation}
  \sum_{k=0}^{T_i\wedge T_j} \|x_i(k)-x_j(k)\|^2 \leq 1,
  \label{eq:l2diameter}
\end{equation}
then the following inequality holds:
\begin{equation}
  \sum_{k=0}^\infty \|x_0(k) - c(k)\|^2 \leq 0.5\log N -\frac{1}{12}.
  \label{eq:centerl2error}
\end{equation}
\label{theorem:centerl2error}
\end{theorem}
Before proving Theorem~\ref{theorem:centerl2error}, we need the following lemmas:
\begin{lemma}
  The following matrix inequality holds:
  \begin{displaymath}
    \Lambda= \begin{bmatrix}
      \lambda_1&&\\
      &\ddots&\\
      &&\lambda_n
    \end{bmatrix}\geq \mathbf 1\mathbf 1^T,
  \end{displaymath}
  if and only if for all $i$, $\lambda_i >0$, and the following inequality holds:
  \begin{equation}
    \sum_{i=1}^n \frac{1}{\lambda_i} \leq 1.
    \label{eq:inversesum}
  \end{equation}
    \label{lemma:inversesum}
\end{lemma}
\begin{proof}
  We first prove the ``only if''. If $\Lambda \geq \mathbf 1\mathbf 1^T$, then $\lambda_i > 0$ for all $i$. Otherwise, $0 = e_i^T \Lambda e_i < e_i^T \mathbf 1\mathbf 1^T e_i = 1$, where $e_i$ is the $i$th canonical basis. Therefore $\Lambda$ is invertible, by Lemma 1.1 in~\cite{Ding2007}, we have
  \begin{displaymath}
    \det(\Lambda - \mathbf 1\mathbf 1^T) =(1-\mathbf 1^T \Lambda^{-1}\mathbf 1) \det(\Lambda) = \left( 1-\sum_{i=1}^n \frac{1}{\lambda_i} \right)\prod_{i=1}^n \lambda_i.
  \end{displaymath}
  Since $\Lambda \geq \mathbf 1\mathbf 1^T$, the determinant of $\Lambda - \mathbf 1\mathbf 1^T$ is non-negative, which proves \eqref{eq:inversesum}.

  Now assume that $\lambda_i > 0$ and \eqref{eq:inversesum} holds. Denote the $k$th leading principal minor of $\Lambda -\mathbf 1\mathbf 1^T$ as $\Delta_k$. By Lemma 1.1 in~\cite{Ding2007}, we have that
  \begin{displaymath}
   \Delta_k =  \left( 1-\sum_{i=1}^k \frac{1}{\lambda_i} \right)\prod_{i=1}^k \lambda_i \geq 0,
  \end{displaymath}
  which proves that $\Lambda \geq \mathbf 1\mathbf 1^T$.
\end{proof}
\begin{lemma}
  \label{lemma:sdprelaxation}
  Consider the following semidefinite programming problem:
  \begin{align}
    \label{eq:sdprelaxation}
    &\mathop{\textrm{maximize}}\limits_{X \geq 0}&
    & \tr(F_0X)\\
    &\textrm{subject to}&
    & \tr(F_iX) = 1,\,\forall i = 1,\dots,n, \nonumber
  \end{align}
  where $F_0,F_1,\dots,F_n \geq 0$ are positive semidefinite. Suppose that \eqref{eq:sdprelaxation} is feasible and bounded from above. Denote the optimal solution of \eqref{eq:sdprelaxation} as $X^*$. Then there exists a rank one positive semidefinite matrix $X$, such that $\tr(F_iX) = 1$ for all $i=1,\dots,n$ and $\tr(F_0X) = \tr(F_0X^*)$. 
\end{lemma}
\begin{proof}
  Since $X^*$ is the optimal solution, denote $x_{ij}^*$ as the entry on the $i$th row and $j$th column of $X^*$. Let us consider the matrix
  \begin{displaymath}
    X = \begin{bmatrix}
      \sqrt{x_{11}^*}&\dots&\sqrt{x_{nn}^*}
    \end{bmatrix}^T \begin{bmatrix}
      \sqrt{x_{11}^*}&\dots&\sqrt{x_{nn}^*}
    \end{bmatrix} .
  \end{displaymath}
  It is clear that $x_{ii} = x^*_{ii}$. Thus, by the fact that $F_i$ is diagonal for all $i=1,\dots,n$, we have $\tr(F_iX) = \tr(F_iX^*) = 1$, which implies that $X$ satisfies the contraints.

  On the other hand, since $X^*$ is positive semidefinite, we have $x_{ij} = \sqrt{x_{ii}^*x_{jj}^*}\geq x_{ij}^* $. Hence, $\tr(F_0X)\geq \tr(F_0X^*)$. By the fact that $X^*$ is the optimal solution, $\tr(F_0X) = \tr(F_0X^*)$ and we can conclude the proof.
\end{proof}
\begin{lemma}
  \label{lemma:inverseopt}
  Consider the following optimization problem:
  \begin{align}
    \label{eq:inverseopt}
    &\mathop{\textrm{minimize}}\limits_{\lambda_1,\dots,\lambda_n > 0}&
    & \sum_{i=1}^n\lambda_i\\
    &\textrm{subject to}&
    & \sum_{i=1}^j\frac{1}{\lambda_i}\leq (j+1)^2,\,\forall j=1,\dots,n\nonumber.
  \end{align}
  The optimal solution is given by $\lambda_1^* = 1/4,\,\lambda_i^* = 1/(2i+1)$, for $i = 2,\dots,n$. The optimal value is given by
  \begin{equation}
    f(n) = \frac{1}{4} + \sum_{i=2}^n \frac{1}{2i+1}\leq 0.5 \log(n+1)-\frac{1}{12}
    \label{eq:oddharmonicseries}
  \end{equation}
\end{lemma}
\begin{proof}
We assume that $n \geq 2$, since the case where $n=1$ is trivial. Let us define 
\begin{displaymath}
  s_k = \sum_{i=1}^k \frac{1}{\lambda_i}.
\end{displaymath}
For simplicity, define $s_0 = 0$. Hence, \eqref{eq:inverseopt} can be rewritten as:
  \begin{align}
    \label{eq:inverseopt2}
    &\mathop{\textrm{minimize}}\limits_{0<s_1<\dots<s_n}&
    & \sum_{i=1}^n\frac{1}{s_i-s_{i-1}}\\
    &\textrm{subject to}&
    & s_i\leq (i+1)^2,\,\forall i=1,\dots,n\nonumber.
  \end{align}
  Let $\eta_i^* = 0$, $s_i^* = (i+1)^2$ for $i=1,\dots,n$. For simplicity, define $s_0^* = s_0 = 0$. Further define
  \begin{displaymath}
    \mu_i^* = \begin{cases}
      \frac{1}{16}-\frac{1}{25}&\text{ if }i=1\\
      \frac{1}{(2i+1)^2} -\frac{1}{(2i+1)^2} &\text{ if }i=2,\dots,n-1\\
      \frac{1}{(2n+1)^2}&\text{ if }i=n
    \end{cases}.
  \end{displaymath}
  One can easily verify that the KKT conditions hold:
\begin{enumerate}
  \item $s_i^* -  (i+1)^2\leq 0$ and $\mu_i^*(s_i^* - (i+1)^2) = 0$, for all $i=1,\dots,n$.
  \item $s_{i-1}^* -  s_{i}^*\leq 0$ and $\eta_i^*(s_{i-1}^* - s_i^*) = 0$, for all $i=1,\dots,n$.
  \item $\mu_i^*  \geq 0$.
  \item $\eta_i^* \geq 0$, for all $i=1,\dots,n$.
  \item Define the Lagrangian as
    \begin{displaymath}
      \mathcal L(s_1,\dots,s_n,\mu_1,\dots,\mu_n,\eta_1,\dots,\eta_n) = \sum_{i=1}^n\left[\frac{1}{s_i-s_{i-1}} + \mu_i (s_i-(i+1)^2) + \eta_i(s_{i-1}-s_{i})\right].
    \end{displaymath}
    One can verify that the following equality holds for all $i=1,\dots,n$:
    \begin{displaymath}
      \frac{d}{ds_i} \mathcal L(s_1^*,\dots,s_n^*,\mu_1^*,\dots,\mu_n^*,\eta_1^*,\dots,\eta_n^*) = 0
    \end{displaymath}
\end{enumerate}
It can be easily verified that \eqref{eq:inverseopt2} is a convex optimization problem. Thus, the KKT conditions are sufficient for optimality~\cite{Boyd2004}, which implies that $s_i^* = (i+1)^2$ is the optimal solution for \eqref{eq:inverseopt2} and hence $\lambda_1^* = 1/4$ and $\lambda_i^* = 1/(2i+1)$ for $i = 2,\dots,n$.
 
To prove \eqref{eq:oddharmonicseries}, observe that
\begin{displaymath}
  \frac{1}{2i+1}\leq\int_{t=0}^1 \frac{1}{2}\left(\frac{1}{2i+1+t}+\frac{1}{2i+1-t}\right)dt = \frac{1}{2} \int_{t=2i}^{2i+2}\frac{1}{t}dt \leq \frac{1}{2}\log(i+1)-\frac{1}{2}\log(i).
\end{displaymath}
 Therefore,
 \begin{displaymath}
   f(n) = \frac{1}{3} + \sum_{i=2}^n\frac{1}{2i+1} -\frac{1}{12} \leq 0.5(\log(1n+1) -\log(1)) -\frac{1}{12} = 0.5\log(n+1) - \frac{1}{12}.
 \end{displaymath}
\end{proof}
We are now ready to prove Theorem~\ref{theorem:centerl2error}.
\begin{proof}
  Let us denote the supremum of the LHS of \eqref{eq:centerl2error} as $M$. The proof of the theorem is divided into 4 steps:
  
  \emph{Step 1: Formulate an optimization problem that gives an upper bound of $M$.}
  
  Without loss of generality, we can assume that $x_0(k) = 0$ for all $k$. Furthermore, by triangular inequality, we have
  \begin{displaymath}
    \|x_0(k)-c(k)\| \leq \frac{1}{|\mathcal L(k)|} \sum_{i\in \mathcal L(k)}\| x_i(k)\|,
  \end{displaymath}
  which implies that
  \begin{displaymath}
    \|x_0(k)-c(k)\|^2 \leq \frac{1}{|\mathcal L(k)|^2} \left(\sum_{i\in \mathcal L(k)}\| x_i(k)\|\right)^2.
  \end{displaymath}
  Consider the following optimization problem:
  \begin{align}
    \label{eq:optcenterl2}
    &\mathop{\textrm{maximize}}\limits_{\{x_i(k)\},\,T_1\geq\dots\geq T_{N-1}}&
    & \sum_{k=0}^\infty\frac{1}{|\mathcal L(k)|^2}\left( \sum_{i\in \mathcal L(k)}\|x_i(k)\|\right)^2\\
    &\textrm{subject to}&
    & \sum_{k=0}^{T_i} \|x_i(k)\|^2\leq 1,\,\forall i=1,\dots,N-1.\nonumber
  \end{align}
  Denote the optimal value of problem~\eqref{eq:optcenterl2} as $M_1$. One can see that $M\leq M_1$. 
  
  \emph{Step 2: Manipulate \eqref{eq:optcenterl2} into a finite horizon optimization problem.}

 For simplicity, let us define $T_N = -1$. For any $1\leq i\leq N-1$ and $0\leq \tau\leq N-1-i$, let us define
  \begin{displaymath}
    s_i(\tau) = \begin{cases}
      \sqrt{ \sum_{k=T_{N-\tau}+1}^{T_{N-\tau-1}}\|x_i(k)\|^2} &\text{ if }T_{N-\tau-1} < T_{N-\tau} \\
      0&\text{ if }T_{N-\tau-1} = T_{N-\tau}.
    \end{cases}
  \end{displaymath}

  If $T_{N-\tau-1} = T_{N-\tau}$, then $s_i(\tau) = 0$ for all $i$. Thus, 
  \begin{equation}
    \label{eq:alterobj1}
   \frac{1}{(N-\tau)^2} \sum_{i=1}^{N-1-\tau} s_i(\tau) = 0.
  \end{equation}

  On the other hand, if $T_{N-1-\tau} > T_{N-\tau}$, we know that $|\mathcal L(k)| = N-\tau$, for $k\in [T_{N-1-\tau}+1 , T_{N-\tau -1}]$. Hence,
  \begin{equation}
    \label{eq:alterobj2}
    \sum_{k=T_{N-\tau}+1}^{T_{N-\tau-1}}\frac{1}{|\mathcal L(k)|^2}\left( \sum_{i\in \mathcal L(k)}\|x_i(k)\|\right) = \frac{1}{(N-\tau)^2} \left\|\sum_{i=1}^{N-\tau-1} x_i(T_{N-\tau}+1:T_{N-\tau-1}) \right\|^2,
  \end{equation}
  where
  \begin{displaymath}
    x_i(T_{N-\tau}+1:T_{N-\tau-1}) \triangleq \begin{bmatrix}
      x_i(T_{N-\tau}+1)\\
      \vdots\\
      x_i(T_{N-\tau-1})
    \end{bmatrix}.
  \end{displaymath}
  We know that $\|x_i(T_{N-\tau}+1:T_{N-\tau-1})\| = s_i(\tau)$. By Cauchy-Schwarz inequality,
  \begin{equation}
    \label{eq:alterobj3}
    \left\|\sum_{i=1}^{N-\tau-1} x_i(T_{N-\tau}+1:T_{N-\tau-1}) \right\|^2\leq\left(\sum_{i=1}^{N-\tau-1}\left \| x_i(T_{N-\tau}+1:T_{N-\tau-1})\right\|\right)^2 = \left(\sum_{i=1}^{N-\tau-1} s_i(k)\right)^2,
  \end{equation}
  and the inequality is tight when $x_i(T_{N-\tau}+1:T_{N-\tau-1})$ are scalar multiplication of each other. Combining \eqref{eq:alterobj1}, \eqref{eq:alterobj2} and \eqref{eq:alterobj3}, we know that \eqref{eq:optcenterl2} can be rewritten as 
  \begin{align}
    \label{eq:optcenterl2removeTleq}
    &\mathop{\textrm{maximize}}\limits_{s_i(\tau) \geq 0}&
    & \sum_{\tau=0}^{N-2}\frac{1}{(N-\tau)^2}\left( \sum_{i= 1}^{N-1-\tau} s_i(\tau)\right)^2\\
    &\textrm{subject to}&
    & \sum_{\tau=0}^{N-1-i} s_i(\tau)^2\leq 1,\,\forall i=1,\dots,N-1.\nonumber
  \end{align}
  Clearly, the optimal value of \eqref{eq:optcenterl2removeTleq} is attained when all the constraints are tight, which implies that the optimal value of the following problem is also $M_1$:
  \begin{align}
    \label{eq:optcenterl2removeT}
    &\mathop{\textrm{maximize}}\limits_{s_i(\tau) \geq 0}&
    & \sum_{\tau=0}^{N-2}\frac{1}{(N-\tau)^2}\left( \sum_{i= 1}^{N-1-\tau} s_i(\tau)\right)^2\\
    &\textrm{subject to}&
    & \sum_{\tau=0}^{N-1-i} s_i(\tau)^2= 1,\,\forall i=1,\dots,N-1.\nonumber
  \end{align}
 
 \emph{Step 3: Relax \eqref{eq:optcenterl2removeT} into a convex optimization problem and prove that the relaxation is exact.}

 Define $\xi \in \mathbb R^{\frac{N(N-1)}{2}}$ be a $N(N-1)/2$ dimensional vector consisted of $s_i(\tau)$s, such that
  \begin{displaymath}
    \xi \triangleq \begin{bmatrix}
      s_1(0)&\cdots&s_{N-1}(0)&s_1(1)&\cdots&s_{N-2}(1)&\cdots&s_{1}(N-2)\end{bmatrix} .
  \end{displaymath}
  One can verify that the $\tau(2N-\tau-1)/2+i$ entry of $\xi$ is given by:
  \begin{displaymath}
    \xi_{\frac{\tau}{2}(2N-\tau-1)+i} = s_i(\tau),\forall 1\leq i\leq N-1,\,0\leq \tau\leq N-1-i.
  \end{displaymath}

  Define $F_0$ as a block diagonal matrix, where
  \begin{displaymath}
    F_0 = \text{diag}\left(\frac{\mathbf 1_{N-1}\mathbf 1^T_{N-1}}{N^2},\frac{\mathbf 1_{N-2}\mathbf 1^T_{N-2}}{(N-1)^2},\dots,\frac{1}{2^2}\right),
  \end{displaymath}
  where $\mathbf 1_k$ is a $k$-dimensional all one vector. Further define $F_i$, $i=1,\dots,N-1$ as a diagonal matrix, such that the $j$th diagonal entry is $1$ if and only if
  \begin{displaymath}
    j = \frac{\tau}{2}(2N-\tau-1) + i,\text{ for some }0\leq \tau\leq N-1-i. 
  \end{displaymath}
  The $j$th diagonal entry is zero otherwise. From the above definition, we know that
  \begin{displaymath}
     \sum_{\tau=0}^{N-2}\frac{1}{(N-\tau)^2}\left( \sum_{i= 1}^{N-1-\tau}s_i(\tau)\right)^2 = \xi^T F_0 \xi = \tr(F_0\xi \xi^T),
  \end{displaymath}
  and
  \begin{displaymath}
     \sum_{\tau=0}^{N-1-i} s_i(\tau)^2 = \xi^T F_i \xi = \tr(F_i\xi\xi^T).
  \end{displaymath}
  Thus, define $X = \xi \xi^T\geq 0$. We can relax \eqref{eq:optcenterl2removeT} to
  \begin{align}
    \label{eq:sdprelaxation2}
    &\mathop{\textrm{maximize}}\limits_{X \geq 0}&
    & \tr(F_0X)\\
    &\textrm{subject to}&
    & \tr(F_iX) = 1,\,\forall i = 1,\dots,N-1 \nonumber
  \end{align}
 By Lemma~\ref{lemma:sdprelaxation}, we know that the relaxation is exact. Hence, the optimal value of \eqref{eq:sdprelaxation2} is $M_1$. 
  
  \emph{Step 4: Solve the dual problem of \eqref{eq:sdprelaxation2}.}

  The dual problem of \eqref{eq:sdprelaxation2} can be written as:
  \begin{align}
    \label{eq:sdpdual}
    &\mathop{\textrm{minimize}}\limits_{\lambda_1,\dots,\lambda_n}&
    & \sum_{i=1}^{N-1} \lambda_i\\
    &\textrm{subject to}&
    &\sum_{i=1}^{N-1} \lambda_i F_i\geq F_0. \nonumber
  \end{align}
  It is trivial to verify that there exists an $X > 0$ strictly positive, such that $\tr(F_iX) =1$ for all $i$. By Slater's condition~\cite{Boyd2004}, strong duality holds between \eqref{eq:sdprelaxation2} and \eqref{eq:sdpdual}. Thus, the optimal value of \eqref{eq:sdpdual} is also $M_1$. By the definition of $F_i$, we know that
  \begin{displaymath}
    \sum_{i=1}^{N-1} \lambda_i F_i = \diag(\Lambda_{1:N-1},\Lambda_{1:N-2},\dots,\Lambda_{1:1}),
  \end{displaymath}
  where 
  \begin{displaymath}
    \Lambda_{1:i} = \diag(\lambda_1,\dots,\lambda_i)\in \mathbb R^{i\times i}. 
  \end{displaymath}
  Hence, $\sum_{i=1}^{N-1}\lambda_i F_i \geq F_0$ is equivalent to
  \begin{equation}
    \diag(\lambda_1,\dots,\lambda_j) \geq \frac{1}{(j+1)^2} \mathbf 1\mathbf 1^T ,\,\forall j=1,\dots,N-1.
    \label{eq:matrixcomparison}
  \end{equation}
  By Lemma~\ref{lemma:inversesum}, \eqref{eq:matrixcomparison} is equivalent to
  \begin{displaymath}
    \sum_{i=1}^j\frac{1}{\lambda_i}\leq (j+1)^2,\,\lambda_i > 0,i=1,\dots,j
  \end{displaymath}
  Thus, using Lemma~\ref{lemma:inverseopt}, we can conclude the proof. 
\end{proof}

