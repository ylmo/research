function [y] = myuni(S,sample) 

y = zeros(size(S,1),sample);

for k = 1:size(S,1)
    long = -S(k,1) + S(k,2);
    beg = S(k,1); 
    y(k,:) = beg + long*rand(1,sample);  
end
end