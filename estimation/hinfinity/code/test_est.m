close all;clear all;clc;

% system paramter 
A = 1;
B = [1 0 0 0];
C = [1;1;1];
D = [0 1 0 0;0 0 1 0;0 0 0 1]; 
BD = [B;D];Dn = eye(3);Bn = 1;

% simulation variable
T = 50;
time = 1:T;
x = zeros(1,T);
w = 2*rand(4,T)-1;
a = zeros(3,T);
a(1,:) = 0.5*(0:T-1);  % attack is at node 1
y = zeros(3,T);

% state dynamics 
for t = 1:T-1
    x(t+1) = A*x(t)+B*w(:,t);
    y(:,t) = C*x(t)+D*w(:,t)+a(:,t);
end

%estimator for non-adversarial environment
theta = -1/3;
K = theta * [1 1 1];
hatx = zeros(1,T);
for t = 1:T-1
    hatx(t+1) = A*hatx(t) - K*(y(:,t)-C*hatx(t));
end
error = x - hatx;
plot(0:T-1,error,'g')

figure(2)
%resilient estimator
cL = [2 3;1 3;1 2];
mu = -0.5;
K = mu*[1 1];
hatxl = zeros(3,T);
terminate = [T+1;T+1;T+1];

for Lindex = 1:3
    for t = 1:T-1        
        cI = cL(Lindex,:);
        r = y(cI,t) - C(cI,:)*hatxl(Lindex,t);
        if norm(r,inf) > 3 & terminate(Lindex) > t
            terminate(Lindex) = t;
        end
        hatxl(Lindex,t+1) = A*hatxl(Lindex,t) - K*r;
    end
end

for i = 1:3
    hold on
    plot(0:terminate(i)-2,x(1:terminate(i)-1)-hatxl(i,1:terminate(i)-1));
    plot(terminate(i)-2:T-1,x(terminate(i)-1:T)-hatxl(i,terminate(i)-1:T),'g');
    
end

%global estimator
for t = 1:T
    cLt = find(terminate > t);
    tmp = hatxl(cLt,t);
    hatx(t) = (max(tmp)+min(tmp))/2;
end
plot(0:T-1,x-hatx,'r')
