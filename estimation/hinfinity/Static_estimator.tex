\subsection{Problem Formulation}
Assume that the state of the system $x \in \mathbb R^n$ and $m$ sensors are measuring the system:
\begin{displaymath}
  y_i = h_i x + w_i + a_i, 
\end{displaymath}
where $h_i$ is a row vector and $w_i$ is the measurement noise. $a_i$ is the bias injected by the attacker, such that $a_i\neq 0$ only when the sensor $i$ is compromised. Define $\mathcal S \triangleq \{1,\dots,m\}$ as the set of sensors. Denote
\begin{displaymath}
  y \triangleq \begin{bmatrix}
    y_1\\
    \vdots\\
    y_m
  \end{bmatrix} ,\, H \triangleq \begin{bmatrix}
    h_1\\
    \vdots\\
    h_m
  \end{bmatrix} ,\, w \triangleq \begin{bmatrix}
    w_1\\
    \vdots\\
    w_m
  \end{bmatrix} ,\,a \triangleq \begin{bmatrix}
    a_1\\
    \vdots\\
    a_m
  \end{bmatrix} 
\end{displaymath}
As a result,
\begin{displaymath}
  y = Hx + w + a. 
\end{displaymath}
In this draft, we consider a more general sensor model, defined as
\begin{equation}
  y = Hx + Gw + a,
  \label{eq:sensormodel}
\end{equation}
where $G\in \mathbb R^{m\times m}$ and is assumed to be full row rank. We further assume that the sensor noise $w \in \mathbb R^p$ is 2-norm bounded, i.e.,
\begin{displaymath}
  \|w\|_2\leq \delta. 
\end{displaymath}
We assume that the bias vector $a$ is 0-norm bounded, i.e.,
\begin{displaymath}
  \|a\|_0\leq l, 
\end{displaymath}
where $l$ indicates the number of compromised sensors.

Let us define the set $\mathbb Y$ as the set of all possible measurements, i.e.,
\begin{displaymath}
  \mathbb Y  \triangleq \{y\in \mathbb R^m:\exists \,x,\,w,\,a,\text{ such that }\|w\|_2\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
For any $y\in \mathbb Y$, we can define the set $\mathbb X(y)$ as the set of feasible $x$ that can generate $y$, i.e.,
\begin{displaymath}
  \mathbb X(y) \triangleq \{x\in\mathbb R^n:\exists \,w,\,a,\text{ such that }\|w\|_2\leq \delta,\,\|a\|_0\leq l\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
An estimator is a function $f:\mathbb Y\rightarrow \mathbb R^n$, where $\hat x = f(y)$. The magnitude of the worst case estimation error is defined as
\begin{displaymath}
  e(f) \triangleq \sup_{y\in \mathbb Y} \rho(f(y),\mathbb X(y)) .
\end{displaymath}

Clearly, from the definition of the Chebyshev center, we know that the optimal estimator with smallest $e(f)$ is given by
\begin{displaymath}
  f^*(y) \triangleq c(\mathbb X(y)). 
\end{displaymath}
The worst case error magnitude is thus given by
\begin{displaymath}
  e(f^*)\triangleq \sup_{y\in\mathbb Y} r(\mathbb X(y)).
\end{displaymath}

In the following subsections, we provides an upper and lower bound for $e(f^*)$. We further derive the optimal estimator. 

\subsection{Performance Bounds for the Optimal Estimator}
This subsection is devoted to analyzing the performance of the optimal estimator. To this end, for any index set $\mathcal I = \{i_1,\dots,i_j\}$, let us define subspace $\mathcal V_\mathcal I \triangleq \text{span}(e_{i_1},\dots,e_{i_j})\subseteq \mathbb R^m$, where $e_{i}\in \mathbb R^m$ is the $i$th canonical basis vector. Define the following set:
\begin{displaymath}
  \mathbb X_{\mathcal I}(y) \triangleq \{x\in\mathbb R^n:\exists \,w,\,a\in \mathcal V_\Ic,\text{ such that }\|w\|_2\leq \delta\text{ and }y=Hx+Gw+a\}.
\end{displaymath}
Hence, $\mathbb X_{\mathcal I}(y)$ represents all possible states that can generate measurement $y$ when the sensors in $\mathcal I$ are good and the sensors in $\mathcal I^c$ are compromised. Thus
\begin{displaymath}
  \mathbb X(y)  = \bigcup_{|\mathcal I|=m-l}\mathbb X_{\mathcal I}(y). 
\end{displaymath}
For any $\mathcal I = \{i_1,\dots,i_j\}$, we can define 
\begin{displaymath}
  H_{\mathcal I} \triangleq \begin{bmatrix}
    h_{i_1}\\
    \vdots\\
    h_{i_j}
  \end{bmatrix},\, G_{\mathcal I} \triangleq \begin{bmatrix}
    g_{i_1}\\
    \vdots\\
    g_{i_j}
  \end{bmatrix},\,y_{\mathcal I}\triangleq \begin{bmatrix}
    y_{i_1}\\
    \vdots\\
    y_{i_j}
  \end{bmatrix},
\end{displaymath}
where $g_i$ is the $i$th row vector of $G$. If $H_{\mathcal I}$ is full column rank, then we define
\begin{align*}
  \Xi_{\mathcal I} &\triangleq G_IG_I^T,\,&
  K_{\mathcal I}&\triangleq \left(H_{\mathcal I}^T\Xi_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1}H^T_{\mathcal I}\Xi_{\mathcal I}^{-1},\\
  P_{\mathcal I} &\triangleq \left(H_{\mathcal I}^T \Xi_{\mathcal I}^{-1}H_{\mathcal I}\right)^{-1},\, &
  U_{\mathcal I} &\triangleq (I-H_{\mathcal I}K_{\mathcal I})^T \Xi_{\mathcal I}^{-1}(I-H_{\mathcal I}K_{\mathcal I}).
\end{align*}
The following theorem provides bounds on $e(f^*)$:
\begin{theorem}
  If there exists $|\mathcal K| =m-2l$, such that $H_{\mathcal K}$ is not of full column rank, then $e(f^*) = \infty$. If for all $\mathcal K = m-2l$, $H_{\mathcal K}$ is full column rank, then for all possible $y\in \mathbb Y$, we have
  \begin{equation}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) = 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
    \label{eq:diameter}
  \end{equation}
  Therefore, $e(f^*)$ satisfies
  \begin{equation}
    \max_{|\mathcal K| = m-2l}\delta\sqrt{\sigma(P_{\mathcal K})}\leq e(f^*)\leq \max_{|\mathcal K| = m-2l}\delta\sqrt{2\sigma(P_{\mathcal K})},
    \label{eq:radius}
  \end{equation}
  where $\sigma(P)$ is the spectral radius of $P$.
\end{theorem}
\begin{proof}
  Let us consider a pair of set $\mathcal I,\,\mathcal J$ with cardinality $m-l$. Define $\mathcal K$ as
  \begin{displaymath}
   \mathcal K =  \mathcal I\bigcap \mathcal J .
  \end{displaymath}
  Clearly, $|\mathcal K|\geq m-2l$. Now for any point $x_1 \in \mathbb X(y,\mathcal I)$ and $x_2 \in \mathbb X(y,\mathcal J)$, we have:
  \begin{equation}
    Hx_1 +G w_1 + a_1 = H x_2 + Gw_2 + a_2 = y,  
    \label{eq:difference1}
  \end{equation}
  where $a_1\in \mathcal V_\Ic$ and $a_2\in \mathcal V_{\mathcal J^c}$. Thus, \eqref{eq:difference1} implies that
  \begin{equation}
    H_{\mathcal K}(x_1-x_2)= G_{\mathcal K}(w_2-w_1),
    \label{eq:difference2}
  \end{equation}

  Hence, if $H_{\mathcal K}$ is of full column rank, then by the fact that $\|w_2-w_1\|_2\leq 2\delta$, we have
  \begin{displaymath}
    \sigma(P_{\mathcal K})^{-1}\|x_1-x_2\|^2 \leq (x_1-x_2)^T P_{\mathcal K}^{-1} (x_1-x_2)^T \leq 4\delta^2.
  \end{displaymath}

  Hence, $\|x_1-x_2\|_2\leq 2\sqrt{\sigma(P_{\mathcal K})}$. It is easy to prove that for any $\mathcal K_1\subseteq \mathcal K_2$,
  \begin{displaymath}
    \sigma(P_{\mathcal K_1})\geq \sigma(P_{\mathcal K_2}).
  \end{displaymath}
  Therefore,
  \begin{displaymath}
    \sup_{y\in\mathbb Y} d(\mathbb X(y)) \leq 2\delta\max_{|\mathcal K| \geq m-2l}\sqrt{\sigma(P_{\mathcal K})} = 2\delta\max_{|\mathcal K| = m-2l}\sqrt{\sigma(P_{\mathcal K})}.
  \end{displaymath}
  Now we need to prove that the equality of \eqref{eq:diameter} holds. Suppose that we find $x_1,\,x_2,\,w_1,\,w_2$ that satisfies \eqref{eq:difference1} and $\|x_1-x_2\| = 2\sqrt{\sigma(P_{\mathcal K})}$. We know that
  \begin{displaymath}
    H_{\mathcal K} x_1 + G_{\mathcal K}w_1 =  H_{\mathcal K} x_2 + G_{\mathcal K}w_2.
  \end{displaymath}
  Therefore, let us create $y$, such that
  \begin{displaymath}
    y_{\mathcal K} = H_{\mathcal K}x_1,\,y_{\mathcal I^c} = H_{\mathcal I^c}x_2,\,y_{\mathcal J^c\backslash\mathcal I^c} = H_{\mathcal J^c\backslash\mathcal I^c}x_1.
  \end{displaymath}
  Thus, $x_1\in \mathbb X_{\mathcal I}(y)$ and $x_2\in \mathbb X_{\mathcal J}(y)$, which implies that \eqref{eq:diameter} holds. \eqref{eq:radius} can be proved by applying Theorem~\ref{theorem:Jung}.
\end{proof}
\subsection{Estimator Design}
In this subsection, we first characterize the shape of $\mathbb X_{\mathcal I}(y)$:
\begin{theorem}
  Define the function $V_{\I}(x):\mathbb R^n\rightarrow \mathbb R$ as the solution of the following optimization problem:
  \begin{align}
    &\mathop{\textrm{minimize}}\limits_{w\in \mathbb R^m}&
    & \|w\|^2\nonumber\\
    &\textrm{subject to}&
    & G_\I w= y_{\I}-H_\I x.  \label{eq:optstatic}
  \end{align}
  Then $V_\I(x)$ is given by
  \begin{equation}
    V_\I(x) = (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))+ \varepsilon_{\I}(y) ,
    \label{eq:valuefunctionstatic}
  \end{equation}
  where
  \begin{equation}
     \hat x_{\I}(y) = K_\I y_\I,
    \label{eq:optstaticsolution}
  \end{equation}
  and 
  \begin{equation}
    \label{eq:optstaticvalue}
    \varepsilon_{\I}(y) = y_{\I}^TU_{\I}y_{\I}.
  \end{equation}
  \label{theorem:valuefunctionstatic}
\end{theorem}
\begin{proof}
  Consider the constraint of the optimization problem~\eqref{eq:optstatic}
  \begin{equation}
    y_{\I} - H_{\I}x = G_{\I}w.
    \label{eq:reducedequation}
  \end{equation}
  As $G$ is full row rank, $G_{\I}$ is also full row rank. Consider the singular value decomposition of $G_\I$, we get
  \begin{displaymath}
    G_\I = Q_1 \begin{bmatrix} 
      \Lambda & \mathbf 0
    \end{bmatrix} Q_2,
  \end{displaymath}
  where $Q_1,Q_2$ are orthogonal matrices with proper dimensions and $\Lambda$ is an invertible and diagonal matrix. Hence, \eqref{eq:reducedequation} is equivalent to 
  \begin{equation}
    \Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I \hat x_\I(y_\I) = \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation2}
  \end{equation}
  where $v = Q_2 w$ and $\|v\|  = \|w\|$. By projecting $\Lambda^{-1} Q_1^T y_\I$ into the subspace $\text{span}(\Lambda^{-1}Q_1^T H_\I)$, we have
  \begin{equation}
    \left[\Lambda^{-1} Q_1^Ty_\I - \Lambda^{-1} Q_1^T H_\I \hat x_{\I}(y)\right] + \Lambda^{-1} Q_1^T H_\I \left[x - \hat x_{\I}(y) \right]= \begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v.
    \label{eq:reducedequation3}
  \end{equation}
  The first term on the LHS of \eqref{eq:reducedequation3} is perpendicular to the second term. Thus, \eqref{eq:reducedequation3} is equivalent to
  \begin{displaymath}
   \varepsilon_\I(y) +  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))= \left\|\begin{bmatrix} 
      I &\mathbf 0
    \end{bmatrix}v\right\|^2 \leq \|v\|^2 = \|w\|^2.
  \end{displaymath}
  Clearly, the equality holds when $v = \begin{bmatrix}v_1,\dots,v_{|\I|},0,\dots,0\end{bmatrix}$. Hence
  \begin{displaymath}
    V_{\I}(x)= \varepsilon_\I(y) +  (x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y)).
  \end{displaymath}
\end{proof}

By Theorem~\ref{theorem:valuefunctionstatic}, we immediately have the following corollary:
\begin{corollary}
  If $\varepsilon_\I(y) > \delta^2$, then $\mathbb X_{\I}(y)$ is an empty set. Otherwise, $\mathbb X_{\I}(y)$ is an ellipsoid given by
  \begin{equation}
    \mathbb X_{\I}(y) = \{x:(x-\hat x_{\I}(y))^TP_{\I}^{-1} (x-\hat x_{\I}(y))\leq \delta^2 - \varepsilon_{\I}(y)\} 
  \end{equation}
\end{corollary}
\begin{proof}
  By definition, $x\in \mathbb X_{\I}(y)$ is equivalent to the existence of $w$, such that $\|w\|\leq \delta$ and
  \begin{displaymath}
    y_{\I} = H_{\I}x + G_{\I}w.
  \end{displaymath}
  Hence, the corollary holds by Theorem~\ref{theorem:valuefunctionstatic}. 
\end{proof}
\begin{remark}
  One can view $\varepsilon_{\I}(y)$ as the deviation of the measurement from the attack model, i.e., the adversary compromises the sensors in $\Ic$. Hence, if $\varepsilon_\I(y)\geq \delta^2$, i.e., the deviation cannot be explained by the noise, then $\mathbb X_\I(y)$ is empty, which implies that the compromised sensor set is not $\Ic$. Hence, some sensors in $\I$ must be compromised. On the other hand, a small deviation gives a larger $\mathbb X_\I(y)$.
\end{remark}

Since $\mathbb X(y) = \bigcup_{|\I|=l}\mathbb X_\I(y)$, we know that $\mathbb X(y)$ is a union of ellipsoids. One can refer to \cite{Yildirm2006} for more detailed algorithm to compute the center of $\mathbb X(y)$. 

\comment{Yilin: I think his convex optimization based algorithm will definitely work. The other one should work with some modifications. need to double check. However, this is not our main contribution.}
