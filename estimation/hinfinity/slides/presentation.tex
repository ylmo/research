\documentclass{beamer}
\usepackage{subfigure}
\usepackage{amssymb, amsmath, amsfonts}
\usepackage{tikz}
\usetikzlibrary{external,matrix,arrows,fit,backgrounds,mindmap,plotmarks}

\usepackage{pgfplots}

\newlength\figureheight
\newlength\figurewidth

\ifnum\pdfshellescape=1
\tikzexternalize[prefix=tikzpdf/]
\fi

\newcommand{\tikzdir}[1]{../tikz/#1.tikz}
\newcommand{\inputtikz}[1]{\tikzsetnextfilename{#1}\input{\tikzdir{#1}}}

\newcommand{\tI}{\tilde {\mathcal I}}
\newcommand{\tA}{\tilde A}
\newcommand{\ty}{\tilde y}
\newcommand{\tx}{\tilde x}
\newcommand{\tw}{\tilde w}
\newcommand{\tv}{\tilde v}
\newcommand{\tC}{\tilde C}
\newcommand{\tP}{\tilde P}
\newcommand{\Ic}{{\mathcal I^c}}
\newcommand{\I}{{\mathcal I}}
\newcommand{\J}{{\mathcal J}}
\newcommand{\K}{{\mathcal K}}
\newcommand{\G}{{\mathcal G}}

\DeclareMathOperator{\MSE}{MSE}
\DeclareMathOperator{\rank}{rank}
\DeclareMathOperator{\Med}{Med}
\DeclareMathOperator{\Max}{Max}
\DeclareMathOperator{\Min}{Min}
\DeclareMathOperator{\tr}{tr}
\DeclareMathOperator{\Cov}{Cov}
\DeclareMathOperator{\logdet}{log\;det}
\DeclareMathOperator{\argmin}{arg\;min}
\DeclareMathOperator{\argmax}{arg\;max}
\let\Tiny\tiny

\mode<presentation>

\title[abc]{abc}
\author[Yilin Mo]{Yilin Mo}
\institute[Caltech]{Control and Dynamical Systems\\ California Institute of Technology}
\date[Dec 15, 2014]{CDC 2015}
\usetheme{CambridgeUS}
\useinnertheme{circles}
\useoutertheme{infolines}
\definecolor{caltechcolor}{RGB}{255,102,0}
\usecolortheme[RGB={255,102,0}]{structure} 
\setbeamercolor*{palette primary}{fg=caltechcolor!60!black,bg=gray!30!white}
\setbeamercolor*{palette secondary}{fg=caltechcolor!70!black,bg=gray!15!white}
\setbeamercolor*{palette tertiary}{bg=caltechcolor!80!black,fg=gray!10!white}
\setbeamercolor*{palette quaternary}{fg=caltechcolor,bg=gray!5!white}
\setbeamercolor{titlelike}{parent=palette primary,fg=caltechcolor}
%\logo{\includegraphics[height=0.75cm]{caltechlogo.pdf}}

\begin{document}

\begin{frame}
  \titlepage
\end{frame}
\section{Introduction}

\section{Problem Formulation}
\begin{frame}{System Description}
  Consider the following linear system:
  \begin{align*}
    x(k+1) &= A x(k) + Bw(k)\\
    y(k) & = Cx(k) + Dw(k),
  \end{align*}
  where $x(k)\in \mathbb R^n$ is the state, $y(k)\in \mathbb R^m$ is the measurements from sensors and $Bw(k)$ and $Dw(k)$ present process and measurement noise respectively.
\end{frame}

\begin{frame}{Estimation Problem}
  Our goal is to construct an state estimator:
  \begin{displaymath}
    \hat x(k+1) = A \hat x(k) - K \left( y(k) - C\hat x(k) \right),
  \end{displaymath}
  in order to minimize the estimation error $e(k) = x(k)-\hat x(k)$.

  \begin{itemize}
    \item Expected performance:

     $w(k)$ is Gaussian: Kalman filtering.
    \item Worst-case performance:

     $w(k)\in l_2$: $\mathcal H_\infty$ filtering.

     $w(k)\in l_\infty$: $\mathcal L_1$ filtering.
  \end{itemize}

\end{frame}

\begin{frame}[fragile]{Estimation Problem}
\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=caltechcolor!50,fill=caltechcolor!20,rounded corners, semithick,minimum size=7mm},
      point/.style={coordinate},
      every node/.append style={font=\small}
      ]
      \matrix[row sep = 5mm, column sep = 5mm]{
      %first row
      \node (B) [box] {$B$};
      &\node (noise) {$w$};
      &;
      &\node (D) [box] {$D$};
      &;
      &;
      &\\
      %second row
       \node(plant) [box] {$(zI-A)^{-1}$};
      & \node (p1) [point] {};
      &\node (C) [box] {$C$};
      & \node (plus1) [circle,draw,inner sep=2pt] {};
      &\node (estimator) [box] {Estimator};
      & \node (plus2) [circle,draw,inner sep=2pt] {};
      &\node (error) {$e$};\\
      %third row
      ;
      &;
      &;
      &;
      &;
      &;
      &;\\
      %fourth row
      ;
      &\node (p2) [point] {};
      &;
      &;
      &;
      &\node (p3) [point] {};
      &\\
      };
      \draw [semithick] (plus1.north)--(plus1.south);
      \draw [semithick] (plus1.east)--(plus1.west);
      \draw [semithick] (plus2.north)--(plus2.south);
      \draw [semithick] (plus2.east)--(plus2.west);
      \draw [semithick,->] (noise)--(B);
      \draw [semithick,->] (B)--(plant);
      \draw [semithick,->] (plant)-- node[midway,above]{$x$} (C);
      \draw [semithick,->] (p1)--(p2)--(p3)--node[very near end,right]{$+$} (plus2);
      \draw [semithick,->] (C)--(plus1);
      \draw [semithick,->] (noise)--(D);
      \draw [semithick,->] (D)--(plus1);
      \draw [semithick,->] (plus1)--node[midway,above]{$y$} (estimator);
      \draw [semithick,->] (estimator)--node[midway,below]{$\hat x$}
      node[very near end,above] {$-$} 
      (plus2);
      \draw [semithick,->] (plus2)--(error);
    \end{tikzpicture}
  \end{center}
\end{figure}
\end{frame}

\begin{frame}{Estimation Performance}
  \begin{itemize}
    \item The estimation error and the residue $r(k) = y(k) - C\hat x(k)$ satisfies:
      \begin{align*}
	e(k+1) &= (A+K C) e(k) + (B+K D) w(k),\\
	r(k) &=C e(k) + D w(k).
      \end{align*}
    \item Let us define the linear mappings $ E(K)$ and $R(K)$ as:
      \begin{displaymath}
	E(K):w\rightarrow e,\,R(K):w\rightarrow r.
      \end{displaymath}
    \item Then we can compute the induced norm of $E(K)$ and $R(K)$ and thus
      \begin{displaymath}
	\|e\|_\infty \leq \|E(K)\| \|w\|_\infty,\, \|r\|_\infty \leq \|R(K)\| \|w\|_\infty.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Important Observation}
  Consider the following system:
  \begin{displaymath}
   x(k+1) = Ax(k) + B w(k),\,y(k) = Cx(k) + D w(k). 
  \end{displaymath}
  If $(A,C)$ is detectable and $y(0:k) = 0$, then we can build an estimator:
\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=caltechcolor!50,fill=caltechcolor!20,rounded corners, semithick,minimum size=7mm},
      point/.style={coordinate},
      every node/.append style={font=\small}
      ]
      \matrix[row sep = 5mm, column sep = 5mm]{
      %first row
      \node (B) [box] {$B$};
      &\node (noise) {$w$};
      &;
      &\node (D) [box] {$D$};
      &;
      &;
      &\\
      %second row
       \node(plant) [box] {$(zI-A)^{-1}$};
      & \node (p1) [point] {};
      &\node (C) [box] {$C$};
      & \node (plus1) [circle,draw,inner sep=2pt] {};
      &\node (estimator) [box] {Estimator};
      & \node (plus2) [circle,draw,inner sep=2pt] {};
      &\node (error) {$e$};\\
      %fourth row
      ;
      &\node (p2) [point] {};
      &;
      &;
      &;
      &\node (p3) [point] {};
      &\\
      };
      \draw [semithick] (plus1.north)--(plus1.south);
      \draw [semithick] (plus1.east)--(plus1.west);
      \draw [semithick] (plus2.north)--(plus2.south);
      \draw [semithick] (plus2.east)--(plus2.west);
      \draw [semithick,->] (noise)--(B);
      \draw [semithick,->] (B)--(plant);
      \draw [semithick,->] (plant)-- node[midway,above]{$x$} (C);
      \draw [semithick,->] (p1)--(p2)--(p3)--node[very near end,right]{$+$} (plus2);
      \draw [semithick,->] (C)--(plus1);
      \draw [semithick,->] (noise)--(D);
      \draw [semithick,->] (D)--(plus1);
      \draw [semithick,->] (plus1)--node[midway,above]{$y$} (estimator);
      \draw [semithick,->] (estimator)--node[midway,below]{$\hat x$}
      node[very near end,above] {$-$} 
      (plus2);
      \draw [semithick,->] (plus2)--(error);
    \end{tikzpicture}
  \end{center}
\end{figure}
Since $y(0:k) = 0$, $\hat x(0:k) = 0$, which implies that:
  \begin{displaymath}
   \|x(0:k)\|_\infty = \|e(0:k)\|_\infty \leq \|e\|_\infty \leq  \left(\inf_K \|E(K)\|\right) \|w\|_\infty .
  \end{displaymath}
\end{frame}



\begin{frame}{Estimation with Compromised Sensory Data}
  \begin{itemize}
    \item Suppose that $\gamma$ out of $m$ sensors are compromised by an adversary and he/she can change the readings from the compromised sensors arbitrarily.
    \item As a result,
      \begin{displaymath}
	y(k) = C x(k) + D w(k) + a(k),
      \end{displaymath}
      where $\|a\|_0 = \left|\{i:\exists k,\,s.t.,a_i(k)\neq 0\}\right|\leq \gamma$.
    \item How to do estimation with compromised sensory data?
      \begin{itemize}
	\item How to detect/isolate the bad sensors? 
	\item How much disturbance can the attacker introduce to the system without being detected?
	\item How to design a resilient estimator (with the detection scheme)?
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Estimation with Compromised Sensory Data}
\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=caltechcolor!50,fill=caltechcolor!20,rounded corners, semithick,minimum size=7mm},
      point/.style={coordinate},
      every node/.append style={font=\small}
      ]
      \matrix[row sep = 5mm, column sep = 5mm]{
      %first row
      \node (B) [box] {$B$};
      &\node (noise) {$w$};
      &;
      &\node (D) [box] {$D$};
      &;
      &;
      &\\
      %second row
       \node(plant) [box] {$(zI-A)^{-1}$};
      & \node (p1) [point] {};
      &\node (C) [box] {$C$};
      & \node (plus1) [circle,draw,inner sep=2pt] {};
      &\node (estimator) [box] {Estimator};
      & \node (plus2) [circle,draw,inner sep=2pt] {};
      &\node (error) {$e$};\\
      %third row
      ;
      &;
      &;
      &\node (attack) {a};
      &;
      &;
      &\\
      %fourth row
      ;
      &\node (p2) [point] {};
      &;
      &;
      &;
      &\node (p3) [point] {};
      &\\
      };
      \draw [semithick] (plus1.north)--(plus1.south);
      \draw [semithick] (plus1.east)--(plus1.west);
      \draw [semithick] (plus2.north)--(plus2.south);
      \draw [semithick] (plus2.east)--(plus2.west);
      \draw [semithick,->] (noise)--(B);
      \draw [semithick,->] (B)--(plant);
      \draw [semithick,->] (plant)-- node[midway,above]{$x$} (C);
      \draw [semithick,->] (p1)--(p2)--(p3)--node[very near end,right]{$+$} (plus2);
      \draw [semithick,->] (C)--(plus1);
      \draw [semithick,->] (noise)--(D);
      \draw [semithick,->] (D)--(plus1);
      \draw [semithick,->] (attack)--(plus1);
      \draw [semithick,->] (plus1)--node[midway,above]{$y$} (estimator);
      \draw [semithick,->] (estimator)--node[midway,below]{$\hat x$}
      node[very near end,above] {$-$} 
      (plus2);
      \draw [semithick,->] (plus2)--(error);
    \end{tikzpicture}
  \end{center}
\end{figure}
\end{frame}

\begin{frame}{Estimation with Compromised Sensory Data}
  \begin{itemize}
    \item We assume that $w\in l_\infty$ and 
      \begin{displaymath}
	\|w\|_\infty = \sup_{k} \|w(k)\|_\infty \leq \varepsilon.
      \end{displaymath}
    \item We assume that the estimator knows $\gamma$. However, it does not know which set of sensors is compromised.
    \item An estimator is an infinite sequence of mappings $f \triangleq (f_0,\,f_1,\,\dots)$, where each $f_k$ maps measurements $y(0:k-1)$ to an estimate of the state $\hat x(k)$, i.e., $\hat x(k) = f_k(y(0:k-1))$.
    \item Clearly, the estimation error $e(k)=x(k)-\hat x(k)$ depends on the noise $w$, the attacker's action $a$ and estimator design $f$.
    \item We are concerned with the worst-case estimation error:
      \begin{displaymath}
	e^*(f) = \sup_{\|a\|_0\leq \gamma,\|w\|_\infty\leq \varepsilon} \|e\|_\infty.
      \end{displaymath}
    \item Question: Does there exist an estimator with a finite $e^*$? If so, can we construct one?
  \end{itemize}
\end{frame}

\section{Main result}
\begin{frame}{Fundamental Limitation}
  \begin{itemize}
    \item Let 
      \begin{displaymath}
	C \triangleq \begin{bmatrix}
	  C_1\\
	  \vdots\\
	  C_m
	\end{bmatrix},
      \end{displaymath}
      where each $C_i$ is a row vector.
    \item Let $\mathcal I = \{i_1,\dots,i_l\}\subset\{1,\dots,m\}$. Define
      \begin{displaymath}
	C_\mathcal I \triangleq \begin{bmatrix}
	  C_{i_1}\\
	  \vdots\\
	  C_{i_l}
	\end{bmatrix}.
      \end{displaymath}
    \item Similarly, we can define $D_\mathcal I$ and $y_{\mathcal I}(k)$.
  \end{itemize}
\end{frame}
\begin{frame}{Fundamental Limitation}
  \begin{theorem}
 There does not exist an estimator $f$ with a finite $e^*(f)$ if there exists an index set $\K $ with cardinality $|\K| = m - 2 \gamma$, such that $(A, C_\K)$ is not detectable.
  \end{theorem}
  \begin{proof}[Sketch of the Proof]
    If there exists an index set $|\K| = m-2\gamma$, such that $(A,C_\K)$ is not detectable, then we can construct two processes $(w,a)$ and $(w',a')$, such that 
    \begin{itemize}
      \item The corresponding state $\|x-x'\|_\infty = \infty$.
      \item The corresponding measurement $y = y'$.
    \end{itemize}
    Since $y = y'$, the state estimator will give the same state estimate $\hat x$ for both cases. Hence, the worst-case estimation error is $\infty$.
  \end{proof}
\end{frame}

\begin{frame}{Estimator Design}
  \begin{itemize}
    \item We assume that for any index set $|\K|=m-2\gamma$, $(A,C_\K)$ is detectable.
    \item We want to construct an estimator with a finite $e^*$.
    \item Since we do not know which set of the sensors are compromised, we will build a ``local'' estimator for every possibility:
      \begin{displaymath}
	\hat x^{\I}(t+1) = A \hat x^\I (t)  - K^\I ( y_\I(t) -   C_\I \hat x^\I(t)),
      \end{displaymath}
      where $K^\I$ is the estimation gain such that $A+K^\I C_\I$ is stable.
  \end{itemize}
\end{frame}

\begin{frame}{Local Estimator}
  \begin{itemize}
    \item Suppose that $a_\I(k) = 0$ for all $\I$, i.e., $\I$ is the set of good sensors, then the estimation error and the residue $r^\I(k)=y_\I(k)-C_\I\hat x^\I(k)$ satisfy:
      \begin{align*}
	e^\I(k+1) &= (A+K^\I C_\I) e^\I(k) + (B+K^\I D_\I) w(k),\\
	r^\I(t) &=C_\I e^\I(k) + D_\I w(k).
      \end{align*}
    \item Let us define the linear mappings $ E^\I(K^\I)$ and $R^\I(K^\I)$ as:
      \begin{displaymath}
	E^\I(K^\I):w\rightarrow e^\I,\,R^\I(K^\I):w\rightarrow r^\I.
      \end{displaymath}
    \item Then we can compute the induced norm of $E^\I(K^\I)$ and $R^\I(K^\I)$ and thus
      \begin{displaymath}
	\|e^\I\|_\infty \leq \|E^\I(K^\I)\| \varepsilon,\, \|r^\I\|_\infty \leq \|R^\I(K^\I)\| \varepsilon.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Local Detector}
  \begin{itemize}
    \item  We know that if $a^\I = 0$, then
      \begin{displaymath}
	 \|r^\I\|_\infty \leq \|R^\I(K^\I)\| \varepsilon.
      \end{displaymath}
    \item As a result, if we find that
      \begin{displaymath}
	\|r^\I(0:k)\|_\infty > \sup_{0\leq t\leq k}  \|R^\I(K^\I)\|\varepsilon,
      \end{displaymath}
      then $a^\I\neq 0$, i.e., the index set $\I$ contains compromised sensors.
    \item Therefore, we should not ``trust'' the local estimate $\hat x^\I$.
    \item Define the collection of trusted index sets at time $k$ as
      \begin{displaymath}
	\mathcal T(k) = \{\I:\,\|r^\I(0:k)\|_\infty \leq \|R^\I(K^\I)\|\varepsilon \}.
      \end{displaymath}
  \end{itemize}
\end{frame}

\begin{frame}{Global Fusion}
     We compute the state estimate $\hat x(k)$ using the trusted local estimate $\hat x^\I(k)$, $\I\in \mathcal T(k)$:
      \begin{displaymath}
	\hat x_i(k) = \frac{1}{2} \left(\max_{\I\in \mathcal T(k)} \hat x_i^\I(k) + \min_{\I\in \mathcal T(k)} \hat x_i^\I(k)\right).
      \end{displaymath}
 \begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}
      \draw  [color=caltechcolor!50,fill=caltechcolor!20,semithick] (0,0) rectangle (4,2.5);
      \node [fill=red,inner sep = 1.5pt,circle, label=above:$\hat x^{\I_1}(k)$] at (2.5,2.5) {};
      \node [fill=red,inner sep = 1.5pt,circle, label=above:$\hat x^{\I_2}(k)$] at (0,1) {};
      \node [fill=red,inner sep = 1.5pt,circle, label=right:$\hat x^{\I_3}(k)$] at (4,0) {};
      \node [fill=red,inner sep = 1.5pt,circle, label=below:$\hat x^{\I_4}(k)$] at (1,1) {};
      \node [fill=black,inner sep = 1.5pt,circle, label=above:$\hat x^{\I_5}(k)$] at (5,1.5) {};
      \node [fill=blue,inner sep = 1.5pt,circle, label=above:$\hat x(k)$] at (2,1.25) {};
      \draw [->,semithick] (-1,-0.5)--(6,-0.5);
      \draw [->,semithick] (-0.5,-1)--(-0.5,3);
    \end{tikzpicture}
    \caption{$\mathcal T(k) = \{\mathcal I_1,\mathcal I_2,\mathcal I_3,\mathcal I_4\}$}
  \end{center}
\end{figure}     
\end{frame}

\begin{frame}[fragile]{Estimator Design}
  \begin{figure}
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=caltechcolor!50,fill=caltechcolor!20,rounded corners, semithick,minimum size=7mm},
      point/.style={coordinate},
      every node/.append style={font=\small}
      ]
      \matrix[row sep = 7mm, column sep =6mm]{
      %first row
      \node (local1) [box] {Local Estimator $\I_1$};
      &\node (local2) [box] {Local Estimator $\I_2$};
      &\node {$\cdots$};
      &\node (local3) [box] {Local Estimator $\I_l$};\\
      %second row
      \node (detector1) [box] {Local Detector $\I_1$};
      &\node (detector2) [box] {Local Detector $\I_2$};
      &\node {$\cdots$};
      &\node (detector3) [box] {Local Detector $\I_l$};\\
      %third row
      ;
      &\node (fusion) [box] {Global Fusion };
      &;
      &;\\
     };
      \draw [semithick,->] (local1)--(detector1);
      \draw [semithick,->] (local2)--(detector2);
      \draw [semithick,->] (local3)--(detector3);
      \draw [semithick,->] (detector1)--(fusion);
      \draw [semithick,->] (detector2)--(fusion);
      \draw [semithick,->] (detector3)--(fusion);
    \end{tikzpicture}
  \end{center}
\end{figure}
\end{frame}

\begin{frame}{Estimation Performance}
  \begin{theorem}
If $(A,C_\K)$ is detectable for all $|\K| = m-2\gamma$, then the worst-case estimation error for the state estimator described above is finite. Furthermore, the following upper bound on $e^*(f)$ holds: 
\begin{align}
  e^*(f)  \leq  \max_{|\I|=|\J| = m-\gamma } \Big( \| E^\I(K^\I) \|   + {1 \over 2}  \alpha^{\I\cap\J} [\beta^\I(K^\I)+\beta^\J(K^\J)] \Big) \epsilon
  \label{eq:worstcaseerror}
\end{align}
where $\alpha^\K$ is defined as 
\begin{align*}
  \alpha^\K  \triangleq \inf_{K:A+KC_\K\text{ strictly stable}}\left\| \left[
\begin{array}{c|c}
  A+K C_\K & \begin{bmatrix}
    I&K
  \end{bmatrix}\\
  \hline
  I &  0
\end{array}
\right]\right\|,
\end{align*}
and $\beta^\I(K^\I)$ is defined as
\begin{align*}
 \beta^\I(K^\I) \triangleq \max(\|K^\I\|,1)\,\|R^\I(K^\I)\|. 
\end{align*}
\end{theorem}
\end{frame}

\begin{frame}{Some Comments}
  \begin{itemize}
    \item Let $\mathcal G$ be the true set of the good sensors.
    \item The estimation error can be written as
      \begin{displaymath}
	e = x - \hat x = x - \hat x^\mathcal G + \hat x^\mathcal G - \hat x.
      \end{displaymath}
    \item The first term is bounded by
      \begin{displaymath}
	\|x - \hat x^\mathcal G\|_\infty \leq \|E^\G(K^\G)\| \varepsilon.
      \end{displaymath}
    \item The second term is caused by the fact that we do not know the set of compromised sensors. 
      \begin{itemize}
	\item We know that if the index sets $\I,\J$ are trusted at time $k$, then $r^\I(0:k),\,r^\J(0:k)$ will be small.
	\item As a result, we can prove that $x^\I(0:k) - x^\J(0:k)$ will be small. Therefore, the error from the second term is bounded.
      \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}{Numerical Examples}
  We consider the following system:
  \begin{align*}
    x(k+1) &= x(k) + w_0(k),\\
    y(k) &= \begin{bmatrix}
      1\\
      1\\
      1
    \end{bmatrix} x(k) + \begin{bmatrix}
      w_1(k)\\
      w_2(k)\\
     w_3(k) 
    \end{bmatrix} + a(k),
  \end{align*}
  where $w(k) = \begin{bmatrix}
    w_0(k)&w_1(k)&w_2(k)&w_3(k)
  \end{bmatrix}$ and $\|w\|_\infty \leq 1$. In this example, we randomly generate $w(k)$ from a uniform distribution. The attack $a(k) = \begin{bmatrix}
    0.5k&0&0
  \end{bmatrix}$.
\end{frame}

\begin{frame}{Numerical Example}
\setlength{\figureheight}{5cm}
\setlength{\figurewidth}{6cm}
\begin{figure}[<+htpb+>]
  \begin{center}
  \inputtikz{esterror}
  \end{center}
\end{figure}
\end{frame}
\end{document}
