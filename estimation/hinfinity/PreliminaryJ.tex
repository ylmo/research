\subsection*{System and norm}
Consider a linear time-invariant system
\BEQL
\label{eq:LTI}
P: &&x(t+1) = A x(t) + B u(t)\\
&&\nonumber y(t) = C x(t) + D u(t),
\EEQL
where $u(t)\in\mathbb R^l$ is the input, $y(t)\in \mathbb R^m$ is the ouput, and $x(t)\in \mathbb R^n$ is the state. The matrices $A,\,B,\,C,\,D$ are real matrices with proper dimensions. We set the initial condition to be $x(0) = 0$. 
With slight abuse of notation, we denote
\begin{equation}
  P \triangleq  \left[
  \begin{array}{c|c}
    A & B \\
    \hline
    C &  D
  \end{array}
  \right]. 
\end{equation} 
Let $\hat P( e^{i \theta} ) = B ( e^{i \theta} I - A ) C + D$ be the transfer function of $P$.  
The impulse response of \eqref{eq:LTI} is defined as the following:
\begin{equation} 
  H(t) \triangleq \Bigg\{\begin{array}{ll}
    D \; &t = 0 \\
    C A^{t-1} B & t \geq 1
  \end{array}.
\end{equation}
by which the system output can be written as $y = H*w$. 

When $A$ is strictly stable ($|eig(A)| < 1$), the $\mathcal H_2, \mathcal H_\infty, \mathcal L_1$ norm of the system is defined as \cite{dahleh1994control}:  
\BEQ
&&\|P\|_{\mathcal H_2} = \sum_{t = 0}^\infty tr H(t)H^T(t) = \|P\|_{2 \rightarrow \infty} \\
&&\|\hat P\|_{\mathcal H_\infty} = \sup_{\theta} \sigma_{\max} \hat P( e^{i \theta}) = \|P\|_{2 \rightarrow 2} \\ 
&&\| P\|_{\mathcal L_1} = \underset{1\leq i \leq n}{\text{max}} \sum_{j = 1}^{p} \sum^{\infty}_{t = 0} |h_{ij} (t)| = \|P\|_{\infty \rightarrow \infty} , 
\EEQ
where $\mathcal H_2$ norm is the induced norm from $l_2$ to $\l_\infty$, $\mathcal H_\infty$ norm from $l_2$ to $l_2$, and $\mathcal L_1$ norm from $l_\infty$ to $l_\infty$.  


\subsection*{State estimator}

\BDEF
\label{def:state_estimator}
A causal state estimator can be defined an infinite sequence of mappings $f \triangleq (f_0,\,f_1,\,\dots)$, where each $f_t$ maps past measurements $y(0:t-1)$ to an estimate of the current state $\hat x(t)$, i.e., $\hat x(t) = f_t(y(0:t-1))$. 
We say the estimator produce bounded estimation error if the estimation error $e(t) = x(t) - \hat x(t)$ is bounded, i.e., $\exists \delta \text{ s.t } \|e(t)\| < \delta$. 
\EDEF


If $(A,C)$ is detectable, then there exists $K\in \mathbb R^{n\times m}$ such that $A+KC$ is strictly stable. Now consider the following form of linear estimator for system \eqref{eq:LTI} :
\BEQL
\label{eq:LTIest}
\hat x(t+1) = A \hat x(t) - K ( y(t) - C\hat x(t)),
\EEQL
where the intitial condition is set to be $\hat x(0)=0$. 

The estimation error and the residue vector are respectively defined as 
\begin{align}
 & e(t) \triangleq x(t) - \hat x(t)\\
&  r(t) \triangleq y(t) - C\hat x(t).
  \label{eq:deferrorandresidue}
\end{align}
Lemma \ref{lemma:nec_original} provides bounds for $\|e(t)\|$ and $\|r(t)\|$:

\BLEM
\label{lemma:nec_original}
Consider system \eqref{eq:LTI} with $\|u\|_p \leq \varepsilon$. The estimator \eqref{eq:LTIest} with $A+KC$ being strictly stable satisfies the following inequalities:
\BEQL
\label{eq:errorbound}
&&\| e \|_q \leq \| E(K) \|_{p \rightarrow q} \varepsilon ,  \\
&&\| r \|_q \leq \| G(K)\|_{p \rightarrow q} \varepsilon.
\label{eq:residuebound}
\EEQL
where $(p, q) \in \{(2,2),(2,\infty),(\infty,\infty)\}$ and
\BEQ 
E(K) \eq 
\left[
\begin{array}{c|c}
  A + K C & B+ K D\\
  \hline
  I &  0
\end{array}
\right], \\
G(K) \eq 
\left[
\begin{array}{c|c}
  A + K C & B+ K D \\
  \hline
  C &  D
\end{array}
\right].\EEQ
\ELEM 

\BPF
Manipulating \eqref{eq:LTIest}, we have
\begin{align*}
  e(t+1) &= (A + K C)  e(t) + (B  + KD) w(t),\,e(0) = 0,\\
  r(t) &= C e(t) + D w(t).
\end{align*}
Therefore, \eqref{eq:errorbound} and \eqref{eq:residuebound} follows from the definition of induced operator norm. 
\EPF

Lemma \ref{lemma:y0} provides bounds for $\|x\|$ when $y(0:T) = 0$ for some $T \in \inn$.
\BLEM
\label{lemma:y0}
Consider system (\ref{eq:LTI}) where $(A,C)$ is detectable and $\|u\|_p \leq \varepsilon$. If $y(0:T) = 0$ for some $T \in \inn$, then the truancated sequence of the state $x(0:T)$ satisfies the following inequalities: 
\BEQ
\| x(0:T) \|_q \leq \inf_{K:A+KC\text{ strictly stable}}\| E(K) \|_{p \rightarrow q} \varepsilon
\label{eq:finitestatenorminf}
\EEQ
\ELEM

\BPF
Since $(A,C)$ is detectable, $A+KC$ is strictly stable for some $K$. We construct a state estimator of the form \eqref{eq:LTIest} using this stabilizing $K$. The condition $y(0:T) = 0$ implies $\hat x(0:T) = 0$. By Lemma \ref{lemma:nec_original} we have
\begin{equation}
  \begin{split}
    \| x(0:T) \|_q &= \| x(0:T)-\hat x(0:T) \|_q\\
    &=\|e(0:T)\|_q \\
    &\leq  \|e\|_q \\
    &\leq \| E(K) \|_{p \rightarrow q} \varepsilon. 
  \end{split}
  \label{eq:finitestatenorm}
\end{equation}
\eqref{eq:finitestatenorminf} follows from taking infimum over all stabilizing $K$.

\EPF