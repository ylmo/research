This section is devoted to the construction of a resilience estimator $f$ and characterization of the corresponding worst-case performance $\rho(f)$. By Theorem~\ref{thm:nonexistence}, we know that the following assumption is necessary for the existence of the resillient estimator.  
\begin{description}
\item[C.] $(A, C_\K)$ is detactable for any $\K \subset \mathcal S$ with cardinality $n - 2\gamma$
\end{description}

Therefore, we will assume that Assumption~C holds throughout the section. 

\subsection{State Estimator Design}
\label{sec:design} 
We propose the following estimator design for system (\ref{eq:system}) under the Assumption A-C. 

\subsubsection{Local Estimator and Detector}

Let $\mi = \{i_1, \cdots, i_{m-\gamma} \}\subset S$ be a index set with cardinality $n-\gamma$. Denote the collection of all such index sets as
\begin{align*}
  \mathcal L \triangleq \{\I\subset \mathcal S:|\I|=m-\gamma\} .
\end{align*}
For any $\mi \in \comset$, Assumption~C implies the existence of $K^\mi$ such that $A + K^\mi C_\mi$ is strictly stable. Therefore, we can construct a stable ``local'' estimator, which only uses the truncated measurement $y_\I(t)$ to compute the state estimate\footnote{We use superscript notation for $x^\mi$ and $K^\mi$ in order to differentiate it from projection, which is written as subscript.}:
\begin{align}
\hat x^{\mi}(t+1) = A \hat x^\mi (t)  - K^\mi ( y_\mi(t) -   C_\mi \hat x^\mi(t)),
\label{eq:est}
\end{align}
with initial condition $\hat x^\I (0) = 0$.

For each local estimator, let us define the corresponding error and residue vector as
\begin{align}
e^\I(t) \triangleq x(t) - \hat x^\I(t),\, r^\I(t) \triangleq y_\I(t) - C_\I\hat x^\I(t).  
\end{align}
we define the corresponding linear operators as follows:
\begin{align}
  \label{eq:esterror2}
  E^\mi(K^\mi) &\triangleq \left[
  \begin{array}{c|c}
    A+K^\mi C_\mi & B  + K^\mi D_\mi \\
    \hline
    I &  0
  \end{array}
  \right], \\
  G^\mi(K^\mi)& \triangleq 
  \left[
  \begin{array}{c|c}
    A + K^\mi C_\mi & B+ K^\mi D_\mi  \\
    \hline
    C_\mi &  D_\mi 
  \end{array}
  \right].
\end{align}

By Lemma~\ref{lemma:nec_original}, we know that if $a_\I(t) = 0$ for all $t$, i.e., if $\mathcal I$ does not contain any compromised sensors, then the following inequality holds:
\BEQL
\label{eq:localdetection}
&&|| r^\I  ||_\infty \leq || G^\mi(K^\mi)||_1 \varepsilon.
\EEQL

As a result, we will assign each local estimator a local detector, which checks if the following inequality holds at each time $t$:
\BEQL
\label{eq:localdetection1}
&&|| r^\I(0:t) ||_\infty \leq || G^\mi(K^\mi)||_1 \varepsilon.
\EEQL

If \eqref{eq:localdetection1} fails to hold, then we know the set $\mathcal I$ contains at least $1$ compromised sensor and hence the local estimate $\hat x^\I(t)$ is corrupted by the adversary. 

On the other hand, we call $\hat x^\I(t)$ a \emph{valid} local estimate from time $0$ to $t$ if \eqref{eq:localdetection1} holds at time $t$. We further define the set $\mathcal L(t)$ as
\begin{align}
  \mathcal L(t) \triangleq \{\I\in \mathcal S:\text{ \eqref{eq:localdetection1} holds at time }t\}.
\label{eq:comset}
\end{align}

\begin{remark}
  Notice that \eqref{eq:localdetection1} is only a sufficient condition for the index set $\mathcal I$ to contain compromised sensors and it is not necessarily tight. One can potentially design better local detectors to check if there exist compromised sensors in the index set $\mathcal I$ to provide better performance. However, the local detector based on \eqref{eq:localdetection1} is suffice for us to design a resilient estimator.
\end{remark}

\subsubsection{Global Data Fusion}

We will then fuse all the \emph{valid} local estimation $\hat x^\I(t)$ at time $t$ to generate the state estimate $\hat x(t)$. Since we are concerned with the infinite norm of the estimation error, we will use the following equation to compute each entry of $\hat x(t)$:
\begin{eqnarray}
  \label{eq:meanest}
  \hat x_i(t) = {1 \over 2} \Big( \min_{\mi \in \comset(t)} \hat x^\mi_i (t)  + \max_{\mi \in \comset(t)} \hat x^\mi_i (t) \Big).
\end{eqnarray} 

%We use the condition in (\ref{eq:localdetection}) to detect potential attack. Following lemma justify why it is a reasonable strategy. 
%\BLEM
%\label{lemma:nec}
%Consider system (\ref{eq:system}) with assumption A-C. For each local estimator with the set of sensors indiced by $\mi \in \comset(0)$, if there exists $t \in \mathbb N$ such that the local state estimator satisfy 
%\BEQL
%\label{eq:esterror1}
%&&|| y  - C \hat x^\mi  ||_\infty > || G^\mi(L^\mi)||_1 \epsilon,
%\EEQL
%then the set of sensors that are indices by $\mi$ contains at least one compromised sensors (i.e., $supp(a_\mi) > 0)$. 
%\ELEM
%
%\BPF
%Same with Lemma \ref{lemma:nec_original}. 
%\EPF

\subsection{Upper Bound on the Worst-Case Estimation Error}
We now provide an upper bound on the worst-case performance $\rho(f)$ for our estimator design, which is given by the following theorem:
\BTHM
\label{thm:esterror}
Under Assumption A-C, the state estimator described in Section~\ref{sec:design} is a resilient estimator for system \eqref{eq:system}. Furthermore, the following inequality on $\rho(f)$ holds:
\begin{align}
  \rho(f)  \leq  \max_{\I,\J\in \mathcal L} \Big( \| E^\mi(K^\I) \|_1   + {1 \over 2}  \alpha^{\I\cap\J} [\beta^\I(K^\I)+\beta^\J(K^\J)] \Big) \epsilon
  \label{eq:worstcaseerror}
\end{align}
where $\alpha^\K$ is defined as 
\begin{align*}
  \alpha^\K  \triangleq \inf_{K:A+KC_\K\text{ strictly stable}}\left\| \left[
\begin{array}{c|c}
  A+K C_\K & \begin{bmatrix}
    I&K
  \end{bmatrix}\\
  \hline
  I &  0
\end{array}
\right]\right\|_1,
\end{align*}
and $\beta^\I(K^\I)$ is defined as
\begin{align*}
 \beta^\I(K^\I) \triangleq \max(\|K^\I\|_i,1)\,\|G^\I(K^\I)\|_1. 
\end{align*}
\ETHM

\begin{remark}
  It is worth noticing that if $\I$ does not contain compromised sensors, then minimizing the infinite norm of the local estimation error $e^\I(t)$ is equivalent to minimizing $\| E^\mi(K^\I) \|_1$. The second term on the RHS of \eqref{eq:worstcaseerror} exists since the estimator does not know which local estimate can be trusted at the beginning.
\end{remark}
Several intermediate results are needed before proving Theorem~\ref{thm:esterror}. We first prove the following lemma to bound the divergence of the local estimates:
%% Second term 
\BLEM
\label{lemma:difference}
For any two index sets $\I,\,\J\in \mathcal L(T)$, the following inequality holds:
\begin{align}
 \| \hat x^{\I}(0:T) - \hat x^{\J}(0:T) \|_\infty  \leq \varepsilon\alpha^{\I\cap\J}[\beta^\I(K^\I)+\beta^\J(K^\J)].
 \label{eq:localestimationdivergence}
\end{align}
\ELEM
\BPF
By \eqref{eq:est}, we have
\begin{align}
  \hat x^\I(t+1) &= A\hat x^\I(t) - K^\I r^\I(t),\,\hat x^\I(t) = 0,\nonumber\\
  y_\I(t) &= C_\I \hat x^\I(t) + r^\I(t).
  \label{eq:suberror}
\end{align}
Let us define $\mathcal K = \I\cap\J$, we know that
\begin{align*}
  y_\K(t) = C_\K \hat x^\I(t) + P_{\K,\I} r^\I(t),
\end{align*}
where $P_{\K,\I}\in \mathbb R^{|\K|\times|\I|}$ is the unique matrix that satisfies:
\begin{align*}
  P_\K = P_{\K,\I} P_\I. 
\end{align*}
Now let us define $\phi^\I(t) \triangleq -K^\I r^\I(t)$ and $\varphi^\I(t) \triangleq P_{\K,\I} r^\I(t)$. If $t\leq T$, we know that \eqref{eq:localdetection1} holds at time $t$, which implies that
\begin{align*}
 \|r^\I(t)\|_\infty\leq\|G^\I(K^\I)\|_1 \varepsilon.
\end{align*}
As a result,
\begin{align*}
  \left\|\begin{bmatrix}
\phi^\I(t)\\
\varphi^\I(t)
  \end{bmatrix}\right\|_\infty &\leq    \left\|\begin{bmatrix}
-K^\I\\
P_{\K,\I}
  \end{bmatrix}\right\|_i \|r^\I(t)\|_\infty= \beta^\I(K^\I)\varepsilon,
\end{align*}
where we use the fact that each row of $P_{\K,\I}$ is a canonical basis vector in $\mathbb R^{|\I|}$. Therefore, we have
\begin{align}
  \hat x^\I(t+1) &= A\hat x^\I(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)\\
    \varphi^\I(t)
  \end{bmatrix},\,\hat x^\I(t) = 0,\nonumber\\
  y_\K(t) &= C_\K \hat x^\I(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)\\
    \varphi^\I(t)
  \end{bmatrix}.
  \label{eq:suberror2}
\end{align}
Similarly, we have
\begin{align}
  \hat x^\J(t+1) &= A\hat x^\J(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\J(t)\\
    \varphi^\J(t)
  \end{bmatrix},\,\hat x^\J(t) = 0,\nonumber\\
  y_\K(t) &= C_\K \hat x^\J(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\J(t)\\
    \varphi^\J(t)
  \end{bmatrix},
  \label{eq:suberror3}
\end{align}
with
\begin{align*}
 \left\|\begin{bmatrix}
    \phi^\J(t)\\
    \varphi^\J(t)
  \end{bmatrix} \right\|\leq \beta^\J(K^\J)\varepsilon,\,\forall t\leq T.
\end{align*}

Now let us consider $\Delta\hat x(t) = \hat x^I(t)-\hat x^j(t)$. By \eqref{eq:suberror2} and \eqref{eq:suberror3}, we know that
\begin{align*}
  \Delta \hat x(t+1) &= A\Delta \hat x(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)-\phi^\J(t)\\
    \varphi^\I(t)-\varphi^\J(t)
  \end{bmatrix},\,\Delta\hat x(t) = 0,\nonumber\\
  0 &= C_\K \Delta \hat x(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)-\phi^\J(t)\\
    \varphi^\I(t)-\varphi^\J(t)
  \end{bmatrix}.
\end{align*}
Hence, \eqref{eq:localestimationdivergence} can be proved by Lemma~\ref{lemma:y0}.
\EPF

\begin{lemma}
  \label{lemma:mediandivergence}
Let $r_1,\dots,r_l$ be real numbers. Define
\begin{align*}
  r = \frac{1}{2}\left(\max_i r_i + \min_i r_i\right).
\end{align*}
Then for any $i$, we have
\begin{align}
  |r - r_i| \leq \frac{1}{2} \max_j |r_j-r_i|. 
  \label{eq:mediandivergence}
\end{align}
\end{lemma}
\begin{proof}
  Without loss of generality, we assume that $r_1$ and $r_2$ are the largest and the smallest number among all $r_i$s respectively. Therefore, 
  \begin{align*}
    r-r_i = \frac{1}{2}(r_1 - r_i) -\frac{1}{2} \left(r_i- r_2\right).
  \end{align*}
  Therefore, if $r_1-r_i \geq r_i - r_2$, then
  \begin{align*}
    |r-r_i| = r-r_i \leq \frac{1}{2}(r_1-r_i) = \frac{1}{2}\max_j|r_j-r_i|.
  \end{align*}
  Similarly, one can prove that \eqref{eq:mediandivergence} holds when $r_1-r_i < r_i - r_2$.
\end{proof}
Now we are ready to prove Theorem~\ref{thm:esterror}. 
\BPF
Let $\mathcal G \in \mathcal L$ be the set of good sensors. By Lemma~\ref{lemma:nec_original}, we know that
\begin{align*}
  \|x - \hat x^{\mathcal G}\|_\infty \leq \|E^{\mathcal G}(K^{\mathcal G})\|_1\varepsilon.
\end{align*}
Furthermore, $\mathcal G\in \mathcal L(t)$ for all $t$. At any given time $t$, assuming that the index set $\mathcal J$ also belongs to $\mathcal L(t)$. By Lemma~\ref{lemma:difference}, we have
\begin{align*}
  \|\hat x^{\mathcal G}(t) - \hat x^{\mathcal J}(t)\|_\infty \leq \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)].
\end{align*}
Therefore, by Lemma~\ref{lemma:mediandivergence}, we know that
\begin{align*}
  \|\hat x(t) -  \hat x^{\mathcal G}(t)\|_\infty &\leq\frac{1}{2}\max_{\J\in \mathcal L(t)} \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)]\\
  &\leq\frac{1}{2}\max_{\J\in \mathcal L} \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)].
\end{align*}
By triangular inequality, we have
\begin{align}
  \|e(t)\|_\infty &\leq   \|E^{\mathcal G}(K^{\mathcal G})\|_1\varepsilon\label{eq:errortime}\\
 \nonumber &+\frac{1}{2}\max_{\J\in \mathcal L} \alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)]\varepsilon.
\end{align}
Thus, by taking the supremum over all possible ``good'' sensor set $\mathcal G$, we can prove \eqref{eq:worstcaseerror}.
\EPF

Combining Theorem~\ref{thm:nonexistence} and Theorem~\ref{thm:esterror}, we have the following corollary:
\begin{corollary}
  A necessary and sufficient condition for the existence of a resilient estimator is that $(A,C_\K)$ is detectable for any index set $\K\subset \mathcal S$ with cardinality $2\gamma$.
\end{corollary}
