Consider a dynamical system of the form
\BEQL
\nonumber
&&x(t+1) = A x(t) + B w(t)\\
&&y(t) = C x(t)  + D w(t) +  a(t). \label{eq:system}
\EEQL
where $y(t) \in \inr^{m}$ is the measurements, $w(t) \in \inr^l$ is the disturbance, $x(t) \in \inr^{n}$ is the state, and $a(t) \in \inr^{m}$ is the bias injected by the adversary. We set the initial condition to be $x(0)=0$. Each sensor is indexed by $i \in \{ 1, \cdots, m\}$ and produce measurement $y_i(t)$, which is stacked together to form $y(t) = [y_1(t),\dots,y_m(t)]^T$. We denote the set of all sensors as $\mathcal S \triangleq \{1,\dots,m\}$. 

In this paper, we make the following assumptions:
\begin{description}
  \item[A.] The disturbance is in $l_p$ space and is bounded by $\|w\|_p \leq \varepsilon$.
  \item[B.] The maximum number of sensors the adversary can cahnge the measurement is $\gamma$, i.e., $\|a\|_0\leq \gamma$. We denote the set of compromised sensors as $\mathcal C\subset \mathcal S$, the set of non-compromised sensors as $\mathcal G\triangleq \mathcal S\backslash \mathcal C$. Notice that non-compromised sensor satisfy $a_i(t) = 0$ for all $t$.
  \item[C.] The value of $\varepsilon$ and $\gamma$ is known. The set of the compromised sensors $\mathcal C$ is unknown, and is possibly undetectable in the sense defined in \cite{pajic2014robustness,fawzi2012security}.
\end{description}

\begin{remark}
Alternatively, the value of $\gamma$ can be a design parameter. In other words, the system designer can decide how much compromised sensors the system needs to tolerate. In general, increasing $\gamma$ will increase the resilience of the detector under attack at the expense of heavy computation and suboptimal performance.  
\end{remark}

The goal of this paper is to design a causal state estimator (definition \ref{def:state_estimator}) that produce state estimate $\hat x(t) = f( y(0:t-1))$. The state estimation error $e(t) \triangleq x(t) - \hat x(t)$ is a function of the disturbance $w$, the bias $a$ injected by the adversary and the estimator $f$. We abbreviate $e_{w,a,f}(t)$ as $e(t)$. In this paper, we evaluate estimator performance using worst-case estimation error 
\begin{equation}
  \rho(f) \triangleq \sup_{\|w\|_p\leq \varepsilon,\,\|a\|_0\leq \gamma} \|e_{w,a,f}(0:\infty)\|_q, 
  \label{eq:defworstcaseerror}
\end{equation}
where the pair $(p, q)$ is one of the following
\BEQ
(2,2),(2,\infty),(\infty,\infty).
\EEQ

\BDEF
\label{resilientest}
An estimator $f$ is called an resilient estimator if $\rho(f)<q$.
\EDEF

Fig~\ref{fig:robustestimationwithattack} illustrate the the estimation problem discussed above. We first provide necessary condition for the existence of a resilient estimator, followed by the proposed estimation methods.

\begin{figure}[ht]
  \begin{center}
    \begin{tikzpicture}[>=stealth',
      box/.style={rectangle, draw=blue!50,fill=blue!20,rounded corners, semithick,minimum size=7mm},
      point/.style={coordinate},
      every node/.append style={font=\small}
      ]
      \matrix[row sep = 5mm, column sep = 5mm]{
      %first row
      \node (B) [box] {$B$};
      &\node (noise) {$w$};
      &;
      &\node (D) [box] {$D$};
      &;
      &;
      &\\
      %second row
       \node(plant) [box] {$(zI-A)^{-1}$};
      & \node (p1) [point] {};
      &\node (C) [box] {$C$};
      & \node (plus1) [circle,draw,inner sep=2pt] {};
      &\node (estimator) [box] {Estimator};
      & \node (plus2) [circle,draw,inner sep=2pt] {};
      &\node (error) {$e$};\\
      %third row
      ;
      &;
      &;
      &\node (attack) {a};
      &;
      &;
      &\\
      %fourth row
      ;
      &\node (p2) [point] {};
      &;
      &;
      &;
      &\node (p3) [point] {};
      &\\
      };
      \draw [semithick] (plus1.north)--(plus1.south);
      \draw [semithick] (plus1.east)--(plus1.west);
      \draw [semithick] (plus2.north)--(plus2.south);
      \draw [semithick] (plus2.east)--(plus2.west);
      \draw [semithick,->] (noise)--(B);
      \draw [semithick,->] (B)--(plant);
      \draw [semithick,->] (plant)-- node[midway,above]{$x$} (C);
      \draw [semithick,->] (p1)--(p2)--(p3)--node[very near end,right]{$+$} (plus2);
      \draw [semithick,->] (C)--(plus1);
      \draw [semithick,->] (noise)--(D);
      \draw [semithick,->] (D)--(plus1);
      \draw [semithick,->] (attack)--(plus1);
      \draw [semithick,->] (plus1)--node[midway,above]{$y$} (estimator);
      \draw [semithick,->] (estimator)--node[midway,below]{$\hat x$}
      node[very near end,above] {$-$} 
      (plus2);
      \draw [semithick,->] (plus2)--(error);
    \end{tikzpicture}
  \end{center}
  \caption{Diagram of the Estimation Problem in Adversarial Environment. $z^{-1}$ is the unit delay.}
  \label{fig:robustestimationwithattack} 
\end{figure}