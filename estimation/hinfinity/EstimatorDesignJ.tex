This section propose a resilient estimator and analyze its performance. We impose the system to satisfy the necessary condition for the existence of the resilient estimator:
\begin{description}
\item[D.] $(A, C_\K)$ is detactable for any $\K \subset \mathcal S$ with cardinality $n - 2\gamma$
\end{description}


\subsection{State Estimator Design}
\label{sec:design} 
We propose an estimator for system (\ref{eq:system}) under assumption A-D. 

\subsubsection{Estimator Design}

Let $\mi = \{i_1, \cdots, i_{m-\gamma} \}\subset S$ be an index set with cardinality $n-\gamma$. Denote the collection of all such index sets as
\begin{align*}
  \mathcal L \triangleq \{\I\subset \mathcal S:|\I|=m-\gamma\} .
\end{align*}
For any $\mi \in \comset$, Assumption~C implies the existence of $K^\mi$ such that $A + K^\mi C_\mi$ is strictly stable. Therefore, we can construct a stable \emph{local} estimator, which only uses a subset of sensor measurements $y_\I(t)$ to compute the state estimate\footnote{We use superscript notation for $x^\mi$ and $K^\mi$ in order to differentiate it from projection, which is written as subscript.}:
\begin{align}
\hat x^{\mi}(t+1) = A \hat x^\mi (t)  - K^\mi ( y_\mi(t) -   C_\mi \hat x^\mi(t)),
\label{eq:est}
\end{align}
with initial condition $\hat x^\I (0) = 0$.

For each local estimator, let us define the corresponding error and residue vector as
\begin{align}
&e^\I(t) \triangleq x(t) - \hat x^\I(t) \\
&r^\I(t) \triangleq y_\I(t) - C_\I\hat x^\I(t).  
\end{align}
The corresponding linear operators $E^\mi(K^\mi) :w \rightarrow e$ and $G^\mi(K^\mi): w \rightarrow r$ is 
\begin{align}
  \label{eq:esterror2}
  E^\mi(K^\mi) &\triangleq \left[
  \begin{array}{c|c}
    A+K^\mi C_\mi & B  + K^\mi D_\mi \\
    \hline
    I &  0
  \end{array}
  \right], \\
  G^\mi(K^\mi)& \triangleq 
  \left[
  \begin{array}{c|c}
    A + K^\mi C_\mi & B+ K^\mi D_\mi  \\
    \hline
    C_\mi &  D_\mi 
  \end{array}
  \right].
\end{align}


When $\mathcal I$ does not contain any compromised sensors, i.e., $a_\mi = 0$, from lemma~\ref{lemma:nec_original}, we have
\BEQL
\label{eq:localdetection}
&&|| r^\I  ||_q \leq || G^\mi(K^\mi)||_{p \rightarrow q} \varepsilon.
\EEQL
We use \eqref{eq:localdetection} to detect if a given set of sensors are compromised. In other words, if 
\BEQL
\label{eq:localdetection1}
&&|| r^\I(0:t) ||_q \leq || G^\mi(K^\mi)||_{p \rightarrow q} \varepsilon.
\EEQL
fails to hold, then we know the set $\mathcal I$ contains at least one compromised sensor. We call $\hat x^\I(t)$ a \emph{valid} local estimate from time at time $t$ if \eqref{eq:localdetection1} holds. Denote the set of valid set of sensors as
\begin{align}
  \mathcal L(t) \triangleq \Big\{\I\in \mathcal S:|| r^\I(0:t) ||_q \leq || G^\mi(K^\mi)||_{p \rightarrow q}\; \varepsilon\Big\}.
\label{eq:comset}
\end{align}

\begin{remark}
The condition \eqref{eq:localdetection1} is sufficient but not necessary condition for the index set $\mathcal I$ to contain compromised sensors. Moreover, for the case $(p,q) = (\infty, \infty)$, one can replance \eqref{eq:localdetection1} with element-wise tighter bound for better performance
\BEQ
&&|r^\I_i(0:t)|  \leq \sum_{j = 1}^{l} \sum_{\tau = 0}^{t} \{ h_{G^\mi(K^\mi)} \}_{ij}
\varepsilon,
\EEQ
where $h_{G^\mi(K^\mi)}$ is the impulse response of the LTI system  $E^\mi(K^\mi)$. 
We demonstrate the estimator design using \eqref{eq:localdetection1} in this paper so that the main ideas is not blurred with notational complexity.
\end{remark}

\subsubsection{Global Data Fusion}

We fuse all the \emph{valid} local estimation $\hat x^\I(t)$ at time $t$ to generate the state estimate $\hat x(t)$. 
When $q = \infty$, we fuse as:
\begin{eqnarray}
  \label{eq:meanest}
  \hat x_i(t) = {1 \over 2} \Big( \min_{\mi \in \comset(t)} \hat x^\mi_i (t)  + \max_{\mi \in \comset(t)} \hat x^\mi_i (t) \Big).
\end{eqnarray} 
When $q = 2$, we fuse as:
\begin{eqnarray}
  \label{eq:meanest}
  \hat x(t) = {1 \over |\comset(t)|} \sum_{\mi(t) \in \comset(t)} \hat x_\mi(t). 
\end{eqnarray} 

\subsection{Estimator performance analysis}
We now provide an upper bound on the worst-case estimation error $\rho(f)$. 
\BTHM
\label{thm:esterror}
Under Assumption A-D, the state estimator described in Section~\ref{sec:design} is a resilient estimator for system \eqref{eq:system}. Furthermore, when $q = \infty$ ($p = 2, \infty$), following inequality holds:
\begin{align}
  \rho(f)  \leq  \max_{\I,\J\in \mathcal L} \Big( \| E^\mi(K^\I) \|_{p \rightarrow \infty}   + {1 \over 2}  \alpha^{\I\cap\J} [\beta^\I(K^\I)+\beta^\J(K^\J)] \Big) \epsilon
  \label{eq:worstcaseerror}
\end{align}
where $\alpha^\K$ is defined as 
\begin{align*}
  \alpha^\K  \triangleq \inf_{K:A+KC_\K\text{ strictly stable}}\left\| \left[
\begin{array}{c|c}
  A+K C_\K & \begin{bmatrix}
    I&K
  \end{bmatrix}\\
  \hline
  I &  0
\end{array}
\right]\right\|_1,
\end{align*}
and $\beta^\I(K^\I)$ is defined as
\begin{align*}
 \beta^\I(K^\I) \triangleq \max(\|K^\I\|_{\infty \rightarrow \infty},1)\,\|G^\I(K^\I)\|_{p \rightarrow \infty}. 
\end{align*}
\ETHM

\begin{remark}
  It is worth noticing that if $\I$ does not contain compromised sensors, then minimizing the infinite norm of the local estimation error $e^\I(t)$ is equivalent to minimizing $\| E^\mi(K^\I) \|_1$. The second term on the RHS of \eqref{eq:worstcaseerror} exists since the estimator does not know which local estimate can be trusted at the beginning.
\end{remark}

We use lemma \ref{lemma:difference}-\ref{lemma:mediandivergence} to prove theorem \ref{thm:esterror}. 
 
\BLEM(Divergence of local estimators)
\label{lemma:difference}
For any two index sets $\I,\,\J\in \mathcal L(T)$, the following inequality holds:
\begin{align}
 \| \hat x^{\I}(0:T) - \hat x^{\J}(0:T) \|_\infty  \leq \varepsilon\alpha^{\I\cap\J}[\beta^\I(K^\I)+\beta^\J(K^\J)].
 \label{eq:localestimationdivergence}
\end{align}
\ELEM
\BPF
By \eqref{eq:est}, we have
\begin{align}
  \hat x^\I(t+1) &= A\hat x^\I(t) - K^\I r^\I(t),\,\hat x^\I(t) = 0,\nonumber\\
  y_\I(t) &= C_\I \hat x^\I(t) + r^\I(t).
  \label{eq:suberror}
\end{align}
Let us define $\mathcal K = \I\cap\J$, we know that
\begin{align*}
  y_\K(t) = C_\K \hat x^\I(t) + P_{\K,\I} r^\I(t),
\end{align*}
where $P_{\K,\I}\in \mathbb R^{|\K|\times|\I|}$ is the unique matrix that satisfies:
\begin{align*}
  P_\K = P_{\K,\I} P_\I. 
\end{align*}
Now let us define $\phi^\I(t) \triangleq -K^\I r^\I(t)$ and $\varphi^\I(t) \triangleq P_{\K,\I} r^\I(t)$. If $t\leq T$, we know that \eqref{eq:localdetection1} holds at time $t$, which implies that
\begin{align*}
 \|r^\I(0:T)\|_\infty\leq\|G^\I(K^\I)\|_{p \rightarrow q} \;\varepsilon.
\end{align*}
As a result,
\begin{align*}
  \left\|\begin{bmatrix}
\phi^\I(0:T)\\
\varphi^\I(0:T)
  \end{bmatrix}\right\|_\infty &\leq    \left\|\begin{bmatrix}
-K^\I\\
P_{\K,\I}
  \end{bmatrix}\right\|_{\infty \rightarrow \infty} \|r^\I(0:T)\|_\infty \\
  &= \beta^\I(K^\I)\;\varepsilon,
\end{align*}
where we use the fact that each row of $P_{\K,\I}$ is a canonical basis vector in $\mathbb R^{|\I|}$. Therefore, we have
\begin{align}
  \hat x^\I(t+1) &= A\hat x^\I(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)\\
    \varphi^\I(t)
  \end{bmatrix},\,\hat x^\I(t) = 0,\nonumber\\
  y_\K(t) &= C_\K \hat x^\I(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)\\
    \varphi^\I(t)
  \end{bmatrix}.
  \label{eq:suberror2}
\end{align}
Similarly, we have
\begin{align}
  \hat x^\J(t+1) &= A\hat x^\J(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\J(t)\\
    \varphi^\J(t)
  \end{bmatrix},\,\hat x^\J(t) = 0,\nonumber\\
  y_\K(t) &= C_\K \hat x^\J(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\J(t)\\
    \varphi^\J(t)
  \end{bmatrix},
  \label{eq:suberror3}
\end{align}
with
\begin{align*}
 \left\|\begin{bmatrix}
    \phi^\J(0:T)\\
    \varphi^\J(0:T)
  \end{bmatrix} \right\|_\infty \leq \beta^\J(K^\J)\;\varepsilon.
\end{align*}

Now let us consider $\Delta\hat x(t) = \hat x^\mi(t)-\hat x^\mj(t)$. By \eqref{eq:suberror2} and \eqref{eq:suberror3}, we know that
\begin{align*}
  \Delta \hat x(t+1) &= A\Delta \hat x(t) + \begin{bmatrix} 
    I & 0 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)-\phi^\J(t)\\
    \varphi^\I(t)-\varphi^\J(t)
  \end{bmatrix},\,\Delta\hat x(t) = 0,\nonumber\\
  0 &= C_\K \Delta \hat x(t) +  \begin{bmatrix} 
    0 & I 
  \end{bmatrix}\begin{bmatrix}
    \phi^\I(t)-\phi^\J(t)\\
    \varphi^\I(t)-\varphi^\J(t)
  \end{bmatrix}.
\end{align*}
Hence, \eqref{eq:localestimationdivergence} can be proved by Lemma~\ref{lemma:y0}.
\EPF

\begin{lemma}
  \label{lemma:mediandivergence}
Let $r_1,\dots,r_l$ be real numbers. Define
\begin{align*}
  r = \frac{1}{2}\left(\max_i r_i + \min_i r_i\right).
\end{align*}
Then for any $i$, we have
\begin{align}
  |r - r_i| \leq \frac{1}{2} \max_j |r_j-r_i|. 
  \label{eq:mediandivergence}
\end{align}
\end{lemma}
\begin{proof}
  Without loss of generality, we assume that $r_1$ and $r_2$ are the largest and the smallest number among all $r_i$s respectively. Therefore, 
  \begin{align*}
    r-r_i = \frac{1}{2}(r_1 - r_i) -\frac{1}{2} \left(r_i- r_2\right).
  \end{align*}
  Therefore, if $r_1-r_i \geq r_i - r_2$, then
  \begin{align*}
    |r-r_i| = r-r_i \leq \frac{1}{2}(r_1-r_i) = \frac{1}{2}\max_j|r_j-r_i|.
  \end{align*}
  Similarly, one can prove that \eqref{eq:mediandivergence} holds when $r_1-r_i < r_i - r_2$.
\end{proof}
Now we are ready to prove Theorem~\ref{thm:esterror}. 
\BPF
Let $\mathcal G \in \mathcal L$ be the set of non-compromised sensors. By Lemma~\ref{lemma:nec_original}, we know that
\begin{align*}
  \|x - \hat x^{\mathcal G}\|_\infty \leq \|E^{\mathcal G}(K^{\mathcal G})\|_{p \rightarrow \infty} \varepsilon.
\end{align*}
Furthermore, $\mathcal G\in \mathcal L(t)$ for all $t$. At any given time $t$, assuming that the index set $\mathcal J$ also belongs to $\mathcal L(t)$. By Lemma~\ref{lemma:difference}, we have
\begin{align*}
  \|\hat x^{\mathcal G}(t) - \hat x^{\mathcal J}(t)\|_\infty \leq \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)].
\end{align*}
Therefore, by Lemma~\ref{lemma:mediandivergence}, we know that
\begin{align*}
  \|\hat x(t) -  \hat x^{\mathcal G}(t)\|_\infty &\leq\frac{1}{2}\max_{\J\in \mathcal L(t)} \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)]\\
  &\leq\frac{1}{2}\max_{\J\in \mathcal L} \varepsilon\alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)].
\end{align*}
By triangular inequality, we have
\begin{align}
  \|e(t)\|_\infty &\leq   \|E^{\mathcal G}(K^{\mathcal G})\|_{p \rightarrow \infty}\varepsilon\label{eq:errortime}\\
 \nonumber &+\frac{1}{2}\max_{\J\in \mathcal L} \alpha^{\mathcal G\cap\J}[\beta^{\mathcal G}(K^{\mathcal G})+\beta^\J(K^\J)]\varepsilon.
\end{align}
Thus, by taking the supremum over all possible non-compromised sensor set $\mathcal G$, we reach \eqref{eq:worstcaseerror}.
\EPF

Combining Theorem~\ref{thm:nonexistence} and Theorem~\ref{thm:esterror}, we have the following corollary:
\begin{corollary}
  A necessary and sufficient condition for the existence of a resilient estimator is that $(A,C_\K)$ is detectable for any index set $\K\subset \mathcal S$ with cardinality $2\gamma$.
\end{corollary}
